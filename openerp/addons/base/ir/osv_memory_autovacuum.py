# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2010 OpenERP s.a. (<http://openerp.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


import re
import openerp
import pdb
import psycopg2
import csv
import time
import urllib2
import datetime
import logging
from datetime import datetime
from dateutil.relativedelta import relativedelta
from operator import itemgetter
import time
import xmlrpclib
import os
import ftplib
import socket
# from voylla_modules.config import home 
import openerp
import pdb
from openerp import SUPERUSER_ID
from openerp import pooler, tools
from openerp.osv import fields, osv, expression
from openerp.tools.translate import _
from openerp.tools.float_utils import float_round as round

import openerp.addons.decimal_precision as dp

class osv_memory_autovacuum(openerp.osv.osv.osv_memory):
    """ Expose the osv_memory.vacuum() method to the cron jobs mechanism. """
    _name = 'osv_memory.autovacuum'
    def update_costmethod(self,cr,uid,context=None):
      product=self.pool.get("product.template")
      k=product.search(cr,uid,[('cost_method','=',False)])
      print 'start'
      product.write(cr,uid,k,{'cost_method':'real'})
      print 'done'
    def update_variant_count(self,cr,uid,context=None):
      product=self.pool.get('product.template')
      attribute_line=self.pool.get('product.attribute.line')
      p_ids=attribute_line.search(cr,uid,[('id','>',0)])
      p=attribute_line.browse(cr,uid,p_ids)
      for i in p:
        if len(i.value_ids)!=len(i.product_tmpl_id.product_variant_ids):
          array=[]
          def move(x):
            return x.id
          check=map(move,i.value_ids)
          prod1=i.product_tmpl_id.product_variant_ids
          for k in prod1:
            if k.attribute_value_ids[0].id not in check:
              line_id=i.id
              val_id=val_id=k.attribute_value_ids[0].id
              sql='INSERT INTO product_attribute_line_product_attribute_value_rel (line_id,val_id) VALUES (%s,%s)'
              cr.execute(sql,(line_id,val_id))
              cr.commit()
          print 'start processing'
        print 'commplete'







    def update_variant(self,cr,uid,context=None):
      product_obj=self.pool.get('product.product')
      reader = open('update_ean.csv','rb').readlines()
      firstline=True
      for col in reader:
        if firstline==True:
          firstline=False
          continue
        row=col.split(',')
        ids=product_obj.search(cr,uid,[('name_template','=',row[0]),('active','=',True)])
        product_obj.write(cr,uid,ids,{'ean13':row[1]})
        print row[0]
        print 'done'
    def update_variant1(self,cr,uid,context=None):
      product_obj=self.pool.get('product.product')
      reader = open('update_ean1.csv','rb').readlines()
      firstline=True
      for col in reader:
        if firstline==True:
          firstline=False
          continue
        row=col.split(',')
        ids=product_obj.search(cr,uid,[('ean13','=',row[1])])
        product_obj.write(cr,uid,ids,{'ean13':row[3]})
        print row[0]
        print 'done'
    def ean_remove(self,cr,uid,context=None):
      product_obj=self.pool.get('product.template')
      reader = open('tax_rates_final.csv','rb').readlines()
      list_tax=['CST_2%','CST_1%','RAJ_VAT_0%','RAJ_VAT_1%','RAJ_VAT_5%','RAJ_VAT_14%']
      tax=self.pool.get('account.tax')
      ids=tax.search(cr,uid,[('description','in',list_tax)])
      t=tax.browse(cr,uid,ids)
      temp_hash={}
      for x in t:
        temp_hash.update({x.description:x.id})
      firstline=True
      for col in reader:
        row=col.split(',')
        ids=product_obj.search(cr,uid,[('name','=',row[0])])
        vals={'supplier_taxes_id': [[6, False,[temp_hash[row[1].split('\n')[0]]]]]}
        product_obj.write(cr,uid,ids,vals)
        cr.commit()
        print row[0]
    def partner(self,cr,uid,context=None):
      supplier=self.pool.get('res.partner')
      reader = open('supplier.csv','rb').readlines()
      firstline=True
      for col in reader:
        if firstline==True:
          firstline=False
          continue
        row=col.split(',')
        vals={'ref':row[3],'notify_email': 'always', 'active': True, 'street': row[6], 
          'city': row[5],'supplier_code': row[0], 'supplier': True, 'type': 'contact',
           'email':row[2], 'phone': row[7],'name': row[1], 'mobile': row[4]}

        
        supplier_id=supplier.search(cr,uid,[('supplier_code','=',row[0])])
        if len(supplier_id)!=0:
          supplier.write(cr,uid,supplier_id,vals)
        else:
          supplier.create(cr,uid,vals)
          cr.commit()
    def submit_file(self,filename,basename):
    	import_file=open(filename, 'rb')
      	le = len(import_file.readlines())
      	if le!=1:
        	ftp = ftplib.FTP("staging.voylla.com")
        	ftp.login("ErpProductUpload", "ErpProductUpload") #(username, password)
        	ftp.storlines("STOR " + basename, open(filename, 'rb'))

    def delete_variant(self,cr,uid,context=None):
      product_template=self.pool.get('product.template')
      product_obj=self.pool.get('product.product')
      product_attribute_value=self.pool.get('product.attribute.value')
      product_attribute_line=self.pool.get('product.attribute.line')
      cr.execute("""(select distinct(att_id) from product_attribute_value_product_product_rel)""")
      res=cr.fetchall()
      for temp in res:
        d=[]
        d.append(temp[0])
        cr.execute("""(select * from product_attribute_value_product_product_rel where (att_id=%s))""",d)
        products=cr.fetchall()
        def make(x):
          return x[1]
        temp_variants=map(make,products)
        for per_products in temp_variants:
          a=product_obj.browse(cr,uid,per_products).product_tmpl_id.product_variant_ids
          def ids_array(x):
            return x.id
          temp_product_id=map(ids_array,a)
          print temp_product_id
          temp_product_id=list(set(temp_product_id) - set([per_products]))
          update_variants =list(set(temp_variants) & set(temp_product_id))
          temp_variants =list(set(temp_variants) - set(update_variants)- set([per_products]))
          if len(update_variants)!=0:
            product_obj.write(cr,uid,update_variants,{'active':False})
          print 'done'


    def dimension_import(self,cr,uid,context=None):
       reader = open('dimension.csv','rb').readlines()
       taxon_dim_obj=self.pool.get('product.dimension')
       firstline=True
       for row in reader:
        
        if firstline==True:
          firstline=False
          continue
        col_=row[1:-1].split(',')
        ids=self.pool.get('product.taxon').search(cr,uid,[('name','=',col_[0] )])
        print col_[0] 
        if len(ids)==0:
          vals={'name':col_[0]}
          ids=self.pool.get('product.taxon').create(cr,uid,vals)
        for row_ in col_[1:]:
          temp={}
          if len(row_)==0:
            continue

          name_=row_.split('(')
          unit='cm'
          if len(name_)!=1: 
            unit=name_[1].split(')')[0]
          dimension_num=str(col_.index(row_))
          temp={'name':name_[0],'unit':unit,'dimension_num':dimension_num,'value':0}
          if type(ids) is list:
            temp.update({'taxon_id':ids[0]})
          else:
            temp.update({'taxon_id':ids})
          taxon_dim_obj.create(cr,uid,temp)

    def tag_import(self,cr,uid,context=None):
       reader = open('tag.csv','rb').readlines()
       taxon_dim_obj=self.pool.get('product.tagvalue')
       for row in reader:
        col_=row[:-1].split(',')
        for col in col_[1:]:
          val={}
          if len(col)==0:
            continue
          val.update({'tag_type':col_[0],'name':col})
          taxon_dim_obj.create(cr,uid,val)
    def var_fromean(self,cr,uid,context=None):
      prod=self.pool.get('product.template')
      var_obj_line=self.pool.get('product.attribute.line')
      var_obj=self.pool.get('product.attribute.value')
      prod_prod=self.pool.get('product.product')

      reader = open('ean_var.csv','rb').readlines()
      p=len(reader)
      factor=p/200
      remainder=p%200
      start=0
      end=199
      count=0
      firstline=True
      product_no_of=[]
      print 'start processing'
      while( count < factor+1):
        count+=1
        my_list=reader[start : end]
        if count==factor:
          start+=200
          end+=remainder
        else:
          start+=200
          end+=200

        
        for col in my_list:
          row=col[:-1].split(',') 
          print row[0]
          if row[1]!='':
            product_id=prod.search(cr,uid,[('name','=',row[0])])
            if len(product_id)==0:
              print 'product is not there'
              print row[0]
              continue
            prod1=prod.browse(cr,uid,product_id)
            attribute_id=var_obj.search(cr,uid,[('name','=',row[1]),('attribute_id','=',1)])
            if len(attribute_id)==0:
              temp_attribute={}
              temp_attribute.update({'name':row[2],'attribute_id':1})
              attribute_id.append(var_obj.create(cr,uid,temp_attribute))
            jj=var_obj_line.search(cr,uid,[('product_tmpl_id','=',product_id[0])])
            prod1=prod.browse(cr,uid,product_id).product_variant_ids
            flag_temp=False
            for xz in prod1:
              if len(xz.attribute_value_ids)!=0 and xz.attribute_value_ids.id==attribute_id[0]:
                xz.write({'ean13':row[2]})
                prod_prod.create(cr,uid,{'product_tmpl_id': product_id[0], 'attribute_value_ids': [(6, 0,attribute_id)],'ean13':row[2]})
                cr.commit()
                flag_temp=True
            if flag_temp:
              continue
            for  tt in prod.browse(cr,uid,product_id).attribute_line_ids.value_ids:
              if tt.id==attribute_id[0]:
                prod_prod.create(cr,uid,{'product_tmpl_id': product_id[0], 'attribute_value_ids': [(6, 0,attribute_id)],'ean13':row[2]})
                cr.commit()
                flag_temp=True
            if flag_temp:
              continue


            
            if len(jj)!=0:
              prod_prod.create(cr,uid,{'product_tmpl_id': product_id[0], 'attribute_value_ids': [(6, 0,attribute_id)],'ean13':row[2]})
              sql='INSERT INTO product_attribute_line_product_attribute_value_rel (line_id,val_id) VALUES (%s,%s)'
              cr.execute(sql,(jj[0],attribute_id[0]))
              cr.commit()
            else:
              check_temp={'ean13':row[2],'attribute_line_ids': [[0, False, {'attribute_id': 1, 'value_ids': [[6, False, attribute_id]]}]]}
              prod1.write(check_temp)
              prod1.product_variant_ids.write({'ean13':row[2],'attribute_value_ids': [(4, attribute_id[0])]})
          else:
            product_id=prod.search(cr,uid,[('name','=',row[0])])
            if len(product_id)==0:
              print row[0]
              print 'product in not there ' 
              continue
            prod.write(cr,uid,product_id,{'ean13':row[2]})

        print 'end processing'

    def check(self,cr,uid,context=None):
      prod_obj=self.pool.get('product.template')
      product_product_object=self.pool.get('product.product')
      product_attribute_value=self.pool.get('product.attribute.value')
      reader = open('all_product.csv','rb').readlines()
      p=len(reader)
      factor=p/200
      remainder=p%200
      start=0
      end=199
      count=0
      firstline=True
      while( count < factor+1):
        count+=1
        my_list=reader[start : end]
        if count==factor:
          start+=200
          end+=remainder
        else:
          start+=200
          end+=200

        
        for col in my_list:
          row=col[:-1].split(',') 
          if firstline==True:
            firstline=False
            continue
          product_id=prod_obj.search(cr,uid,[('name','=',row[0])])
          if len(product_id)==0:
              print row[0]
              print 'sku not found'
              continue
          if row[1]=='':
              product_product_id=self.pool.get('product.product').search(cr,uid,[('product_tmpl_id','=',product_id[0]),('active','=',True)])
              if len(product_product_id)==1:
                product_product_object.write(cr,uid,product_product_id[0],{'ean13':row[1],'threep_reference':row[3]})
          else:
              k=product_attribute_value.search(cr,uid,[('name','=',row[2])])
              p=product_attribute_value.read(cr,uid,k,['product_ids'])
              product_product_id=prod_obj.read(cr,uid,product_id,['product_variant_ids'])
              if len(product_product_id)==0 or len(p)==0:
                continue
              temp_list=list(set(p[0]['product_ids']) & set(product_product_id[0]['product_variant_ids']))
              if len(temp_list)!=0:
                product_product_object.write(cr,uid,temp_list[0],{'ean13':row[1],'threep_reference':row[3]})
          print 'done'
          cr.commit()
          print row[0]


  

    def temp_method(self,cr,uid,model,vals):
      obj=self.pool.get(model).search(cr,uid,[('name','=',vals)])
      if len(obj)==0:
        vals_temp={'name':vals}
        return self.pool.get(model).create(cr,uid,vals_temp)
      else:
        return obj[0]

    def tag_to_product(self,cr,uid,vals,context=None):
      product_tagvalue_obj=self.pool.get('product.tagvalue')
      product_tagname_obj=self.pool.get('product.tag.name')
      tag_temp=[]
      for tg in vals:
        ids=[]
        ids=product_tagvalue_obj.search(cr,uid,[('name','=',tg[1]),('tag_type','=',tg[0])])
        if len(ids)==0:
          temp_val={}
          temp_val.update( {'name':tg[1],'tag_type':tg[0] } )
          ids.append(product_tagvalue_obj.create(cr,uid,temp_val))
        tag_temp.append([0,False,{'tag_type':tg[0],'user_type':ids[0]}])
      return tag_temp
    def variant_creation(self,cr,uid,value,ids,ean,context=None):
      var_obj_line=self.pool.get('product.attribute.line')
      var_obj=self.pool.get('product.attribute.value')
      prod_prod=self.pool.get('product.product')
      prod=self.pool.get('product.template').browse(cr,uid,ids)
      attribute_id=var_obj.search(cr,uid,[('name','=',value),('attribute_id','=',1)])
      if len(attribute_id)==0:
        temp_attribute={}
        temp_attribute.update({'name':value,'attribute_id':1})
        attribute_id.append(var_obj.create(cr,uid,temp_attribute))
      jj=var_obj_line.search(cr,uid,[('product_tmpl_id','=',ids)])
      if len(jj)!=0:
        product_id_array=var_obj.read(cr,uid,attribute_id,['product_ids'])[0]['product_ids']
        product_ids_from_product_template=prod.product_variant_ids
        def ids_array(x):
          return x.id
        temp_product_id=map(ids_array,product_ids_from_product_template)
        if len(list(set(temp_product_id) & set(product_id_array)))!=0:
          return True
        prod_prod.create(cr,uid,{'product_tmpl_id': ids, 'attribute_value_ids': [(6, 0,attribute_id)],'ean13':ean})
      else:
        check_temp={'ean13':ean,'attribute_line_ids': [[0, False, {'attribute_id': 1, 'value_ids': [[6, False, attribute_id]]}]]}
        prod.write(check_temp)
        prod.product_variant_ids.write({'ean13':ean,'attribute_value_ids': [(4, attribute_id[0])]})
    
    def product_category(self,cr,uid,value,context=None):
      obj_product_category=self.pool.get('product.taxonomy')
      product_category=self.pool.get('product.category')
      real_categ=[]
      if len(value)==0:
        return real_categ
      for categ in value.split('|'):
        id_1=id_2=id_3=[]
        temp_categ=categ.split(':')
        if temp_categ[0]!=False:
          id_1=product_category.search(cr,uid,[('name','=',temp_categ[0])])
          if len(id_1)==0:
            id_1.append( product_category.create(cr,uid,{'name':temp_categ[0]}) )
          if temp_categ[1]!=False:
            id_2=product_category.search(cr,uid,[('name','=',temp_categ[1]),('parent_id','=',id_1[0] )])
            if len(id_2)==0:
              id_2.append(product_category.create(cr,uid,{'name':temp_categ[1],'parent_id':id_1[0]}))
            if temp_categ[2]!=False and temp_categ[2]!='':
              id_3=product_category.search(cr,uid,[('name','=',temp_categ[2]),('parent_id','=',id_2[0] )])
              if len(id_3)==0:
                id_3.append(product_category.create(cr,uid,{'name':temp_categ[2],'parent_id':id_2[0]}))
              real_categ.append([0, False, {'categ_id': id_3[0]}])
            else:
              real_categ.append([0, False, {'categ_id': id_2[0]}])
          else:
            real_categ.append([0, False, {'categ_id': id_1[0]}])
      return real_categ
    def product_property(self,cr,uid,value,context=None):
      property_1=value.split('|')
      check={}
      for temp in property_1:
        k=temp.split(':')
        if k[0]=='sales_package':
          temp_obj=self.pool.get('product.salespackage')
          ll=temp_obj.search(cr,uid,[('name','=',k[1].strip())])
          if len(ll)==0:
            ll.append(temp_obj.create(cr,uid,{'name':k[1].strip()}))
          check.update({'salespackage':ll[0]})
        if k[0]=='gender':
          temp_obj=self.pool.get('product.gender')
          ll=temp_obj.search(cr,uid,[('name','=',k[1].strip())])
          if len(ll)==0:
            ll.append(temp_obj.create(cr,uid,{'name':k[1].strip()}))
          check.update({'gender':ll[0]})
        if k[0]=='theme':
          temp_obj=self.pool.get('product.theme')
          ll=temp_obj.search(cr,uid,[('name','=',k[1].strip())])
          if len(ll)==0:
            ll.append(temp_obj.create(cr,uid,{'name':k[1].strip()}))
          check.update({'theme_':ll[0]})
        if k[0]=='design':
          temp_obj=self.pool.get('product.design')
          ll=temp_obj.search(cr,uid,[('name','=',k[1].strip())])
          if len(ll)==0:
            ll.append(temp_obj.create(cr,uid,{'name':k[1].strip()}))
          check.update({'design':ll[0]})
        if k[0]=='surfacefinish':
          temp_obj=self.pool.get('product.surfacefinish')
          ll=temp_obj.search(cr,uid,[('name','=',k[1].strip())])
          if len(ll)==0:
            ll.append(temp_obj.create(cr,uid,{'name':k[1].strip()}))
          check.update({'surface_finish_one':ll[0]})
        if k[0]=='color_palette' or k[0]=='primary_base_colour':
          temp_obj=self.pool.get('product.color')
          temp_str=re.split('And | and',k[1])[0].strip()
          ll=temp_obj.search(cr,uid,[('name','=',temp_str)])
          if len(ll)==0:
            ll.append(temp_obj.create(cr,uid,{'name':temp_str}))
          check.update({'color_one':ll[0]})
        if k[0]=='secondry_base_colour':
          temp_obj=self.pool.get('product.seccolor')
          ll=temp_obj.search(cr,uid,[('name','=',k[1].strip())])
          if len(ll)==0:
            ll.append(temp_obj.create(cr,uid,{'name':k[1].strip()}))
          check.update({'color_two':ll[0]})
        if k[0]=='primary_gemstone_colour':
          temp_obj=self.pool.get('product.gemcolor')
          ll=temp_obj.search(cr,uid,[('name','=',k[1].strip())])
          if len(ll)==0:
            ll.append(temp_obj.create(cr,uid,{'name':k[1].strip()}))
          check.update({'gem_color_one':ll[0]})
        if k[0]=='secondry_gemstone_colour':
          temp_obj=self.pool.get('product.gemseccolor')
          ll=temp_obj.search(cr,uid,[('name','=',k[1].strip())])
          if len(ll)==0:
            ll.append(temp_obj.create(cr,uid,{'name':k[1].strip()}))
          check.update({'gem_color_two':ll[0]})
        if k[0]=='age_group':
          check.update({'agegroup':k[1].strip()})
      return check

    def test(self,cr,uid,context=None):
      reader = open('product_import.csv','rb').readlines()
      p=len(reader)
      factor=p/200
      remainder=p%200
      start=0
      end=199
      count=0
      firstline=True
      while( count < factor+1):
        count+=1
        my_list=reader[start : end]
        if count==factor:
          start+=200
          end+=remainder
        else:
          start+=200
          end+=200

        
        for col in my_list:
          row=col[:-1].split(',') 
          if firstline==True:
            firstline=False
            continue
          prod=self.pool.get('product.template')
          if len(row[18])!=0:
            ll=prod.search(cr,uid,[('name','=',row[0])])
            if len(ll)!=0 :
              self.variant_creation(cr,uid,row[18],ll[0],row[21])
              continue
          if len(prod.search(cr,uid,[('name','=',row[0])]))!=0:
            continue
          categ_id=self.product_category(cr,uid,row[5])
          product_pro=self.product_property(cr,uid,row[6])
          supplier_id=self.pool.get('res.partner').search(cr,uid,[('supplier_code','=',row[2])])
          if len(supplier_id)==0:
            email=row[2]+'@voylla.com'
            supplierinfo_detail={'name':row[1],'supplier_code':row[2],'supplier':True,'detailed_bio':row[4],
            'brief_bio':row[3],'email':email}
            supplier_id.append(self.pool.get('res.partner').create(cr,uid,supplierinfo_detail,context=None))
          temp_val_tag=[]
          property_1=row[8].split('|')
          check={}
          for temp in property_1:
            temp_tag=temp.split(':')
            if temp_tag[0]=='collection':
              temp_val_tag.append(['collection',temp_tag[1]])
            if temp_tag[0]=='type':
              temp_val_tag.append(['type',temp_tag[1]])

            if temp_tag[0]=='material':
              temp_val_tag.append(['material',temp_tag[1] ])

            if temp_tag[0]=='plating':
              temp_val_tag.append(['plating',temp_tag[1]])

            if temp_tag[0]=='free_products':
              temp_val_tag.append(['free_product',temp_tag[1]])
            if temp_tag[0]=='gemstone':
              temp_val_tag.append(['gemstone',temp_tag[1]])
            if temp_tag[0]=='home_page_link':
              temp_val_tag.append(['home_page',temp_tag[1]])
            if temp_tag[0]=='occasion':
              temp_val_tag.append(['occassion',temp_tag[1]])
          temp_tag=self.tag_to_product(cr,uid,temp_val_tag)
          vals={'ean13':row[21],'uom_id':1,'cost_method':'real','taxonomy':categ_id,'name':row[0],'state':'end','company_id':1,
          'uom_po_id':1,'type':'product','description':row[17],'valuation':'manual_periodic','active':True,
          'product_name':row[16],'list_price':row[13],'standard_price':row[11],'mrp':row[12],'tagid':temp_tag,'seller_ids': [[0, False, {'pricelist_ids': [], 
          'name': supplier_id[0], 'company_id': 1, 'delay': 1, 'min_qty': 0, 'product_code': row[15] }]]}
          vals.update(product_pro)
          if 'agegroup' not in vals:
            vals.update({'agegroup':'20+ Yrs'})
          prod_id=prod.create(cr,uid,vals,context=None)
          cr.commit()
          print row[0]
          if len(row[18])!=0:
            temp_val={'variants_size':row[18],'variants_price':row[13],'variant_id':prod_id,'variants_ean':'var'}
            self.variant_creation(cr,uid,row[18],prod_id,row[21])

    def product_import(self, cr, uid, context=None):
       k=datetime.now().strftime("%H-%M-%d-%m-%y")
       obj = self.pool.get('product.template')
       sql=''
       #if context=='update_flag':
       sql="Select id from product_template where write_date >= NOW() - '9 Hour 30 minute'::INTERVAL and flag_complete"
       #else:
       #sql="Select id from product_template where flag_complete "
       cr.execute(sql)
       res_val = cr.fetchall()
       ids = [ r[0] for r in res_val]
       res=obj.browse(cr,uid,ids)
       if len(res)==0:
        return 
       header=['VOYLLA_PRODUCT_CODE ','Designer Product Code' ,'Designer Name' ,'Designer Id' ,'Variant', 'Sales Package' ,
       'Buyer Mrp' ,'Retail Price' , 'Cost' , 'Quantity' , 'Product Name' , 'Product Description', 
       'Gender' , 'Taxonomy' , 'Taxons' , 'tag_type_1' , 'tag_type_2' , 'Primary Base Color' , 
       'Secondary Base Color' , 'Primary Stone Color', 'Secondary Stone Color' ,'Theme' ,'Design' 
        ,'Surface Finish' , 'Age Group','tag_collection_1' , 'tag_occasion_1' , 'tag_occasion_2' , 
        'tag_material_1' , 'tag_material_2' , 'tag_plating_1', 'tag_plating_2', 'tag_gemstones_1', 
        'tag_gemstones_2' ,'Tag_Free Product' , 'TAG_HOME_PAGE_LINK_1' , 'update_available_Date' ,'MOD' ,'LeadTime' ,
        'Diamond_price','Making Charge','Gemstone Price','ean','Dimension1','Dimension2','Dimension3','Dimension4','Dimension5','  Dimension6','  Dimension7','  Dimension8','  Dimension9','  Dimension10',' Dimension11',' Dimension12',' Dimension13',' Dimension14',' Dimension15',' Dimension16 ','Dimension17 ','Dimension18',' Dimension19',' Dimension20',' Dimension21',' Dimension22',' Dimension23',' Dimension24',' Dimension25',' Dimension26','Dimension27','Dimension28',' Dimension29',' Dimension30',' Dimension31',' Dimension32', 'Dimension33', 'Dimension34' ,'Dimension35', 'Dimension36' ,'Dimension37' ,'Dimension38', 'Dimension39' ,'Dimension40']
       d=[]
       d.append(header)
       for val in res:
        a=['']*80
        a[0]=val.name
        if len(val.seller_ids)!=0:
          a[1]=val.seller_ids[0].product_name#1
          a[2]=''#2
          a[3]=val.seller_ids[0].name.supplier_code
        #4variant
        if len(val.salespackage)!=0:
          a[5]=val.salespackage.name#5salespackage
        #price
        a[6]=val.mrp
        #6
        a[7]=val.list_price #7
        a[8]=val.standard_price#
        #quantity
        a[9]= 0#9
        #name
        if val.product_name==False:
          a[10]='Name is missing'
        else:
          a[10]=val.product_name.encode('utf-8')#10
        #description
        if val.description==False:
          a[11]="description is missing"#11
        else:
          a[11]=val.description.encode('utf-8')

        #gender
        if len(val.gender)!=0:
          a[12]=val.gender.name#12
        #taxonomy
        taxonomy_full=''
        taxon_full=''
        type_full=''
        for category in val.taxonomy:
          name=category.categ_id.complete_name.split('/')
          count=len(name)
          if count >=1:
            if taxonomy_full=='':
              taxonomy_full=name[0].strip()
            else:
              taxonomy_full+='$'+name[0].strip()

          if count >=2:
            if taxon_full=='':
              taxon_full+=name[1].strip()
            else:
              taxon_full+='$'+name[1].strip()
          if count >=3:
            if type_full=='':
              type_full=name[2].strip()
            else:
              type_full+='$'+name[2].strip()


        
        a[13]=taxonomy_full#13
        #taxon
        a[14]=taxon_full
        a[15]=type_full
        #color
        if len(val.color_one)!=0:
          a[17]=val.color_one.name
        #color_2
        if len(val.color_two)!=0:
          a[18]=val.color_two.name
        #gemstone color
        if len(val.gem_color_one)!=0:
          a[19]=val.gem_color_one.name
        if len(val.gem_color_two)!=0:
          a[20]=val.gem_color_two.name
        #theme
        if len(val.theme_)!=0:
          a[21]=val.theme_.name
        #design
        if len(val.design)!=0:
          a[22]=val.design.name
        #surface finish
        if len(val.surface_finish_one)!=0:
          a[23]=val.surface_finish_one.name
        #age_group
        a[24]=val.agegroup
        
        tags=[]
        for t in val.tagid:
          tags.append([t.tag_type,t.user_type.name])
        type_flag=True
        type_occassion=True
        type_material=True
        type_plating=True
        type_gemstone=True
        for k in tags:
          if k[0]=='collection':
            a[25]=k[1]
          elif k[0]=='occassion':
            if type_occassion:
              a[26]=k[1]
              type_occassion=False
            else:
              a[27]=k[1]
          elif k[0]=='material':
            if type_material:
              a[28]=k[1]
              type_material=False
            else:
              a[29]=k[1]


          elif k[0]=='plating':
            if type_plating:
              a[30]=k[1]
              type_plating=False
            else:
              a[31]=k[1]

          elif k[0]=='gemstone':
            if type_gemstone:
              a[32]=k[1]
              type_gemstone=False
            else:
              a[33]=k[1]

          elif k[0]=='free_product':
            a[34]=k[1]
          elif k[0]=='home_page':
            a[35]=k[1]
        a[36]='Yes'
        a[37]='No'
        a[38]=''
        dim_array_val=[]
        a[42]=val.masterean
        temp_array_variants_num=a[:]
        for t in val.childdimid:
          if t.value > 0:
            dim_array_val.append([t.dimension_num,t.value]) 
        for p in dim_array_val:
          a[42+int(p[0])]=float(p[1])
        d.append(a)
        if len(val.attribute_line_ids)!=0:
          for product in val.product_variant_ids:
            temp_array_variants=temp_array_variants_num[:]
            temp_array_variants[4]=product.attribute_value_ids.name
            temp_array_variants[7]+=int(product.price_extra)
            temp_array_variants[42]=product.ean13
            dim_array_val=[]
            for t in product.childdimid:
              if t.value > 0:
                dim_array_val.append([t.dimension_num,t.value]) 
            for p in dim_array_val:
              temp_array_variants[42+int(p[0])]=float(p[1])
            d.append(temp_array_variants)
       k=datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
       if context=='update_flag':
        k='update_product_'+k+'.csv'
       else:
        k='product_'+k+'.csv'
       k1=home+'import/'+k
       with open(k1, "wb") as f:
          writer = csv.writer(f)
          writer.writerows(d)
          f.close()
       self.submit_file(k1,k)
    def power_on(self, cr, uid, context=None):
        for model in self.pool.models.values():
            if model.is_transient():
                model._transient_vacuum(cr, uid, force=True)
        return True


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
