# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, osv
from openerp.tools.translate import _
import pdb
from datetime import datetime
class product_batch_movement(osv.osv_memory):
    _name = 'product.batch_movement'
    def _sel_selection(self,cr,uid,context=None):
        current_user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        if current_user.login=='admin':
            return [ ('7','Inbound'),('8','Inbound Rejected'),('20','Outbound'),('17','merchandising'),
                      ('11','content'),('26','Dimension'),('14','photoshoot'),('4','QC'),
                      ('46','warehouse'),('23','HardTagging') ]
        elif current_user.name=='content':
            return [('11','content')]
        elif current_user.name=='inbound':
            return [('7','Inbound'),('8','Inbound Rejected')]
        elif current_user.name=='merchandising':
            return [('17','merchandising')]
        elif current_user.name=='Outbound':
            return [('20','outbound')]
        elif current_user.name=='Dimension':
            return [('26','Dimension measurement')]
        elif current_user.name=='HardTagging':
            return [('23','HardTagging')]
        elif current_user.name=='photoshoot':
            return [('14','photoshoot')]
        elif current_user.name=='warehouse':
            return [('46','warehouse')]
        elif current_user.name=='qc':
            return [('4','qc')]
    def _Destination(self,cr,uid,context=None):
        
        current_user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        if current_user.login=='admin':
            return [ ('7','Inbound'),('8','Inbound Rejected'),('20','Outbound'),('17','merchandising'),
                      ('11','content'),('26','Dimension'),('14','photoshoot'),('4','QC'),
                      ('46','warehouse'),('23','HardTagging') ]
        elif current_user.name=='content':
            return [('7','Inbound'),('8','Inbound Rejected')]
        elif current_user.name=='inbound':
            return [ ('20','Outbound'),('17','merchandising'),
                      ('11','content'),('27','Dimension'),('14','photoshoot'),('4','QC'),
                      ('46','warehouse'),('23','HardTagging'),('8','Inbound Rejected')]
        elif current_user.name=='merchandising':
            return [('7','Inbound')]
        elif current_user.name=='Outbound':
            return [('30','warehouse')]
        elif current_user.name=='Dimension':
            return [('7','Inbound'),('8','Inbound Rejected')]
        elif current_user.name=='HardTagging':
            return [('7','Inbound'),('8','Inbound Rejected')]
        elif current_user.name=='photoshoot':
            return [('7','Inbound'),('8','Inbound Rejected')]
        elif current_user.name=='warehouse':
            return [('7','Inbound'),('20','outbound')]
        elif current_user.name=='qc':
            return [('7','Inbound'),('8','Inbound Rejected')]



    _columns = {

        'location_origin':fields.selection(_sel_selection,'Location Origin'),
        'location_dest': fields.selection(_Destination,'Destination' ),
    
    }
    def move_product(self, cr, uid, ids, context=None):
        """
        To get the date and print the report
        @return : return report
        """
    
        product_product_obj = self.pool.get('product.product')
        if context is None:
            context = {}
        datas = {'ids': context.get('active_ids', [])}
        temp_loc_dest=self.browse(cr, uid, context['res_id'], context=context)
        context={'location':int(temp_loc_dest.location_origin)}
        obj=self.pool.get('stock.move')
        #obj.valid_content(cr,uid,datas,context=None)
        for prod in product_product_obj.browse(cr, uid, datas['ids'], context=context):
            if prod.qty_available > 0:
                c1={}
                c1={'lang': 'en_US', 'tz': False, 'uid': uid, 'invoice_state': None, 'procure_method': 'make_to_stock', 'search_default_future': True}
                vals={}
                vals={'state':'confirmed','product_uom':1,'company_id':1,'location_dest_id':int(temp_loc_dest.location_dest),'location_id':int(temp_loc_dest.location_origin),'product_id':prod.id,'name':prod.name,'date_expected':datetime.now().strftime('%Y-%m-%d %H:%M:%S'),'date':datetime.now().strftime('%Y-%m-%d %H:%M:%S'),'product_uom_qty':prod.qty_available}
                obj.create(cr, uid,vals, context=c1)

class product_price_list(osv.osv_memory):
    _name = 'product.price_list'
    _description = 'Price List'

    _columns = {
        'price_list': fields.many2one('product.pricelist', 'PriceList', required=True),
        'qty1': fields.integer('Quantity-1'),
        'qty2': fields.integer('Quantity-2'),
        'qty3': fields.integer('Quantity-3'),
        'qty4': fields.integer('Quantity-4'),
        'qty5': fields.integer('Quantity-5'),
    }
    _defaults = {
        'qty1': 1,
        'qty2': 5,
        'qty3': 10,
        'qty4': 0,
        'qty5': 0,
    }

    def print_report(self, cr, uid, ids, context=None):
        """
        To get the date and print the report
        @return : return report
        """
        if context is None:
            context = {}
        datas = {'ids': context.get('active_ids', [])}
        res = self.read(cr, uid, ids, ['price_list','qty1', 'qty2','qty3','qty4','qty5'], context=context)
        res = res and res[0] or {}
        res['price_list'] = res['price_list'][0]
        datas['form'] = res
        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'product.pricelist',
            'datas': datas,
       }
product_price_list()
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

