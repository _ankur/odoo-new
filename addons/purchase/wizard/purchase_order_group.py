# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import time
import pdb
from datetime import datetime
from openerp.osv import fields, osv
from openerp import netsvc
from openerp import pooler
from openerp.osv.orm import browse_record, browse_null
from openerp.tools.translate import _
class temp_product(osv.osv):
    _name="temp.product"
    _columns={
    'product':fields.many2one('product.product','Product'),
    'qty':fields.integer('Quantity'),
    'qty_accept':fields.integer('Accepted Quantity'),
    'name':fields.char('po name'),
    'po_id':fields.integer('purchase order'),
    'flag':fields.boolean('processed')
    }
    _defults={
    'po_id':1,
    'flag':False
    }
    def move_orders(self, cr, uid, ids,qty_accept,context=None):
        print True
        
temp_product()

class purchase_order_group(osv.osv_memory):
    _name = "purchase.order.group"
    _description = "Purchase Order Merge"
    def _sel_selection(self,cr,uid,context=None):
        user_group=self.pool.get('res.groups')
        ll=self.pool.get('res.users').read(cr,uid,[uid],['groups_id'])
        temp=ll[0]['groups_id']
        inbound_id=user_group.search(cr,uid,[('name','=','Inbound User')])
        other_id=user_group.search(cr,uid,[('name','in',('Measurement User','Photoshoot User','Merchandising User',
            'Hard Tagging User','Quality Control User','Content User'))])
        if inbound_id[0] in temp:
            return [('8','Inbound Rejected'),('17','merchandising'),
                      ('11','content'),('26','Dimension'),('14','photoshoot'),('4','QC'),
                      ('46','warehouse'),('23','HardTagging')]
        elif len( list(set(temp).intersection(other_id)))!=0:
            return [('8','Inbound Rejected'),('7','Inbound')]
        else:
            return []
    _columns={
        'dest':fields.selection(_sel_selection,'Location to move',required=True),
        'temp':fields.one2many('temp.product','po_id')
    
    }

    def fields_view_get(self, cr, uid, view_id=None, view_type='form',
                        context=None, toolbar=False, submenu=False):
        """
         Changes the view dynamically
         @param self: The object pointer.
         @param cr: A database cursor
         @param uid: ID of the user currently logged in
         @param context: A standard dictionary
         @return: New arch of view.
        """
        if context is None:
            context={}
        res = super(purchase_order_group, self).fields_view_get(cr, uid, view_id=view_id, view_type=view_type, context=context, toolbar=toolbar,submenu=False)
        if context.get('active_model','') == 'purchase.order' and len(context['active_ids']) > 1:
            raise osv.except_osv(_('Warning!'),
            _('You can move one PO at a time'))
        return res
    def on_change(self, cr, uid, ids,dest,context=None):
        po=self.pool.get('purchase.order').browse(cr,uid,context['active_ids'])
        stock_picking=self.pool.get('stock.picking')
        stock_picking_id=stock_picking.search(cr,uid,[('origin','=',po.name),('state','in',('done','confirmed'))])
        stock_picking_obj=stock_picking.browse(cr,uid,stock_picking_id)
        def move_ids(x):
            return x.move_lines
        temp=map(move_ids,stock_picking_obj)
        print temp
        temp=reduce(lambda x,y:x+y,temp)
        available_product={}
        for stock_moves in temp:
            if stock_moves.state in ['confirmed','done','assigned'] and stock_moves.location_id.id==context['location_id']:
                if stock_moves.product_id.id in available_product:
                    available_product[stock_moves.product_id.id]-=stock_moves.product_qty
                else:
                    available_product.update({stock_moves.product_id.id: -stock_moves.product_qty})
            elif stock_moves.state in ['done'] and stock_moves.location_dest_id.id==context['location_id']:
                if stock_moves.product_id.id in available_product:
                    available_product[stock_moves.product_id.id]+=stock_moves.product_qty
                else:
                    available_product.update({stock_moves.product_id.id:stock_moves.product_qty})

        temp_obj=self.pool.get('temp.product')
        temp_po_array=[]
        temp_obj1=[]
        for item in available_product:
            vals={'product':item,'name':po.name,'qty':available_product[item],'create_uid':uid}
            temp_po_array1=temp_obj.search(cr,uid,[('product','=',item),('name','=',po.name),('create_uid','=',uid),('flag','=',False)])
            if len(temp_po_array1)==0:
                vals.update({'qty_accept':available_product[item]})
                temp_po_array.append(temp_obj.create(cr,uid,vals))
            else:
                temp_obj.write(cr,uid,temp_po_array1[0],{'qty_accept':available_product[item],'qty':available_product[item]})
                temp_po_array.append(temp_po_array1[0])
        vals = {'temp': temp_obj.browse(cr,uid,temp_po_array)}  
        return {'value': vals}
    def move_orders(self, cr, uid, ids, context=None):
        purchase_order = self.pool.get('purchase.order')
        po_obj=purchase_order.browse(cr,uid,context['active_ids'])
        purchase_order_group = self.pool.get('purchase.order.group')
        temp_dest=purchase_order_group.read(cr, uid, context['res_id'],['temp','dest'] ,context=context)
        stock_move=self.pool.get('stock.move')
        temp_product=self.pool.get('temp.product')
        purchase_line_item_move=temp_product.browse(cr,uid,temp_dest['temp'])
        stock_picking=self.pool.get('stock.picking')
        stock_picking_id=stock_picking.search(cr,uid,[('origin','=',po_obj.name),('state','in',('done','confirmed'))])
        check=temp_product.search(cr,uid,[('name','=',po_obj.name),('create_uid','=',uid),('flag','=',False)])
        purchase_line_item_move=temp_product.browse(cr,uid,check)
        for product_order in purchase_line_item_move:
            c1={}
            c1={'lang': 'en_US', 'tz': False, 'uid': uid, 'invoice_state': None, 'procure_method': 'make_to_stock', 'search_default_future': True}
            vals={}
            vals={'state':'confirmed','product_uom':1,'company_id':1,
            'location_dest_id':int(temp_dest['dest']),'location_id':context['location_id'],
            'product_id':product_order.product.id,'name':product_order.product.name,'origin':po_obj.name,'picking_id':stock_picking_id[0],
            'date_expected':datetime.now().strftime('%Y-%m-%d %H:%M:%S'),'date':datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
            'product_uom_qty':product_order.qty_accept}
            stock_move.create(cr, uid,vals, context=c1)
        temp_product.write(cr,uid,check,{'flag':True})
    def merge_orders(self, cr, uid, ids, context=None):
        """
             To merge similar type of purchase orders.

             @param self: The object pointer.
             @param cr: A database cursor
             @param uid: ID of the user currently logged in
             @param ids: the ID or list of IDs
             @param context: A standard dictionary

             @return: purchase order view

        """
        order_obj = self.pool.get('purchase.order')
        proc_obj = self.pool.get('procurement.order')
        mod_obj =self.pool.get('ir.model.data')
        if context is None:
            context = {}
        result = mod_obj._get_id(cr, uid, 'purchase', 'view_purchase_order_filter')
        id = mod_obj.read(cr, uid, result, ['res_id'])

        allorders = order_obj.do_merge(cr, uid, context.get('active_ids',[]), context)
        for new_order in allorders:
            proc_ids = proc_obj.search(cr, uid, [('purchase_id', 'in', allorders[new_order])], context=context)
            for proc in proc_obj.browse(cr, uid, proc_ids, context=context):
                if proc.purchase_id:
                    proc_obj.write(cr, uid, [proc.id], {'purchase_id': new_order}, context)

        return {
            'domain': "[('id','in', [" + ','.join(map(str, allorders.keys())) + "])]",
            'name': _('Purchase Orders'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'purchase.order',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'search_view_id': id['res_id']
        }

purchase_order_group()

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
