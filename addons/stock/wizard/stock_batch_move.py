# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-TODAY OpenERP SA (<http://openerp.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, osv
from openerp.tools.misc import DEFAULT_SERVER_DATETIME_FORMAT
import time
from datetime import datetime, timedelta
import pdb
from openerp.tools.translate import _
class order_management(osv.osv):
    _name='order.management'
    _columns={
        'product_id':fields.many2one('product.product','product_id'),
        'name':fields.text('purchase order number'),
        'qty':fields.integer('qty'),
        'qty_reject':fields.integer('qty_reject'),
        'qty_total':fields.integer('total'),

    }

class stock_batch_move_line(osv.osv_memory):
    _name="stock.batch.move.line"
    _columns={
        'product_id':fields.many2one('product.product','product',required=True),
        'qty':fields.integer('qty',required=True),
        'batch_id':fields.many2one('stock.batch.move')
    }

class stock_batch_move(osv.osv_memory):
    _name = "stock.batch.move"
    _description = "Partial Move Processing Wizard"
    _columns = {
        'move_ids':fields.one2many('stock.batch.move.line','batch_id')
    }
    def do_partial(self, cr, uid, ids, context=None):
        order_obj=self.pool.get('order.management')
        stock_batch_move_obj = self.browse(cr,uid,context['res_id'])
        stock_move_obj=self.pool.get('stock.move')
        if context is None:
            context = {}
        datas = {'ids': context.get('active_ids', [])}
        c2={}
        c2={'status':'done'}
        date_1 = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        JJ=datetime.strptime(date_1, "%Y-%m-%d %H:%M:%S")
        kk=(JJ+timedelta(days=3)).strftime('%Y-%m-%d %H:%M:%S')
        check=self.pool.get('stock.move').search(cr,uid,[('id','in',context['active_ids']),('state','in',['done'])])
        if len(check)==0:
            raise osv.except_osv(('No object to move'), ('status of product may be not confirm  first Confirm then do movement' ) )

        temp_loc_dest=self.pool.get('stock.move').browse(cr, uid, check, context=c2)
        def sqr(x):
            return x.product_id.product_tmpl_id.id
        temp_obj_id=map(sqr,temp_loc_dest)
        stock_move_obj.valid_content(cr,uid,temp_obj_id,context=None)

        if len(stock_batch_move_obj.move_ids)==0:
            for prod in temp_loc_dest:
                if prod.product_qty > 0:
                    c1={}
                    c1={'lang': 'en_US', 'tz': False, 'uid': uid, 'invoice_state': None, 'procure_method': 'make_to_stock', 'search_default_future': True}
                    vals={}
                    vals={'state':'confirmed','product_uom':1,'company_id':1,'location_id':int(prod.location_dest_id.id),
                    'picking_id':prod.picking_id.id,'location_dest_id':int(prod.location_id.id),'product_id':prod.product_id.id,
                    'name':prod.product_id.name,'date_expected':kk,'date':datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
                    'product_uom_qty':prod.product_qty}
                    stock_move_obj.create(cr, uid,vals, context=c1)
        else:
            for prod in temp_loc_dest:
                flag=True
                for obj_line in stock_batch_move_obj.move_ids:
                    if  obj_line.qty > prod.product_qty:
                        raise osv.except_osv(('error'), ('Rejected quantity is greater than avaiable quantity'))
                    if prod.product_id.name==obj_line.product_id.name:
                        if prod.product_qty >= obj_line.qty:
                            c1={}
                            c1={'lang': 'en_US', 'tz': False, 'uid': uid, 'invoice_state': None, 'procure_method': 'make_to_stock', 'search_default_future': True}
                            vals={}
                            vals={'state':'confirmed','product_uom':1,
                            'company_id':1,'location_id':int(prod.location_dest_id.id),
                            'picking_id':prod.picking_id.id,'location_dest_id':int(prod.location_id.id),'product_id':prod.product_id.id,'name':prod.product_id.name,
                            'date_expected':kk,
                            'date':datetime.now().strftime('%Y-%m-%d %H:%M:%S'),'product_uom_qty':prod.product_qty-obj_line.qty}
                            stock_move_obj.create(cr, uid,vals, context=c1)
                            re_id=order_obj.search(cr,uid,[('name','=',prod.picking_id.origin),('product_id','=',prod.product_id.id)])
                            re_id_obj=order_obj.browse(cr,uid,re_id)[0]
                            vals.update({'location_dest_id':8})
                            vals.update({'product_uom_qty':obj_line.qty})
                            stock_move_obj.create(cr, uid,vals, context=c1)
                            cr.execute('update order_management SET qty=%s,qty_reject=%s where id=%s',(re_id_obj.qty-obj_line.qty,re_id_obj.qty_reject+obj_line.qty,re_id[0]) )
                            flag=False
                            break
                if flag==True:
                    c1={}
                    c1={'lang': 'en_US', 'tz': False, 'uid': uid, 'invoice_state': None, 'procure_method': 'make_to_stock', 'search_default_future': True}
                    vals={}
                    vals={'state':'confirmed','product_uom':1,
                    'picking_id':prod.picking_id.id,'company_id':1,'location_id':int(prod.location_dest_id.id),
                    'location_dest_id':int(prod.location_id.id),'product_id':prod.product_id.id,'name':prod.product_id.name,
                    'date_expected':kk,
                    'date':datetime.now().strftime('%Y-%m-%d %H:%M:%S'),'product_uom_qty':prod.product_qty}
                    stock_move_obj.create(cr, uid,vals, context=c1)

