import openerp.http as http
from openerp.http import request
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
import pdb
import json
import psycopg2
import xml.etree.ElementTree as ET
#from voylla_modules.outbound.outbound import order_order
import voylla_modules.outbound.outbound
#from openerp.modules.registry import RegistryManager
from functools import wraps
from werkzeug.wrappers import BaseResponse as Response
import difflib
import boto.sqs
#from config import order_queue, API_KEY
from config import API_KEY
import urllib, urllib2
import xml.etree.ElementTree as ET
from datetime import datetime
from threading import Thread, Lock
import time
import logging
from celery.contrib import rdb
mutex = Lock()
_logger = logging.getLogger(__name__)
from functools import wraps


def retry(ExceptionToCheck, tries=4, delay=3, backoff=2, logger=None):
	"""Retry calling the decorated function using an exponential backoff.

	http://www.saltycrane.com/blog/2009/11/trying-out-retry-decorator-python/
	original from: http://wiki.python.org/moin/PythonDecoratorLibrary#Retry

	:param ExceptionToCheck: the exception to check. may be a tuple of
		exceptions to check
	:type ExceptionToCheck: Exception or tuple
	:param tries: number of times to try (not retry) before giving up
	:type tries: int
	:param delay: initial delay between retries in seconds
	:type delay: int
	:param backoff: backoff multiplier e.g. value of 2 will double the delay
		each retry
	:type backoff: int
	:param logger: logger to use. If None, print
	:type logger: logging.Logger instance
	"""
	def deco_retry(f):

		@wraps(f)
		def f_retry(*args, **kwargs):
			mtries, mdelay = tries, delay
			
			while mtries > 1:
				try:
					return f(*args, **kwargs)
				except ExceptionToCheck, e:
					msg = "%s, Retrying in %d seconds... product import retry" % (str(e), mdelay)
					if logger:
						logger.warning(msg)
					else:
						print msg
					time.sleep(mdelay)
					mtries -= 1
					mdelay *= backoff
			kwargs.update({'mtries':mtries})
			return f(*args, **kwargs)

		return f_retry  # true decorator

	return deco_retry


def authenticate(f):
	@wraps(f)
	def wrapper(*args, **kwargs):
		headers = request.httprequest.headers
		try:
			token = headers["API_KEY"]
		except KeyError:
			return Response(json.dumps("API_KEY not provided"), 401, {'WWW-Authenticate': 'Token realm="Application!"'}, content_type="application/json")
		if not valid_token(token):
			return Response(json.dumps('API_KEY Invalid!'), 401, {'WWW-Authenticate': 'Token realm="Application!"'}, content_type="application/json")
		return f(*args, **kwargs)
	return wrapper


def valid_token(token):
	if token != API_KEY:
		return False
	else:
		return True


def validate_node_xml(node, required_fields, node_name):
	missing_fields = list(set(required_fields) - set([element.tag for element in node.iter()]) )
	if not missing_fields:
		return True
	else:
		raise KeyError("Required field(s) " + ", ".join(missing_fields) + " missing from %s" %(node_name))


def validate_node_json(node, required_fields, node_name):
	missing_fields = list(set(required_fields) - set(node.keys()) )
	if not missing_fields:
		return True
	else:
		raise KeyError("Required field(s) " + ", ".join(missing_fields) + " missing from %s" %(node_name))


def validate_order_creation_request_xml(node, required_fields, **kwargs):
	validate_node_xml(node, required_fields, "Order")
	address = node.find("Address")
	validate_node_xml(address, kwargs["Address"], "Address")
	ship_address = node.find("Ship_Address")
	if ship_address is not None:
		validate_node_xml(ship_address, kwargs["Address"], "Ship_Address")
	else:
		ship_address = None
	payments = node.find("Payments")
	for payment in payments:
		validate_node_xml(payment, kwargs["Payments"], "Payments")
	adjustments = node.find("Adjustments")
	for adjustment in adjustments:
		validate_node_xml(adjustment, kwargs["Adjustments"], "Adjustment")
	line_items = node.find("Line_Items")
	if line_items.find("./") is None:
		raise KeyError("Line_Items cannot be empty")
	for line_item in line_items:
		validate_node_xml(line_item, kwargs["Line_Items"], "Line_Item")
	return address, payments, adjustments, line_items, ship_address


def validate_values(element, key, valid_values):
	value = element[key]
	if value not in valid_values:
		raise KeyError("Invalid value %s for %s" %(value, key))


def validate_order_creation_request_json(node, required_fields, **kwargs):
	validate_node_json(node, required_fields, "Order")
	address = node["Address"]
	validate_node_json(address, kwargs["Address"], "Address")
	if "Ship_Address" in node:
		ship_address = node["Ship_Address"]
		validate_node_json(ship_address, kwargs["Address"], "Ship_Address")
	else:
		ship_address = None
	payments = node["Payments"]
	if not isinstance(payments, list):
		raise KeyError("A list of Payments expected")
	for payment in payments:
		validate_node_json(payment, kwargs["Payments"], "Payments")
		validate_values(payment, "Payment_Method", ["Gateway","InternationalPayment", "Gateway::PayuIn", "CashOnDelivery", "ThirdPartyPayment", "PaymentMethod::Check", "PaypalPayment", "Gateway::Bogus", "GiftVoucherPayment", "Cash", "BankTransfer","PaytmWallet", "free_rep"])
	adjustments = node["Adjustments"]
	if not isinstance(adjustments, list):
		raise KeyError("A list of Adjustments expected")
	for adjustment in adjustments:
		validate_node_json(adjustment, kwargs["Adjustments"], "Adjustments")
	line_items = node["Line_Items"]
	if not isinstance(line_items, list):
		raise KeyError("A list of Line_Items expected")
	if not line_items:
		raise KeyError("Line_Items cannot be empty")
	for line_item in line_items:
		validate_node_json(line_item, kwargs["Line_Items"], "Line_Item")
	return address, payments, adjustments, line_items, ship_address



class SpreeApi(http.Controller):

	REQUIRED_FIELDS_ORDER_CREATION = ["Order_Completed_Date","Order_Number","Line_Items","Address","Payments","Adjustments","Currency","Shipping_Type","Status","Notes","Total"]
	LINE_ITEM_FIELDS = ["Quantity","Sale_Price","MRP"]
	ADDRESS_FIELDS = ["Street","City","Zip_Code","State","Country","Phone","Email","Customer_Name"]
	ADJUSTMENT_FIELDS = ["amount", "notes", "label"]
	PAYMENT_FIELDS = ["Payment_Method","Payment_ID", "Amount"]
	OPTIONAL_FIELDS_ORDER_CREATION = ["Giftwrap","Giftwrap_Message"]
	STATES_POSSIBLE = ["Chhattisgarh","Goa","Sikkim","Meghalaya","Tamil Nadu","Jammu and Kashmir","Madhya Pradesh","Rajasthan","Uttar Pradesh","Uttrakhand","Andhra Pradesh","Dadra and Nagar Haveli","Army Post Office","Nagaland","Jharkhand","Lakshadweep","Maharashtra","Mizoram","Punjab","West Bengal","Tripura","Himachal Pradesh","Arunachal Pradesh","Karnataka","Gujarat","Manipur","Odisha","Haryana","Assam","Chandigarh","Daman and Diu","Andaman and Nicobar","Bihar","Kerala","Pondicherry","Delhi"]
	ADDRESS_FIELDS_MAP = {"email" : 'Email', "name" : 'Customer_Name', "street" : 'Street', "city" : 'City', "zip" : 'Zip_Code', "state_name" : 'State', "country" : 'Country', "phone" : 'Phone'}

	@http.route('/spree_api/orders', methods=["POST","GET"], auth="public")
	@authenticate
	#@http.route('/spree_api/some_html', type="http", methods=["POST","GET"], auth="public")
	def some_html(self):
		cr = request.cr
		uid = request.uid
		if request.httprequest.method=="GET":
			my_user_record = request.registry.get("res.users").browse(cr, uid, uid)
			return my_user_record.name
		elif request.httprequest.method=="POST":
			headers = request.httprequest.headers
			data = request.httprequest.data
			response = {}
			content_type = str(headers["Content-Type"])
			if content_type == "json":
				try:
					body = json.loads(data)
				except:
					return Response(json.dumps("Not a valid JSON"), status=400, content_type="application/json")

				for order_id, order_details in body.iteritems():
					try:
						address, payments, adjustments, line_items, ship_address = validate_order_creation_request_json(order_details, self.REQUIRED_FIELDS_ORDER_CREATION,
							**{"Payments":self.PAYMENT_FIELDS, "Address":self.ADDRESS_FIELDS, "Line_Items":self.LINE_ITEM_FIELDS, "Adjustments":self.ADJUSTMENT_FIELDS})
					except KeyError as e:
							return Response("Bad request, {}".format(e.args[0]), status=400)

					order_registry = request.registry.get('sale.order')
					failure_notification_registry = request.registry.get('failure.notification')

					try:
						order_id = order_details['Order_Number']

						check_order = order_registry.search(cr, 1, [("order_id","=",order_id)])
						res_user_registry = request.registry.get("res.users")
						create_uid = res_user_registry.search(cr,1,[('login','=','voc_bot')])
						voc_uid = res_user_registry.browse(cr,1,create_uid) if len(create_uid) > 0 else res_user_registry.browse(cr,1,[1])
						if check_order:
							order_obj = order_registry.browse(cr, 1 , check_order)
							status = order_obj.status
							order_write_uid = order_obj.write_uid
							if status != 'confirmed' and order_write_uid == voc_uid:
								status = order_details["Status"]
								update = self.update_order(cr, request, check_order, status)
								response[order_id] = update
							continue
						order_completed_date = order_details['Order_Completed_Date']
						try:
							order_completed_date = order_completed_date.split("+")[0].strip()
							order_completed_date = datetime.strptime(order_completed_date, "%Y-%m-%d %H:%M:%S")
						except:
							order_completed_date = datetime.now()

						currency = order_details['Currency']
						shipping_type = order_details['Shipping_Type']
						status = order_details["Status"]
						notes = order_details["Notes"]
						total_payment = order_details['Total']
						try:
							assigned_courier = order_details["assigned_courier"]
						except:
							assigned_courier = None
						try:
							awb = order_details["AWB"]
						except:
							awb = None
						try:
							courier_name = order_details["courier_name"]
						except:
							courier_name = None
						try:
							order_state = order_details["Order_State"]
						except:
							order_state = None
						try:
							product_giftwrap = order_details['Giftwrap']
						except:
							product_giftwrap = False
						try:
							giftwrap_message = order_details ['Giftwrap_Message']
						except:
							giftwrap_message = ""
						try:
							source = order_details["Source"]
						except:
							source = None
						order_import = order_details.get("Order_Import")

						address_hash = {}
						address_field_map = self.ADDRESS_FIELDS_MAP
						for k,v in address_field_map.iteritems():
							address_hash[k] = address[v]

						ship_address_hash = None
						if ship_address is not None:
							ship_address_hash = {}
							for k,v in address_field_map.iteritems():
								ship_address_hash[k] = ship_address[v]

						response[order_id] = self.create_order(cr, request, order_id, order_completed_date, currency, shipping_type, status, notes, total_payment, assigned_courier, awb, courier_name, order_state, 
															product_giftwrap, giftwrap_message, source, address_hash, ship_address_hash, line_items, adjustments, payments, order_import)

					except:
						order_number = order_details['Order_Number']
						failure = failure_notification_registry.search(cr, 1, [("order_id", "=", "%s" %(order_number)), ("type", "=", "order_creation")])
						if not failure:
							vals = {"name": "Order Creation Failure %s" %(order_number), "type":"order_creation", "description":"Failed to create order due to uncaught exception", "order_id":order_number }
							failure_notification_registry.create(cr, 1, vals)
						response[order_id] = "failure"


				return Response(json.dumps(response), status=200, content_type="application/json")


			elif content_type == "xml":
				try:
					root = ET.fromstring(data)
				except:
					return Response(json.dumps("Not a valid JSON"), status=400, content_type="application/json")


				for child in root:
					try:
						address, payments, adjustments, line_items, ship_address = validate_order_creation_request_xml(child, self.REQUIRED_FIELDS_ORDER_CREATION,
						**{"Payments":self.PAYMENT_FIELDS, "Address":self.ADDRESS_FIELDS, "Line_Items":self.LINE_ITEM_FIELDS, "Adjustments":self.ADJUSTMENT_FIELDS})
					except KeyError as e:
						return Response("Bad request, {}".format(e.args[0]), status=400)

					order_registry = request.registry.get('sale.order')
					failure_notification_registry = request.registry.get('failure.notification')

					try:
						order_id = child.find('Order_Number').text
						check_order = order_registry.search(cr, 1, [("order_id","=",order_id)])
						if check_order:
							response[order_id] = "duplicate order"
							continue

						order_completed_date = child.find('Order_Completed_Date').text
						try:
							order_completed_date = order_completed_date.split("+")[0].strip()
							order_completed_date = datetime.strptime(order_completed_date, "%Y-%m-%d %H:%M:%S")
						except:
							order_completed_date = datetime.now()

						currency = child.find("Currency").text
						shipping_type = child.find('Shipping_Type').text
						status = child.find("Status").text.lower()
						notes = child.find("Notes").text
						total_payment =child.find('Total').text
						assigned_courier = child.find("Assigned_Courier")
						if assigned_courier is not None:
							assigned_courier = assigned_courier.text
						awb = child.find("AWB")
						if awb is not None:
							awb = awb.text
						order_state = child.find("Order_State")
						if order_state is not None:
							order_state = order_state.text
						product_giftwrap = child.find('Giftwrap')
						if product_giftwrap is not None:
							product_giftwrap = product_giftwrap.text
						else:
							product_giftwrap = False
						giftwrap_message = child.find('Giftwrap_Message')
						if giftwrap_message is not None:
							giftwrap_message = giftwrap_message.text
						else:
							giftwrap_message = ""
						source = child.find("Source")
						if source is not None:
							source = source.text
						else:
							source = None
						order_import = child.find("Order_Import")
						if order_import is not None:
							order_import = order_import.text
						else:
							order_import = None

						address_hash = {}
						address_hash["email"] =address.find('Email').text
						address_hash["name"] = address.find('Customer_Name').text
						address_hash["street"] =address.find('Street').text
						address_hash["city"] = address.find('City').text
						address_hash["zip"] = address.find('Zip_Code').text
						address_hash["state_name"] = address.find('State').text
						address_hash["country"] = address.find('Country').text
						address_hash["phone"] = address.find('Phone').text

						ship_address_hash = None
						if ship_address is not None:
							ship_address_hash = {}
							ship_address_hash["email"] = ship_address.find('User_Email').text
							ship_address_hash["name"] = ship_address.find('Customer_Name').text
							ship_address_hash["street"] = ship_address.find('Street').text
							ship_address_hash["city"] = ship_address.find('City').text
							ship_address_hash["city"] = ship_address.find('Zip_Code').text
							ship_address_hash["state_name"] = ship_address.find('State').text
							ship_address_hash["country"] = ship_address.find('Country').text
							ship_address_hash["phone"] = ship_address.find('Phone').text

						line_items_list = []
						for line_item in line_items:
							line_item_hash = {}
							line_item_hash["EAN"] = line_item.find('EAN')
							line_item_hash["SKU"] = line_item.find("SKU")
							line_item_hash["Size"] = line_item.find("Size")
							line_item_hash["Quantity"] = line_item.find('Quantity').text
							line_item_hash["Sale_Price"] = line_item.find('Sale_Price').text
							line_item_hash["MRP"] = line_item.find('MRP').text
							line_items_list.append(line_item_hash)

						adjustments_list = []
						for adjustment in adjustments:
							adjustment_hash = {}
							adjustment_hash["label"] = adjustment.find("label").text
							adjustment_hash["notes"] = adjustment.find("notes").text
							adjustment_hash["amount"] = adjustment.find("amount").text
							adjustments_list.append(adjustment_hash)
							

						payments_list = []
						for payment in payments:
							payment_hash = {}
							payment_hash["Payment_Method"] = payment.find("Payment_Method").text
							payment_hash["Payment_ID"] = payment.find("Payment_ID").text
							payment_hash["Amount"] = payment.find("Amount").text
							payments_list.append(payment_hash)

						response[order_id] = self.create_order(cr, request, order_id, order_completed_date, currency, shipping_type, status, notes, total_payment, assigned_courier, courier_name, awb,
							order_state, product_giftwrap, giftwrap_message, source, address_hash, ship_address_hash, line_items_list, adjustments_list, payments_list, order_import)

					except:
						order_number = order_details['Order_Number']
						failure = failure_notification_registry.search(cr, 1, [("order_id", "=", "%s" %(order_number)), ("type", "=", "order_creation")])
						if not failure:
							vals = {"name": "Order Creation Failure %s" %(order_number), "type":"order_creation", "description":"Failed to create order due to uncaught exception", "order_id":order_number }
							failure_notification_registry.create(cr, 1, vals)
						response[order_id] = "failure"


				return Response(json.dumps(response), status=200, content_type="application/json")
						

	def create_order(self, cr, request, order_id, order_completed_date, currency, shipping_type, status, notes, total_payment, assigned_courier, awb, courier_name, order_state, 
		product_giftwrap, giftwrap_message, source, address_hash, ship_address_hash, line_items, adjustments, payments, order_import):

		order_registry = request.registry.get('sale.order')
		product_registry = request.registry.get("product.product")
		line_item_registry = request.registry.get('sale.order.line')
		product_pricelist_registry = request.registry.get("product.pricelist")
		product_pricelist_version_registry = request.registry.get("product.pricelist.version")
		res_partner_registry = request.registry.get("res.partner")
		state_registry = request.registry.get("res.country.state")
		adjustment_registry = request.registry.get("sale.order.adjustment")
		payment_registry = request.registry.get("sale.order.payment")
		country_registry = request.registry.get("res.country")
		currency_registry = request.registry.get("res.currency")
		tax_registry = request.registry.get("account.tax")
		fiscal_position_registry = request.registry.get("account.fiscal.position")
		source_registry = request.registry.get("crm.tracking.source")
		failure_notification_registry = request.registry.get('failure.notification')
		res_user_registry = request.registry.get("res.users")

		create_uid = res_user_registry.search(cr,1,[('login','=','voc_bot')])
		uid = create_uid[0] if len(create_uid) > 0 else 1


		if shipping_type is None:
			shipping_type = "standard"


		state_name = address_hash["state_name"]
		country_name = address_hash["country"]
		state_ids = state_registry.search(cr, uid, [("name","=",state_name)])
		if state_ids:
			state_id = state_ids[0]
			address_hash["state_id"] = state_id

		country_ids = country_registry.search(cr, uid, [("name","=",country_name)])
		if country_ids:
			country_id = country_ids[0]
			address_hash["country_id"] = country_id
		address_hash.pop("state_name")
		address_hash.pop("country")
		address_hash["active"] = True

		# try:
		#     fiscal_position = fiscal_position_registry.search(cr, 1, [("state","=",state_id), ("country_id","=",country_id)])
		#     if not fiscal_position:
		#         fiscal_position = fiscal_position_registry.search(cr, 1, [("country_id","=",country_id)])
		#     if fiscal_position:
		#         fiscal_position_id = fiscal_position[0]
		#     else:
		#         fiscal_position_id = None
		# except NameError:
		#     fiscal_position_id = None

		email = address_hash["email"]
		phone = address_hash["phone"]
		customer_id = None
		if email != "NA" and phone != "NA":
			customer_id = res_partner_registry.search(cr, uid, ["|", ("email", "=", email), ("phone", "=", phone) ])
		elif email != "NA":
			customer_id = res_partner_registry.search(cr, uid, [("email", "=", email)])
			address_hash.pop("phone")
		elif phone != "NA":
			customer_id = res_partner_registry.search(cr, uid, [("phone", "=", phone)])
			address_hash.pop("email")
		if not customer_id:
			address_hash["customer"] = True
			address_id = res_partner_registry.create(cr, uid, address_hash)
			customer_id = address_id
		else:
			customer_id = customer_id[0]
			address_hash["customer"] = False
			address_hash["parent_id"] = customer_id
			address_id = res_partner_registry.create(cr, uid, address_hash)


		if ship_address_hash is not None:
			state_name = ship_address_hash["state_name"]
			country_name = ship_address_hash["country"]
			state_ids = state_registry.search(cr, uid, [("name","=",state_name)])
			if state_ids:
				state_id = state_ids[0]
				ship_address_hash["state_id"] = state_id
			country_ids = country_registry.search(cr, uid, [("name","=",country_name)])
			if country_ids:
				country_id = country_ids[0]
				ship_address_hash["country_id"] = country_id
			ship_address_hash["customer"] = False
			ship_address_hash["active"] = True
			ship_address_hash["parent_id"] = customer_id
			ship_address_hash.pop("state_name")
			ship_address_hash.pop("country")
			ship_address_id = res_partner_registry.create(cr, uid, ship_address_hash)
		else:
			ship_address_id = address_id


		currency_id = currency_registry.search(cr, uid, [("name","=",currency)])
		if currency_id:
			currency_id = currency_id[0]
		else:
			currency_id = currency_registry.search(cr, uid, [("name","=","INR")])


		pricelist_id = product_pricelist_registry.search(cr, uid, [("type","=","sale"), ("currency_id","=",currency_id), ("sale_type","=","default")])
		if pricelist_id:
			pricelist_id = pricelist_id[0]
		else:
			pricelist_id = product_pricelist_registry.create(cr, uid, {"type":"sale", "name":"%s Sale Pricelist" %(currency), "currency_id":currency_id, "active":True, "sale_type":"default"})
			product_pricelist_version_registry.create(cr, uid, {"pricelist_id":pricelist_id, "active":True, "name":"%s Sale Pricelist Version" %(currency)})


		order_vals = {"date_order":order_completed_date, "order_id":order_id, "currency":currency, "shipping_type":shipping_type, "status":status, "note":notes, "product_giftwrap":product_giftwrap, "giftwrap_message":giftwrap_message, "pricelist_id":pricelist_id, "partner_id":customer_id, "partner_shipping_id":ship_address_id, "partner_invoice_id":address_id, "currency_id":currency_id, "is_automated":True}
		if assigned_courier is not None:
			order_vals["assigned_courier"] = assigned_courier
		if courier_name is not None:
			order_vals["courier_name"] = courier_name
		if awb is not None:
			order_vals["awb"] = awb
		if order_state is not None:
			order_vals["state"] = order_state
		if source is not None:
			source_id = source_registry.search(cr, uid, [("name","=",source)])
			if not source_id:
				source_id = source_registry.create(cr, uid, {"name":source})
			else:
				source_id = source_id[0]
			order_vals["source_id"] = source_id
		if order_import is True:
			order_vals["state"] = "manual"
		x = order_registry.create(cr, uid, order_vals)
		order_notes = order_registry.read(cr, uid, x)["note"]


		for item in line_items:
			prod_id = None
			quantity = item['Quantity']
			product_cost_sp = item['Sale_Price']
			product_cost_mrp = item['MRP']

			if item.has_key("EAN"):
				ean = item["EAN"]
				if ean is not None:
					prod_id = product_registry.search(cr, uid, [("ean13","=",ean), ("active", "=", "True")])
				else:
					prod_id = []
				if not prod_id:
					order_notes += " | EAN %s not found" %(ean)
					status = "not_confirmed"
					failure = failure_notification_registry.search(cr, uid, [("order_id", "=", order_id), ("type","=","order_line_creation")])
					if not failure:
						vals = {"name": "Failed to add product to order %s" %(order_id), "type":"order_line_creation", "description":"Could not find item %s" %(ean), "order_id":order_id }
						failure_notification_registry.create(cr, uid, vals)
					continue
				prod_id = prod_id[0]
			elif item.has_key("SKU"):
				sku = item["SKU"]
				prod_ids = product_registry.search(cr, uid, [("name_template","=",sku), ("active", "=", "True")])
				if len(prod_ids) == 0:
					order_notes += " | SKU %s not found" %(sku)
					status = "not_confirmed"
					continue
				elif len(prod_ids) > 1:
					if not item.has_key("Size"):
						order_notes += " | size not specified for sku %s" %(sku)
						status = "not_confirmed"
						continue
					else:
						size = item["Size"]
						size_var = None
						for prod_id in prod_ids:
							if product_registry.browse(cr, uid, prod_id).attribute_value_ids.name == size:
								size_var = prod_id
								break
						if size_var is None:
							order_notes += " | size %s not found for sku %s" %(size, sku)
							status = "not_confirmed"
							continue
						prod_id = size_var
				else:
					prod_id = prod_ids[0]
			elif item.has_key("Threep_Number"):
				threep_number = item["Threep_Number"]
				if threep_number is not None:
					prod_id = product_registry.search(cr, uid, [("threep_reference","=",threep_number), ("active", "=", "True")])
				else:
					prod_id = []
				if not prod_id:
					order_notes += " | Threep_Number %s not found" %(threep_number)
					status = "not_confirmed"
					failure = failure_notification_registry.search(cr, uid, [("order_id", "=", order_id), ("type","=","order_line_creation")])
					if not failure:
						vals = {"name": "Failed to add product to order %s" %(order_id), "type":"order_line_creation", "description":"Could not find item %s" %(threep_number), "order_id":order_id }
						failure_notification_registry.create(cr, uid, vals)
					continue
			else:
				order_notes += " | EAN, Threep_Number or SKU expected"
				status = "not_confirmed"

			if prod_id is not None:
				product_obj = product_registry.browse(cr, uid, prod_id)
				# discount = (float(product_cost_mrp) - float(product_cost_sp))/float(product_cost_mrp) * 100
				# vals = {"order_id":x, "product_id":prod_id, "product_uom":1, "product_uom_qty":float(quantity), "price_unit":product_cost_mrp, "name":product_obj.product_tmpl_id.product_name, "state":"confirmed", "discount":discount}
				vals = {"order_id":x, "product_id":prod_id, "product_uom":1, "product_uom_qty":float(quantity), "price_unit":product_cost_sp, "name":product_obj.product_tmpl_id.product_name, "state":"confirmed"}
				if "Cost_Price" in item:
					cost_price = item["Cost_Price"]
					vals["cost_price_unit"] = cost_price
				if "Designer_Code" in item:
					designer_code = item["Designer_Code"]
					designer_ids = res_partner_registry.search(cr, uid, [("supplier","=",True),("supplier_code","=",designer_code)])
					if designer_ids:
						vals["product_partner_id"] = designer_ids[0]
				order_registry.add_order_line(cr, uid, vals)
		order_registry.write(cr, uid, x, {"note":order_notes, "status":status})

		for adjustment in adjustments:
			label = adjustment["label"]
			notes = adjustment["notes"]
			amount = adjustment["amount"]
			adjustment_registry.create(cr, uid, {"order_id":x, "adjusment_label":label, "adjustment_notes":notes, "amount":amount})

		for payment in payments:
			method = payment["Payment_Method"]
			reference = payment["Payment_ID"]
			amount = payment["Amount"]
			existing_payments = order_registry.browse(cr, uid, x).payments
			if payments:
				payment_registry.unlink(cr, uid, [payment_obj.id for payment_obj in existing_payments])
			payment_registry.create(cr, uid, {"order_id":x, "payment_method":method, "payment_reference":reference, "amount":amount})

		response = "order created"
		return response


	def update_order(self, cr, request, check_order, status):
		order_obj = request.registry.get("sale.order")
		res_user_registry = request.registry.get("res.users")
		create_uid = res_user_registry.search(cr,1,[('login','=','voc_bot')])
		uid = create_uid[0] if len(create_uid) > 0 else 1
		order = order_obj.browse(cr, uid, check_order[0])
		if order.status == status:
			return "duplicate order"
		else:
			order.status = status
			return "order update to %s" %(status)


	@http.route('/spree_api/orders/cancel', methods=["POST","GET"], auth="public")
	@authenticate
	def cancel_order(self, **kwargs):
		# https://www.odoo.com/forum/help-1/question/how-to-delete-an-invoice-after-validation-20831
		cr = request.cr
		uid = request.uid

		if request.httprequest.method=="GET":
			order_numbers = kwargs.get("order_numbers")
			if order_numbers is None:
				return Response(json.dumps("missing 'order_numbers' in the request"), status=400, content_type="application/json")

			order_numbers = order_numbers.split(",")

			sale_order_obj = request.registry.get("sale.order")
			sale_order_ids = sale_order_obj.search(cr, 1, [("order_id", "in", order_numbers)])
			sale_orders = sale_order_obj.browse(cr, 1, sale_order_ids)
			response = {}
			for sale_order in sale_orders:
				order_state = sale_order.state
				order_number = sale_order.order_id
				if state == "done":
					response[order_number] = "processed"
				else:
					response[order_number] = "not processed"

			for order_num in list(set(order_numbers) - set(response.keys())):
				response[order_num] = "not found"
			return Response(json.dumps(response), status=200, content_type="application/json")

		elif request.httprequest.method=="POST":
			headers = request.httprequest.headers
			data = request.httprequest.data
			sale_order_obj = request.registry.get('sale.order')
			sale_order_line_obj = request.registry.get('sale.order.line')
			account_invoice_cancel_obj = request.registry.get("account.invoice.cancel")
			stock_move_obj = request.registry.get("stock.move")

			response = {}
			cannot_cancel = {}

			content_type = str(headers["Content-Type"])
			if content_type == "json":
				try:
					order_numbers = json.loads(data)
				except:
					return Response(json.dumps("Not a valid JSON"), status=400, content_type="application/json")

				if not isinstance(order_numbers, list):
					return Response(json.dumps("Expected a list of order numbers"), status=400, content_type="application/json")

				sale_order_ids = sale_order_obj.search(cr, 1, [("order_id", "in", order_numbers)])
				sale_orders = sale_order_obj.browse(cr, 1, sale_order_ids)
				cancel_invoice_ids = []
				for sale_order in sale_orders:
					if sale_order.state == 'done' or sale_order.state == 'cancel':
						cannot_cancel[sale_order.id] = sale_order.order_id
						continue
					invoice_ids = sale_order.invoice_ids
					for invoice in invoice_ids:
						if invoice.state in ("draft", "cancel", "open"):
							invoice.signal_workflow('invoice_cancel')
						else:
							cannot_cancel[sale_order.id] = sale_order.order_id

				cancel_order_ids = list(set(sale_order_ids) - set(cannot_cancel.keys()))
				sale_order_obj.write(cr, 1, cancel_order_ids, {"state": "cancel"})

				cancel_sale_orders = sale_order_obj.browse(cr, 1, cancel_order_ids)

				for order in cancel_sale_orders:
					order_lines = order.order_line


					for order_line in order_lines:
						x = order_line.id
						order_line.state = 'cancel'
						stock_move_ids = stock_move_obj.search(cr, 1, [('name','=',x),('state','=','assigned')])
						if stock_move_ids:
							stock_move_obj.action_cancel(cr, 1, stock_move_ids)

				cancelled = [o.order_id for o in cancel_sale_orders]
				not_cancelled = cannot_cancel.values()
				response["canceled"] = cancelled
				response["not_canceled"] = not_cancelled

				return Response(json.dumps(response), status=200, content_type="application/json")

			else:
				return Response(json.dumps("Data expected in JSON format"), status=400, content_type="application/json")

		else:
			return Response(json.dumps("Request Forbidden"), status=403, content_type="application/json")

	
	@http.route('/spree_api/invoices', methods=["POST","GET"], auth="public")
	@authenticate
	@retry(Exception, tries=4, delay=3)
	def create_invoices(self, **kwargs):
		mutex.acquire()
		cr = request.cr
		uid = request.uid
		model_obj = request.registry.get("warehouse.picklist")
		try:
			if request.httprequest.method=="POST":
				headers = request.httprequest.headers
				data = request.httprequest.data
				order_ids = json.loads(data)
				x = model_obj.create_invoices(cr, 1, order_ids)
				mutex.release()
				return Response(json.dumps("%s: Invoice Generated" %(order_ids)), status=200, content_type="application/json")
		except Exception as e:
			mutex.release()
			if 'mtries' not in kwargs:
				raise Exception("Fail")
			else:
				return Response(json.dumps("%s \n %s: Invoice Generation Failure" %(order_ids,e)), status=500, content_type="application/json")
		mutex.release()

	@http.route('/spree_api/quantity_update/error', methods=["POST","GET"], auth="public")
	@authenticate
	def repost_error(self, **kwargs):
		mutex.acquire()
		cr = request.cr
		uid = request.uid
		model_obj = request.registry.get("quantity.update.failure")
		try:
			if request.httprequest.method=="POST":
				headers = request.httprequest.headers
				data = request.httprequest.data
				order_ids = json.loads(data)
				for error in order_ids['error']:
					vals = {'name':error['sku'],'type':order_ids["error_type"],"channel":order_ids["channel"],"description":str(error)}
					model_obj.create(cr,1,vals)

				# x = model_obj.create_invoices(cr, 1, order_ids)
				mutex.release()
				return Response(json.dumps("%s: Error Reported" %(order_ids)), status=200, content_type="application/json")
		except:
			mutex.release()
			return Response(json.dumps("%s: Invoice Generation Failure" %(order_ids)), status=200, content_type="application/json")
		mutex.release()    

	@http.route('/spree_api/stock_move', methods=["POST","GET"], auth="public")
	@authenticate
	def create_stock_move(self, **kwargs):
		mutex.acquire()
		obj = request.registry.get('stock.move')
		try:
			request_body = json.loads(request.httprequest.data)
		except:
			mutex.release()
			return Response(json.dumps("Invalid JSON"), status=403, content_type="application/json")
		cr = request.cr
		uid = request.uid
		quantity = request_body.get("quantity")
		product_id = request_body.get("product_id")
		product_uom = request_body.get("product_uom")
		state = request_body.get("state")
		origin = request_body.get("origin")
		location_id = request_body.get("location_id")
		location_dest_id = request_body.get("location_dest_id")
		c1={'lang': 'en_US', 'tz': False, 'uid': uid, 'invoice_state': None, 'procure_method': 'make_to_stock', 'search_default_future': True}
		vals2={'state':state,'product_uom':product_uom,'origin':origin,
			'company_id':1,'location_id':location_id,'location_dest_id':location_dest_id,'product_id':product_id,
			'name':origin,'date_expected': datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT),
			'date':datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT),'product_uom_qty':quantity}
		try:
			id_done = obj.create(cr, 1, vals2, context=c1)
			# obj.action_done(cr,1,id_done)
			mutex.release()
			return Response(json.dumps(" %s Completed Stock Move" %(vals2)), status=200, content_type="application/json")
		except:
			mutex.release()
			return Response(json.dumps(" %s Failure" %(vals2)), status=200, content_type="application/json")        
		mutex.release()

	@http.route('/threep/orders', methods=["POST","GET"], auth="public")
	@authenticate
	def threep_order_creation(self):
		mutex.acquire()
		try:
			request_body = json.loads(request.httprequest.data)
		except:
			mutex.release()
			return Response(json.dumps("Invalid JSON"), status=403, content_type="application/json")
		cr = request.cr
		uid = request.uid
		order_ref = request_body.get("order_ref")
		line_items = request_body.get("line_items")
		address = request_body.get("address")
		create_date = request_body.get("create_date")
		try:
			  create_date = datetime.strptime(create_date, "%Y-%m-%dT%H:%M:%SZ")
			  create_date = fields.datetime.context_timestamp(cr, uid, create_date)
		except:
			  pass
		currency = request_body.get("currency")
		shipping_type = request_body.get("shipping_type")
		status = request_body.get("status")
		channel = request_body.get("channel")
		total_amount = request_body.get("total_amount")
		shipping_cost = request_body.get("shipping_cost")
		payment_id = request_body.get("payment_id")
		promotion_discount = request_body.get("promotion_discount")
		payment_method = request_body.get("payment_method")
		context = {}
		context['orderId'] = request_body.get("additional_note")
		context = context.get('orderId')
		threep_order_creation_obj = request.registry.get("threep.order.creation")
		order_obj = request.registry.get("sale.order")
		check_order = order_obj.search(cr, 1, [("order_id","=",order_ref)])
		if check_order:
			if channel=='Amazon':
				x = threep_order_creation_obj.amazon_update_orders(cr,1,check_order,status,order_ref,line_items,total_amount,address,channel)
				mutex.release()
				return x
			elif channel=='Flipkart':
				x = threep_order_creation_obj.flipkart_update_orders(cr,1,check_order,status)
				mutex.release()
				return x
			else:
				mutex.release()
				return True
		else:
			try:
				x = threep_order_creation_obj.create_orders(cr, 1, order_ref, line_items, address, create_date, currency, shipping_type, status, channel, total_amount, shipping_cost, None, promotion_discount,payment_method,context)
				mutex.release()
				return Response(json.dumps(" %s Order created" %(order_ref)), status=200, content_type="application/json")
			except:
				mutex.release()
				return Response(json.dumps(" %s Failure" %(order_ref)), status=200, content_type="application/json")
		mutex.release()

			# elif content_type == "xml":
			#     root = ET.fromstring(data)
			#     for child in root:
			#         try:
			#             address, payments, adjustments, line_items, bill_address = validate_order_creation_request_xml(child, self.REQUIRED_FIELDS_ORDER_CREATION,
			#             **{"Payments":self.PAYMENT_FIELDS, "Address":self.ADDRESS_FIELDS, "Line_Items":self.LINE_ITEM_FIELDS, "Adjustments":self.ADJUSTMENT_FIELDS})
			#         except KeyError as e:
			#             return Response("Bad request, {}".format(e.args[0]), status=400)

			#         order_registry = request.registry.get('sale.order')
			#         order_id = child.find('Order_Number').text
			#         check_order = order_registry.search(cr, 1, [("order_id","=",order_id)])
			#         if check_order:
			#             response += "duplicate order %s," %(order_id)
			#             continue

			#         order_completed_date = child.find('Order_Completed_Date').text
			#         try:
			#             order_completed_date = order_completed_date.split("+")[0].strip()
			#             order_completed_date = datetime.strptime(order_completed_date, "%Y-%m-%d %H:%M:%S")
			#         except:
			#             order_completed_date = datetime.now()

			#         currency = child.find("Currency").text
			#         shipping_type = child.find('Shipping_Type').text
			#         email_placed_by = child.find('Placed_By').text
			#         status = child.find("Status").text.lower()
			#         notes = child.find("Notes").text
			#         product_giftwrap = child.find('Giftwrap').text
			#         giftwrap_message = child.find('Giftwrap_Message').text
			#         total_payment =child.find('Total').text
			#         assigned_courier = child.find("Assigned_Courier")
			#         if assigned_courier is not None:
			#             assigned_courier = assigned_courier.text
			#         awb = child.find("AWB")
			#         if awb is not None:
			#             awb = awb.text
			#         order_state = child.find("Order_State")
			#         if order_state is not None:
			#             order_state = order_state.text

			#         user_email =address.find('User_Email').text
			#         user_name = address.find('Customer_Name').text
			#         user_address =address.find('Street').text
			#         user_city = address.find('City').text
			#         city_zip_code = address.find('Zip_Code').text
			#         user_state = address.find('State').text
			#         user_country = address.find('Country').text
			#         user_phone = address.find('Phone').text

			#         product_registry = request.registry.get("product.product")
			#         line_item_registry = request.registry.get('sale.order.line')
			#         product_pricelist_registry = request.registry.get("product.pricelist")
			#         product_pricelist_version_registry = request.registry.get("product.pricelist.version")
			#         res_partner_registry = request.registry.get("res.partner")
			#         state_registry = request.registry.get("res.country.state")
			#         adjustment_registry = request.registry.get("sale.order.adjustment")
			#         payment_registry = request.registry.get("sale.order.payment")
			#         country_registry = request.registry.get("res.country")
			#         currency_registry = request.registry.get("res.currency")
			#         tax_registry = request.registry.get("account.tax")

			#         valid_states = self.STATES_POSSIBLE
			#         if user_state not in valid_states:
			#             state_val = difflib.get_close_matches(user_state.lower(), valid_states)
			#             if state_val:
			#                 user_state = state_val[0]
			#         state = state_registry.search(cr, 1, [("name","=",user_state)])
			#         if not state:
			#             state_id = 1
			#         else:
			#             state_id = state[0]
			#         country = country_registry.search(cr, 1, [("code","=",user_country)])
			#         if not country:
			#             country_id = 1
			#         else:
			#             country_id = country[0]

			#         customer_id = res_partner_registry.search(cr, 1, [("email","=",email_placed_by)])
			#         if not customer_id:
			#             address_id = res_partner_registry.create(cr, 1, {"name":user_name, "customer":True, "active":True, "street":user_address, "zip":city_zip_code, "country_id":country_id, "email":user_email, "phone":user_phone, "state_id":state_id})
			#             customer_id = address_id
			#         else:
			#             customer_id = customer_id[0]
			#             address_id = res_partner_registry.create(cr, 1, {"name":user_name, "customer":False, "active":True, "street":user_address, "zip":city_zip_code, "parent_id":customer_id, "country_id":country_id, "email":user_email, "phone":user_phone, "state_id":state_id})
			#         if bill_address is not None:
			#             user_email =bill_address.find('User_Email').text
			#             user_name = bill_address.find('Customer_Name').text
			#             user_address =bill_address.find('Street').text
			#             user_city = bill_address.find('City').text
			#             city_zip_code = bill_address.find('Zip_Code').text
			#             user_state = bill_address.find('State').text
			#             user_country = bill_address.find('Country').text
			#             user_phone = bill_address.find('Phone').text
			#             valid_states = self.STATES_POSSIBLE
			#             if user_state not in valid_states:
			#                 state_val = difflib.get_close_matches(user_state.lower(), valid_states)
			#                 if state_val:
			#                     user_state = state_val[0]
			#             state = state_registry.search(cr, 1, [("name","=",user_state)])
			#             if not state:
			#                 state_id = 1
			#             else:
			#                 state_id = state[0]
			#             country = country_registry.search(cr, 1, [("code","=",user_country)])
			#             if not country:
			#                 country_id = 1
			#             else:
			#                 country_id = country[0]
			#             bill_address_id = res_partner_registry.create(cr, 1, {"name":user_name, "customer":False, "active":True, "street":user_address, "zip":city_zip_code, "parent_id":customer_id, "country_id":country_id, "email":user_email, "phone":user_phone, "state_id":state_id})
			#         else:
			#             bill_address_id = address_id

			#         currency_id = currency_registry.search(cr, 1, [("name","=",currency)])
			#         if currency_id:
			#             currency_id = currency_id[0]
			#         else:
			#             currency_id = currency_registry.search(cr, 1, [("name","=","INR")])
			#         pricelist_id = product_pricelist_registry.search(cr, 1, [("type","=","sale"),("currency_id","=",currency_id)])
			#         if pricelist_id:
			#             pricelist_id = pricelist_id[0]
			#         else:
			#             pricelist_id = product_pricelist_registry.create(cr, 1, {"type":"sale", "name":"%s Sale Pricelist" %(currency), "currency_id":currency_id, "active":True})
			#             product_pricelist_version_registry.create(cr, 1, {"pricelist_id":pricelist_id, "active":True, "name":"%s Sale Pricelist Version" %(currency)})

			#         order_vals = {"date_order":order_completed_date, "order_id":order_id, "currency":currency, "shipping_type":shipping_type, "email_placed_by":email_placed_by, "status":status, "note":notes, "product_giftwrap":product_giftwrap, "giftwrap_message":giftwrap_message, "pricelist_id":pricelist_id, "partner_id":customer_id, "partner_shipping_id":address_id, "partner_invoice_id":bill_address_id, "currency_id":currency_id}
			#         if order_state is not None:
			#             order_vals["state"] = order_state
			#         if assigned_courier is not None:
			#             order_vals["assigned_courier"] = assigned_courier
			#         if awb is not None:
			#             order_vals["awb"] = awb
			#         x = order_registry.create(cr, 1, order_vals)
			#         order_notes = order_registry.read(cr, 1, x)["note"]

			#         for line_item in line_items:
			#             prod_id = None
			#             ean = line_item.find('EAN')
			#             sku = line_item.find("SKU")
			#             size = line_item.find("Size")
			#             quantity = line_item.find('Quantity').text
			#             product_cost_sp = line_item.find('Sale_Price').text
			#             product_cost_mrp = line_item.find('MRP').text
			#             if ean is not None:
			#                 ean = ean.text
			#                 prod_id = product_registry.search(cr, 1, [("ean13","=",ean)])
			#                 if not prod_id:
			#                     order_notes += " | EAN %s not found" %(ean)
			#                     continue
			#                 prod_id = prod_id[0]
			#             elif sku is not None:
			#                 sku = sku.text
			#                 prod_ids = product_registry.search(cr, 1, [("name_template","=",sku)])
			#                 if len(prod_ids) == 0:
			#                     order_notes += " | SKU %s not found" %(sku)
			#                     continue
			#                 elif len(prod_ids) > 1:
			#                     if size is None:
			#                         order_notes += " | size not specified for sku %s" %(sku)
			#                         continue
			#                     else:
			#                         size = size.text
			#                         size_var = None
			#                         for prod_id in prod_ids:
			#                             if product_registry.browse(cr, 1, prod_id).attribute_value_ids.name == size:
			#                                 size_var = prod_id
			#                                 break
			#                         if size_var is None:
			#                             order_notes += " | size %s not found for sku %s" %(size, sku)
			#                             continue
			#                         prod_id = size_var
			#                 else:
			#                     prod_id = prod_ids[0]
			#             else:
			#                 order_notes += " | EAN or SKU expected"

			#             if prod_id is not None:
			#                 product_obj = product_registry.browse(cr, 1, prod_id)
			#                 discount = (float(product_cost_mrp) - float(product_cost_sp))/float(product_cost_mrp) * 100
			#                 vals = {"order_id":x, "product_id":prod_id, "product_uom":1, "product_uom_qty":float(quantity), "price_unit":product_cost_mrp, "name":product_obj.product_tmpl_id.product_name, "state":"confirmed","discount":discount}
			#                 cost_price_elm = line_item.find("Cost_Price")
			#                 if cost_price_elm is not None:
			#                     cost_price = cost_price_elm.text
			#                     vals["cost_price_unit"] = cost_price
			#                 designer_elm = line_item.find("Designer_Code")
			#                 if designer_elm is not None:
			#                     designer_code = designer_elm.text
			#                     designer_ids = res_partner_registry.search(cr, 1, [("supplier","=",True),("supplier_code","=",designer_code)])
			#                     if designer_ids:
			#                         vals["product_partner_id"] = designer_ids[0]
			#                 y = order_registry.add_order_line(cr,1,vals)

			#         order_registry.write(cr, 1, x, {"note":order_notes})

			#         for adjustment in adjustments:
			#             label = adjustment.find("label").text
			#             notes = adjustment.find("notes").text
			#             amount = adjustment.find("amount").text
			#             adjustment_registry.create(cr, 1, {"order_id":x, "adjusment_label":label, "adjustment_notes":notes, "amount":amount})

			#         for payment in payments:
			#             method = payment.find("Payment_Method").text
			#             reference = payment.find("Payment_ID").text
			#             amount = payment.find("Amount").text
			#             payment_registry.create(cr, 1, {"order_id":x, "payment_method":method, "payment_reference":reference, "amount":amount})
			#         response += "order %s created," %(order_id)

			#     return Response(json.dumps(response), status=200, content_type="application/json")


		#     else:
		#         return Response(json.dumps("Content-Type Not Acceptable"), status=406, content_type="application/json")
		# else:
		#     return Response(json.dumps("Request Forbidden"), status=403, content_type="application/json")
		
	@http.route('/threep/amazon_int_orders', methods=["POST","GET"], auth="public")
	@authenticate
	def threep_order_creation_amazon_international(self):
		mutex.acquire()
		try:
			request_body = json.loads(request.httprequest.data)
		except:
			mutex.release()
			return Response(json.dumps("Invalid JSON"), status=403, content_type="application/json")
		cr = request.cr
		uid = request.uid
		order_ref = request_body.get("order_ref")
		line_items = request_body.get("line_items")
		address = request_body.get("address")
		create_date = request_body.get("create_date")
		try:
			  create_date = datetime.strptime(create_date, "%Y-%m-%dT%H:%M:%SZ")
			  create_date = fields.datetime.context_timestamp(cr, uid, create_date)
		except:
			  pass
		currency = request_body.get("currency")
		shipping_type = request_body.get("shipping_type")
		status = request_body.get("status")
		channel = request_body.get("channel")
		total_amount = request_body.get("total_amount")
		shipping_cost = request_body.get("shipping_cost")
		payment_id = request_body.get("payment_id")
		promotion_discount = request_body.get("promotion_discount")
		payment_method = request_body.get("payment_method")
		threep_order_creation_obj = request.registry.get("threep.order.creation")
		order_obj = request.registry.get("sale.order")
		check_order = order_obj.search(cr, 1, [("order_id","=",order_ref)])
		# payment = {}
		# payment["payment_method"] = "ThirdPartyPayment"
		# payment["payment_reference"] = order_ref
		# payment["amount"] =  float(total_amount)
		# adjustments = []
		# adjustment = {}
		# adjustment["adjusment_label"] = "standard_shipping"
		# adjustment["adjustment_notes"] = "Amazon Order Shipping Charge"
		# adjustment["amount"] = shipping_cost
		# adjustments.append(adjustment)
		# # promotion_discount = sum( i for i in [y["promotion_discount"] for y in line_items])
		# adjustment = {}
		# adjustment["adjusment_label"] = "promotion"
		# adjustment["adjustment_notes"] = "Amazon Promotion"
		# adjustment["amount"] = -promotion_discount
		# adjustments.append(adjustment)
		if check_order:
			if channel=='Amazon.com':
				x = threep_order_creation_obj.amazon_int_update_orders(cr,1,check_order,status,order_ref,line_items,total_amount,address,channel,currency)
				mutex.release()
				return x
			else:
				mutex.release()
				return True
		else:
			try:
				x = threep_order_creation_obj.create_orders(cr, 1, order_ref, line_items, address, create_date, currency, shipping_type, status, channel, total_amount, shipping_cost, None, promotion_discount,payment_method)
				mutex.release()
				return Response(json.dumps(" %s Order created" %(order_ref)), status=200, content_type="application/json")
			except:
				mutex.release()
				return Response(json.dumps(" %s Failure" %(order_ref)), status=200, content_type="application/json")
		mutex.release()



	@http.route('/spree_api/pull_from_sqs', methods=["GET"], auth="none")
	def pull_from_sqs(self):

		if request.httprequest.method=="GET":
			order_data = order_queue.get_messages(message_attributes=['content_type'])
			if not order_data:
				return Response(json.dumps("No New Orders!"), status=200, content_type="application/json")
			for order in order_data:

				body = order.get_body()
				try:
					content_type = order.message_attributes["content_type"]["string_value"]
				except KeyError:
					return Response(json.dumps("content_type not specified in SQS message"), status=400, content_type="application/json")

				url = "http://127.0.0.1:8069/spree_api/orders"
				header = {"Content-Type":content_type, "API_KEY":API_KEY}

				if content_type == "xml":
					try:
						root = ET.fromstring(body)
					except ET.ParseError:
						return Response(json.dumps("Invalid XML"), status=400, content_type="application/json")

				elif content_type == "json":
					try:
						x = json.loads(body)
					except ValueError:
						return Response(json.dumps("Invalid JSON"), status=400, content_type="application/json")


				req = urllib2.Request(url, body, header)
				try:
					response = urllib2.urlopen(req)
					print response.read()
				except urllib2.HTTPError, e:
						print e.code
						print e.msg
						print e.headers
						print e.fp.read()
		else:
			return Response(json.dumps("Request Forbidden"), status=403, content_type="application/json")
	@http.route('/spree_api/update_state',methods=["POST","GET"], auth="public")
	@authenticate
	def update_order_shipment_state(self):
		mutex.acquire()
		awb_cour = request.registry.get('awb.courier')
		account_invoice_reg = request.registry.get('account.invoice')
		tracking_details_reg = request.registry.get('tracking.details')
		cr = request.cr
		uid = request.uid
		try:
			
			if request.httprequest.method == "POST":
				tracking_details = json.loads(request.httprequest.data)
		except:
			mutex.release()
			return Response(json.dumps("Invalid JSON"), status=403, content_type="application/json")
		awb_ids = awb_cour.search(cr,1,[('awb_no','in',tracking_details.keys())])
		awb_objs = awb_cour.browse(cr,1,awb_ids)
		try:
			for awb in awb_objs:    
				if tracking_details[awb.awb_no]['status_type'] == 'RT' and tracking_details[awb.awb_no]['return_awb'] :
					awb_cour.write(cr,1,awb.id,{'return_awb':tracking_details[awb.awb_no]['return_awb']})

				if tracking_details[awb.awb_no]['status_type'] in ['DL','RT','UD','IT','lost']:
					awb_state = awb.shipment_state
					inv_id = account_invoice_reg.search(cr,1,[('order_id','=',awb.order_no.order_id)])
					if awb_state != tracking_details[awb.awb_no]['status_type'] and tracking_details[awb.awb_no]['status'] in ['Delivered','RTO Initiated','Undelivered']:
						vals = {}
						vals = {'invoice_id':inv_id[0],'reason':tracking_details[awb.awb_no]['reason'],'status':tracking_details[awb.awb_no]['status'],'status_time':tracking_details[awb.awb_no]['status_date_time'],'status_loc':tracking_details[awb.awb_no]['status_loc']}
						tracking_details_reg.create(cr,1,vals)
					elif tracking_details[awb.awb_no]['status'] == 'Dispatched':
						vals = {}
						vals = {'invoice_id':inv_id[0],'status':tracking_details[awb.awb_no]['status'],'status_time':tracking_details[awb.awb_no]['status_date_time'],'status_loc':tracking_details[awb.awb_no]['status_loc']}
						track_id = tracking_details_reg.search(cr,1,[('invoice_id','=',awb.invoice_no.id),('status','=','Dispatched')])
						track_obj = tracking_details_reg.browse(cr,1,track_id)
						if not len(track_obj):
							tracking_details_reg.create(cr,1,vals)
						for track in track_obj:
							diff_format = '%d %B %Y,%H:%M'
							disp_date = datetime.strptime(tracking_details[awb.awb_no]['status_date_time'],'%Y-%m-%d,%H:%M:%S')
							tdelta = disp_date - datetime.strptime(track.status_time, diff_format)		
							if (tdelta.seconds/60)<=0:
								continue
							tracking_details_reg.create(cr,1,vals)
						awb_cour.write(cr,1,awb.id,{'is_delayed_sent':True})
					awb_cour.write(cr,1,awb.id,{'shipment_state':tracking_details[awb.awb_no]['status_type'],'return_awb':tracking_details[awb.awb_no]['return_awb']})
					
			mutex.release()
			return Response(json.dumps("Completed update shipment state: "+str(tracking_details.keys())), status=200, content_type="application/json")
		except Exception,e:
			mutex.release()
			return Response(json.dumps("Update shipment status Failed"+e.message), status=300, content_type="application/json")        

	@http.route('/spree_api/pricelist_update', methods=["POST","GET"], auth="public")
	@authenticate
	def create_pricelist(self, **kwargs):
		mutex.acquire()
		product_pricelist_item_registry = request.registry.get('product.pricelist.item')
		try:
			request_body = json.loads(request.httprequest.data)
		except:
			mutex.release()
			return Response(json.dumps("Invalid JSON"), status=403, content_type="application/json")
		cr = request.cr
		uid = request.uid
		update_dict = request_body.get("update_dict")
		try:
			id_done = product_pricelist_item_registry.create(cr, 1, update_dict)
			mutex.release()
			return Response(json.dumps(" %s Completed Pricelist Item import" %(update_dict)), status=200, content_type="application/json")
		except:
			mutex.release()
			return Response(json.dumps(" %s Failure" %(update_dict)), status=200, content_type="application/json")        
		mutex.release()

	@http.route('/spree_api/update_reverse_state',methods=["POST","GET"], auth="public")
	@authenticate
	def update_reverse_shipment_state(self):
		mutex.acquire()
		cr_reg = request.registry.get('outbound.cr.requests')
		cr = request.cr
		uid = request.uid
		try:
			
			if request.httprequest.method == "POST":
				tracking_details = json.loads(request.httprequest.data)
		except:
			mutex.release()
			return Response(json.dumps("Invalid JSON"), status=403, content_type="application/json")		
		try:
			if tracking_details:
				cr_id = cr_reg.search(cr,1,[('return_awb','=',tracking_details.keys()[0])])
				if cr_id:
					cr_obj = cr_reg.browse(cr,1,cr_id)
					cr_reg.write(cr,1,cr_obj.id,{'reverse_shipment_status':tracking_details[cr_obj.return_awb]})
			mutex.release()
			return Response(json.dumps("======reverse shipment state updated======"+str(tracking_details.keys())), status=200, content_type="application/json")
		except Exception,e:
			mutex.release()
			return Response(json.dumps("==========Reverse shipment state is not updated=========="+e.message), status=300, content_type="application/json")        


	@http.route('/spree_api/quantity_upload', methods=["POST","GET"], auth="public")
	@authenticate
	def quantity_move_from_virtual_inventory_loss(self):
		request.registry.get("stock.move")
		product_template = request.registry.get("product.template")
		cr = request.cr
		uid = request.uid
		if request.httprequest.method=="GET":
			my_user_record = request.registry.get("res.users").browse(cr, uid, uid)
			return my_user_record.name
		elif request.httprequest.method=="POST":
			headers = request.httprequest.headers
			data = request.httprequest.data
			response = {}
			content_type = str(headers["Content-Type"])
			if content_type == "json":
				try:
					body = json.loads(data)
				except:
					return Response(json.dumps("Not a valid JSON"), status=400, content_type="application/json")

				for sku,details in body.iteritems():
					get_product_id = "select id from product_product where name_template='%s'"%(sku)
					cr.execute(get_product_id)
					product_id = cr.fetchall()
					if not product_id:
						get_product_id = "select id from product_product where threep_reference='%s'"%(sku)
						cr.execute(get_product_id)
						product_id = cr.fetchall()
						if not product_id:
							get_product_id = "select id from product_product where ean13='%s' and active=true"%(sku)
							cr.execute(get_product_id)
							product_id = cr.fetchall()

					# if sku == 'SCBOM22370':
					#     pdb.set_trace()
					if details[0]['Type of'] == "FBA":
						try:
							stock_mve_query = """insert into stock_move (create_date,product_qty,location_id,
								partner_id,company_id,write_uid,priority,state,date_expected,create_uid,
								propagate,procure_method,name,location_dest_id,write_date,product_id,
								invoice_state,product_uom,product_uom_qty,date) values (now(),%d,36,1,1,1,1,'done',now()-interval'5 hours 30 minutes',1,true,'make_to_stock',
								'Quantity Upload',57,now()-interval'5 hours 30 minutes',%d,'none',1,1,now()-interval'5 hours 30 minutes')"""%(int(details[0]['quantity']),product_id[0][0])
							cr.execute(stock_mve_query)
						except:
							pass
							# response[sku]="failure"

					elif details[0]['Type of'] == "SOI":
						try:
							stock_mve_query = """insert into stock_move (create_date,product_qty,location_id,
								partner_id,company_id,write_uid,priority,state,date_expected,create_uid,
								propagate,procure_method,name,location_dest_id,write_date,product_id,
								invoice_state,product_uom,product_uom_qty,date) values (now(),%d,36,1,1,1,1,'done',now()-interval'5 hours 30 minutes',1,true,'make_to_stock',
								'Quantity Upload',66,now()-interval'5 hours 30 minutes',%d,'none',1,1,now()-interval'5 hours 30 minutes')"""%(int(details[0]['WMS Inventory']),product_id[0][0])
							cr.execute(stock_mve_query)
						except:
							pass
							# response[sku]="failure"
					elif details[0]["Type of"] == "Offline Bangalore":
						try:
							stock_mve_query = """insert into stock_move (create_date,product_qty,location_id,
								partner_id,company_id,write_uid,priority,state,date_expected,create_uid,
								propagate,procure_method,name,location_dest_id,write_date,product_id,
								invoice_state,product_uom,product_uom_qty,date) values (now(),%d,36,1,1,1,1,'done',now()-interval'5 hours 30 minutes',1,true,'make_to_stock',
								'Quantity Upload',59,now()-interval'5 hours 30 minutes',%d,'none',1,1,now()-interval'5 hours 30 minutes')"""%(int(details[0]['Qty']),product_id[0][0])
							cr.execute(stock_mve_query)
						except:
							pass
							# response[sku]="failure"
					elif details[0]["Type of"] == "Offline Nagpur":
						try:
							stock_mve_query = """insert into stock_move (create_date,product_qty,location_id,
								partner_id,company_id,write_uid,priority,state,date_expected,create_uid,
								propagate,procure_method,name,location_dest_id,write_date,product_id,
								invoice_state,product_uom,product_uom_qty,date) values (now(),%d,36,1,1,1,1,'done',now()-interval'5 hours 30 minutes',1,true,'make_to_stock',
								'Quantity Upload',73,now()-interval'5 hours 30 minutes',%d,'none',1,1,now()-interval'5 hours 30 minutes')"""%(int(details[0]['Qty']),product_id[0][0])
							cr.execute(stock_mve_query)
						except:
							pass
							# response[sku]="failure"

					elif details[0]["Type of"] == "Offline Pune":
						try:
							stock_mve_query = """insert into stock_move (create_date,product_qty,location_id,
								partner_id,company_id,write_uid,priority,state,date_expected,create_uid,
								propagate,procure_method,name,location_dest_id,write_date,product_id,
								invoice_state,product_uom,product_uom_qty,date) values (now(),%d,36,1,1,1,1,'done',now()-interval'5 hours 30 minutes',1,true,'make_to_stock',
								'Quantity Upload',77,now()-interval'5 hours 30 minutes',%d,'none',1,1,now()-interval'5 hours 30 minutes')"""%(int(details[0]['Qty']),product_id[0][0])
							cr.execute(stock_mve_query)
						except:
							pass
							# response[sku]="failure"
		return Response(json.dumps(response), status=200, content_type="application/json")

	@http.route('/spree_api/product_dimension', methods=["POST","GET"], auth="public")
	@authenticate
	def product_dimension(self, **kwargs):
		request_body = json.loads(request.httprequest.data)
		cursor = request.cr
		data = "','".join(request_body['skus'])
		query = "select distinct * from get_product_dimension() where sku in ('"+data+"');"
		dimensions_query = cursor.execute(query)
		dimensions_list = cursor.fetchall()
		dimensions_list = json.dumps(dimensions_list)
		return dimensions_list
