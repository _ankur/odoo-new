# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import tools
from openerp.osv import fields, osv

class supplier_sale_report(osv.osv):
    _name = "supplier.sale.report"
    _description = "Sales Orders Statistics"
    _auto = False
    _rec_name = 'date'

    def _current_user_partner(self, cursor, user, ids, name, arg, context=None):
        # vals = {'pid': sale_order_objs}  
        res = {}
        cur_user = self.pool.get('res.users')
        cur_user_partner_id = cur_user.search(cursor,user,[('id','=',user)],context=None)
        cur_user_obj=cur_user.browse(cursor, user, cur_user_partner_id)
        for po in self.browse(cursor, user, ids, context=context):
            res[po.id] = po.partner_id
        return res

    def _current_user_partner_search(self, cursor, user, ids, name, arg, context=None):
        res = {}
        cur_user = self.pool.get('res.users')
        cur_user_partner_id = cur_user.search(cursor,user,[('id','=',user)],context=None)
        cur_user_obj=cur_user.browse(cursor, user, cur_user_partner_id)
        return [('partner_id', '=', cur_user_obj.partner_id.id )]

    _columns = {
        'date': fields.datetime('Date Order', readonly=True),  # TDE FIXME master: rename into date_order
        'date_confirm': fields.date('Date Confirm', readonly=True),
        'product_id': fields.many2one('product.product', 'Product', readonly=True),
        'product_uom_qty': fields.float('# of Qty', readonly=True),
        'partner_id': fields.many2one('res.partner', 'Partner', readonly=True),
        'cost_price_total': fields.float('Total Cost Price', readonly=True),
        'categ_id': fields.many2one('product.category','Category of Product', readonly=True),
        'state': fields.selection([
            ('draft', 'Quotation'),
            ('waiting_date', 'Waiting Schedule'),
            ('manual', 'Manual In Progress'),
            ('progress', 'In Progress'),
            ('invoice_except', 'Invoice Exception'),
            ('done', 'Done'),
            ('cancel', 'Cancelled')
            ], 'Order Status', readonly=True),
        'current_user_partner_id':fields.function(_current_user_partner, type='integer', string="Partner Id",fnct_search=_current_user_partner_search,method=True)
    }
    _order = 'date desc'

    def _select(self):
        select_str = """
             SELECT min(l.id) as id,
                    l.product_id as product_id,
                    sum(l.product_uom_qty) as product_uom_qty,
                    sum(l.product_uom_qty * l.cost_price_unit) as cost_price_total,
                    s.dispatched_date as date,
                    s.state,
                    s.date_confirm as date_confirm,
                    l.product_partner_id as partner_id,
                    t.categ_id as categ_id
        """
        return select_str

    def _from(self):
        from_str = """
                sale_order_line l
                      join sale_order s on (l.order_id=s.id)
                        left join product_product p on (l.product_id=p.id)
                            left join product_template t on (p.product_tmpl_id=t.id)
                    left join product_uom u on (u.id=l.product_uom)
                    left join product_uom u2 on (u2.id=t.uom_id)
        """
        return from_str

    def _group_by(self):
        group_by_str = """
            GROUP BY l.product_id,
                    l.order_id,
                    t.uom_id,
                    t.categ_id,
                    s.dispatched_date,
                    s.date_confirm,
                    l.product_partner_id,
                    s.user_id,
                    s.company_id,
                    s.state,
                    s.pricelist_id,
                    s.project_id,
                    s.section_id
        """
        return group_by_str

    def init(self, cr):
        # self._table = sale_report
        tools.drop_view_if_exists(cr, self._table)
        cr.execute("""CREATE or REPLACE VIEW %s as (
            %s
            FROM ( %s )
            where s.dispatched_date is not null
            %s
            )""" % (self._table, self._select(), self._from(), self._group_by()))

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:


