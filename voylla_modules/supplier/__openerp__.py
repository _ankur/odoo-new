{
    'name': "Supplier",
    'version': "1.0",
    'author': "Dheeraj Mathur",
    'category': "Tools",
    'depends': ['stock', 'procurement','purchase','crm','sale'],
    'data': [
        'security/supplier_security.xml',
        'security/ir.model.access.csv',
        'supplier_view.xml',
        'report/supplier_sale_report_view.xml',
        'supplier_workflow.xml'
    ],
    'update_xml': [
        'supplier_workflow.xml'
    ],
    'demo': [],
    'installable': True,
    'auto_install': True,
    'application': True,
}