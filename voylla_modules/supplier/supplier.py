from openerp.osv import fields, osv
from openerp import tools, SUPERUSER_ID
import pdb
import xlwt
import subprocess
import shlex
import datetime
import re
# Adding Voylla Specific Fields to Product

class supplier_product_category(osv.osv):
	_name='supplier.product.category'
	_columns = {
		'name': fields.char('Category Name', required=True)
	}
supplier_product_category()

class supplier_product(osv.osv):
	_name='supplier.product'
	def _get_image(self, cr, uid, ids, name, args, context=None):
		result = dict.fromkeys(ids, False)
		for obj in self.browse(cr, uid, ids, context=context):
			result[obj.id] = tools.image_get_resized_images(obj.image_one, avoid_resize_medium=True)
		return result
	def _set_image(self, cr, uid, id, name, value, args, context=None):
		return self.write(cr, uid, [id], {'image_one': tools.image_resize_image_big(value)}, context=context)	
	
	def supplier_product_new(self, cr, uid, ids):
		self.write(cr, uid, ids, { 'state' : 'new' })
		return True
	def supplier_product_accepted(self, cr, uid, ids, context=None):
		self.write(cr, uid, ids, { 'state' : 'accepted' })
		return True
	def supplier_product_rejected(self, cr, uid, ids):
		self.write(cr, uid, ids, { 'state' : 'rejected' })
		return True
	def supplier_product_rfs(self, cr, uid, ids):
		self.write(cr, uid, ids, { 'state' : 'rfs' })
		return True
	def supplier_product_negotiation(self, cr, uid, ids):
		self.write(cr, uid, ids, { 'state' : 'negotiation' })
		return True
	def supplier_partner_id(self, cr, uid, ids):
		return self.pool['res.users'].browse(cr, uid, uid, context=context).partner_id.id
	_columns = {
		'image': fields.binary('Image'),
		'image_medium': fields.function(_get_image, fnct_inv=_set_image,
			string="Medium-sized image", type="binary", multi="_get_image",
			store={
				'supplier.product': (lambda self, cr, uid, ids, c={}: ids, ['image'], 10),
			},
			help="Medium-sized image of the product. It is automatically "\
					"resized as a 128x128px image, with aspect ratio preserved, "\
					"only when the image exceeds one of those sizes. Use this field in form views or some kanban views."),
		'transfer_price': fields.integer('Transfer Price'),
		'state': fields.selection([
			('new', 'New Product'),
			('accepted', 'Accepted'),
			('rejected', 'Rejected'),
			('rfs', 'Request for Sample'),
			('negotiation', 'In Negotiation'),
			], string='Status', index=True, readonly=True, default='new',
			track_visibility='onchange', copy=False,
			help="Gives the status of the quotation or sales order. \nThe exception status is automatically set when a cancel operation occurs in the processing of a document linked to the sales order. \nThe 'Waiting Schedule' status is set when the invoice is confirmed but waiting for the scheduler to run on the order date.", select=True),
		'supplier_code': fields.char('Supplier Product Code', size=15, required=True),
		'qty':fields.integer('Quantity', required=True),
		'material':fields.char('material', required=True),
		'repeatable':fields.boolean('Repeatable'),
		'leadtime':fields.integer('Minimum leadtime', required=True),
		'notes':fields.text('Notes'),
		'category':fields.many2one('supplier.product.category','Category',required=True)
	}

	_defaluts={
		'state': 'new',
	}
supplier_product()
# Adding Voylla Specific Fields to Supplier

class supplier_product_messaging(supplier_product):
	_inherit = 'mail.thread'

class add_fnct_field_to_purchase_order(osv.osv):
	_inherit = 'purchase.order'
	def _current_user_partner(self, cursor, user, ids, name, arg, context=None):
		# vals = {'pid': sale_order_objs}  
		res = {}
		cur_user = self.pool.get('res.users')
		cur_user_partner_id = cur_user.search(cursor,user,[('id','=',user)],context=None)
		cur_user_obj=cur_user.browse(cursor, user, cur_user_partner_id)
		for po in self.browse(cursor, user, ids, context=context):
			res[po.id] = po.partner_id
		return res

	def _current_user_partner_search(self, cursor, user, ids, name, arg, context=None):
		res = {}
		cur_user = self.pool.get('res.users')
		cur_user_partner_id = cur_user.search(cursor,user,[('id','=',user)],context=None)
		cur_user_obj=cur_user.browse(cursor, user, cur_user_partner_id)
		return [('partner_id', '=', cur_user_obj.partner_id.id )]

	_columns = {
		'current_user_partner_id':fields.function(_current_user_partner, type='integer', string="Partner Id",fnct_search=_current_user_partner_search,method=True)
	}
add_fnct_field_to_purchase_order()

class add_cost_price_unit_to_sale_order_line(osv.osv):
    _inherit = 'sale.order.line'
    _columns = {
		'cost_price_unit':fields.float('Unit Cost Price', readonly=True, states={'draft': [('readonly', False)]}),
		'product_partner_id':fields.many2one('res.partner', 'Partner', readonly=True, states={'draft': [('readonly', False)], 'sent': [('readonly', False)]}, change_default=True, select=True, track_visibility='always')
	}
add_cost_price_unit_to_sale_order_line()




class monthly_supplier_report(osv.osv):
	_name='monthly.supplier.report'

	_columns={
	'name': fields.char('Supplier Name'),
	'url': fields.char('Url', size=1024),
	'supplier_id': fields.integer('Supplier id'),
	'month':fields.text('Month'),
	'year':fields.text('Year')
	# 'inv_url': fields.char('Url', size=1024)
	# 'cur_login_user': fields.function(print_supplier_specific_report, type="integer", fnct_search=None, obj=None, method=True, store=False)
	}


	def print_sales_report(self,cr,uid,ids,context=None):
		# product = self.pool.get('product_product')
		# monthly_report=self.pool.get('monthly.supplier.report')
		today = datetime.date.today()
		first = datetime.date(day=1, month=today.month, year=today.year)
		month = first - datetime.timedelta(days=1)
		sale_query="select gms.SKU, gms.Supplier_Product_Code, gcfr.Category ,gms.Cost, gms.Qty , gms.Supplier_Name, gms.Supplier_id from get_monthly_sale() as gms left join get_category_for_report() as gcfr on gms.SKU=gcfr.Product_name order by gms.Supplier_Name"
		cr.execute(sale_query)
		sale_store = cr.fetchall()
		credit_query = "select gms.SKU, gms.Supplier_Product_Code, gcfr.Category ,gms.Cost, gms.Qty, gms.Status,gms.Supplier_Name, gms.Supplier_id  from get_rto_cr() as gms left join get_category_for_report() as gcfr on gms.SKU=gcfr.Product_name order by gms.Supplier_Name"
		cr.execute(credit_query)
		credit_store = cr.fetchall()
		cur_rname = credit_store[0][6]
		s_name=sale_store[0][5]
		s_id=sale_store[0][6]
		cur_sup='temp'
		# pdb.set_trace()
		if sale_store[0][5]<=credit_store[0][6]:
			flag=sale_store[0][5]
			temp=sale_store[0][0][:5]
		else:
			flag=credit_store[0][6]
			temp=credit_store[0][0][:5]

		if sale_store[-1][5]>=credit_store[-1][6]:
			last=sale_store[-1][5]
		else:
			last=credit_store[-1][6]
		style = xlwt.easyxf('font: height 200, name Times New Roman, bold on; align: wrap on,vert centre;')
		for val in range(10000):
			book = xlwt.Workbook()
			report = book.add_sheet('Sales Report')
			total_sale=0.0
			total_credit=0.0
			first_col=report.col(0)
			first_col.width=256*16
			sec_col=report.col(1)
			sec_col.width=256*16
			third_col=report.col(2)
			third_col.width=256*18
			report.write_merge(1,1,2,6,'Voylla.com Monthly Designer Sales Report',style)
			report.write_merge(3,3,0,2,'Month : %s'%month.strftime("%m/%Y"))
			report.write_merge(4,4,0,2,'Designer Name : %s'%s_name)
			report.write_merge(3,7,3,6,'Please Send the Invoice in favour of:\nVoylla Retail Pvt Ltd\nE-521, RIICO Industrial Area, Near Chatrala Circle,\nSitapura, Jaipur 302022 (RAJ)\nTIN: ',style)
			report.write(9,0, 'SKU',style);
			report.write(9,1, 'Designer Code',style);
			report.write(9,2, 'Category',style);
			report.write(9,3, 'Cost',style);
			report.write(9,4, 'Quantity',style);
			report.write(9,5, 'Total Cost',style);
			for index,supplier in enumerate(sale_store):
				cur_sup=supplier
				if supplier[5]==flag:
					for col,column in enumerate(supplier):
						if col<5:
							report.write(index+10,col,column);
						elif col==5:
							if not supplier[3]:
								pass
							else:
								# pdb.set_trace()
								total_cost=xlwt.Formula("%f*%f"%(supplier[3],supplier[4]))
								report.write(index+10,col,total_cost)
								total_sale+=(supplier[3]*supplier[4])
						else:
							continue
				else:
						# 	continue
					del sale_store[:index]
					break;
			report.write_merge(5,5,0,2,'Amount : %d'%total_sale)
			report = book.add_sheet('Credit Report')
			first_col=report.col(0)
			first_col.width=256*16
			sec_col=report.col(1)
			sec_col.width=256*16
			third_col=report.col(2)
			third_col.width=256*18
			report.write_merge(1,1,2,6,'Voylla.com Monthly Designer Credit Report',style)
			report.write_merge(3,3,0,1,'Month : %s'%month.strftime("%m/%Y"))
			report.write_merge(4,4,0,1,'Designer Name : %s'%s_name)
			report.write_merge(3,8,3,6,'Please Send the Credit Note in favour of:\nVoylla Retail Pvt Ltd\nE-521, RIICO Industrial Area, Near Chatrala Circle,\nSitapura, Jaipur 302022 (RAJ)\nTIN: ',style)
			report.write(9,0, 'SKU',style);
			report.write(9,1, 'Designer Code',style);
			report.write(9,3, 'Cost',style);
			report.write(9,2, 'Category',style);
			report.write(9,4, 'Quantity',style);
			report.write(9,5, 'Total Cost',style);
			report.write(9,6, 'Status',style);
			for index1,suppliers in enumerate(credit_store):
				cur_rname=suppliers
				if suppliers[6]==flag:
					for col,column in enumerate(suppliers):
						# pdb.set_trace()
						if col<5:
							report.write(index1+10,col,column)
						elif col==5:
							report.write(index1+10,col+1,column)
						elif col==6:
							if not suppliers[3]:
								pass
							else:
								total_cost=xlwt.Formula("%f*%f"%(suppliers[3],suppliers[4]))
								report.write(index1+10,col-1,total_cost)
								total_credit+=(suppliers[3]*suppliers[4])				
				else:
					del credit_store[:index1]
					break
			# pdb.set_trace()
			temp2 = s_name.replace(" ","")
			temp1 = re.sub('[^a-zA-Z0-9 \n\.]', '', temp2)
			report.write_merge(5,5,0,2,'Amount : %d'%total_credit)
			book.save('Voylla_Sales_Report_%s_%s.xls'%(temp1,month.strftime("%m-%Y")))
			feed='Voylla_Sales_Report_%s_%s.xls'%(temp1,month.strftime("%m-%Y"))

			c1="s3cmd put "+ feed +" s3://voyllaerp/erp_tag/"
			args = shlex.split(c1)
			p = subprocess.Popen(args)
			p.wait()
			c2="s3cmd setacl s3://voyllaerp/erp_tag/"+feed+" --acl-public"
			args = shlex.split(c2)
			subprocess.Popen(args)
			p.wait()
			url_base = 'https://s3.amazonaws.com/voyllaerp/erp_tag/'+feed
			# if not s_name:
			name_id= 'Sales_Report'
			# else:
			# 	name_id=s_name+'Sales_Report'+'_'+month.strftime("%m-%Y")
			# pdb.set_trace()
			self.create(cr,uid,{'name':name_id, 'month': month.strftime("%B"),'year': month.strftime("%Y") ,'url':url_base,'supplier_id':s_id})
			# 
			if flag==last:
				break;
			else:
				flag=cur_sup[5]
				s_name=cur_sup[5]
				s_id=cur_sup[6]
				temp=cur_sup[0][:5]
				# pdb.set_trace()
				if cur_rname[6].lower()<flag.lower():
					flag=cur_rname[6]
					s_name=cur_rname[6]
					s_id=cur_rname[7]
					temp=cur_rname[0][:5]

	def print_inventory_report(self,cr,uid,ids,context=None):
		today = datetime.date.today()
		first = datetime.date(day=1, month=today.month, year=today.year)
		month = first - datetime.timedelta(days=1)
		inventory_query="select apq.SKU, apq.Supplier_Product_Code, gcfr.Category ,apq.Cost, apq.Qty, apq.Supplier_Name, apq.Supplier_id from inventory_report() as apq left join get_category_for_report() as gcfr on apq.SKU=gcfr.Product_name order by apq.Supplier_Name"
		cr.execute(inventory_query)
		inventory_store = cr.fetchall()
		s_name=inventory_store[0][5]
		s_id=inventory_store[0][6]
		cur_sup='temp'
		# pdb.set_trace()
		flag=inventory_store[0][5]
		temp=inventory_store[0][0][:5]

		last=inventory_store[-1][5]

		style = xlwt.easyxf('font: height 200, name Times New Roman, bold on; align: wrap on,vert centre;')
		for val in range(10000):
			book = xlwt.Workbook()
			report = book.add_sheet('Inventory Report')
			total_inventory=0.0
			first_col=report.col(0)
			first_col.width=256*16
			sec_col=report.col(1)
			sec_col.width=256*16
			third_col=report.col(2)
			third_col.width=256*18
			report.write_merge(1,1,2,6,'Voylla.com Monthly Designer Inventory Report',style)
			report.write_merge(3,3,0,2,'Month : %s'%month.strftime("%m/%Y"))
			report.write_merge(4,4,0,2,'Designer Name : %s'%s_name)
			# report.write_merge(3,7,3,6,'Please Send the Invoice in favour of:\nVoylla Retail Pvt Ltd\nE-521, RIICO Industrial Area, Near Chatrala Circle,\nSitapura, Jaipur 302022 (RAJ)\nTIN: ',style)
			report.write(9,0, 'SKU',style);
			report.write(9,1, 'Designer Code',style);
			report.write(9,2, 'Category',style);
			report.write(9,3, 'Cost',style);
			report.write(9,4, 'Quantity',style);
			report.write(9,5, 'Total Cost',style);
			for index,supplier in enumerate(inventory_store):
				cur_sup=supplier
				if supplier[5]==flag:
					for col,column in enumerate(supplier):
						if col<5:
							report.write(index+10,col,column);
						elif col==5:
							if not supplier[3]:
								pass
							else:
								# pdb.set_trace()
								total_cost=xlwt.Formula("%f*%f"%(supplier[3],supplier[4]))
								report.write(index+10,col,total_cost)
								total_inventory+=(supplier[3]*supplier[4])
						else:
							continue
				else:
						# 	continue
					del inventory_store[:index]
					break;
			report.write_merge(5,5,0,2,'Amount : %d'%total_inventory)
			
			# pdb.set_trace()
			temp2 = s_name.replace(" ","")
			temp1 = re.sub('[^a-zA-Z0-9 \n\.]', '', temp2)
			book.save('Voylla_Inventory_Report_%s_%s.xls'%(temp1,month.strftime("%m-%Y")))
			feed='Voylla_Inventory_Report_%s_%s.xls'%(temp1,month.strftime("%m-%Y"))

			c1="s3cmd put "+ feed +" s3://voyllaerp/erp_tag/"
			args = shlex.split(c1)
			p = subprocess.Popen(args)
			p.wait()
			c2="s3cmd setacl s3://voyllaerp/erp_tag/"+feed+" --acl-public"
			args = shlex.split(c2)
			subprocess.Popen(args)
			p.wait()
			url_base = 'https://s3.amazonaws.com/voyllaerp/erp_tag/'+feed
			# if not s_name:
			name_id='Inventory Report'
			# else:
			# 	name_id=s_name+'_'+month.strftime("%m-%Y")
			# pdb.set_trace()
			self.create(cr,uid,{'name':name_id,'month':month.strftime("%B"),'year':month.strftime("%Y"),'url':url_base,'supplier_id':s_id})
			# 
			if flag==last:
				break;
			else:
				flag=cur_sup[5]
				s_name=cur_sup[5]
				s_id=cur_sup[6]
				temp=cur_sup[0][:5]
				# pdb.set_trace()