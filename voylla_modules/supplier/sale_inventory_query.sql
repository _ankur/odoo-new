

-- CR function
CREATE OR REPLACE FUNCTION get_monthly_cr() RETURNS table( SKU text, Supplier_Product_Code text, 
	Cost double precision, Qty numeric, Status text, Supplier_Name text, Supplier_id integer ) AS $$ 
select get_product_id.name as product_name,supplier.product_code as supplier_code,
get_product_id.cost as cost,sum(get_product_id.qty) as qty,cast('CR' as text) as rto_cr,
partner.name as supplier_name,users.id as user_id from
(select p1.name,p1.oc_product_template_id as template_id,p1.qty ,p2.cost
from (select oc.order_number as order_number,p.id as oc_product_id,
	p.product_tmpl_id as oc_product_template_id,
p.name_template as name,count(*) as qty from outbound_order_cr as ooc left join product_product as p
 on ooc.ean = p.ean13,
outbound_cr as oc where ooc.pack_id = oc.id  and 
ooc.create_date >= (select date_trunc('month',now()-interval'1 month')) 	 and 
ooc.create_date < (select date_trunc('month',now())) group by order_number ,oc_product_id,
oc_product_template_id,
name) as p1 , (select sol.id as solid,sol.cost_price_unit as cost ,so.order_id as so_name ,
sol.product_id as solid_product_id 
from sale_order_line as sol ,sale_order as so  where sol.order_id = so.id) as p2 
where p1.order_number = p2.so_name and p1.oc_product_id =p2.solid_product_id ) as get_product_id 
left join product_supplierinfo as supplier on get_product_id.template_id = supplier.product_tmpl_id 
left join res_partner  as partner on supplier.name=partner.id
left join res_users as users on partner.id = users.partner_id
group by get_product_id.name,supplier.product_code,cost,rto_cr,users.id,supplier_name,user_id $$
LANGUAGE SQL;

-- RTO function
CREATE OR REPLACE FUNCTION get_monthly_rto() RETURNS table(SKU text, Supplier_Product_Code text, Cost double precision, Qty numeric, Status text, Supplier_Name text,Supplier_id integer) AS $$ 
select product.name_template as product_name,supplier.product_code as supplier_code,
sol.cost_price_unit as cost,sum(sol.product_uom_qty) as qty ,cast('RTO' as text) as rto_cr,partner.name as supplier_name,users.id as user_id from product_product as product 
left join product_supplierinfo as supplier on product.product_tmpl_id = supplier.product_tmpl_id 
left join res_partner  as partner on supplier.name=partner.id
left join res_users as users on partner.id = users.partner_id,outbound_rto as rto 
left join sale_order_line as sol on sol.rto_id= rto.id
where rto.create_date >= (select date_trunc('month',now()-interval'1 month')) and rto.create_date < (select date_trunc('month',now())) 
 and sol.product_id = product.id group by product.name_template,supplier.product_code,cost,rto_cr,supplier_name,users.id $$ 
LANGUAGE SQL;


-- RTo and CR joining FUNCTION
Create OR REPLACE FUNCTION get_rto_cr() RETURNS TABLE(SKU text,Supplier_Product_Code text, Cost double precision, Qty numeric, Status text, Supplier_Name text, Supplier_id integer ) AS $$ select SKU, Supplier_Product_Code, Cost, Qty, Status, Supplier_Name, Supplier_id from get_monthly_cr() union select SKU, Supplier_Product_Code, Cost, Qty, Status, Supplier_Name, Supplier_id from get_monthly_rto() $$
LANGUAGE SQL;

-- Sale Report
CREATE OR REPLACE FUNCTION get_monthly_sale() RETURNS table (SKU text, Supplier_Product_Code text, 
Cost double precision, Qty numeric, Supplier_Name text,Supplier_id integer ) AS $$
select t.name_template ,info.product_code,t.cost_price_unit,sum(t.product_uom_qty),partner.display_name,users.id
from(select product.product_tmpl_id,product.name_template,sol.cost_price_unit,sol.product_uom_qty
from sale_order_line as sol ,product_product as product ,sale_order as so
where sol.product_id = product.id and so.dispatched_date >= (select date_trunc('month', now()-interval'1 month'))
and so.dispatched_date < (select date_trunc('month', now())) and sol.order_id = so.id )as t 
left join (select name,product_tmpl_id, string_agg(product_code,'::') as product_code from product_supplierinfo 
	group by product_tmpl_id,name ) as info on t.product_tmpl_id = info.product_tmpl_id
inner join res_partner as partner on info.name= partner.id
left join res_users as users on partner.id = users.partner_id group by t.name_template, info.product_code,
t.cost_price_unit,partner.display_name,users.id $$
LANGUAGE SQL;



-- Getting Category(toxon), Type
CREATE or REPLACE FUNCTION get_product_id_taxon() RETURNS TABLE(product_id integer,taxon text) AS $$
select p.id as product_id,string_agg(ppc.taxon,'::') from (select pc.id as pc_id,
rtrim(ltrim(pc.name)) as taxon from product_category as pc 
left join product_category as ppc on pc.parent_id =ppc.id 
left join product_category as pppc on ppc.parent_id = pppc.id where pc.parent_id is not null and pppc.name is null
union
select pc.id as pc_id,rtrim(ltrim(ppc.name)) as parent
from product_category as pc left join product_category as ppc on pc.parent_id =ppc.id
left join product_category as pppc on ppc.parent_id = pppc.id 
where pc.parent_id is not null and pppc.name is not null) as ppc,product_template as p 
left join product_taxonomy as pt on p.id=pt.taxonomy 
where ppc.pc_id=pt.categ_id GROUP BY product_id$$
LANGUAGE SQL;

-- Category(toxon), Type FUNCTION
CREATE or REPLACE FUNCTION get_category_for_report() RETURNS TABLE (Product_name char, Category text) AS $$ select p.name ,k.taxon from get_product_id_taxon() as k,product_template as p where k.product_id =p.id $$
LANGUAGE SQL;

-- Getting Product cost
CREATE OR REPLACE FUNCTION get_product_cost() RETURNS TABLE(cost double precision,
product_id integer)
AS $$ select pph.cost,pph.product_template_id as product_id from product_price_history as pph ,
(select product_template_id,max(create_date) as last_updated_date from product_price_history
group by product_template_id) as last_updated_product
where pph.product_template_id = last_updated_product.product_template_id and pph.create_date=last_updated_product.last_updated_date
$$
LANGUAGE SQL;


-- Live product

create or replace function inventory_report() returns
 table (SKU text, Supplier_Product_Code text, Qty numeric, Cost double precision,
  Supplier_Name text, Supplier_id integer ) as $$
(select  t.sku as SKU, t.dcode as Supplier_Product_Code,t.count as Qty,gpc.cost as Cost, t.sname as Name, users.id as Supplier_id from
(select SUM(p_count.count) as count , product.name_template as sku,
info.product_code as dcode, partner.display_name as sname, partner.id as pid, product.product_tmpl_id as pdid  from 
(SELECT  SUM(qty.count) as count, qty.product_id
FROM
	(
	    select -1 * sum(product_uom_qty) as count,product_id from stock_move  as sm
	    where location_id = 30 or location_id =29 or location_id =66 
	    or location_id =77 or location_id =61 or location_id =58
	    or location_id =73 or location_id =72 or location_id =57
	    or location_id =78 or location_id =75 or location_id =59
	    or location_id =18 and state = 'done'and sm.date<(select date_trunc('month', now())) 
	     group by product_id
	    UNION ALL
	    select sum(product_uom_qty) as count,product_id from stock_move  as sm
	    where location_dest_id = 30 or location_dest_id =29 or location_dest_id =66 
	    or location_dest_id =77 or location_dest_id =61 or location_dest_id =58
	    or location_dest_id =73 or location_dest_id =72 or location_dest_id =57
	    or location_dest_id =78 or location_dest_id =75 or location_dest_id =59
	    or location_dest_id =18
	    and state = 'done' and sm.date<(select date_trunc('month', now())) 
	     group by product_id
	) as qty 
       
group by qty.product_id) as p_count , product_product as product ,product_supplierinfo as info , res_partner as partner
where p_count.product_id = product.id and product.product_tmpl_id = info.product_tmpl_id and info.name= partner.id and count>0
 group by sku,dcode,sname,pid,pdid) as t
left join get_product_cost() as gpc on gpc.product_id=pdid
left join res_users as users on users.partner_id= t.pid) order by name 
$$ LANGUAGE SQL;




















