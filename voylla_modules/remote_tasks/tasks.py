from openerp.osv import fields, osv
import os
from ConfigParser import RawConfigParser
import pdb
from config import config
from config import SPREE_URL, SPREE_TOKEN
from celery import Celery

#BROKER = config.get(os.environ.get("ODOO_ENV")).get('BROKER')
BROKER = "amqp://erp:erp@localhost:5672/erp"
remote_queue = Celery(broker = BROKER)

class remote_tasks(osv.osv):

	_name = "remote.tasks"

	def update_snapdeal(self, cr, uid, user, password, location, update, context=None):
		remote_queue.send_task("tasks.update_snapdeal_qty_price", args=(user, password, location, update))

	def update_spree(self, cr, uid, update, context=None):
		remote_queue.send_task("tasks.update_spree", args=(SPREE_TOKEN, SPREE_URL, update))

	def generate_invoice(self, cr, uid, order_ids, context=None):
		remote_queue.send_task("tasks.generate_invoice", args=(order_ids))
		
	def import_order_from_queue(self,cr, uid, order_id, order_line_items, address_hash, create_date, currency, shipping_type, status, source, total_amount, shipping_cost,payment_method,promotion_discount):	
		remote_queue.send_task("tasks.import_order_from_queue", args=(order_id, order_line_items, address_hash, create_date, currency, shipping_type, status, source, total_amount, shipping_cost,payment_method,promotion_discount))

	def create_complete_stock_moves(self,cr, uid, quantity,product_id,product_uom,state,origin,location_id,location_dest_id):
		remote_queue.send_task("tasks.create_complete_stock_moves", args=(quantity,product_id,product_uom,state,origin,location_id,location_dest_id))

	def update_amazon(self, cr, uid, update, context=None):
		remote_queue.send_task("tasks.update_amazon", args=('a',update))

	def update_pricelist(self,cr,uid,update_dict,context=None):
		remote_queue.send_task("tasks.update_pricelist",args=([update_dict]))

	def update_myntra(self, cr, uid, update, context=None):
		remote_queue.send_task("tasks.update_myntra", args=('a',update))	

	def update_shipment_state_delhivery(self, cr, uid, update,api_url,is_reverse=False, context=None):
		remote_queue.send_task("tasks.update_shipment_state_delhivery", args=(update,api_url,is_reverse))	
		
	def update_shipment_state_bluedart(self, cr, uid, update,api_url,is_reverse=False, context=None):
		remote_queue.send_task("tasks.update_shipment_state_bluedart", args=(update,api_url,is_reverse))	

	def dispatch_sms_of_scanned_product(self, cr,uid, values, context=None):
		values = [values]
		remote_queue.send_task("tasks.dispatch_sms_of_last_n_minutes", args=([values]))

	def register_pickup_and_generate_awb_nos(self, cr, uid, update_dict,register_pickup=True, context=None):
		remote_queue.send_task("tasks.register_pickup_and_generate_awb_nos", args=(update_dict,register_pickup))
