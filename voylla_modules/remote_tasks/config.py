import os

config = {
			"development":{
							"BROKER": "amqp://"
						},
			"staging":{
						"BROKER": "amqp://worker:worker@localhost:5672/worker_staging"
						},
			"production":{
						"BROKER": "amqp://voylla_odoo:voylla_odoo@localhost:5672/voylla_odoo"
						}
		}

if os.environ.get("ODOO_ENV") == "production":
	SPREE_URL = "http://app-temp-01.voylla.com/api/update_inventory.json"
	SPREE_TOKEN = "aded4bb18bd01df22dcd05997c6c71b6"
elif os.environ.get("ODOO_ENV") == "staging":
	SPREE_URL = "http://beta.voylla.com/api/update_inventory.json"
	SPREE_TOKEN = "2318dfe156f4f90e8f6274fc876d6fb3"
else:
	SPREE_URL = "http://localhost:3000/api/update_inventory.json"
	SPREE_TOKEN = "2318dfe156f4f90e8f6274fc876d6fb3"
