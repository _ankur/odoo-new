from openerp.osv import fields, osv

# Adding Voylla Specific Fields to Product

import csv
import base64#file encode
import urllib2 #file download from url
# Adding Voylla Specific Fields to Supplier
import pdb
class addproduct_image(osv.osv):
	_inherit='product.template'
	def _get_product(self, cr, uid, ids, context=None):
		res={}
		for product in self.pool.get('product.template').browse(cr,uid,ids,context=context):
			res[product.id]=True
		return res.keys()
	def image_upload(self,cr,uid,fields,vals,args,context=None):
		res={}
		k=self.browse(cr,uid,fields)
		link=k[0].link
		if link==False:
			res[k.id]='No'
			return res
		photo = base64.encodestring(urllib2.urlopen(link).read())
		val={'image':photo}
		self.write(cr,uid,fields,val)
		res[k.id]='yes'
		return res
	_columns={
		'web_url':fields.function(image_upload,type='char',method=True,
			store={'product.template':(_get_product,['link'] ,10 ) }),
		'link':fields.char('url')
	}
	def imageupload(self,cr,uid,vals,context=None):
		reader = open('product_import.csv','rb').readlines()
		p=len(reader)
		factor=p/200
		remainder=p%200
		start=0
		end=199
		count=0
		firstline=True
		export_csv=[]
		header=['sku','link','binary']
		export_csv.append(header)
		while( count < factor+1):
			count+=1
			my_list=reader[start : end]
			if count==factor:
				start+=200
				end+=remainder
			else:
				start+=200
				end+=200
			for col in my_list:
				row=col[:-1].split(',') 
				if firstline==True:
					firstline=False
					continue
				a=['']*3
				a[0]=row[0]
				link=a[1]=row[1]
				a[2]=base64.encodestring(urllib2.urlopen(link).read())
				export_csv.append(a)
		k1='image_with_binary.csv'
		with open(k1, "wb") as f:
			writer = csv.writer(f)
			writer.writerows(d)
			f.close()


