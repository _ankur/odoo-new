{
    'name': "Voylla ManuFacturing ",
    'version': "1.0",
    'author': "Devendra Mishra",
    'category': "Tools",
    'depends': ['mrp'],
    'data': [
        'voylla_manufacturing_view.xml',
        'wizard/manufacturing_process_view.xml',
        'mrp_bom.xml'
    ],
    'demo': [],
    'installable': True,
    'auto_install': True,
    'application': True,
}