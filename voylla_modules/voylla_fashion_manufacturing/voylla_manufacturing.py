from openerp.osv import fields, osv
import pdb
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
import base64
import re
from openerp.tools.translate import _
from collections import defaultdict
from datetime import datetime
class voylla_work_order(osv.osv):
	_name='voylla.work.order'
	_description ='Work Order'
	def _bom_orders_count_mo_done(self, cr, uid, ids, name, arg, context=None):
		res = {}
		for product_tmpl_id in self.browse(cr, uid, ids):
			res[product_tmpl_id.id] = len(filter( lambda x:x.state=='done', product_tmpl_id.work_order_line_item))
		return res
	def _bom_orders_count_mo(self, cr, uid, ids, name, arg, context=None):
		res = {}
		for product_tmpl_id in self.browse(cr, uid, ids):
			res[product_tmpl_id.id] = len(filter( lambda x:x.state!='done', product_tmpl_id.work_order_line_item))
		return res
	_columns={
		'name':fields.char('Name'),
		'mo_count': fields.function(_bom_orders_count_mo, string='# Manufacturing Orders', type='integer'),
		'mo_count_done': fields.function(_bom_orders_count_mo_done, string='# Manufacturing Orders', type='integer'),
		'source_document':fields.char('source'),
		'work_order_line_item':fields.one2many('mrp.production','voylla_work_order_id','Manufacturing Order')
	}
	_defaults={
		'name': lambda x, y, z, c: x.pool.get('ir.sequence').get(y, z, 'voylla.work.order') or '/'
	}
	def action_view_mos_done(self, cr, uid, ids, context=None):
		origin = self.browse(cr,uid,ids).work_order_line_item
		origin = filter(lambda x:x.state=='done' ,origin)
		attachment_id = map(lambda x:x.id, origin)
		temp_str='['+','.join(str(x) for x in attachment_id)+']'
		return {
			'domain':"[('id','in',%s)]"%temp_str,
			'actions':'manu_act_product_mrp_production',
			'view_type': 'form',
			'view_mode': 'tree,form',
			'res_model': 'mrp.production',
			'view_id': False,
			'type': 'ir.actions.act_window',
			# 'search_view_id':attachment_id 
		}

	def action_view_mos(self, cr, uid, ids, context=None):
		origin = self.browse(cr,uid,ids).work_order_line_item
		origin = filter(lambda x:x.state!='done' ,origin)
		attachment_id = map(lambda x:x.id, origin)
		temp_str='['+','.join(str(x) for x in attachment_id)+']'
		return {
			'domain':"[('id','in',%s)]"%temp_str,
			'actions':'manu_act_product_mrp_production',
			'view_type': 'form',
			'view_mode': 'tree,form',
			'res_model': 'mrp.production',
			'view_id': False,
			'type': 'ir.actions.act_window',
			# 'search_view_id':attachment_id 
		}
	def _get_act_window_dict(self, cr, uid, name, context=None):
		print name
		mod_obj = self.pool.get('ir.model.data')
		act_obj = self.pool.get('ir.actions.act_window')
		result = mod_obj.xmlid_to_res_id(cr, uid, name, raise_if_not_found=True)
		result = act_obj.read(cr, uid, [result], context=context)[0]
		return result
	def create(self,cr,uid,vals,context=None):
		return super(voylla_work_order , self).create(cr,uid,vals,context)
	def write(self,cr,uid,ids,vals,context=None):
		return super(voylla_work_order,self).write(cr,uid,ids,vals,context)

class mrp_production(osv.osv):
	_inherit='mrp.production'
	_columns={
		'voylla_work_order_id':fields.integer('work order id'),
		'picking_id':fields.many2one('stock.picking','picking')
	}
class stock_picking_qty(osv.osv):
	_name='stock.picking.qty'
	_columns={
		'product_id':fields.many2one('product.product',select=1),
		'stock_picking_id':fields.many2one('stock.picking',select=1),
		'qty_total':fields.integer('qty'),
		'origin':fields.char('source'),
		'unreserved_qty':fields.integer('Unreserved qty'),

	}

class mrp_bom(osv.osv):
	_inherit='mrp.bom'
	_columns={
		'ean':fields.char('EAN', size=13),
		'name':fields.char('Name'),
		'bom_active':fields.boolean('Active Bom'),
		'active':fields.boolean('Active',required=True),
	}
	_defaults= {
		'bom_active' :True
	}

	def copy(self, cr, uid, id, default=None, context=None):
		bom = self.browse(cr, uid, id, context=context)
		default = dict(default or {},
					  name=_("%s") % bom.product_tmpl_id.id)
		return super(mrp_bom, self).copy(cr, uid, id, default, context)
	
	def create(self,cr,uid,vals,context=None):
		product_obj = self.pool.get('product.product')
		if vals['ean'] != False:
			try:
				product_id = product_obj.search(cr,uid,[('ean13','=',vals['ean'])])[0]
				vals['product_id'] = product_id
			except:
				raise osv.except_osv(('Warning!'), ("EAN %s not found")%(vals['ean']))
		return super(mrp_bom, self).create(cr,uid,vals,context)

class import_bom_line(osv.osv):
	_name = 'import.bom.line'
	_columns = {
			'module_file' : fields.binary('Module File'),
	}

	def read_module_file(self,cr,uid,data,context=None):
		csv_data = base64.decodestring(data.module_file)
		chars_to_remove = ['\r', ' ', '\n']
		chars_to_remove_1 = ['\r', '\n']
		rx = "[%s]" %(re.escape(''.join(chars_to_remove)))
		rx_1 = "[%s]" %(re.escape(''.join(chars_to_remove_1)))
		rows = csv_data.split("\n")
		try:
			header = re.sub(rx, '', rows[0]).split(",")
			csv_transfer_details = rows[1:]
			return header,csv_transfer_details,rx_1
		except:
			return False

	def create_finding_product(self,cr,uid,template_name,cost):
		prod_temp_reg = self.pool.get('product.template')
		template_sql = "insert into product_template (uom_id,uom_po_id,type,categ_id,name,active,create_date,write_date,create_uid) values (1,1,'consu',1,'%s','true',now(),now(),1) RETURNING id"%(template_name)
		cr.execute(template_sql)
		tmpl_id = cr.fetchone()
		prod_temp_reg.write(cr,uid,tmpl_id[0],{'standard_price':cost})
		product_sql = "Insert into product_product (name_template,product_tmpl_id,active,create_date,write_date,create_uid,write_uid) values ('%s',%d,'true',now(),now(),1,1) RETURNING id"%(template_name,tmpl_id[0])
		cr.execute(product_sql)
		product_id = cr.fetchone()
		return product_id

	def create_bill_of_material(self,cr,uid,bom_line):
		product_obj = self.pool.get('product.product')
		all_bom_line_objs = defaultdict(list)
		for line in bom_line:
			product_id = product_obj.search(cr,uid,[('name_template','=',line['bom_product'])])
			if not product_id:
				product_id = self.create_finding_product(cr,uid,line['bom_product'],line['cost'])
			
			all_bom_line_objs[product_id[0]].append(line['cost'])
			all_bom_line_objs[product_id[0]].append(line['qty'])
		return all_bom_line_objs

	def import_bom_line(self,cr,uid,ids,context=None):
		product_obj = self.pool.get('product.product')
		mrp_bom_line_reg = self.pool.get('mrp.bom.line')
		mrp_bom_reg = self.pool.get('mrp.bom')
		prod_temp_reg = self.pool.get('product.template')
		data = self.browse(cr, uid, ids[0] , context=context)
		header,csv_transfer_details,rx_1 = self.read_module_file(cr,uid,data,context)
		bom = {}
		if csv_transfer_details == False:
			raise osv.except_osv(('Unexpected Failure'), ('Please try again \n or \n Connect to admin'))
		for product in csv_transfer_details[:-1]:
			lines = dict(zip(header, re.sub(rx_1, '', product).split(",")))
			if lines['product'] in bom:
				bom[lines['product']].append(lines)
			else:
				bom[lines['product']] = [lines]
		for line in bom.keys():
			product_id = product_obj.search(cr,uid,[('name_template','=',line)])
			if not product_id:
				raise osv.except_osv(('Main Product Not Found'), ('%s not found \n Please Check Again')%line)
			all_bom_line_objs = self.create_bill_of_material(cr,uid,bom[line])
			cost = 0
			prod_obj = product_obj.browse(cr,uid,product_id)
			mrp_bom_id = mrp_bom_reg.search(cr,uid,[('product_id','=',product_id[0])])
			if mrp_bom_id:
				raise osv.except_osv(('Warning!!!!'), ('BOM for %s Already exists,Please check manually')%line)
			mrp_bom_id = mrp_bom_reg.create(cr,uid,{'product_qty':1,'name':prod_obj.name_template,'product_tmpl_id':prod_obj.product_tmpl_id.id,'product_id':prod_obj.id,'type':'normal','ean':prod_obj.ean13})
			for obj in all_bom_line_objs.keys():
				cost = cost + int(all_bom_line_objs[obj][0])
				mrp_bom_line_reg.create(cr,uid,{'product_id':obj,'product_uos_qty':1,'bom_id':mrp_bom_id,'product_uom':1,'product_qty': int(all_bom_line_objs[obj][1]),'type':'normal'})
			if not prod_obj.product_tmpl_id.standard_price:
				prod_temp_reg.write(cr,uid,prod_obj.product_tmpl_id.id,{'standard_price':cost})

	def generate_manufacturing_order(self,cr,uid,ids,context=None):
		data = self.browse(cr, uid, ids[0] , context=context)
		product1 = self.pool.get("product.product")
		bom  =  self.pool.get("mrp.bom")
		mrp_production = self.pool.get("mrp.production")
		stock_picking = self.pool.get("stock.picking")
		header,csv_transfer_details,rx_1 = self.read_module_file(cr,uid,data,context)
		if csv_transfer_details == False:
			raise osv.except_osv(('Unexpected Failure'), ('Please try again \n or \n Connect to admin'))
		for product in csv_transfer_details[:-1]:
			mrp_production_vals = {}
			lines = dict(zip(header, re.sub(rx_1, '', product).split(",")))
			stock_picking_id = stock_picking.search(cr,uid,[('origin','=',lines["po_number"]),('state','in',('partially_available','assigned','done','confirmed'))])
			work_order_ids = self.pool.get('voylla.work.order').search(cr,uid,[('source_document','=',lines["po_number"])])
			if len(work_order_ids)==0:
				work_order_id = self.pool.get('voylla.work.order').create(cr,uid,{'source_document':lines["po_number"]})
			else:
				work_order_id=work_order_ids[0]
			product_ids = product1.search(cr,uid,[('ean13' ,'=',lines["ean"])])
			if len(product_ids)==0:
				raise osv.except_osv('error', 'ean13 not found %s' % (lines["ean"]))
			bom_id = bom.search(cr,uid,[('product_id','=',product_ids[0]),('active','=',True)])

			if len(bom_id)!=1:
				raise osv.except_osv('error', 'multiple bom found or no bom found for ean %s' % (lines["ean"]))
			bom_id_obj = bom.browse(cr,uid,bom_id)
			mrp_production_vals.update({'origin':lines["po_number"], 'product_uos_qty': 0,
					 'user_id': uid, 'product_uom': 1, 'date_planned': datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
					  'company_id': 1, 'move_lines': [], 'workcenter_lines': [], 'routing_id': False,
					  'priority': '1', 'bom_id': bom_id_obj.id, 'message_follower_ids': False, 
					  'location_src_id': 88, 'location_dest_id': 89,'product_qty': lines["qty"], 
					  'product_uos': False, 'message_ids': False, 
					   'allow_reorder': False, 'product_id': bom_id_obj.product_id.id,'picking_id':stock_picking_id[0],
					   'voylla_work_order_id':work_order_id})
			mrp_production.create(cr,uid ,mrp_production_vals)
			

		

