from openerp.osv import fields, osv
import pdb
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
class voylla_work_order(osv.osv):
	_name='voylla.work.order'
	_description ='Work Order'
	def _bom_orders_count_mo_done(self, cr, uid, ids, name, arg, context=None):
		res = {}
		for product_tmpl_id in self.browse(cr, uid, ids):
			res[product_tmpl_id.id] = len(filter( lambda x:x.state=='done', product_tmpl_id.work_order_line_item))
		return res
	def _bom_orders_count_mo(self, cr, uid, ids, name, arg, context=None):
		res = {}
		for product_tmpl_id in self.browse(cr, uid, ids):
			res[product_tmpl_id.id] = len(filter( lambda x:x.state!='done', product_tmpl_id.work_order_line_item))
		return res
	_columns={
		'name':fields.char('Name'),
		'mo_count': fields.function(_bom_orders_count_mo, string='# Manufacturing Orders', type='integer'),
		'mo_count_done': fields.function(_bom_orders_count_mo_done, string='# Manufacturing Orders', type='integer'),
		'source_document':fields.char('source'),
		'work_order_line_item':fields.one2many('mrp.production','voylla_work_order_id','Manufacturing Order')
	}
	_defaults={
		'name': lambda x, y, z, c: x.pool.get('ir.sequence').get(y, z, 'voylla.work.order') or '/'
	}
	def action_view_mos_done(self, cr, uid, ids, context=None):
		origin = self.browse(cr,uid,ids).work_order_line_item
		origin = filter(lambda x:x.state=='done' ,origin)
		attachment_id = map(lambda x:x.id, origin)
		temp_str='['+','.join(str(x) for x in attachment_id)+']'
		return {
			'domain':"[('id','in',%s)]"%temp_str,
			'actions':'manu_act_product_mrp_production',
			'view_type': 'form',
			'view_mode': 'tree,form',
			'res_model': 'mrp.production',
			'view_id': False,
			'type': 'ir.actions.act_window',
			# 'search_view_id':attachment_id 
		}

	def action_view_mos(self, cr, uid, ids, context=None):
		origin = self.browse(cr,uid,ids).work_order_line_item
		origin = filter(lambda x:x.state!='done' ,origin)
		attachment_id = map(lambda x:x.id, origin)
		temp_str='['+','.join(str(x) for x in attachment_id)+']'
		return {
			'domain':"[('id','in',%s)]"%temp_str,
			'actions':'manu_act_product_mrp_production',
			'view_type': 'form',
			'view_mode': 'tree,form',
			'res_model': 'mrp.production',
			'view_id': False,
			'type': 'ir.actions.act_window',
			# 'search_view_id':attachment_id 
		}
	def _get_act_window_dict(self, cr, uid, name, context=None):
		print name
		mod_obj = self.pool.get('ir.model.data')
		act_obj = self.pool.get('ir.actions.act_window')
		result = mod_obj.xmlid_to_res_id(cr, uid, name, raise_if_not_found=True)
		result = act_obj.read(cr, uid, [result], context=context)[0]
		return result
	def create(self,cr,uid,vals,context=None):
		return super(voylla_work_order , self).create(cr,uid,vals,context)
	def write(self,cr,uid,ids,vals,context=None):
		return super(voylla_work_order,self).write(cr,uid,ids,vals,context)

class mrp_production(osv.osv):
	_inherit='mrp.production'
	_columns={
		'voylla_work_order_id':fields.integer('work order id'),
		'picking_id':fields.many2one('stock.picking','picking')
	}
class stock_picking_qty(osv.osv):
	_name='stock.picking.qty'
	_columns={
		'product_id':fields.many2one('product.product',select=1),
		'stock_picking_id':fields.many2one('stock.picking',select=1),
		'qty_total':fields.integer('qty'),
		'origin':fields.char('source'),
		'unreserved_qty':fields.integer('Unreserved qty'),

	}

class mrp_bom(osv.osv):
	_inherit='mrp.bom'
	_columns={
		'ean':fields.char('EAN', size=13),
	}

	def create(self,cr,uid,vals,context=None):
		product_obj = self.pool.get('product.product')
		if vals['ean'] != False:
			try:
				product_id = product_obj.search(cr,uid,[('ean13','=',vals['ean'])])[0]
				vals['product_id'] = product_id
			except:
				raise osv.except_osv(('Warning!'), ("EAN %s not found")%(vals['ean']))
		return super(mrp_bom, self).create(cr,uid,vals,context)
