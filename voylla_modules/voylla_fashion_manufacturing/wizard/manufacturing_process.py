from openerp.osv import fields, osv
import pdb
class manufacturing_process(osv.osv):
	_name='manufacturing.process'
	_columns={
		'name':fields.char('name')
	}
	def confirm_production(self,cr,uid,ids,context):
		mrp_production = self.pool.get("mrp.production")
		mrp_product_produce = self.pool.get('mrp.product.produce')
		mrp_ids = context.get('active_ids')
		mrp_production.action_confirm(cr,uid,mrp_ids)
		mrp_production.action_assign(cr,uid,mrp_ids)
		mrp_production.write(cr,uid,mrp_ids,{'state':'ready'})
		mrp_production.browse(cr,uid,mrp_ids)
		for for_production in mrp_production.browse(cr,uid,mrp_ids):
			if for_production.state=='done' and len(for_production.move_created_ids)!=1:
				continue
			product_to_produce = for_production.move_created_ids[0]
			if product_to_produce.remaining_qty==0:
				continue
			temp_vals={'track_production': False, 'product_id': product_to_produce.product_id.id, 'mode': 'consume_produce',
			 'product_qty': product_to_produce.remaining_qty, 'lot_id': False}
			temp_moves_array=[]
			for stock_move in for_production.move_lines:
			 	temp_moves_array.append([0, False, {'lot_id': False, 'product_id': stock_move.product_id.id, 'product_qty': stock_move.availability}]) 
			temp_vals.update({'consume_lines':temp_moves_array})
			ids = mrp_product_produce.create(cr,uid,temp_vals)
			data=mrp_product_produce.browse(cr,uid,ids)
			mrp_production.action_produce(cr, uid, for_production.id,product_to_produce.remaining_qty,'consume_produce', data, context=context)
			for_production.write({'state':'done'})
		return {'type': 'ir.actions.act_window_close'}