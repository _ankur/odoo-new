{
    'name': "Offline Store",
    'version': "1.0",
    'author': "Devendra Mishra",
    'category': "Tools",
    'depends': ['purchase'],
    'data': [
        'security/offline_security.xml',
        'security/ir.model.access.csv',
        'offline_store_view.xml'
    ],
    'demo': [],
    'installable': True,
    'auto_install': True,
    'application': True,
}