from openerp.osv import fields, osv
import pdb
import urllib
import urllib2
import json
from datetime import datetime
import shlex
import subprocess
class offline_feedback(osv.osv):
	_name = "offline.feedback"
	_description = "Offline Feedback"
	def _current_user_partner(self, cursor, user, ids, name, arg, context=None):
		# vals = {'pid': sale_order_objs}  
		res = {}
		cur_user = self.pool.get('res.users')
		cur_user_partner_id = cur_user.search(cursor,user,[('id','=',user)],context=None)
		cur_user_obj=cur_user.browse(cursor, user, cur_user_partner_id)
		for po in self.browse(cursor, user, ids, context=context):
			res[po.id] = po.partner_id
		return res

	def _current_user_partner_search(self, cursor, user, ids, name, arg, context=None):
		res = {}
		cur_user = self.pool.get('res.users')
		cur_user_partner_id = cur_user.search(cursor,user,[('id','=',user)],context=None)
		cur_user_obj=cur_user.browse(cursor, user, cur_user_partner_id)
		return [('partner_id', '=', cur_user_obj.partner_id.id )]
	_columns={
		'product':fields.char('EAN code of product'),
		'type':fields.many2one('product.category','Type of Product'),
		'qty':fields.integer('Quantity required'),
		'Feedback':fields.text('Any feedback'),
	}
class offline_inventory(osv.osv):
	_name='offline.inventory'
	_description='Inventory'
	_columns={
		'product':fields.char('Ean code of product'),
		'qty':fields.integer('Qty available'),
		'replenish':fields.boolean('Replenish Product'),
	}
class add_product_quant(osv.osv):
	_inherit='stock.quant'
	def _test_fnct(self, cr, uid, ids, name, arg, context=None):
		ean_obj=self.browse(cr,uid,ids) 
		res={}
		for k in ean_obj:
			res.update({k.id:1})
		return res

	def _test_fnct_search(self, cr, uid, model,field_name, arg, context=None):
		category_id=self.pool.get('ir.module.category').search(cr,uid,[('name','=','offline Users')])
		cur_user = self.pool.get('res.users')
		cur_user_partner_id = cur_user.search(cr,uid,[('id','=',uid)],context=None)
		cur_user_obj=cur_user.browse(cr, uid, cur_user_partner_id)
		def move_ids(x):
			if len(x.category_id)!=0 and int(x.category_id.id)==int(category_id[0]):
				return x.id
		temp=map(move_ids,cur_user_obj.groups_id)
		location_id=self.pool.get('stock.location').search(cr,uid,[('group_id','=',temp)])
		if len(location_id)!=0:
			return [('location_id','child_of',location_id[0])]
	_columns={
		'offline_inventory':fields.function(_test_fnct,type='integer',string='current inventory',fnct_search=_test_fnct_search,method=True)
	}
class stock_location(osv.osv):
	_inherit='stock.location'
	def get_current_inventory_by_file_name(self,cr,uid,ids,context=None):
		file_name = self.browse(cr,uid,ids).name
		k=datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
		complete_name = str( file_name.replace(" ","") + k)
		#complete_name = file_name + k
		sql= """COPY(select * from get_bin_specific_qty(%s)) to '/tmp/"""+complete_name+""".csv' delimiter ',' csv header"""
		cr.execute(sql,(ids[0],))
		feed=str(complete_name+'.csv')
		FTP_SERVER = "db01.voylla.com"
		FTP_USER = "ubuntu"
		FEED_LOCATION = "/tmp"
		cmd = "scp %s@%s:%s/%s ." %(FTP_USER, FTP_SERVER, FEED_LOCATION,feed)       ####Get the feed from staging
		args = shlex.split(cmd)
		p = subprocess.Popen(args)
		p.wait()
		convert_zip="gzip "+feed
		args = shlex.split(convert_zip)
		p = subprocess.Popen(args)
		p.wait()
		feed= feed+'.gz'
		url_base='https://s3.amazonaws.com/voyllaerp/erp_tag/'+feed
		c1="s3cmd put "+ feed +" s3://voyllaerp/erp_tag/"
		args = shlex.split(c1)
		p = subprocess.Popen(args)
		p.wait()
		c2="s3cmd setacl s3://voyllaerp/erp_tag/"+feed+" --acl-public"
		args = shlex.split(c2)
		subprocess.Popen(args)
		p.wait()
		vals={'name':feed,'type':'url','url':url_base}
		attachment_id=int(self.pool.get('ir.attachment').create(cr,uid,vals))
		#os.remove(home+'/'+feed)
		return {
			'domain':"[('id','=',%i)]"%attachment_id,
			'actions':'check_action_attachment',
			'view_type': 'form',
			'view_mode': 'tree,form',
			'res_model': 'ir.attachment',
			'view_id': False,
			'type': 'ir.actions.act_window',
			'search_view_id':attachment_id 
		}
