from openerp.osv import osv, fields
import pdb

class stock_move_delete(osv.osv):
	_name='stock.move.complete'
	def complete_stock_moves(self, cr, uid, ids, context=None):
		self.pool.get('stock.move').action_done(cr,uid,context['active_ids'],context=context)