from openerp.osv import fields, osv
from datetime import datetime,timedelta
import csv
import pdb
import logging
_logger = logging.getLogger(__name__)
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
class stock_bin_movement(osv.osv):
	_name='stock.bin_movement'
	def divide_stock_move_copy(self, cr, uid, id, locationdestid,qty_for_done,qty_for_pending,context=None):
		stock_move = self.pool.get('stock.move')
		ret = stock_move.copy(cr, uid, id, default=None, context=context)
		stock_move.write(cr,uid,ret,{'state':'assigned','product_uom_qty':qty_for_pending})
		stock_move.write(cr, uid, id, {'location_dest_id':locationdestid,'product_uom_qty':qty_for_done}, context=context)
		return ret
	def identfied_bin(self,cr,uid,ids,context=None):
		if context:
			return {'value':{'locationdestid':int(context.split(',')[0])}}
		return
	def button_dummy(self, cr, uid, ids, context=None):
		return True
	def create_done_equal_qty_movement(self,cr,uid,stock_move_id,locationdestid,context=None):
		sql='update stock_move set location_dest_id=%s where id=%s'
		cr.execute(sql,(locationdestid,stock_move_id,))
	def add_into_queue(self,cr,uid,product_id,locationdestid,qty,context=None):
		queue_stock_bin = self.pool.get('queue.stock_bin')
		vals={}
		vals={'product_id':product_id,'stock_location':locationdestid,'total_qty':qty,'is_active':True}
		queue_stock_bin.create(cr,uid,vals)
	def move_product_to_bin(self,cr,uid,ids,context=None):
		get_identified_bin = self.browse(cr,uid,ids)
		product_info_with_qty =get_identified_bin.stock_transfer_detail
		sku_having_issue =[]
		for product in product_info_with_qty:
			temp = self.search_and_done_stock_move(cr,uid,product.product.id ,get_identified_bin.locationdestid.id,product.qty, get_identified_bin.origin )
		return {
			'actions':'action_product_view',
			'view_type': 'form',
			'view_mode': 'form',
			'res_model': 'stock.bin_movement',
			'view_id': False,
			'type': 'ir.actions.act_window',
			# 'search_view_id':attachment_id 
		}
	def search_and_done_stock_move(self,cr,uid,product_id,location_dest_id,qty,origin,context=None):
		stock_move = self.pool.get('stock.move')
		sql='select id,product_uom_qty from stock_move where location_dest_id=84 and product_id=%s and origin=%s and state in (%s,%s) order by create_date asc limit 1;'
		cr.execute(sql,(product_id,origin,'assigned','confirmed',))
		k = cr.fetchall()
		if len(k)>0:
			if k[0][1] == qty:
				stock_move_id=k[0][0]
				self.create_done_equal_qty_movement(cr,uid,stock_move_id,location_dest_id,context)
				self.add_into_queue(cr,uid,product_id,location_dest_id,qty,context)
				stock_move.action_done(cr,uid,stock_move_id)
			else:
				qty_to_done=qty
				qty_for_pending = k[0][1] - qty
				if qty_for_pending < 0:
					raise osv.except_osv("Error","sku having stock_move is pending")

				self.divide_stock_move_copy(cr,uid,k[0][0],location_dest_id,qty_to_done,qty_for_pending)
				self.add_into_queue(cr,uid,product_id,location_dest_id,qty_to_done,context)
				stock_move.action_done(cr,uid,k[0][0])

	def check_product_to_bin(self,cr,uid,ids,context=None):
		current_object = self.browse(cr,uid,ids)
		product_info_with_qty = current_object.stock_transfer_detail
		sku_having_issue =[]
		for product in product_info_with_qty:
			temp = self.search_stock_move(cr,uid,product.product.id ,product.qty ,current_object.origin)
			if not temp:
				sku_having_issue.append(product.product.name_template)
		if len(sku_having_issue):
			sku_string=','.join(sku_having_issue)
			raise osv.except_osv("Error!"," %s sku having no or less qty stock move " % (sku_string))
		else:
			raise osv.except_osv("fine","all stock move is available please Process")


	def search_stock_move(self,cr,uid,product_id,qty,origin,context=None):
		stock_move = self.pool.get('stock.move')
		sql='select id,product_uom_qty from stock_move where location_dest_id=84 and product_id=%s and origin=%s and state in (%s,%s) order by create_date asc limit 1;'
		cr.execute(sql,(product_id,origin,'assigned','confirmed',))
		k = cr.fetchall()
		if len(k)>0:
			if k[0][1] >= qty:
				return True
			else:
				return False
		else:
			return False
		# stock_move.search(cr,uid,)

	def product_moves_to_box(self,cr,uid,ids,vals,token,context=None):
		product = self.pool.get('product.product')
		transfer_product_bin = self.pool.get('transfer.product_bin')
		if vals:
			product_vals = vals.split(',')
			#if len(vals) > 4:
			#	raise ("vals not complateted")
			ids =[]
			if len(product_vals) == 5 and product_vals[4]!='':
				#raise osv.except_osv("Error","Ean is not present or not in proper format")
				ids = product.search(cr,uid,[('ean13','=',product_vals[4])])
			elif len(product_vals) >= 6 and product_vals[4]!='':
				ids = product.search(cr,uid,[('ean13','=',product_vals[4])])
			if len(ids)==0:
				raise osv.except_osv("error","ean not found")
			already_exist_product = transfer_product_bin.search(cr,uid,[('product','in',ids),('token','=',token)])
			if len(already_exist_product) >0:
				product_id_qty = transfer_product_bin.browse(cr,uid,already_exist_product)
				product_id_qty.write({'qty':int(product_id_qty.qty)+1})
			else:
				transfer_product_bin.create(cr,uid,{'product':ids[0],'token':token,'qty':1})
			transfer_ids = transfer_product_bin.search(cr,uid,[('token','=',token)])
			return {'value':{'stock_transfer_detail':transfer_product_bin.browse(cr,uid,transfer_ids),'productutility':None}}


		return 

	def manufacturing_availbilty(self,cr,uid,ids,context=None):
		product_info_with_qty = self.browse(cr,uid,ids).stock_transfer_detail
		sku_having_issue =[]
		for product in product_info_with_qty:
			sql = "SELECT  SUM(qty.count) as count FROM ( select -1 * sum(product_uom_qty) as count from stock_move  where location_id=84 and state in ('done','assigned') and product_id=%d UNION ALL select sum(product_uom_qty) as count from stock_move  where location_dest_id=84 and state = 'done' and product_id=%d) as qty"%(product.product.id,product.product.id)
			cr.execute(sql)
			temp = cr.fetchall()
			# temp = self.search_stock_move(cr,uid,product.product.id ,product.qty)
			if temp[0][0]<product.qty:
				sku_having_issue.append(product.product.name_template)
		if len(sku_having_issue):
			sku_string=','.join(sku_having_issue)
			raise osv.except_osv("Error!"," %s sku having no or less qty stock move " % (sku_string))
		else:
			return True
		return

	def move_bin_to_box(self,cr,uid,ids,context=None):
		self.manufacturing_availbilty(cr,uid,ids)
		get_identified_bin = self.browse(cr,uid,ids)
		product_info_with_qty =get_identified_bin.stock_transfer_detail
		for product in product_info_with_qty:
			self.create_stock_move(cr,uid,product.product.id ,get_identified_bin.locationdestid.id,product.qty)
			self.add_into_queue(cr,uid,product.product.id ,get_identified_bin.locationdestid.id,product.qty,context)
			# temp = self.search_and_done_stock_move(cr,uid,product.product.id ,get_identified_bin.locationdestid.id,product.qty)
		return {
			'actions':'action_product_view',
			'view_type': 'form',
			'view_mode': 'form',
			'res_model': 'stock.bin_movement',
			'view_id': False,
			'type': 'ir.actions.act_window',
			# 'search_view_id':attachment_id 
		}
	def create_stock_move(self,cr,uid,product_id,destination_location,qty):
		stock_move = self.pool.get('stock.move')
		product = self.pool.get('product.product').browse(cr,uid,product_id).name_template
		c1={}
		c1={'lang': 'en_US', 'tz': False, 'uid': uid, 'invoice_state': None, 'procure_method': 'make_to_stock', 'search_default_future': True}
		val_hash={'state':'confirmed','product_uom':1,'company_id':1,
		'location_dest_id':int(destination_location),'location_id':84,
		'product_id':product_id,'name':product,'origin':'Manufacturing To Box','picking_id':False,
		'date_expected':datetime.now().strftime('%Y-%m-%d %H:%M:%S'),'date':datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
		'product_uom_qty':qty}
		stock_id = stock_move.create(cr, uid,val_hash, context=c1)
		stock_move.action_done(cr,uid,stock_id)
		return


	_columns={
		'locationname':fields.char('location'),
		'origin':fields.char('source',required=True),
		'location_src_id':fields.many2one('stock.location','source location',required=True,select=1,readonly=True),
		'locationdestid':fields.many2one('stock.location','destination location',required=True,select=1),
		'productutility':fields.char('Scan Product'),
		'stock_transfer_detail':fields.one2many('transfer.product_bin','stock_bin_movement_id','product details'),
		'token_number':fields.integer('Token')

	}
	_defaults={
		'location_src_id':84,
		'token_number': lambda x, y, z, c: x.pool.get('token.bin_movement').get_next(y, z, 'generate')
	}

class movement_between_bins(osv.osv):
	_name='movement.between.bins'

	def identfied_bin(self,cr,uid,ids,context=None):
		object_queue=self.pool.get('queue.stock_bin').browse(cr,uid,context).stock_location.id
		if object_queue:
			return {'value':{'location_src_id':int(object_queue)}}
		return

	def identfied_bin1(self,cr,uid,ids,context=None):
		object_queue=self.pool.get('queue.stock_bin').browse(cr,uid,context).stock_location.id
		if object_queue:
			return {'value':{'locationdestid':int(object_queue)}}
		return	

	def button_dummy(self, cr, uid, ids, context=None):
		return True

	def add_into_queue(self,cr,uid,product_id,locationdestid,qty,queue_dest_id,context=None):
		queue_stock_bin = self.pool.get('queue.stock_bin')
		search_queue_dest=queue_stock_bin.search(cr,uid,[('id','=',queue_dest_id),('stock_location','=',locationdestid)])
		dest_qty=queue_stock_bin.browse(cr,uid,search_queue_dest).total_qty
		updated_qty=dest_qty+qty
		# vals={}
		# vals={'product_id':product_id,'stock_location':locationdestid,'total_qty':updated_qty,'is_active':True}
		voucher_id = queue_stock_bin.search(cr,uid,[('id','=',queue_dest_id),('stock_location','=',locationdestid)])
		x = queue_stock_bin.write(cr,uid,voucher_id,{'product_id':product_id,'stock_location':locationdestid,'total_qty':updated_qty,'is_active':True})

	def check_product_to_bin(self,cr,uid,ids,context=None):
		current_object = self.browse(cr,uid,ids)
		product_info_with_qty = current_object.stock_transfer_detail
		sku_having_issue =[]
		source_stock_location=current_object.location_src_id.id
		dest_stock_location=current_object.locationdestid.id
		queue_source_id=current_object.source_id
		queue_dest_id=current_object.destination_id
		queue_stock_bin=self.pool.get('queue.stock_bin')

		search_queue_source=queue_stock_bin.search(cr,uid,[('id','=',queue_source_id),('stock_location','=',source_stock_location)])
		search_queue_dest=queue_stock_bin.search(cr,uid,[('id','=',queue_dest_id),('stock_location','=',dest_stock_location)])
		source_qty=queue_stock_bin.browse(cr,uid,search_queue_source).available_qty
		source_product_id=[]
		source_product_id.append(queue_stock_bin.browse(cr,uid,search_queue_source).product_id.id)
		# dest_qty=queue_stock_bin.browse(cr,uid,search_queue_dest).total_qty
		dest_product_id=queue_stock_bin.browse(cr,uid,search_queue_dest).product_id.id

		product_ids=[]
		for product in product_info_with_qty:
			# temp = self.search_stock_move(cr,uid,product.product.id ,product.qty ,current_object.origin)
			qty_scanned=product.qty
			product_ids.append(product.product.id)
		if queue_source_id==queue_dest_id:
			raise osv.except_osv("Error!","source and destination are same")
		if not source_product_id[0]==dest_product_id:
			raise osv.except_osv("Error!","source and destination does not contain same product")	
		if len(set(product_ids)-set(source_product_id)) !=0:
			raise osv.except_osv("Error!","Scanned product is not in source bin")	
		if source_qty>= qty_scanned:
			raise osv.except_osv("fine","Quantity is available,please Process")
		else:	
			raise osv.except_osv("Error!"," sku having no or less qty ")

	def product_moves_to_box(self,cr,uid,ids,vals,token,context=None):
		product = self.pool.get('product.product')
		transfer_product_bin = self.pool.get('transfer.bin')
		if vals:
			product_vals = vals.split(',')
			ids =[]
			if len(product_vals) == 5 and product_vals[4]!='':
				#raise osv.except_osv("Error","Ean is not present or not in proper format")
				ids = product.search(cr,uid,[('ean13','=',product_vals[4])])
			elif len(product_vals) >= 6 and product_vals[4]!='':
				ids = product.search(cr,uid,[('ean13','=',product_vals[4])])
			if len(ids)==0:
				raise osv.except_osv("error","ean not found")
			already_exist_product = transfer_product_bin.search(cr,uid,[('product','in',ids),('token','=',token)])
			if len(already_exist_product) >0:
				product_id_qty = transfer_product_bin.browse(cr,uid,already_exist_product)
				product_id_qty.write({'qty':int(product_id_qty.qty)+1})
			else:
				id_create=transfer_product_bin.create(cr,uid,{'product':ids[0],'token':token,'qty':1})
			transfer_ids = transfer_product_bin.search(cr,uid,[('token','=',token)])
			return {'value':{'stock_transfer_detail':transfer_product_bin.browse(cr,uid,transfer_ids),'productutility':None}}
		return

	
	def move_bin_to_box(self,cr,uid,ids,context=None):
		access=self.check_product_to_bin_bool(cr,uid,ids)
		if access:
			current_object = self.browse(cr,uid,ids)
			product_info_with_qty = current_object.stock_transfer_detail
			sku_having_issue =[]
			source_stock_location=current_object.location_src_id.id
			dest_stock_location=current_object.locationdestid.id
			queue_source_id=current_object.source_id
			queue_dest_id=current_object.destination_id
			queue_stock_bin=self.pool.get('queue.stock_bin')

			search_queue_source=queue_stock_bin.search(cr,uid,[('id','=',queue_source_id),('stock_location','=',source_stock_location)])
			search_queue_dest=queue_stock_bin.search(cr,uid,[('id','=',queue_dest_id),('stock_location','=',dest_stock_location)])
			source_qty=queue_stock_bin.browse(cr,uid,search_queue_source).total_qty
			dest_qty=queue_stock_bin.browse(cr,uid,search_queue_dest).total_qty
			for product in product_info_with_qty:
				qty_scanned=product.qty
				product_id=product.product.id

			self.create_stock_move(cr,uid,product_id ,dest_stock_location,source_stock_location,qty_scanned,queue_source_id)
			self.add_into_queue(cr,uid,product_id ,dest_stock_location,qty_scanned,queue_dest_id,context)
		return {
			'actions':'action_product_view',
			'view_type': 'form',
			'view_mode': 'form',
			'res_model': 'movement.between.bins',
			'view_id': False,
			'type': 'ir.actions.act_window',
		} 

	def check_product_to_bin_bool(self,cr,uid,ids,context=None):
		current_object = self.browse(cr,uid,ids)
		product_info_with_qty = current_object.stock_transfer_detail
		sku_having_issue =[]
		source_stock_location=current_object.location_src_id.id
		dest_stock_location=current_object.locationdestid.id
		queue_source_id=current_object.source_id
		queue_dest_id=current_object.destination_id
		queue_stock_bin=self.pool.get('queue.stock_bin')

		search_queue_source=queue_stock_bin.search(cr,uid,[('id','=',queue_source_id),('stock_location','=',source_stock_location)])
		search_queue_dest=queue_stock_bin.search(cr,uid,[('id','=',queue_dest_id),('stock_location','=',dest_stock_location)])
		source_qty=queue_stock_bin.browse(cr,uid,search_queue_source).available_qty
		source_product_id=[]
		source_product_id.append(queue_stock_bin.browse(cr,uid,search_queue_source).product_id.id)
		# dest_qty=queue_stock_bin.browse(cr,uid,search_queue_dest).total_qty
		dest_product_id=queue_stock_bin.browse(cr,uid,search_queue_dest).product_id.id

		product_ids=[]
		for product in product_info_with_qty:
			# temp = self.search_stock_move(cr,uid,product.product.id ,product.qty ,current_object.origin)
			qty_scanned=product.qty
			product_ids.append(product.product.id)

		if queue_source_id==queue_dest_id:
			raise osv.except_osv("Error!","source and destination are same")	
		if not source_product_id[0]==dest_product_id:
			raise osv.except_osv("Error!","source and destination does not contain same product")	
		if len(set(product_ids)-set(source_product_id)) !=0:
			raise osv.except_osv("Error!","Scanned product is not in source bin")	
		if source_qty>= qty_scanned:
			return True
		else:	
			raise osv.except_osv("Error!"," sku having no or less qty ")	

	

	def create_stock_move(self,cr,uid,product_id,destination_location,source_stock_location,qty,queue_id):
		stock_move = self.pool.get('stock.move')
		product = self.pool.get('product.product').browse(cr,uid,product_id).name_template
		c1={}
		c1={'lang': 'en_US', 'tz': False, 'uid': uid, 'invoice_state': None, 'procure_method': 'make_to_stock', 'search_default_future': True}
		val_hash={'state':'confirmed','product_uom':1,'company_id':1,
		'location_dest_id':int(destination_location),'location_id':source_stock_location,
		'product_id':product_id,'name':product,'origin':'Box To Box','picking_id':False,
		'date_expected':datetime.now().strftime('%Y-%m-%d %H:%M:%S'),'date':datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
		'product_uom_qty':qty,'queue_stock_bin_id':queue_id}
		stock_id = stock_move.create(cr, uid,val_hash, context=c1)
		stock_move.action_done(cr,uid,stock_id)
		return


	_columns={
		# 'locationname':fields.char('location destination'),
		# 'origin':fields.char('source',required=True),
		'location_src_id':fields.many2one('stock.location','source location',required=True),
		'locationdestid':fields.many2one('stock.location','destination location',required=True),
		'source_id':fields.integer('Source scaleable ID'),
		'destination_id':fields.integer('Destination scaleable ID'),
		'productutility':fields.char('Scan Product'),
		'stock_transfer_detail':fields.one2many('transfer.bin','stock_bin_movement_id','product details'),
		'token_number':fields.integer('Token'),
	}

	_defaults={
		'token_number': lambda x, y, z, c: x.pool.get('token.bin_to_bin_movement').get_next(y, z, 'generate')
	}	

class transfer_bin(osv.osv):
	_name='transfer.bin'
	_columns={
		'stock_bin_movement_id':fields.integer('stock bin movement'),
		'product':fields.many2one('product.product','product',readonly=True,required=True,select=1),
		'qty':fields.integer('product qty',readonly=True),
		'token':fields.integer('token'),
	}	
class token_bin_to_bin_movement(osv.osv):
   	_name='token.bin_to_bin_movement'
	_columns={
		'next_number':fields.integer('number')
		}
	def get_next(self,cr,uid,vals,context=None):
		k = self.browse(cr,uid,1)
		k.write({'next_number':k.next_number+1})
		return k.next_number
	
class transfer_product_bin(osv.osv):
	_name='transfer.product_bin'
	_columns={
		'stock_bin_movement_id':fields.integer('stock bin movement'),
		'product':fields.many2one('product.product','product',readonly=True,required=True,select=1),
		'qty':fields.integer('product qty',readonly=True),
		'token':fields.integer('token'),
	}
class stock_move(osv.osv):
	_inherit='stock.move'
	_columns={
		'queue_stock_bin_id':fields.integer('queue stock bin id')
	}
class queue_stock_bin(osv.osv):
	_name='queue.stock_bin'
	def _calulate_pending_qty(self, cr, uid, ids, name, arg, context=None):
		res = {}
		for queue in self.browse(cr, uid, ids):
			filter_ids = filter( lambda x : x.state in ['confirmed','assigned'], queue.stock_move_id )
			filtered_qty = map( lambda x : x.product_uom_qty ,filter_ids)
			if len(filtered_qty) > 0:
				res[queue.id] = reduce( lambda x,y:x+y,filtered_qty)
			else:
				res[queue.id] = 0
		return res
	def _calulate_done_qty(self, cr, uid, ids, name, arg, context=None):
		res = {}
		for queue in self.browse(cr, uid, ids):
			filter_ids = filter( lambda x : x.state=='done', queue.stock_move_id )
			filtered_qty = map( lambda x : x.product_uom_qty ,filter_ids)
			if len(filtered_qty) >0:
				res[queue.id] = reduce( lambda x,y:x+y,filtered_qty)
			else:
				res[queue.id] = 0
		return res
	def _calulate_available_qty(self, cr, uid, ids, name, arg, context=None):
		res = {}
		for queue in self.browse(cr, uid, ids):
			filter_ids = filter( lambda x : x.state in ['confirmed','assigned'], queue.stock_move_id )
			filtered_qty = map( lambda x : x.product_uom_qty ,filter_ids)
			pending_qty =0
			if len(filtered_qty) > 0:
				pending_qty = reduce( lambda x,y:x+y,filtered_qty)
			done_qty=0
			filter_ids = filter( lambda x : x.state=='done', queue.stock_move_id )
			filtered_qty = map( lambda x : x.product_uom_qty ,filter_ids)
			if len(filtered_qty) > 0:
				done_qty= reduce( lambda x,y:x+y,filtered_qty)
			res[queue.id] = queue.total_qty - pending_qty - done_qty
		return res


	def _is_active(self, cr, uid, ids, name, arg, context=None):
		res = {}
		for queue in self.browse(cr, uid, ids):
			filter_ids = filter( lambda x:x.state=='done', queue.stock_move_id )
			filtered_qty = map( lambda x : x.product_uom_qty ,filter_ids)
			if len(filtered_qty) >0:
				res[queue.id] = reduce( lambda x,y:x+y,filtered_qty)
				k = reduce( lambda x,y:x+y,filtered_qty)
				if queue.total_qty > 0 and queue.total_qty==k:
					res[queue.id] =False
				else:
					res[queue.id]=True
			else:
				res[queue.id]=True 
		return res
	def do_active_true_to_false(self,cr,uid,context=None):
		ids = self.search(cr,uid,[('is_active','=',True),('write_date','>',(datetime.now() - timedelta(minutes=500)).strftime("%Y-%m-%d %H:%M:%S"))])
		to_do_false = []
		for obj in self.browse(cr,uid,ids):
			if obj.total_qty == obj.total_qty:
				to_do_false.append(obj.id)
		self.write(cr,uid,to_do_false,{'is_active':'True'})
	def get_available_qty(self,cr,uid,product_id,context=None):
		ids = self.search(cr,uid,[('product_id','=',product_id)])
		qty = 0
		for queue_item in self.browse(cr,uid,ids,context):
			qty = qty + queue_item.available_qty
		return qty

		 
	_columns={
		'product_id':fields.many2one('product.product','Product',required=True,select=1),
		'total_qty':fields.integer('qty'),
		'pending_qty':fields.function(_calulate_pending_qty,string='pending qty', type='integer'),
		'done_qty':fields.function(_calulate_done_qty,string='Done qty', type='integer'),
		'available_qty':fields.function(_calulate_available_qty,string='Available qty',type='integer'),
		'is_active':fields.function(_is_active,string='Active', type='boolean',store=True),
		'stock_location':fields.many2one('stock.location',required=True,select=1),
		'stock_move_id':fields.one2many('stock.move','queue_stock_bin_id','stock moves')
	}
	_defaults={
		'is_active':True,
		'stock_location':84
	}
class token_bin_movement(osv.osv):
	_name='token.bin_movement'
	_columns={
		'next_number':fields.integer('number')
	}
	def get_next(self,cr,uid,vals,context=None):
		k = self.browse(cr,uid,1)
		k.write({'next_number':k.next_number+1})
		return k.next_number
class sale_order_line(osv.osv):
	_inherit='sale.order.line'
	_columns={
		'bins':fields.one2many('bin.location_qty','line_id','bins')
	}
class bin_location_qty(osv.osv):
	_name='bin.location_qty'
	_columns={
		'location_id':fields.many2one('stock.location','stock location'),
		'qty':fields.integer('Qty'),
		'line_id':fields.integer('line'),
		'product_id':fields.many2one('product.product','Product')
	}
