{
    'name': "Voylla Warehouse",
    'version': "1.0",
    'author': "Voylla Retail Pvt. Ltd.",
    'category': "Tools",
    'depends': ['stock', 'procurement','purchase','crm','sale'],
    'data': ['security/warehouse_security.xml',
        'security/ir.model.access.csv',
        'product_view.xml',
        'product_move.xml',
        'import_stock_movement.xml',
        'complete_stock_move.xml',
        'warehouse_bin_view.xml'
        ],
    'demo': [],
    'installable': True,
    'auto_install': True,
    'application': True,
}