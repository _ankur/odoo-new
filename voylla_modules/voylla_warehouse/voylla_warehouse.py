from openerp.osv import fields, osv
from datetime import datetime,timedelta
import csv
import pdb
import logging
_logger = logging.getLogger(__name__)
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
from voylla_modules.config import home
import base64
import re
import xlwt
import subprocess
import shlex
import os
	
class stock_move(osv.osv):
	_inherit = 'stock.move'

	def correct_stock_moves(self,cr,uid,context=None):
		stock_move_registry = self.pool.get('stock.move')
		sale_order_line_registry = self.pool.get('sale.order.line')
		good_bin_location = 30
		outbound_location = 20
		stock_move_ids = stock_move_registry.search(cr,uid,[('state','=','assigned'),('location_id','=',good_bin_location),('location_dest_id','=',outbound_location)])
		stock_move_obj = stock_move_registry.browse(cr,uid,stock_move_ids)
		for stock_move in stock_move_obj:
			try:
				if isinstance(int(stock_move.name),int):
					sale_order_line_id = sale_order_line_registry.search(cr,uid,[('id','=',stock_move.name)])
					if sale_order_line_id:
						sale_order_line_obj = sale_order_line_registry.browse(cr,uid,sale_order_line_id)
						state = sale_order_line_obj.state
						if state == 'cancel':
							stock_move_registry.action_cancel(cr,uid,stock_move.id,context=context)
							_logger.info(('Cancelled against sale_order_id %s')%(stock_move.name))
					elif not sale_order_line_id:
						stock_move_registry.action_cancel(cr,uid,stock_move.id,context=context)
						_logger.info('Cancelled %s'%(stock_move.name))
			except:
				_logger.info('Not Cancelled %s'%(stock_move.name))
				continue
			

	def qty_upload(self,cr,uid,filename,logfile,context=None):
		out_file = open(logfile,"w")
		with open(filename) as csvfile:
			reader= csv.DictReader(csvfile)
			stock_move_obj = self.pool.get('stock.move')
			product_obj = self.pool.get('product.product')
			name = 'Quantity Upload'
			inventory_loss_location = 36
			good_bin_location = 30

			error_list = []
			error_list1 = []
			error_list2 = []
			c1 = {'lang': 'en_US', 'tz': False, 'uid': uid, 'invoice_state': None, 'procure_method': 'make_to_stock', 'search_default_future': True}
			for row in reader:
				product_id = product_obj.search(cr,uid,[('ean13','=',row['EAN13']),('active','=',True)])
				if len(product_id) == 1:
					_logger.info(product_id)
					try:
						uom_id = product_obj.browse(cr,uid,product_id).product_tmpl_id.uom_id.id
						virtual_quant = product_obj._product_available(cr, uid,
							[product_id[0]],context={'location':good_bin_location,'compute_child':False})[product_id[0]]['virtual_available']
						
						if row['EAN13'] in mod_list1:
							quantity = virtual_quant
							source_location = good_bin_location
							destination_location = inventory_loss_location
						elif int(row['Qntity']) == virtual_quant:
							continue	
						elif int(row['Qntity']) < virtual_quant:
							quantity = virtual_quant - int(row['Qntity'])
							source_location = good_bin_location
							destination_location = inventory_loss_location
						elif int(row['Qntity']) > virtual_quant:
							quantity = int(row['Qntity']) - virtual_quant
							source_location = inventory_loss_location
							destination_location = good_bin_location
						vals2={'state':'assigned','product_uom':uom_id, 
						'company_id':1,'location_id':source_location,'location_dest_id':destination_location,'product_id':product_id[0],
						'name':name,'date_expected': datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT),
						'date':datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT),'product_uom_qty':quantity}
						id_done = stock_move_obj.create(cr, uid,vals2, context=c1)
						stock_move_obj.action_done(cr,uid,id_done)
						cr.commit()
						_logger.info('updated %s with quantity %s \n'%(row['EAN13'],quantity))
						out_file.write('updated %s with quantity %s'%(row['EAN13'],row['Qntity']))
					except:
						error_list2.append(row['EAN13'])
						pass
				elif len(product_id) == 2:
					error_list1.append(row['EAN13'])
				else:
					error_list.append(row['EAN13'])
			_logger.info(('error in stock_move %s \n'%(error_list2),'ean not assigned to any products %s \n' %(error_list),'same ean to multiple products %s \n'%(error_list1)))
			out_file.write(('error in stock_move %s \n'%(error_list2),'ean not assigned to any products %s \n' %(error_list),'same ean to multiple products %s \n'%(error_list1)))
	def push_qty_of_combo_products_to_channel(self,cr,uid,look_back,min_quantity,context=None):
		product_obj = self.pool.get('product.product')
		location = []
		update_list = []
		#reference_list = []
		locations_to_sync = self.pool.get('bins.to.sync').search(cr,uid,[('to_sync','=',True)])
		for loc in locations_to_sync:
			bin_location_id = self.pool.get('bins.to.sync').browse(cr,uid,loc).name.id
			location.append(bin_location_id)
			location = location + self.pool.get('stock.location').search(cr,uid,[('location_id','=',bin_location_id)])
		stock_move_obj = self.pool.get('stock.move')
		check_time = datetime.now() - timedelta(minutes = look_back)
		check_time = check_time.strftime(DEFAULT_SERVER_DATETIME_FORMAT)
		product_template_obj = self.pool.get('product.template')
		combo_products_id = product_template_obj.search(cr,uid,[('is_combos','=',True)])
		combo_products_obj = product_template_obj.browse(cr,uid,combo_products_id)
		for product_template in combo_products_obj:
			reference_list = []
			product_variant = product_template.product_variant_ids[0]
			all_products_in_combo = product_template.variant_product
			product_template_id = product_template.id
			master_ean = product_template.masterean
			selling_price = product_template.list_price
			mrp = product_template.mrp
			product_threep_reference = product_variant.threep_reference
			product_ean = product_variant.ean13
			product_sku = product_template.name
			product_suppliers = product_variant.seller_ids
			leadtime = []
			if product_suppliers:
				for supplier in product_suppliers:
					leadtime.append(supplier.delay)
				if leadtime:
					leadtime = min(leadtime)
			try:
				if int(selling_price) < 501:
					shipping_charge = 50
				else:
					shipping_charge = 0
			except:
				shipping_charge = 2
				pass
			prod_virtual_quant = 10000
			quantity = 0
			available_on='NO'
			ean=''
			_logger.info("combo qty start sync")
			for combo in all_products_in_combo:
				product = combo.product
				ean = product.ean13
				product_qty_in_combo = combo.qty
				stock_move_id =stock_move_obj.search(cr,uid,[('write_date','>=',check_time),'|',('location_dest_id','in',location),('location_id','in',location),])
				available_on='NO'
				if stock_move_id:
					stock_move_browse_obj = stock_move_obj.browse(cr,uid,stock_move_id)
					for stock_move in stock_move_browse_obj:
						available_on='NO'
						if stock_move.location_id.name=='Inbound' and stock_move.picking_id!=False:
							available_on='YES'
						if stock_move.state!='done':
							continue
				if not ean in reference_list:
					total_quantity= 0
					if product.route_ids:
						not_mod=True
						for route in product.route_ids:
							if route.name == 'Make To Order':
								not_mod=False
								quantity = total_quantity + 1000
						if not_mod:
							quantity = self.pool.get('queue.stock_bin').get_available_qty(cr,uid,product.id)
					if quantity < product_qty_in_combo:
						prod_virtual_quant = 0
						continue	
					if quantity < prod_virtual_quant:
						prod_virtual_quant = int(quantity)/int(product_qty_in_combo)
				_logger.info(product_sku)
				_logger.info(prod_virtual_quant)
				_logger.info(ean)
			if prod_virtual_quant == 10000:
				prod_virtual_quant = 0			
			update_list.append({"available_on":str(available_on),"parent_sku":str(master_ean),"selling_price":str(selling_price),
				"shipping_charge":shipping_charge,"mrp":mrp,"sku":product_sku,"ean":str(product_ean),
				"threep_id": str(product_threep_reference),'leadtime':leadtime, "quantity":prod_virtual_quant})
			reference_list.append(ean)
		remote_tasks_obj = self.pool.get('remote.tasks')
		update_list_batches = [update_list[x:x+10] for x in xrange(0, len(update_list), 10)]
		for batch in update_list_batches:
			remote_tasks_obj.update_spree(cr, uid, batch)
			# remote_tasks_obj.update_amazon(cr, uid, batch)
			# remote_tasks_obj.update_myntra(cr, uid, batch)

	def push_mod_qty_to_channels(self,cr,uid,context=None):
		product_obj = self.pool.get('product.product')
		remote_tasks_obj = self.pool.get('remote.tasks')
		product_template_obj = self.pool.get('product.template')
		product_template_ids = product_template_obj.search(cr,uid,[('active','=',True),('flag_complete','=',True)]) 
		product_ids = product_obj.search(cr,uid,[('active','=',True),('ean13','!=',False),('product_tmpl_id','in',product_template_ids)])
		update_list = []
		for product_id in product_ids:
			mod=False
			product_routes = product_obj.browse(cr,uid,product_id).route_ids
			if product_routes:
				for route in product_routes:
					if route.name == 'Make To Order':
						mod = True
			if not mod:
				continue
			_logger.info(product_id)	
			available_on = 'YES'
			ean = product_obj.browse(cr,uid,product_id).ean13
			quantity = 1000
			update_list.append({'ean':ean,'quantity':quantity})
		update_list_batches = [update_list[x:x+10] for x in xrange(0, len(update_list), 10)]
		for batch in update_list_batches:
			_logger.info(batch)
			remote_tasks_obj.update_spree(cr,uid,batch)
					
	def push_qty_to_channels(self,cr,uid,look_back,min_quantity,context=None):
		product_obj = self.pool.get('product.product')
		location = []
		locations_to_sync = self.pool.get('bins.to.sync').search(cr,uid,[('to_sync','=',True)])
		for loc in locations_to_sync:
			bin_location_id = self.pool.get('bins.to.sync').browse(cr,uid,loc).name.id
			location.append(bin_location_id)
			location = location + self.pool.get('stock.location').search(cr,uid,[('location_id','=',bin_location_id)])
		stock_move_obj = self.pool.get('stock.move')
		check_time = datetime.now() - timedelta(minutes = look_back)
		check_time = check_time.strftime(DEFAULT_SERVER_DATETIME_FORMAT)
		stock_move_id = self.pool.get('stock.move').search(cr,uid,[('write_date','>=',check_time),'|',('location_dest_id','in',location),('location_id','in',location)])
		update_list = []
		reference_list = []
		if stock_move_id:
			for stock_move in stock_move_id:
				prod_virtual_quant = 0
				stock_move_browse_obj=stock_move_obj.browse(cr,uid,stock_move)
				product_id = stock_move_browse_obj.product_id.id
				available_on='NO'
				if stock_move_browse_obj.location_id.id==89 and stock_move_browse_obj.picking_id!=False:
					available_on='YES'
					if stock_move_browse_obj.state!='done':
						continue
				_logger.info(product_id)
				product_product_obj = self.pool.get('product.product').browse(cr,uid,product_id)
				product_template_obj = product_product_obj.product_tmpl_id
				product_template_id = product_template_obj.id
				master_ean = product_template_obj.masterean
				selling_price = product_template_obj.list_price
				mrp = product_template_obj.mrp
				product_threep_reference = product_product_obj.threep_reference
				product_ean = product_product_obj.ean13
				product_sku = product_template_obj.name
				product_suppliers = product_product_obj.seller_ids
				leadtime = []
				for supplier in product_suppliers:
					leadtime.append(supplier.delay)
				if leadtime:
					leadtime = min(leadtime)
				try:
					if int(selling_price) < 501:
						shipping_charge = 50
					else:
						shipping_charge = 0
				except:
					shipping_charge = 2
					pass
				if not product_ean in reference_list:
					
					#virtual_quant = product_obj._product_available(cr, uid[product_id],context={'location':loc,'compute_child':True})[product_id]['virtual_available']
					prod_virtual_quant = self.pool.get('queue.stock_bin').get_available_qty(cr,uid,product_id)
					mod=False
					product_routes = product_obj.browse(cr,uid,product_id).route_ids
					if product_routes:
						for route in product_routes:
							if route.name == 'Make To Order':
								mod=True
					if mod:
						prod_virtual_quant = 1000
					if product_template_obj.is_back_order:
						sql = ("""select * from quantity_sync_get_all_product_quantity_of_back_orders() where product_id=%s""")%product_product_obj.id
						cr.execute(sql)
						res_query = cr.fetchall()
						if res_query:
							prod_virtual_quant = res_query[0][0]
							if prod_virtual_quant:
								available_on='YES'
					_logger.info(prod_virtual_quant)
					if prod_virtual_quant < min_quantity:
						update_list.append({"available_on":str(available_on),"parent_sku":str(master_ean),"selling_price":str(selling_price),
							"shipping_charge":shipping_charge,"mrp":mrp,"sku":product_sku,"ean":str(product_ean),
							"threep_id": str(product_threep_reference),'leadtime':leadtime, "quantity":prod_virtual_quant})
						reference_list.append(product_ean)
		remote_tasks_obj = self.pool.get('remote.tasks')
		update_list_batches = [update_list[x:x+10] for x in xrange(0, len(update_list), 10)]
		for batch in update_list_batches:
			remote_tasks_obj.update_spree(cr, uid, batch)
			# remote_tasks_obj.update_amazon(cr,uid,batch)
			# remote_tasks_obj.update_myntra(cr, uid, batch)
			
	def rto_cr(self,cr,uid,filename,context=None):
		with open(filename) as csvfile:
			reader= csv.DictReader(csvfile)
			stock_move_obj = self.pool.get('stock.move')
			product_obj = self.pool.get('product.product')
			out_inv = self.pool.get('outbound.invoice')
			source_location_id = 40
			destination_location_id = 7
			c1 = {'lang': 'en_US', 'tz': False, 'uid': uid, 'invoice_state': None, 'procure_method': 'make_to_stock', 'search_default_future': True}
			for row in reader:
				origin = row['Order No']
				try:
					ean = out_inv.calculate_ean(cr,uid,1,row['SKU Code'],row['Size'],context)
				except:
					print row['SKU Code'],row['Size']
					continue
				product_id = product_obj.search(cr,uid,[('ean13','=',ean),('active','=',True)])
				uom_id = product_obj.browse(cr,uid,product_id).product_tmpl_id.uom_id.id
				name = '%s %s'%(row['Return Type'],row['Processing Date'])
				quantity = int(row['Quantity'])
				vals2={'state':'assigned','product_uom':uom_id, 'origin':origin,
						'company_id':1,'location_id':source_location_id,'location_dest_id':destination_location_id,'product_id':product_id[0],
						'name':name,'date_expected': datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT),
						'date':datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT),'product_uom_qty':quantity}
				try:
					id_done = stock_move_obj.create(cr, uid,vals2, context=c1)
					cr.commit()
				except:
					error_list.append(row)
					_logger.info(prod_virtual_quant)
					continue

	def cancel_rto_cr(self,cr,uid,name,context=None):
		stock_move_obj = self.pool.get('stock.move')
		ids = stock_move_obj.search(cr,uid,[('name','=',name)])
		stock_move_obj.action_cancel(cr,uid,ids,context=context)
		_logger.info('cancelled %s'%(ids))

	def update_selected_eans(self, cr, uid, eans, context=None):
		location = []
		locations_to_sync = self.pool.get('bins.to.sync').search(cr,uid,[('to_sync','=',True)])
		for loc in locations_to_sync:
			location.append(self.pool.get('bins.to.sync').browse(cr,uid,loc).name.id)
		product_obj = self.pool.get("product.product")
		update_list = []
		for loc in location:
			for ean in eans:
				product_id = product_obj.search(cr, uid, [("ean13", "=", ean), ("active", "=", "True")])
				if not product_id:
					continue
				else:
					mod=False
					product_routes = product_obj.browse(cr,uid,product_id).route_ids
					if product_routes:
						for route in product_routes:
							if route.name == 'Make To Order':
								mod=True
					if mod:
						quantity = 1000
					else:
						quantity = product_obj._product_available(cr, uid,
								[product_id[0]],context={'location':loc,'compute_child':False})[product_id[0]]['virtual_available']
					update = {"ean": ean, "quantity": quantity, "available_on": "NO"}
					update_list.append(update)

		remote_tasks_obj = self.pool.get('remote.tasks')
		update_list_batches = [update_list[x:x+10] for x in xrange(0, len(update_list), 10)]
		for batch in update_list_batches:
			remote_tasks_obj.update_spree(cr, uid, batch)


class stock_move_delete(osv.osv):
	_name='stock.move.delete'
	def delete_stock_moves(self, cr, uid, ids, context=None):
		self.pool.get('stock.move').action_cancel(cr,uid,context['active_ids'],context=context)

class stock_location_product(osv.osv_memory):
	_inherit = 'stock.location.product'

	def download_location_inventory(self,cr,uid,ids,context=None):
		location_ids = tuple(context.get('active_ids'))
		if len(context.get('active_ids')) == 1:
			location_ids = "("+str(context.get('active_ids')[0])+")"
		sql = """select t1.name_template as sku,p_value_1.name as size,t1.ean13 as ean,taxon.name as taxon,sl.name as bin,
		t1.count as available ,t2.count as on_hand from 
		(select sum(p_count.count) as count , product.name_template,product.ean13,product.id,p_count.location_id,product.product_tmpl_id 
			from (SELECT  SUM(qty.count) as count, qty.product_id,qty.location_id FROM
				(select -1 * sum(product_uom_qty) as count,product_id,location_id from stock_move where
				location_id in %s and state in ('done','assigned','confirmed') group by product_id,location_id
				UNION ALL
				select sum(product_uom_qty) as count,product_id,location_dest_id as location_id from stock_move where
				location_dest_id in %s and state in ('done','assigned','confirmed') group by product_id,location_dest_id)
as qty group by qty.product_id,qty.location_id) as p_count, 
product_product as product where p_count.product_id = product.id group by product.name_template,product.ean13,product.id,
p_count.location_id,product.product_tmpl_id) as t1 
left join
(select sum(p_count.count) as count, product.name_template,product.ean13,product.id,p_count.location_id,
	product.product_tmpl_id 
	from (SELECT  SUM(qty.count) as count, qty.product_id,qty.location_id FROM
		(select -1 * sum(product_uom_qty) as count,product_id,location_id from stock_move where
		location_id in %s  and state in ('done') group by product_id,location_id
		UNION ALL
		select sum(product_uom_qty) as count,product_id,location_dest_id as location_id from stock_move where
		location_dest_id in %s and state in ('done') group by product_id,location_dest_id) 
as qty group by qty.product_id,qty.location_id) as p_count,
product_product as product where p_count.product_id = product.id group by product.name_template,product.ean13,product.id,
p_count.location_id,product.product_tmpl_id) as t2 on t1.id=t2.id and t1.location_id=t2.location_id
left join stock_location sl on sl.id=t1.location_id left join product_template pt on pt.id=t1.product_tmpl_id
left join product_taxon taxon on taxon.id=pt.taxon 
left join product_attribute_value_product_product_rel p_value on p_value.prod_id = t1.id 
left join product_attribute_value p_value_1 on p_value_1.id=p_value.att_id and p_value_1.attribute_id=1 where coalesce(t1.count,0.0)!=0.0 or coalesce(t2.count,0.0)!=0.0
order by sl.name,t1.name_template asc"""%(str(location_ids),str(location_ids),str(location_ids),str(location_ids))
		cr.execute(sql)
		result = cr.fetchall()
		headers = ['SKU','Size','EAN','Taxon','Bin','Available Qty','Quantity On Hand']
		prod_ind = []
		final_file = []
		if not result:
			return
		location_name = result[0][headers.index('Bin')]
		final_file.append(headers)
		for record in result:
			final_file.append(list(record))
		name1 = 'All_inventory_file_'+datetime.now().strftime('%Y%m%d')+'.xls'
		if location_name and '_A_' in location_name:
			name1 = 'MF_A_'+datetime.now().strftime('%Y%m%d')+'.xls'
		if location_name and '_B_' in location_name:
			name1 = 'MF_B_'+datetime.now().strftime('%Y%m%d')+'.xls'
		if location_name and '_C_' in location_name:
			name1 = 'MF_C_'+datetime.now().strftime('%Y%m%d')+'.xls'
		if location_name and '_D_' in location_name:
			name1 = 'MF_D_'+datetime.now().strftime('%Y%m%d')+'.xls'
		k1=home+'/import/'+name1
		with open(k1, "wb") as f:
			writer = csv.writer(f)
			writer.writerows(final_file)
			f.close()
		c1="s3cmd put "+ k1 +" s3://voyllaerp/erp_tag/"
		args = shlex.split(c1)
		p=subprocess.Popen(args)
		p.wait()
		c2="s3cmd setacl s3://voyllaerp/erp_tag/"+name1+" --acl-public"
		args = shlex.split(c2)
		subprocess.Popen(args)
		p.wait()
		url_base='https://s3.amazonaws.com/voyllaerp/erp_tag/'+name1
		vals={'name':name1,'type':'url','url':url_base}
		attachment_id=int(self.pool.get('ir.attachment').create(cr,uid,vals))
		os.remove(k1)
		return {
			'domain':"[('id','=',%i)]"%attachment_id,
			'actions':'check_action_attachment',
			'view_type': 'form',
			'view_mode': 'tree,form',
			'res_model': 'ir.attachment',
			'view_id': False,
			'type': 'ir.actions.act_window',
			'search_view_id':attachment_id
		}

