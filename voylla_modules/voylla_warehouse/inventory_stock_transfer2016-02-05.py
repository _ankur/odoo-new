from openerp.osv import osv, fields
import pdb
from datetime import datetime
import xlwt
import subprocess
import shlex
import base64
import re

class inventory_stock_transfer(osv.osv):
	_name='inventory.stock.transfer'
	
	def _type_selection(self, cr, uid, context=None):
		return [('draft', 'draft'), ('pending', 'pending'),('done','done')]
	
	_columns={
		'name':fields.char('stock transfer name',required=True),
		'state':fields.selection(_type_selection, string='Type',readonly=True,select=1),
		'notes':fields.text('Description'),
		'destination':fields.many2one('stock.location', 'Destination Location', select=1),
		'inventory_stock_transfer_line':fields.one2many('inventory.stock.transfer.line','inventory_stock_transfer_id','inventory_stock_transfer',track_visibility='always'),
	}
	_defaults={
		'name': lambda x, y, z, c: x.pool.get('ir.sequence').get(y, z, 'inventory.stock.transfer') or '/',
		'state':'draft'
	}

class inventory_stock_transfer_line(osv.osv):
	_name='inventory.stock.transfer.line'
	def _type_selection(self, cr, uid, context=None):
		return [('draft','draft'),('assigned', 'assigned'), ('queued', 'queued'),('done','done')]
	
	_columns={
		'inventory_stock_transfer_id':fields.integer('Inventory stock transfer'),
		'product_id':fields.many2one('product.product','Product',select=1,required=True),
		'product_qty':fields.integer('Qty',states={'draft': [('readonly', False)]}),
		'state':fields.selection(_type_selection, string='Type',readonly=True,select=1),
		'price':fields.float('Unit Price'),
		'inventory_alot_bin':fields.one2many('stn.inventory.allocate.bin','allocate_bin','allocation_track',track_visibility='always')
	}
	_defaults={
		'state':'draft'
	} 
	def create(self,cr,uid,vals,context=None):
		sol = self.pool.get('sale.order.line')
		alot_bin = self.pool.get('stn.inventory.allocate.bin')
		stock_move = self.pool.get('stock.move')
		combo_product = self.pool.get('combo.product')
		product_product = self.pool.get('product.product')
		stn_obj = self.pool.get('inventory.stock.transfer').browse(cr,uid,vals['inventory_stock_transfer_id'])
		destination_location = stn_obj.destination
		product_id = vals.get('product_id')
		product_qty = vals.get('product_qty')
		product = product_product.browse(cr,uid,product_id).name_template
		is_combo = product_product.browse(cr,uid,product_id).product_tmpl_id.is_combos
		if is_combo:
			product_in_combo = combo_product.search(cr,uid,[('qty1','=',product_id)])
			location_with_qty = []
			product_list = []
			for element in product_in_combo:
				combo_obj = combo_product.browse(cr,uid,element).product
				product_id = product_product.browse(cr,uid,combo_obj.id).id
				combo_qty = combo_product.browse(cr,uid,element).qty
				combo_location_with_qty = sol.is_item_available_in_qty(cr,uid,product_id,product_qty*combo_qty,context = None)
				if not combo_location_with_qty:
					location_with_qty = False
					break
				else:
					location_with_qty.append(combo_location_with_qty)
					product_list.append(product_id)
		else:
			location_with_qty = sol.is_item_available_in_qty(cr,uid,product_id,product_qty,context = None)
		flag_assigned = False
		if location_with_qty:
			flag_assigned = True
			vals.update({'state':'assigned'})
			x = super(inventory_stock_transfer_line , self).create(cr,uid,vals,context)
		else:
			vals.update({'state':'queued'})
			x = super(inventory_stock_transfer_line , self).create(cr,uid,vals,context)
		if flag_assigned:
			if is_combo:
				for index,queue in enumerate(location_with_qty):
					for sub_queue in queue:
						value = {'product_id':product_list[index],'move_location':sub_queue[0],'qty':sub_queue[1],'queque_location':sub_queue[2],'allocate_bin':x}
						alot_bin.create(cr,uid,value)
						c1={}
						c1={'lang': 'en_US', 'tz': False, 'uid': uid, 'invoice_state': None, 'procure_method': 'make_to_stock', 'search_default_future': True}
						val_hash={'state':'confirmed','product_uom':1,'company_id':1,
						'location_dest_id':int(destination_location.id),'location_id':sub_queue[0],
						'product_id':product_id,'name':product,'origin':stn_obj.name,'picking_id':False,
						'date_expected':datetime.now().strftime('%Y-%m-%d %H:%M:%S'),'date':datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
						'product_uom_qty':sub_queue[1],'queue_stock_bin_id':sub_queue[2]}
						stock_move.create(cr, uid,val_hash, context=c1)	
			else:
				for queue in location_with_qty:
					value = {'product_id':product_id,'move_location':queue[0],'qty':queue[1],'queque_location':queue[2],'allocate_bin':x}
					alot_bin.create(cr,uid,value)
					c1={}
					c1={'lang': 'en_US', 'tz': False, 'uid': uid, 'invoice_state': None, 'procure_method': 'make_to_stock', 'search_default_future': True}
					val_hash={'state':'confirmed','product_uom':1,'company_id':1,
					'location_dest_id':int(destination_location.id),'location_id':queue[0],
					'product_id':product_id,'name':product,'origin':stn_obj.name,'picking_id':False,
					'date_expected':datetime.now().strftime('%Y-%m-%d %H:%M:%S'),'date':datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
					'product_uom_qty':queue[1],'queue_stock_bin_id':queue[2]}
					stock_move.create(cr, uid,val_hash, context=c1)
		return x

	def onchange_product_for_stn(self,cr,uid,ids,product_id,context=None):
		product_obj = self.pool.get('product.product')
		unit_price = product_obj.browse(cr,uid,product_id).standard_price
		res = {'value':{'product_id':product_id,'price':unit_price}}
		return res


class generating_stn_file_for_warehouse(osv.osv):
	_name = 'generating.stn.file.for.warehouse'
	def generate_stn_move(self,cr,uid,ids,context=None):
		product_obj = self.pool.get('product.product')
		stock_transer_line_obj = self.pool.get('inventory.stock.transfer.line')
		book = xlwt.Workbook(encoding="utf-8")
		sheet = book.add_sheet("STN File")
		stock_transfer = self.pool.get('inventory.stock.transfer').browse(cr,uid,context['active_ids'])
		stn_name = stock_transfer.name
		style = xlwt.easyxf('font: name Times New Roman, bold on; align: wrap on,vert centre;')
		sheet.write_merge(0, 2, 0, 3,'Picklist Note:%s \n DATE : %s' %(stn_name,datetime.now()), style)
		col =3
		sheet.write(col,0,'SKU',style)
		sheet.write(col,1,'Size',style)
		sheet.write(col,2,'Taxon',style)
		sheet.write(col,3,'Qty',style)
		sheet.write(col,4,'Box No',style)
		stock_transer_line = stock_transfer.inventory_stock_transfer_line
		for stock_line in stock_transer_line:
			if stock_line.state == 'queued':
				continue
			assigned_queue_bin = stock_line.inventory_alot_bin
			for box in assigned_queue_bin:
				size,taxon = self.get_size_and_taxon(cr,uid,box.product_id)
				sku = box.product_id.name_template
				box_no = box.queque_location.stock_location.complete_name
				col += 1
				sheet.write(col,0,sku)
				sheet.write(col,1,size)
				sheet.write(col,2,taxon)
				sheet.write(col,3,box.qty)
				sheet.write(col,4,box_no)
		book.save('stock_transfer_file.xls')
		feed='stock_transfer_file.xls'
		c1="s3cmd put "+ feed +" s3://voyllaerp/erp_tag/"
		args = shlex.split(c1)
		p = subprocess.Popen(args)
		p.wait()
		c2="s3cmd setacl s3://voyllaerp/erp_tag/"+feed+" --acl-public"
		args = shlex.split(c2)
		subprocess.Popen(args)
		p.wait()
		url_base = 'https://s3.amazonaws.com/voyllaerp/erp_tag/'+feed
		vals={'name':feed,'type':'url','url':url_base}
		attachment_id=int(self.pool.get('ir.attachment').create(cr,uid,vals))
		return {
			'domain':"[('id','=',%i)]"%attachment_id,
			'actions':'check_action_attachment',
			'view_type': 'form',
			'view_mode': 'tree,form',
			'res_model': 'ir.attachment',
			'view_id': False,
			'type': 'ir.actions.act_window',
			'search_view_id':attachment_id
		}

	def get_size_and_taxon(self,cr,uid,product_id):
		size_sql = "select name from product_attribute_value where id in (select att_id from product_attribute_value_product_product_rel where prod_id = %d)"%(product_id.id)
		cr.execute(size_sql)
		size = cr.fetchall()
		taxon_sql = "select taxon from get_product_id_taxon() where product_id=%d"%(product_id.product_tmpl_id)
		cr.execute(taxon_sql)
		taxon = cr.fetchall()
		if not size and not taxon:
			size = None
			taxon = None
			return size,taxon
		if not size:
			size = None
			return size,taxon[0][0]
		if not taxon:
			taxon = None
			return size[0],taxon
		else:
			return size[0],taxon[0][0]

class import_inventory_stock_transfer(osv.osv):
	_name='import.inventory.stock.transfer'
	_columns={
		'location_id':fields.many2one('stock.location','Destination', required=True),
		'note':fields.text('Note'),
		'module_file':fields.binary('Module File'),
	}
	def import_stock_transfer(self, cr, uid, ids, context=None):
		stock_location = self.pool.get('stock.location')
		product_product = self.pool.get('product.product')
		stock_transfer = self.pool.get('inventory.stock.transfer')
		data = self.browse(cr, uid, ids[0] , context=context)
		csv_data = base64.decodestring(data.module_file)

		chars_to_remove = ['\r', ' ', '\n']
		chars_to_remove_1 = ['\r', '\n']
		rx = "[%s]" %(re.escape(''.join(chars_to_remove)))
		rx_1 = "[%s]" %(re.escape(''.join(chars_to_remove_1)))
		rows = csv_data.split("\n")
		try:
			header = re.sub(rx, '', rows[0]).split(",")
			csv_transfer_details = rows[1:]
		except:
			pass
		destination = data.location_id.id
		note = data.note
		transfer_dict = {}
		vals = []
		for index,stock in enumerate(csv_transfer_details):
			if stock == '':
				continue
			stock_details = dict(zip(header, re.sub(rx_1, '', stock).split(",")))
			try:
				product_id = product_product.search(cr,uid,[('ean13','=',stock_details['ean'])])[0]
			except:
				raise osv.except_osv(('No Such EAN'), ('%s Not Found \n Please check again')%stock_details['ean'])
			price = product_product.browse(cr,uid,product_id)[0].standard_price
			product_qty = int(stock_details['qty'])
			vals.append([0,False,{'price':price,'product_id':product_id,'product_qty':product_qty}])
		value = {'create_uid':uid,'create_date':datetime.now().strftime('%Y-%m-%d %H:%M:%S'),'notes':note,'destination':destination,
			'inventory_stock_transfer_line':vals,'write_uid':uid}
		context = {'lang': 'en_US', 'tz': 'Asia/Kolkata', 'uid': uid}
		stock_transfer.create(cr,uid,value,context)
		return

class inventory_allocate_bin_for_stn(osv.osv):
	_name='stn.inventory.allocate.bin'
	_columns = {
		'allocate_bin':fields.integer('Allocate Bin'),
		'product_id':fields.many2one('product.product','Product'),
		'move_location':fields.many2one('stock.location','From Location'),
		'qty':fields.integer('Qyt'),
		'queque_location':fields.many2one('queue.stock_bin','Queue Box No'),
	}