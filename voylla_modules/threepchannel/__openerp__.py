{
    'name': "3P Channel",
    'version': "1.0",
    'author': "Prateek",
    'category': "Tools",
    'depends': ['crm','product','purchase'],
    'data': [
        'security/threepchannel_security.xml',
        'crm_lead_view.xml',
        'product_pricelist_view.xml',
        'security/ir.model.access.csv',
        'failure_notification_view.xml'
        
        
    ],
    'demo': [],
    'installable': True,
    'auto_install': True,
    'application': True,
}