from openerp.osv import osv, fields

class channel_mapping(osv.osv):

	_name = "channel.mapping"

	_columns = {
		# 'name':
		# 'channel': fields.many2one("crm.tracking.source","Chanel Name"),
		# 'voylla_threep_number': fields.one2many('product.product',),
		# 'three_p_number': fields.one2many('product.product','threep_reference'),
		'channel': fields.many2one("crm.tracking.source","Chanel Name"),
		'voylla_threep_number': fields.text('Voylla Threep Number'),
		'three_p_number': fields.text('Threep Number'),
	}