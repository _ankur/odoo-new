from openerp.osv import osv, fields


class product_pricelist(osv.osv):

	_inherit = "product.pricelist"

	_columns = {
		"sale_type": fields.selection([
						("default", "Default"),
						("promotional", "Promotional"),
					], "Sale Type")
	}