from openerp.osv import osv, fields


class quantity_update_failure(osv.osv):

	_name = "quantity.update.failure"
	_columns = {
		"name":fields.char("SKU"),
		"type": fields.selection([
			("wrong_value", "Wrong Value"),
			("error_from_amazon", "Error From Amazon"),
			], "Error Type"),
		"description":fields.text('Description'),
		"channel":fields.char("Channel"),
	}
	_sql_constraints = [
        ('unique_sku_channel', 'unique(name,channel)', 'SKU already added')
        ]