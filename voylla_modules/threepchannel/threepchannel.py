from openerp.osv import fields, osv
import pdb
import base64
from datetime import datetime,timedelta
import logging
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
import re
from openerp.tools.translate import _
from openerp.exceptions import except_orm
# from addons.crm import crm_lead

_logger = logging.getLogger(__name__)

class threep_channel(osv.osv):
        _name ='threep.channel'
        _description = 'Threepchannel'

        _columns={
        'task': fields.char('Task',size=120, required=True),
        'date': fields.date('Date',required=True),
        'notes': fields.text('Notes'),
        }

class crm_detail(osv.Model):
    _name = "crm.detail"
    _description = "Channels"
    _columns = {
    'Task owner name': fields.char('Task owner name', required=True),
        'date': fields.date('Date', required=True),
    'status': fields.boolean('Status'),
        }
class three_p_channel(osv.osv):
        _name="three.p.channel"
        _columns = {
        'name':fields.char('Channels', required=True),
        }

# class add_crm_lead(osv.osv):

#       _inherit="crm.lead"
#       _columns = {
#       'threePchannels':fields.many2many('three.p.channel','many2many_sale_persons','channels_id','regts_id','Channels', select=True),
#       'salesperson':fields.many2many('res.users','many2many_saleperson_user','persons_id','users_id','Salesperson', select=True),
#       }


# class add_crm_lead(osv.osv):

#   _inherit="crm.lead"
#   _columns = {
#   'threePchannels':fields.many2many('three.p.channel','many2many_sale_persons','channels_id','regts_id','Channels', select=True),
#   'salesperson':fields.many2many('res.users','many2many_saleperson_user','persons_id','users_id','Salesperson', select=True),
#   }

class add_crm_tracking(osv.osv):
        _inherit="product.pricelist"
        _columns={
        'crmtracking': fields.many2one('crm.tracking.source','Source'),

        }

class add_crm_lead(osv.osv):

        _inherit="crm.lead"

        # def _get_default_section_id(self, cr, uid, user_id=False, context=None):
        #     # import pdb
        #     # pdb.set_trace()
        #     """ Gives default section by checking if present in the context """
        #     section_id = self._resolve_section_id_from_context(cr, uid, context=context) or False
        #     if not section_id:
        #         section_id = self.pool.get('res.users').browse(cr, uid, user_id or uid, context).default_section_id.id or False
        #         return section_id

        def on_change_salesperson(self, cr, uid, ids, salesperson, context=None):
            # import pdb
            # pdb.set_trace()
            temp=salesperson[0][2]
            for salesperson in temp:
                """ When changing the user, also set a section_id or restrict section id
                to the ones user_id is member of. """
                section_id = self._get_default_section_id(cr, uid, user_id=salesperson, context=context) or False
                if salesperson and not section_id:
                    section_ids = self.pool.get('crm.case.section').search(cr, uid, ['|', ('user_id', '=', salesperson), ('member_ids', '=', salesperson)], context=context)
                    if section_ids:
                        section_id = section_ids[0]
                return {'value': {'section_id': section_id}}   

        # def _resolve_section_id_from_context(self, cr, uid, context=None):
        #          """ Returns ID of section based on the value of 'section_id'
        #          context key, or None if it cannot be resolved to a single
        #          Sales Team."""
        #          import pdb
        #          pdb.set_trace()
        #          if context is None:
        #             context = {}
        #             if type(context.get('default_section_id')) in (int, long):
        #                 return context.get('default_section_id')
        #                 if isinstance(context.get('default_section_id'), basestring):
        #                     section_ids = self.pool.get('crm.case.section').name_search(cr, uid, name=context['default_section_id'], context=context)
        #                     if len(section_ids) == 1:
        #                         return int(section_ids[0][0])
        #                 return None 


        def onchange_stage_id(self, cr, uid, ids, stage_id, context=None):
                if not stage_id:
                        return {'value': {}}
                all_child_id = self.pool.get('crm.lead').search(cr,uid,[('parent_id','=',ids[0])])
                children_obj = self.pool.get('crm.lead').browse(cr,uid,all_child_id)
                stage = self.pool.get('crm.case.stage').search(cr,uid,[('name','=','Completed')])
                if stage_id == stage[0]:
                        check=True
                        for child in children_obj:
                                if child.stage_id.name != 'Completed':
                                        check = False
                        if check == False:
                                raise osv.except_osv(('Warning!'), ("All subtasks are not completed"))
                stage = self.pool.get('crm.case.stage').browse(cr, uid, stage_id, context=context)
                if not stage.on_change:
                        return {'value': {}}
                vals = {'probability': stage.probability}
                if stage.probability >= 100 or (stage.probability == 0 and stage.sequence > 1):
                        vals['date_closed'] = fields.datetime.now()
                return {'value': vals}

        _columns = {
        'categ_ids': fields.many2many('crm.case.categ', 'crm_lead_category_rel', 'lead_id', 'category_id', 'Channels', \
            domain="['|', ('section_id', '=', section_id), ('section_id', '=', False), ('object_id.model', '=', 'crm.lead')]"),
        'threepchannels':fields.many2many('crm.tracking.source','many2many_sale_persons','channels_id','regts_id','Channels', select=True),
        'parent_id': fields.many2one('crm.lead','Parent Task'),
        'salesperson': fields.many2many('res.users','many2many_saleperson_user','persons_id','users_id','Assign To', select=True, track_visibility='onchange'),
        'status_id' : fields.selection([ ('catalog','Catalog'), ('pricing','Pricing'),('review','Review'),('pricesync','PriceSync'),('qtysync','QtySync'),('template','Template'),('misc','Misc'),('promoprice','PromoPrice')],'Status', select=True,),
        'threePchannels':fields.many2many('three.p.channel','many2many_sale_persons','channels_id','regts_id','Channels', select=True),
        'assign_by':fields.many2one('res.users','Assign By')
        }



class pricelist_items_import(osv.osv):
    _name = "pricelist.items.import"

    _columns = {
        "name": fields.char("Name"),
        'module_file': fields.binary('Module file'),
        "pricelist_version_id": fields.many2one("product.pricelist.version", "Pricelist Version"),
    }

    def import_pricelist_items(self, cr, uid, ids, vals, context=None):
        pricelist_obj = self.pool.get("product.pricelist")
        pricelist_version_obj = self.pool.get("product.pricelist.version")
        pricelist_item_obj = self.pool.get("product.pricelist.item")
        product_obj = self.pool.get("product.product")

        pricelist_import = self.browse(cr, uid, ids[0], context=context)
        pricelist_version = pricelist_import.pricelist_version_id
        existing_items = pricelist_version.items_id
        existing_items_id = [existing_item.id for existing_item in existing_items]

        #pricelist_item_obj.unlink(cr, uid, existing_items_id)
        csv_data = base64.decodestring(pricelist_import.module_file)

        chars_to_remove = ['\r', '\n']
        chars_to_remove_1 = ['\r', '\n']
        rx = "[%s]" %(re.escape(''.join(chars_to_remove)))

        _logger.info(csv_data)
        rows = csv_data.split("\n")
        try:
            header = re.sub(rx, '', rows[0]).split(",")
            csv_price_details = rows[1:]
        except:
            raise osv.except_osv(_('Warning!'), _("The csv file is incorrect!\nPlease check and retry."))
        order_details_dict = {}

        for order in csv_price_details:
            if order == '':
                continue

            price_details = dict(zip(header, re.sub(rx, '', order).split(",")))
            threep_reference = price_details.get("ThreeP SKU")
            selling_price = price_details.get("Selling Price")

            if threep_reference is None or selling_price is None:
                raise osv.except_osv(_('Warning!'), _("'Selling Price' or 'ThreeP SKU' is None."))

            product_id = product_obj.search(cr, uid, [("threep_reference", "=", threep_reference)])
            if not product_id:
                print "%s not found!" %(threep_reference)
            else:
                product_id = product_id[0]
                product = product_obj.browse(cr, uid, product_id)
                list_price = product.product_tmpl_id.list_price
                name = product.name_template
                surchange = float(selling_price) - list_price

                vals = {'price_round': 0, 'price_min_margin': 0, 'name': name, 'sequence': 5, 'price_max_margin': 0, 'product_tmpl_id': False, 
                'base': 1, 'price_discount': 0, 'price_version_id': pricelist_version.id, 'min_quantity': 0, 'price_surcharge': surchange, 'categ_id': False, 'product_id': product_id}
                pricelist_item_obj.create(cr, uid, vals)
                # print vals
