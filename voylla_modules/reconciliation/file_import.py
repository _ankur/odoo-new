from openerp.osv import osv, fields
import pdb
import base64
from datetime import datetime,timedelta
import logging
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
import re
# import xlrd
# from xlrd import open_workbook
# from xlrd import open_workbook,XL_CELL_TEXT
from datetime import date,datetime,time
# from xlrd import open_workbook,xldate_as_tuple
# from list_excel import get_excel_files
import os
import os.path
# import pandas as pd
import inspect
import glob
from openerp.tools.translate import _



_logger = logging.getLogger(__name__)

class file_import(osv.osv):
	_name = 'file.import'
	

	_columns = {
	'ref_no':fields.char("Reference Number"),
	'module_file':fields.binary("Module File"),
	'top':fields.selection([('cod','COD'),('prepaid','Prepaid'),('threep','Threep')],'Payment Source',select=True,),
	'payment_partner':fields.selection([('payu','PayU'),('paypal','Paypal')],'Payment Partner'),
	'courier':fields.selection([('courier','Courier')],'Payment Courier'),
	'journal_id':fields.many2one("account.journal","Journal",required=True),
	}

	def generate_reconcile_entries(self, cr, uid,Service_Tax,Service_Fee,Amount,Net_Amount,Transaction_ID,Requested_Action,context=None,):
		if context['top'] != 'prepaid':
			raise osv.except_osv(('Warning!'), ("Type of payment is not prepaid"))
		trans_id = str(Transaction_ID)
		status = str(Requested_Action)
		amount_actual = float(Amount)

		# Needs revision in case of partial shipment
		sale_order_obj = self.pool.get('sale.order')
		# transaction_id = sale_order_id.search(cr,uid,[('awb','=',trans_id)])
		# sale_obj_id = sale_order_id.browse(cr,uid,transaction_id)
		sale_order_payment_id = self.pool.get('sale.order.payment').search(cr,uid,[('payment_reference', '=',trans_id)])
		account_invoice_refund = self.pool.get('account.invoice.refund')
		account_invoice_confirm = self.pool.get('account.invoice.confirm')
		
		if len(sale_order_payment_id) ==0:
			raise osv.except_osv(('Warning!'), ("payment reference or awb number %s not found or type of payment is not prepaid")%(Transaction_ID))
		else:
			voucher_id_list = []
			for sale_id in sale_order_payment_id:
				sale_object = self.pool.get('sale.order.payment').browse(cr,uid,sale_id).order_id
				inv=self.pool.get('sale.order').browse(cr,uid,sale_object.id).invoice_ids
				date = inv.date_due
				invoice_name = inv.reference
				order_number = inv.reference
				account_id = inv.account_id.id
				date_original = inv.date_invoice
				amount_original = inv.amount_total
				if amount_actual > amount_original:
						raise osv.except_osv(('Warning!'),("Amount entered is greater then actual invoice amount or invoice is not generated for payment reference no. %s")%(Transaction_ID))
				partner_id = inv.partner_id.id
				currency_id = inv.currency_id.id
				try:
					move_id = inv.move_id.line_id[0].id
					period_id=inv.period_id.id
					state = inv.state
					amount_unreconciled = inv.residual
					account = self.pool.get('account.account').search(cr,uid,[('name','=','Bank')])
					account_obj_id = self.pool.get('account.account').browse(cr,uid,account).id
					voucher=self.pool.get('account.voucher')
					journal_id =  context['journal_id']
					top = context['type']
					payment_partner = context['payment_partner']

					amount = float(Service_Tax) + float(Service_Fee) + float(Net_Amount)
					
				except:
					raise osv.except_osv(('Warning!'), ("Invoice is not generated"))


				if status =='refund':
					if state != 'paid':
						amount =- float(Net_Amount)
						vals1 ={'lang': 'en_US', 'tz': False, 'uid': uid,'reference':order_number, 'active_model': 'account.invoice', 'journal_type': 'sale', 'search_disable_custom_filters': True,
						'active_ids': [inv.id], 'type': 'out_invoice', 'active_id':inv.id}

						refund_id = account_invoice_refund.create(cr,uid,vals1,context=None)
						if refund_id:
							z = account_invoice_refund.invoice_refund(cr,uid,[refund_id],context=vals1)
							# y = account_invoice_confirm.invoice_confirm(cr,uid,refund_id,context=vals1)
					elif state == 'paid':
						amount =- float(Net_Amount)
						vals1 ={'lang': 'en_US', 'tz': False, 'uid': uid,'reference':order_number, 'active_model': 'account.invoice', 'journal_type': 'sale', 'search_disable_custom_filters': True,
						'active_ids': [inv.id], 'type': 'out_invoice', 'active_id':inv.id}
						refund_id = account_invoice_refund.create(cr,uid,vals1,context=None)
						
						# refund_invoice = account_invoice_refund.invoice_refund(cr,uid,context1['active_id'],context1)
						if refund_id:
							y = account_invoice_refund.invoice_refund(cr,uid,[refund_id],context=vals1)
							vals2 ={'lang': 'en_US', 'tz': False, 'uid': uid,'reference':order_number, 'active_model': 'account.invoice', 'journal_type': 'sale', 'search_disable_custom_filters': True,
							'active_ids': y['refund_id'], 'type': 'out_invoice', 'active_id':y['refund_id']}
							z = account_invoice_confirm.invoice_confirm(cr,uid,[refund_id],context=vals2)
					return z
				
				# journal_id = values['journal_id']
				vals={'comment': 'Write-Off', 'line_cr_ids': [[0, False, {'date_due': date, 'account_id': account_id, 
				'date_original': date_original,'reference':order_number,'move_line_id': move_id, 'amount_original': amount_original, 
				'amount': amount,'service_tax':Service_Tax,'service_fees':Service_Fee,'payment_partner':payment_partner,'top':top,'net_amount':Net_Amount, 'amount_unreconciled': amount_unreconciled}]], 'payment_option': 'without_writeoff', 
				'name': False, 'reference':order_number, 'line_dr_ids': [], 'type': 'receipt', 'writeoff_acc_id': False, 'journal_id': journal_id, 
				'is_multi_currency': False, 'company_id': inv.account_id.company_id.id, 'analytic_id': False,
				'pre_line': True, 'amount': amount,'service_tax':Service_Tax,'service_fees':Service_Fee,'payment_partner':payment_partner,'top':top,'net_amount':Net_Amount, 'period_id': period_id, 'payment_rate_currency_id': currency_id, 'narration': False, 
				'date': date, 'payment_rate': 1, 'partner_id': partner_id, 'account_id': account_obj_id}
				voucher_id = voucher.create(cr, uid, vals,context=None)
				voucher_id_list.append(voucher_id)
				for voucher_id in voucher_id_list:
					if voucher_id:
						z=voucher.button_proforma_voucher(cr,uid,[voucher_id],context=None)
						
						x = super(file_import, self).create(cr, uid, vals, context=context)
				if context['type'] == 'prepaid':
					self.write(cr,uid,x,{'top':'prepaid'})

				return x

	def generate_reconcile_cod_entries(self, cr, uid,Amount,Waybill,PaymentDate,context=None,):
		if context['top'] != 'cod':
			raise osv.except_osv(('Warning!'), ("Type of payment is not Cash on Delivery"))
		trans_id = str(Waybill)
		amount_actual = float(Amount)
		
		# Needs revision in case of partial shipment
		sale_order_id = self.pool.get('sale.order')
		# transaction_id = sale_order_id.search(cr,uid,[('awb','=',trans_id)])
		# sale_obj_id = sale_order_id.browse(cr,uid,transaction_id)
		# try:
		sale_order_payment_id = self.pool.get('sale.order').search(cr,uid,[('awb', '=',trans_id)])
		if len(sale_order_payment_id) ==0:
			raise osv.except_osv(('Warning!'), ("payment reference or awb number %s not found")%(Waybill))
		elif context['type'] != 'cod':
			raise osv.except_osv(('Warning!'), ("payment source is not COD"))
		else:
			voucher_id_list = []
			for sale_id in sale_order_payment_id:
				sale_id = sale_id	
				#will change in case of partial shipment
				inv=self.pool.get('sale.order').browse(cr,uid,sale_id).invoice_ids
				date = inv.date_due
				order_number = inv.reference
				account_id = inv.account_id.id
				date_original = inv.date_invoice
				amount_original = inv.amount_total
				if amount_actual > amount_original:
					raise osv.except_osv(('Warning!'),("Amount entered is greater then actual invoice amount or invoice is not generated for awb no. %s")%(Waybill))
				partner_id = inv.partner_id.id
				currency_id = inv.currency_id.id
				amount = float(Amount)
				if amount > amount_original:
					raise osv.except_osv(('Warning!'),("Amount entered is greater then actual invoice amount or invoice is not generated for awb no. %s")%(Waybill))
				try:
					move_id = inv.move_id.line_id[0].id
					period_id=inv.period_id.id
					amount_unreconciled = inv.residual
					payment_courier = context['payment_courier']
					# service_tax = values['service_tax']
					# percentage = self.pool.get('account.tax').browse(cr,uid,x[0][2]).amount
					# if context['type'] =='threep':
					# 	amount = values['amount_collected'] + values['marketing_fees']+ values['shipping_charges'] + values['fba_charges'] + values['payment_fees']
					# 	pass
					# if context['type'] =='prepaid':

				except:
					raise osv.except_osv(('Warning!'), ("Invoice is not generated"))
				
				# journal_id = values['journal_id']
				account = self.pool.get('account.account').search(cr,uid,[('name','=','Bank')])
				account_obj_id = self.pool.get('account.account').browse(cr,uid,account).id
				voucher=self.pool.get('account.voucher')
				journal_id =  context['journal_id']
				top = context['type']
				date_payment = PaymentDate
				date_pay = datetime.strptime(date_payment,"%d/%m/%Y")
				# date_pay = date_pay.strftime('%d/%m/%y')
			
				vals={'comment': 'Write-Off', 'line_cr_ids': [[0, False, {'date_due': date, 'account_id': account_id, 
				'date_original': date_original,'move_line_id': move_id, 'amount_original': amount_original, 
				'amount': amount,'top':top,'payment_courier':payment_courier,'payment_date':date_pay ,'amount_unreconciled': amount_unreconciled}]], 'payment_option': 'without_writeoff', 
				'name': False, 'reference': order_number, 'line_dr_ids': [], 'type': 'receipt', 'writeoff_acc_id': False, 'journal_id': journal_id, 
				'is_multi_currency': False, 'company_id': inv.account_id.company_id.id, 'analytic_id': False,
				'pre_line': True, 'amount': amount,'payment_courier':payment_courier,'payment_date':date_pay, 'top':top,'period_id': period_id, 'payment_rate_currency_id': currency_id, 'narration': False, 
				'date': '2015-02-13', 'payment_rate': 1, 'partner_id': partner_id, 'account_id': account_obj_id}
				voucher_id = voucher.create(cr, uid, vals,context=None)

				voucher_id_list.append(voucher_id)
				for voucher_id in voucher_id_list:
					if voucher_id:
						z=voucher.button_proforma_voucher(cr,uid,[voucher_id],context=None)
						for record in inv:
							record.signal_workflow('shipment_delivered')
						x = super(file_import, self).create(cr, uid, vals, context=context)

				if context['type'] == 'cod_courier':
					self.write(cr,uid,x,{'top':'cod'})
				return x

	def get_codcharges(self,cr,uid,Courier, AWBNumber,OnwardsShippingCharges,OnwardsCODCharges,OnwardsLocalTaxes,OnwardsShippingTax,OnwardsCODTax,ReturnShippingCharges,ReturnCODCharges,ReturnShippingTax,ReturnCODTax, context=None):
		trans_id = str(AWBNumber)
		inv = self.pool.get('sale.order')
		sale_order_payment_id = self.pool.get('sale.order').search(cr,uid,[('awb', '=',trans_id)])
		account = self.pool.get('account.account').search(cr,uid,[('name','=','Bank')])
		account_obj_id = self.pool.get('account.account').browse(cr,uid,account).id
		account_voucher_registry = self.pool.get('account.voucher')
		for sale_id in sale_order_payment_id:
			sale_id = sale_id	
			#will change in case of partial shipment
			inv=self.pool.get('sale.order').browse(cr,uid,sale_id).invoice_ids
			date = inv.date_due
			order_number = inv.reference
			# voucher_id = voucher.create(cr, uid, vals,context=None)
			voucher_id = account_voucher_registry.search(cr,uid,[('reference', '=',order_number)])
			# z=account_voucher_registry.button_proforma_voucher(cr,uid,voucher_id,context=None)
			x = account_voucher_registry.write(cr,uid,voucher_id,{'onwards_shipping_charges':OnwardsShippingCharges,'onwards_cod_charges':OnwardsCODCharges,'onwards_shipping_tax':OnwardsShippingTax,'onwards_cod_tax':OnwardsCODTax,'onwards_local_tax':OnwardsLocalTaxes,'return_shipping_charges':ReturnShippingCharges,'return_cod_charges':ReturnCODCharges,'return_shipping_tax':ReturnShippingTax,'return_cod_tax':ReturnCODTax,'courier':Courier})
			if context['type'] == 'cod_courier':
				self.write(cr,uid,x,{'top':'cod'})
			return x



		_columns = {
		"name" : fields.char("Reference/Waybill", required=True),
		"top" : fields.selection([('prepaid','Prepaid'),('cod','COD'),('threep','Threep')],'Type of Payment',select=True,),
		"sub_order_number" : fields.char("Order Number"),
		"amount_collected" : fields.float("Amount Collected",required=True),
		"journal_id":fields.many2one("account.journal", "Journal",required=True),
		"marketing_fees":fields.float("Marketing Fees"),
		"payment_fees":fields.float("Payment Fees"),
		"shipping_charges":fields.float("Shipping Charges"),
		"fba_charges":fields.float("FBA Charges"),
		"service_tax":fields.float("Service Tax"),
		# "service_tax":fields.many2many('account.tax','order_account_tax_rec','account_line_tax','service_tax1','Service Tax'),
		"service_fees":fields.float("Service Fees"),
		"channels":fields.selection([ ('amazon','Amazon'), ('flipkart','Flipkart'),('ebay','eBay'),('snapdeal','Snapdeal'),('jabong','Jabong'),('paytm','Paytm'),('limeroad','Limeroad'),('fashion&u','Fashion & U'),],'Channels', select=True,)
		}

	def import_file_charges(self,cr,uid,ids,context=None):
		data = self.browse(cr, uid, ids[0] , context=context)
		csv_data = base64.decodestring(data.module_file)
			
		file_creation_registry = self.pool.get("account.move.line")
		chars_to_remove = ['\r', ' ', '\n']
		chars_to_remove_1 = ['\r', '\n']
		rx = "[%s]" %(re.escape(''.join(chars_to_remove)))
		rx_1 = "[%s]" %(re.escape(''.join(chars_to_remove_1)))
		print csv_data
		rows = csv_data.split("\n")

		try:
			header = re.sub(rx, '', rows[0]).split(",")
			csv_prepaid_details = rows[1:]
		except:
			pass 		
 		prepaid_details = {}
 		
		for prepaid in csv_prepaid_details:
			if prepaid == '':
				continue
			prepaid_details = dict(zip(header, re.sub(rx_1, '', prepaid).split(",")))
			move_id1= self.get_codcharges(cr,uid,prepaid_details['Courier'],prepaid_details['AWBNumber'],prepaid_details['OnwardsShippingCharges'],prepaid_details['OnwardsCODCharges'],prepaid_details['OnwardsLocalTaxes'],prepaid_details['OnwardsShippingTax'],prepaid_details['OnwardsCODTax'],prepaid_details['ReturnShippingCharges'],prepaid_details['ReturnCODCharges'],prepaid_details['ReturnShippingTax'],prepaid_details['ReturnCODTax'],context=context)

	
	def import_file(self,cr,uid,ids,context=None):

		data = self.browse(cr, uid, ids[0] , context=context)
		csv_data = base64.decodestring(data.module_file)
			
		file_creation_registry = self.pool.get("account.move.line")
		chars_to_remove = ['\r', ' ', '\n']
		chars_to_remove_1 = ['\r', '\n']
		rx = "[%s]" %(re.escape(''.join(chars_to_remove)))
		rx_1 = "[%s]" %(re.escape(''.join(chars_to_remove_1)))
		print csv_data
		rows = csv_data.split("\n")

		try:
			header = re.sub(rx, '', rows[0]).split(",")
			csv_prepaid_details = rows[1:]
		except:
			pass 		
 		prepaid_details = {}
 		
		for prepaid in csv_prepaid_details:
			if prepaid == '':
				continue
			prepaid_details = dict(zip(header, re.sub(rx_1, '', prepaid).split(",")))
			move_id1= self.generate_reconcile_entries(cr,uid,prepaid_details['ServiceTax'],prepaid_details['ServiceFee'],prepaid_details['Amount'],prepaid_details['NetAmount'],prepaid_details['TransactionID'],prepaid_details['RequestedAction'],context=context)

	def import_file_cod(self,cr,uid,ids,context=None):
		data = self.browse(cr, uid, ids[0] , context=context)
		csv_data = base64.decodestring(data.module_file)
			
		file_creation_registry = self.pool.get("account.move.line")
		chars_to_remove = ['\r', ' ', '\n']
		chars_to_remove_1 = ['\r', '\n']
		rx = "[%s]" %(re.escape(''.join(chars_to_remove)))
		rx_1 = "[%s]" %(re.escape(''.join(chars_to_remove_1)))

		print csv_data
		rows = csv_data.split("\n")

		try:
			header = re.sub(rx, '', rows[0]).split(",")
			csv_cod_details = rows[1:]
		except:
			pass
 		cod_details = {}
 		
		for cod in csv_cod_details:
			if cod == '':
				continue
			cod_details = dict(zip(header, re.sub(rx_1, '', cod).split(",")))
			move_id2= self.generate_reconcile_cod_entries(cr,uid,cod_details['Amount'],cod_details['Waybill'],cod_details['PaymentDate'], context=context)


	

class account_voucher(osv.osv):
	_inherit='account.voucher'

	def create(self, cr, uid, values, context=None):
		print values		
		x = super(account_voucher, self).create(cr, uid, values, context=context)
		return x

	def button_proforma_voucher(self,cr,uid,ids,context=None):
		if context is None:
			context = {}
			# self.signal_workflow(cr, uid, ids, 'proforma_voucher')
			proxy = self.pool['account.invoice']
			for record in proxy.browse(cr,uid,ids,context=context):
				state = record.state
				if state == 'draft':
					self.signal_workflow(cr, uid, ids, 'proforma_voucher')
					record.signal_workflow('shipment_delivered')
					return {'type': 'ir.actions.act_window_close'}
		else:
			active_ids = context.get('active_ids', []) or []

			proxy = self.pool['account.invoice']
			for record in proxy.browse(cr, uid, active_ids, context=context):
				state =record.state
				if state == 'draft':
					self.signal_workflow(cr, uid, ids, 'proforma_voucher')
					# record.signal_workflow('shipment_intransit')
					
					# x = self.write(cr, uid, ids, { 'state' : 'cancel'})
				elif state == 'open':
					self.signal_workflow(cr, uid, ids, 'invoice_refunded')
					record.signal_workflow('invoice_refunded')
					record.signal_workflow('shipment_return')

					return {'type': 'ir.actions.act_window_close'}
				else:
					return False

class account_voucher_line(osv.osv):
	_inherit='account.voucher.line'

	def create(self, cr, uid, values, context=None):
		print values
		x = super(account_voucher_line, self).create(cr, uid, values, context=context)
		return x

class account_move_line(osv.osv):
	_inherit='account.move.line'

	def create(self, cr, uid, values, context=None):
		print values
		move_id = values['move_id']
		account_id = values['account_id']
		partner_id = values['partner_id']
		period_id = values['period_id']
		debit = values['debit']
		credit = values['credit']
		journal_id = values['journal_id']
		date = values['date']
		vals = {'account_id':account_id, 'currency_id': False, 'period_id': period_id, 'date': date, 'partner_id': partner_id,
		 'move_id': move_id, 'analytic_account_id': False, 'name': '/', 'journal_id': journal_id, 'credit': credit, 'debit': debit,'tax_amount':credit,'amount_currency': False}
		x = super(account_move_line, self).create(cr, uid, vals, context=context)
		return x
	_columns={
	"service_tax":fields.many2one('account.tax','Service Tax'),
	"service_fees":fields.float('Service Fees'),
	}

class account_move(osv.osv):
	_inherit = 'account.move'

	def create(self,cr,uid,values,context=None):
		print values
		journal_id = values['journal_id']
		x = super(account_move,self).create(cr,uid,values,context=context)

		return x

class account_invoice_refund(osv.osv):
	_inherit = 'account.invoice.refund'
	
	def create(self,cr,uid,values,context=None):
		print values
		x = super(account_invoice_refund, self).create(cr,uid,values,context=context)

		return x

	def invoice_refund(self, cr, uid, ids, context=None):
		data_refund = self.read(cr, uid, ids, ['filter_refund'],context=context)[0]['filter_refund']
		return self.compute_refund(cr, uid, ids, data_refund, context=context)

	def compute_refund(self, cr, uid, ids, mode='refund', context=None):
		inv_obj = self.pool.get('account.invoice')
		reconcile_obj = self.pool.get('account.move.reconcile')
		account_m_line_obj = self.pool.get('account.move.line')
		mod_obj = self.pool.get('ir.model.data')
		act_obj = self.pool.get('ir.actions.act_window')
		inv_tax_obj = self.pool.get('account.invoice.tax')
		inv_line_obj = self.pool.get('account.invoice.line')
		res_users_obj = self.pool.get('res.users')
		if context is None:
			context = {}

		for form in self.browse(cr, uid, ids, context=context):
			created_inv = []
			date = False
			period = False
			description = False
			company = res_users_obj.browse(cr, uid, uid, context=context).company_id
			journal_id = form.journal_id.id
			for inv in inv_obj.browse(cr, uid, context.get('active_ids'), context=context):
				if inv.state in ['draft', 'proforma2', 'cancel']:
					raise osv.except_osv(_('Error!'), _('Cannot %s draft/proforma/cancel invoice.') % (mode))
				if inv.reconciled and mode in ('cancel', 'modify'):
					raise osv.except_osv(_('Error!'), _('Cannot %s invoice which is already reconciled, invoice should be unreconciled first. You can only refund this invoice.') % (mode))
				if form.period.id:
					period = form.period.id
				else:
					period = inv.period_id and inv.period_id.id or False

				if not journal_id:
					journal_id = inv.journal_id.id

				if form.date:
					date = form.date
					if not form.period.id:
							cr.execute("select name from ir_model_fields \
											where model = 'account.period' \
											and name = 'company_id'")
							result_query = cr.fetchone()
							if result_query:
								cr.execute("""select p.id from account_fiscalyear y, account_period p where y.id=p.fiscalyear_id \
									and date(%s) between p.date_start AND p.date_stop and y.company_id = %s limit 1""", (date, company.id,))
							else:
								cr.execute("""SELECT id
									from account_period where date(%s)
									between date_start AND  date_stop  \
									limit 1 """, (date,))
							res = cr.fetchone()
							if res:
							    period = res[0]
				else:
					date = inv.date_invoice
				if form.description:
					description = form.description
				else:
					description = inv.name

				if not period:
						raise osv.except_osv(_('Insufficient Data!'), \
											_('No period found on the invoice.'))

				refund_id = inv_obj.refund(cr, uid, [inv.id], date, period, description, journal_id, context=context)
				refund = inv_obj.browse(cr, uid, refund_id[0], context=context)
				inv_obj.write(cr, uid, [refund.id], {'date_due': date,
												'check_total': inv.check_total})
				inv_obj.button_compute(cr, uid, refund_id)
				created_inv.append(refund_id[0])
				if mode in ('cancel', 'modify'):
					movelines = inv.move_id.line_id
					to_reconcile_ids = {}
					for line in movelines:
						if line.account_id.id == inv.account_id.id:
							to_reconcile_ids.setdefault(line.account_id.id, []).append(line.id)
						if line.reconcile_id:
							line.reconcile_id.unlink()
							refund.signal_workflow('invoice_open')
							refund = inv_obj.browse(cr, uid, refund_id[0], context=context)
							for tmpline in  refund.move_id.line_id:
								if tmpline.account_id.id == inv.account_id.id:
									to_reconcile_ids[tmpline.account_id.id].append(tmpline.id)
							for account in to_reconcile_ids:
								account_m_line_obj.reconcile(cr, uid, to_reconcile_ids[account],
											writeoff_period_id=period,
											writeoff_journal_id = inv.journal_id.id,
											writeoff_acc_id=inv.account_id.id
											)
					if mode == 'modify':
						invoice = inv_obj.read(cr, uid, [inv.id],
									['name', 'type', 'number', 'reference',
									'comment', 'date_due', 'partner_id',
									'partner_insite', 'partner_contact',
									'partner_ref', 'payment_term', 'account_id',
									'currency_id', 'invoice_line', 'tax_line',
									'journal_id', 'period_id'], context=context)
						invoice = invoice[0]
						del invoice['id']
						invoice_lines = inv_line_obj.browse(cr, uid, invoice['invoice_line'], context=context)
						invoice_lines = inv_obj._refund_cleanup_lines(cr, uid, invoice_lines, context=context)
						tax_lines = inv_tax_obj.browse(cr, uid, invoice['tax_line'], context=context)
						tax_lines = inv_obj._refund_cleanup_lines(cr, uid, tax_lines, context=context)
						invoice.update({
							'type': inv.type,
							'date_invoice': date,
							'state': 'draft',
							'number': False,
							'invoice_line': invoice_lines,
							'tax_line': tax_lines,
							'period_id': period,
							'name': description
						})
						for field in ('partner_id', 'account_id', 'currency_id',
										'payment_term', 'journal_id'):
								invoice[field] = invoice[field] and invoice[field][0]
						inv_id = inv_obj.create(cr, uid, invoice, {})
						if inv.payment_term.id:
							data = inv_obj.onchange_payment_term_date_invoice(cr, uid, [inv_id], inv.payment_term.id, date)
							if 'value' in data and data['value']:
								inv_obj.write(cr, uid, [inv_id], data['value'])
						created_inv.append(inv_id)
						refund_id = inv_id

                        # context = context.update({'refund_id':inv_id})

			xml_id = (inv.type == 'out_refund') and 'action_invoice_tree1' or \
					(inv.type == 'in_refund') and 'action_invoice_tree2' or \
					(inv.type == 'out_invoice') and 'action_invoice_tree3' or \
					(inv.type == 'in_invoice') and 'action_invoice_tree4'
			result = mod_obj.get_object_reference(cr, uid, 'account', xml_id)
			id = result and result[1] or False

			result = act_obj.read(cr, uid, [id], context=context)[0]
			invoice_domain = eval(result['domain'])
			invoice_domain.append(('id', 'in', created_inv))
			result['domain'] = invoice_domain
			result['refund_id'] = refund_id
			return result

class account_invoice(osv.osv):
	_inherit = 'account.invoice'

	def action_lost(self):
		for inv in self:
			if inv.state == 'cancel':
				self.write(cr,uid,ids,{'shipping_state':'intransit'})
				return True

	def action_cancelled(self):
		for inv in self:
			if inv.state == 'cancel':
				self.write({'shipping_state':'cancelled'})
				return True

	def action_return(self):
		for inv in self:
			if inv.state == 'refund':
				self.write({'shipping_state':'return'})
				return True

	def action_intransit(self):
		# moves = self.env['account.move']
		for inv in self:
			if inv.state == 'open':
				self.write(cr,uid,ids,{'shipping_state':'intransit'})
				return True

	def action_delivered(self):
		for inv in self:
			if inv.state == 'paid':
				self.write({'shipping_state':'delivered'})
				return True

	def action_refunded(self):
		moves = self.env['account.move']
		for inv in self:
			if inv.move_id:
				moves += inv.move_id
			if inv.payment_ids:
				for move_line in inv.payment_ids:
					if move_line.reconcile_partial_id.line_partial_ids:
						raise except_orm(_('Error!'), _('You cannot cancel an invoice which is partially paid. You need to unreconcile related payment entries first.'))

		self.write({'state': 'refund', 'move_id': False})
		self._log_event(-1.0, 'Refund Invoice')
		return True

	def action_cancel(self):
		moves = self.env['account.move']
		for inv in self:
			if inv.move_id:
				moves += inv.move_id
			if inv.payment_ids:
				for move_line in inv.payment_ids:
					if move_line.reconcile_partial_id.line_partial_ids:
						raise except_orm(_('Error!'), _('You cannot cancel an invoice which is partially paid. You need to unreconcile related payment entries first.'))
		# First, set the invoices as cancelled and detach the move ids
		self.write({'state': 'cancel', 'move_id': False})
		self.write({'shipping_state':'cancelled', 'move_id':False})
		if moves:
			# second, invalidate the move(s)
			moves.button_cancel()
			# delete the move this invoice was pointing to
			# Note that the corresponding move_lines and move_reconciles
			# will be automatically deleted too
			moves.unlink()
		self._log_event(-1.0, 'Cancel Invoice')
		return True

	_columns = {
	'shipping_state':fields.selection([
		('intransit','In Transit'),
		('lost','Lost'),
		('delivered','Delivered'),
		('return','Return'),
		('cancelled','Cancelled'),
		],string='Shipping Status',index=True, readonly=True, default='intransit',
		track_visibility='onchange', copy=False),
	'state' : fields.selection([
			('draft','Draft'),
			('proforma','Pro-forma'),
			('proforma2','Pro-forma'),
			('open','Open'),
			('paid','Paid'),
			('cancel','Cancelled'),
			('refund','Refunded'),
		], string='Status', index=True, readonly=True, default='draft',
		track_visibility='onchange', copy=False,
		help=" * The 'Draft' status is used when a user is encoding a new and unconfirmed Invoice.\n"
				" * The 'Pro-forma' when invoice is in Pro-forma status,invoice does not have an invoice number.\n"
				" * The 'Open' status is used when user create invoice,a invoice number is generated.Its in open status till user does not pay invoice.\n"
				" * The 'Paid' status is set automatically when the invoice is paid. Its related journal entries may or may not be reconciled.\n"
				" * The 'Cancelled' status is used when user cancel invoice.")
	}

		
		
		





		


	
		
