{
    'name': "Reconciliation",
    'version': "1.0",
    'author': "Sudeep Mathur,Prateek",
    'category': "Tools",
    'depends': ['account','sale'],
    'data': [
        'view.xml',
        'file_import.xml',
        'security/reconciliation_security.xml',
        'invoice_workflow.xml',
        'shipping_status_workflow.xml',
        # 'affilate_invoice.xml',
        # 'sale_report_gen.xml',
        # 'payment_report.xml'
        ],
    'update_xml': [],
    'demo': [],
    'installable': True,
    'auto_install': False,
    'application': True,
}