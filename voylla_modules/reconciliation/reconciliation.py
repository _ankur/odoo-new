from openerp.osv import fields, osv
import pdb

class reconciliation_cod(osv.osv):
	_name = "reconciliation.cod"

	def create(self, cr, uid, values, context=None):
		
		# Needs revision in case of partial shipment
		if context['type'] == 'cod_courier':
			sale_id=self.pool.get('sale.order').search(cr,uid,[('awb','=',values['name'])])
		if context['type'] == 'threep':
		    sale_order_payment_id = self.pool.get('sale.order.payment').search(cr,uid,[('payment_reference', '=',values['name'])])
		    sale_id = self.pool.get('sale.order.payment').browse(cr,uid,sale_order_payment_id).order_id.id	
		elif context['type'] == 'prepaid':
			sale_order_payment_id = self.pool.get('sale.order.payment').search(cr,uid,[('payment_reference','=',values['name'])])
			sale_id = self.pool.get('sale.order.payment').browse(cr,uid,sale_order_payment_id).order_id.id
			#will change in case of partial shipment
		inv=self.pool.get('sale.order').browse(cr,uid,sale_id).invoice_ids[0]
		date = inv.date_due
		account_id = inv.account_id.id
		date_original = inv.date_invoice
		amount_original = inv.amount_total
		partner_id = inv.partner_id.id
		currency_id = inv.currency_id.id
		move_id = inv.move_id.line_id[0].id
		period_id=inv.period_id.id
		amount_unreconciled = inv.residual
		# service_tax = values['service_tax']
		# percentage = self.pool.get('account.tax').browse(cr,uid,x[0][2]).amount
		if context['type'] =='threep':
			amount = values['amount_collected'] + values['marketing_fees']+ values['shipping_charges'] + values['fba_charges'] + values['payment_fees']
			pass
		if context['type'] =='prepaid':
		    amount = values['amount_collected'] + values['service_fees'] + values['service_tax']
		else:
		    amount = values['amount_collected']
		journal_id = values['journal_id']
		account = self.pool.get('account.account').search(cr,uid,[('name','=','Bank')])
		account_obj_id = self.pool.get('account.account').browse(cr,uid,account).id
		voucher=self.pool.get('account.voucher')
		vals={'comment': 'Write-Off', 'line_cr_ids': [[0, False, {'date_due': date, 'account_id': account_id, 
		'date_original': date_original,'move_line_id': move_id, 'amount_original': amount_original, 
		'amount': amount, 'amount_unreconciled': amount_unreconciled}]], 'payment_option': 'without_writeoff', 
		'name': False, 'reference': False, 'line_dr_ids': [], 'type': 'receipt', 'writeoff_acc_id': False, 'journal_id': journal_id, 
		'is_multi_currency': False, 'company_id': inv.account_id.company_id.id, 'analytic_id': False,
		'pre_line': True, 'amount': amount, 'period_id': period_id, 'payment_rate_currency_id': currency_id, 'narration': False, 
		'date': '2015-02-13', 'payment_rate': 1, 'partner_id': partner_id, 'account_id': account_obj_id}
		voucher_id = voucher.create(cr, uid, vals,context=None)
		if voucher_id:
			z=voucher.button_proforma_voucher(cr,uid,[voucher_id],context=None)
		x = super(reconciliation_cod, self).create(cr, uid, values, context=context)

		if context['type'] == 'cod_courier':
			self.write(cr,uid,x,{'top':'cod'})
		if context['type'] == 'prepaid':
			self.write(cr,uid,x,{'top':'prepaid'})
		if context['type'] == 'threep':
		    self.write(cr,uid,x,{'top':'threep'})	
		return x


	_columns = {
		"name" : fields.char("Reference/Waybill", required=True),
		"top" : fields.selection([('prepaid','Prepaid'),('cod','COD'),('threep','Threep')],'Type of Payment',select=True,),
		"sub_order_number" : fields.char("Order Number"),
		"amount_collected" : fields.float("Amount Collected",required=True),
		"journal_id":fields.many2one("account.journal", "Journal",required=True),
		"marketing_fees":fields.float("Marketing Fees"),
		"payment_fees":fields.float("Payment Fees"),
		"shipping_charges":fields.float("Shipping Charges"),
		"fba_charges":fields.float("FBA Charges"),
		"service_tax":fields.float("Service Tax"),
		# "service_tax":fields.many2many('account.tax','order_account_tax_rec','account_line_tax','service_tax1','Service Tax'),
		"service_fees":fields.float("Service Fees"),
		"channels":fields.selection([ ('amazon','Amazon'), ('flipkart','Flipkart'),('ebay','eBay'),('snapdeal','Snapdeal'),('jabong','Jabong'),('paytm','Paytm'),('limeroad','Limeroad'),('fashion&u','Fashion & U'),],'Channels', select=True,)
        }

class reconciliation_prepaid(osv.osv):
	_name = "reconciliation.prepaid"

	def create(self, cr, uid, values, context=None):
		
		# Needs revision in case of partial shipment
		if context['type'] == 'cod_courier':
			sale_id=self.pool.get('sale.order').search(cr,uid,[('awb','=',values['name'])])
		if context['type'] == 'threep':
		    sale_order_payment_id = self.pool.get('sale.order.payment').search(cr,uid,[('payment_reference', '=',values['name'])])
		    sale_id = self.pool.get('sale.order.payment').browse(cr,uid,sale_order_payment_id).order_id.id	
		elif context['type'] == 'prepaid':
			sale_order_payment_id = self.pool.get('sale.order.payment').search(cr,uid,[('payment_reference','=',values['name'])])
			for sale_id in sale_order_payment_id:
				sale_id = sale_id
			#will change in case of partial shipment
				sale_object = self.pool.get('sale.order.payment').browse(cr,uid,sale_id).order_id
				inv=self.pool.get('sale.order').browse(cr,uid,sale_object.id).invoice_ids[0]
				date = inv.date_due
				account_id = inv.account_id.id
				date_original = inv.date_invoice
				amount_original = inv.amount_total
				partner_id = inv.partner_id.id
				currency_id = inv.currency_id.id
				move_id = inv.move_id.line_id[0].id
				period_id=inv.period_id.id
				amount_unreconciled = inv.residual
		# service_tax = values['service_tax']
		# percentage = self.pool.get('account.tax').browse(cr,uid,x[0][2]).amount
		if context['type'] =='threep':
			amount = values['amount_collected'] + values['marketing_fees']+ values['shipping_charges'] + values['fba_charges'] + values['payment_fees']
		if context['type'] =='prepaid':
		    amount = values['net_amount'] + values['service_fees'] + values['service_tax']
		else:
		    amount = values['amount_collected']
		journal_id = values['journal_id']
		top = context['type']
		account = self.pool.get('account.account').search(cr,uid,[('name','=','Bank')])
		account_obj_id = self.pool.get('account.account').browse(cr,uid,account).id
		voucher=self.pool.get('account.voucher')
		vals={'comment': 'Write-Off', 'line_cr_ids': [[0, False, {'date_due': date,'top':top, 'account_id': account_id, 
		'date_original': date_original,'move_line_id': move_id, 'amount_original': amount_original, 
		'amount': amount,'service_tax':values['service_tax'],'service_fees':values['service_fees'],'net_amount':values['net_amount'], 'amount_unreconciled': amount_unreconciled}]], 'payment_option': 'without_writeoff', 
		'name': False, 'reference': False, 'line_dr_ids': [], 'type': 'receipt', 'writeoff_acc_id': False, 'journal_id': journal_id, 
		'is_multi_currency': False, 'company_id': inv.account_id.company_id.id, 'analytic_id': False,
		'pre_line': True, 'amount': amount,'service_tax':values['service_tax'],'service_fees':values['service_fees'],'net_amount':values['net_amount'], 'period_id': period_id, 'payment_rate_currency_id': currency_id, 'narration': False, 
		'date': '2015-02-13', 'payment_rate': 1, 'partner_id': partner_id,'top':top, 'account_id': account_obj_id}
		voucher_id = voucher.create(cr, uid, vals,context=None)

		if voucher_id:
			z=voucher.button_proforma_voucher(cr,uid,[voucher_id],context=None)
		x = super(reconciliation_prepaid, self).create(cr, uid, values, context=context)

		if context['type'] == 'cod_courier':
			self.write(cr,uid,x,{'top':'cod'})
		if context['type'] == 'prepaid':
			self.write(cr,uid,x,{'top':'prepaid'})
		if context['type'] == 'threep':
		    self.write(cr,uid,x,{'top':'threep'})	
		return x


	_columns = {
		"name" : fields.char("Reference/Waybill", required=True),
		"top" : fields.selection([('prepaid','Prepaid'),('cod','COD'),('threep','Threep')],'Type of Payment',select=True,),
		"sub_order_number" : fields.char("Order Number"),
		"amount_collected" : fields.float("Amount Collected",required=True),
		"journal_id":fields.many2one("account.journal", "Journal",required=True),
		"service_tax":fields.float("Service Tax"),
		"net_amount":fields.float("Net Amount"),
		"service_fees":fields.float("Service Fees"),
		"marketing_fees":fields.float("Marketing Fees"),
		"payment_fees":fields.float("Payment Fees"),
		"shipping_charges":fields.float("Shipping Charges"),
        }

class entries_journal(osv.osv):
	_inherit="account.move"

	_columns={
		"service_tax":fields.float('Service Tax'),
		"service_fees":fields.float('Service Fees'),
		"net_amount":fields.float('Net Amount'),
	}

class entries_journal_form(osv.osv):
	_inherit="account.voucher"

	_columns={
		"service_tax":fields.float('Service Tax'),
		"service_fees":fields.float('Service Fees'),
		"net_amount":fields.float('Net Amount'),
		"onwards_shipping_charges":fields.float('Onwards Shipping Charges'),
		"onwards_cod_charges":fields.float('Onwards COD Charges'),
		"onwards_shipping_tax":fields.float('Onwards Shipping Tax'),
		"onwards_cod_tax":fields.float('Onwards COD Tax'),
		"onwards_local_tax":fields.float('Onwards Local Tax'),
		"return_shipping_charges":fields.float('Return Shipping Charges'),
		"return_cod_charges":fields.float('Return COD Charges'),
		"return_shipping_tax":fields.float('Return Shipping Charges'),
		"return_cod_tax":fields.float('Return COD Tax'),
		"courier":fields.char('Courier'),
		"top" : fields.selection([('prepaid','Prepaid'),('cod','COD'),('threep','Threep')],'Type of Payment',select=True,),
		"payment_courier":fields.selection([('Courier','Couriers')], 'Courier Name'),
		"payment_partner":fields.selection([('payu','PayU'),('paypal','Pay Pal')],'Payment Partner'),
		"payment_date":fields.datetime('Payment Date'),
		"shipping_charges":fields.float('Shipping Charges'),
		"payment_fees":fields.float('Payment Fees'),
		"marketing_fees":fields.float('Marketing Fees'),
		"principal":fields.float('Principal'),
		"fbadelivery_services_fee":fields.float("FBA Delivery Service Fees"),
		"product_tax":fields.float("Product Tax"),
		"delivery_service_tax":fields.float("Delivery Service Tax"),
		"comission":fields.float("Comission"),
		"comission_tax":fields.float("Comission Tax"),
		"fixed_closing_fee":fields.float("Fixed Closing Fee"),
		"fbaweight_handling_fee":fields.float("FBA Weight Handling Fee"),
		"amazon_easy_shipping_charges":fields.float("Amazon Easy Shipping Charges"),
		"amazon_easy_shipping_charges_tax":fields.float("Amazon Easy Shipping Tax"),
		"shipping":fields.float("Shipping"),
		"promotion_rebates":fields.float("Promotion Rebates"),
		"channel":fields.selection([ ('amazon','Amazon'), ('flipkart','Flipkart'),('ebay','eBay'),('snapdeal','Snapdeal'),('jabong','Jabong'),('paytm','Paytm'),('limeroad','Limeroad'),('fashion&u','Fashion & U'),],'Channels', select=True,)
	}

class reconciliation_threep(osv.osv):
	_name = "reconciliation.threep"

	def create(self, cr, uid, values, context=None):
		
		# Needs revision in case of partial shipment
		if context['type'] == 'cod_courier':
			sale_id=self.pool.get('sale.order').search(cr,uid,[('awb','=',values['name'])])
		if context['type'] == 'threep':
		    sale_order_payment_id = self.pool.get('sale.order.payment').search(cr,uid,[('payment_reference', '=',values['name'])])
		    sale_id = self.pool.get('sale.order.payment').browse(cr,uid,sale_order_payment_id).order_id.id	
		elif context['type'] == 'prepaid':
			sale_order_payment_id = self.pool.get('sale.order.payment').search(cr,uid,[('payment_reference','=',values['name'])])
			for sale_id in sale_order_payment_id:
				sale_id = self.pool.get('sale.order.payment').browse(cr,uid,sale_order_payment_id).order_id.id
			#will change in case of partial shipment
				inv=self.pool.get('sale.order').browse(cr,uid,sale_id).invoice_ids[0]
				date = inv.date_due
				account_id = inv.account_id.id
				date_original = inv.date_invoice
				amount_original = inv.amount_total
				partner_id = inv.partner_id.id
				currency_id = inv.currency_id.id
				move_id = inv.move_id.line_id[0].id
				period_id=inv.period_id.id
				amount_unreconciled = inv.residual
		# service_tax = values['service_tax']
		# percentage = self.pool.get('account.tax').browse(cr,uid,x[0][2]).amount
		if context['type'] =='threep':
			amount = values['amount_collected'] + values['marketing_fees']+ values['shipping_charges'] + values['fba_charges'] + values['payment_fees']
		if context['type'] =='prepaid':
		    amount = values['amount_collected'] + values['service_fees'] + values['service_tax']
		else:
		    amount = values['amount_collected']
		journal_id = values['journal_id']
		account = self.pool.get('account.account').search(cr,uid,[('name','=','Bank')])
		account_obj_id = self.pool.get('account.account').browse(cr,uid,account).id
		voucher=self.pool.get('account.voucher')
		vals1={'comment': 'Write-Off', 'line_cr_ids': [[0, False, {'date_due': date, 'account_id': account_id, 
		'date_original': date_original,'move_line_id': move_id, 'amount_original': amount_original, 
		'amount': amount, 'amount_unreconciled': amount_unreconciled}]], 'payment_option': 'without_writeoff', 
		'name': False, 'reference': False, 'line_dr_ids': [], 'type': 'receipt', 'writeoff_acc_id': False, 'journal_id': journal_id, 
		'is_multi_currency': False, 'company_id': inv.account_id.company_id.id, 'analytic_id': False,
		'pre_line': True, 'amount': amount, 'period_id': period_id, 'payment_rate_currency_id': currency_id, 'narration': False, 
		'date': '2015-02-13', 'payment_rate': 1, 'partner_id': partner_id, 'account_id': account_obj_id}
		voucher_id4 = voucher.create(cr, uid, vals1,context=None)

		vals2={'comment': 'Write-Off', 'line_cr_ids': [[0, False, {'date_due': date, 'account_id': account_id, 
		'date_original': date_original,'move_line_id': move_id, 'amount_original': amount_original, 
		'amount': amount, 'amount_unreconciled': amount_unreconciled}]], 'payment_option': 'without_writeoff', 
		'name': False, 'reference': False, 'line_dr_ids': [], 'type': 'receipt', 'writeoff_acc_id': False, 'journal_id': journal_id, 
		'is_multi_currency': False, 'company_id': inv.account_id.company_id.id, 'analytic_id': False,
		'pre_line': True, 'amount':amount, 'period_id': period_id, 'payment_rate_currency_id': currency_id, 'narration': False, 
		'date': '2015-02-13', 'payment_rate': 1, 'partner_id': partner_id, 'account_id': account_obj_id}
		voucher_id5 = voucher.create(cr, uid, vals2,context=None)

		vals3={'comment': 'Write-Off', 'line_cr_ids': [[0, False, {'date_due': date, 'account_id': account_id, 
		'date_original': date_original,'move_line_id': move_id, 'amount_original': amount_original, 
		'amount':amount, 'amount_unreconciled': amount_unreconciled}]], 'payment_option': 'without_writeoff', 
		'name': False, 'reference': False, 'line_dr_ids': [], 'type': 'receipt', 'writeoff_acc_id': False, 'journal_id': journal_id, 
		'is_multi_currency': False, 'company_id': inv.account_id.company_id.id, 'analytic_id': False,
		'pre_line': True, 'amount':amount, 'period_id': period_id, 'payment_rate_currency_id': currency_id, 'narration': False, 
		'date': '2015-02-13', 'payment_rate': 1, 'partner_id': partner_id, 'account_id': account_obj_id}
		voucher_id6 = voucher.create(cr, uid, vals3,context=None)



		if voucher_id4:
			z=voucher.button_proforma_voucher(cr,uid,[voucher_id4],context=None)
		if voucher_id5:
			z=voucher.button_proforma_voucher(cr,uid,[voucher_id5],context=None)
		if voucher_id6:
			z=voucher.button_proforma_voucher(cr,uid,[voucher_id6],context=None)
		x = super(threep, self).create(cr, uid, values, context=context)

		if context['type'] == 'cod_courier':
			self.write(cr,uid,x,{'top':'cod'})
		if context['type'] == 'prepaid':
			self.write(cr,uid,x,{'top':'prepaid'})
		if context['type'] == 'threep':
		    self.write(cr,uid,x,{'top':'threep'})	
		return x


	_columns = {
		"name" : fields.char("Reference/Waybill", required=True),
		"top" : fields.selection([('prepaid','Prepaid'),('cod','COD'),('threep','Threep')],'Type of Payment',select=True,),
		"sub_order_number" : fields.char("Order Number"),
		"amount_collected" : fields.float("Amount Collected",required=True),
		"journal_id":fields.many2one("account.journal", "Journal",required=True),
		"marketing_fees":fields.float("Marketing Fees"),
		"payment_fees":fields.float("Payment Fees"),
		"shipping_charges":fields.float("Shipping Charges"),
		"fba_charges":fields.float("FBA Charges"),
		"service_tax":fields.float("Service Tax"),
		"cod_charges":fields.float("COD Charges"),
		# "service_tax":fields.many2many('account.tax','order_account_tax_rec','account_line_tax','service_tax1','Service Tax'),
		"service_fees":fields.float("Service Fees"),
		"channels":fields.selection([ ('amazon','Amazon'), ('flipkart','Flipkart'),('ebay','eBay'),('snapdeal','Snapdeal'),('jabong','Jabong'),('paytm','Paytm'),('limeroad','Limeroad'),('fashion&u','Fashion & U'),],'Channels', select=True,)
        }


class affilatesreferral_reconciliation(osv.osv):
	_name="affilatesreferral.reconciliation"
	_columns={
		"name":fields.char("Customer Name", required=True),
		"address":fields.char("Address"),
		"invoice_number":fields.float("Invoice Number", required=True),
		"date":fields.date("Dated"),
		"sno":fields.float("SNO."),
		"particulars":fields.char("Particulars"),
		"amount":fields.float("Amount"),
		"details":fields.char("Details"),
		"affilate_charges":fields.float("Affilate Charges", required=True),
		"bank_details":fields.char("Bank Details"),
		"total":fields.float("Total")

	}

class paymentcharges_reconciliation(osv.osv):
	_name="paymentcharges.reconciliation"
	_columns={
	"product":fields.char("Product", required=True),
	"payment_method":fields.selection([('payu','Payu'),('cod','COD'),],'Payment Method', required=True,),
	"amount_received":fields.float("Amount Received", required=True),
	"payment_fees":fields.float("Payment Fees"),

	}

class rto_reconciliation(osv.osv):
	_name="rto.reconciliation"
	_columns={
	"total_charges":fields.float("Total Charges", required=True),
	"status":fields.selection([('refund','Refund')],'Refund', required=True,),
	"refund_commission":fields.float("Refund Commission"),
	"amount_refunded":fields.float("Amount Refunded")
	}

class cr_reconciliation(osv.osv):
	_name="cr.reconciliation"
	_columns={
	"product_charges":fields.float("Product Charges", required=True),
	"status":fields.selection([('refund','Refund')],'Status'),
	"reverse_shipping_charges":fields.float("Reverse Shipping Charges"),
	"net_amount":fields.float("Net Amount")

	}
		


		


   
				
		

		
				
		
		
	

		
