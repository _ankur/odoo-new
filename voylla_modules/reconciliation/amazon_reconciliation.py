from openerp.osv import osv, fields
import pdb
import base64
from datetime import datetime,timedelta
import logging
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
import re
from datetime import date,datetime,time
import os
import os.path
import inspect
import glob
from openerp.tools.translate import _



_logger = logging.getLogger(__name__)


class amazon_file_import(osv.osv):
	_name = 'amazon.file.import'


	_columns = {
	'ref_no':fields.char("Reference Number"),
	'module_file':fields.binary("Module File"),
	'top':fields.selection([('cod','COD'),('prepaid','Prepaid'),('threep','Threep')],'Payment Source',select=True,),
	'channel':fields.selection([ ('amazon','Amazon'), ('flipkart','Flipkart'),('ebay','eBay'),('snapdeal','Snapdeal'),('jabong','Jabong'),('paytm','Paytm'),('limeroad','Limeroad'),('fashion&u','Fashion & U'),],'Channel', select=True,),
	'journal_id':fields.many2one("account.journal","Journal",required=True),
	}

	def generate_reconcile_amazon_entries(self,cr,uid,transactiontype,orderid,Principal,ProductTax,FBADeliveryServicesFee, DeliveryServiceFeeTax,FBAWeightHandlingFee,Commission,CommissionTax,FixedClosingFee,AmazonEasyShipCharges,AmazonEasyShipChargesTax,PromotionRebates,Shipping,context= None,):
		if context['top'] != 'threep':
			raise osv.except_osv(('Warning!'), ("Type of payment is not prepaid"))

		trans_id = str(orderid)
		amount_received = float(Principal) + float(PromotionRebates) + float(Shipping)
		Type = str(transactiontype)

		# Needs revision in case of partial shipment
		sale_order_obj = self.pool.get('sale.order')
		account_invoice_refund = self.pool.get('account.invoice.refund')
		account_invoice_confirm = self.pool.get('account.invoice.confirm')
		sale_order_payment_id = self.pool.get('sale.order').search(cr,uid,[('order_id', '=',trans_id)])
		voucher_id_list = []
		for sale_id in sale_order_payment_id:
			sale_object = self.browse(cr,uid,sale_id)
			inv=self.pool.get('sale.order').browse(cr,uid,sale_id).invoice_ids
			date = inv.date_due
			invoice_name = inv.reference
			order_number = inv.reference
			account_id = inv.account_id.id
			date_original = inv.date_invoice
			state = inv.state
			amount_original = inv.amount_total
			if amount_received > amount_original:
				raise osv.except_osv(('Warning!'),("Amount entered is greater then actual invoice amount or invoice is not generated for payment reference no. %s")%(orderid))
			partner_id = inv.partner_id.id
			currency_id = inv.currency_id.id
			amount = amount_received

			try:
				move_id = inv.move_id.line_id[0].id
				period_id=inv.period_id.id
				amount_unreconciled = inv.residual
				channel = context['channel']

			except:
				raise osv.except_osv(('Warning!'), ("Invoice is not generated"))

			account = self.pool.get('account.account').search(cr,uid,[('name','=','Bank')])
			account_obj_id = self.pool.get('account.account').browse(cr,uid,account).id
			voucher=self.pool.get('account.voucher')
			journal_id =  context['journal_id']
			top = context['type']
			promotion_rebates = float(PromotionRebates)
			shipping = float(Shipping)
			principal = float(Principal)
			product_tax = float(ProductTax)
			fbadelivery_services_fee = float(FBADeliveryServicesFee)
			delivery_service_tax = float(DeliveryServiceFeeTax)
			comission = float(Commission)
			comission_tax = float(CommissionTax)
			fixed_closing_fee = float(FixedClosingFee)
			fbaweight_handling_fee = float(FBAWeightHandlingFee)

			if Type =='Refund':
					if state != 'paid':
						# total = float(total)
						# charges =  float(promotionalrebates) + float(other) + float(othertransactionfees) + float(fbafees) + float(sellingfees)
						# amount = -(float(total) - (charges))

						amount = -float(Principal)
						vals1 ={'lang': 'en_US', 'tz': False, 'uid': uid,'reference':order_number, 'active_model': 'account.invoice', 'journal_type': 'sale', 'search_disable_custom_filters': True,
						'active_ids': [inv.id], 'type': 'out_invoice', 'active_id':inv.id}

						refund_id = account_invoice_refund.create(cr,uid,vals1,context=None)
						if refund_id:
							z = account_invoice_refund.invoice_refund(cr,uid,[refund_id],context=vals1)
							# y = account_invoice_confirm.invoice_confirm(cr,uid,refund_id,context=vals1)
					elif state == 'paid':
						# total = float(total)
						# charges =  float(promotionalrebates) + float(other) + float(othertransactionfees) + float(fbafees) + float(sellingfees)
						# amount = -(float(total) - (charges))
						amount = -float(Principal)

						vals1 ={'lang': 'en_US', 'tz': False, 'uid': uid,'reference':order_number, 'active_model': 'account.invoice', 'journal_type': 'sale', 'search_disable_custom_filters': True,
						'active_ids': [inv.id], 'type': 'out_invoice', 'active_id':inv.id}
						refund_id = account_invoice_refund.create(cr,uid,vals1,context=None)
						
						# refund_invoice = account_invoice_refund.invoice_refund(cr,uid,context1['active_id'],context1)
						if refund_id:
							y = account_invoice_refund.invoice_refund(cr,uid,[refund_id],context=vals1)
							vals2 ={'lang': 'en_US', 'tz': False, 'uid': uid,'reference':order_number, 'active_model': 'account.invoice', 'journal_type': 'sale', 'search_disable_custom_filters': True,
							'active_ids': y['refund_id'], 'type': 'out_invoice', 'active_id':y['refund_id']}
							z = account_invoice_confirm.invoice_confirm(cr,uid,[refund_id],context=vals2)
					return z
				

			if Type == 'Other-transaction':
				move_id3 = self.get_amazon_easy_shipping(cr,uid,Type,orderid,Principal,ProductTax,FBADeliveryServicesFee, DeliveryServiceFeeTax,FBAWeightHandlingFee,Commission,CommissionTax,FixedClosingFee,invoice_name,AmazonEasyShipCharges,AmazonEasyShipChargesTax, PromotionRebates,Shipping, context=context)
			# date_pay = date_pay.strftime('%d/%m/%y')
			# voucher_id = voucher.search(cr,uid,[('reference', '=',invoice_name)])
			if Type == 'Order':
				vals = {'comment': 'Write-Off', 'line_cr_ids': [[0, False, {'date_due': date, 'account_id': account_id, 
				'date_original': date_original,'move_line_id': move_id, 'amount_original': amount_original, 
				'amount': amount_received,'top':top,'channel':channel,'product_tax':product_tax,'fbadelivery_services_fee':fbadelivery_services_fee,'promotion_rebates':promotion_rebates,'shipping':shipping,'principal':principal,'fba_charges':delivery_service_tax,'fbaweight_handling_fee':fbaweight_handling_fee,'comission':comission,'comission_tax':comission_tax,'amount_unreconciled': amount_unreconciled}]], 'payment_option': 'without_writeoff', 
				'name': False, 'reference': order_number, 'line_dr_ids': [], 'type': 'receipt', 'writeoff_acc_id': False, 'journal_id': journal_id, 
				'is_multi_currency': False, 'company_id': inv.account_id.company_id.id, 'analytic_id': False,
				'pre_line': True, 'amount': amount_received,'channel':channel,'product_tax':product_tax,'fbaweight_handling_fee':fbaweight_handling_fee,'principal':principal,'shipping':shipping,'promotion_rebates':promotion_rebates,'fbadelivery_services_fee':fbadelivery_services_fee,'fba_charges':delivery_service_tax,'comission':comission,'comission_tax':comission_tax, 'top':top,'period_id': period_id, 'payment_rate_currency_id': currency_id, 'narration': False, 
				'date': date, 'payment_rate': 1, 'partner_id': partner_id, 'account_id': account_obj_id}
				voucher_id = voucher.create(cr, uid, vals,context=None)
				voucher_id_list.append(voucher_id)
				for voucher_id in voucher_id_list:
					if voucher_id:
						z=voucher.button_proforma_voucher(cr,uid,[voucher_id],context=None)
						x = super(amazon_file_import, self).create(cr, uid, vals, context=context)
				# else:voylla_modules/outbound/outbound.py
				# 	voucher_id = voucher.create(cr,uid,vals,context=None)
				# 	z=voucher.button_proforma_voucher(cr,uid,[voucher_id],context=None)
				# 	# x = voucher.write(cr,uid,voucher_id,{'other':other ,'total' : total,'amount': amount})
				# 	x = super(amazon_file_import, self).create(cr, uid, vals, context=context)


				if context['type'] == 'threep':
					# self.write(cr,uid,x,{'top':'threep'})
					return x


	def get_amazon_easy_shipping(self,cr,uid,Type,orderid,Principal,ProductTax,FBADeliveryServicesFee, DeliveryServiceFeeTax,FBAWeightHandlingFee,Commission,CommissionTax,FixedClosingFee,invoice_name,AmazonEasyShipCharges,AmazonEasyShipChargesTax,PromotionRebates,Shipping, context= None,):
		trans_id = str(orderid)
		amount_received = float(Principal) + float(PromotionRebates) + float(Shipping)
		Type = str(Type)
		sale_order_obj = self.pool.get('sale.order')
		sale_order_payment_id = self.pool.get('sale.order').search(cr,uid,[('order_id', '=',trans_id)])
		voucher_id_list = []
		for sale_id in sale_order_payment_id:
			sale_object = self.browse(cr,uid,sale_id)
			inv=self.pool.get('sale.order').browse(cr,uid,sale_id).invoice_ids
			date = inv.date_due
			invoice_name = inv.reference
			order_number = inv.reference
			account_id = inv.account_id.id
			date_original = inv.date_invoice
			amount_original = inv.amount_total
			if amount_received > amount_original:
				raise osv.except_osv(('Warning!'),("Amount entered is greater then actual invoice amount or invoice is not generated for payment reference no. %s")%(orderid))
			partner_id = inv.partner_id.id
			currency_id = inv.currency_id.id
			amount = amount_received

			try:
				move_id = inv.move_id.line_id[0].id
				period_id=inv.period_id.id
				amount_unreconciled = inv.residual
				channel = context['channel']

			except:
				raise osv.except_osv(('Warning!'), ("Invoice is not generated"))

			account = self.pool.get('account.account').search(cr,uid,[('name','=','Bank')])
			account_voucher_registry = self.pool.get('account.voucher')
			journal_id =  context['journal_id']
			top = context['type']
			principal = float(Principal)
			product_tax = float(ProductTax)
			fbadelivery_services_fee = float(FBADeliveryServicesFee)
			delivery_service_tax = float(DeliveryServiceFeeTax)
			comission = float(Commission)
			comission_tax = float(CommissionTax)
			fixed_closing_fee = float(FixedClosingFee)
			shipping = float(Shipping)
			promotion_rebates = float(PromotionRebates)
			fbaweight_handling_fee = float(FBAWeightHandlingFee)
			account_obj_id = self.pool.get('account.account').browse(cr,uid,account).id
			# voucher_id = account_voucher_registry.search(cr,uid,[('reference', '=',invoice_name)])
			vals = {'comment': 'Write-Off', 'line_cr_ids': [[0, False, {'date_due': date, 'account_id': account_id, 
				'date_original': date_original,'move_line_id': move_id, 'amount_original': amount_original, 
				'amount': amount_received,'top':top,'channel':channel,'product_tax':product_tax,'fbadelivery_services_fee':fbadelivery_services_fee,'shipping':shipping,'promotion_rebates':promotion_rebates,'principal':principal,'fba_charges':delivery_service_tax,'fbaweight_handling_fee':fbaweight_handling_fee,'comission':comission,'comission_tax':comission_tax,'amount_unreconciled': amount_unreconciled}]], 'payment_option': 'without_writeoff', 
				'name': False, 'reference': order_number, 'line_dr_ids': [], 'type': 'receipt', 'writeoff_acc_id': False, 'journal_id': journal_id, 
				'is_multi_currency': False, 'company_id': inv.account_id.company_id.id, 'analytic_id': False,
				'pre_line': True, 'amount': amount_received,'channel':channel,'product_tax':product_tax,'fbaweight_handling_fee':fbaweight_handling_fee,'principal':principal,'shipping':shipping,'promotion_rebates':promotion_rebates,'fbadelivery_services_fee':fbadelivery_services_fee,'fba_charges':delivery_service_tax,'comission':comission,'comission_tax':comission_tax, 'top':top,'period_id': period_id, 'payment_rate_currency_id': currency_id, 'narration': False, 
				'date': date, 'payment_rate': 1, 'partner_id': partner_id, 'account_id': account_obj_id}
			# if len(voucher_id) == 0:
			voucher_reg = account_voucher_registry.create(cr,uid,vals,context=None)
			z=account_voucher_registry.button_proforma_voucher(cr,uid,[voucher_reg],context=None)
			x = super(amazon_file_import, self).create(cr, uid, vals, context=context)
			# else:
			# 	z=account_voucher_registry.button_proforma_voucher(cr,uid,voucher_id,context=None)
			# 	x = account_voucher_registry.write(cr,uid,voucher_id,{'other':other ,'total' : total})
			if context['type'] == 'threep':
			# self.write(cr,uid,x,{'top':'threep'})
				return x

	def import_file_amazon(self,cr,uid,ids,context=None):
		data = self.browse(cr, uid, ids[0] , context=context)
		csv_data = base64.decodestring(data.module_file)
			
		file_creation_registry = self.pool.get("account.move.line")
		chars_to_remove = ['\r', ' ', '\n']
		chars_to_remove_1 = ['\r', '\n']
		rx = "[%s]" %(re.escape(''.join(chars_to_remove)))
		rx_1 = "[%s]" %(re.escape(''.join(chars_to_remove_1)))

		print csv_data
		rows = csv_data.split("\n")

		try:
			header = re.sub(rx, '', rows[0]).split(",")
			csv_amazon_details = rows[1:]
		except:
			pass
 		amazon_details = {}
 		
		for amazon in csv_amazon_details:
			if amazon == '':
				continue
			amazon_details = dict(zip(header, re.sub(rx_1, '', amazon).split(",")))
			move_id2= self.generate_reconcile_amazon_entries(cr,uid,amazon_details['transactiontype'],amazon_details['orderid'],amazon_details['Principal'],amazon_details['ProductTax'],amazon_details['FBADeliveryServicesFee'], amazon_details['DeliveryServiceFeeTax'],amazon_details['FBAWeightHandlingFee'],amazon_details['Commission'],amazon_details['CommissionTax'],amazon_details['FixedClosingFee'],amazon_details['AmazonEasyShipCharges'],amazon_details['AmazonEasyShipChargesTax'],amazon_details['PromotionRebates'],amazon_details['Shipping'], context=context)

		