from voylla_modules.config import home
import os
from os import listdir
from os.path import isfile, join
import subprocess
import datetime
from os import listdir
from os.path import isfile, join
import subprocess
from datetime import datetime,timedelta
import shlex
import spur
import ast
import json
import sys
import pdb
import re
import openerp
import pdb
import psycopg2
import csv
import time
import urllib2
import datetime
import logging
from datetime import datetime
from dateutil.relativedelta import relativedelta
from operator import itemgetter
import time
import xmlrpclib
import os
import ftplib
import socket
from voylla_modules.config import home 
import openerp
import pdb
from openerp import SUPERUSER_ID
from openerp import pooler, tools
from openerp.osv import fields, osv, expression
from openerp.tools.translate import _
from openerp.tools.float_utils import float_round as round
import logging
from psycopg2.extensions import ISOLATION_LEVEL_READ_COMMITTED
from voylla_modules.file_split import split

_logger = logging.getLogger(__name__)
from functools import wraps

def retry(ExceptionToCheck, tries=4, delay=3, backoff=2, logger=None):
    """Retry calling the decorated function using an exponential backoff.

    http://www.saltycrane.com/blog/2009/11/trying-out-retry-decorator-python/
    original from: http://wiki.python.org/moin/PythonDecoratorLibrary#Retry

    :param ExceptionToCheck: the exception to check. may be a tuple of
        exceptions to check
    :type ExceptionToCheck: Exception or tuple
    :param tries: number of times to try (not retry) before giving up
    :type tries: int
    :param delay: initial delay between retries in seconds
    :type delay: int
    :param backoff: backoff multiplier e.g. value of 2 will double the delay
        each retry
    :type backoff: int
    :param logger: logger to use. If None, print
    :type logger: logging.Logger instance
    """
    def deco_retry(f):

        @wraps(f)
        def f_retry(*args, **kwargs):
            mtries, mdelay = tries, delay
            while mtries > 1:
                try:
                    return f(*args, **kwargs)
                except ExceptionToCheck, e:
                    msg = "%s, Retrying in %d seconds... product import retry" % (str(e), mdelay)
                    if logger:
                        logger.warning(msg)
                    else:
                        print msg
                    time.sleep(mdelay)
                    mtries -= 1
                    mdelay *= backoff
            return f(*args, **kwargs)

        return f_retry  # true decorator

    return deco_retry
class spree_cron(osv.osv):
    _name = "spree.cron"
    def price_import_file(self,cr,uid,context=None):
      sql="select p.sale_price,p.cost,p.mrp,product.name,product.update_flag from product_price_history as p left join product_template as product on p.product_template_id = product.id where p.write_date >= NOW() - interval'12 hours'"
      cr.execute(sql)
      res_val = cr.fetchall()
      product=self.pool.get('product.template')
      d=[]
      header=['sku','product_variant','size','diamond','gold','sale price','cost price','mrp']
      d.append(header)
      for r in res_val:
        a=['']*8
        sale_price=r[0]
        cost=r[1]
        mrp=r[2]
        if mrp > sale_price > cost and r[4]:
          a[0]=r[3]
          a[1]='product'
          a[5]=sale_price
          a[6]=cost
          a[7]=mrp
          d.append(a)
      k=datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
      k='price_'+k+'.csv'
      k1=home+'/import/'+k
      with open(k1, "wb") as f:
        writer = csv.writer(f)
        writer.writerows(d)
        f.close()
      if  os.environ.get("ODOO_ENV")=='production':
        self.submit_file(k1,k,"ErpPriceUpload","ErpPriceUpload")

    def update_product_import(self,cr,uid,context=None):
      context='update_flag'
      sql="Select id from product_template where write_date >= NOW() - '8 Hour 45 minute'::INTERVAL and update_flag"
      self.product_import_and_update(cr,uid,sql,context)
    def product_import(self,cr,uid,context=None):
      sql="Select id from product_template where write_date >= NOW() - '8 Hour 45 minute'::INTERVAL and update_flag=False and flag_complete"
      self.product_import_and_update(cr,uid,sql,context)
    def product_import_and_update(self, cr, uid,values, context=None):
       k=datetime.now().strftime("%H-%M-%d-%m-%y")
       obj = self.pool.get('product.template')
       precious_obj=self.pool.get('product.precious.dimension')
       cr.execute(values)
       
       res_val = cr.fetchall()
       ids = [ r[0] for r in res_val]
       res=obj.browse(cr,uid,ids)
       if len(res)==0:
        return 
       header=["VOYLLA_PRODUCT_CODE","Designer Product Code","Designer Name","Designer Id","variation size","property sales_package","Buyer Mrp","Retail Price","Cost","Quantity","Product Name","Product Description","property gender","Taxonomy","Taxons","tag type_1","tag type_2","property primary_base_colour","property secondry_base_colour","property primary_gemstone_colour","property secondry_gemstone_colour","property theme","property design","property surface_finish","property age_group","tag collection_1","tag occasion_1","tag occasion_2","tag material_1","tag material_2","tag plating_1","tag plating_2","tag gemstones_1","tag gemstones_2","tag free_products_1","tag HOME_PAGE_LINK_1","update_available_Date","MOD","LeadTime","Diamond_price","Making Charge","Gemstone Price","EAN","Dimension1","Dimension2","Dimension3","Dimension4","Dimension5","Dimension6","Dimension7","Dimension8","Dimension9","Dimension10","Dimension11","Dimension12","Dimension13","Dimension14","Dimension15","Dimension16","Dimension17 ","Dimension18","Dimension19","Dimension20","Dimension21","Dimension22","Dimension23","Dimension24","Dimension25","Dimension26","Dimension27","Dimension28","Dimension29","Dimension30","Dimension31","Dimension32","Dimension33","Dimension34","Dimension35","Dimension36","Dimension37","Dimension38","Dimension39","Dimension40","Variations","variation diamond","variation gold","three_p_number","Dimension41","Dimension42","Dimension43","Dimension44","Dimension45","Dimension46","Dimension47","Dimension48","Dimension49","Dimension50","Dimension51","Dimension52","Dimension53","Dimension54","Dimension55","Dimension56","stone_number"]
       d=[]
       d.append(header)
       for val in res:
          
          if val.precious==True:
            dimid=val.id
            no_of_stones=val.no_of_stones
            for num in range(0,int(no_of_stones)):
              a=['']*104
              general_list=precious_obj.search(cr,uid,[('dimid','=',dimid),('stone_no','=','0')])
              for obj1_id in general_list:
                obj1=precious_obj.browse(cr,uid,obj1_id)
                if obj1.dimension_num is not None:
                  a[86+obj1.dimension_num]=obj1.value 
              a[0]=val.name
              precious_list=precious_obj.search(cr,uid,[('dimid','=',dimid),('stone_no','=',str(num+1))])
              stone_number=num+1
              a[103]=stone_number
              for obj1_id in precious_list:
                obj1=precious_obj.browse(cr,uid,obj1_id)
                if obj1.dimension_num is not None:
                  a[86+obj1.dimension_num]=obj1.value
              if len(val.seller_ids)!=0:
                vendor_code = val.seller_ids[0].product_code#1
                if vendor_code==False:
                  a[1]=a[0][:5]
                else:
                  a[1]=vendor_code.encode('utf-8')
                a[2]=''#2
                if val.seller_ids[0].name.supplier_code==False:
                  a[3]=a[0][:5]
                else:
                  a[3]=val.seller_ids[0].name.supplier_code
              
              #4variant
              if len(val.salespackage)!=0:
                a[5]=val.salespackage.name#5salespackage
              #price
              a[6]=val.mrp
              #6
              a[7]=val.list_price #7
              a[8]=val.standard_price#
              #quantity
              a[9]= 0#9
              #name
              if val.product_name==False:
                a[10]='Name is missing'
                continue
              else:
                a[10]=val.product_name.encode('utf-8')#10
              #description
              if val.description==False:
                a[11]="description is missing"#11
                continue
              else:
                a[11]=val.description.encode('utf-8')
              #gender
              gender_name=''
              if len(val.gender)!=0:
                a[12]=val.gender.name#12
                if a[12]=='Men':
                  gender_name='Mens '
              #taxonomy
              taxonomy_full=''
              taxon_full=''
              type_full=''
              for category in val.taxonomy:
                name=category.categ_id.complete_name.split('/')
                count=len(name)
                if count >=1:
                  if taxonomy_full=='':
                    taxonomy_full=gender_name + (name[0]).strip()
                  else:
                    taxonomy_full+='$'+gender_name + (name[0]).strip()

                if count >=2:
                  if taxon_full=='':
                    taxon_full=gender_name+ (name[1]).strip()
                  else:
                    taxon_full+='$'+gender_name+ (name[1]).strip()
                if count >=3:
                  if type_full=='':
                    type_full=name[2].strip()
                  else:
                    type_full+='$'+name[2].strip()



              
              a[13]=taxonomy_full#13
              #taxon
              a[14]=taxon_full
              a[15]=type_full
              #color
              if len(val.color_one)!=0:
                a[17]=val.color_one.name
              #color_2
              if len(val.color_two)!=0:
                a[18]=val.color_two.name
              #gemstone color
              if len(val.gem_color_one)!=0:
                a[19]=val.gem_color_one.name
              if len(val.gem_color_two)!=0:
                a[20]=val.gem_color_two.name
              #theme
              if len(val.theme_)!=0:
                a[21]=val.theme_.name
              #design
              if len(val.design)!=0:
                a[22]=val.design.name
              #surface finish
              if len(val.surface_finish_one)!=0:
                a[23]=val.surface_finish_one.name
              #age_group
              a[24]=val.agegroup
              
              tags=[]
              for t in val.tagid:
                tags.append([t.tag_type,t.user_type.name])
              type_flag=True
              type_occassion=True
              type_material=True
              type_plating=True
              type_gemstone=True
              for k in tags:
                if k[0]=='collection':
                  a[25]=k[1]
                elif k[0]=='occassion':
                  if type_occassion:
                    a[26]=k[1]
                    type_occassion=False
                  else:
                    a[27]=k[1]
                elif k[0]=='material':
                  if type_material:
                    a[28]=k[1]
                    type_material=False
                  else:
                    a[29]=k[1]


                elif k[0]=='plating':
                  if type_plating:
                    a[30]=k[1]
                    type_plating=False
                  else:
                    a[31]=k[1]

                elif k[0]=='gemstone':
                  if type_gemstone:
                    a[32]=k[1]
                    type_gemstone=False
                  else:
                    a[33]=k[1]

                elif k[0]=='free_product':
                  a[34]=k[1]
                elif k[0]=='home_page':
                  a[35]=k[1]
                elif k[0] =='type':
                  a[15]=a[15]+'$%s'%(k[1])  
              a[36]='Yes'
              is_mod='NO'
              for mod in val.route_ids:
                if mod.name=='Make To Order':
                  is_mod='YES'
                  break
              a[37]=is_mod
              if len(val.seller_ids)!=0 and is_mod=='YES':
                a[38]=val.seller_ids[0].delay
              dim_array_val=[]
              product_ean_genrator=self.pool.get('product.template')
              if val.masterean==False:
                temp_ean=product_ean_genrator.generate_next_ean13_code(cr,uid,context)
                val.masterean=temp_ean
                if val.threep_reference in [False,'']:
                  val.threep_reference=temp_ean
                elif val.threep_reference=='N':
                  val.threep_reference=val.name
                if len(val.attribute_line_ids)==0:
                  val.product_variant_ids.ean13=temp_ean
                  if val.product_variant_ids.threep_reference in [False,'']:
                    val.product_variant_ids.threep_reference=temp_ean
                  elif val.product_variant_ids.threep_reference=='N':
                    val.product_variant_ids.threep_reference=val.name
                cr.commit()
              m=0
              try:
                if val.is_combos == True:
                  for taxons in val.taxon_list:
                    a[43+m]=taxons.taxon[0].name[0:len(taxons.taxon[0].name)-7] +''+str(taxons.taxon_num)
                    m=m+1
                    for l in range(0,len(val.childdimid)) :
                      if val.childdimid[l].taxon_num_id==taxons.taxon_num and val.childdimid[l].taxon_id.id==taxons.taxon.id:
                        if val.childdimid[l].value>0:  
                          a[43+m]=val.childdimid[l].value
                          m=m+1
                        else:
                          m=m+1
              except:
                _logger.info("==================================")
                _logger.info("======== PRO IMP ERROR ===========")
                _logger.info(val.name)
                _logger.info("==================================")

              else:
                for t in val.childdimid:
                  if t.value > 0:
                    dim_array_val.append([t.dimension_num,t.value]) 
                for p in dim_array_val:
                  a[42+int(p[0])]=float(p[1])
              variants_type={}
              if len(val.product_variant_ids)==0:
                continue
              else:
                variants_type=val.product_variant_ids[0].get_product_size()
              varaintion_type_str='NA'
              for j in variants_type:
                if varaintion_type_str=='NA':
                  varaintion_type_str=j
                else:
                  varaintion_type_str+=','+j
              a[42]=val.masterean
              a[86]=val.threep_reference
              a[83]=varaintion_type_str
              d.append(a)
              temp_array_variants_num=a[:]
              
              if len(val.attribute_line_ids)!=0:
                for product in val.product_variant_ids:
                  variant_size_hash=product.get_product_size()
                  temp_array_variants=temp_array_variants_num[:]
                  if 'size' in variant_size_hash:
                    temp_array_variants[4]=variant_size_hash['size']
                  if 'diamond' in variant_size_hash:
                    temp_array_variants[84]=variant_size_hash['diamond']
                  if 'gold' in variant_size_hash:
                    temp_array_variants[85]=variant_size_hash['gold']
                  temp_array_variants[7]+=int(product.price_extra)
                  if product.ean13==False or product.ean13==product.masterean or product.ean13=='':
                    temp_ean=product_ean_genrator.generate_next_ean13_code(cr,uid,context)
                    product.ean13=temp_ean
                    if product.threep_reference in [False,'']:
                      product.threep_reference=temp_ean
                    elif product.threep_reference=='N':
                      if 'size' in variant_size_hash:
                        variant_size=variant_size_hash['size']
                        if variant_size.lower()=="m" or variant_size.lower()=="adjustable" or variant_size.lower()=="free size":
                          product.threep_reference=val.name
                        else:
                          product.threep_reference=val.name+'_'+variant_size
                      cr.commit()
                    temp_array_variants[42]=temp_ean
                    temp_array_variants[86]=product.threep_reference
                  else:
                    temp_array_variants[42]=product.ean13
                    temp_array_variants[86]=product.threep_reference
                  dim_array_val=[]
                  for t in product.childdimid:
                    if t.value > 0:
                      dim_array_val.append([t.dimension_num,t.value]) 
                  for p in dim_array_val: 
                    temp_array_variants[42+int(p[0])]=float(p[1])
                  d.append(temp_array_variants)
          else:
            a=['']*104
            a[0]=val.name
            if len(val.seller_ids)!=0:
              vendor_code = val.seller_ids[0].product_code#1
              if vendor_code==False:
                a[1]=a[0][:5]
              else:
                a[1]=vendor_code.encode('utf-8')
              a[2]=''#2
              if val.seller_ids[0].name.supplier_code==False:
                a[3]=a[0][:5]
              else:
                a[3]=val.seller_ids[0].name.supplier_code
              
              #4variant
            if len(val.salespackage)!=0:
              a[5]=val.salespackage.name#5salespackage
            #price
            a[6]=val.mrp
            #6
            a[7]=val.list_price #7
            a[8]=val.standard_price#
            #quantity
            a[9]= 0#9
            #name
            if val.product_name==False:
              a[10]='Name is missing'
              continue
            else:
              a[10]=val.product_name.encode('utf-8')#10
            #description
            if val.description==False:
              a[11]="description is missing"#11
              continue
            else:
              a[11]=val.description.encode('utf-8')
            #gender
            gender_name=''
            if len(val.gender)!=0:
              a[12]=val.gender.name#12
              if a[12]=='Men':
                gender_name='Mens '
            #taxonomy
            taxonomy_full=''
            taxon_full=''
            type_full=''
            for category in val.taxonomy:
              name=category.categ_id.complete_name.split('/')
              count=len(name)
              if count >=1:
                if taxonomy_full=='':
                  taxonomy_full=gender_name + (name[0]).strip()
                else:
                  taxonomy_full+='$'+gender_name + (name[0]).strip()

              if count >=2:
                if taxon_full=='':
                  taxon_full=gender_name+ (name[1]).strip()
                else:
                  taxon_full+='$'+gender_name+ (name[1]).strip()
              if count >=3:
                if type_full=='':
                  type_full=name[2].strip()
                else:
                  type_full+='$'+name[2].strip()



            
            a[13]=taxonomy_full#13
            #taxon
            a[14]=taxon_full
            a[15]=type_full
            #color
            if len(val.color_one)!=0:
              a[17]=val.color_one.name
            #color_2
            if len(val.color_two)!=0:
              a[18]=val.color_two.name
            #gemstone color
            if len(val.gem_color_one)!=0:
              a[19]=val.gem_color_one.name
            if len(val.gem_color_two)!=0:
              a[20]=val.gem_color_two.name
            #theme
            if len(val.theme_)!=0:
              a[21]=val.theme_.name
            #design
            if len(val.design)!=0:
              a[22]=val.design.name
            #surface finish
            if len(val.surface_finish_one)!=0:
              a[23]=val.surface_finish_one.name
            #age_group
            a[24]=val.agegroup
            
            tags=[]
            for t in val.tagid:
              tags.append([t.tag_type,t.user_type.name])
            type_flag=True
            type_occassion=True
            type_material=True
            type_plating=True
            type_gemstone=True
            for k in tags:
              if k[0]=='collection':
                a[25]=k[1]
              elif k[0]=='occassion':
                if type_occassion:
                  a[26]=k[1]
                  type_occassion=False
                else:
                  a[27]=k[1]
              elif k[0]=='material':
                if type_material:
                  a[28]=k[1]
                  type_material=False
                else:
                  a[29]=k[1]


              elif k[0]=='plating':
                if type_plating:
                  a[30]=k[1]
                  type_plating=False
                else:
                  a[31]=k[1]

              elif k[0]=='gemstone':
                if type_gemstone:
                  a[32]=k[1]
                  type_gemstone=False
                else:
                  a[33]=k[1]

              elif k[0]=='free_product':
                a[34]=k[1]
              elif k[0]=='home_page':
                a[35]=k[1]
              elif k[0] =='type':
                a[15]=a[15]+'$%s'%(k[1])  
            a[36]='Yes'
            is_mod='NO'
            for mod in val.route_ids:
              if mod.name=='Make To Order':
                is_mod='YES'
                break
            a[37]=is_mod
            if len(val.seller_ids)!=0 and is_mod=='YES':
              a[38]=val.seller_ids[0].delay
            dim_array_val=[]
            product_ean_genrator=self.pool.get('product.template')
            if val.masterean==False:
              temp_ean=product_ean_genrator.generate_next_ean13_code(cr,uid,context)
              val.masterean=temp_ean
              if val.threep_reference in [False,'']:
                val.threep_reference=temp_ean
              elif val.threep_reference=='N':
                val.threep_reference=val.name
              if len(val.attribute_line_ids)==0:
                val.product_variant_ids.ean13=temp_ean
                if val.product_variant_ids.threep_reference in [False,'']:
                  val.product_variant_ids.threep_reference=temp_ean
                elif val.product_variant_ids.threep_reference=='N':
                  val.product_variant_ids.threep_reference=val.name
              cr.commit()
            m=0
            try:
              if val.is_combos == True:
                for taxons in val.taxon_list:
                  a[43+m]=taxons.taxon[0].name[0:len(taxons.taxon[0].name)-7] +''+str(taxons.taxon_num)
                  m=m+1
                  for l in range(0,len(val.childdimid)) :
                    if val.childdimid[l].taxon_num_id==taxons.taxon_num and val.childdimid[l].taxon_id.id==taxons.taxon.id:
                      if val.childdimid[l].value>0:  
                        a[43+m]=val.childdimid[l].value
                        m=m+1
                      else:
                        m=m+1
            except:
              _logger.info("==================================")
              _logger.info("======== PRO IMP ERROR ===========")
              _logger.info(val.name)
              _logger.info("==================================")

            else:
              for t in val.childdimid:
                if t.value > 0:
                  dim_array_val.append([t.dimension_num,t.value]) 
              for p in dim_array_val:
                a[42+int(p[0])]=float(p[1])
            variants_type={}
            if len(val.product_variant_ids)==0:
              continue
            else:
              variants_type=val.product_variant_ids[0].get_product_size()
            varaintion_type_str='NA'
            for j in variants_type:
              if varaintion_type_str=='NA':
                varaintion_type_str=j
              else:
                varaintion_type_str+=','+j
            a[42]=val.masterean
            a[86]=val.threep_reference
            a[83]=varaintion_type_str
            d.append(a)
            temp_array_variants_num=a[:]
            
            if len(val.attribute_line_ids)!=0:
              for product in val.product_variant_ids:
                variant_size_hash=product.get_product_size()
                temp_array_variants=temp_array_variants_num[:]
                if 'size' in variant_size_hash:
                  temp_array_variants[4]=variant_size_hash['size']
                if 'diamond' in variant_size_hash:
                  temp_array_variants[84]=variant_size_hash['diamond']
                if 'gold' in variant_size_hash:
                  temp_array_variants[85]=variant_size_hash['gold']
                temp_array_variants[7]+=int(product.price_extra)
                if product.ean13==False or product.ean13==product.masterean or product.ean13=='':
                  temp_ean=product_ean_genrator.generate_next_ean13_code(cr,uid,context)
                  product.ean13=temp_ean
                  if product.threep_reference in [False,'']:
                    product.threep_reference=temp_ean
                  elif product.threep_reference=='N':
                    if 'size' in variant_size_hash:
                      variant_size=variant_size_hash['size']
                      if variant_size.lower()=="m" or variant_size.lower()=="adjustable" or variant_size.lower()=="free size":
                        product.threep_reference=val.name
                      else:
                        product.threep_reference=val.name+'_'+variant_size
                    cr.commit()
                  temp_array_variants[42]=temp_ean
                  temp_array_variants[86]=product.threep_reference
                else:
                  temp_array_variants[42]=product.ean13
                  temp_array_variants[86]=product.threep_reference
                dim_array_val=[]
                for t in product.childdimid:
                  if t.value > 0:
                    dim_array_val.append([t.dimension_num,t.value]) 
                for p in dim_array_val:
                  temp_array_variants[42+int(p[0])]=float(p[1])
                d.append(temp_array_variants)
            

       k=datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
       if context=='update_flag':
        k='update_product_'+k+'.csv'
       else:
        k='product_'+k+'.csv'
       # k1=home+k
       k1=home+'/import/'+k
       with open(k1, "wb") as f:
          writer = csv.writer(f)
          writer.writerows(d)
          f.close()
       total_part_of_file=len(d)/20   
       if len(d)%100!=0: 
           total_part_of_file=total_part_of_file+1
       split(open('%s'%(k1), 'r'))   
       filename=home
       if os.environ.get("ODOO_ENV") == "production":
        self.submit_file(k1,k,"ErpProductUpload","ErpProductUpload",total_part_of_file)

    @retry(Exception, tries=5, delay=5)
    def submit_file(self,name,basename,user,password,part):
      b=str(name).split('.')

      m=b[0].split('-')
      current_piece=10	
      for one in range(1,part+1): 
        #filename=home+'output_%s.csv'%(one)
        #basename='output_%s.csv'%(one)
	b=str(name).split('.')

	m=b[0].split('-')
        #current_piece=m[-1]
	k=b[0].split('/')
        lenght=len(k[-1])
        name_change=k[-1]
        d=''
        for x in name_change[0:lenght-2]:
      	   d=d+x       

	c=b[0].split('/')
	filename=str(home)+'/'+str(d)+'%s.csv'%(current_piece)
        #filename=str(b[0])+'_%s.csv'%(one)
        basename=str(d)+'%s.csv'%(current_piece)

        import_file=open(filename, 'rb')
        le = len(import_file.readlines())
	current_piece =int(current_piece)+1
        if le > 1:
            try:
              ftp = ftplib.FTP("staging.voylla.com")
              ftp.login(user,password) #(username, password)
              ftp.storlines("STOR " + basename, open(filename, 'rb'))
            except:
              raise Exception("Fail")
    def dimension_import(self,cr,uid,context=None):
       reader = open('dimension.csv','rb').readlines()
       taxon_dim_obj=self.pool.get('product.dimension')
       firstline=True
       for row in reader:
        
        if firstline==True:
          firstline=False
          continue
        col_=row[1:-1].split(',')
        ids=self.pool.get('product.taxon').search(cr,uid,[('name','=',col_[0] )])
        if len(ids)==0:
          vals={'name':col_[0]}
          ids=self.pool.get('product.taxon').create(cr,uid,vals)
        for row_ in col_[1:]:
          temp={}
          if len(row_)==0:
            continue

          name_=row_.split('(')
          unit='cm'
          if len(name_)!=1: 
            unit=name_[1].split(')')[0]
          dimension_num=str(col_.index(row_))
          temp={'name':name_[0],'unit':unit,'dimension_num':dimension_num,'value':0}
          if type(ids) is list:
            temp.update({'taxon_id':ids[0]})
          else:
            temp.update({'taxon_id':ids})
          taxon_dim_obj.create(cr,uid,temp)
