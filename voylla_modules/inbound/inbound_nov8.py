
import re
import shlex
import subprocess
import openerp
import pdb
import psycopg2
import csv
import time
import urllib2
import datetime
import logging
from datetime import datetime
from dateutil.relativedelta import relativedelta
from operator import itemgetter
import time
import xmlrpclib
import os
import ftplib
import socket
from voylla_modules.config import home, applicable_bogos
import openerp
import pdb
from openerp import SUPERUSER_ID
from openerp import pooler, tools
from openerp.osv import fields, osv, expression
from openerp.tools.translate import _
from openerp.tools.float_utils import float_round as round
import pytz
import base64
from openerp import SUPERUSER_ID, workflow
from datetime import datetime
from dateutil.relativedelta import relativedelta
from operator import attrgetter
from openerp.tools.safe_eval import safe_eval as eval
from openerp.osv.orm import browse_record_list, browse_record, browse_null
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT, DATETIME_FORMATS_MAP
from openerp.tools.float_utils import float_compare

import logging
from functools import wraps
import openerp.addons.decimal_precision as dp
def retry(ExceptionToCheck, tries=4, delay=3, backoff=2, logger=None):
    def deco_retry(f):

        @wraps(f)
        def f_retry(*args, **kwargs):
            mtries, mdelay = tries, delay
            while mtries > 1:
                try:
                    return f(*args, **kwargs)
                except ExceptionToCheck, e:
                    msg = "%s, Retrying in %d seconds... product import retry" % (str(e), mdelay)
                    if logger:
                        logger.warning(msg)
                    else:
                        print msg
                    time.sleep(mdelay)
                    mtries -= 1
                    mdelay *= backoff
            return f(*args, **kwargs)

        return f_retry  # true decorator

    return deco_retry

class eanmaker(osv.osv):
	_name='eanmaker'
	_columns={
		'name':fields.char('Name'),
		'country_code':fields.integer('Country Code'),
		'company_code':fields.integer('Company code'),
		'count':fields.integer('Number of ean allocated')
	}
class product_import(osv.osv):
	_name='product.import'
	_columns={
		'name':fields.char('Name'),
		'file':fields.binary('module_file')
	}
	def send_mail(self,cr,uid,vals,context=None):
		email_template_obj = self.pool.get('email.template')
		#template_id =  self.pool.get('email.template').search(cr,uid,[('name','=','Email Template Voylla')])
		#values = email_template_obj.generate_email(cr, uid, template_id[0],ids,context=None)
		values={}
		values['email_from'] = 'help@voylla.com'
		values['email_to'] = 'inbound@voylla.in' 
		values['subject'] = "content update import"
		values["body"] = vals
		values['is_send'] = True
		mail_mail_obj = self.pool.get('mail.mail')
		msg_id = mail_mail_obj.create(cr, uid, values, context=context)
		mail_mail_obj.send(cr, uid, [msg_id], context=context) 
	def import_file(self,cr,uid,ids,context=None):
		
		product_category = self.pool.get('product.category')
		product_object=self.pool.get('product.template')
		data = self.browse(cr, uid, ids[0] , context=context)
		csv_data = base64.decodestring(data.file)

		chars_to_remove = ['\r', ' ', '\n']
		chars_to_remove_1 = ['\r', '\n']
		rx = "[%s]" %(re.escape(''.join(chars_to_remove)))
		rx_1 = "[%s]" %(re.escape(''.join(chars_to_remove_1)))

		print csv_data
		rows = csv_data.split("\n")
		try:
			header = re.sub(rx, '', rows[0]).split(",")
			csv_order_details = rows[1:]
		except:
			pass
		product_details = {}
		mail_vals='product import info \n'
		for product in csv_order_details:
			if product == '':
				continue
			product_details = dict(zip(header, re.sub(rx_1, '', product).split(",")))
			product_ids=product_object.search(cr,uid,[('name','=',product_details['sku'])])
			sku_issue = product_details['sku']
			print sku_issue 
			if len(product_ids)==1:
				vals={}
				if product_details['product_name'] not in [False,'']:
					vals.update({'product_name':product_details['product_name'].replace('$',',')})
				if product_details['description'] not in [False,'']:
					vals.update({'description':product_details['description'].replace('$',',')})
				if product_details['GENDER'] not in [False,'']:
					gender_ids = self.pool.get('product.gender').search(cr,uid,[('name','=',product_details['GENDER'])])
					if len(gender_ids)==1:
						vals.update({'gender':gender_ids[0]})
					else:
						sku_issue += ' gender value is not matched,' 

				if product_details['TAG_TYPE_1'] not in [False,'']:
					categ_id = product_category.search(cr,uid,[('name','=',product_details['TAG_TYPE_1'])])
					if len(categ_id)==0:
						sku_issue += ' type value is not correct,'
					taxon_id = product_category.search(cr,uid,[('name','=',product_details['TAXONS'])])
					if len(taxon_id)==0:
						sku_issue += ' taxon value is not correct,'
					category_flag = True 
					for i in categ_id:
						if taxon_id[0] == product_category.browse(cr,uid,i).parent_id.id:
							vals.update({'taxonomy': [[0, False, {'categ_id': i}]]})
							category_flag = False
							break
					if len(categ_id)!=0 and category_flag:
						vals.update({'taxonomy': [[0, False, {'categ_id': categ_id[0]}]]})

				if product_details['PrimaryBaseColor'] not in [False,'']:
					color_ids = self.pool.get('product.color').search(cr,uid,[('name','=',product_details['PrimaryBaseColor'])])
					if len(color_ids):
						vals.update({'color_one':color_ids[0]})
					else:
						sku_issue += ' Primary color value is not correct,'


				if product_details['SecondaryBaseColor'] not in [False,'']:
					color_ids = self.pool.get('product.seccolor').search(cr,uid,[('name','=',product_details['SecondaryBaseColor'])])
					if len(color_ids):
						vals.update({'color_two':color_ids[0]})
					else:
						sku_issue += ' Secondry color value is not correct,'

				if product_details['PrimaryStoneColor'] not in [False,'']:
					color_ids = self.pool.get('product.gemcolor').search(cr,uid,[('name','=',product_details['PrimaryStoneColor'])])
					if len(color_ids):
						vals.update({'gem_color_one':color_ids[0]})
					else:
						sku_issue += ' primary gemstone value is not correct,'
				if product_details['PrimaryBaseColor'] not in [False,'']:
					color_ids = self.pool.get('product.gemseccolor').search(cr,uid,[('name','=',product_details['SecondaryStoneColor'])])
					if len(color_ids):
						vals.update({'gem_color_two':color_ids[0]})
					else:
						sku_issue += ' secondry gemstone value is not correct,'
				if product_details['THEME'] not in [False,'']:
					theme_id = self.pool.get('product.theme').search(cr,uid,[('name','=',product_details['THEME'])])
					if len(theme_id)==1:
						vals.update({'theme_':theme_id[0]})
					else:
						sku_issue += ' theme value is not correct,'
				if product_details['Design'] not in [False,'']:
					design_id = self.pool.get('product.design').search(cr,uid,[('name','=',product_details['Design'])])
					if len(design_id)==1:
						vals.update({'design':design_id[0]})
					else:
						sku_issue += ' design value is not correct,'
				if product_details['salespackage'] not in [False,'']:
					sale_package_id = self.pool.get('product.salespackage').search(cr,uid,[('name','=',product_details['salespackage'])])
					if len(sale_package_id)==1:
						vals.update({'salespackage':sale_package_id[0]})
					else:
						sale_id=self.pool.get('product.salespackage').create(cr,uid,{'name':product_details['salespackage']})
						vals.update({'salespackage':int(sale_id)})
				if product_details['SurfaceFinish'] not in [False,'']:
					sf_id = self.pool.get('product.surfacefinish').search(cr,uid,[('name','=',product_details['SurfaceFinish'])])
					if len(sf_id)==1:
						vals.update({'surface_finish_one':sf_id[0]})
					else:
						sku_issue += ' surfacefinish value is not correct,'
				if product_details['AgeGroup'] not in [False,'']:
					vals.update({'agegroup':product_details['AgeGroup']})
				else:
					sku_issue += ' agegroup value is not correct,'

				tag_array=[]

				if product_details['TAG_COLLECTION_1'] not in [False,'']:
					tag_id=self.pool.get('product.tagvalue').search(cr,uid,[('name','=',product_details['TAG_COLLECTION_1']),
						('tag_type','=','collection')])
					if len(tag_id)==1:
						tag_array.append([0, False, {'user_type': tag_id[0], 'tag_type': 'collection'}])
					else:
						sku_issue += ' collection tag value is not correct,'
				if product_details['TAG_OCCASION_1'] not in [False,'']:
					tag_id=self.pool.get('product.tagvalue').search(cr,uid,[('name','=',product_details['TAG_OCCASION_1']),
						('tag_type','=','occassion')])
					if len(tag_id)==1:
						tag_array.append([0, False, {'user_type': tag_id[0], 'tag_type': 'occassion'}])
					else:
						sku_issue += ' occassion 1 tag value is not correct,'
				if product_details['TAG_OCCASION_2'] not in [False,'']:
					tag_id=self.pool.get('product.tagvalue').search(cr,uid,[('name','=',product_details['TAG_OCCASION_2']),
						('tag_type','=','occassion')])
					if len(tag_id)==1:
						tag_array.append([0, False, {'user_type': tag_id[0], 'tag_type': 'occassion'}])
					else:
						sku_issue += ' occassion 2 value is not correct,'
				if product_details['TAG_MATERIAL_1'] not in [False,'']:
					tag_id=self.pool.get('product.tagvalue').search(cr,uid,[('name','=',product_details['TAG_MATERIAL_1']),
						('tag_type','=','material')])
					if len(tag_id)==1:
						tag_array.append([0, False, {'user_type': tag_id[0], 'tag_type': 'material'}])
					else:
						sku_issue += ' material 1 value is not correct,'
				if product_details['TAG_MATERIAL_2'] not in [False,'']:
					tag_id=self.pool.get('product.tagvalue').search(cr,uid,[('name','=',product_details['TAG_MATERIAL_2']),
						('tag_type','=','material')])
					if len(tag_id)==1:
						tag_array.append([0, False, {'user_type': tag_id[0], 'tag_type': 'material'}])
					else:
						sku_issue += ' material 2 value is not correct,'
				if product_details['TAG_PLATING_1'] not in [False,'']:
					tag_id=self.pool.get('product.tagvalue').search(cr,uid,[('name','=',product_details['TAG_PLATING_1']),
						('tag_type','=','plating')])
					if len(tag_id)==1:
						tag_array.append([0, False, {'user_type': tag_id[0], 'tag_type': 'plating'}])
					else:
						sku_issue += ' plating  1 value is not correct,'

				if product_details['TAG_PLATING_2'] not in [False,'']:
					tag_id=self.pool.get('product.tagvalue').search(cr,uid,[('name','=',product_details['TAG_PLATING_2']),
						('tag_type','=','plating')])
					if len(tag_id)==1:
						tag_array.append([0, False, {'user_type': tag_id[0], 'tag_type': 'plating'}])
					else:
						sku_issue += ' plating 2 value is not correct,'

				if product_details['TAG_GEMSTONES_1'] not in [False,'']:
					tag_id=self.pool.get('product.tagvalue').search(cr,uid,[('name','=',product_details['TAG_GEMSTONES_1']),
						('tag_type','=','gemstone')])
					if len(tag_id)==1:
						tag_array.append([0, False, {'user_type': tag_id[0], 'tag_type': 'gemstone'}])
					else:
						sku_issue += ' gemstone 1 value is not correct,'


				if product_details['TAG_GEMSTONES_2'] not in [False,'']:
					tag_id=self.pool.get('product.tagvalue').search(cr,uid,[('name','=',product_details['TAG_GEMSTONES_2']),
						('tag_type','=','gemstone')])
					if len(tag_id)==1:
						tag_array.append([0, False, {'user_type': tag_id[0], 'tag_type': 'gemstone'}])
					else:
						sku_issue += ' tag gemstone 2 value is not correct,'

				if product_details['TAG_FREE_PRODUCTS_1'] not in [False,'']:
					tag_id=self.pool.get('product.tagvalue').search(cr,uid,[('name','=',product_details['TAG_FREE_PRODUCTS_1']),
						('tag_type','=','free_product')])
					if len(tag_id)==1:
						tag_array.append([0, False, {'user_type': tag_id[0], 'tag_type': 'free_product'}])
					else:
						sku_issue += ' free_product value is not correct,'


				if product_details['TAG_HOME_PAGE_LINK_1'] not in [False,'']:
					tag_id=self.pool.get('product.tagvalue').search(cr,uid,[('name','=',product_details['TAG_HOME_PAGE_LINK_1']),
						('tag_type','=','home_page')])
					if len(tag_id)==1:
						tag_array.append([0, False, {'user_type': tag_id[0], 'tag_type': 'home_page'}])
					else:
						sku_issue += ' home_page value is not correct,'

				mail_vals += sku_issue+'\n'
				vals.update({'tagid':tag_array})
				product_object.write(cr,uid,product_ids[0],vals)
			else:
				raise osv.except_osv((' sku %s not available ') % (sku_issue,))	
			

class price_import(osv.osv):
	_name='price.import'
	_columns={
		'name':fields.char('Name'),
		'file':fields.binary('module_file')
	}
	@retry(Exception, tries=3, delay=2)
	def update_product_price(self,cr,uid,ids,vals,context=None):
		product_object=self.pool.get('product.template')
		try:
			product_object.browse(cr,uid,ids).write(vals)
		except Exception as e:
			pass
	def import_file(self,cr,uid,ids,context=None):
		product_object=self.pool.get('product.template')
		data = self.browse(cr, uid, ids[0] , context=context)
		csv_data = base64.decodestring(data.file)

		chars_to_remove = ['\r', ' ', '\n']
		chars_to_remove_1 = ['\r', '\n']
		rx = "[%s]" %(re.escape(''.join(chars_to_remove)))
		rx_1 = "[%s]" %(re.escape(''.join(chars_to_remove_1)))
		rows = csv_data.split("\n")
		try:
			header = re.sub(rx, '', rows[0]).split(",")
			csv_order_details = rows[1:]
		except:
			pass
		product_details = {}

		for product in csv_order_details:
			if product == '':
				continue
			product_details = dict(zip(header, re.sub(rx_1, '', product).split(",")))
			product_ids=product_object.search(cr,uid,[('name','=',product_details['sku'])])
			if len(product_ids)==1:
				product=product_object.browse(cr,uid,product_ids)
				vals={}
				if 'mrp' in product_details: 
					vals.update({'mrp':product_details['mrp']})
				if 'sale_price' in product_details:
					vals.update({'list_price':product_details['sale_price']})
				if 'cost_price' in product_details:
					vals.update({'standard_price':product_details['cost_price']})
				vals.update({'update_flag':True})
				try:
					product.write(vals)
				except:
					raise osv.except_osv(('Error'),(' sku %s not available ') % (product_details['sku'],))
				#self.update_product_price(cr,uid,product.id,vals)
			else:
				raise osv.except_osv(('Error'),(' sku %s not available ') % (product_details['sku'],))


	def import_file_product(self,cr,uid,ids,context=None):
		
		product_category = self.pool.get('product.category')
		product_object=self.pool.get('product.template')
		data = self.browse(cr, uid, ids[0] , context=context)
		csv_data = base64.decodestring(data.file)

		chars_to_remove = ['\r', ' ', '\n']
		chars_to_remove_1 = ['\r', '\n']
		rx = "[%s]" %(re.escape(''.join(chars_to_remove)))
		rx_1 = "[%s]" %(re.escape(''.join(chars_to_remove_1)))

		print csv_data
		rows = csv_data.split("\n")
		try:
			header = re.sub(rx, '', rows[0]).split(",")
			csv_order_details = rows[1:]
		except:
			pass
		product_details = {}
		mail_vals='product import info \n'
		for product in csv_order_details:
			if product == '':
				continue
			product_details = dict(zip(header, re.sub(rx_1, '', product).split(",")))
			product_ids=product_object.search(cr,uid,[('name','=',product_details['sku'])])
			sku_issue = product_details['sku'] 
			if len(product_ids)==1:
				vals={}
				if product_details['product_name'] not in [False,'']:
					vals.update({'product_name':product_details['product_name'].replace('$',',')})
				if product_details['description'] not in [False,'']:
					vals.update({'description':product_details['description'].replace('$',',')})
				if product_details['GENDER'] not in [False,'']:
					gender_ids = self.pool.get('product.gender').search(cr,uid,[('name','=',product_details['GENDER'])])
					if len(gender_ids)==1:
						vals.update({'gender':gender_ids[0]})
					else:
						sku_issue += ' gender value is not matched,' 

				if product_details['TAG_TYPE_1'] not in [False,'']:
					categ_id = product_category.search(cr,uid,[('name','=',product_details['TAG_TYPE_1'])])
					if len(categ_id)==0:
						sku_issue += ' type value is not correct,'
					taxon_id = product_category.search(cr,uid,[('name','=',product_details['TAXONS'])])
					if len(taxon_id)==0:
						sku_issue += ' taxon value is not correct,'
					category_flag = True 
					for i in categ_id:
						if taxon_id[0] == product_category.browse(cr,uid,i).parent_id.id:
							vals.update({'taxonomy': [[0, False, {'categ_id': i}]]})
							category_flag = False
							break
					if len(categ_id)!=0 and category_flag:
						vals.update({'taxonomy': [[0, False, {'categ_id': categ_id[0]}]]})

				if product_details['PrimaryBaseColor'] not in [False,'']:
					color_ids = self.pool.get('product.color').search(cr,uid,[('name','=',product_details['PrimaryBaseColor'])])
					if len(color_ids):
						vals.update({'color_one':color_ids[0]})
					else:
						sku_issue += ' Primary color value is not correct,'


				if product_details['SecondaryBaseColor'] not in [False,'']:
					color_ids = self.pool.get('product.seccolor').search(cr,uid,[('name','=',product_details['SecondaryBaseColor'])])
					if len(color_ids):
						vals.update({'color_two':color_ids[0]})
					else:
						sku_issue += ' Secondry color value is not correct,'

				if product_details['PrimaryStoneColor'] not in [False,'']:
					color_ids = self.pool.get('product.gemcolor').search(cr,uid,[('name','=',product_details['PrimaryStoneColor'])])
					if len(color_ids):
						vals.update({'gem_color_one':color_ids[0]})
					else:
						sku_issue += ' primary gemstone value is not correct,'
				if product_details['PrimaryBaseColor'] not in [False,'']:
					color_ids = self.pool.get('product.gemseccolor').search(cr,uid,[('name','=',product_details['SecondaryStoneColor'])])
					if len(color_ids):
						vals.update({'gem_color_two':color_ids[0]})
					else:
						sku_issue += ' secondry gemstone value is not correct,'
				if product_details['THEME'] not in [False,'']:
					theme_id = self.pool.get('product.theme').search(cr,uid,[('name','=',product_details['THEME'])])
					if len(theme_id)==1:
						vals.update({'theme_':theme_id[0]})
					else:
						sku_issue += ' theme value is not correct,'
				if product_details['Design'] not in [False,'']:
					design_id = self.pool.get('product.design').search(cr,uid,[('name','=',product_details['Design'])])
					if len(design_id)==1:
						vals.update({'design':design_id[0]})
					else:
						sku_issue += ' design value is not correct,'
				#if product_details['salespackage'] not in [False,'']:
				#	sale_package_id = self.pool.get('product.salespackage').search(cr,uid,[('name','=',product_details['salespackage'])])
				#	if len(sale_package_id)==1:
				#		vals.update({'salespackage':sale_package_id[0]})
				#	else:
				#		sale_id=self.pool.get('product.salespackage').create(cr,uid,{'name':product_details['salespackage']})
				#		vals.update({'salespackage':int(sale_id)})
				if product_details['SurfaceFinish'] not in [False,'']:
					sf_id = self.pool.get('product.surfacefinish').search(cr,uid,[('name','=',product_details['SurfaceFinish'])])
					if len(sf_id)==1:
						vals.update({'surface_finish_one':sf_id[0]})
					else:
						sku_issue += ' surfacefinish value is not correct,'
				if product_details['AgeGroup'] not in [False,'']:
					vals.update({'agegroup':product_details['AgeGroup']})
				else:
					sku_issue += ' agegroup value is not correct,'

				tag_array=[]

				if product_details['TAG_COLLECTION_1'] not in [False,'']:
					tag_id=self.pool.get('product.tagvalue').search(cr,uid,[('name','=',product_details['TAG_COLLECTION_1']),
						('tag_type','=','collection')])
					if len(tag_id)==1:
						tag_array.append([0, False, {'user_type': tag_id[0], 'tag_type': 'collection'}])
					else:
						sku_issue += ' collection tag value is not correct,'
				if product_details['TAG_OCCASION_1'] not in [False,'']:
					tag_id=self.pool.get('product.tagvalue').search(cr,uid,[('name','=',product_details['TAG_OCCASION_1']),
						('tag_type','=','occassion')])
					if len(tag_id)==1:
						tag_array.append([0, False, {'user_type': tag_id[0], 'tag_type': 'occassion'}])
					else:
						sku_issue += ' occassion 1 tag value is not correct,'
				if product_details['TAG_OCCASION_2'] not in [False,'']:
					tag_id=self.pool.get('product.tagvalue').search(cr,uid,[('name','=',product_details['TAG_OCCASION_2']),
						('tag_type','=','occassion')])
					if len(tag_id)==1:
						tag_array.append([0, False, {'user_type': tag_id[0], 'tag_type': 'occassion'}])
					else:
						sku_issue += ' occassion 2 value is not correct,'
				if product_details['TAG_MATERIAL_1'] not in [False,'']:
					tag_id=self.pool.get('product.tagvalue').search(cr,uid,[('name','=',product_details['TAG_MATERIAL_1']),
						('tag_type','=','material')])
					if len(tag_id)==1:
						tag_array.append([0, False, {'user_type': tag_id[0], 'tag_type': 'material'}])
					else:
						sku_issue += ' material 1 value is not correct,'
				if product_details['TAG_MATERIAL_2'] not in [False,'']:
					tag_id=self.pool.get('product.tagvalue').search(cr,uid,[('name','=',product_details['TAG_MATERIAL_2']),
						('tag_type','=','material')])
					if len(tag_id)==1:
						tag_array.append([0, False, {'user_type': tag_id[0], 'tag_type': 'material'}])
					else:
						sku_issue += ' material 2 value is not correct,'
				if product_details['TAG_PLATING_1'] not in [False,'']:
					tag_id=self.pool.get('product.tagvalue').search(cr,uid,[('name','=',product_details['TAG_PLATING_1']),
						('tag_type','=','plating')])
					if len(tag_id)==1:
						tag_array.append([0, False, {'user_type': tag_id[0], 'tag_type': 'plating'}])
					else:
						sku_issue += ' plating  1 value is not correct,'

				if product_details['TAG_PLATING_2'] not in [False,'']:
					tag_id=self.pool.get('product.tagvalue').search(cr,uid,[('name','=',product_details['TAG_PLATING_2']),
						('tag_type','=','plating')])
					if len(tag_id)==1:
						tag_array.append([0, False, {'user_type': tag_id[0], 'tag_type': 'plating'}])
					else:
						sku_issue += ' plating 2 value is not correct,'

				if product_details['TAG_GEMSTONES_1'] not in [False,'']:
					tag_id=self.pool.get('product.tagvalue').search(cr,uid,[('name','=',product_details['TAG_GEMSTONES_1']),
						('tag_type','=','gemstone')])
					if len(tag_id)==1:
						tag_array.append([0, False, {'user_type': tag_id[0], 'tag_type': 'gemstone'}])
					else:
						sku_issue += ' gemstone 1 value is not correct,'


				if product_details['TAG_GEMSTONES_2'] not in [False,'']:
					tag_id=self.pool.get('product.tagvalue').search(cr,uid,[('name','=',product_details['TAG_GEMSTONES_2']),
						('tag_type','=','gemstone')])
					if len(tag_id)==1:
						tag_array.append([0, False, {'user_type': tag_id[0], 'tag_type': 'gemstone'}])
					else:
						sku_issue += ' tag gemstone 2 value is not correct,'

				if product_details['TAG_FREE_PRODUCTS_1'] not in [False,'']:
					tag_id=self.pool.get('product.tagvalue').search(cr,uid,[('name','=',product_details['TAG_FREE_PRODUCTS_1']),
						('tag_type','=','free_product')])
					if len(tag_id)==1:
						tag_array.append([0, False, {'user_type': tag_id[0], 'tag_type': 'free_product'}])
					else:
						sku_issue += ' free_product value is not correct,'


				if product_details['TAG_HOME_PAGE_LINK_1'] not in [False,'']:
					tag_id=self.pool.get('product.tagvalue').search(cr,uid,[('name','=',product_details['TAG_HOME_PAGE_LINK_1']),
						('tag_type','=','home_page')])
					if len(tag_id)==1:
						tag_array.append([0, False, {'user_type': tag_id[0], 'tag_type': 'home_page'}])
					else:
						sku_issue += ' home_page value is not correct,'

				mail_vals += sku_issue+'\n'
				vals.update({'tagid':tag_array})
				product_object.write(cr,uid,product_ids[0],vals)
			else:
				raise osv.except_osv('Error!','sku  %s' % ( product_details['sku'], ))
	def generate_ean_for_sku(self,cr,uid,ids,context=None):
		product_object=self.pool.get('product.template')
		data = self.browse(cr, uid, ids[0] , context=context)
		csv_data = base64.decodestring(data.file)

		chars_to_remove = ['\r', ' ', '\n']
		chars_to_remove_1 = ['\r', '\n']
		rx = "[%s]" %(re.escape(''.join(chars_to_remove)))
		rx_1 = "[%s]" %(re.escape(''.join(chars_to_remove_1)))
		rows = csv_data.split("\n")
		try:
			header = re.sub(rx, '', rows[0]).split(",")
			csv_order_details = rows[1:]
		except:
			pass
		product_details = {}
		sku_list =[]
		for product in csv_order_details:
			#pdb.set_trace()
			if product == '':
				continue
			product_details = dict(zip(header, re.sub(rx_1, '', product).split(",")))
			sku_list.append(product.replace(" ",""))
		product_ids=product_object.search(cr,uid,[('name','in',sku_list)])
		print product_ids
		product_object.genrate_ean_product_tempalte_ids(cr,uid,product_ids)
							
			
			

class product_surfacefinish(osv.osv):
	_name='product.surfacefinish'
	_columns = {
		'name':fields.char('Surface finish',size=64,required=True)
	}

class product_attribte_value(osv.osv):
	_inherit='product.attribute.value'
	_columns={
		'size_in_float':fields.char('correct size')
	}
class product_taxonomy(osv.osv):
	_name='product.taxonomy'
	_columns = {
		'categ_id': fields.many2one('product.category','Category/Taxon / Type' ,size=64, translate=True,required=True),
        'taxonomy':fields.integer('taxonomy'),
	}
	_defaults={
		'taxonomy':1
	}
class only_property(osv.osv):
    _name = 'property'
    _description = 'Property'
    _columns = {
        'name': fields.char('Name', size=64, translate=True,required=True),
    }

class product_salespackage(osv.osv):
	_name='product.salespackage'
	_columns = {
		'name':fields.char('Name',size=64,required=True)
	}
product_salespackage()

class product_seccolor(osv.osv):
	_name='product.seccolor'
	_columns = {
		'name':fields.char('color',size=64,required=True)
	}
product_seccolor()

class product_color(osv.osv):
	_name='product.color'
	_columns = {
		'name':fields.char('color',size=64,required=True)
	}
product_color()
class product_gemcolor(osv.osv):
	_name='product.gemcolor'
	_columns = {
		'name':fields.char('Gemstone Color',size=64,required=True)
	}
product_gemcolor()
class product_gemseccolor(osv.osv):
	_name='product.gemseccolor'
	_columns = {
		'name':fields.char('Gemstone Secondry Color',size=64,required=True)
	}
product_gemseccolor()

class product_gender(osv.osv):
	_name='product.gender'
	_columns = {
		'name':fields.char('Name',size=64,required=True)
	}
product_gender()
class product_theme(osv.osv):
	_name='product.theme'
	_columns={
		'name':fields.char('Theme',size=64,required=True)
	}
product_theme()
class product_design(osv.osv):
	_name='product.design'
	_columns = {
		'name':fields.char('Name',size=64,required=True)
	}
product_design()
class product_tag_name(osv.osv):
	_name = 'product.tag.name'
	_description = 'Product Tag Name'
	def _user_type_selection(self, cr, uid,context=None):
		return [('embedded', 'Direct link or embed code'), ('emails','Emails'), ]
	def onchange_tag(self,cr,uid,tag_type,context=None):
		tag_value_obj=self.pool.get('product.tagvalue')
		ids=tag_value_obj.search(cr,uid,[('tag_type','=',context)])
		return {'domain':{'user_type':[('id','in',ids)]}}

	_columns = {
    	'tag_type':fields.selection(( ('type','Type'),('collection','Collection'),('material','Material'),
    		('plating','Plating'),('gemstone','Gemstone'),('occassion','Occassion'),
    		('home_page','Home page'),('free_product','Free Product')),'Tag Type',required=True),
		'user_type': fields.many2one('product.tagvalue','Tag Value',required=True),
		'tag_id':fields.many2one('product.template','Product'),
		'tag_value_name':fields.related('user_type','name',type='char',string="Tag Name")

	}

product_tag_name()

class product_taxon(osv.osv):
	_name='product.taxon'
	_columns = {
		'name':fields.char('Name',size=64,required=True),
		'dimension_name':fields.one2many('product.dimension','taxon_id','Dimension')
	}
product_taxon()
class add_voylla_fields_to_supplier(osv.osv):
	_inherit = "res.partner"

	_columns = {
		'brief_bio': fields.text('Brief Bio'),
		'detailed_bio': fields.text('Detailed Bio'),
		'supplier_code': fields.char('Supplier Code', size=5)
	}
add_voylla_fields_to_supplier()
class product_tagvalue(osv.osv):
	_name='product.tagvalue'
	_columns={
		'tag_type':fields.selection(( ('type','Type'),('collection','Collection'),('material','Material'),
    		('plating','Plating'),('gemstone','Gemstone'),('occassion','Occassion'),
    		('home_page','Home page'),('free_product','Free Product')),'Tag Type',required=True),
		'name':fields.char('Tag Name',required=True)

	}
product_tagvalue()

class product_dimension(osv.osv):
	_name='product.dimension'
	_columns = {
		'name' : fields.char('Dimension Name'),
		'unit':fields.selection((('cm','CM'),('mm','MM'),('inches','INCH'),('gms','GRAM'),('carats','Carats') ), 'unit',required=True,select=True),
		'taxon_id':fields.many2one('product.taxon','Taxon'),
		'dimid':fields.integer('product_id'),
		'varid':fields.integer('variant_id'),
		'value':fields.float('Value',required=True),
		'dimension_num':fields.selection((('1','1'),('2','2'),('3','3'),('4','4'),('5','5'),('6','6'),('7','7'),
			('8','8'),('9','9'),('10','10'),('11','11'),('12','12'),('13','13'),('14','14'),
			('15','15'),('16','16'),('17','17'),('18','18'),('19','19'),('20','20'),('21','21'),
			('22','22'),('23','23'),('24','24'),('25','25'),('26','26'),('27','27'),('28','28'),
			('29','29'),('30','30'),('31','31'),('32','32'),('33','33'),('34','34'),('35','35'),
			('36','36'),('37','37'),('38','38'),('39','39'),('40','40') ),'Dimension Number'),
		'taxon_num_id':fields.integer('Taxon Number'),
		
	}
product_dimension()
class add_dimension_inproduct(osv.osv):
	_inherit='product.product'
	_columns={
		'childdimid':fields.one2many('product.dimension','varid','Ref'),
		'manu_cost':fields.float('Manufacturing Cost'),
		'gemstone_price':fields.float('Gemstone Price'),
		'diamond_price':fields.float('Diamond price'),
		'available_on_date':fields.date('Order Date',select=True,copy=False),
		'consignment':fields.boolean('Non-Consign')
	}
	def get_product_size(self,cr,uid,ids,context=None):
		size=self.browse(cr,uid,ids).attribute_value_ids
		size_hash={}
		for i in size:
			size_hash.update({i.attribute_id.name : i.name })
		return size_hash
	def is_product_bogo(self,cr,uid,ids,context=None):
		size=self.browse(cr,uid,ids)
		if len(size.tagid)==0:
			return False
		for tag in size.tagid:
			if tag.tag_type=='free_product':
				return True
		return False

	def get_image_url(self,cr,uid,ids,context=None):
		url = "http://images.voylla.com/i/96248/original/swatch-image20140619-9657-zydon1.jpg?1403131787"
		return url


	def get_product_bogo_tag(self, cr, uid, product_id, context=None):
		tags = self.browse(cr, uid, product_id).tagid
		bogo_tag_values = []
		for tag in tags:
			if tag.tag_type == 'free_product':
				if tag.user_type.name in applicable_bogos:
					bogo_tag_values.append(tag.user_type.name)
		return bogo_tag_values


	def onchange_taxon(self,cr,uid,ids,taxon,context=None):
		if len(ids)!=0:
			dimension_line=self.pool.get('product.dimension')
			ids2=dimension_line.search(cr,uid,[('taxon_id','=',taxon),('varid','in',ids)])
			if len(ids2)!=0:
				vals={'childdimid':dimension_line.browse(cr,uid,ids2),'lst_price':self.browse(cr,uid,ids).lst_price}
				return {'value':vals }
			else:
				ids1=dimension_line.search(cr,uid,[('taxon_id','=',taxon)])
				specific_dimension=dimension_line.read(cr,uid,ids1,['name','unit','dimension_num'])
				taxon_dim=[]
				ids_dimension=[]
				for x in specific_dimension:
					if x['name'] not in taxon_dim and x['dimension_num']!='':
						taxon_dim.append(x['name'])
						temp_vals={'name':x['name'],'unit':x['unit'],'dimension_num':x['dimension_num'],'value':0,'taxon_id':taxon,'varid':ids[0]}
						ids_dimension.append(int(dimension_line.create(cr,uid,temp_vals)))
				vals={'childdimid':dimension_line.browse(cr,uid,ids_dimension),'lst_price':self.browse(cr,uid,ids).lst_price}
				return {'value':vals }

	def name_get_order_line(self, cr, uid, ids, context=None):
		product = self.browse(cr, uid, ids)
		name = product.product_tmpl_id.product_name
		if not name:
			name = product.name
		return name

class combo_product(osv.osv):
	_name='combo.product'
	_columns={
		'product':fields.many2one('product.product','Product',required=True),
		'qty':fields.integer('Quantity'),
		'qty1':fields.integer('Product')
	}
class combo_products(osv.osv):
	_name='combo.products'
	_columns={
		'taxon':fields.many2one('product.taxon','Taxons',required=True),
        'qty':fields.integer('Number of items'),
        'taxon_num':fields.integer('Taxon Number'),
		
		
	}
	_defaults={

		'taxon_num' : 1

	}
class product_template_sync(osv.osv):
	_name='product.template_sync'
	_columns={}
	def push_specific_product_qty_to_channels(self,cr,uid,ids,context=None):
		product_obj = self.pool.get('product.product')
		location = []		
		update_list = []
		min_quantity =1000
		reference_list = []
		product_template = self.pool.get("product.template")
		for product_template_obj in product_template.browse(cr,uid,context['active_ids']):
			for product_product_obj in product_template_obj.product_variant_ids:
				prod_virtual_quant = 0
				available_on='NO'
				product_template_id = product_template_obj.id
				master_ean = product_template_obj.masterean
				selling_price = product_template_obj.list_price
				mrp = product_template_obj.mrp
				product_threep_reference = product_product_obj.threep_reference
				product_ean = product_product_obj.ean13
				product_sku = product_template_obj.name
				product_suppliers = product_product_obj.seller_ids
				leadtime = []
				for supplier in product_suppliers:
					leadtime.append(supplier.delay)
				if leadtime:
					leadtime = min(leadtime)
				try:
					if int(selling_price) < 501:
						shipping_charge = 50
					else:
						shipping_charge = 0
				except:
					shipping_charge = 2
					pass
				if not product_ean in reference_list:
					
					#virtual_quant = product_obj._product_available(cr, uid[product_id],context={'location':loc,'compute_child':True})[product_id]['virtual_available']
					prod_virtual_quant = self.pool.get('queue.stock_bin').get_available_qty(cr,uid,product_product_obj.id)
					mod=False
					product_routes = product_product_obj.route_ids
					if product_routes:
						for route in product_routes:
							if route.name == 'Make To Order':
								mod=True
					if mod:
						prod_virtual_quant = 1000
					#_logger.info(prod_virtual_quant)
					if prod_virtual_quant < min_quantity:
						update_list.append({"available_on":str(available_on),"parent_sku":str(master_ean),"selling_price":str(selling_price),
							"shipping_charge":shipping_charge,"mrp":mrp,"sku":product_sku,"ean":str(product_ean),
							"threep_id": str(product_threep_reference),'leadtime':leadtime, "quantity":prod_virtual_quant})
						reference_list.append(product_ean)
		remote_tasks_obj = self.pool.get('remote.tasks')
		update_list_batches = [update_list[x:x+10] for x in xrange(0, len(update_list), 10)]
		for batch in update_list_batches:
			remote_tasks_obj.update_spree(cr, uid, batch)



class add_voylla_fields_to_product(osv.osv):

	_inherit = "product.template"
	_columns = {
		'mrp':fields.float('MRP'),
		'product_name': fields.char('Product Name'),
		'threep_reference':fields.text('Product Three P Number'),
		'masterean':fields.char('Master Ean',copy=False),
		'taxonomy': fields.one2many('product.taxonomy','taxonomy','Product category',select=1),
		'design': fields.many2one('product.design','Design',select=1),
		'surface_finish_one': fields.many2one('product.surfacefinish','Surface Finish',select=1),
		'gender': fields.many2one('product.gender','Gender',select=1),
		'salespackage': fields.many2one('product.salespackage','Sales Package',select=1),
		'color_one': fields.many2one('product.color',select=2,string="Primary Colour"),
		'color_two': fields.many2one('product.seccolor','Secondry Color',select=1),
		'gem_color_one': fields.many2one('product.gemcolor',select=1,string="Gemstone Primary Colour"),
		'gem_color_two': fields.many2one('product.gemseccolor','Gem Secondry Color',select=1),
		'theme_': fields.many2one('product.theme','Theme',select=1),
		'tagid':fields.one2many('product.tag.name','tag_id','tags'),
		'product_tag_gemstone_one':fields.related('tagid','tag_value_name', type='char', string='Gemstone'),
		'product_tag_collection':fields.related('tagid','tag_value_name', type='char', string='Collection'),
		'product_tag_type':fields.related('tagid','tag_value_name', type='char', string='Type'),
		'product_tag_material':fields.related('tagid','tag_value_name', type='char', string='Material'),
		'supplier_product_code': fields.related('seller_ids','product_code', type='char', string='Supplier Product Code'),
		'childdimid':fields.one2many('product.dimension','dimid','Ref'),
		'taxon':fields.many2one('product.taxon',string="Taxon"),
		'agegroup': fields.selection(( ('0-1 Yrs','Kids'), ('1-9 Yrs','Youth'), ('13-19 Yrs','Men'),('20+ Yrs','All') ),'Age Group',select=1),
		'flag_complete': fields.boolean('Product Completed'),
		'update_flag': fields.boolean('Update Product'),
		'making_charges':fields.float('Making charges per gram'),
		'description': fields.text('Description',translate=True,
            help="A precise description of the Product, used only for internal information purposes."),
		'is_combos':fields.boolean('Comobo'),
		'variant_product':fields.one2many('combo.product','qty1','product for combo'),
		'permalink': fields.text("Permalink"),
		'taxon_list':fields.one2many('combo.products','qty','Combo Taxons'),
		
	}
	_defaults = {
		'flag_complete':False,
		'update_flag':False,
		'valuation': 'manual_periodic',
		'cost_method':'real',
	}
	
	def get_product_taxon(self,cr,uid,ids,context=None):
		taxonomy_id=self.browse(cr,uid,ids).taxonomy
		if len(taxonomy_id)==0:
			return False
		else:
			taxon=taxonomy_id[0].categ_id.complete_name.split('/')
			if len(taxon) > 1:
				return taxon[1].strip()
			else:
				return False
	def genrate_ean_product_tempalte_ids(self , cr , uid , ids ,context=None):
		for product in self.browse(cr,uid,ids):
			for p in product.product_variant_ids:
				if not p.ean13:
					if len(p.get_product_size())==0:
						ean = self.generate_next_ean13_code(cr,uid,context)
						vals={'ean13':ean,'threep_reference':ean,'masterean':ean}
						p.write(vals)
						product.write(vals)
					else:
						ean = self.generate_next_ean13_code(cr,uid,context)
						vals={'ean13':ean,'threep_reference':ean}
						p.write(vals)
						if not product.masterean:
							ean = self.generate_next_ean13_code(cr,uid,context)
							vals={'masterean':ean,'threep_reference':ean}
							product.write(vals)
				cr.commit()





	def button_dummy(self,cr,uid,ids,context=None):
		import json
		product = self.browse(cr,uid,ids)
		dimension_line=self.pool.get('product.dimension')
		combo_obj=self.pool.get('combo.products')
		ids_dimension=[]
		taxon_name=[]
		for combos in product.taxon_list:
			ids1=dimension_line.search(cr,uid,[('taxon_id','=',combos.taxon.id)])
			specific_dimension=dimension_line.read(cr,uid,ids1,['name','unit','dimension_num','taxon_num_id'])
			taxon_name.append(combos.taxon.name)
			taxon_num=combos.taxon_num
			taxon_dim=[]

			for x in specific_dimension:
				if x['name'] not in taxon_dim and x['dimension_num']!='':
					taxon_dim.append(x['name'])
					temp_vals={'name':x['name'],'unit':x['unit'],'dimension_num':x['dimension_num'],'taxon_num_id':taxon_num,
					'value':0,'taxon_id':combos.taxon.id
					,'dimid':ids[0]}
					ids_dimension.append(int(dimension_line.create(cr,uid,temp_vals)))
		vals={'childdimid':dimension_line.browse(cr,uid,ids_dimension),'standard_price':product.standard_price}
		return {'value':vals,'standard_price':product.standard_price }
	def onchange_taxon(self,cr,uid,ids,taxon,context=None):
		if len(ids)!=0:
			product=self.browse(cr,uid,ids)
			dimension_line=self.pool.get('product.dimension')
			ids2=dimension_line.search(cr,uid,[('taxon_id','=',taxon),('dimid','in',ids)])

			if len(ids2)!=0:
				vals={'childdimid':dimension_line.browse(cr,uid,ids2),'standard_price':product.standard_price}
				return {'value':vals}
			else:
				ids1=dimension_line.search(cr,uid,[('taxon_id','=',taxon)])
				specific_dimension=dimension_line.read(cr,uid,ids1,['name','unit','dimension_num'])
				taxon_dim=[]
				ids_dimension=[]
				for x in specific_dimension:
					if x['name'] not in taxon_dim and x['dimension_num']!='':
						taxon_dim.append(x['name'])
						temp_vals={'name':x['name'],'unit':x['unit'],'dimension_num':x['dimension_num'],'value':0,'taxon_id':taxon,'dimid':ids[0]}
						ids_dimension.append(int(dimension_line.create(cr,uid,temp_vals)))
				vals={'childdimid':dimension_line.browse(cr,uid,ids_dimension),'standard_price':product.standard_price}
				return {'value':vals,'standard_price':product.standard_price }
	def generate_next_ean13_code(self,cr,uid,context=None):
		ean_obj=self.pool.get('eanmaker')
		current_ean_id=ean_obj.search(cr,uid,[('name','=','Voylla')])
		if len(current_ean_id)==0:
			vals={}
			vals.update({'name':'Voylla','country_code':890,'company_code':7275,'count':20000})
			current_ean_id.append(ean_obj.create(cr,uid,vals))
		current_ean=ean_obj.browse(cr,uid,current_ean_id)
		twelve_digits_code = int(str(current_ean.country_code) + str(current_ean.company_code) + str(current_ean.count))
		multiply = ["1","3","1","3","1","3","1","3","1","3","1","3"]
		multiply_with = [int(i) for i in str(twelve_digits_code)]
		calculated_results = []
		for idx, val in enumerate(multiply):
			temp=int(val) * int(multiply_with[idx])
			calculated_results.append(temp)
		last_digit=int(str((10 - sum(calculated_results) % 10)).rjust(2, "0"))%10
		ean_obj.write(cr,uid,current_ean_id,{'count':current_ean.count+1})
		return int(str(twelve_digits_code) + str(last_digit))
	def write(self,cr,uid,ids,vals,context=None):
		price={}
		product_obj=self.browse(cr,uid,ids)
		for product in product_obj:
			if product.flag_complete or 'flag_complete' in vals and vals['flag_complete']:
				price.update({'standard_price':product.standard_price})
				price.update({'list_price':product.list_price})
				price.update({'mrp':product.mrp})
				if 'mrp' in vals:
					price.update({'mrp':vals['mrp']})
				if 'standard_price' in vals:
					price.update({'standard_price':vals['standard_price']})
				if 'list_price' in vals:
					price.update({'list_price':vals['list_price']})
				if float(price['standard_price']) > float(price['list_price']):
					raise osv.except_osv('Error!','cost price is greater than selling price')
					return False
				if float(price['list_price']) > float(price['mrp']):
					raise osv.except_osv('Error!','sale price is greater than mrp')
					return False
		temp_id=super(add_voylla_fields_to_product,self).write(cr,uid,ids,vals,context=context)
		return temp_id
class add_fields_purchase_order_line(osv.osv):
	_inherit='purchase.order.line'
	def onchange_product_id(self, cr, uid, ids, pricelist_id, product_id, qty, uom_id,partner_id, date_order=False, fiscal_position_id=False, date_planned=False,name=False, price_unit=False, state='draft', context=None):
		print 'dev'
		if context is None:
			context = {}
		res = {'value': {'price_unit': price_unit or 0.0, 'name': name or '', 'product_uom' : uom_id or False}}
		if not product_id:
			return res
		product_product = self.pool.get('product.product')
		product_uom = self.pool.get('product.uom')
		res_partner = self.pool.get('res.partner')
		product_pricelist = self.pool.get('product.pricelist')
		account_fiscal_position = self.pool.get('account.fiscal.position')
		account_tax = self.pool.get('account.tax')
		context_partner = context.copy()
		if partner_id:
			lang = res_partner.browse(cr, uid, partner_id).lang
			context_partner.update( {'lang': lang, 'partner_id': partner_id} )
		product = product_product.browse(cr, uid, product_id, context=context_partner)
		#call name_get() with partner in the context to eventually match name and description in the seller_ids field
		dummy, name = product_product.name_get(cr, uid, product_id, context=context_partner)[0]
		if product.description_purchase:
			name += '\n' + product.description_purchase
		res['value'].update({'name': name})

		# - set a domain on product_uom
		res['domain'] = {'product_uom': [('category_id','=',product.uom_id.category_id.id)]}
		# - check that uom and product uom belong to the same category
		product_uom_po_id = product.uom_po_id.id
		if not uom_id:
			uom_id = product_uom_po_id

		if product.uom_id.category_id.id != product_uom.browse(cr, uid, uom_id, context=context).category_id.id:
			if context.get('purchase_uom_check') and self._check_product_uom_group(cr, uid, context=context):
				res['warning'] = {'title': _('Warning!'), 'message': _('Selected Unit of Measure does not belong to the same category as the product Unit of Measure.')}
			uom_id = product_uom_po_id

		res['value'].update({'product_uom': uom_id})
		# - determine product_qty and date_planned based on seller info
		if not date_order:
			date_order = fields.datetime.now()
		supplierinfo = False
		precision = self.pool.get('decimal.precision').precision_get(cr, uid, 'Product Unit of Measure')
		for supplier in product.seller_ids:
			if partner_id and (supplier.name.id == partner_id):
				supplierinfo = supplier
				if supplierinfo.product_uom.id != uom_id:
					res['warning'] = {'title': _('Warning!'), 'message': _('The selected supplier only sells this product by %s') % supplierinfo.product_uom.name }
				min_qty = product_uom._compute_qty(cr, uid, supplierinfo.product_uom.id, supplierinfo.min_qty, to_uom_id=uom_id)
				if float_compare(min_qty , qty, precision_digits=precision) == 1: # If the supplier quantity is greater than entered from user, set minimal.
					if qty:
						res['warning'] = {'title': _('Warning!'), 'message': _('The selected supplier has a minimal quantity set to %s %s, you should not purchase less.') % (supplierinfo.min_qty, supplierinfo.product_uom.name)}
					qty = min_qty
		dt = self._get_date_planned(cr, uid, supplierinfo, date_order, context=context).strftime(DEFAULT_SERVER_DATETIME_FORMAT)
		qty = qty or 1.0
		res['value'].update({'date_planned': date_planned or dt})
		if qty:
			res['value'].update({'product_qty': qty})
		price = price_unit
		if price_unit is False or price_unit is None:
			# - determine price_unit and taxes_id
			if pricelist_id:
				date_order_str = datetime.strptime(date_order, DEFAULT_SERVER_DATETIME_FORMAT).strftime(DEFAULT_SERVER_DATE_FORMAT)
				price = product_pricelist.price_get(cr, uid, [pricelist_id],product.id, qty or 1.0, partner_id or False, {'uom': uom_id, 'date': date_order_str})[pricelist_id]
			else:
				price = product.standard_price

		taxes = account_tax.browse(cr, uid, map(lambda x: x.id, product.supplier_taxes_id))
		fpos = fiscal_position_id and account_fiscal_position.browse(cr, uid, fiscal_position_id, context=context) or False
		taxes_ids = account_fiscal_position.map_tax(cr, uid, fpos, taxes)
		res['value'].update({'price_unit': price, 'taxes_id': taxes_ids})
		# change for taxon and manu cost
		res['value'].update({'manu_cost':product.manu_cost})

		if len(product.product_tmpl_id.taxonomy)!=0:
			taxonomy = product.product_tmpl_id.taxonomy[0].categ_id.id
			res['value'].update({'taxonomy':taxonomy})
		if len(product.seller_ids)!=0:
			res['value'].update({'supplier_product_code':product.seller_ids[0].product_code})
		else:
			res['value'].update({'supplier_product_code':product.name})
		if len(product.taxon)!=0:
			res['value'].update({'taxon_id':product.taxon})
		return res
	_columns={
		'supplier_product_code':fields.char('Supplier Product Code'),
		'manu_cost':fields.float('Manufacturing Cost'),
		'taxon_id':fields.many2one('product.taxon','Taxon'),
		'taxonomy':fields.many2one('product.category','Product Category')
	}


class product_from_button(osv.osv):
	_name='product.from_button'
	_columns={}
	def call_another_method(self, cr, uid, ids, context=None):
		#self.pool.get('osv_memory.autovacuum').test(cr,uid,context)
		#model_obj = self.pool.get('warehouse.picklist')
		#self.pool.get("sale.order").check_and_payment_method(cr,uid,context)
		#cr.commit()
		#self.pool.get('spree.cron').product_import(cr, uid, context)
		#self.pool.get('pending.sale.order').get_inventory_pending_specific_id_orders(cr,uid,['5900509528'])
		#self.pool.get('pending.sale.order').get_inventory_pending_orders(cr, uid)
		#self.pool.get('warehouse.picklist').remove_adjustments_amazon_orders_fix(cr, uid, "filename", context=None)
		self.pool.get('gift_and_offer.quantity').gift_coin_adjustment(cr,uid,context=None)

	def genrate_qty_file(self,cr,uid,ids,context=None):
		k=datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
		sql= """COPY(select * from get_all_product_quantity()) TO '/tmp/all_product_qty_"""+k+""".csv' delimiter ',' csv header"""
		cr.execute(sql,(k,))
		k2=str('all_product_qty_'+k+'.csv')
		c1="s3cmd put "+k2+" s3://voyllaerp/erp_tag/"
		c2="s3cmd setacl s3://voyllaerp/erp_tag/all_product_qty_"+k+".csv --acl-public"
		FTP_SERVER = "db01.voylla.com"
		FTP_USER = "ubuntu"
		FEED_LOCATION = "/tmp"
		feed='all_product_qty_'+k+'.csv'
		cmd = "scp %s@%s:%s/%s ." %(FTP_USER, FTP_SERVER, FEED_LOCATION,feed)       ####Get the feed from staging
		args = shlex.split(cmd)
		p = subprocess.Popen(args)
		p.wait()
		convert_zip="gzip "+feed
		args = shlex.split(convert_zip)
		p = subprocess.Popen(args)
		p.wait()
		feed='all_product_qty_'+k+'.csv.gz'
		url_base='https://s3.amazonaws.com/voyllaerp/erp_tag/'+feed
		c1="s3cmd put "+ feed +" s3://voyllaerp/erp_tag/"
		args = shlex.split(c1)
		p = subprocess.Popen(args)
		p.wait()
		c2="s3cmd setacl s3://voyllaerp/erp_tag/"+feed+" --acl-public"
		args = shlex.split(c2)
		subprocess.Popen(args)
		p.wait()
		vals={'name':feed,'type':'url','url':url_base}
		attachment_id=int(self.pool.get('ir.attachment').create(cr,uid,vals))
		os.remove(home+'/'+feed)
		return {
			'domain':"[('id','=',%i)]"%attachment_id,
			'actions':'check_action_attachment',
			'view_type': 'form',
			'view_mode': 'tree,form',
			'res_model': 'ir.attachment',
			'view_id': False,
			'type': 'ir.actions.act_window',
			'search_view_id':attachment_id 
		}
	def genrate_all_product_file(self,cr,uid,ids,context=None):
		k=datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
		sql= """COPY(select * from get_all_product_from_erp()) TO '/tmp/all_product_"""+k+""".csv' delimiter ',' csv header"""
		cr.execute(sql,(k,))
		k2=str('all_product_'+k+'.csv')
		c1="s3cmd put "+k2+" s3://voyllaerp/erp_tag/"
		c2="s3cmd setacl s3://voyllaerp/erp_tag/all_product_"+k+".csv --acl-public"
		FTP_SERVER = "db01.voylla.com"
		FTP_USER = "ubuntu"
		FEED_LOCATION = "/tmp"
		feed='all_product_'+k+'.csv'
		cmd = "scp %s@%s:%s/%s ." %(FTP_USER, FTP_SERVER, FEED_LOCATION,feed)       ####Get the feed from staging
		args = shlex.split(cmd)
		p = subprocess.Popen(args)
		p.wait()
		convert_zip="gzip "+feed
		args = shlex.split(convert_zip)
		p = subprocess.Popen(args)
		p.wait()
		feed='all_product_'+k+'.csv.gz'
		url_base='https://s3.amazonaws.com/voyllaerp/erp_tag/'+feed
		c1="s3cmd put "+ feed +" s3://voyllaerp/erp_tag/"
		args = shlex.split(c1)
		p = subprocess.Popen(args)
		p.wait()
		c2="s3cmd setacl s3://voyllaerp/erp_tag/"+feed+" --acl-public"
		args = shlex.split(c2)
		subprocess.Popen(args)
		p.wait()
		vals={'name':feed,'type':'url','url':url_base}
		attachment_id=int(self.pool.get('ir.attachment').create(cr,uid,vals))
		os.remove(home+'/'+feed)
		return {
			'domain':"[('id','=',%i)]"%attachment_id,
			'actions':'check_action_attachment',
			'view_type': 'form',
			'view_mode': 'tree,form',
			'res_model': 'ir.attachment',
			'view_id': False,
			'type': 'ir.actions.act_window',
			'search_view_id':attachment_id 
		}

	def genrate_cr_report_file(self,cr,uid,ids,context=None):
		k=datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
		sql= """COPY(select * from get_cr_report()) TO '/tmp/cr_report_"""+k+""".csv' delimiter ',' csv header"""
		cr.execute(sql,(k,))
		k2=str('cr_report_'+k+'.csv')
		c1="s3cmd put "+k2+" s3://voyllaerp/reports/"
		c2="s3cmd setacl s3://voyllaerp/reports/cr_report_"+k+".csv --acl-public"
		FTP_SERVER = "db01.voylla.com"
		FTP_USER = "ubuntu"
		FEED_LOCATION = "/tmp"
		feed='cr_report_'+k+'.csv'
		cmd = "scp %s@%s:%s/%s ." %(FTP_USER, FTP_SERVER, FEED_LOCATION,feed)       ####Get the feed from staging
		args = shlex.split(cmd)
		p = subprocess.Popen(args)
		p.wait()
		url_base='https://s3.amazonaws.com/voyllaerp/reports/'+feed
		c1="s3cmd put "+ feed +" s3://voyllaerp/reports/"
		args = shlex.split(c1)
		p = subprocess.Popen(args)
		p.wait()
		c2="s3cmd setacl s3://voyllaerp/reports/"+feed+" --acl-public"
		args = shlex.split(c2)
		subprocess.Popen(args)
		p.wait()
		vals={'name':feed,'type':'url','url':url_base}
		attachment_id=int(self.pool.get('ir.attachment').create(cr,uid,vals))
		os.remove(home+'/'+feed)
		return {
			'domain':"[('id','=',%i)]"%attachment_id,
			'actions':'check_action_attachment',
			'view_type': 'form',
			'view_mode': 'tree,form',
			'res_model': 'ir.attachment',
			'view_id': False,
			'type': 'ir.actions.act_window',
			'search_view_id':attachment_id 
		}

	def genrate_rto_report_file(self,cr,uid,ids,context=None):
		k=datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
		sql= """COPY(select * from get_rto_report()) TO '/tmp/rto_report_"""+k+""".csv' delimiter ',' csv header"""
		cr.execute(sql,(k,))
		k2=str('rto_report_'+k+'.csv')
		c1="s3cmd put "+k2+" s3://voyllaerp/reports/"
		c2="s3cmd setacl s3://voyllaerp/reports/rto_report_"+k+".csv --acl-public"
		FTP_SERVER = "db01.voylla.com"
		FTP_USER = "ubuntu"
		FEED_LOCATION = "/tmp"
		feed='rto_report_'+k+'.csv'
		cmd = "scp %s@%s:%s/%s ." %(FTP_USER, FTP_SERVER, FEED_LOCATION,feed)       ####Get the feed from staging
		args = shlex.split(cmd)
		p = subprocess.Popen(args)
		p.wait()
		url_base='https://s3.amazonaws.com/voyllaerp/reports/'+feed
		c1="s3cmd put "+ feed +" s3://voyllaerp/reports/"
		args = shlex.split(c1)
		p = subprocess.Popen(args)
		p.wait()
		c2="s3cmd setacl s3://voyllaerp/reports/"+feed+" --acl-public"
		args = shlex.split(c2)
		subprocess.Popen(args)
		p.wait()
		vals={'name':feed,'type':'url','url':url_base}
		attachment_id=int(self.pool.get('ir.attachment').create(cr,uid,vals))
		os.remove(home+'/'+feed)
		return {
			'domain':"[('id','=',%i)]"%attachment_id,
			'actions':'check_action_attachment',
			'view_type': 'form',
			'view_mode': 'tree,form',
			'res_model': 'ir.attachment',
			'view_id': False,
			'type': 'ir.actions.act_window',
			'search_view_id':attachment_id 
		}	

	def genrate_shipment_report_file(self,cr,uid,ids,context=None):
		k=datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
		sql= """COPY(select * from get_shipment_report()) TO '/tmp/shipment_report_"""+k+""".csv' delimiter ',' csv header"""
		cr.execute(sql,(k,))
		k2=str('shipment_report_'+k+'.csv')
		c1="s3cmd put "+k2+" s3://voyllaerp/reports/"
		c2="s3cmd setacl s3://voyllaerp/reports/shipment_report_"+k+".csv --acl-public"
		FTP_SERVER = "db01.voylla.com"
		FTP_USER = "ubuntu"
		FEED_LOCATION = "/tmp"
		feed='shipment_report_'+k+'.csv'
		cmd = "scp %s@%s:%s/%s ." %(FTP_USER, FTP_SERVER, FEED_LOCATION,feed)       ####Get the feed from staging
		args = shlex.split(cmd)
		p = subprocess.Popen(args)
		p.wait()
		url_base='https://s3.amazonaws.com/voyllaerp/reports/'+feed
		c1="s3cmd put "+ feed +" s3://voyllaerp/reports/"
		args = shlex.split(c1)
		p = subprocess.Popen(args)
		p.wait()
		c2="s3cmd setacl s3://voyllaerp/reports/"+feed+" --acl-public"
		args = shlex.split(c2)
		subprocess.Popen(args)
		p.wait()
		vals={'name':feed,'type':'url','url':url_base}
		attachment_id=int(self.pool.get('ir.attachment').create(cr,uid,vals))
		os.remove(home+'/'+feed)
		return {
			'domain':"[('id','=',%i)]"%attachment_id,
			'actions':'check_action_attachment',
			'view_type': 'form',
			'view_mode': 'tree,form',
			'res_model': 'ir.attachment',
			'view_id': False,
			'type': 'ir.actions.act_window',
			'search_view_id':attachment_id 
		}	

	def genrate_userlist_file(self,cr,uid,ids,context=None):
		k=datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
		sql= """COPY(select * from get_userlist_report()) TO '/tmp/userlist_report_"""+k+""".csv' delimiter ',' csv header"""
		cr.execute(sql,(k,))
		k2=str('userlist_report_'+k+'.csv')
		c1="s3cmd put "+k2+" s3://voyllaerp/reports/"
		c2="s3cmd setacl s3://voyllaerp/reports/userlist_report_"+k+".csv --acl-public"
		FTP_SERVER = "db01.voylla.com"
		FTP_USER = "ubuntu"
		FEED_LOCATION = "/tmp"
		feed='userlist_report_'+k+'.csv'
		cmd = "scp %s@%s:%s/%s ." %(FTP_USER, FTP_SERVER, FEED_LOCATION,feed)       ####Get the feed from staging
		args = shlex.split(cmd)
		p = subprocess.Popen(args)
		p.wait()
		url_base='https://s3.amazonaws.com/voyllaerp/reports/'+feed
		c1="s3cmd put "+ feed +" s3://voyllaerp/reports/"
		args = shlex.split(c1)
		p = subprocess.Popen(args)
		p.wait()
		c2="s3cmd setacl s3://voyllaerp/reports/"+feed+" --acl-public"
		args = shlex.split(c2)
		subprocess.Popen(args)
		p.wait()
		vals={'name':feed,'type':'url','url':url_base}
		attachment_id=int(self.pool.get('ir.attachment').create(cr,uid,vals))
		os.remove(home+'/'+feed)
		return {
			'domain':"[('id','=',%i)]"%attachment_id,
			'actions':'check_action_attachment',
			'view_type': 'form',
			'view_mode': 'tree,form',
			'res_model': 'ir.attachment',
			'view_id': False,
			'type': 'ir.actions.act_window',
			'search_view_id':attachment_id 
		}

	def generate_order_dump(self,cr,uid,ids,context=None):
		k=datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
		sql= """COPY(select * from order_dump_45_days()) TO '/tmp/order_dump_"""+k+""".csv' delimiter ',' csv header"""
		cr.execute(sql,(k,))
		k2=str('order_dump_'+k+'.csv')
		c1="s3cmd put "+k2+" s3://voylladb-backups/dumps/order_dump/"
		c2="s3cmd setacl s3://voylladb-backups/dumps/order_dump/"+k+".csv --acl-public"
		FTP_SERVER = "db01.voylla.com"
		FTP_USER = "ubuntu"
		FEED_LOCATION = "/tmp"
		feed='order_dump_'+k+'.csv'
		cmd = "scp %s@%s:%s/%s ." %(FTP_USER, FTP_SERVER, FEED_LOCATION,feed)       ####Get the feed from staging
		args = shlex.split(cmd)
		p = subprocess.Popen(args)
		p.wait()
		url_base='https://s3.amazonaws.com/voylladb-backups/dumps/order_dump/'+feed
		c1="s3cmd put "+ feed +" s3://voylladb-backups/dumps/order_dump/"
		args = shlex.split(c1)
		p = subprocess.Popen(args)
		p.wait()
		c2="s3cmd setacl s3://voylladb-backups/dumps/order_dump/"+feed+" --acl-public"
		args = shlex.split(c2)
		subprocess.Popen(args)
		p.wait()
		vals={'name':feed,'type':'url','url':url_base}
		attachment_id=int(self.pool.get('ir.attachment').create(cr,uid,vals))
		os.remove(home+'/'+feed)
		return {
			'domain':"[('id','=',%i)]"%attachment_id,
			'actions':'check_action_attachment',
			'view_type': 'form',
			'view_mode': 'tree,form',
			'res_model': 'ir.attachment',
			'view_id': False,
			'type': 'ir.actions.act_window',
			'search_view_id':attachment_id 
		}

	def all_products_batch_dump(self,cr,uid,ids,context=None):
		k=datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
		change_path = os.chdir('/mnt/voylla-production/current')
		new_dir = "dump_"+k
		new_dir_path = '/mnt/voylla-production/current/'+new_dir
		new_dir_formed = os.makedirs( new_dir, 0777 )
		sql= """COPY(select * from get_all_product_from_erp_modified()) TO '/tmp/all_product_modified_"""+k+""".csv' delimiter ';' csv header"""
		cr.execute(sql,(k,))
		k1=str('all_product_modified_'+k+'.csv')
		sql1= """COPY(select * from get_product_description()) TO '/tmp/all_product_description_"""+k+""".csv' delimiter ',' csv header"""
		cr.execute(sql1,(k,))
		k2=str('all_product_description_'+k+'.csv')
		sql2= """COPY(select * from get_product_dimension()) TO '/tmp/all_products_dimension_"""+k+""".csv' delimiter ',' csv header"""
		cr.execute(sql2,(k,))
		k3=str('all_products_dimension_'+k+'.csv')
		FTP_SERVER = "db01.voylla.com"
		FTP_USER = "ubuntu"
		FEED_LOCATION = "/tmp"
		cmd = "scp %s %s@%s:%s/%s ." %(new_dir_path, FTP_USER, FTP_SERVER, FEED_LOCATION,k1)       ####Get the feed from staging
		args = shlex.split(cmd)
		p = subprocess.Popen(args)
		p.wait()
		cmd = "scp %s %s@%s:%s/%s ." %(new_dir_path, FTP_USER, FTP_SERVER, FEED_LOCATION,k2)       ####Get the feed from staging
		args = shlex.split(cmd)
		p = subprocess.Popen(args)
		p.wait()
		cmd = "scp %s %s@%s:%s/%s ." %(new_dir_path, FTP_USER, FTP_SERVER, FEED_LOCATION,k3)       ####Get the feed from staging
		args = shlex.split(cmd)
		p = subprocess.Popen(args)
		p.wait()
		convert_zip="zip -r"+new_dir+".zip "+new_dir
		args = shlex.split(convert_zip)
		p = subprocess.Popen(args)
		p.wait()
		feed= new_dir+'.zip'
		url_base='https://s3.amazonaws.com/voylladb-backups/dumps/all_products_modified/'+feed
		c1="s3cmd put "+ feed +" s3://voylladb-backups/dumps/order_dump/"
		args = shlex.split(c1)
		p = subprocess.Popen(args)
		p.wait()
		c2="s3cmd setacl s3://voylladb-backups/dumps/all_products_modified/"+feed+" --acl-public"
		args = shlex.split(c2)
		subprocess.Popen(args)
		p.wait()
		vals={'name':feed,'type':'url','url':url_base}
		attachment_id=int(self.pool.get('ir.attachment').create(cr,uid,vals))
		os.remove(feed)
		return {
			'domain':"[('id','=',%i)]"%attachment_id,
			'actions':'check_action_attachment',
			'view_type': 'form',
			'view_mode': 'tree,form',
			'res_model': 'ir.attachment',
			'view_id': False,
			'type': 'ir.actions.act_window',
			'search_view_id':attachment_id 
		}

	def submit_file(self,filename,basename):
		import_file=open(filename, 'rb')
		le = len(import_file.readlines())
		if le!=1:
			ftp = ftplib.FTP("staging.voylla.com")
			ftp.login("ErpPriceUpload", "ErpPriceUpload") #(username, password)
			ftp.storlines("STOR " + basename, open(filename, 'rb'))
	def price_import_file(self,cr,uid,context=None):
		sql="select p.sale_price,p.cost,p.mrp,product.name,product.update_flag from product_price_history as p left join product_template as product on p.product_template_id = product.id  where p.write_date >= NOW() - interval' 7 hours'"
		cr.execute(sql)
		res_val = cr.fetchall()
		product=self.pool.get('product.template')
		d=[]
		header=['sku','product_variant','size','diamond','gold','sale price','cost price','mrp']
		d.append(header)
		for r in res_val:
			a=['']*8
			sale_price=r[0]
			cost=r[1]
			mrp=r[2]
			if mrp > sale_price > cost and r[4]:
				a[0]=r[3]
				a[1]='product'
				a[5]=sale_price
				a[6]=cost
				a[7]=mrp
				d.append(a)
		k=datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
		k='price_'+k+'.csv'
		k1=home+'/import/'+k
		with open(k1, "wb") as f:
			writer = csv.writer(f)
			writer.writerows(d)
			f.close()
		if  os.environ.get("ODOO_ENV")=='production':
			self.submit_file(k1,k)

class add_list_product_price_history(osv.osv):
	_inherit='product.price.history'
	_columns={
		'sale_price':fields.integer('Selling Price'),
		'mrp':fields.integer('MRP')
	}

class add_action_done(osv.osv):
	_inherit='stock.move'
	def action_done(self,cr,uid,ids,context=None):
		stock_picking_qty=self.pool.get('stock.picking.qty')
		stock_move = self.browse(cr,uid,ids)
		for x in stock_move:
			if int(x.location_id.id) == 89 and int(x.location_dest_id.location_id.id) == 84 and x.picking_id !=False and x.origin !=False and 'PO' in x.origin:
				x.product_id.available_on_date=datetime.now()
			elif int(x.location_dest_id.id)== 84:
				raise osv.except_osv("Error!","Can't receive in Manufacturing BIN")
			elif int(x.location_dest_id.id) ==88 and x.picking_id!=False:
				stock_picking_ids = stock_picking_qty.search(cr,uid,[('product_id','=',x.product_id.id),('stock_picking_id','=',x.picking_id.id)])
				if len(stock_picking_ids)==0:
					vals={}
					vals.update({'origin':x.origin,'product_id':x.product_id.id,'stock_picking_id':x.picking_id.id,'qty_total':x.product_uom_qty,'unreserved_qty':x.product_uom_qty})
					stock_picking_qty.create(cr,uid,vals)
				else:
					t=stock_picking_qty.browse(cr,uid,stock_picking_ids[0])
					new_qty = t.qty_total + x.product_uom_qty
					new_unreserved_qty = t.unreserved_qty + x.product_uom_qty
					t.write({'qty_total':new_qty,'unreserved_qty':new_unreserved_qty})
			elif int(x.location_id.id) ==88 and x.picking_id!=False:
				stock_picking_ids = stock_picking_qty.search(cr,uid,[('product_id','=',x.product_id.id),('stock_picking_id','=',x.picking_id.id)])
				if len(stock_picking_ids)!=0:
					t=stock_picking_qty.browse(cr,uid,stock_picking_ids[0])
					new_qty = t.qty_total - x.product_uom_qty
					t.write({'qty_total':new_qty})

		return super(add_action_done , self).action_done(cr,uid,ids,context)


	def make_array_hash(self,cr,uid,ids):
		spree_array=[]
		stock_move_id=self.browse(cr,uid,ids)
		stock_location=self.pool.get('stock.location')
		good_bin_id=stock_location.search(cr,uid,[('name','=','Good BIN')])
		for x in stock_move_id:
			temp={}
			if x.location_dest_id.id==good_bin_id[0]:
				temp.update({'sku':x.product_id.product_tmpl_id.name,'qty':x.product_uom_qty})
				if len(x.product_id.attribute_value_ids)!=0:
					temp.update({'size':x.product_id.attribute_value_ids.name})
				if x.product_id.ean13=='':
					next_ean=self.pool.get('product.template').generate_next_ean13_code(cr,uid,context=None)
					self.pool.get('product.product').write(cr,uid,x.product_id.id,{'ean13':next_ean})
					temp.update({'ean':next_ean})
				else:
					temp.update({'ean':x.product_id.ean13})
				spree_array.append(temp)
		url = 'http://localhost:3000/api/quantity_from_erp'
		req = urllib2.Request(url, data=json.dumps(spree_array))
		response = urllib2.urlopen(req)
	# def action_done(self, cr, uid, ids, context=None):
	# 	self.make_array_hash(cr,uid,ids)
	# 	super(add_action_done,self).action_done(cr,uid,ids,context=None)
	# 	return True


