CREATE TABLE temp (sku text, permalink text);
copy temp from '/tmp/permalinks.csv' DELIMITER ',' CSV HEADER;
select * from temp;
update product_template
set permalink = temp.permalink
from temp
where temp.sku = product_template.name