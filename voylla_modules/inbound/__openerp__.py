{
    'name': "Inbound",
    'version': "1.0",
    'author': "Dheeraj Mathur",
    'category': "Tools",
    'depends': ['stock', 'content', 'hard_tagging','measurement','outbound','photoshoot','quality_control','voylla_warehouse'],
    'data': [
        'security/inbound_security.xml',
        'security/ir.model.access.csv',
        'return_order.xml',
        'product_view.xml',
        'stock_view.xml',
        'partner_view.xml',
        'purchase_view.xml',
        'all_orders_view.xml',
        
    ],
    'demo': [],
    'installable': True,
    'auto_install': True,
    'application': True,
}