from openerp.osv import osv, fields
import contextlib, StringIO
import httplib
httplib.HTTPConnection.debuglevel = 1 
from requests import session
import cookielib
import urllib2
import xml.etree.ElementTree as ET
from os.path import isfile, join
import subprocess
from datetime import date,datetime,timedelta
import shlex
import spur
import ast
import json
import sys
import pdb
import logging
from collections import defaultdict
import getpass
import pytz
import csv
import os
from os import listdir
import pdb
_logger = logging.getLogger(__name__)

@contextlib.contextmanager
def stdout_redirect(where):
    sys.stdout = where
    try:
        yield where
    finally:
        sys.stdout = sys.__stdout__

class flipkart_order_creation(osv.osv):

	USERNAME="orders-flipkart@voylla.com"
	PASSWORD="FK#2105@V0ylla"
	SERVER_URL = "https://seller.flipkart.com/login"
	MAIN_ORDER_URL = "https://seller.flipkart.com/order_management/new_order_items?filterByFullfillment=non_fa&filterByStatus=approved&filterByStatus=on_hold&page_size=20&sellerId=w9cnaikeqptlnvr7&sortBy=dispatch_date"

	payload = {
	    #'action': 'submitLogin',
	    'username': USERNAME,
	    'password': PASSWORD,
	    "authName":"flipkart"
	}
	_name = 'flipkart.order.creation'
	
	def getFeeds(self, ORDER_URL):	
		orders = []
		# sys.stdout = open("xyz","w")
		# sys.stderr = open("abc","w")
		with session() as c:
			x = c.post(self.SERVER_URL, data=self.payload, headers={"Connection":"keep-alive"})
			SID = dict(y.split('=') for y in x.headers['set-cookie'].split(';') if "=" in y)["connect.sid"]
			resp = c.get(ORDER_URL, headers= {"Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8","Accept-Encoding":"gzip, deflate, sdch","Accept-Language":"en-US,en;q=0.8","Connection":"keep-alive","Cookie":"connect.sid="+SID+"; __utmt=1; is_login=true; sellerId=w9cnaikeqptlnvr7; __utma=143439159.2024438333.1421143306.1421143306.1421143306.1; __utmb=143439159.2.10.1421143306; __utmc=143439159; __utmz=143439159.1421143306.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)","Host":"seller.flipkart.com","Referer":"https://seller.flipkart.com/dashboard","User-Agent":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36"}) #{"Cookie":cookie})
			# print x.headers
			# print resp.headers
			# print resp.text
			resp_json = json.loads(resp.text)
			for item in resp_json["items"]:
				orders.append(item)
			if resp_json["hasMore"] == True: # Recursively loading page to fetch all new orders
				self.getFeeds(self.MAIN_ORDER_URL + "&next_page=" + resp_json["nextPage"]) 
				temp_orders = self.getFeeds(self.MAIN_ORDER_URL + "&next_page=" + resp_json["nextPage"]) 
				orders = orders + temp_orders 
		return orders

	def create_orders(self, cr, uid, context=None):
		MAIN_ORDER_URL = "https://seller.flipkart.com/order_management/new_order_items?filterByFullfillment=non_fa&filterByStatus=approved&filterByStatus=on_hold&page_size=20&sellerId=w9cnaikeqptlnvr7&sortBy=dispatch_date"
		with stdout_redirect(StringIO.StringIO()) as fk_stdout:
			details = self.getFeeds(MAIN_ORDER_URL)
		fk_stdout.seek(0)
		_logger.info(fk_stdout.read())
		_logger.info(details)
		for order_details in details:
			order_creation_registry = self.pool.get("threep.order.creation")
			order_registry = self.pool.get('sale.order')
			source_registry = self.pool.get("crm.tracking.source")
			source_id = source_registry.search(cr, uid, [("name","=","Flipkart")])
			if source_id:
				source_id = source_id[0]
			else:
				source_id = source_registry.create(cr, uid, {"name":"Flipkart"})

			orderItemId = order_details['orderItemId']
			check_order = order_registry.search(cr, uid, [("order_id","=",orderItemId)])	
			if check_order:
				existing_order = order_registry.browse(cr, uid, check_order)
				if order_details["statusLabel"] == "Approved" and existing_order.status == "not_confirmed":
					status = "confirmed"
					# existing_order = {}
					# existing_order["id"] = check_order[0]
					order_creation_registry.update_order(cr, uid, existing_order , None, None, None, None, status)


			else:
				address_hash = {}
				address_hash["ship_name"] = order_details["shipping_address_name"]
				if order_details["shipping_address_line2"] is None:
					address_hash["ship_address"] = order_details["shipping_address_line1"]
				else:	
					address_hash["ship_address"] = order_details["shipping_address_line1"] + order_details["shipping_address_line2"] 
				address_hash["ship_zip_code"] = order_details["shipping_address_pincode"]
				address_hash["country_code"] = "India"
				address_hash["state_name"] = order_details["shipping_address_state"]
				address_hash["city_name"] = order_details["shipping_address_city"]
				address_hash["ship_phone"] = order_details["phone"]
				# address_hash["email"] = order_details["shipping_email"]
				address_hash["email"] = "NA"							
				create_date = order_details["orderDate"] + " " +order_details["created_time"].split(" ")[0]
				
				try:
					create_date = datetime.strptime(create_date, "%b %d, %Y %H:%M") - timedelta(hours=5, minutes=30)
				except:
					pass
				
				currency = "INR"
				shipping_type = "3rd_party"
				
				if order_details["statusLabel"] == "Approved":	
					status = "confirmed"
				else:
					status = "not_confirmed"
					payment = "COD"
				
				channel = "Flipkart"
				
				order_id = order_details["orderId"]
				orderItemId = order_details["orderItemId"]
				fsn_no = order_details["fsn"]
				shipping = float(order_details["shippingFees"])
				total_amount = float(order_details["totalPrice"])
				
				line_items = []
				ol_item = {}
				ol_item["sku"] = order_details["sku"]
				ol_item["sale_price"] = int(total_amount-shipping) 
				ol_item["qty"] = order_details["quantity"]
				line_items.append(ol_item)

				order_creation_registry.create_orders(cr, uid, orderItemId, line_items, address_hash, create_date, currency, shipping_type, status, channel, total_amount, shipping)
				cr.commit()

