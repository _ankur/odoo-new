from openerp.osv import osv, fields
import contextlib, StringIO
import httplib
httplib.HTTPConnection.debuglevel = 1 
from requests import session
import urllib2
import xml.etree.ElementTree as ET
from os.path import isfile, join
import subprocess
from datetime import date,datetime,timedelta
import shlex
import spur
import ast
import json
import sys
import pdb
import logging
from collections import defaultdict
import getpass
import pytz
import csv
import os
from os import listdir
import pdb
_logger = logging.getLogger(__name__)

@contextlib.contextmanager
def stdout_redirect(where):
    sys.stdout = where
    try:
        yield where
    finally:
        sys.stdout = sys.__stdout__

class groupon_order_creation(osv.osv):

	_name = 'groupon.order.creation'
	
	data = {
		"j_username":"konica@team2.voylla.com",
		"j_password":"VgRp@765!",
		"_spring_security_remember_me":"on"
	}

	headers={
		"Accept":"application/json, text/javascript, */*; q=0.01",
		"Accept-Encoding":"gzip,deflate",
		"Accept-Language":"en-US,en;q=0.8",
		"Connection":"keep-alive",
		"Content-Length":"83",
		"Content-Type":"application/x-www-form-urlencoded; charset=UTF-8",
		"Cookie":"__utma=161407609.581962238.1433844885.1433844885.1433844885.1; __utmc=161407609; __utmz=161407609.1433844885.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); _ga=GA1.2.581962238.1433844885; JSESSIONID=38C76D7F43E58A84F5F7982CD67B160B; __utmt=1; __utma=179463843.581962238.1433844885.1433844951.1433848878.2; __utmb=179463843.6.8.1433848899855; __utmc=179463843; __utmz=179463843.1433844951.1.1.utmcsr=unicommerce.com|utmccn=(referral)|utmcmd=referral|utmcct=/new",
		"Host":"voyllaretail.unicommerce.com",
		"Origin":"https://voyllaretail.unicommerce.com",
		"Referer":"https://voyllaretail.unicommerce.com/login",
		"User-Agent":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/37.0.2062.120 Chrome/37.0.2062.120 Safari/537.36"
	}
	
	def getFeeds(self):	
		with session() as s:
			p = s.post('https://voyllaretail.unicommerce.com/login_security_check',data=self.data, headers=self.headers)
			SID = dict(y.split('=') for y in p.headers['set-cookie'].split(';') if "=" in y)[' HttpOnly, JSESSIONID']
			resp = s.post('https://voyllaretail.unicommerce.com/data/tasks/export/data',data=json.dumps({"noOfResults":100,"start":0,"columns":["saleOrderNum","created","channel","customer","status","paymentSummary","onHold","productInfo","orderDate","displayOrderCode","channelColorCode","channelShortName","channelName","sourceCode","customerName","customerEmail","customerMobile","customerAddress","paymentMethod","currencyCode","orderPrice","itemTypeSkus","itemTypeIds","sellerSkus","itemIdToDetails","sellerSkuToDetails","shippingMethod","orderPriority","facilityId","tenantId"],"fetchResultCount":"true","filters":[{"id":"dateRangeFilter","dateRange":{"textRange":"LAST_7_DAYS"}}],"sortColumns":[{"column":"created","descending":"true"}],"name":"DATATABLE_SEARCH_ORDERS"}), headers={"Accept":"application/json, text/javascript, */*; q=0.01", "Accept-Encoding":"gzip,deflate", "Accept-Language":"en-US,en;q=0.8", "Connection":"keep-alive", "Content-Type":"application/json", "Cookie":"__utma=161407609.581962238.1433844885.1433844885.1433844885.1; __utmc=161407609; __utmz=161407609.1433844885.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); _ga=GA1.2.581962238.1433844885; SPRING_SECURITY_REMEMBER_ME_COOKIE=a29uaWNhQHRlYW0yLnZveWxsYS5jb206MTQzNTA1OTQ4ODc1Nzo5ZjFhY2YwYzBmYTdmYmVlZmI2MTFlZTk5NWEzZDQ3Yw; JSESSIONID="+SID+"; __utma=179463843.581962238.1433844885.1433844951.1433848878.2; __utmc=179463843; __utmz=179463843.1433844951.1.1.utmcsr=unicommerce.com|utmccn=(referral)|utmcmd=referral|utmcct=/new", "Host":"voyllaretail.unicommerce.com", "Origin":"https://voyllaretail.unicommerce.com", "Referer":"https://voyllaretail.unicommerce.com/orders", "User-Agent":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/37.0.2062.120 Chrome/37.0.2062.120 Safari/537.36"})
			response = json.loads(resp.text)
			details = {}
			for item in response['rows']:
				for i in range (0,len(item)):	
					details_temp = {}
					details_temp['order'] = item['values'][0]
					details_temp['channel'] = item['values'][2]
					details_temp['status'] = item['values'][4]
					details_temp['on hold'] = item['values'][6]
					details_temp['display number'] = item['values'][9]
					details_temp['customer'] = item['values'][14]
					details_temp['email'] = item['values'][15]
					details_temp['phoneno'] = item['values'][16]
					details_temp['address'] = item['values'][17]
					details_temp['payment'] = item['values'][18]
					details_temp['currency'] = item['values'][19]
					details_temp['Amount'] = item['values'][20]
					details_temp['groupon item code'] = item['values'][23]
					details_temp['product'] = item['values'][25]
					details_temp['fulfillment'] = item['values'][26]
					details[item['values'][9]] = details_temp
			return details

	def create_orders(self, cr, uid, context=None):
		state_mapping = {'AP': 'Andhra Pradesh', 'AR': 'Arunachal Pradesh', 'AS': 'Assam', 'BR': 'Bihar', 'CT': 'Chhattisgarh[note 1]', 'GA': 'Goa', 'GJ': 'Gujarat', 'HR': 'Haryana', 'HP': 'Himachal Pradesh', 'JK': 'Jammu and Kashmir', 'JH': 'Jharkhand', 'KA': 'Karnataka', 'KL': 'Kerala', 'MP': 'Madhya Pradesh', 'MH': 'Maharashtra', 'MN': 'Manipur', 'ML': 'Meghalaya', 'MZ': 'Mizoram', 'NL': 'Nagaland', 'OR': 'Odisha[note 2]', 'PB': 'Punjab', 'RJ': 'Rajasthan', 'SK': 'Sikkim', 'TN': 'Tamil Nadu', 'TG': 'Telangana[note 3]', 'TR': 'Tripura', 'UT': 'Uttarakhand[note 4]', 'UP': 'Uttar Pradesh', 'WB': 'West Bengal', 'AN': 'Andaman and Nicobar Islands', 'CH': 'Chandigarh', 'DN': 'Dadra and Nagar Haveli', 'DD': 'Daman and Diu', 'DL': 'Delhi', 'LD': 'Lakshadweep', 'PY': 'Puducherry'}
		with stdout_redirect(StringIO.StringIO()) as groupon_stdout:
			details = self.getFeeds()
		groupon_stdout.seek(0)
		_logger.info(groupon_stdout.read())
		_logger.info(details)
		for key in details:
			order_details = details[key]			
			order_creation_registry = self.pool.get("threep.order.creation")
			order_registry = self.pool.get('sale.order')
			source_registry = self.pool.get("crm.tracking.source")
			source_id = source_registry.search(cr, uid, [("name","=","Groupon")])
			if source_id:
				source_id = source_id[0]
			else:
				source_id = source_registry.create(cr, uid, {"name":"Groupon"})

			orderItemId = order_details['display number']
			check_order = order_registry.search(cr, uid, [("order_id","=",orderItemId)])	
			if check_order:
				existing_order = order_registry.browse(cr, uid, check_order)
				if order_details['status'] == "COMPLETE" and existing_order.status == "not_confirmed":
					status = "confirmed"
					order_creation_registry.update_order(cr, uid, existing_order , None, None, None, None, status)


			else:
				address = (order_details["address"]).split(",")
				length = len(address)
				address_hash = {}
				address_hash["ship_zip_code"] = address[length-1]
				try:	
					address_hash["state_name"] = state_mapping[address[length-2]]
					address_hash["country_code"] = "India"
				except:
					address_hash["state_name"] = "NA"
					address_hash["country_code"] = "NA"
				address_hash["city_name"] = address[length-3]
				address = address[0:length-3]
				address_hash["ship_address"] = ",".join(address)
				address_hash["ship_name"] = order_details["customer"]
				address_hash["ship_phone"] = order_details["phoneno"]
				address_hash["email"] = order_details["email"]							
				create_date = datetime.now()
				
				try:
					create_date = datetime.strptime(create_date, "%Y-%m-%d %H:%M:%S") - timedelta(hours=5, minutes=30)
				except:
					pass
				
				currency = "INR"
				shipping_type = "3rd_party"
				
				if order_details['status'] == "COMPLETE":	
					status = "confirmed"
				else:
					status = "not_confirmed"
				
				if order_details['payment'] == "Prepaid":
					payment_method = "ThirdPartyPayment"
				else:
					payment_method = "CashOnDelivery"

				channel = "Groupon"
				
				product = order_details['product']
				size = product.split('Size: ')[1]
				color = (product.split('Size: ')[0]).split(' Color: ')[1]
				sku = ((product.split('Size: ')[0]).split(' Color: ')[0]).split('-')[1]
				sku_groupon = ((product.split('Size: ')[0]).split(' Color: ')[0]).split('~')[0]

				total_amount = float(order_details["Amount"])
				shipping = 0
				line_items = []
				ol_item = {}
				ol_item["sku"] = sku
				ol_item["sale_price"] = total_amount
				ol_item["qty"] = 1
				line_items.append(ol_item)

				order_creation_registry.create_orders(cr, uid, orderItemId, line_items, address_hash, create_date, currency, shipping_type, status, channel, total_amount, shipping)
				cr.commit()

