from openerp.osv import osv, fields
import os
from os import listdir
from os.path import isfile, join
import subprocess
import datetime
from datetime import date,datetime,timedelta
import shlex
import spur
import ast
import json
import sys
import xml.etree.ElementTree as ET
import pdb
import logging
from collections import defaultdict
import getpass
import pytz
import csv
_logger = logging.getLogger(__name__)

class yepme_order_creation(osv.osv):

	_name = 'yepme.order.creation'

	FTP_SERVER = "ec2-54-251-85-20.ap-southeast-1.compute.amazonaws.com"
	FTP_USER = "root"
	# FEED_LOCATION = "/home/yepme/voylla/new-orders"
	PROCESSED_FEED_LOCATION = "/home/yepme/voylla/new-orders/processed"
	PREFIX = "PartnerOrders_"
	SHIPPED_ORDERS_STATUS_FEED_LOCATION = "/home/yepme/voylla/order-status/"
	
	def getFeeds(self, FEED_LOCATION):																####returns the list of feeds in FEED_LOCATION
		user = getpass.getuser()
		shell = spur.SshShell(hostname=self.FTP_SERVER, username=self.FTP_USER, private_key_file="/home/%s/.ssh/id_rsa.pub" %(user), missing_host_key=spur.ssh.MissingHostKey.accept)
		with shell:
			result = shell.run(["ls", FEED_LOCATION])
			feeds = result.output.decode().split("\n")
		return feeds


	def moveFeed(self, feedName, FEED_LOCATION):
		user = getpass.getuser()
		shell = spur.SshShell(hostname=self.FTP_SERVER, username=self.FTP_USER, private_key_file="/home/%s/.ssh/id_rsa.pub" %(user), missing_host_key=spur.ssh.MissingHostKey.accept)
		with shell:
			shell.run(["mv", FEED_LOCATION+"/"+feedName, self.PROCESSED_FEED_LOCATION+"/"+feedName])


	def readFeed(self, feedName ,FEED_LOCATION ):
		user = getpass.getuser()
		shell = spur.SshShell(hostname=self.FTP_SERVER, username=self.FTP_USER, private_key_file="/home/%s/.ssh/id_rsa.pub" %(user), missing_host_key=spur.ssh.MissingHostKey.accept)
		with shell:
			result = shell.run(["cat", FEED_LOCATION+"/"+feedName])
			return result.output

	def scpFeed(self, file_name):
		cmd = "scp %s %s@%s:%s" %(file_name, self.FTP_USER,self.FTP_SERVER, self.SHIPPED_ORDERS_STATUS_FEED_LOCATION)       #### Send feed to staging
		args = shlex.split(cmd)
		p = subprocess.Popen(args)
		p.wait()
		

	def get_order_details(self, FTP_USER, FTP_SERVER, FEED_LOCATION, context=None):
		details = {}
		
		feeds = self.getFeeds(FEED_LOCATION)
		if feeds == []:
			details["error"] = "No feed found"
		else:
			for feed in feeds:
				if feed.startswith("PartnerOrders_") == False:
					continue
				else:
				# time = feed.split("PartnerOrders_",1)[1].split(".")[0]
				# if time.startswith("Last3days"):
				# 	time = time.split("Last3days")[1]
				# t1 = datetime.now()
				# t2 = datetime.strptime(time,"%Y-%d-%m--%H-%M-%S")
				# if (t1-t2) < timedelta(minutes=lookback_time):
					cmd = "scp %s@%s:%s/%s ." %(FTP_USER, FTP_SERVER, FEED_LOCATION,feed)       ####Get the feed from staging
					args = shlex.split(cmd)
					p = subprocess.Popen(args)
					p.wait()
					# feed = self.readFeed(feed)										###Read the file remotely
					tempdetails = self.parse(feed)	
					_logger.info(tempdetails)
					for key,value in tempdetails.items():										#For multiple feeds Check if its already in the hash. If yes, check status has changed
						if key in details:
							if value != details[key]:
								details[key] = value
						else:
							details[key] = value
				return feed,details

	def parse(self, feed):
		details = {}
		
		csv_file = csv.DictReader(open(feed), dialect='excel', delimiter=',')
		
		for row in csv_file:
			orderNum = row["partner_order_ref"]
			if orderNum not in details:
				details[orderNum] = {}
			details[orderNum]["OrderNum"] = orderNum
			if "SubOrders" not in details[orderNum].keys():
				details[orderNum]["SubOrders"] = {}
			orderID = row["OrderID"]
			details[orderNum]["SubOrders"][orderID] = {}
			details[orderNum]["SubOrders"][orderID]["sku"] = row["SKU"]
			details[orderNum]["SubOrders"][orderID]["qty"] = row["qty"]
			details[orderNum]["SubOrders"][orderID]["price"] = row["selling_price"]
			details[orderNum]["SubOrders"][orderID]["is_delivery_added"] = row["IsDeliveryAdded"]
			details[orderNum]["billing_customer_name"] = row["billing_name"]
			details[orderNum]["billing_email"] = row["billing_email"]
			details[orderNum]["billing_phone"] = row["billing_phone"]
			details[orderNum]["billing_address_1"] = row["billing_address"]
			details[orderNum]["billing_address_2"] = ""
			details[orderNum]["billing_city"] = row["billing_city"]
			details[orderNum]["billing_postalCode"] = row["billing_pincode"]
			details[orderNum]["billing_state"] = row["billing_state"]
			details[orderNum]["billing_country"] = "India"
			details[orderNum]["shipping_customer_name"] = row["shipping_name"]
			details[orderNum]["shipping_email"] = row["shipping_email"]
			details[orderNum]["shipping_phone"] = row["shipping_phone"]
			details[orderNum]["shipping_address_1"] = row["shipping_address"]
			details[orderNum]["shipping_address_2"] = ""
			details[orderNum]["shipping_city"] = row["shipping_city"]
			details[orderNum]["shipping_postalCode"] = row["shipping_pincode"]
			details[orderNum]["shipping_state"] = row["shipping_state"]
			details[orderNum]["shipping_country"] = "India"
			details[orderNum]["payment_mode"] = row["ModeOfPayment"]
		return details
		
	def create_orders(self, cr, uid, FEED_LOCATION, lookback_time, context=None):	
		order_creation_registry = self.pool.get("threep.order.creation")
		order_registry = self.pool.get('sale.order')
		product_mapping = self.pool.get('channel.mapping')
		source_registry = self.pool.get("crm.tracking.source")
		source_id = source_registry.search(cr, uid, [("name","=","Yepme")])
		if source_id:
			source_id = source_id[0]
		else:
			source_id = source_registry.create(cr, uid, {"name":"Yepme"})

		confirmed_order_query = cr.execute("select order_id from sale_order where write_date >= NOW() - '%s Hour'::INTERVAL and source_id=%s and status='confirmed'" %(lookback_time+6, source_id))
		confirmed_order_list = cr.fetchall()
		confirmed_orders = []
				
		order = self.get_order_details(self.FTP_USER,self.FTP_SERVER,FEED_LOCATION)
		feed = order[0]
		details = order[1]
		_logger.info(details)
		order_ids = []
		for order_id, order_details in details.iteritems():
			order_ids.append(order_id)
			check_order = order_registry.search(cr, uid, [("order_id","=",order_id)])	
			if check_order:
				continue

			else:	
				address_hash = {}
				address_hash["ship_name"] = order_details["shipping_customer_name"]
				if order_details["shipping_address_2"] is None:
					address_hash["ship_address"] = order_details["shipping_address_1"] 
				else:	
					address_hash["ship_address"] = order_details["shipping_address_1"] + order_details["shipping_address_2"]
				address_hash["ship_zip_code"] = order_details["shipping_postalCode"]
				address_hash["country_code"] = order_details["shipping_country"]
				state = order_details["shipping_state"]
				if state is not None:
					state = state.title()
				address_hash["state_name"] = state
				address_hash["city_name"] = order_details["shipping_city"]
				address_hash["ship_phone"] = order_details["shipping_phone"]
				address_hash["email"] = order_details["shipping_email"]
								
				create_date = datetime.now()
				
				try:
					create_date = datetime.strptime(create_date, "%Y-%m-%d %H:%M:%S") - timedelta(hours=5, minutes=30)
				except:
					pass

				currency = "INR"
				shipping_type = "standard"
				channel = "Yepme"
				payment_method = order_details["payment_mode"]
				if payment_method == "Credit":
					status = "confirmed"
					payment_method = "ThirdPartyPayment"
				elif payment_method == "COD":
					payment_method = "CashOnDelivery"
					status = "not_confirmed"
					
				dd = defaultdict(list)
				items = order_details["SubOrders"]
				sub_order_id = []
				for x in items:
					sub_order_id.append(x)

				for key in items:
					dd[key].append(items[key])

				
				shipping = 0.0	
				for key in items:
					if items[key]['is_delivery_added'] == '1':
						shipping += 50.0
				
				total_amount = 0.0
				line_items = []
				suborder_codes = []

				for key in dd:
					for rc_item in dd[key]:
						ol_item = {}
						sku = rc_item["sku"]
						product_mapping = self.pool.get('channel.mapping')
						product_id = product_mapping.search(cr, uid, [("three_p_number","=",sku),("channel","=",source_id)])
						sku_l = product_mapping.browse(cr,uid,product_id).voylla_threep_number
						ol_item["sku"] = str(sku_l)
						ol_item["qty"] = rc_item["qty"]
						if items[key]['is_delivery_added'] == '1':
							ol_item["sale_price"] = (int(rc_item["price"])-50) 
						else:	
							ol_item["sale_price"] = rc_item["price"]
						ol_item["order_item_id"] = key 	
						suborder_codes.append(sub_order_id)
						total_amount += float(rc_item["price"])
						line_items.append(ol_item)
				order_creation_registry.create_orders(cr, uid, order_id, line_items, address_hash, create_date, currency, shipping_type, status, channel, total_amount, shipping, None, None, payment_method)
				cr.commit()
		order_ids_db = []
		for item in order_ids:
			order_id = order_registry.search(cr, uid, [("order_id","=",item)])
			order_registry_item = order_registry.browse(cr,uid,order_id)
			order_ids_db.append(order_registry_item.order_id)
		compare = cmp(order_ids, order_ids_db);
		if compare == 0:
			self.moveFeed(feed,FEED_LOCATION)
			os.remove(feed)
		else:
			order_not_formed = list(set(order_ids)-set(order_ids_db))
			for item in order_not_formed:
				vals = {"name": "Failed to create order %s" %(item), "type":"order_creation", "description":"Order creation fails due to feed miss", "order_id":item }
				failure_notification_registry.create(cr, uid, vals)	

	def yepme_order_shipment_status(self, cr, uid, lookback_time, context=None):
		source_registry = self.pool.get("crm.tracking.source")
		order_registry = self.pool.get('sale.order')
		order_line_registry = self.pool.get('sale.order.line')
		product_mapping = self.pool.get('channel.mapping')
		product_product = self.pool.get('product.product')
		source_id = source_registry.search(cr, uid, [("name","=","Yepme")])
		if source_id:
			source_id = source_id[0]
		confirmed_order_query = cr.execute("select id from sale_order where write_date >= NOW() - '%s Hour'::INTERVAL and source_id=%s and state='done'" %(lookback_time, source_id))
		res_val = cr.fetchall()
		ids = [ r[0] for r in res_val]
		header=["partner_order_ref","increment_id","sku","qty_ordered","status"]
		d=[]
		d.append(header)
		for item in ids:
			order_registry_item = order_registry.browse(cr,uid,item)
			order_no = order_registry_item.order_id
			increment_id = order_registry_item.name
			sale_order_line_objs = order_registry_item.order_line
			skus = []	
			for value in sale_order_line_objs:
				a=['']*5	
				value = value.id
				order_line_values = order_line_registry.browse(cr,uid,value)
				productID = order_line_values.product_id[0].id
				order_item_id = order_line_values.order_item_id
				sku = product_product.browse(cr,uid,productID).threep_reference
				skus.append(sku)
				a[0] = order_item_id
				a[1] = increment_id
				a[2] = sku
				a[3] = 1
				a[4] = "Dispatched"
				d.append(a)
		k=datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
		k='Order_Status_'+k+'.csv'
		with open(k, "wb") as f:
			writer = csv.writer(f)
			writer.writerows(d)
			f.close()
		self.scpFeed(k)
		os.remove(k)				
