from openerp.osv import osv, fields
import re
import json
import pdb
from datetime import datetime
from werkzeug.wrappers import BaseResponse as Response
from  psycopg2.extensions import TransactionRollbackError
import os
import logging
_logger = logging.getLogger(__name__)

class third_party_apis(osv.osv):

	_name = 'threep.order.creation'


	def master_cron(self, cr, uid, amazon_lookback, yepme_feed_location, yepme_lookback, ebay_lookback):
		amazon_obj = self.pool.get("amazon.order.creation")
		flipkart_obj = self.pool.get("flipkart.order.creation")
		yepme_obj = self.pool.get("yepme.order.creation")
		ebay_obj = self.pool.get("ebay.order.creation")
		if os.environ.get("ODOO_ENV") == "production":
			amazon_obj.create_orders(cr, uid, amazon_lookback)
			flipkart_obj.create_orders(cr, uid)
			yepme_obj.create_orders(cr, uid, yepme_feed_location, yepme_lookback)
			ebay_obj.create_orders(cr, uid, ebay_lookback)


	def cancel_orders(self, cr, uid, orders_to_cancel, context=None):
		invoice_registry = self.pool.get("account.invoice")
		order_registry = self.pool.get('sale.order')
		if orders_to_cancel:
			invoices_to_cancel = []
			for x in orders_to_cancel:
				invoice_id = order_registry.browse(cr, uid, x).invoice_ids.ids
				invoices_to_cancel = list(set(invoices_to_cancel) | set(invoice_id))
			invoice_registry.write(cr, uid, invoices_to_cancel, {"state":"draft"})
			for oid in orders_to_cancel:
				order_registry.action_cancel(cr, uid, oid)


	def update_order(self, cr, uid, existing_order, line_items, address, adjustments, payments, status, context=None, currency=None):
		order_registry = self.pool.get('sale.order')
		product_registry = self.pool.get("product.product")
		line_item_registry = self.pool.get('sale.order.line')
		product_pricelist_registry = self.pool.get("product.pricelist")
		res_partner_registry = self.pool.get("res.partner")
		state_registry = self.pool.get("res.country.state")
		adjustment_registry = self.pool.get("sale.order.adjustment")
		payment_registry = self.pool.get("sale.order.payment")
		source_registry = self.pool.get("crm.tracking.source")
		country_registry = self.pool.get("res.country")
		currency_registry = self.pool.get("res.currency")
		fiscal_position_registry = self.pool.get("account.fiscal.position")

		if line_items:
			aoqtys = {}
			aoprices = {}
			for line_item in line_items:
				sku = line_item["sku"]
				qty = line_item["qty"]
				price = line_item["sale_price"]
				aoqtys[sku] = int(qty)
				aoprices[sku] = price
			soqtys = {}
			order_lines = existing_order.order_line
			for order_line_obj in order_lines:
				# order_line_obj = line_item_registry.browse(cr, uid, order_line_id)
				order_line_id = order_line_obj.id
				ol_sku = order_line_obj.product_id.threep_reference
				ol_qty = order_line_obj.product_uom_qty
				soqtys[ol_sku] = int(ol_qty)
				ol_mrp = order_line_obj.price_unit
				ol_sp = aoprices[ol_sku]
				if ol_mrp != 0.0:
					# ol_discount = (float(ol_mrp) - float(ol_sp) )/ float(ol_mrp) * 100
					# line_item_registry.write(cr, uid, order_line_obj.id, {"discount":ol_discount})
					line_item_registry.write(cr, uid, order_line_id, {"price_unit": ol_sp})

		order_update = {}
		
		if address:
			order_update["note"] = existing_order.note
			ship_name = address["ship_name"]
			ship_address = address["ship_address"]
			ship_zip_code = address["ship_zip_code"]
			country_code_or_name = address["country_code"]
			state_name = address["state_name"]
			ship_phone = address["ship_phone"]
			email = address["email"]
			city = address.get("city_name")

			numeric = re.compile(r'[^\d]')
			if ship_phone != "NA":
				ship_phone = numeric.sub('', ship_phone)
			if country_code_or_name == "IN" or country_code_or_name == "India":
				if len(ship_phone) == 11 and ship_phone.startswith("0"):
					ship_phone = ship_phone[1:]
				elif len(ship_phone) == 12 and ship_phone.startswith("91"):
					ship_phone = ship_phone[2:]

			customer_id = []
			if ship_phone != "NA":
				customer_id = res_partner_registry.search(cr, uid, [("phone","=",ship_phone), ("customer","=",True)])
			if not customer_id and email != "NA":
				customer_id = res_partner_registry.search(cr, uid, [("email","=",email), ("customer","=",True)])
			if customer_id:
				customer_id = customer_id[0]

			country_id = country_registry.search(cr, uid, ["|", ("code","=",country_code_or_name), ("name", "=", country_code_or_name)])
			if not country_id:
				country_id = None
				order_update["note"] += " | country code not found, please update manually - code=%s" %(country_code_or_name)
			else:
				country_id = country_id[0]

			state_id = state_registry.search(cr, uid, [("name","=",state_name)])
			if not state_id:
				state_id = None
				order_update["note"] += " | state not found, please update manually - name=%s" %(state_name)
			else:
				state_id = state_id[0]

			if customer_id:
				if ship_phone != "NA" and email != "NA":
					address_id = res_partner_registry.create(cr, uid, {"name":ship_name, "active":True, "street":ship_address,
						"city":city, "zip":ship_zip_code, "parent_id":customer_id, "country_id":country_id, "phone":ship_phone, "state_id":state_id, "email":email})
				elif ship_phone != "NA":
					address_id = res_partner_registry.create(cr, uid, {"name":ship_name, "active":True, "street":ship_address,
						"city":city, "zip":ship_zip_code, "parent_id":customer_id, "country_id":country_id, "phone":ship_phone, "state_id":state_id})
				elif email != "NA":
					address_id = res_partner_registry.create(cr, uid, {"name":ship_name, "active":True, "street":ship_address,
						"city":city, "zip":ship_zip_code, "parent_id":customer_id, "country_id":country_id, "state_id":state_id, "email":email})
				else:
					address_id = res_partner_registry.create(cr, uid, {"name":ship_name, "active":True, "street":ship_address,
						"city":city, "zip":ship_zip_code, "parent_id":customer_id, "country_id":country_id, "state_id":state_id})
			else:
				if ship_phone != "NA" and email != "NA":
					customer_id = res_partner_registry.create(cr, uid, {"name":ship_name, "customer":True, "active":True, "street":ship_address,
						"city":city, "zip":ship_zip_code, "country_id":country_id, "phone":ship_phone, "state_id":state_id, "email":email})
				elif ship_phone != "NA":
					customer_id = res_partner_registry.create(cr, uid, {"name":ship_name, "customer":True, "active":True, "street":ship_address,
						"city":city, "zip":ship_zip_code, "country_id":country_id, "phone":ship_phone, "state_id":state_id})
				elif email != "NA":
					customer_id = res_partner_registry.create(cr, uid, {"name":ship_name, "customer":True, "active":True, "street":ship_address,
						"city":city, "zip":ship_zip_code, "country_id":country_id, "state_id":state_id, "email":email})
				else:
					customer_id = res_partner_registry.create(cr, uid, {"name":ship_name, "customer":True, "active":True, "street":ship_address,
						"city":city, "zip":ship_zip_code, "country_id":country_id, "state_id":state_id})

				address_id = customer_id

			order_update["partner_invoice_id"] = address_id
			order_update["partner_shipping_id"] = address_id
			order_update["partner_id"] = customer_id

			fiscal_position = fiscal_position_registry.get_fiscal_position(cr, uid, "", customer_id, address_id)
			order_update["fiscal_position"] = fiscal_position
			order_line_ids = [line.id for line in existing_order.order_line]
			update_order_lines_onchange = [(6, 0, order_line_ids)]
			update_order_lines = order_registry.onchange_fiscal_position(cr, uid, existing_order.id, fiscal_position, update_order_lines_onchange, context=None)
			update_order_lines = update_order_lines.get("value").get("order_line")
			if update_order_lines is not None:
				order_update["order_line"] = update_order_lines


		order_update["status"] = "confirmed"
		if line_items:
			if aoqtys != soqtys:
				note = "This order is partially created. Order contains %s!" %(str(aoqtys))
				if note not in existing_order.note:
					order_update["note"] += " | " + note
		
		if currency is None:
			currency = "INR"

		currency_id = currency_registry.search(cr,uid,[("name","=",currency)])
		pricelist_id = product_pricelist_registry.search(cr, uid, [("type","=","sale"),("sale_type","=","default"),("currency_id","=",currency_id)])
		order_update["currency"] = currency
		order_update["pricelist_id"] = pricelist_id[0]
		order_registry.write(cr, uid, existing_order.id, order_update)

		if adjustments:
			for adjustment in adjustments:
				adjusment_label = adjustment["adjusment_label"]
				adjustment_notes = adjustment["adjustment_notes"]
				amount = adjustment["amount"]
				if float(amount) != 0:
					adjustment_registry.create(cr, uid, {"order_id":existing_order.id, "adjusment_label":adjusment_label, "adjustment_notes":adjustment_notes,
						"amount":amount})
		if payments:
			for payment in payments:
				payment_method = payment["payment_method"]
				payment_reference = payment["payment_reference"]
				amount = payment["amount"]
			existing_payments = order_registry.browse(cr, uid, existing_order.id).payments
			if existing_payments:
				payment_registry.unlink(cr, uid, [payment_obj.id for payment_obj in existing_payments])
			payment_registry.create(cr, uid, {"order_id":existing_order.id, "payment_method":payment_method, "payment_reference":payment_reference, "amount":amount})


	def create_orders(self, cr, uid, order_ref, line_items, address, create_date, currency, shipping_type, status, channel, total_amount, shipping_cost=None, payment_id=None, promotion_discount=None, payment_method=None, context=None):
		order_registry = self.pool.get('sale.order')
		product_registry = self.pool.get("product.product")
		product_tmpl_registry = self.pool.get("product.template")
		line_item_registry = self.pool.get('sale.order.line')
		product_pricelist_registry = self.pool.get("product.pricelist")
		res_partner_registry = self.pool.get("res.partner")
		state_registry = self.pool.get("res.country.state")
		adjustment_registry = self.pool.get("sale.order.adjustment")
		payment_registry = self.pool.get("sale.order.payment")
		source_registry = self.pool.get("crm.tracking.source")
		country_registry = self.pool.get("res.country")
		currency_registry = self.pool.get("res.currency")
		failure_notification_registry = self.pool.get('failure.notification')
		if currency == 'NA':
			currency = "INR"
		currency_id = currency_registry.search(cr,uid,[("name","=",currency)])
		pricelist_id = product_pricelist_registry.search(cr, uid, [("type","=","sale"),("sale_type","=","default"),("currency_id","=",currency_id)])
		try:
			pricelist_id = pricelist_id[0]
		except:
			vals = {"name": "No pricelist for given order %s" %(order_ref), "type":"order_line_creation", "description":"Could not find pricelist for currency %s "%(currency), "order_id":order_ref }
			failure_notification_registry.create(cr, 1, vals)
				
		ship_name = address["ship_name"]
		ship_address = address["ship_address"]
		ship_zip_code = address["ship_zip_code"]
		country_code_or_name = address["country_code"]
		state_name = address["state_name"]
		ship_phone = address["ship_phone"]
		email = address["email"]
		city = address.get("city_name")
		_logger.info("processing order %s" %(order_ref))
		###Consider only numerics and strip leading 0/91 in case of indian numbers:
		numeric = re.compile(r'[^\d]')
		if ship_phone != "NA":
			ship_phone = numeric.sub('', ship_phone)
		if country_code_or_name == "IN" or country_code_or_name == "India":
			if len(ship_phone) == 11 and ship_phone.startswith("0"):
				ship_phone = ship_phone[1:]
			elif len(ship_phone) == 12 and ship_phone.startswith("91"):
				ship_phone = ship_phone[2:]
		##search by phone number, if not found search by email
		customer_id = []
		if ship_phone != "NA":
			customer_id = res_partner_registry.search(cr, uid, [("phone","=",ship_phone), ("customer","=",True)])
		if not customer_id and email != "NA":
			customer_id = res_partner_registry.search(cr, uid, [("email","=",email), ("customer","=",True)])
		if customer_id:
			customer_id = customer_id[0]

		country_id = country_registry.search(cr, uid, ["|", ("code","=",country_code_or_name), ("name", "=", country_code_or_name)])
		notes = str(order_ref)
		if payment_id != None:
			notes += " | %s" %(payment_id)
		if not country_id:
			country_id = None
			notes += " | country code/name not found, please update manually - code=%s" %(country_code_or_name)
		else:
			country_id = country_id[0]

		state_id = state_registry.search(cr, uid, [("name","=",state_name)])
		if not state_id:
			state_id = None
			notes += " | state not found, please update manually - name=%s" %(state_name)
		else:
			state_id = state_id[0]

		if context != None:
			notes += str("| "+context)
		
		try:

			if customer_id:
				if ship_phone != "NA" and email != "NA":
					address_id = res_partner_registry.create(cr, uid, {"name":ship_name, "active":True, "street":ship_address, "parent_id":customer_id,
						"city":city, "zip":ship_zip_code, "customer":True, "country_id":country_id, "phone":ship_phone, "state_id":state_id, "email":email})
				elif ship_phone != "NA":
					address_id = res_partner_registry.create(cr, uid, {"name":ship_name, "active":True, "street":ship_address, "parent_id":customer_id,
						"city":city, "zip":ship_zip_code, "customer":True, "country_id":country_id, "phone":ship_phone, "state_id":state_id})
				elif email != "NA":
					address_id = res_partner_registry.create(cr, uid, {"name":ship_name, "active":True, "street":ship_address, "parent_id":customer_id,
						"city":city, "zip":ship_zip_code, "customer":True, "country_id":country_id, "state_id":state_id, "email":email})
				else:
					address_id = res_partner_registry.create(cr, uid, {"name":ship_name, "active":True, "street":ship_address, "parent_id":customer_id,
						"city":city, "zip":ship_zip_code, "customer":True, "country_id":country_id, "state_id":state_id})


			else:
				if ship_phone != "NA" and email != "NA":
					customer_id = res_partner_registry.create(cr, uid, {"name":ship_name, "customer":True, "active":True, "street":ship_address,
						"city":city, "zip":ship_zip_code, "country_id":country_id, "phone":ship_phone, "state_id":state_id, "email":email})
				elif ship_phone != "NA":
					customer_id = res_partner_registry.create(cr, uid, {"name":ship_name, "customer":True, "active":True, "street":ship_address,
						"city":city, "zip":ship_zip_code, "country_id":country_id, "phone":ship_phone, "state_id":state_id})
				elif email != "NA":
					customer_id = res_partner_registry.create(cr, uid, {"name":ship_name, "customer":True, "active":True, "street":ship_address,
						"city":city, "zip":ship_zip_code, "country_id":country_id, "state_id":state_id, "email":email})
				else:
					customer_id = res_partner_registry.create(cr, uid, {"name":ship_name, "customer":True, "active":True, "street":ship_address,
						"city":city, "zip":ship_zip_code, "country_id":country_id, "state_id":state_id})

				address_id = customer_id

			source_id = source_registry.search(cr, 1, [("name","=",channel)])
			if source_id:
				source_id = source_id[0]
			else:
				source_id = source_registry.create(cr, uid, {"name":channel})

			if isinstance(create_date, datetime):
				order_vals = {"date_order":create_date, "order_id":order_ref, "currency":currency, "shipping_type":shipping_type, "status":status, "note":notes, "pricelist_id":pricelist_id, "partner_id":customer_id, "partner_shipping_id":address_id, "partner_invoice_id":address_id, "source_id":source_id, "is_automated":True}
			else:
				order_vals = {"order_id":order_ref, "currency":currency, "shipping_type":shipping_type, "status":status, "note":notes, "pricelist_id":pricelist_id, "partner_id":customer_id, "partner_shipping_id":address_id, "partner_invoice_id":address_id, "source_id":source_id, "is_automated":True}
			new_order = order_registry.create(cr, uid, order_vals)

			for line_item in line_items:
				sku = line_item["sku"]
				quantity = line_item["qty"]
				sale_price = line_item["sale_price"]
				prod_id = product_registry.search(cr, uid, [("threep_reference","=",sku)])
				if not prod_id:
					notes = order_registry.read(cr, uid, new_order)["note"] + "| product %s not found" %(sku)
					status = "not_confirmed"
					order_registry.write(cr, uid, new_order, {"note":notes, "status":status})
					failure = failure_notification_registry.search(cr, 1, [("order_id", "=", order_ref), ("type","=","order_line_creation")])
					if not failure:
						vals = {"name": "Failed to add product to order %s" %(order_ref), "type":"order_line_creation", "description":"Could not find item ", "order_id":order_ref }
						failure_notification_registry.create(cr, 1, vals)
				else:
					prod_id = prod_id[0]
					product_obj = product_registry.browse(cr, uid, prod_id)
					product_tmpl_obj = product_obj.product_tmpl_id
					mrp = product_tmpl_obj.mrp
					product_name = product_tmpl_obj.product_name
					if product_name is None:
						product_name = sku
					if mrp is not None and mrp != 0:
						if sale_price != "NA":
							# discount = (float(mrp) - float(sale_price))/float(mrp) * 100
							# vals = {"order_id":new_order, "product_id":prod_id, "product_uom":1, "product_uom_qty":float(quantity), "price_unit":mrp,
							# 		"name":product_name, "state":"confirmed","discount":discount}
							vals = {"order_id":new_order, "product_id":prod_id, "product_uom":1, "product_uom_qty":float(quantity), "price_unit":sale_price,
									"name":product_name, "state":"confirmed"}
						else:
							sale_price = product_tmpl_obj.list_price
							vals = {"order_id":new_order, "product_id":prod_id, "product_uom":1, "product_uom_qty":float(quantity), "price_unit":sale_price,
									"name":product_name, "state":"confirmed"}
					else:
						vals = {"order_id":new_order, "product_id":prod_id, "product_uom":1, "product_uom_qty":float(quantity), "price_unit":sale_price,
							"name":product_name, "state":"confirmed"}

					cost_price = product_tmpl_obj.standard_price
					if cost_price is not None and cost_price != 0.0:
						vals["cost_price_unit"] = cost_price
					designer = product_tmpl_obj.seller_id
					if designer:
						vals["product_partner_id"] = designer.id
					if 'order_item_id' in line_item:
						vals['order_item_id']=line_item['order_item_id']
					y = order_registry.add_order_line(cr, uid, vals)
			if total_amount != "NA":
				if payment_id != None:
					payment_vals = {"order_id":new_order, "payment_method":"ThirdPartyPayment", "payment_reference":payment_id, "amount":total_amount}	
				else:
					payment_vals = {"order_id":new_order, "payment_method":"ThirdPartyPayment", "payment_reference":order_ref, "amount":total_amount}
				if payment_method is not None:
					if payment_method in ["CashOnDelivery", "ThirdPartyPayment"]:
						payment_vals["payment_method"] = payment_method
				existing_payments = order_registry.browse(cr, uid, new_order).payments
				if existing_payments:
					payment_registry.unlink(cr, uid, [payment_obj.id for payment_obj in existing_payments])
				payment_registry.create(cr, uid, payment_vals)

			if shipping_cost != None:
				if shipping_cost != 0.0:
					adjustment_registry.create(cr, uid, {"order_id":new_order, "adjusment_label":"standard_shipping", "adjustment_notes":"", "amount":float(shipping_cost)})

			if promotion_discount is not None:
				if promotion_discount != 0.0:
					adjustment_registry.create(cr, uid, {"order_id":new_order, "adjusment_label":"promotion", "adjustment_notes":"%s Promotion" %(channel), "amount":-float(promotion_discount)})
			cr.commit()
		except TransactionRollbackError:
			_logger.info("SKIPPING %s" %(order_ref))

	def amazon_update_orders(self,cr,uid,ids,status,order_ref,line_items,total_amount,address,channel):
		order_obj=self.pool.get('sale.order')
		existing_order = order_obj.browse(cr, uid, ids)
		if status == "not_confirmed":
			return Response(json.dumps(" %s: no updates in pending order" %(order_ref)), status=200, content_type="application/json")
		elif existing_order.status == "confirmed":
			return Response(json.dumps(" %s: duplicate order" %(order_ref)), status=200, content_type="application/json")
		elif status == "confirmed":
			try:
				shipping_cost = sum( i for i in [y["shipping"] for y in line_items])
				adjustments = []
				adjustment = {}
				adjustment["adjusment_label"] = "standard_shipping"
				adjustment["adjustment_notes"] = "Amazon Order Shipping Charge"
				adjustment["amount"] = shipping_cost
				adjustments.append(adjustment)
				promotion_discount = sum( i for i in [y["promotion_discount"] for y in line_items])
				adjustment = {}
				adjustment["adjusment_label"] = "promotion"
				adjustment["adjustment_notes"] = "Amazon Promotion"
				adjustment["amount"] = -promotion_discount
				adjustments.append(adjustment)
				payments = []
				payment = {}
				payment["payment_method"] = "ThirdPartyPayment"
				payment["payment_reference"] = order_ref
				payment["amount"] =  float(total_amount)
				self.update_order(cr, 1, existing_order, line_items, address, adjustments, payments, channel)
				return Response(json.dumps(" %s: Order updated" %(order_ref)), status=200, content_type="application/json")
			except Exception as e:
				_logger.info(" %s: Order update failure %s" %(order_ref,e.message))
				return Response(json.dumps(" %s: Order update failure %s" %(order_ref,e.message)), status=200, content_type="application/json")

	def amazon_int_update_orders(self,cr,uid,ids,status,order_ref,line_items,total_amount,address,channel,currency):
		order_obj=self.pool.get('sale.order')
		existing_order = order_obj.browse(cr, uid, ids)
		if status == "not_confirmed":
			return Response(json.dumps(" %s: no updates in pending order" %(order_ref)), status=200, content_type="application/json")
		elif existing_order.status == "confirmed":
			return Response(json.dumps(" %s: duplicate order" %(order_ref)), status=200, content_type="application/json")
		elif status == "confirmed":
			try:
				shipping_cost = sum( i for i in [y["shipping"] for y in line_items])
				adjustments = []
				adjustment = {}
				adjustment["adjusment_label"] = "standard_shipping"
				adjustment["adjustment_notes"] = "Amazon Order Shipping Charge"
				adjustment["amount"] = shipping_cost
				adjustments.append(adjustment)
				promotion_discount = sum( i for i in [y["promotion_discount"] for y in line_items])
				adjustment = {}
				adjustment["adjusment_label"] = "promotion"
				adjustment["adjustment_notes"] = "Amazon Promotion"
				adjustment["amount"] = -promotion_discount
				adjustments.append(adjustment)
				payments = []
				payment = {}
				payment["payment_method"] = "ThirdPartyPayment"
				payment["payment_reference"] = order_ref
				payment["amount"] =  float(total_amount)
				self.update_order(cr, 1, existing_order, line_items, address, None, payments, status, None, currency)
				return Response(json.dumps(" %s: Order updated" %(order_ref)), status=200, content_type="application/json")
			except Exception as e:
				_logger.info(" %s: Order update failure %s" %(order_ref,e.message))
				return Response(json.dumps(" %s: Order update failure %s" %(order_ref,e.message)), status=200, content_type="application/json")
	
	def flipkart_update_orders(self,cr,uid,ids,status):
		order_obj=self.pool.get('sale.order')
		existing_order = order_obj.browse(cr, uid,ids)
		if status=="confirmed" and existing_order.status == "not_confirmed":
			self.update_order(cr, uid, existing_order , None, None, None, None, status)
			return Response(json.dumps(" %s: Order updated" %(ids)), status=200, content_type="application/json")
		return Response(json.dumps(" %s: Order updated" %(ids)), status=200, content_type="application/json")




