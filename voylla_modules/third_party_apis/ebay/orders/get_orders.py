from openerp.osv import osv, fields
import httplib, ConfigParser, codecs
from xml.dom.minidom import parse, parseString
import xml.etree.ElementTree as ET
from xml.dom import minidom
import subprocess
import shlex
import os
import json
from datetime import datetime,timedelta,time
import tempfile
import sys
import ast
import urllib2
import pdb
import contextlib, StringIO
import logging
_logger = logging.getLogger(__name__)


@contextlib.contextmanager
def stdout_redirect(where):
    sys.stdout = where
    try:
        yield where
    finally:
        sys.stdout = sys.__stdout__

class ebay_order_creation(osv.osv):

    _name = "ebay.order.creation"

    # specify eBay API dev,app,cert IDs
    DEV_ID = "4d4a9e8e-bf21-472f-9ed3-3c539e824af5"
    APP_ID = "Voyllab4a-8867-4bc7-8f6a-dffa45ca38e"
    CERT_ID = "c36f030c-6c06-48bc-a37f-4d3251be5e78"
    #get the server details from the config file
    SERVER_URL = "api.ebay.com"
    SERVER_DIR = "/ws/api.dll"

    # specify eBay token
    # note that eBay requires encrypted storage of user data
    USER_TOKEN = "AgAAAA**AQAAAA**aAAAAA**V+yMVQ**nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6AFk4GkCJSKoQ2dj6x9nY+seQ**yQwCAA**AAMAAA**3KKWZ8Jx7jJNCrh78EJK6xY7j0gX5ehUmOpChcQ2Z0hLDWgJfvJ3oIE9eQxg+myoJIy6lbNox+b80kR5z9a2GtsdvVpGgWprXSNwWt+Z2qg1LMBQlyjqtWCOMIBm4Rdg6OiiiWx2/xuxUVCbq3dmuBJ8pUk/ljS0NJRbwnn6gVhP1C/bZa5VJgTCqqu8fIDqz0coLLDNz/GBCX5Pvf1RDztZT3jJA0zw9eA1/09JkOlhEZgkfTxJhS3+27GOnmzHfR/koFvpfSFEgFk6rHp3pRc28MRwn+g0JZTo5BXiFttxVdXlUkxqaN856JIe2MnZrWCifVa/LNQOrBiRoLRvROIAhxRKBDQHFULApGCEVwmL48zOpc92VDhiPIfRq/ZLm6313WfZSoFbbqDGVS/5V8oDM77hNwHjuhz2T7AtrTtQlTNxz4chQA7dGLVytc0+V1DqYCOY/dIi1xrzYfm87yTZftjpz7+2rZqkrobj7WBAM4nqhi1oFuJir0Z52LAdAtzfnT4KGrIlT2QsxvA+1Ok3Voy1oa2woInS7Jjhu9fv2dZmT3E9cl74TfjKQDKNHBGVHFhKo5gWQodgGcV/h+0tatlcn6Ic+SRnJSy0EUuQjZITzjuof37jHtV+Dupfu/h7ClLtsqPvHN7MfKsJB7ViNmnuSlP0jZzgAwaO6tiinzqI95TO2/nRdCMH11pm49fFCCnqLzfJMzYsp01McZsUurIbcpZqL+WnjzgelLa1bkg51UYmJUqjnmgvwFPP"
    #eBay Call Variables
    SITE_ID = "203"
    #verb specifies the name of the call
    VERB = "GetOrders"
    #The API level that the application conforms to
    COMPATIBILITY_LEVEL = "705"
    namespaces = {'resp': 'urn:ebay:apis:eBLBaseComponents'}

    def get_order_details(self, cr, uid, lookback_time, confirmed_orders):

        time_now = datetime.now()
        hour_now = time_now.strftime('%H')
        time_3 = time(3,0)
        hour_3 = time_3.strftime('%H')
        lookback_time = lookback_time + 6

        ##if time is 3a.m. lookback time is 78 (72 + 5:30 ~ 78) else 18 (12 + 5:30 ~ 18)
        if hour_now == hour_3:
            time_from = (datetime.now()- timedelta(hours=78)).strftime('%Y-%m-%dT%H:%M:%S.000Z')
        else:
            time_from = (datetime.now()- timedelta(hours=lookback_time)).strftime('%Y-%m-%dT%H:%M:%S.000Z')

        time_to = datetime.now().strftime('%Y-%m-%dT%H:%M:%S.000Z')

        # specify the connection to the eBay environment
        connection = httplib.HTTPSConnection(self.SERVER_URL)
        # specify a POST with the results of generateHeaders and generateRequest
        # detailLevel = 1, ViewAllNodes = 1  - this gets the entire tree
        with stdout_redirect(StringIO.StringIO()) as fk_stdout:
            connection.request("POST", self.SERVER_DIR, self.buildRequestXml(time_from, time_to), self.buildHttpHeaders())
            response = connection.getresponse()
        if response.status != 200:
            _logger.info("Error sending request: " + response.reason)
            exit
        else: #response successful
            # store the response data and close the connection
            data = response.read()
            connection.close()
            pretty_xml = minidom.parseString(data)
            pretty_xml_as_string = pretty_xml.toprettyxml()
            _logger.info(pretty_xml_as_string)
            root = ET.fromstring(data)
            has_more = root.find("resp:HasMoreOrders", namespaces=self.namespaces).text
            i = 2
            while has_more == "true":
                root, has_more = self.getNextResponse(time_from, time_to, str(i), root)
                _logger.info(root, has_more)
                i += 1

            details = self.parseResponse(root, confirmed_orders)
            # parse the response data into a DOM
            response = parseString(data)

            # check for any Errors
            errorNodes = response.getElementsByTagName('Errors')
            response.unlink()
            return details
        

    # FUNCTION: buildHttpHeaders
    # Build together the required headers for the HTTP request to the eBay API
    def buildHttpHeaders(self):
        httpHeaders = {"X-EBAY-API-COMPATIBILITY-LEVEL": self.COMPATIBILITY_LEVEL,
                   "X-EBAY-API-DEV-NAME": self.DEV_ID,
                   "X-EBAY-API-APP-NAME": self.APP_ID,
                   "X-EBAY-API-CERT-NAME": self.CERT_ID,
                   "X-EBAY-API-CALL-NAME": self.VERB,
                   "X-EBAY-API-SITEID": self.SITE_ID,
                   "Content-Type": "text/xml"}
        return httpHeaders

    # FUNCTION: buildRequestXml
    # Build the body of the call (in XML) incorporating the required parameters to pass
    def buildRequestXml(self, time_from, time_to):
        requestXml = "<?xml version='1.0' encoding='utf-8'?>"+\
                     "<GetOrdersRequest xmlns=\"urn:ebay:apis:eBLBaseComponents\">"+\
                     "<RequesterCredentials><eBayAuthToken>" + self.USER_TOKEN + "</eBayAuthToken></RequesterCredentials>"+\
                     "<WarningLevel>High</WarningLevel>"+\
                     "<CreateTimeFrom>"+time_from+"</CreateTimeFrom>"+\
                     "<CreateTimeTo>"+time_to+"</CreateTimeTo>"+\
                     "<OrderRole>Seller</OrderRole>"+\
                     "<OrderStatus>All</OrderStatus>"+\
                     "<DetailLevel>ReturnAll</DetailLevel>"+\
                     "<ViewAllNodes>True</ViewAllNodes>"+\
                  "</GetOrdersRequest>"
        return requestXml


    def buildNextRequestXml(self, time_from, time_to, page_number):
        requestXml = "<?xml version='1.0' encoding='utf-8'?>"+\
                     "<GetOrdersRequest xmlns=\"urn:ebay:apis:eBLBaseComponents\">"+\
                     "<RequesterCredentials><eBayAuthToken>" + self.USER_TOKEN + "</eBayAuthToken></RequesterCredentials>"+\
                     "<WarningLevel>High</WarningLevel>"+\
                     "<CreateTimeFrom>"+time_from+"</CreateTimeFrom>"+\
                     "<CreateTimeTo>"+time_to+"</CreateTimeTo>"+\
                     "<OrderRole>Seller</OrderRole>"+\
                     "<OrderStatus>All</OrderStatus>"+\
                     "<DetailLevel>ReturnAll</DetailLevel>"+\
                     "<ViewAllNodes>True</ViewAllNodes>"+\
                     "<Pagination>"+\
                        "<EntriesPerPage>100</EntriesPerPage>"+\
                        "<PageNumber>"+page_number+"</PageNumber>"+\
                     "</Pagination>"+\
                  "</GetOrdersRequest>"
        return requestXml


    def getNextResponse(self, time_from, time_to, page_number, previousRoot):
        connection = httplib.HTTPSConnection(self.SERVER_URL)
        connection.request("POST", self.SERVER_DIR, self.buildNextRequestXml(time_from, time_to, page_number), self.buildHttpHeaders())
        nextResponse = connection.getresponse()
        if nextResponse.status != 200:
            _logger.info("Error sending request: " + response.reason)
        else:
            nextData = nextResponse.read()
            connection.close()
            pretty_xml = minidom.parseString(nextData)
            pretty_xml_as_string = pretty_xml.toprettyxml()
            _logger.info(pretty_xml_as_string)
            nextRoot = ET.fromstring(nextData)
            nextOrderArray = nextRoot.find("resp:OrderArray", namespaces=self.namespaces)
            _logger.info(nextOrderArray)
            previousOrderArray = previousRoot.find("resp:OrderArray", namespaces=self.namespaces)
            has_more = nextRoot.find("resp:HasMoreOrders", namespaces=self.namespaces).text
            for order in nextOrderArray:
                previousOrderArray.append(order)
            _logger.info(previousRoot, has_more)
            return previousRoot, has_more

    

    def parseResponse(self, root, confirmed_orders):
        state_codes = {"AN" : "Andaman and Nicobar", "AP" : "Andhra Pradesh", "AR" : "Arunachal Pradesh", "AS" : "Assam", "BR" : "Bihar", "CH" : "Chandigarh", "CT" : "Chhattisgarh", "DN" : "Dadra and Nagar Haveli", "DD" : "Daman and Diu", "DL" : "Delhi", "GA" : "Goa", "GJ" : "Gujarat", "HR" : "Haryana", "HP" : "Himachal Pradesh", "JK" : "Jammu and Kashmir", "JH" : "Jharkhand", "KA" : "Karnataka", "KL" : "Kerala", "LD" : "Lakshadweep", "MP" : "Madhya Pradesh", "MH" : "Maharashtra", "MN" : "Manipur", "ML" : "Meghalaya", "MZ" : "Mizoram", "NL" : "Nagaland", "OR" : "Odisha", "PY" : "Pondicherry", "PB" : "Punjab", "RJ" : "Rajasthan", "SK" : "Sikkim", "TN" : "Tamil Nadu", "TR" : "Tripura", "UP" : "Uttar Pradesh", "UT" : "Uttrakhand", "WB" : "West Bengal"}
        paisapay_endpoint = "https://shippingservices.ebay.in//psservice/shipping/v1/paisapayId/"
        paisapay_headers = {
        "Accept": "application/json",
        "Authorization": "TOKEN AgAAAA**AQAAAA**aAAAAA**V+yMVQ**nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6AFk4GkCJSKoQ2dj6x9nY+seQ**yQwCAA**AAMAAA**3KKWZ8Jx7jJNCrh78EJK6xY7j0gX5ehUmOpChcQ2Z0hLDWgJfvJ3oIE9eQxg+myoJIy6lbNox+b80kR5z9a2GtsdvVpGgWprXSNwWt+Z2qg1LMBQlyjqtWCOMIBm4Rdg6OiiiWx2/xuxUVCbq3dmuBJ8pUk/ljS0NJRbwnn6gVhP1C/bZa5VJgTCqqu8fIDqz0coLLDNz/GBCX5Pvf1RDztZT3jJA0zw9eA1/09JkOlhEZgkfTxJhS3+27GOnmzHfR/koFvpfSFEgFk6rHp3pRc28MRwn+g0JZTo5BXiFttxVdXlUkxqaN856JIe2MnZrWCifVa/LNQOrBiRoLRvROIAhxRKBDQHFULApGCEVwmL48zOpc92VDhiPIfRq/ZLm6313WfZSoFbbqDGVS/5V8oDM77hNwHjuhz2T7AtrTtQlTNxz4chQA7dGLVytc0+V1DqYCOY/dIi1xrzYfm87yTZftjpz7+2rZqkrobj7WBAM4nqhi1oFuJir0Z52LAdAtzfnT4KGrIlT2QsxvA+1Ok3Voy1oa2woInS7Jjhu9fv2dZmT3E9cl74TfjKQDKNHBGVHFhKo5gWQodgGcV/h+0tatlcn6Ic+SRnJSy0EUuQjZITzjuof37jHtV+Dupfu/h7ClLtsqPvHN7MfKsJB7ViNmnuSlP0jZzgAwaO6tiinzqI95TO2/nRdCMH11pm49fFCCnqLzfJMzYsp01McZsUurIbcpZqL+WnjzgelLa1bkg51UYmJUqjnmgvwFPP"
        }
        shipment_endpoint = "https://shippingservices.ebay.in//psservice/shipping/v1/shipmentType/"
        shipment_headers = paisapay_headers

        order_array = root.find("resp:OrderArray", namespaces=self.namespaces)

        details = {}
        
        for order in order_array.findall('resp:Order', namespaces=self.namespaces):
            orderId = order.find("resp:OrderID", namespaces=self.namespaces).text
            if orderId in confirmed_orders:
                continue
            orderStatus = order.find("resp:OrderStatus", namespaces=self.namespaces).text
            create_date = order.find("resp:CreatedTime", namespaces=self.namespaces).text

            if (orderStatus == "Pending" or orderStatus == "Completed") is not True:
                continue

            details[orderId] = {}
            details[orderId]["orderStatus"] = orderStatus
            details[orderId]["create_date"] = create_date

            #['PaisaPayEscrow', 'COD']
            checkout = order.find('resp:CheckoutStatus', namespaces=self.namespaces)
            paymentMethod = checkout.find('resp:PaymentMethod', namespaces=self.namespaces).text
            status = checkout.find('resp:Status', namespaces=self.namespaces).text
            subtotal = order.find('resp:Subtotal', namespaces=self.namespaces)
            amount = subtotal.text
            currency = subtotal.attrib["currencyID"]

            shippingDetails = order.find('resp:ShippingServiceSelected', namespaces=self.namespaces)
            if shippingDetails is not None:
                shipping_cost = shippingDetails.find('resp:ShippingServiceCost', namespaces=self.namespaces).text
                details[orderId]["shippingCost"] = shipping_cost


            details[orderId]["amount"] = amount
            details[orderId]["currency"] = currency
            details[orderId]["paymentMethod"] = paymentMethod
            details[orderId]["checkoutStatus"] = status


            address = order.find('resp:ShippingAddress', namespaces=self.namespaces)
            if address is not None:
                name = address.find('resp:Name', namespaces=self.namespaces).text
                details[orderId]["name"] = name

                street1 = address.find('resp:Street1', namespaces=self.namespaces).text
                street2 = address.find('resp:Street2', namespaces=self.namespaces).text
                if street2 is not None:
                    street = street1 + street2
                else:
                    street = street1
                details[orderId]["address"] = street

                city = address.find('resp:CityName', namespaces=self.namespaces).text
                details[orderId]["city"] = city

                state = address.find('resp:StateOrProvince', namespaces=self.namespaces).text
                if state in state_codes:
                    state = state_codes[state]
                details[orderId]["state"] = state

                countryCode = address.find('resp:Country', namespaces=self.namespaces).text
                details[orderId]["countryCode"] = countryCode

                country = address.find('resp:CountryName', namespaces=self.namespaces).text
                details[orderId]["country"] =  country

                phone = address.find('resp:Phone', namespaces=self.namespaces)
                if phone is None or phone.text is None:
                    phone = "NA"
                else:
                    phone = phone.text
                details[orderId]["phone"] = phone

                postalCode = address.find('resp:PostalCode', namespaces=self.namespaces).text
                details[orderId]["postalCode"] = postalCode

            transactions = order.find('resp:TransactionArray', namespaces=self.namespaces)
            lineItems = []
            for transaction in transactions.findall('resp:Transaction', namespaces=self.namespaces):
                line_item_id = transaction.find("resp:OrderLineItemID", namespaces=self.namespaces).text
                buyer = transaction.find('resp:Buyer', namespaces=self.namespaces)
                if buyer is not None:
                    email = buyer.find('resp:Email', namespaces=self.namespaces)
                    if email is not None:
                        email = email.text
                    else:
                        email = "NA"
                    details[orderId]["email"] = email
                item = transaction.find('resp:Item', namespaces=self.namespaces)
                line_item = {}
                sku = item.find('resp:SKU', namespaces=self.namespaces).text
                qty = transaction.find('resp:QuantityPurchased', namespaces=self.namespaces).text
                sale_price = transaction.find('resp:TransactionPrice', namespaces=self.namespaces).text
                line_item["sku"] = sku
                line_item["qty"] = qty
                line_item["sale_price"] = sale_price
                lineItems.append(line_item)
            details[orderId]["lineItems"] = lineItems

            # external_transaction = order.find('resp:ExternalTransaction', namespaces=self.namespaces)
            # transaction_id = external_transaction.find('resp:ExternalTransactionID', namespaces=self.namespaces).text
            #print line_item_id
            paisapay_url = paisapay_endpoint + line_item_id
            request = urllib2.Request(paisapay_url, headers=paisapay_headers)
            try:
                _logger.info("Getting paisapay id")
                resp_obj = urllib2.urlopen(request)
                response = json.loads(resp_obj.read())
                paisapayId = response["paisaPayId"]
                details[orderId]["transaction_id"] = paisapayId
            except urllib2.HTTPError, e:
                details[orderId]["transaction_id"] = "Failed to fetch paisaPayID, please update manually"

            shipment_url = shipment_endpoint + line_item_id
            request1 = urllib2.Request(shipment_url, headers=shipment_headers)
            try:
                _logger.info("Getting shipment details")
                resp_obj = urllib2.urlopen(request1)
                response = json.loads(resp_obj.read())
                shipmentType = response["shipmentType"]
                details[orderId]["shipmentType"] = shipmentType
            except urllib2.HTTPError, e:
                details[orderId]["shipmentType"] = "Failed to fetch shipmentType, please update manually"


        return details



    def create_orders(self, cr, uid, lookback_time, context=None):
        order_creation_registry = self.pool.get("threep.order.creation")
        order_registry = self.pool.get('sale.order')
        source_registry = self.pool.get("crm.tracking.source")
        source_id = source_registry.search(cr, uid, [("name","=","Ebay")])
        if source_id:
            source_id = source_id[0]
        else:
            source_id = source_registry.create(cr, uid, {"name":"Ebay"})

        confirmed_order_query = cr.execute("select order_id from sale_order where write_date >= NOW() - '%s Hour'::INTERVAL and source_id=%s and status='confirmed'" %(lookback_time+6, source_id))
        confirmed_order_list = cr.fetchall()
        confirmed_orders = []
        for cot in confirmed_order_list:
            confirmed_orders.append(cot[0])

        details = self.get_order_details(cr, uid, lookback_time, confirmed_orders)
        _logger.info(details)

        for order_id, order_details in details.iteritems():
            order_ref_id = order_details["transaction_id"]
            check_order = order_registry.search(cr, uid, [("order_id","=",order_ref_id)])
            if check_order:
                continue
                ####NEED TO UPDATE FOR PENDING ORDERS
                # existing_order = order_registry.read(cr, uid, check_order[0], ["status", "order_line","partner_shipping_id","note"])
                # if order_details["status"] = "Pending":
                #     continue
                # elif existing_order["status"] == "confirmed":
                #     continue
                # elif order_details["orderStatus"] == "Completed":
                    # ebay_line_items = order_details["lineItems"]
                    # adjustments = []
                    # adjustment = {}
                    # adjustment["adjusment_label"] = "standard_shipping"
                    # adjustment["adjustment_notes"] = "Amazon Order Shipping Charge"
                    # adjustment["amount"] = shipping_cost
                    # adjustments.append(adjustment)
                    # payments = []
                    # payment = {}
                    # payment["payment_method"] = "ThirdPartyPayment"
                    # payment["payment_reference"] = order_id
                    # payment["amount"] =  float(order_details["amount"])
                    # address_hash = {}
                    # address_hash["ship_name"] = order_details["name"]
                    # address_hash["ship_address"] = order_details["address_1"] + order_details["address_2"]
                    # address_hash["ship_zip_code"] = order_details["postalCode"]
                    # address_hash["country_code"] = order_details["countryCode"]
                    # address_hash["state_name"] = order_details["state"]
                    # address_hash["ship_phone"] = order_details["phone"]
                    # address_hash["email"] = "NA"
                    # order_creation_registry.update_order(cr, uid, existing_order, amazon_line_items, address_hash, adjustments, payments, "Amazon")

            else:
                address_hash = {}
                address_hash["ship_name"] = order_details["name"]
                address_hash["ship_address"] = order_details["address"]
                address_hash["ship_zip_code"] = order_details["postalCode"]
                address_hash["country_code"] = order_details["countryCode"]
                address_hash["state_name"] = order_details["state"]
                address_hash["ship_phone"] = order_details["phone"]
                address_hash["email"] = order_details["email"]
                address_hash["city_name"] = order_details["city"]
                create_date = order_details["create_date"]
                try:
                    create_date = datetime.strptime(create_date, "%Y-%m-%dT%H:%M:%S.000Z")
                    create_date = fields.datetime.context_timestamp(cr, uid, create_date, context)
                except:
                    pass
                currency = order_details["currency"]
                shipping_type = order_details["shipmentType"]
                if shipping_type == "POWERSHIP":
                    shipping_type = "3rd_party"
                else:
                    shipping_type = "standard"
                status = order_details["orderStatus"]
                if status == "Completed":
                    status = "confirmed"
                else:
                    status = "not_confirmed"

                shipping_cost = order_details["shippingCost"]
                payment_id = order_details["transaction_id"]

                ebay_line_items = order_details["lineItems"]
                total_amount = order_details["amount"]
                order_creation_registry.create_orders(cr, uid, payment_id, ebay_line_items, address_hash, create_date, currency, shipping_type, status,
                                                        "Ebay", total_amount, shipping_cost, payment_id)


# c = ebay_order_creation()
# print c.get_order_details(1000)
