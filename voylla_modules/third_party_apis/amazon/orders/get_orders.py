from openerp.osv import osv, fields
import shlex
import subprocess
from datetime import datetime,timedelta
from datetime import time as time1
import os
import sys
import xml.etree.ElementTree as ET
import json
import ast
import time as time2
import tempfile
import difflib
import pdb
import logging
_logger = logging.getLogger(__name__)



class amazon_order_creation(osv.osv):
#class amazon_order_creation():

	_name = 'amazon.order.creation'

	SCRIPT_PATH = os.path.dirname(os.path.realpath(__file__)) + "/src/MarketplaceWebServiceOrders/Samples/"
	STATES_POSSIBLE = ["Chhattisgarh","Goa","Sikkim","Meghalaya","Tamil Nadu","Jammu and Kashmir","Madhya Pradesh","Rajasthan","Uttar Pradesh","Uttrakhand","Andhra Pradesh","Dadra and Nagar Haveli","Army Post Office","Nagaland","Jharkhand","Lakshadweep","Maharashtra","Mizoram","Punjab","West Bengal","Tripura","Himachal Pradesh","Arunachal Pradesh","Karnataka","Gujarat","Manipur","Odisha","Haryana","Assam","Chandigarh","Daman and Diu","Andaman and Nicobar","Bihar","Kerala","Pondicherry","Delhi", "Telangana"]
	AMAZON_SHIPPING_LEVELS = ["IN Exp Dom 2","IN Std Dom 2_50k","IN Std Dom 2_50k_cod"]
	namespaces = {'resp': 'https://mws.amazonservices.com/Orders/2013-09-01'}


	def checkNextToken(self, respRoot):
		respResults = respRoot.find("resp:ListOrdersResult", namespaces=self.namespaces)
		respNextToken = respResults.find("resp:NextToken", namespaces=self.namespaces)
		if respNextToken is not None:
			NextToken = respNextToken.text
			return NextToken
		else:
			return None

	def checkNextToken1(self, respRoot):
		respResults = respRoot.find("resp:ListOrdersByNextTokenResult", namespaces=self.namespaces)
		respNextToken = respResults.find("resp:NextToken", namespaces=self.namespaces)
		if respNextToken is not None:
			NextToken = respNextToken.text
			return NextToken
		else:
			return None

	##handleNextToken recursively gets orders corresponding to NextToken and appends them to the previous response file
	def handleNextToken(self, token, previousXmlString, ListOrdersCounter):
		####generate response corresponding to the NextToken
		cmdNext = "php -f ListOrdersByNextToken.php %s" %(token)
		ListOrdersCounter += 1
		argsNext = shlex.split(cmdNext)
		pNext = subprocess.Popen(argsNext, stdout=subprocess.PIPE)
		(stdoutNext, stderrnext) = pNext.communicate()
		nextXmlString = stdoutNext
		previousXmlString = self.mergeXML(previousXmlString, nextXmlString)

		####Now check if the response in nextRespFile has NextToken
		nextRoot = ET.fromstring(nextXmlString)
		nextToken = self.checkNextToken1(nextRoot)
		if nextToken is not None:
			if ListOrdersCounter >= 5:
				time2.sleep(62)
			ListOrdersCounter, previousXmlString = self.handleNextToken(nextToken, previousXmlString, ListOrdersCounter)
		return ListOrdersCounter, previousXmlString


	def mergeXML(self, xmlString1, xmlString2):
		root = ET.fromstring(xmlString1)
		results = root.find("resp:ListOrdersResult", namespaces=self.namespaces)
		order_array = results.find("resp:Orders", namespaces=self.namespaces).getchildren()

		_logger.info(xmlString2)
		root1 = ET.fromstring(xmlString2)
		results1 = root1.find("resp:ListOrdersByNextTokenResult", namespaces=self.namespaces)
		order_array1 = results1.find("resp:Orders", namespaces=self.namespaces).getchildren()

		for order in order_array1:
			order_array.append(order)

		correct_data = ET.tostring(root).replace('ns0:', '').replace(':ns0','')
		return correct_data


	def get_order_details(self, cr, uid, lookback_time, confirmed_orders, context=None):

		ListOrdersCounter = 0
		ListOrderItemsCounter = 0

		time_now = datetime.now()
		hour_now = time_now.strftime('%H')
		time_3 = time1(3,0)
		hour_3 = time_3.strftime('%H')
		lookback_time = lookback_time + 6
		##if time is 3a.m. lookback time is 78 (72 + 5:30 ~ 78) else 18 (12 + 5:30 ~ 18)
		if hour_now == hour_3:
			time = (datetime.now()- timedelta(hours=78)).strftime('%Y-%m-%dT%H:%M:%S') 
		else:
			time = (datetime.now()- timedelta(hours=lookback_time)).strftime('%Y-%m-%dT%H:%M:%S')

		##ListOrders API call would give order details for orders created after a given point of time
		current_path = os.getcwd()
		os.chdir(self.SCRIPT_PATH)
		cmd = "php -f ListOrders.php %s" %(time)
		ListOrdersCounter += 1
		args = shlex.split(cmd)
		p = subprocess.Popen(args, stdout=subprocess.PIPE)
		(stdout1, stderr) = p.communicate()
		xmlString = stdout1
		_logger.info(xmlString)


		##Handle next token if present
		root = ET.fromstring(xmlString)
		nextToken = self.checkNextToken(root)
		if nextToken is not None:
			ListOrdersCounter, xmlString = self.handleNextToken(nextToken, xmlString, ListOrdersCounter)

		root = ET.fromstring(xmlString)
		results = root.find("resp:ListOrdersResult", namespaces=self.namespaces)
		nextToken = results.find("resp:NextToken", namespaces=self.namespaces)
		order_array = results.find("resp:Orders", namespaces=self.namespaces)

		amznIds = []
		items = {}
		error = []
		canceled = []

		for child in order_array.findall('resp:Order', namespaces=self.namespaces):
			status = child.find("resp:OrderStatus", namespaces=self.namespaces).text
			##skip FBA orders
			channel = child.find("resp:FulfillmentChannel", namespaces=self.namespaces).text
			if channel == "AFN":
				continue
			##only unshipped or pending orders considered for order creation
			amznId = child.find("resp:AmazonOrderId", namespaces=self.namespaces).text
			if status == "Unshipped" or status == "Pending":
				amznIds.append(amznId)
			if status == "Canceled":
				canceled.append(amznId)

		_logger.info(amznIds)


		#print amznIds
		for order in amznIds:
			if order in confirmed_orders:
				continue
			items[order] = []
			##ListOrderItems API call gives the item level details (sku, qty etc.) for a given amazon order number
			cmd = "php -f ListOrderItems.php %s" %(order)
			ListOrderItemsCounter += 1
			args = shlex.split(cmd)
			p = subprocess.Popen(args, stdout=subprocess.PIPE)
			(stdout, stderr) = p.communicate()
			itemXmlString = stdout
			if ListOrderItemsCounter >= 29:
				time2.sleep(2)						##sleep for 2 seconds, restore rate for he API call is 1 request/2 secs
			try:
				rootItem = ET.fromstring(itemXmlString)
			except ET.ParseError:
				error.append(order)
				_logger.info("ET.ParseError for %s" %(order))
				continue
			results = rootItem.find("resp:ListOrderItemsResult", namespaces=self.namespaces)
			item_array = results.find("resp:OrderItems", namespaces=self.namespaces)
			for child in item_array.findall('resp:OrderItem', namespaces=self.namespaces):
				temp = {}
				sku = child.find("resp:SellerSKU", namespaces=self.namespaces).text
				qty = child.find("resp:QuantityOrdered", namespaces=self.namespaces).text
				ship = child.find("resp:ShippingPrice", namespaces=self.namespaces)
				if ship is not None:
					ship_amount = float(ship.find("resp:Amount", namespaces=self.namespaces).text)
				else:
					ship_amount = "NA"
				price_elem = child.find("resp:ItemPrice", namespaces=self.namespaces)
				if price_elem is not None:
					sale_price = float(price_elem.find("resp:Amount", namespaces=self.namespaces).text)
					sale_price = sale_price/int(qty)
				else:
					sale_price = "NA"
				promotion_discount_element = child.find("resp:PromotionDiscount", namespaces=self.namespaces)
				if promotion_discount_element is not None:
					promotion_discount = float(promotion_discount_element.find("resp:Amount", namespaces=self.namespaces).text)
				else:
					promotion_discount = "NA"

				temp["sku"] = sku
				temp["qty"] = qty
				temp["shipping"] = ship_amount
				temp["sale_price"] = sale_price
				temp["promotion_discount"] = promotion_discount
				items[order].append(temp)

		details = self.parse(root)
		# print details

		rdict = {}
		rdict["items"] = items
		rdict["details"] = details
		rdict["error"] = error
		rdict["canceled"] = canceled
		_logger.info(rdict)
		os.chdir(current_path)
		return rdict



	def parse(self,root):
		# print ET.tostring(root)
		results = root.find("resp:ListOrdersResult", namespaces=self.namespaces)
		order_array = results.find("resp:Orders", namespaces=self.namespaces)
		detailsHash = {}
		i=0
		for order in order_array.findall('resp:Order', namespaces=self.namespaces):
			amazon_id = order.find('resp:AmazonOrderId', namespaces=self.namespaces).text
			status = order.find('resp:OrderStatus', namespaces=self.namespaces).text
			if status != "Pending" and status != "Unshipped":
				continue
			details = {}
			details['status'] = status
			create_date = order.find('resp:PurchaseDate', namespaces=self.namespaces).text
			details['create_date'] = create_date
			channel = order.find('resp:FulfillmentChannel', namespaces=self.namespaces).text
			details['channel'] = channel
			address = order.find('resp:ShippingAddress', namespaces=self.namespaces)
			if address is not None:
				name = address.find('resp:Name', namespaces=self.namespaces).text
				details['name']= name
				address_1 = address.find('resp:AddressLine1', namespaces=self.namespaces)
				if address_1 is not None:
					address_1 = address_1.text
				else:
					address_1 = "address line 1 not specified in the feed"
				address_2 = address.find('resp:AddressLine2', namespaces=self.namespaces)
				if address_2 is not None:
					address_2 = address_2.text
				else:
					address_2 = " "
				details['address_1']=address_1
				details['address_2']=address_2
				city = address.find('resp:City', namespaces=self.namespaces)
				if city is not None:
					city = city.text
				else:
					city = "city not specified"
				details['city']= city
				state = address.find('resp:StateOrRegion', namespaces=self.namespaces)
				if state is not None:
					state = state.text
				else:
					state = "Maharashtra"
				if state not in self.STATES_POSSIBLE:
					state_val = difflib.get_close_matches(state.lower(),self.STATES_POSSIBLE)
					if state_val:
						state = state_val[0]
				details['state']=state
				countryCode = address.find('resp:CountryCode', namespaces=self.namespaces).text
				details['countryCode']=countryCode
				phone = address.find('resp:Phone', namespaces=self.namespaces)
				if phone is not None:
					phone = phone.text
				else:
					phone = "NA"
				details['phone']=phone
				postalCode = address.find('resp:PostalCode', namespaces=self.namespaces).text
				details['postalCode']=postalCode
			else:
				details['name'] = "Pending Order"
				details['address_1'] = "Customer details not available"
				details['address_2'] = "NA"
				details['city'] = "NA"
				details['state'] = "NA"
				details['countryCode'] = "NA"
				details['phone'] = "NA"
				details['postalCode'] = "NA"
			payment_method_element = order.find('resp:PaymentMethod', namespaces=self.namespaces)
			if payment_method_element is not None:
				payment_method = payment_method_element.text
			else:
				payment_method = "COD"
			details['payment_method'] = payment_method
			service = order.find('resp:ShipmentServiceLevelCategory', namespaces=self.namespaces).text
			details['service'] = service
			ship_service_level = order.find("resp:ShipServiceLevel", namespaces=self.namespaces).text
			if ship_service_level in self.AMAZON_SHIPPING_LEVELS:
				ship_service = "amazon"
			else:
				ship_service = "seller"
			details["ship_service"] = ship_service

			order_type = order.find('resp:OrderType', namespaces=self.namespaces).text
			details['order_type'] = order_type
			buyer_name_element = order.find('resp:BuyerName', namespaces=self.namespaces)
			if buyer_name_element is not None:
				details['buyer_name'] = buyer_name_element.text
			else:
				details["buyer_name"] = "NA"
			order_total = order.find('resp:OrderTotal', namespaces=self.namespaces)
			if order_total is not None:
				total = order_total.find('resp:Amount', namespaces=self.namespaces).text
				currency_code = order_total.find('resp:CurrencyCode', namespaces=self.namespaces).text
			else:
				total = "NA"
				currency_code = "NA"
			details["amount"] = total
			details["currency_code"] = currency_code
			detailsHash[amazon_id] = details
		return detailsHash



	def create_orders(self, cr, uid, lookback_time, context=None):
		if os.environ.get("ODOO_ENV") != "production":
			return
		order_creation_registry = self.pool.get("threep.order.creation")
		order_registry = self.pool.get('sale.order')
		source_registry = self.pool.get("crm.tracking.source")
		source_id = source_registry.search(cr, uid, [("name","=","Amazon")])
		if source_id:
			source_id = source_id[0]
		else:
			source_id = source_registry.create(cr, uid, {"name":"Amazon"})

		confirmed_order_query = cr.execute("select order_id from sale_order where write_date >= NOW() - '%s Hour'::INTERVAL and source_id=%s and status='confirmed'" %(lookback_time+6, source_id))
		confirmed_order_list = cr.fetchall()
		confirmed_orders = []
		for cot in confirmed_order_list:
			confirmed_orders.append(cot[0])
		_logger.info(confirmed_orders)

		data = self.get_order_details(cr, uid, lookback_time, confirmed_orders)
		_logger.info(data)
		orders_to_cancel = []
		for order_id in data["canceled"]:
			cancel = order_registry.search(cr, uid, [("order_id","=",order_id)])
			if cancel:
				orders_to_cancel.append(cancel)
		order_creation_registry.cancel_orders(cr, uid, orders_to_cancel)

		items = data["items"]
		details = data["details"]
		for order_id, order_details in details.iteritems():
			if order_details["channel"] == "AFN":
				continue
			check_order = order_registry.search(cr, uid, [("order_id","=",order_id)])
			if check_order:
				# existing_order = order_registry.read(cr, uid, check_order[0], ["status", "order_line","partner_shipping_id","note"])
				existing_order = order_registry.browse(cr, uid, check_order[0])
				if order_details["status"] == "Pending":
					continue
				elif existing_order.status == "confirmed":
					continue

				elif order_details["status"] == "Unshipped":
					amazon_line_items = items[order_id]
					shipping_cost = sum( i for i in [y["shipping"] for y in amazon_line_items])
					adjustments = []
					adjustment = {}
					adjustment["adjusment_label"] = "standard_shipping"
					adjustment["adjustment_notes"] = "Amazon Order Shipping Charge"
					adjustment["amount"] = shipping_cost
					adjustments.append(adjustment)

					promotion_discount = sum( i for i in [y["promotion_discount"] for y in amazon_line_items])
					adjustment = {}
					adjustment["adjusment_label"] = "promotion"
					adjustment["adjustment_notes"] = "Amazon Promotion"
					adjustment["amount"] = -promotion_discount
					adjustments.append(adjustment)

					payments = []
					payment = {}
					payment["payment_method"] = "ThirdPartyPayment"
					payment["payment_reference"] = order_id
					payment["amount"] =  float(order_details["amount"])
					address_hash = {}
					address_hash["ship_name"] = order_details["name"]
					address_hash["ship_address"] = order_details["address_1"] + order_details["address_2"]
					address_hash["ship_zip_code"] = order_details["postalCode"]
					address_hash["country_code"] = order_details["countryCode"]
					address_hash["state_name"] = order_details["state"]
					address_hash["ship_phone"] = order_details["phone"]
					address_hash["email"] = "NA"
					address_hash["city_name"] = order_details["city"]
					order_creation_registry.update_order(cr, uid, existing_order, amazon_line_items, address_hash, adjustments, payments, "Amazon")

			else:
				address_hash = {}
				address_hash["ship_name"] = order_details["name"]
				address_hash["ship_address"] = order_details["address_1"] + order_details["address_2"]
				address_hash["ship_zip_code"] = order_details["postalCode"]
				address_hash["country_code"] = order_details["countryCode"]
				address_hash["state_name"] = order_details["state"]
				address_hash["ship_phone"] = order_details["phone"]
				address_hash["city_name"] = order_details["city"]

				create_date = order_details["create_date"]
				try:
					create_date = datetime.strptime(create_date, "%Y-%m-%dT%H:%M:%SZ")
					create_date = fields.datetime.context_timestamp(cr, uid, create_date, context)
				except:
					pass
				currency = order_details["currency_code"]
				shipping_type = order_details["ship_service"]
				amazon_line_items = items[order_id]

				if shipping_type == "amazon":
					shipping_type = "3rd_party"
				else:
					shipping_type = "standard"

				status = order_details["status"]
				if status == "Pending":
					status = "not_confirmed"
					shipping_cost = None
					discount = None
					address_hash["email"] = "pending@amazon.com"
				elif status == "Unshipped":
					shipping_cost = sum( i for i in [y["shipping"] for y in amazon_line_items])
					discount = sum( i for i in [y["promotion_discount"] for y in amazon_line_items])
					status = "confirmed"
					address_hash["email"] = "NA"

				total_amount = order_details["amount"]
				order_creation_registry.create_orders(cr, uid, order_id, amazon_line_items, address_hash, create_date, currency, shipping_type, status, "Amazon", total_amount, shipping_cost, None, discount)
			# cr.commit()



# c = amazon_order_creation()
# print c.get_order_details("cr", 1, 1, [])
# # c.create_orders([])