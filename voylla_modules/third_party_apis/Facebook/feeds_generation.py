from openerp.osv import osv, fields
import os
from os import listdir
from os.path import isfile, join
import subprocess
import datetime
from datetime import date,datetime,timedelta
import shlex
import spur
import ast
import json
import sys
import xml.etree.ElementTree as ET
import pdb
import logging
from collections import defaultdict
import getpass
import pytz
import csv
_logger = logging.getLogger(__name__)

class facebook_ad_feeds(osv.osv):

    _name = 'facebook.ad.feeds'

    FTP_SERVER = "ec2-54-251-85-20.ap-southeast-1.compute.amazonaws.com"
    FTP_USER = "root"
    FEED_LOCATION = "/home/yepme/"
    
    def scpFeed(self, file_name):
        cmd = "scp %s %s@%s:%s" %(file_name, self.FTP_USER,self.FTP_SERVER, self.FEED_LOCATION)       #### Send feed to staging
        args = shlex.split(cmd)
        p = subprocess.Popen(args)
        p.wait()

    def generateXML_rss(self, cr, uid,list_all,context=None):
        date_time=datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
        file_name = "FacebookXML_rss_"+date_time+".xml"
        xmlData = open(file_name,"w")
        xmlData.write('<?xml version="1.0"?>' + "\n")
        xmlData.write('<rss xmlns:g="http://base.google.com/ns/1.0" version="2.0">' + "\n")
        xmlData.write('<channel>'+"\n")
        xmlData.write('<title>Voylla</title>' + "\n")
        xmlData.write('<link>http://www.voylla.com/</link>' + "\n")
        xmlData.write('<description>Description</description>' + "\n")
        for item in list_all:
            xmlData.write('<item>' + "\n")
            xmlData.write('<g:id>'+item['sku']+'</g:id>' + "\n")
            xmlData.write('<g:title>'+item['title']+'</g:title>' + "\n")
            xmlData.write('<g:description>'+item['description']+'</g:description>' + "\n")
            xmlData.write('<g:link>'+item['link']+'</g:link>' + "\n")
            xmlData.write('<g:image_link>'+item['image_link']+'</g:image_link>' + "\n")
            xmlData.write('<g:brand>Voylla</g:brand>' + "\n")
            xmlData.write('<g:condition>new</g:condition>' + "\n")
            xmlData.write('<g:availability>in stock</g:availability>' + "\n")
            xmlData.write('<g:price>'+str(item['list_price'])+' INR</g:price>' + "\n")
            # if item_hash["item_group_id"] is not False:
            #     xmlData.write('<g:item_group_id></g:item_group_id>' + "\n")
            if item_hash["gender"] is not False:    
                xmlData.write('<g:gender>'+item_hash["gender"]+'</g:gender>' + "\n")
            if item_hash["material"] is not False:  
                xmlData.write('<g:material>'+item_hash["material"]+'</g:material>' + "\n") 
            if item_hash["pattern"] is not False:
                xmlData.write('<g:pattern>'+item_hash["pattern"]+'</g:pattern>' + "\n")
            if item_hash["taxon"] in ['Anklets','Body Jewelry','Bracelets','Brooches & Lapel Pins','Charms & Pendants','Earrings','Jewelry Sets']:
                xmlData.write('<g:product_type>Apparel & Accessories ; Jewelry ; '+item_hash["taxon"]+'</g:product_type>' + "\n")
            xmlData.write('<g:shipping>' + "\n")
            xmlData.write('<g:country>IN</g:country>' + "\n")
            xmlData.write('<g:service>Standard</g:service>' + "\n")
            if item['list_price'] > 500:
                shipping_price = 0.0
            else:
                shipping_price = 50.0
            xmlData.write('<g:price>'+str(shipping_price)+'</g:price>' + "\n")
            xmlData.write('</g:shipping>' + "\n")
            xmlData.write('<g:google_product_category>Apparel & Accessories ; Jewelry </g:google_product_category>' + "\n")
            xmlData.write('</item>' + "\n")
        xmlData.write('</channel>' + "\n")
        xmlData.write('</rss>' + "\n")
        # self.scpFeed(file_name)

    def generateXML_atom(self, cr, uid,list_all,context=None):
        date_time=datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
        file_name = "FacebookXML_atom_"+date_time+".xml"
        xmlData = open(file_name,"w")
        xmlData.write('<?xml version="1.0"?>'+ "\n")
        xmlData.write('<feed xmlns="http://www.w3.org/2005/Atom" xmlns:g="http://base.google.com/ns/1.0">'+ "\n")
        xmlData.write('<title>Voylla</title>'+ "\n")
        xmlData.write('<link rel="self" href="http://www.voylla.com/"/>'+ "\n")
        for item in list_all:
            xmlData.write('<entry>'+ "\n")
            xmlData.write('<g:id>'+item['sku']+'</g:id>'+ "\n")
            xmlData.write('<g:title>'+item['title']+'</g:title>'+ "\n")
            xmlData.write('<g:description>'+item['description']+'</g:description>'+ "\n")
            xmlData.write('<g:link>'+item['link']+'</g:link>'+ "\n")
            xmlData.write('<g:image_link>'+item['image_link']+'</g:image_link>'+ "\n")
            xmlData.write('<g:brand>Voylla</g:brand>'+ "\n")
            xmlData.write('<g:condition>new</g:condition>'+ "\n")
            xmlData.write('<g:availability>in stock</g:availability>'+ "\n")
            xmlData.write('<g:price>'+str(item['list_price'])+' INR</g:price>'+ "\n")
            if item_hash["gender"] is not False:    
                xmlData.write('<g:gender>'+item_hash["gender"]+'</g:gender>' + "\n")
            if item_hash["material"] is not False:  
                xmlData.write('<g:material>'+item_hash["material"]+'</g:material>' + "\n") 
            if item_hash["pattern"] is not False:
                xmlData.write('<g:pattern>'+item_hash["pattern"]+'</g:pattern>' + "\n")
            if item_hash["taxon"] in ['Anklets','Body Jewelry','Bracelets','Brooches & Lapel Pins','Charms & Pendants','Earrings','Jewelry Sets']:
                xmlData.write('<g:product_type>Apparel & Accessories ; Jewelry ; '+item_hash["taxon"]+'</g:product_type>' + "\n")
            if item['list_price'] > 500:
                shipping_price = 0.0
            else:
                shipping_price = 50.0
            xmlData.write('<g:shipping>'+ "\n")
            xmlData.write('<g:country>IN</g:country>'+ "\n")
            xmlData.write('<g:service>Standard</g:service>'+ "\n")
            xmlData.write('<g:price>'+str(shipping_price)+'</g:price>'+ "\n")
            xmlData.write('</g:shipping>'+ "\n")
            xmlData.write('<g:google_product_category>Apparel & Accessories ; Jewelry</g:google_product_category>'+ "\n")
            xmlData.write('</entry>'+ "\n")
        xmlData.write('</feed>'+ "\n")
        # self.scpFeed(file_name)

    def data_extraction(self, cr, uid):
        product_product_registry = self.pool.get('product.product')
        product = product_product_registry.search(cr,uid,[("id","!=","0")])
        location_dest_obj = self.pool.get('stock.location').search(cr,uid,[('complete_name','=','Voylla Locations / Warehouse / Warehouse / Good bin')])
        location_dest_obj_id = self.pool.get('stock.location').browse(cr,uid,location_dest_obj)[0].id
        list_all = []
        for item in product:
            item_hash = {}
            prod_virtual_quant=product_product_registry._product_available(cr, uid,
                [item],context={'location':location_dest_obj_id,'compute_child':False})[item]['virtual_available']           
            if prod_virtual_quant < 1:
                continue
            product_product_obj = product_product_registry.browse(cr,uid,item)
            product_template_obj = product_product_obj.product_tmpl_id
            name = product_template_obj.name
            ean = product_template_obj.ean13
            if name is False:
                continue
            title = product_template_obj.product_name         
            if title is False:
                continue
            description = product_template_obj.description          
            if description is False:
                continue
            link = str("http://www.voylla.com/sku/")+str(name)
            if link is False:
                continue
            image_link = product_template_obj.link
            if image_link is False:
                continue                 
            list_price = product_template_obj.list_price
            if list_price is False:
                continue
            taxon = product_template_obj.taxon
            material = product_template_obj.material
            gender = product_template_obj.gender
            pattern = product_template_obj.theme_ 
            item_hash["sku"] = ean
            item_hash["title"] = title
            item_hash["description"] = description
            item_hash["link"] = link
            item_hash["image_link"] = image_link
            item_hash["list_price"] = list_price
            item_hash["quantity"] = prod_virtual_quant
            item_hash["taxon"] = taxon
            item_hash["material"] = material
            item_hash["gender"] = gender
            item_hash["pattern"] = pattern
            list_all.append(item_hash)
        # generateXML_atom(list_all)
        self.generateXML_rss(cr,uid,list_all)
        self.generateXML_atom(cr,uid,list_all)