from openerp.osv import fields, osv

class config(osv.osv):
	_name = 'config'
	_columns = {
		'name': fields.many2one('crm.tracking.source','Channel'),
		'url' : fields.char('URL'), # e.g. spree url
		'location' : fields.char('Location'), # ftp
		'username' : fields.char('Username'), # ftp username
		'password' : fields.char('Password'), # ftp password
		'key': fields.text('Key Or Token'), # key
	}
	_sql_constraints = [
        ('unique_channel', 'unique(name)', 'Channel already added')
        ]
