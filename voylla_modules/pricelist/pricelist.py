from openerp import models, fields, api, _
from openerp.osv import fields, osv
import pdb
from openerp import http
import csv
import base64
import re
import psycopg2
import sys
import threading
from openerp.api import Environment

con = None

class product_product(osv.osv):
	_inherit = 'product.product'

	def get_product_id(self,cr,uid,ean,context=None):
		product_id =  self.search(cr,uid,[('ean13','=',ean)])
		error_threep_reference = False
		if len(product_id) != 1:
			if len(product_id)==0:
				cr.execute("INSERT INTO failure_notification(name,type,description)\
					VALUES('"'%s'"','pricelist_error','No Product found for given EAN')"%(ean))
				cr.commit()
			if len(product_id)>1:
				cr.execute("INSERT INTO failure_notification(name,type,description)\
					VALUES('"'%s'"','pricelist_error','Multiple Product found for given EAN')"%(ean))
				cr.commit()
			error_threep_reference = True
		return	product_id,error_threep_reference

class import_pricelist(osv.osv):
	_name = "import.pricelist"
	# _inherit = 'product.pricelist'
	_columns={
		"name" : fields.many2one('product.pricelist','Pricelist',required=True, ondelete='cascade'),
		"version":fields.many2one('product.pricelist.version','Pricelist Version',required=True, ondelete='cascade'),
		"import_file" : fields.binary('Import File', required=True)
	}

	def onchange_pricelist(self,cr,uid,ids,name,context=None):
		pricelist_registry = self.pool.get('product.pricelist')
		pricelist_version_registry = self.pool.get('product.pricelist.version')
		if not name:
			return {'value':{'version':False}, 'domain':{'version':[('id','in',[])]}}
		pricelist_version_id = pricelist_version_registry.search(cr,uid,[('pricelist_id','=',name),('active','=',True)])
		return {'domain':{'version':[('id','in',pricelist_version_id)]}}

	def import_pricelist(self,cr,uid,ids,context=None):
		threaded_calculation = threading.Thread(target=self.upload_pricelist, args=(cr, uid, ids, context))
		threaded_calculation.start()

	def upload_pricelist(self,cr,uid,ids,context=None):	
		with Environment.manage():
			new_cr = self.pool.cursor()
			remote_tasks_registry = self.pool.get('remote.tasks')
			product_product_registry = self.pool.get('product.product')
			product_pricelist_item_registry = self.pool.get('product.pricelist.item')
			product_pricelist_registry = self.pool.get('product.pricelist')
			product_pricelist_version_registry = self.pool.get('product.pricelist.version')
			price_type_registry = self.pool.get('price.type')
			data = self.browse(new_cr, uid, ids[0] , context=context)
			pricelist_id = data.name.id
			version_id = data.version.id
			csv_data = base64.decodestring(data.import_file)
			chars_to_remove = ['\r', ' ', '\n']
			chars_to_remove_1 = ['\r', '\n']
			rx = "[%s]" %(re.escape(''.join(chars_to_remove)))
			rx_1 = "[%s]" %(re.escape(''.join(chars_to_remove_1)))
			# _logger.info(csv_data)
			rows = csv_data.split("\n")
			try:
				header = re.sub(rx, '', rows[0]).split(",")
				header.append("version_id")
				header.append("pricelist_id")
				header.append("sequence")
				header.append("min_quantity")
				header.append("base")
				csv_order_details = rows[1:]
			except:
				pass
			order_details_dict = {}
			order_list = []
			error_list = []
			check = True
			count = 0
			# fout = open("update_pricelist.csv", "w")
			for order in csv_order_details:
				count += 1
				print count
				if order == '':
					continue
				order += ',%i,%i,5,0,1'%(int(version_id),int(pricelist_id))
				order_details = dict(zip(header, re.sub(rx_1, '', order).split(",")))
				product_id, error_threep_reference = product_product_registry.get_product_id(new_cr,uid,order_details['ean'])
				if error_threep_reference:
					check = False
					error_list.append(order_details['ean'])
					continue
				try:
					order_details['surcharge'] = float(order_details['surcharge'])
					order_details['pricelist_id'] = int(order_details['pricelist_id'])
					order_details['version_id'] = int(order_details['version_id'])
					order_details['min_quantity'] = int(order_details['min_quantity'])
					order_details['base'] = int(order_details['base'])
					order_details['sequence'] = int(order_details['sequence'])
				except:
					check = False
					new_cr.execute("INSERT INTO failure_notification(name,type,description)\
						VALUES('"'%s'"','pricelist_error','Error in uploaded file')"%(order_details['ean']))
					new_cr.commit()
					error_list.append({order_details['ean']:order_details['surcharge']})
					continue 
				order_details.update({'product_id':product_id[0]})	
				order_list.append(order_details)
				new_cr.execute("INSERT INTO product_pricelist_item(product_id,price_surcharge,min_quantity,\
						price_version_id,sequence,name,base) VALUES(%s,%f,%i,%i,%i,'"'%s'"',%i)"%(order_details['product_id'],\
						order_details['surcharge'],order_details['min_quantity'],order_details['version_id'],order_details['sequence'],order_details['ean'],\
						order_details['base']))
				print 'Hi'
				new_cr.commit()
			print error_list
			new_cr.close()