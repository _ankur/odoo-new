{
    'name': "Import Pricelist",
    'version': "1.0",
    'author': "Voylla",
    'category': "Tools",
    'depends': ['stock', 'procurement','purchase','crm','sale'],
    'data': [
        'pricelist_view.xml',
    ],
    'demo': [],
    'installable': True,
    'auto_install': True,
    'application': True,
}