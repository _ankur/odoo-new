from openerp.osv import osv, fields
import pdb
import base64
from datetime import datetime,timedelta
import logging
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
import re
from voylla_modules.config import home
import xlwt
import subprocess
import shlex
import os
import urllib
import requests
import urllib2
import openerp.addons.decimal_precision as dp

class product_template(osv.osv):
	_inherit = 'product.template'
	def get_product_category(self,cr,uid,ids,context=None):
		taxonomy_id=self.browse(cr,uid,ids).taxonomy
		if len(taxonomy_id)==0:
			return False
		else:
			taxon=taxonomy_id[0].categ_id.complete_name.split('/')
			if len(taxon) > 1:
				return taxon[1].strip()
			else:
				return False


	def get_product_sub_category(self,cr,uid,ids,context=None):
		taxonomy_id=self.browse(cr,uid,ids).taxonomy
		if len(taxonomy_id)==0:
			return False
		else:
			taxon=taxonomy_id[0].categ_id.complete_name.split('/')
			if len(taxon) > 2:
				return taxon[2].strip()
			else:
				return False

class purchase_order_save(osv.osv):
	_name = 'purchase.order.save'
	_columns = {
	'download_file':fields.binary('Download File'),
	}

	def get_po_export(self,cr,uid,ids,context=None):
		
		po_registry = self.pool.get('purchase.order')
		filename = "po_export"+".xls"
		book = xlwt.Workbook(encoding="utf-8")
		sheet = book.add_sheet("Export PO")
		
		header = ['Designer','Voylla Code','Designer Code','Category','Sub Category','Quantity','Cost','Date','PO NO.','Created By']
		row_num = 0
		for index,col_name in enumerate(header):
			sheet.write(0,index,col_name)
		purchase_orders = po_registry.browse(cr,uid,context['active_ids'])
		for purchase_order in purchase_orders:
			line_items = purchase_order.order_line
			for item in line_items:
				row_num += 1
				row = []

				des_name = purchase_order.partner_id.name
				Voylla_code = item.product_id.name_template
				supplier_code =  item.supplier_product_code
				product_category = item.taxon_id.name
				if product_category==False:
					product_category=item.product_id.product_tmpl_id.get_product_category()
				product_subcategory = item.product_id.product_tmpl_id.get_product_sub_category()
				quantity = item.product_qty
				cost = item.price_unit
				order_date = purchase_order.date_order
				ref = purchase_order.name 
				created_by = purchase_order.create_uid.partner_id.name


				row.append(des_name)
				row.append(Voylla_code)
				row.append(supplier_code)
				row.append(product_category)
				row.append(product_subcategory)
				row.append(quantity)
				row.append(cost)
				row.append(order_date)
				row.append(ref)
				row.append(created_by)

				for col_num, value in enumerate(row):
					sheet.write(row_num, col_num, value)

		book.save(filename)
		upload_command = "s3cmd put " + filename + " s3://voylladb-backups/dumps/purchase_order/"
		upload_command_arguments = shlex.split(upload_command)
		upload_process = subprocess.Popen(upload_command_arguments)
		upload_process.wait()
		make_public_command = "s3cmd setacl s3://voylladb-backups/dumps/purchase_order/" + filename + " --acl-public"
		make_public_command_arguments = shlex.split(make_public_command)
		make_public_process = subprocess.Popen(make_public_command_arguments)
		make_public_process.wait()
		outputfilename = "Generated_po.xls"
		url_to_file = "https://s3-ap-southeast-1.amazonaws.com/voylladb-backups/dumps/purchase_order/po_export.xls"
		# resp = requests.get(url_to_file)
		vals={'name':outputfilename,'type':'url','url':url_to_file}
		attachment_id=int(self.pool.get('ir.attachment').create(cr,uid,vals))
		urllib.urlretrieve(url_to_file,outputfilename)
		return {
		'domain':"[('id','=',%i)]"%attachment_id,
		'actions':'check_action_attachment',
		'view_type': 'form',
		'view_mode': 'tree,form',
		'res_model': 'ir.attachment',
		'view_id': False,
		'type': 'ir.actions.act_window',
		'search_view_id':attachment_id 
        }

		self.write(cr, uid, ids, { 'download_file' : outputfilename})
		# output.close()
			

class generate_sku_code(osv.osv):
	_name='generate.sku_code'
	_columns={
		'suppliercode':fields.char('Supplier code',required=True,select=True),
		'nextnumber':fields.integer('number')
	}
class product_import(osv.osv):
	_name = 'product.import'

	_columns = {
	'module_file':fields.binary("Module File"),
	}
		
	def generate_new_products(self,cr,uid,full_current_name,vendorcode,vendorproductcode,MAIN_PRODUCT_QUANTITY,MAIN_PRODUCT_COST_PRICE,FINDING_QTY,FINDING_PRICE,STONE_MAIN_PRODUCT,STONE_MAIN_PRICE,OTHER_PRODUCT_QTY,OTHER_PRICE,Taxes,ManufacturingCost,context=None):
		product_code_new = str(full_current_name)
		product_obj = self.pool.get('product.template')
		product_name = product_code_new
		_main = '_main'
		_finding = '_finding'
		_stone = '_stone'
		_other = '_other'
		Taxes = self.pool.get('account.tax').search(cr,uid,[('name','=',Taxes)])
		# Taxes = []
		po_object_array=[]
		res_partner_obj = self.pool.get('res.partner')
		try:
			partner_id = res_partner_obj.search(cr,uid,[('supplier_code','=',vendorcode)])[0]
		except:
			raise osv.except_osv(('Warning!'), ("supplier not found"))	
		
		res_p_obj = res_partner_obj.browse(cr,uid,partner_id).id
		vals = {'track_all': False, 'track_production': False, 'uos_id': False, 'list_price': ManufacturingCost, 'weight': 0, 'property_stock_procurement': 37, 'taxonomy': [], 'standard_price': 0, 'attribute_line_ids': [], 'masterean': False, 'mes_type': 'fixed',
				'uom_id': 1, 'flag_complete': False, 'property_stock_account_input': False, 'description_purchase': False, 'default_code': False, 'gender': False, 'property_account_income': False, 'message_follower_ids': False, 'message_ids': False, 'childdimid': [],
				'uos_coeff': 1, 'gem_color_one': False, 'surface_finish_one': False, 'salespackage': False, 'sale_ok': True, 'update_flag': False, 'purchase_ok': True, 'product_manager': False, 'track_outgoing': False, 'company_id': 1, 'color_one': False, 'state': False, 
				'loc_rack': False, 'uom_po_id': 1, 'product_name': product_name, 'type': 'consu', 'weight_net': 0, 'taxon_list': [], 'theme_': False, 'gem_color_two': False, 'description': False, 'valuation': 'manual_periodic', 'track_incoming': False, 'property_stock_production': 38, 
				'supplier_taxes_id': [[6, False, Taxes]], 'volume': 0, 'route_ids': [[6, False, [5]]], 'sale_delay': 7, 'link': False, 'description_sale': False, 'active': True, 'property_stock_inventory': 36, 'cost_method': 'standard', 'color_two': False, 'loc_row': False, 'categ_id': 1, 
				'agegroup': False, 'loc_case': False, 'image_medium': False, 'name':product_name, 'produce_delay': 1, 'property_account_expense': False, 'is_combos': False, 'mrp': 0, 'taxon': False, 'packaging_ids': [], 'taxes_id': [[6, False, []]], 'warranty': 0, 'property_stock_account_output': False, 
				'seller_ids': [[0, False, {'pricelist_ids': [], 'name': partner_id, 'sequence': 1, 'company_id': 1, 'delay': 1, 'min_qty': 0, 'product_code': vendorproductcode, 'product_name': False}]], 'design': False, 'making_charges': 0, 'variant_product': [], 'tagid': [], 'threep_reference': False}
		product_name_main = product_code_new + str(_main)
		vals1 = {'track_all': False, 'track_production': False, 'uos_id': False, 'list_price': ManufacturingCost, 'weight': 0, 'property_stock_procurement': 37, 'taxonomy': [], 'standard_price': MAIN_PRODUCT_COST_PRICE, 'attribute_line_ids': [], 'masterean': False, 'mes_type': 'fixed',
				'uom_id': 1, 'flag_complete': False, 'property_stock_account_input': False, 'description_purchase': False, 'default_code': False, 'gender': False, 'property_account_income': False, 'message_follower_ids': False, 'message_ids': False, 'childdimid': [],
				'uos_coeff': 1, 'gem_color_one': False, 'surface_finish_one': False, 'salespackage': False, 'sale_ok': True, 'update_flag': False, 'purchase_ok': True, 'product_manager': False, 'track_outgoing': False, 'company_id': 1, 'color_one': False, 'state': False, 
				'loc_rack': False, 'uom_po_id': 1, 'product_name': product_name_main, 'type': 'consu', 'weight_net': 0, 'taxon_list': [], 'theme_': False, 'gem_color_two': False, 'description': False, 'valuation': 'manual_periodic', 'track_incoming': False, 'property_stock_production': 38, 
				'supplier_taxes_id': [[6, False, Taxes]], 'volume': 0, 'route_ids': [[6, False, [5]]], 'sale_delay': 7, 'link': False, 'description_sale': False, 'active': True, 'property_stock_inventory': 36, 'cost_method': 'standard', 'color_two': False, 'loc_row': False, 'categ_id': 1, 
				'agegroup': False, 'loc_case': False, 'image_medium': False, 'name':product_name_main, 'produce_delay': 1, 'property_account_expense': False, 'is_combos': False, 'mrp': 0, 'taxon': False, 'packaging_ids': [], 'taxes_id': [[6, False, []]], 'warranty': 0, 'property_stock_account_output': False, 
				'seller_ids': [[0, False, {'pricelist_ids': [], 'name': partner_id, 'sequence': 1, 'company_id': 1, 'delay': 1, 'min_qty': 0, 'product_code': vendorproductcode, 'product_name': False}]], 'design': False, 'making_charges': 0, 'variant_product': [], 'tagid': [], 'threep_reference': False}
		product_name_finding = product_code_new + str(_finding)
		vals2 = {'track_all': False, 'track_production': False, 'uos_id': False, 'list_price': ManufacturingCost, 'weight': 0, 'property_stock_procurement': 37, 'taxonomy': [], 'standard_price': FINDING_PRICE, 'attribute_line_ids': [], 'masterean': False, 'mes_type': 'fixed',
				'uom_id': 1, 'flag_complete': False, 'property_stock_account_input': False, 'description_purchase': False, 'default_code': False, 'gender': False, 'property_account_income': False, 'message_follower_ids': False, 'message_ids': False, 'childdimid': [],
				'uos_coeff': 1, 'gem_color_one': False, 'surface_finish_one': False, 'salespackage': False, 'sale_ok': True, 'update_flag': False, 'purchase_ok': True, 'product_manager': False, 'track_outgoing': False, 'company_id': 1, 'color_one': False, 'state': False, 
				'loc_rack': False, 'uom_po_id': 1, 'product_name': product_name_finding, 'type': 'consu', 'weight_net': 0, 'taxon_list': [], 'theme_': False, 'gem_color_two': False, 'description': False, 'valuation': 'manual_periodic', 'track_incoming': False, 'property_stock_production': 38, 
				'supplier_taxes_id': [[6, False, Taxes]], 'volume': 0, 'route_ids': [[6, False, [5]]], 'sale_delay': 7, 'link': False, 'description_sale': False, 'active': True, 'property_stock_inventory': 36, 'cost_method': 'standard', 'color_two': False, 'loc_row': False, 'categ_id': 1, 
				'agegroup': False, 'loc_case': False, 'image_medium': False, 'name':product_name_finding, 'produce_delay': 1, 'property_account_expense': False, 'is_combos': False, 'mrp': 0, 'taxon': False, 'packaging_ids': [], 'taxes_id': [[6, False, []]], 'warranty': 0, 'property_stock_account_output': False, 
				'seller_ids': [[0, False, {'pricelist_ids': [], 'name': partner_id, 'sequence': 1, 'company_id': 1, 'delay': 1, 'min_qty': 0, 'product_code': vendorproductcode, 'product_name': False}]], 'design': False, 'making_charges': 0, 'variant_product': [], 'tagid': [], 'threep_reference': False}
		product_name_stone = product_code_new + str(_stone)
		vals3 = {'track_all': False, 'track_production': False, 'uos_id': False, 'list_price': ManufacturingCost, 'weight': 0, 'property_stock_procurement': 37, 'taxonomy': [], 'standard_price': STONE_MAIN_PRICE, 'attribute_line_ids': [], 'masterean': False, 'mes_type': 'fixed',
				'uom_id': 1, 'flag_complete': False, 'property_stock_account_input': False, 'description_purchase': False, 'default_code': False, 'gender': False, 'property_account_income': False, 'message_follower_ids': False, 'message_ids': False, 'childdimid': [],
				'uos_coeff': 1, 'gem_color_one': False, 'surface_finish_one': False, 'salespackage': False, 'sale_ok': True, 'update_flag': False, 'purchase_ok': True, 'product_manager': False, 'track_outgoing': False, 'company_id': 1, 'color_one': False, 'state': False, 
				'loc_rack': False, 'uom_po_id': 1, 'product_name': product_name_stone, 'type': 'consu', 'weight_net': 0, 'taxon_list': [], 'theme_': False, 'gem_color_two': False, 'description': False, 'valuation': 'manual_periodic', 'track_incoming': False, 'property_stock_production': 38, 
				'supplier_taxes_id': [[6, False, Taxes]], 'volume': 0, 'route_ids': [[6, False, [5]]], 'sale_delay': 7, 'link': False, 'description_sale': False, 'active': True, 'property_stock_inventory': 36, 'cost_method': 'standard', 'color_two': False, 'loc_row': False, 'categ_id': 1, 
				'agegroup': False, 'loc_case': False, 'image_medium': False, 'name':product_name_stone, 'produce_delay': 1, 'property_account_expense': False, 'is_combos': False, 'mrp': 0, 'taxon': False, 'packaging_ids': [], 'taxes_id': [[6, False, []]], 'warranty': 0, 'property_stock_account_output': False, 
				'seller_ids': [[0, False, {'pricelist_ids': [], 'name': partner_id, 'sequence': 1, 'company_id': 1, 'delay': 1, 'min_qty': 0, 'product_code': vendorproductcode, 'product_name': False}]], 'design': False, 'making_charges': 0, 'variant_product': [], 'tagid': [], 'threep_reference': False}
		product_name_other = product_code_new + str(_other)
		vals4 = {'track_all': False, 'track_production': False, 'uos_id': False, 'list_price': ManufacturingCost, 'weight': 0, 'property_stock_procurement': 37, 'taxonomy': [], 'standard_price': OTHER_PRICE, 'attribute_line_ids': [], 'masterean': False, 'mes_type': 'fixed',
				'uom_id': 1, 'flag_complete': False, 'property_stock_account_input': False, 'description_purchase': False, 'default_code': False, 'gender': False, 'property_account_income': False, 'message_follower_ids': False, 'message_ids': False, 'childdimid': [],
				'uos_coeff': 1, 'gem_color_one': False, 'surface_finish_one': False, 'salespackage': False, 'sale_ok': True, 'update_flag': False, 'purchase_ok': True, 'product_manager': False, 'track_outgoing': False, 'company_id': 1, 'color_one': False, 'state': False, 
				'loc_rack': False, 'uom_po_id': 1, 'product_name': product_name_other, 'type': 'consu', 'weight_net': 0, 'taxon_list': [], 'theme_': False, 'gem_color_two': False, 'description': False, 'valuation': 'manual_periodic', 'track_incoming': False, 'property_stock_production': 38, 
				'supplier_taxes_id': [[6, False, Taxes]], 'volume': 0, 'route_ids': [[6, False, [5]]], 'sale_delay': 7, 'link': False, 'description_sale': False, 'active': True, 'property_stock_inventory': 36, 'cost_method': 'standard', 'color_two': False, 'loc_row': False, 'categ_id': 1, 
				'agegroup': False, 'loc_case': False, 'image_medium': False, 'name':product_name_other, 'produce_delay': 1, 'property_account_expense': False, 'is_combos': False, 'mrp': 0, 'taxon': False, 'packaging_ids': [], 'taxes_id': [[6, False, []]], 'warranty': 0, 'property_stock_account_output': False, 
				'seller_ids': [[0, False, {'pricelist_ids': [], 'name': partner_id, 'sequence': 1, 'company_id': 1, 'delay': 1, 'min_qty': 0, 'product_code': vendorproductcode, 'product_name': False}]], 'design': False, 'making_charges': 0, 'variant_product': [], 'tagid': [], 'threep_reference': False}
		try:
			if len(vendorcode) != 0:
				product_id = product_obj.create(cr,uid,vals,context=None)
				product_product_id = product_obj.browse(cr,uid,product_id).product_variant_ids
				manufacturing_cost = product_product_id.list_price
		except:
			raise osv.except_osv(('Warning!'), ("product already created is not generated"))	
		try:
			if len(MAIN_PRODUCT_QUANTITY) or len(MAIN_PRODUCT_COST_PRICE) != 0:
				product_id1 = product_obj.create(cr,uid,vals1,context=None)
				product_product_id = product_obj.browse(cr,uid,product_id1).product_variant_ids
				cost_main = product_product_id.standard_price
				tax=[]
				for tax_id in product_product_id.supplier_taxes_id:
					tax.append(tax_id.id)
				po_object_array.append([0, False, {'product_id': product_product_id.id, 'product_uom': product_product_id.uom_id.id, 'date_planned': datetime.now().strftime("%Y-%m-%d"),
					'price_unit': MAIN_PRODUCT_COST_PRICE, 'taxon_id': product_product_id.taxon.id, 'taxes_id': [[6, False, tax]], 'product_qty': MAIN_PRODUCT_QUANTITY,
					'account_analytic_id': False, 'manu_cost': product_product_id.manu_cost,  'supplier_product_code': product_product_id.seller_ids[0].product_code,
					'name': product_product_id.name_template}])
			else:
				cost_main = 0
		except:
			raise osv.except_osv(('Warning!'), ("product already created is not generated"))
		
		try:
			if (FINDING_QTY) or (FINDING_PRICE) != 0:
				product_id2 = product_obj.create(cr,uid,vals2,context=None)
				product_product_id = product_obj.browse(cr,uid,product_id2).product_variant_ids
				cost_finding = product_product_id.standard_price
				tax=[]
				for tax_id in product_product_id.supplier_taxes_id:
					tax.append(tax_id.id)
				po_object_array.append([0, False, {'product_id': product_product_id.id, 'product_uom': product_product_id.uom_id.id, 'date_planned': datetime.now().strftime("%Y-%m-%d"),
					'price_unit': FINDING_PRICE, 'taxon_id': product_product_id.taxon.id, 'taxes_id': [[6, False, tax]], 'product_qty': FINDING_QTY,
					'account_analytic_id': False, 'manu_cost': product_product_id.manu_cost,  'supplier_product_code': product_product_id.seller_ids[0].product_code,
					'name': product_product_id.name_template}])
			else:
				cost_finding = 0
		except:
			raise osv.except_osv(('Warning!'), ("product already created is not generated"))
		
		try:
			if len(STONE_MAIN_PRODUCT) or len(STONE_MAIN_PRICE) != 0:
				product_id3 = product_obj.create(cr,uid,vals3,context=None)
				product_product_id = product_obj.browse(cr,uid,product_id3).product_variant_ids
				cost_stone = product_product_id.standard_price
				tax=[]
				for tax_id in product_product_id.supplier_taxes_id:
					tax.append(tax_id.id)
				po_object_array.append([0, False, {'product_id': product_product_id.id, 'product_uom': product_product_id.uom_id.id, 'date_planned': datetime.now().strftime("%Y-%m-%d"),
					'price_unit': STONE_MAIN_PRICE, 'taxon_id': product_product_id.taxon.id, 'taxes_id': [[6, False, tax]], 'product_qty': STONE_MAIN_PRODUCT,
					'account_analytic_id': False, 'manu_cost': product_product_id.manu_cost,  'supplier_product_code': product_product_id.seller_ids[0].product_code,
					'name': product_product_id.name_template}])
			else:
				cost_stone = 0
		except:
			raise osv.except_osv(('Warning!'), ("product already created is not generated"))

		try:
			if len(OTHER_PRODUCT_QTY) or len(OTHER_PRICE) !=0:
				product_id4 = product_obj.create(cr,uid,vals4,context=None)
				product_product_id = product_obj.browse(cr,uid,product_id4).product_variant_ids
				cost_other = product_product_id.standard_price
				tax=[]
				for tax_id in product_product_id.supplier_taxes_id:
					tax.append(tax_id.id)
				po_object_array.append([0, False, {'product_id': product_product_id.id, 'product_uom': product_product_id.uom_id.id, 'date_planned': datetime.now().strftime("%Y-%m-%d"),
					'price_unit': OTHER_PRICE, 'taxon_id': product_product_id.taxon.id, 'taxes_id': [[6, False, tax]], 'product_qty': OTHER_PRODUCT_QTY,
					'account_analytic_id': False, 'manu_cost': product_product_id.manu_cost,  'supplier_product_code': product_product_id.seller_ids[0].product_code,
					'name': product_product_id.name_template}])
			else:
				cost_other = 0
		except:
			raise osv.except_osv(('Warning!'), ("product already created is not generated"))
		
		x = super(product_import, self).create(cr, uid, vals, context=context)
		cost_total = cost_main + cost_finding + cost_stone +  cost_other + manufacturing_cost
		y = product_obj.write(cr,uid,product_id,{'standard_price':cost_total})
		# self.generate_po(cr,uid,po_object_array,res_p_obj,context=context)
		return po_object_array,res_p_obj,context

	def generate_po(self,cr,uid,po_object_array,res_p_obj,context=None):
		product=self.pool.get('product.product')
		# product=product.browse(cr,uid,product_id)
		# template = product.product_tmpl_id.standard_price
		purchase_order = self.pool.get('purchase.order')
		# tax=[]
		# for tax_id in product.supplier_taxes_id:
		# 	tax.append(tax_id.id)
		try:
			vals= {'origin': False, 'message_follower_ids': False,
			'order_line': po_object_array, 'company_id': 1, 'currency_id': 21, 'date_order': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
			'location_id': 7, 'message_ids': False, 'dest_address_id': False, 'fiscal_position': False,
			'picking_type_id': 1, 'partner_id': res_p_obj, 'journal_id': 3,
			'bid_validity': False, 'pricelist_id': 2, 'incoterm_id': False,
			'payment_term_id': False,'partner_ref': False, 'notes': False,
			'invoice_method': 'order', 'minimum_planned_date': False,
			'related_location_id': 7}
			return purchase_order.create(cr, uid, vals, context=context)
		except:
			raise osv.except_osv(('Warning!'), ("some value is missing either supplier or product not available"))

	def import_create_product(self,cr,uid,ids,context=None):
		product_obj = self.pool.get('product.template')
		data = self.browse(cr, uid, ids[0] , context=context)
		csv_data = base64.decodestring(data.module_file)
		chars_to_remove = ['\r', ' ', '\n']
		chars_to_remove_1 = ['\r', '\n']
		rx = "[%s]" %(re.escape(''.join(chars_to_remove)))
		rx_1 = "[%s]" %(re.escape(''.join(chars_to_remove_1)))

		print csv_data
		rows = csv_data.split("\n")

		try:
			header = re.sub(rx, '', rows[0]).split(",")
			csv_product_details = rows[1:]
		except:
			pass
		product_details = {}
		generate_sku_obj = self.pool.get('generate.sku_code')
		# product_details_for_po=[]
		order_line=[]
		for product in csv_product_details:
			if product == '':
				continue
			product_details = dict(zip(header, re.sub(rx_1, '',product).split(",")))
			sku_ids=generate_sku_obj.search(cr,uid,[('suppliercode','=',product_details['vendorcode'])])
			current_sku_name_number_obj = generate_sku_obj.browse(cr,uid,sku_ids)[0]
			if current_sku_name_number_obj:
				full_current_name = product_details['vendorcode']+str(current_sku_name_number_obj.nextnumber)
				current_sku_name_number_obj.write({'nextnumber':current_sku_name_number_obj.nextnumber+1})
			else:
				raise osv.except_osv(('Warning!'), ("supplier not there"))
			product_details = dict(zip(header, re.sub(rx_1, '', product).split(",")))
			product_order_line,supplier,context =  self.generate_new_products(cr,uid,full_current_name,product_details['vendorcode'],product_details['vendorproductcode'],product_details['MAIN_PRODUCT_QTY'],product_details['MAIN_PRODUCT_COST_PRICE'],product_details['FINDING_QTY'],product_details['FINDING_PRICE'],product_details['STONE_MAIN_PRODUCT'],product_details['STONE_MAIN_PRICE'],product_details['OTHER_PRODUCT_QTY'],product_details['OTHER_PRICE'], product_details['Taxes'],product_details['ManufacturingCost'], context=context)
			order_line.extend(product_order_line)
		self.generate_po(cr,uid,order_line,supplier,context=context)

class sku_generation_on_request(osv.osv):
	_name = 'sku.generation.on.request'

	_columns={
		'supplier_name' : fields.char('Supplier Name', required=True),
		'supplier_initial' : fields.char('Supplier Initail', required=True),
		'quantity' : fields.integer('Request Quantity', required=True)
	}

	def sku_on_request(self,cr,uid,ids,context=None):
		generate_sku_obj = self.pool.get('generate.sku_code')
		res_partner_obj = self.pool.get('res.partner')
		supplier_info_obj = self.pool.get('product.supplierinfo')
		data = self.browse(cr, uid, ids[0] , context=context)
		sku_produced = []

		try:
			res_patner_id = res_partner_obj.search(cr,uid,[('display_name','=',data.supplier_name)])[0]
		except:
			raise osv.except_osv(('No Such Supplier'), ("Please check Supplier again"))
		try:
			sku_ids=generate_sku_obj.search(cr,uid,[('suppliercode','=',data.supplier_initial)])
		except:
			raise osv.except_osv(('No Such Initail'), ("Please check Supplier Initail again"))
		current_sku_name_number_obj = generate_sku_obj.browse(cr,uid,sku_ids)[0]
		for i in range(int(data.quantity)):
			full_current_name = data.supplier_initial+str(current_sku_name_number_obj.nextnumber)
			current_sku_name_number_obj.write({'nextnumber':current_sku_name_number_obj.nextnumber+1})
			template_sql = "insert into product_template (uom_id,uom_po_id,type,categ_id,name,active,create_date,write_date,create_uid) values (1,1,'consu',1,'%s','true',now(),now(),1) RETURNING id"%(full_current_name)
			cr.execute(template_sql)
			tmpl_id = cr.fetchall()
			product_sql = "Insert into product_product (name_template,product_tmpl_id,active,create_date,write_date,create_uid,write_uid) values ('%s',%d,'true',now(),now(),1,1)"%(full_current_name,tmpl_id[0][0])
			cr.execute(product_sql)
			supplier_info = "Insert into product_supplierinfo (delay,min_qty,product_tmpl_id,name,create_date,write_date,create_uid,write_uid) values (1,0,%d,%d,now(),now(),1,1)"%(tmpl_id[0][0],res_patner_id)
			cr.execute(supplier_info)
			sku_produced.append(full_current_name)
		book = xlwt.Workbook(encoding="utf-8")
		sku_report = book.add_sheet('Request Skus')
		sku_report.write(0,0, 'SKU')
		for index,sku in enumerate(sku_produced):
			sku_report.write(index+1,0,sku)
		book.save('requested_skus.xls')
		feed='requested_skus.xls'
		c1="s3cmd put "+ feed +" s3://voyllaerp/erp_tag/"
		args = shlex.split(c1)
		p = subprocess.Popen(args)
		p.wait()
		c2="s3cmd setacl s3://voyllaerp/erp_tag/"+feed+" --acl-public"
		args = shlex.split(c2)
		subprocess.Popen(args)
		p.wait()
		url_base = 'https://s3.amazonaws.com/voyllaerp/erp_tag/'+feed
		vals={'name':feed,'type':'url','url':url_base}
		attachment_id=int(self.pool.get('ir.attachment').create(cr,uid,vals))
		return {
			'domain':"[('id','=',%i)]"%attachment_id,
			'actions':'check_action_attachment',
			'view_type': 'form',
			'view_mode': 'tree,form',
			'res_model': 'ir.attachment',
			'view_id': False,
			'type': 'ir.actions.act_window',
			'search_view_id':attachment_id
		}	






			
		
