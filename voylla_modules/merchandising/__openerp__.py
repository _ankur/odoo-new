{
    'name': "Merchandising",
    'version': "1.0",
    'author': "Dheeraj Mathur",
    'category': "Tools",
    'depends': ['stock', 'procurement','purchase','crm','sale','supplier'],
    'data': [
        'security/merchandising_security.xml',
        'security/ir.model.access.csv',
        'merchandising_view.xml',
        'report/merchandising_sale_report_view.xml',
        'stock_view.xml'
    ],
    'demo': [],
    'installable': True,
    'auto_install': True,
    'application': True,
}