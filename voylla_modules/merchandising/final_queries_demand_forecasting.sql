create table price_band(name text, min_value double precision, max_value double precision);

insert into price_band (name,min_value,max_value) values ('<=100',0,100),('101-200',101,200),('201-300',201,300),('301-500',301,500),('501-750',501,750),('751-1000',751,1000),('1001-1500',1001,1500),('1501-2000',1501,2000),('>2000',2001,100000);

create or replace function case_convert(name text)
returns text as
$$
declare
_result text;
BEGIN
IF name = 'other' THEN
    _result = 'Other';
    RETURN _result;
ELSE 
    _result = name;
    RETURN _result;
END IF;
END;
$$
language plpgsql;


CREATE or replace FUNCTION get_collection_tag2(tag_name text) RETURNS TABLE(product_name text,tag_value text) AS $$
select p.name as product_name, coalesce(case_convert(collection_tag.tag_value),'Other') as collection_tag_value
from product_template as p left join (select tag_id,user_type,tv.name as tag_value from product_tag_name as p inner join product_tagvalue as tv on p.user_type = tv.id where p.tag_type='collection') as collection_tag on p.id = collection_tag.tag_id
$$
LANGUAGE SQL;


CREATE or replace FUNCTION get_theme_design() RETURNS TABLE(product_name text, theme text, design text) AS $$
select p.name as product_name,coalesce(case_convert(theme.name), 'Other') AS theme,coalesce(case_convert(design.name),'Other') as design
from product_template as p left join product_theme as theme on p.theme_ = theme.id left join product_design as design on p.design = design.id $$
LANGUAGE SQL;


CREATE or replace FUNCTION get_child_parent() RETURNS TABLE(product_name text, child text, parent text) AS $$
select t1.name as product_name, t3.name as child,t4.name as parent
from product_template as t1
left join product_taxonomy as t2 on t1.id = t2.taxonomy
inner join product_category as t3 on t2.categ_id=t3.id
join product_category as t4 on t3.parent_id = t4.id $$
LANGUAGE SQL;


CREATE or replace FUNCTION get_texon_type_theme_design() RETURNS TABLE(product_name text, taxon text, type text, theme text, design text) AS $$
select category_detail.product_name as product_name,category_detail.parent as taxon,category_detail.child as type,
theme_design.theme as theme,theme_design.design as design
from (select * from get_child_parent()) as category_detail left join (select * from get_theme_design()) as theme_design  on category_detail.product_name=theme_design.product_name $$
LANGUAGE SQL;


CREATE or replace FUNCTION get_week_year_price() RETURNS TABLE(qty numeric, product_name text, week_number integer, year double precision, price double precision, price_band text) AS $$
select sum(sol.product_uom_qty),product.name_template as product_name, week_num_year(so.date_order::date) as week_number,extract(year from so.date_order) as year, sol.cost_price_unit as price, pb.name as price_band1
from sale_order_line as sol,sale_order as so,product_product as product, price_band as pb
where sol.product_id=product.id and sol.order_id=so.id and sol.cost_price_unit between pb.min_value and pb.max_value
and product.name_template not in ('AABOM20019')
group by product_name,week_number,year,price,price_band1 order by year, week_number, price_band1 $$
LANGUAGE SQL;


CREATE or replace FUNCTION get_material() RETURNS TABLE(product_name text, material2 text) AS $$
select product_name,string_agg(tag_value,',') as material_tag_value from
(select product.name as product_name,'silver' as tag_value
    from product_tag_name as tag_type ,product_tagvalue as tag_value,product_template as product
    where tag_type.user_type=tag_value.id and tag_type.tag_id = product.id and tag_type.tag_type='material' and tag_value.name similar To '(%Silver%)%' and tag_value.name not like '%Alloy%'
    union
select product.name as product_name,'Bead' as tag_value
    from product_tag_name as tag_type ,product_tagvalue as tag_value,product_template as product
    where tag_type.user_type=tag_value.id and tag_type.tag_id = product.id and tag_type.tag_type='material' and tag_value.name like '%Bead%'
    union
select product.name as product_name,'Alloy' as tag_value
    from product_tag_name as tag_type ,product_tagvalue as tag_value,product_template as product
    where tag_type.user_type=tag_value.id and tag_type.tag_id = product.id and tag_type.tag_type='material' and tag_value.name similar To '(%Metal%|%Alloy%|Silver Alloy)%'
    union
select product.name as product_name,'Gold' as tag_value
    from product_tag_name as tag_type ,product_tagvalue as tag_value,product_template as product
    where tag_type.user_type=tag_value.id and tag_type.tag_id = product.id and tag_type.tag_type='material' and tag_value.name like '%Gold%'
    union
select product.name as product_name,tag_value.name as tag_value
    from product_tag_name as tag_type ,product_tagvalue as tag_value,product_template as product
    where tag_type.user_type=tag_value.id and tag_type.tag_id = product.id and tag_type.tag_type='material' and tag_value.name not similar To '(%Gold%|%Silver%|%Bead%|%Alloy%|%Metal%)%')
as v group by product_name $$
LANGUAGE SQL;


CREATE or replace FUNCTION get_attributes() RETURNS TABLE(product_name text, taxon text, type text, theme text, design text, collection text, material text) AS $$
select taxon_type_theme_design.product_name,taxon_type_theme_design.taxon,taxon_type_theme_design.type,
        taxon_type_theme_design.theme,taxon_type_theme_design.design,collection_material.collection,collection_material.material
from
(select * from get_texon_type_theme_design()) as taxon_type_theme_design
left join (select collection.product_name as product_name ,collection.tag_value as collection, material.material2 as material
from
(select * from get_collection_tag2('collection')) as collection ,(select * from get_material()) as material
where collection.product_name = material.product_name) as collection_material
on taxon_type_theme_design.product_name=collection_material.product_name $$
LANGUAGE SQL;


create or replace function get_top_taxon_type1() returns table(taxon text,type text,qty numeric) AS $$
select p2.taxon,p2.type,sum(p1.qty) from get_week_year_price()as p1 
left join  get_attributes() as p2 on p1.product_name = p2.product_name 
group by p2.taxon ,p2.type order by sum desc limit 50
$$ language sql;


create or replace function get_top_attribute_with_value() returns table(attribute_value text,week_number_year_qty text[]) AS $$
select product_attribute,array_agg(cast(week_number as text) ||'_'|| cast(year as text) ||'_'|| cast(sum as text))
 from (
	select product_attribute, sum(qty), week_number, year 
	from 
	(select ltrim(rtrim(p2.taxon)) ||'_'|| ltrim(rtrim(p2.type)) ||'_'|| ltrim(rtrim(p2.theme)) ||'_'|| ltrim(rtrim(p2.design)) ||'_'|| ltrim(rtrim(p2.collection)) ||'_'|| 
		ltrim(rtrim(p2.material)) ||'_'||p1.price_band as product_attribute,p1.product_name,p1.qty,p1.week_number,p1.year 
	from get_week_year_price() as p1 
		left join  get_attributes() as p2 on p1.product_name = p2.product_name
 			where p2.taxon in (select taxon from get_top_taxon_type1()) 
 				and 
 				p2.type in (select type from get_top_taxon_type1())) as p group by product_attribute, week_number, year
	) as consolidated_array group by product_attribute 
 $$ language sql;


CREATE or replace FUNCTION get_last_month_order() RETURNS TABLE(qty numeric, product_name text,price_band text) AS $$
select sum(sol.product_uom_qty),product.name_template as product_name,pb.name as price_band1
from sale_order_line as sol,sale_order as so,product_product as product, price_band as pb,
(select date_trunc('month',now()- interval'30 days')   as start) as month_start,
(select date_trunc('month',now()) as end) as month_end
where sol.product_id=product.id and sol.order_id=so.id and sol.cost_price_unit between pb.min_value and pb.max_value 
and product.name_template not in ('AABOM20019') and so.date_order > month_start.start and so.date_order < month_end.end
group by product_name,price_band1  $$
LANGUAGE SQL;

create or replace function get_attributes_month_wise()
 returns table(attribute_value text,sku_qty_last_month text[]) As $$ 
select p2.taxon || '_'|| p2.type ||'_'|| p2.theme ||'_'|| p2.design ||'_'|| p2.collection ||'_'|| 
		p2.material ||'_'||p1.price_band as product_attribute,array_agg(distinct p1.product_name||'_'||p1.qty ) as product_name 
	 from get_attributes() as p2 inner join get_last_month_order() as p1 on 
p1.product_name = p2.product_name group by product_attribute 
$$ LANGUAGE sql;


create or replace function get_top_taxon_type_final_out() returns table(attribute_value text,week_number_year_qty text[], sku_qty_last_month text[]) As $$ 
select p1.attribute_value,p1.week_number_year_qty,p2.sku_qty_last_month
from get_top_attribute_with_value() as p1
left join get_attributes_month_wise() as p2
on  p1.attribute_value=p2.attribute_value $$
LANGUAGE SQL;