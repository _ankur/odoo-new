from openerp.osv import fields, osv
import pdb
import re
import xlwt
import urllib
import urllib2
import json
import base64
import csv
from csv import reader
import shlex
import subprocess
import collections
from datetime import datetime
from openerp.tools.translate import _

class add_procurement_order(osv.osv):
	_inherit='procurement.order'
	def create(self, cr, uid, vals, context=None):
		temp_rule_id = 1
		if 'orderpoint_id' in vals:
			temp_rule_id=vals['orderpoint_id']
		elif 'active_model' in context and context['active_model']=='stock.warehouse.orderpoint':
			temp_rule_id=context['active_id']
		stock_order=self.pool.get('stock.warehouse.orderpoint').browse(cr,uid,temp_rule_id)
		vals.update({'rule_id':stock_order.rule_id.id})
		order=super(add_procurement_order, self).create(cr, uid, vals, context=context)
		return order
class add_stock_warehouse_orderpoint(osv.osv):
	_inherit='stock.warehouse.orderpoint'
	_columns={
		'rule_id':fields.many2one('procurement.rule','Procurement Rule')
	}

class import_po(osv.osv):
	_name = 'import.po'
	_columns={
	'name':fields.char("Name"),
	'file':fields.binary("Module File"),
	'list_sku':fields.text('New line separated SKU list'),
	'description_file':fields.binary('Select File')
	}

	def import_csv_description_file(self,cr,uid,ids,context=None):
		product = self.pool.get('product.template')

		data = self.browse(cr, uid, ids[0], context=context)
		csv_data = base64.decodestring(data.description_file)

		sku_description_list = csv_data.split('\n')

		sku_list = []

		for iterator in xrange(1,len(sku_description_list)-1):
			sku_list.append(sku_description_list[iterator].split(',')[0].strip())

		empty_sku_count = sku_list.count('')
		if empty_sku_count != 0:
			raise osv.except_osv(_("Error!"), _("One or many SKU fields are left blank. No. of fields left blank is: %d\n"%(empty_sku_count)))

		repeated_sku = [item for item, count in collections.Counter(sku_list).items() if count > 1]

		if repeated_sku:
			str2 = ';'.join(repeated_sku)
			raise osv.except_osv(_("Error!"), _("One or many SKU fields are repeated.\n These are: %s\n"%str2))

		for iterator in xrange(1,len(sku_description_list)-1):
			sku = sku_list[iterator-1]
			product_id = product.search(cr,uid,[('name','=',sku)])
			temp_list = [sku_description_list[iterator]]
			for line in reader(temp_list):
				val = {'description':line[1].strip()}
			product.write(cr,uid,product_id,val)

	def generate_csv_tags_file(self,cr,uid,ids,context=None):
		fp = open('tags.csv','wb')
		attributes = ['SKU Code','Taxon','Type','Theme','Design','Primary Color','Secondary Color','Gemstone','Gemstone Primary Color','Gemstone Secondary Color','Gender','Material','Plating','Collection']

		for column in attributes:
			fp.write('%s,'%column)
		fp.write('\n')
		fp.close()

		sku = self.browse(cr,uid,ids).list_sku
		sku_list = sku.split('\n')

		for iter in range(len(sku_list)):
			sku_list[iter] = sku_list[iter].replace(' ','')

		# Removing new lines entered by mistake in sequence
		sku_list = filter(None,sku_list)


		product = self.pool.get('product.template')
		product_ids = product.search(cr,uid,[('name','in',sku_list)])


		if len(sku_list) != len(product_ids):
			list_name = product.read(cr,uid,product_ids,['name'])
			actual_sku_list=[]
			for iterator1 in range(len(list_name)):
				actual_sku_list.append(list_name[iterator1]['name'])
			disjoint_list = [x for x in sku_list if x not in actual_sku_list]

			if len(disjoint_list) == 0:
				repeat_sku_list = [item for item, count in collections.Counter(sku_list).items() if count > 1]
				str3 = ';'.join(repeat_sku_list)
				raise osv.except_osv(_("Error!"), _("Some SKU's are repeated. These are: %s"%str3))
			elif len(disjoint_list) != 0 and len(sku_list)-len(product_ids)>len(disjoint_list):
				str1 = ';'.join(disjoint_list)
				repeat_sku_list = [item for item, count in collections.Counter(sku_list).items() if count > 1]
				str3 = ';'.join(repeat_sku_list)
				raise osv.except_osv(_("Error!"), _("Some SKU's are repeated and some SKU's entered are invalid. Invalid SKU's are: %s\nRepeated SKU's are %s\n Click Ok and Re-enter Valid SKU sequence."%(str1,str3)))
			else:
				str1 = ';'.join(disjoint_list)
				raise osv.except_osv(_("Error!"), _("Some SKU's entered are invalid. These SKU's are: %s\n Click Ok and Re-enter Valid SKU sequence."%str1))

		fp = open('tags.csv','ab')

		product_browse = product.browse(cr,uid,product_ids)
		for product_obj in product_browse:

			tag_push = {'gemstone':7, 'material':11, 'plating':12, 'collection':13}

			sku_tags_val = ['']*14

			for tags in product_obj.tagid:
				if tags.tag_type in tag_push:
					sku_tags_val[tag_push[tags.tag_type]]=tags.user_type.name.replace(';',' ').strip()

			sku_tags_val[0] = product_obj.display_name.strip()

			taxon_type_string = product_obj.taxonomy
			if len(taxon_type_string) == 0:
				raise osv.except_osv(_("Error!"), _("SKU %s does not have Taxon,Type in database.\n Please fill that before continuing."%sku_tags_val[0]))
			else:
				taxon_type = taxon_type_string[0].categ_id.display_name.split('/')

			if len(taxon_type) == 3:
				sku_tags_val[1] = taxon_type[1].strip()
				sku_tags_val[2] = taxon_type[2].strip()
			else:
				sku_tags_val[1] = taxon_type[1].strip()
				sku_tags_val[2] = ''

			if product_obj.theme_.name == False:
				sku_tags_val[3] = ''
			else:
				sku_tags_val[3]=product_obj.theme_.name.replace(';',' ').strip()

			if product_obj.design.name == False:
				sku_tags_val[4] = ''
			else:
				sku_tags_val[4]=product_obj.design.name.replace(';',' ').strip()

			if product_obj.color_one.name == False:
				sku_tags_val[5] = ''
			else:
				sku_tags_val[5]=product_obj.color_one.name.replace(';',' ').strip()

			if product_obj.color_two.name == False:
				sku_tags_val[6] = ''
			else:
				sku_tags_val[6]=product_obj.color_two.name.replace(';',' ').strip()

			if product_obj.gem_color_one.name != False:
				sku_tags_val[8]=product_obj.gem_color_one.name.replace(';',' ').strip()
			else:
				sku_tags_val[8]=''

			if product_obj.gem_color_two.name != False:
				sku_tags_val[9]=product_obj.gem_color_two.name.replace(';',' ').strip()
			else:
				sku_tags_val[9]=''

			if product_obj.gender.name != False:
				sku_tags_val[10]=product_obj.gender.name.replace(';',' ').strip()
			else:
				sku_tags_val[10]=''

			for i in range(14):
				if sku_tags_val[i] == '':
					sku_tags_val[i] = ''

			for column in sku_tags_val:
				fp.write('%s,'%column)
			fp.write('\n')

		fp.close()

		feed = 'tags.csv'
		c1="s3cmd put "+ feed +" s3://voyllaerp/erp_tag/"
		args = shlex.split(c1)
		p = subprocess.Popen(args)
		p.wait()
		c2="s3cmd setacl s3://voyllaerp/erp_tag/"+feed+" --acl-public"
		args = shlex.split(c2)
		subprocess.Popen(args)
		p.wait()
		url_base = 'https://s3.amazonaws.com/voyllaerp/erp_tag/'+feed
		vals={'name':feed,'type':'url','url':url_base}
		attachment_id=int(self.pool.get('ir.attachment').create(cr,uid,vals))
		return {'domain':"[('id','=',%i)]"%attachment_id,
				'actions':'check_action_attachment',
				'view_type': 'form',
				'view_mode': 'tree,form',
				'res_model': 'ir.attachment',
				'view_id': False,
				'type': 'ir.actions.act_window',
				'search_view_id':attachment_id
		}


	def genrate_purchase_order_for_make_to_order(self,cr,uid,ean,qty,cost,temp_flag,po_ids=None,context=None):
		product=self.pool.get('product.product')
		id_product=product.search(cr,uid,[('ean13','=',ean)])
		if len(id_product)>1:
			raise osv.except_osv(('Warning!'), ("Multiple ean or supplier is assigned to ean %s")%(ean))

		product=product.browse(cr,uid,id_product)
		template = product.product_tmpl_id.standard_price
		purchase_order = self.pool.get('purchase.order')

		product_id=self.pool.get('purchase.order.line')
		product_id=product_id.browse(cr,uid,id_product)

		tax=[]
		cost_check = True
		if template != float(cost):
			# purchase_order.write(cr,uid,po_ids,{'notes':'cost of ean %s is not same' %(ean)})
			# raise osv.except_osv(('Warning!'), ("In %s Ean cost are not same")%(ean))
			cost_check = False
		# qty_check = False
		if int(qty)==0:
			raise osv.except_osv(('Warning!'), ("In %s Ean Qty is not available")%(ean))


		for tax_id in product.supplier_taxes_id:
			tax.append(tax_id.id)
		# purchase_ids=purchase_order.search(cr,uid,[('partner_id','=',product.seller_id.id),('state','=','draft')])
		try:
			if temp_flag:
				vals= {'origin': False, 'message_follower_ids': False,
				 'order_line': [[0, False, {'product_id': product.id, 'product_uom': product.uom_id.id, 'date_planned': datetime.now().strftime("%Y-%m-%d"),
				  'price_unit': cost, 'taxon_id': product.taxon.id, 'taxes_id': [[6, False, tax]], 'product_qty': qty,
				  'account_analytic_id': False, 'manu_cost': product.manu_cost,  'supplier_product_code': product.seller_ids[0].product_code,
				  'name': product.name_template}]], 'company_id': 1, 'currency_id': 21, 'date_order': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
				   'location_id': 7, 'message_ids': False, 'dest_address_id': False, 'fiscal_position': False,
				   'picking_type_id': 1, 'partner_id': product.seller_id.id, 'journal_id': 3,
				   'bid_validity': False, 'pricelist_id': 2, 'incoterm_id': False,
				    'payment_term_id': False, 'partner_ref': False, 'notes': False,
				    'invoice_method': 'order', 'minimum_planned_date': False,
				     'related_location_id': 7}

				return purchase_order.create(cr, uid, vals, context=context),cost_check
			else:
				vals={'order_line': [[0, False, {'product_id': product.id, 'product_uom': product.uom_id.id,
				'date_planned': datetime.now().strftime("%Y-%m-%d"), 'price_unit': cost, 'taxon_id': product.taxon.id,
				 'taxes_id': [[6, False, tax]], 'product_qty': qty,
				  'account_analytic_id': False,'manu_cost':product.manu_cost,
				  'supplier_product_code': product.seller_ids[0].product_name, 'name': product.name_template}]]}
				purchase_obj=purchase_order.browse(cr,uid,po_ids)
				purchase_obj.write(vals)
				return purchase_obj.id,cost_check
		except:
			raise osv.except_osv(('Warning!'), ("In Ean %s some value is missing either supplier or product not available")%(ean))


	def po_import(self,cr,uid, ids, context=None):

		purchase_order = self.pool.get('purchase.order')
		product_product=self.pool.get('product.product')
		product_template=self.pool.get('product.template')

		data = self.browse(cr, uid, ids[0] , context=context)
		csv_data = base64.decodestring(data.file)

		chars_to_remove = ['\r', ' ', '\n']
		chars_to_remove_1 = ['\r', '\n']
		rx = "[%s]" %(re.escape(''.join(chars_to_remove)))
		rx_1 = "[%s]" %(re.escape(''.join(chars_to_remove_1)))

		print csv_data
		rows = csv_data.split("\n")

		try:
			header = re.sub(rx, '', rows[0]).split(",")
			csv_po_details = rows[1:]
		except:
			pass


		# product_id = self.pool.get('purchase.order.line')

 		product_details = {}
 		temp_flag= True
 		po_ids=None
 		cost_list = []
		for product in csv_po_details:
			if product == '':
				continue

			product_details = dict(zip(header, re.sub(rx_1, '', product).split(",")))
			purchase_id,cost_check = self.genrate_purchase_order_for_make_to_order(cr,uid,product_details['ean'],product_details['qty'],product_details['cost'],temp_flag,po_ids,context=context)

			if not cost_check:
				cost_list.append({product_details['ean']:product_details['cost']})
			# else:
			# 	cr.commit()


			if purchase_id:
				temp_flag=False
				po_ids=purchase_id

		if cost_list:
			purchase_order.write(cr,uid,po_ids,{'notes':'cost of ean %s is not same' %(cost_list)})


class return_challan_record(osv.osv):
	_name='return.challan.record'
	_columns={
		'reference' : fields.many2one('purchase.order', 'PO'),
		'supplier_id' : fields.many2one('res.partner', 'Supplier Name'),
		'name':fields.char('Return challan No'),
		'return_challan_line':fields.one2many('return.challan.line','return_challan_id','Link to return challan')
		# 'challan_no' :
}

	def create(self,cr,uid,vals,context=None):
		if vals.get('name','/')=='/':
			vals['name'] = self.pool.get('ir.sequence').get(cr, uid, 'return.challan.record') or '/'
		return super(return_challan_record, self).create(cr,uid,vals,context)

class import_tags(osv.osv_memory):
	_name = 'import.tags'
	_columns = {
	'file':fields.binary("Module File"),
	}
	def import_product_tag_file(self,cr,uid,ids,context=None):
		product_category = self.pool.get('product.category')
		product_object=self.pool.get('product.template')
		data = self.browse(cr, uid, ids[0] , context=context)
		csv_data = base64.decodestring(data.file)

		chars_to_remove = ['\r', ' ', '\n']
		chars_to_remove_1 = ['\r', '\n']
		rx = "[%s]" %(re.escape(''.join(chars_to_remove)))
		rx_1 = "[%s]" %(re.escape(''.join(chars_to_remove_1)))

		rows = csv_data.split("\n")
		try:
			header = re.sub(rx, '', rows[0]).split(",")
			csv_order_details = rows[1:]
		except:
			pass
		product_details = {}
		for product in csv_order_details:
			vals = {}
			if product == '':
				continue
			product_details = dict(zip(header, re.sub(rx_1, '', product).split(",")))
			product_ids=product_object.search(cr,uid,[('name','=',product_details['sku'])])
			prod_obj = product_object.browse(cr,uid,product_ids)
			sku_issue = product_details['sku']
			error_list = ''
			existing_tag_ids = []
			for tag in prod_obj.tagid:
				existing_tag_ids.append(tag.user_type.id)
			if len(product_ids)==1:
				tag_array=[]
				all_tags = []
				remove_tag_ids = []
				if product_details['TAG_COLLECTION_1'] or product_details['TAG_COLLECTION_2']:
					col_id = []
					if product_details['TAG_COLLECTION_1']:
						tag_id=self.pool.get('product.tagvalue').search(cr,uid,[('name','=',product_details['TAG_COLLECTION_1']),
							('tag_type','=','collection')])
						col_id.append(tag_id)
						if len(tag_id)==1 and tag_id[0] not in existing_tag_ids:
							tag_array.append([0, False, {'user_type': tag_id[0], 'tag_type': 'collection'}])
						elif not len(tag_id):
							error_list += ' collection 1 tag value is not correct,'
					if product_details['TAG_COLLECTION_2'] and product_details['TAG_COLLECTION_1']!=product_details['TAG_COLLECTION_2']:
						tag_id=self.pool.get('product.tagvalue').search(cr,uid,[('name','=',product_details['TAG_COLLECTION_2']),
							('tag_type','=','collection')])
						col_id.append(tag_id)
						if len(tag_id)==1 and tag_id[0] not in existing_tag_ids:
							tag_array.append([0, False, {'user_type': tag_id[0], 'tag_type': 'collection'}])
						elif not len(tag_id):
							error_list += ' collection 2 tag value is not correct,'

					if product_details['Update']:
						remove_ids = self.pool.get('product.tag.name').search(cr,uid,[('tag_id','=',product_ids[0]),('tag_type','=','collection'),('user_type','not in',col_id)])
						remove_tag_ids = remove_tag_ids + remove_ids

				if product_details['Category']:	
						col_id=[]
						tag_id=self.pool.get('product.tagvalue').search(cr,uid,[('name','=',product_details['Category']),
							('tag_type','=','Category')])
						col_id.append(tag_id)
						if len(tag_id)==1 and tag_id[0] not in existing_tag_ids:
							tag_array.append([0, False, {'user_type': tag_id[0], 'tag_type': 'Category'}])
						elif not len(tag_id):
							error_list += ' Category tag value is not correct,'
						if product_details['Update']:
							remove_ids = self.pool.get('product.tag.name').search(cr,uid,[('tag_id','=',product_ids[0]),('tag_type','=','Category'),('user_type','not in',col_id)])
							remove_tag_ids = remove_tag_ids + remove_ids	


				if product_details['TAG_OCCASION_1'] or product_details['TAG_OCCASION_2']:
					oc_id = []
					if product_details['TAG_OCCASION_1']:
						tag_id=self.pool.get('product.tagvalue').search(cr,uid,[('name','=',product_details['TAG_OCCASION_1']),
							('tag_type','=','occassion')])
						oc_id.append(tag_id)
						if len(tag_id)==1 and tag_id[0] not in existing_tag_ids:
							tag_array.append([0, False, {'user_type': tag_id[0], 'tag_type': 'occassion'}])
						elif not len(tag_id):
							error_list += ' occassion 1 tag value is not correct,'
					if product_details['TAG_OCCASION_2'] and product_details['TAG_OCCASION_1']!=product_details['TAG_OCCASION_2']:
						tag_id=self.pool.get('product.tagvalue').search(cr,uid,[('name','=',product_details['TAG_OCCASION_2']),
							('tag_type','=','occassion')])
						oc_id.append(tag_id)
						if len(tag_id)==1 and tag_id[0] not in existing_tag_ids:
							tag_array.append([0, False, {'user_type': tag_id[0], 'tag_type': 'occassion'}])
						elif not len(tag_id):
							error_list += ' occassion 2 value is not correct,'
					if product_details['Update']:
						remove_ids = self.pool.get('product.tag.name').search(cr,uid,[('tag_id','=',product_ids[0]),('tag_type','=','occassion'),('user_type','not in',oc_id)])
						remove_tag_ids = remove_tag_ids + remove_ids

				if product_details['TAG_MATERIAL_1'] or product_details['TAG_MATERIAL_2']:
					mat_id = []
					if product_details['TAG_MATERIAL_1']:
						tag_id=self.pool.get('product.tagvalue').search(cr,uid,[('name','=',product_details['TAG_MATERIAL_1']),
							('tag_type','=','material')])
						mat_id.append(tag_id)
						if len(tag_id)==1 and tag_id[0] not in existing_tag_ids:
							tag_array.append([0, False, {'user_type': tag_id[0], 'tag_type': 'material'}])
						elif not len(tag_id):
							error_list += ' material 1 value is not correct,'
					if product_details['TAG_MATERIAL_2'] and product_details['TAG_MATERIAL_1']!=product_details['TAG_MATERIAL_2']:
						tag_id=self.pool.get('product.tagvalue').search(cr,uid,[('name','=',product_details['TAG_MATERIAL_2']),
							('tag_type','=','material')])
						mat_id.append(tag_id)
						if len(tag_id)==1 and tag_id[0] not in existing_tag_ids:
							tag_array.append([0, False, {'user_type': tag_id[0], 'tag_type': 'material'}])
						elif not len(tag_id):
							error_list += ' material 2 value is not correct,'
					if product_details['Update']:
						remove_ids = self.pool.get('product.tag.name').search(cr,uid,[('tag_id','=',product_ids[0]),('tag_type','=','material'),('user_type','not in',mat_id)])
						remove_tag_ids = remove_tag_ids + remove_ids

				if product_details['TAG_PLATING_1'] or product_details['TAG_PLATING_2']:
					pat_id = []
					if product_details['TAG_PLATING_1']:
						tag_id=self.pool.get('product.tagvalue').search(cr,uid,[('name','=',product_details['TAG_PLATING_1']),
							('tag_type','=','plating')])
						pat_id.append(tag_id)
						if len(tag_id)==1 and tag_id[0] not in existing_tag_ids:
							tag_array.append([0, False, {'user_type': tag_id[0], 'tag_type': 'plating'}])
						elif not len(tag_id):
							error_list += ' plating 1 value is not correct,'

					if product_details['TAG_PLATING_2'] and product_details['TAG_PLATING_2']!=product_details['TAG_PLATING_1']:
						tag_id=self.pool.get('product.tagvalue').search(cr,uid,[('name','=',product_details['TAG_PLATING_2']),
							('tag_type','=','plating')])
						pat_id.append(tag_id)
						if len(tag_id)==1 and tag_id[0] not in existing_tag_ids:
							tag_array.append([0, False, {'user_type': tag_id[0], 'tag_type': 'plating'}])
						elif not len(tag_id):
							error_list += ' plating 2 value is not correct,'
					if product_details['Update']:
						remove_ids = self.pool.get('product.tag.name').search(cr,uid,[('tag_id','=',product_ids[0]),('tag_type','=','plating'),('user_type','not in',pat_id)])
						remove_tag_ids = remove_tag_ids + remove_ids

				if product_details['TAG_GEMSTONES_1'] or product_details['TAG_GEMSTONES_2']:
					gem_id = []
					if product_details['TAG_GEMSTONES_1']:
						tag_id=self.pool.get('product.tagvalue').search(cr,uid,[('name','=',product_details['TAG_GEMSTONES_1']),
							('tag_type','=','gemstone')])
						gem_id.append(tag_id)
						if len(tag_id)==1 and tag_id[0] not in existing_tag_ids:
							tag_array.append([0, False, {'user_type': tag_id[0], 'tag_type': 'gemstone'}])
						elif not len(tag_id):
							error_list += ' gemstone 1 value is not correct,'

					if product_details['TAG_GEMSTONES_2'] and product_details['TAG_GEMSTONES_2']!=product_details['TAG_GEMSTONES_1']:
						tag_id=self.pool.get('product.tagvalue').search(cr,uid,[('name','=',product_details['TAG_GEMSTONES_2']),
							('tag_type','=','gemstone')])
						gem_id.append(tag_id)
						if len(tag_id)==1 and tag_id[0] not in existing_tag_ids:
							tag_array.append([0, False, {'user_type': tag_id[0], 'tag_type': 'gemstone'}])
						elif not len(tag_id):
							error_list += ' tag gemstone 2 value is not correct,'
					if product_details['Update']:
						remove_ids = self.pool.get('product.tag.name').search(cr,uid,[('tag_id','=',product_ids[0]),('tag_type','=','gemstone'),('user_type','not in',gem_id)])
						remove_tag_ids = remove_tag_ids + remove_ids

				if product_details['TAG_FREE_PRODUCTS_1']:
					tag_id=self.pool.get('product.tagvalue').search(cr,uid,[('name','=',product_details['TAG_FREE_PRODUCTS_1']),
						('tag_type','=','free_product')])
					if len(tag_id)==1 and tag_id[0] not in existing_tag_ids:
						tag_array.append([0, False, {'user_type': tag_id[0], 'tag_type': 'free_product'}])
					elif not len(tag_id):
						error_list += ' free_product value is not correct,'
					if product_details['Update']:
						remove_ids = self.pool.get('product.tag.name').search(cr,uid,[('tag_id','=',product_ids[0]),('tag_type','=','free_product'),('user_type','not in',tag_id)])
						remove_tag_ids = remove_tag_ids + remove_ids

				if product_details['TAG_HOME_PAGE_LINK_1']:
					tag_id=self.pool.get('product.tagvalue').search(cr,uid,[('name','=',product_details['TAG_HOME_PAGE_LINK_1']),
						('tag_type','=','home_page')])
					if len(tag_id)==1 and tag_id[0] not in existing_tag_ids:
						tag_array.append([0, False, {'user_type': tag_id[0], 'tag_type': 'home_page'}])
					elif not len(tag_id):
						error_list += ' home_page value is not correct,'
					if product_details['Update']:
						remove_ids = self.pool.get('product.tag.name').search(cr,uid,[('tag_id','=',product_ids[0]),('tag_type','=','home_page'),('user_type','not in',tag_id)])
						remove_tag_ids = remove_tag_ids + remove_ids

				if product_details['TAG_TYPE_1'] or product_details['TAG_TYPE_2']:
					type_id = []
					if product_details['TAG_TYPE_1']:
						tag_id=self.pool.get('product.tagvalue').search(cr,uid,[('name','=',product_details['TAG_TYPE_1']),
							('tag_type','=','type')])
						type_id.append(tag_id)
						if len(tag_id)==1 and tag_id[0] not in existing_tag_ids:
							tag_array.append([0, False, {'user_type': tag_id[0], 'tag_type': 'type'}])
						elif not len(tag_id):
							error_list += ' type 1 value is not correct,'

					if product_details['TAG_TYPE_2'] and product_details['TAG_TYPE_2']!=product_details['TAG_TYPE_1']:
						tag_id=self.pool.get('product.tagvalue').search(cr,uid,[('name','=',product_details['TAG_TYPE_2']),
							('tag_type','=','type')])
						type_id.append(tag_id)
						if len(tag_id)==1 and tag_id[0] not in existing_tag_ids:
							tag_array.append([0, False, {'user_type': tag_id[0], 'tag_type': 'type'}])
						elif not len(tag_id):
							error_list += ' type 2 value is not correct,'
					if product_details['Update']:
						remove_ids = self.pool.get('product.tag.name').search(cr,uid,[('tag_id','=',product_ids[0]),('tag_type','=','type'),('user_type','not in',type_id)])
						remove_tag_ids = remove_tag_ids + remove_ids

				remove_tag_ids = [[2,x,False] for x in remove_tag_ids]
				final_tags_list = []
				try:
					if tag_array:
						tag_array = [list(x) for x in {(tuple(e)) for e in tag_array}]
					final_tags_list = tag_array+remove_tag_ids
				except:
					final_tags_list = tag_array+remove_tag_ids
				if error_list:
					raise osv.except_osv(("Update Product Tags Error!!!!"), ('%s : %s') % (product_details['sku'],error_list))
				vals.update({'tagid':final_tags_list})
				product_object.write(cr,uid,product_ids[0],vals)
			else:
				raise osv.except_osv(("Update Product Tags Error!!!!"), (' sku %s not available ') % (product_details['sku']))


class purchase_status(osv.osv):
	_inherit = 'purchase.order'

	def _sel_select_status(self,cr,uid,context=None):
		user_group = self.pool.get('res.groups')
		ll=self.pool.get('res.users').read(cr,uid,[uid],['groups_id'])
		temp=ll[0]['groups_id']
		inbound_id=user_group.search(cr,uid,[('name','=','Inbound User')])
		other_id=user_group.search(cr,uid,[('name','in',('Measurement User','Photoshoot User','Merchandising User',
			'Hard Tagging User','Quality Control User','Content User'))])
		if inbound_id[0] in temp:
			return [('50','Get Status')]
	def _get_content(self,cr,uid,ids,name,arg,context=None):
		temp = {}
		po = self.pool.get('purchase.order')
		product_obj = self.pool.get('product.product')
		purchase_group_object = self.pool.get('purchase.order.group')
		purchase_object = po.browse(cr,uid,ids)
		for po_ids in purchase_object :
			if po_ids.state=='approved':
				context1 = {'lang': 'en_US', 'tz': 'Asia/Kolkata', 'uid': uid, 'active_model': 'purchase.order',
				'search_disable_custom_filters': True, 'active_ids': [po_ids.id],
				'location_id': 11,'active_id': po_ids.id}
				temp_dest = False
				temp_product = purchase_group_object.on_change(cr,uid,[],temp_dest,context1)
				if len(temp_product['value']['temp'])!=0:
					temp_flag_1 = True
					for t in temp_product['value']['temp']:
						if t.qty > 0:
							temp_flag_1 = False
							temp[po_ids.id] = True
							break
					if temp_flag_1:
						temp[po_ids.id] = False
				else:
					temp[po_ids.id] = False
			else:
				temp[po_ids.id] = False

		return temp


	def _get_hardtagging(self,cr,uid,ids,name,arg,context=None):

		temp = {}
		po = self.pool.get('purchase.order')
		product_obj = self.pool.get('product.product')
		purchase_group_object = self.pool.get('purchase.order.group')
		purchase_object = po.browse(cr,uid,ids)
		for po_ids in purchase_object :
			if po_ids.state=='approved':
				context1 = {'lang': 'en_US', 'tz': 'Asia/Kolkata', 'uid': uid, 'active_model': 'purchase.order',
				'search_disable_custom_filters': True, 'active_ids': [po_ids.id],
				'location_id': 23,'active_id': po_ids.id}
				temp_dest = False
				temp_product = purchase_group_object.on_change(cr,uid,[],temp_dest,context1)
				if len(temp_product['value']['temp'])!=0:
					temp_flag_1 = True
					for t in temp_product['value']['temp']:
						if t.qty > 0:
							temp_flag_1 = False
							temp[po_ids.id] = True
							break
					if temp_flag_1:
						temp[po_ids.id] = False
				else:
					temp[po_ids.id] = False
			else:
				temp[po_ids.id] = False

		return temp

	def _get_qc(self,cr,uid,ids,name,arg,context=None):

		temp = {}
		po = self.pool.get('purchase.order')
		product_obj = self.pool.get('product.product')
		purchase_group_object = self.pool.get('purchase.order.group')
		purchase_object = po.browse(cr,uid,ids)
		for po_ids in purchase_object :
			if po_ids.state=='approved':
				context1 = {'lang': 'en_US', 'tz': 'Asia/Kolkata', 'uid': uid, 'active_model': 'purchase.order',
				'search_disable_custom_filters': True, 'active_ids': [po_ids.id],
				'location_id': 4,'active_id': po_ids.id}
				temp_dest = False
				temp_product = purchase_group_object.on_change(cr,uid,[],temp_dest,context1)
				if len(temp_product['value']['temp'])!=0:
					temp_flag_1 = True
					for t in temp_product['value']['temp']:
						if t.qty > 0:
							temp_flag_1 = False
							temp[po_ids.id] = True
							break
					if temp_flag_1:
						temp[po_ids.id] = False
				else:
					temp[po_ids.id] = False
			else:
				temp[po_ids.id] = False
		return temp

	def _get_inbound(self,cr,uid,ids,name,arg,context=None):

		temp = {}
		po = self.pool.get('purchase.order')
		product_obj = self.pool.get('product.product')
		purchase_group_object = self.pool.get('purchase.order.group')
		purchase_object = po.browse(cr,uid,ids)
		for po_ids in purchase_object :
			if po_ids.state=='approved':
				context1 = {'lang': 'en_US', 'tz': 'Asia/Kolkata', 'uid': uid, 'active_model': 'purchase.order',
				'search_disable_custom_filters': True, 'active_ids': [po_ids.id],
				'location_id': 7,'active_id': po_ids.id}
				temp_dest = False
				temp_product = purchase_group_object.on_change(cr,uid,[],temp_dest,context1)
				if len(temp_product['value']['temp'])!=0:
					temp_flag_1 = True
					for t in temp_product['value']['temp']:
						if t.qty > 0:
							temp_flag_1 = False
							temp[po_ids.id] = True
							break
					if temp_flag_1:
						temp[po_ids.id] = False
				else:
					temp[po_ids.id] = False
			else:
				temp[po_ids.id] = False

		return temp

	def _get_rejected(self,cr,uid,ids,name,arg,context=None):

		temp = {}
		po = self.pool.get('purchase.order')
		product_obj = self.pool.get('product.product')
		purchase_group_object = self.pool.get('purchase.order.group')
		purchase_object = po.browse(cr,uid,ids)
		for po_ids in purchase_object :
			if po_ids.state=='approved':
				context1 = {'lang': 'en_US', 'tz': 'Asia/Kolkata', 'uid': uid, 'active_model': 'purchase.order',
				'search_disable_custom_filters': True, 'active_ids': [po_ids.id],
				'location_id': 8,'active_id': po_ids.id}
				temp_dest = False
				temp_product = purchase_group_object.on_change(cr,uid,[],temp_dest,context1)
				if len(temp_product['value']['temp'])!=0:
					temp_flag_1 = True
					for t in temp_product['value']['temp']:
						if t.qty > 0:
							temp_flag_1 = False
							temp[po_ids.id] = True
							break
					if temp_flag_1:
						temp[po_ids.id] = False
				else:
					temp[po_ids.id] = False
			else:
				temp[po_ids.id] = False

		return temp

	def _get_live(self,cr,uid,ids,name,arg,context=None):

		temp = {}
		po = self.pool.get('purchase.order')
		product_obj = self.pool.get('product.product')
		purchase_group_object = self.pool.get('purchase.order.group')
		purchase_object = po.browse(cr,uid,ids)
		for po_ids in purchase_object :
			if po_ids.state=='approved':
				context1 = {'lang': 'en_US', 'tz': 'Asia/Kolkata', 'uid': uid, 'active_model': 'purchase.order',
				'search_disable_custom_filters': True, 'active_ids': [po_ids.id],
				'location_id': 30,'active_id': po_ids.id}
				temp_dest = False
				temp_product = purchase_group_object.on_change(cr,uid,[],temp_dest,context1)
				if len(temp_product['value']['temp'])!=0:
					temp_flag_1 = True
					for t in temp_product['value']['temp']:
						if t.qty > 0:
							temp_flag_1 = False
							temp[po_ids.id] = True
							break
					if temp_flag_1:
						temp[po_ids.id] = False
				else:
					temp[po_ids.id] = False
			else:
				temp[po_ids.id] = False

		return temp


	_columns = {
			'po_id':fields.integer('purchase order'),
			'qc1':fields.function(_get_qc, string="QC", type="boolean",method="True"),
			'hardtagging1':fields.function(_get_hardtagging, string="Hardtagging", type="boolean", method="True"),
			'content1':fields.function(_get_content, string="Content", type="boolean", method="True"),
			'live1':fields.function(_get_live,string="Live", type="boolean",method="True"),
			'rejected1':fields.function(_get_rejected, string="Rejected", type="boolean", method="True"),
			'inbound1':fields.function(_get_inbound,string="Inbound", type="boolean", method="True"),
			'get_status':fields.char('Get Status'),
			'status':fields.selection(_sel_select_status,'Get Status',required=True),
	}

class po_status(osv.osv):
	_name = 'po.status'
	_columns = {
			'name':fields.many2one('purchase.order','PO Name'),
			'product':fields.many2one('product.product','Product',readonly=True),
			'po_id':fields.integer('purchase order'),
			'qc':fields.integer('QC'),
			'hardtagging':fields.integer('Hardtagging'),
			'content':fields.integer('Content'),
			'live':fields.integer('Live'),
			'rejected':fields.integer('Rejected'),
			'inbound':fields.integer('Inbound'),
			'get_status':fields.char('Get Status'),
			}

class temp_status(osv.osv):
	_name="temp.status"
	def _sel_select_status(self,cr,uid,context=None):
		user_group = self.pool.get('res.groups')
		ll=self.pool.get('res.users').read(cr,uid,[uid],['groups_id'])
		temp=ll[0]['groups_id']
		inbound_id=user_group.search(cr,uid,[('name','=','Inbound User')])
		other_id=user_group.search(cr,uid,[('name','in',('Measurement User','Photoshoot User','Merchandising User',
			'Hard Tagging User','Quality Control User','Content User'))])
		if inbound_id[0] in temp:
			return [('50','Get Status')]
	_columns={
	'temp':fields.one2many('po.status','po_id'),
	'status':fields.selection(_sel_select_status,'Get Status',required=True),

	}


	def on_change(self,cr,uid,ids,status,context=None):
		if not status:
			return
		po = self.pool.get('purchase.order')
		product_obj = self.pool.get('product.product')

		purchase_object = po.browse(cr,uid,context['active_ids'])

		product_object = po.browse(cr,uid,context['active_ids']).order_line
		product_object = po.browse(cr,uid,context['active_ids']).order_line
		stock_picking=self.pool.get('stock.picking')
		stock_picking_id=stock_picking.search(cr,uid,[('origin','=',purchase_object.name)])
		stock_picking_obj=stock_picking.browse(cr,uid,stock_picking_id)
		def move_ids(x):
			return x.move_lines
		temp=map(move_ids,stock_picking_obj)
		temp=reduce(lambda x,y:x+y,temp)
		location_id_list=[]


		for location_id in context['location_id']:
			available_product={}
			for stock_moves in temp:
				if stock_moves.state in ['confirmed','done','assigned'] and stock_moves.location_id.id==location_id:
					# available_product.update({'location_id':location_id})
					if stock_moves.product_id.id in available_product:
						available_product[stock_moves.product_id.id]-=stock_moves.product_qty
					else:
						available_product.update({stock_moves.product_id.id: -stock_moves.product_qty})
					# location_id_list.append(available_product)
				elif stock_moves.state in ['done'] and stock_moves.location_dest_id.id==location_id:
					# available_product.update({'location_id':location_id})
					if stock_moves.product_id.id in available_product:
						available_product[stock_moves.product_id.id]+=stock_moves.product_qty
					else:
						available_product.update({stock_moves.product_id.id:stock_moves.product_qty})
				# location_id_list.append(available_product)
			available_product.update({'location_id':location_id})
			location_id_list.append(available_product)
		print location_id_list
		vals = {'status':status}
		self_id = self.create(cr,uid,vals)
		temp_obj = self.pool.get('po.status')

		temp_po_array=[]
		temp_obj1=[]

		for dict_value in location_id_list:
			if dict_value['location_id']==8:
				location = 'rejected'
			elif dict_value['location_id']==7:
				location = 'inbound'
			elif dict_value['location_id']==4:
				location = 'qc'
			elif dict_value['location_id']==23:
				location = 'content'
			elif dict_value['location_id']==30:
				location = 'live'
			elif dict_value['location_id']==11:
				location = 'hardtagging'
			temp_list = dict_value.keys()
			for value in temp_list:
				if value != 'location_id':
					product_id=value
					values_to_write = {}
					z = temp_obj.search(cr,uid,[('po_id','=', self_id ),('product', '=', value)])
					if not z:
						values3= {"po_id":self_id,"product":value}
						z = temp_obj.create(cr,uid,values3,context)
					if location=='hardtagging':
						hardtagging=temp_obj.browse(cr,uid,z).hardtagging
						values_to_write['hardtagging']= dict_value[value]
					elif location=='inbound':
						inbound=temp_obj.browse(cr,uid,z).inbound
						values_to_write['inbound']= dict_value[value]
					elif location=='rejected':
						values_to_write['rejected']=dict_value[value]
					elif location=='live':
						values_to_write['live']=dict_value[value]
					elif location=='qc':
						values_to_write['qc']=dict_value[value]
					elif location=='content':
						values_to_write['content']=dict_value[value]
					temp_obj.write(cr,uid,z,values_to_write)


		temp_po_array=self.browse(cr,uid,self_id).temp
		vals = {'temp': temp_po_array}
		return {'value': vals}



class return_challan_line(osv.osv):
	_name='return.challan.line'
	_columns={
		'product_id':fields.many2one('product.product','Product'),
		'qty':fields.integer('Qty'),
		# 'price':fields.integer('Price'),
		'remark':fields.char('Remark'),
		'return_challan_id':fields.integer('Retun Challan')
	}

class sales_analytics(osv.osv):
	_name = 'sales.analytics'

	def sale_analytics_report(self,cr,uid,ids,context=None):
		product=self.pool.get('product.template')
		sql = "select * from sales_analytics()"
		cr.execute(sql)
		sale_store = cr.fetchall()
		header = ['sku','create_date','supplier','qty_in_3mon','first_sale','last_sale','avail_date','taxon', 'cost','on_hand','forcasted','qty_afte_avail','sale_price','mrp','sale_in_3month','product_id','size']
		# file_header = ['Sku','Designer','Taxon','Cost','Current Inventory','Create Date','Available On Date','First Sale in last 6 month','Last Sale in last 6 month','Peroid of sale from last procurment','Qunatity sold after last procurement','Quantity sold per month','Values of sales per month','Inventory days','Quantity to Order','Discount on cost needed','Sale Price', 'MRP','Priority Rank','Qty sold in 3 months','Qunatity to order updated','combo components']
		file_header = ['Sku','Designer','Taxon','Cost','Current Inventory','Create Date','Available On Date','First Sale in last 6 month','Last Sale in last 6 month','Peroid of sale from last procurment','Qunatity sold after last procurement','Quantity sold per month','Values of sales per month','Inventory days','Quantity to Order','Discount on cost needed','Sale Price', 'MRP','Priority Rank','Qty sold in 3 months','Size']
		book = xlwt.Workbook()
		analytic_report = book.add_sheet('Sales analytics')
		style = xlwt.easyxf('font: height 200, name Times New Roman, bold on; align: wrap on,vert centre;')
		for index,element in enumerate(file_header):
			analytic_report.write(0,index,'%s'%file_header[index],style)
		col =0
		quantity_order={}
		combo_sku={}
		for index,element in enumerate(sale_store):
			row = dict(zip(header, element))
			if row['qty_afte_avail'] > row['qty_in_3mon']:
				row['qty_afte_avail'] = row['qty_in_3mon']
			try:
				row['on_hand'] = max(row['on_hand'],0)
			except:
				row['on_hand'] = 0
			if row['on_hand'] < 1:
				sale_on_avail = row['last_sale']
			else:
				sale_on_avail = datetime.now()
				sale_on_avail = datetime.strftime(sale_on_avail, "%Y-%m-%d")
			try:
				last_sale_date = datetime.strptime(sale_on_avail, "%Y-%m-%d")
			except:
				last_sale_date = 'N/A'
			try:
				avail_on_date = datetime.strptime(row['avail_date'], "%Y-%m-%d")
				sale_frm_last_procment = abs((last_sale_date-avail_on_date).days) + 1
			except:
				sale_frm_last_procment = 'N/A'
			try:
				sld_per_month = row['qty_afte_avail']*30/sale_frm_last_procment	
			except:
				sld_per_month = 'N/A'
			try:
				inventory_days = int(row['on_hand']*30/sld_per_month + 0.5)
			except:
				inventory_days = 'N/A'
			try:		
				value_sold_per_mon = row['qty_afte_avail']*row['cost']*30/sale_frm_last_procment
				value_per_month = int(value_sold_per_mon+0.5)
			except:
				value_per_month = 'N/A'
			try:
				not_sold_days = abs((datetime.strptime(datetime.strftime(datetime.now(), "%Y-%m-%d"),"%Y-%m-%d")-last_sale_date).days)
				if row['sale_in_3month'] == 0:
					forcasted_values = 0
				elif not_sold_days < 14 or (sale_frm_last_procment <14 and not_sold_days < 30):
					try:	
						forcasted_values = 3*sld_per_month - row['on_hand']
					except:
						forcasted_values = 0
				elif sale_frm_last_procment <14 and not_sold_days< 60:
					try:	
						forcasted_values = 2*sld_per_month - row['on_hand']
					except:
						forcasted_values = 0
				elif sale_frm_last_procment <14 and not_sold_days < 90:
					try:	
						forcasted_values = sld_per_month - row['on_hand']
					except:
						forcasted_values = 0
				else:
					try:	
						forcasted_values = int((7.0/30*sld_per_month) - row['on_hand']+0.5)
					except:
						forcasted_values = 0
			except:
				forcasted_values = 0
			forcasted_values = int(forcasted_values+0.5)
			
			current_date = datetime.now()
			current_date = datetime.strftime(current_date, "%Y-%m-%d")
			current_date = datetime.strptime(current_date, "%Y-%m-%d")
			last_date = row['last_sale']
			last_date = datetime.strptime(last_date, "%Y-%m-%d")
			days_from_last_sale = abs((last_date-current_date).days)
			if days_from_last_sale < 7 and forcasted_values>20:
				priority_rank = 1
			elif days_from_last_sale < 14 and forcasted_values>20:
				priority_rank = 2
			elif days_from_last_sale < 30 and forcasted_values>20:
				priority_rank = 3
			elif days_from_last_sale < 45 :
				priority_rank = 4
			else:
				priority_rank = 5
			
			# if product.browse(cr,uid,row['product_id']).is_combos:
			# 	tmpl_id=row['product_id']
			# 	try:				
			# 		sql2="""select round(min(r.quantity_on_hand/r.qty)) from (select quantity_on_hand,p.qty from (select cp.product,cp.qty from combo_product as cp where cp.qty1=%s) as p left join quantity_sync_get_all_product_quantity() as q on q.product_id=p.product) as r"""%tmpl_id
			# 		cr.execute(sql2)
			# 		combo_min=cr.fetchall()
			# 		combo=combo_min[0][0]
			# 		row['on_hand'] = combo
			# 		combo_sku[row['sku']]=tmpl_id
			# 	except:
			# 		pass	

			analytic_report.write(index+1,col,row['sku'])
			analytic_report.write(index+1,col+1,row['supplier'])
			analytic_report.write(index+1,col+2,'%s'%row['taxon'])
			analytic_report.write(index+1,col+3,row['cost'])
			analytic_report.write(index+1,col+4,row['on_hand'])
			analytic_report.write(index+1,col+5,'%s'%row['create_date'])
			analytic_report.write(index+1,col+6,'%s'%row['avail_date'])
			analytic_report.write(index+1,col+7,'%s'%row['first_sale'])
			analytic_report.write(index+1,col+8,'%s'%row['last_sale'])
			analytic_report.write(index+1,col+9,sale_frm_last_procment)
			analytic_report.write(index+1,col+10,row['qty_afte_avail'])
			analytic_report.write(index+1,col+11,sld_per_month)
			analytic_report.write(index+1,col+12,value_per_month)
			analytic_report.write(index+1,col+13,inventory_days)
			analytic_report.write(index+1,col+14,max(forcasted_values,0))
			analytic_report.write(index+1,col+15,0)
			analytic_report.write(index+1,col+16,row['sale_price'])
			analytic_report.write(index+1,col+17,row['mrp'])
			analytic_report.write(index+1, col+18,priority_rank)
			analytic_report.write(index+1, col+19, row['qty_in_3mon'])
			analytic_report.write(index+1, col+20, row['size'])
			quantity_order[row['sku']]=max(forcasted_values,0)
		
		# combo_dict={}
		# for sku_quant in quantity_order:
		# 	if sku_quant not in combo_sku.keys():
		# 		continue
		# 	try:
		# 		sql3="""select q.name_template from (select product,qty from combo_product where qty1=%s) as p left join product_product as q on q.id=p.product"""%combo_sku[sku_quant]
		# 		cr.execute(sql3)
		# 		all_sku=cr.fetchall()
				
		# 		try:
		# 			if len(all_sku)==0:
		# 				combo_dict[sku_quant]=''
		# 			else:
		# 				name=''
		# 				for a in all_sku:
		# 					name=name+a[0]
		# 					name=name+','
		# 				combo_dict[sku_quant]=name
		# 		except:
		# 			pass		
		# 		if len(all_sku) !=0:
		# 			for one in all_sku:
		# 				quantity_order[one[0]]=quantity_order[one[0]]+quantity_order[sku_quant]
		# 			quantity_order[sku_quant]=0
		# 	except:
		# 		pass		
		
		# for index,element in enumerate(sale_store):
		# 	row = dict(zip(header, element))
		# 	analytic_report.write(index+1, col+20, quantity_order[row['sku']])
		# 	try:
		# 		analytic_report.write(index+1, col+21, combo_dict[row['sku']])
		# 	except:
		# 		pass	

		book.save('sales_analytics.xls')
		feed='sales_analytics.xls'
		c1="s3cmd put "+ feed +" s3://voyllaerp/erp_tag/"
		args = shlex.split(c1)
		p = subprocess.Popen(args)
		p.wait()
		c2="s3cmd setacl s3://voyllaerp/erp_tag/"+feed+" --acl-public"
		args = shlex.split(c2)
		subprocess.Popen(args)
		p.wait()
		url_base = 'https://s3.amazonaws.com/voyllaerp/erp_tag/'+feed
		vals={'name':feed,'type':'url','url':url_base}
		attachment_id=int(self.pool.get('ir.attachment').create(cr,uid,vals))
		return {
			'domain':"[('id','=',%i)]"%attachment_id,
			'actions':'check_action_attachment',
			'view_type': 'form',
			'view_mode': 'tree,form',
			'res_model': 'ir.attachment',
			'view_id': False,
			'type': 'ir.actions.act_window',
			'search_view_id':attachment_id
		}
		# sku_details = dict(zip(header, )
