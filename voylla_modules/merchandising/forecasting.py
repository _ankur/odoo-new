from openerp.osv import fields, osv
import pdb
import re
import urllib
import urllib2
import json
import base64
from datetime import datetime
import datetime
from sys import exit
from math import sqrt
import numpy as np
import math
from scipy.optimize import fmin_l_bfgs_b

class sku_level_forcasting(osv.osv):
	_name='sku.level_forcasting'
	_columns={
		'product':fields.char('Product Name'),
		'forecasted_value':fields.integer('Forecasted value'),
		'product_category_type_2':fields.text('Category id')
	}
	
class week_wise_data(osv.osv):
	_name="week.wise_data"
	_columns={
		'week-1':fields.float('week-1'),
		'week-2':fields.float('week-2'),
		'week-3':fields.float('week-3'),
		'week-4':fields.float('week-4'),
		'product_category_type_1':fields.text('Category id'),
		'sku_level_forcasting':fields.one2many('sku.level_forcasting','product_category_type_2','sku')
	}
	def create(self,cr,uid,vals,context=None):
		return super(week_wise_data,self).create(cr,uid,vals,context)

class product_category_type(osv.osv):

	_name='product.category_type'
	_columns={
		'theme':fields.char('Theme'),
		'design':fields.char('Design'),
		'collection':fields.char('Collection'),
		'material':fields.char('Material'),
		'price_band':fields.char('Cost Range'),
		'forecasted_value':fields.float('Forecasted Value'),
		'attribute_id':fields.text('Attribute'),
		'sku_level_forcasting':fields.one2many('week.wise_data','product_category_type_1','sku')
	}
	def sku_level_forecast(self,cr,uid,vals,context=None):
		ids = self.pool.get('week.wise_data').search(cr,uid,[('product_category_type_1','=',str(vals[0]))])
		return {
            'domain':"[('id','=',%i)]"%ids[0],
            'actions':'demand_forecasting_action-1',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'week.wise_data',
            'view_id': False,
            'target':'new',
            'type': 'ir.actions.act_window',
            'search_view_id':ids[0]
        }

class demand_forecasting(osv.osv):
	_name='demand.forecasting'

	def _calculate_forecasted_value(self, cr, uid, ids, name, arg, context=None):
		temp={}
		demand_obj=self.browse(cr,uid,ids)
		for j in demand_obj:
			k=0
			for i in j.category_type_id:
				k+=i.forecasted_value
			temp[j.id]=k
		return temp

	_columns={
		'this_year_week_no':fields.integer("This Year's Week No."),
		'last_year_week_no':fields.integer("Last Year's Week No."),
		'category_type_id':fields.one2many('product.category_type','attribute_id','Attribute'),
		'taxon':fields.char('Taxon'),
		'type':fields.char('Type'),
		'month':fields.integer('month'),
		'forecasted_value':fields.function(_calculate_forecasted_value,type='integer',string='Total Forecasted Value',method=True)
	}
	def create(self,cr,uid,vals,context=None):
		return super(demand_forecasting,self).create(cr,uid,vals,context)
	def write(self,cr,uid,ids,vals,context=None):
		return super(demand_forecasting,self).write(cr,uid,ids,vals,context)


	def multiplicative(self, cr, uid, data, week_no_mapping_from, week_no_mapping_to, data_points_per_year, number_of_points_to_forecast, taxon, type_val, theme, design, collection, material, price_band, last_month_sku_qty_list, alpha = None, beta = None, gamma = None):

		present_data = data[:]

		def RMSE(params, *args):

			present_data = args[0]
			type = args[1]
			rmse = 0

			alpha, beta, gamma = params
			data_points_per_year = args[2]
			temp = sum(present_data[0:data_points_per_year]) / float(data_points_per_year)

			if type == 'multiplicative':

				seasonal_factor = [present_data[i] / float(temp) for i in range(data_points_per_year)]
				smoothed_average = [present_data[data_points_per_year-1]/float(seasonal_factor[-1])]
				trend_factor = [0.00,]
				forecast_for_rmse_calculation = []
				rmse = 0

			 	for i in range((len(present_data)-data_points_per_year)):
			 		smoothed_average.append(alpha*(present_data[data_points_per_year+i]/seasonal_factor[i])+(1-alpha)*(smoothed_average[-1]+trend_factor[-1]))
			 		trend_factor.append(beta*(smoothed_average[-1]-smoothed_average[-2])+(1-beta)*trend_factor[-1])
			 		seasonal_factor.append(gamma*(present_data[data_points_per_year+i]/smoothed_average[-1])+(1-gamma)*seasonal_factor[i])
			 		forecast_for_rmse_calculation.append((smoothed_average[i]+trend_factor[i])*seasonal_factor[i])

			rmse = sqrt(sum([(m-n)**2 for m,n in zip(present_data[data_points_per_year:],forecast_for_rmse_calculation[:])])/float(len(forecast_for_rmse_calculation)))

			return rmse

		if (alpha == None or beta == None or gamma == None):

			initial_values = np.array([0.0, 1.0, 0.0])
			boundaries = [(0, 1), (0, 1), (0, 1)]
			type = 'multiplicative'
			parameters = fmin_l_bfgs_b(RMSE, x0 = initial_values, args = (present_data, type, data_points_per_year), bounds = boundaries, approx_grad = True)
			## RMSE : func to be minimized.
			## x0 : ndarray, Initial guess.
			## args : arguments to pass to func RMSE.
			## bounds : list, (min, max) pairs for each element in x,
			 # defining the bounds on that parameter. Use None for one
			 # of min or max when there is no bound in that direction.
			## approx_grad : bool, Whether to approximate the gradient
			 # numerically (in which case func returns only the function value).
			## Returns x : array_like, Estimated position of the minimum.

			alpha, beta, gamma = parameters[0]

		temp = sum(present_data[0:data_points_per_year]) / float(data_points_per_year)
		seasonal_factor = [present_data[i] / float(temp) for i in range(data_points_per_year)]
		smoothed_average = [present_data[data_points_per_year-1]/float(seasonal_factor[-1])]
		trend_factor = [0.00,]
		forecast_for_rmse_calculation = []
		Forecast =[]
		rmse = 0

	 	for i in range((len(present_data)-data_points_per_year)):
			smoothed_average.append(alpha*(present_data[data_points_per_year+i]/seasonal_factor[i])+(1-alpha)*(smoothed_average[-1]+trend_factor[-1]))
	 		trend_factor.append(beta*(smoothed_average[-1]-smoothed_average[-2])+(1-beta)*trend_factor[-1])
			seasonal_factor.append(gamma*(present_data[data_points_per_year+i]/smoothed_average[-1])+(1-gamma)*seasonal_factor[i])
	 		forecast_for_rmse_calculation.append((smoothed_average[i]+trend_factor[i])*seasonal_factor[i])

	 	
		no_of_days_in_last_week_of_year = (datetime.date(datetime.datetime.now().year,12,31).isocalendar()[2]+1)%7
		no_of_weeks_till_now = len(seasonal_factor)

		diff = week_no_mapping_from - week_no_mapping_to

	 	for r in range(number_of_points_to_forecast):
			if (no_of_weeks_till_now+5+r)%53 == 53:
	 			Forecast.append(max(math.ceil(((smoothed_average[-1]+(r+5)*trend_factor[-1])*(seasonal_factor[-data_points_per_year+diff+r+4]))*(no_of_days_in_last_week_of_year/float(7.0))),0))
			else:
				Forecast.append(max(math.ceil((smoothed_average[-1]+(r+5)*trend_factor[-1])*(seasonal_factor[-data_points_per_year+diff+r+4])),0))

	 	rmse = sqrt(sum([(m-n)**2 for m,n in zip(present_data[data_points_per_year:],forecast_for_rmse_calculation[:])])/float(len(forecast_for_rmse_calculation)))

	 	# Calculation to break forecast to SKU level
	 	last_month_total_sale = 0.0
	 	for sku_qty in last_month_sku_qty_list:
	 		last_month_total_sale += float(sku_qty[1])

	 	total_forecast_value = sum(Forecast)
	 	sku_forecast_list = []
	 	for sku_qty in last_month_sku_qty_list:
	 		sku_forecast = math.ceil((float(sku_qty[1])/float(last_month_total_sale))*total_forecast_value)
	 		sku_level_temp=[0,False,{'product': sku_qty[0], 'forecasted_value':sku_forecast}]
	 		sku_forecast_list.append(sku_level_temp)

	 	# current_week = datetime.date(datetime.datetime.now().year,datetime.datetime.now().month,datetime.datetime.now().day).isocalendar()[1]
		# vals={'taxon':taxon, 'type':type_val, 'theme':theme, 'design':design, 'collection':collection, 'material':material, 'price_band':price_band}
		# vals1={'category_type_id': [[0, False, {'week': 1, 'price_band': 'SDVSV', 'material': 'SDVDSV', 'collection': 'ASDVASDV', 'theme': 'SDVSDV', 'design': 'SDVASV'}],
		#  [0, False, {'week': 2, 'price_band': 'ASDVV', 'material': 'SDVASDVSVS', 'collection': 'ASDFGVASDV', 'theme': 'ACA', 'design': 'aD'}]],
		#  'type': 'dANGLERS', 'taxon': 'EAARINGS'}
		forecasted_attribute_array = []
		temp_vals = {}
		temp_vals.update({'price_band': price_band, 'material': material,
			 'collection': collection, 'theme': theme, 'design': design,'forecasted_value':total_forecast_value})


		sku_level_temp_hash = {'week-1':Forecast[0],'week-2':Forecast[1],
		'week-3':Forecast[2],'week-4':Forecast[3],'sku_level_forcasting':[0,False,sku_forecast_list]}
		temp_vals.update({'sku_level_forcasting':[0,False,sku_level_temp_hash]})
		temp = [0, False, temp_vals]
		forecasted_attribute_array.append(temp)
		# vals1={}
		# vals1.update({'category_type_id':forecasted_attribute_array})

		month = datetime.datetime.now().strftime("%m")
		make_anew_hash_sku_level_breaking = {'week-1': Forecast[0],'week-2': Forecast[1],'week-3': Forecast[2],'week-4': Forecast[3],
		'sku_level_forcasting':sku_forecast_list }
		hash_prepared = {'forecasted_value': total_forecast_value, 'price_band': price_band, 'material': material,
		'collection': collection, 'theme': theme, 'design': design,
		 'sku_level_forcasting': [[0, False,make_anew_hash_sku_level_breaking]]}
		vals1 = {}
		vals1.update({'category_type_id': [[0, False, hash_prepared]]})

		ids = self.search(cr,uid,[('taxon','=',taxon),('type','=',type_val),('month','=',month)])
		if len(ids) == 0:
			vals1.update({'taxon':taxon,'type':type_val,'month':month})
			self.create(cr,uid,vals1)
		else:
			self.write(cr,uid,ids,vals1)
		cr.commit()

    # We are assuming that there is 53 week's data per year
	def calculate_forecasted_value(self,cr,uid,ids,context=None):

		obj = self.browse(cr,uid,ids,context=context)

		this_year_week_no = int(obj.this_year_week_no)
		last_year_week_no = int(obj.last_year_week_no)

		def mean(list1):
			mu = 0
			for val in list1:
				mu += val
			mu = mu/float(len(list1))
			return mu

		def standard_deviation(list1,mu):
			sigma = 0
			for val in list1:
				sigma += (val-mu)**2
			sigma = sigma/float(len(list1))
			sigma = sqrt(sigma)
			return sigma

		start_year = 2014
		current_year = datetime.datetime.now().year
		total_no_of_weeks_till_prev_year = 0
		for x in range(current_year-2014):
			total_no_of_weeks_till_prev_year += 53
		current_week_number = datetime.date(datetime.datetime.now().year,datetime.datetime.now().month,datetime.datetime.now().day).isocalendar()[1]
		total_no_of_weeks_till_today = total_no_of_weeks_till_prev_year + current_week_number
		# total_no_of_weeks_till_today = 53 + 13  # Till 28th march, 2015

		SQL="select * from get_top_taxon_type_final_out()"
		cr.execute(SQL)
		res_val = cr.fetchall()

		for pro in res_val:

			week_wise_quantity = []
			week_number_year_qty = []
			last_month_sku_qty = []
			try:
				taxon,product_type,theme,design,collection,material,price_band = pro[0].split('_')

				for week_qty in pro[1]:
					temp = week_qty.split('_')
					if float(temp[2]) != 0.0:
						if int(temp[1]) == 53:
							no_of_days_in_last_week_of_year = (datetime.date(int(temp[0]),12,31).isocalendar()[2]+1)%7
							week_number_year_qty.append([int(temp[0]),int(temp[1]),float(temp[2])*(7/float(no_of_days_in_last_week_of_year))])
						else:
							week_number_year_qty.append([int(temp[0]),int(temp[1]),float(temp[2])])

				for sku_qty in pro[2]:
					last_month_sku_qty.append(sku_qty.split('_'))
			except:
				continue

			week_year_qty = sorted(week_number_year_qty, key=lambda element:(element[1],element[0]))

			qty = []
			for w in range(len(week_year_qty)):
				qty.append(week_year_qty[w][2])


			average = mean(qty)
			deviation = standard_deviation(qty,average)

			temp = 0

			# Normalization of quantity data till now
			for iterator in range(total_no_of_weeks_till_today):
				bound = len(week_year_qty)
				if temp < bound and week_year_qty[temp][0] == iterator%53 + 1 and week_year_qty[temp][1] == 2014 + iterator//53:
					week_wise_quantity.append(week_year_qty[temp][2])
					temp += 1

				elif average <= deviation:
					if (iterator%53 + 1) == 53:
						no_of_days_in_last_week_of_year = (datetime.date((2014+iterator//53),12,31).isocalendar()[2]+1)%7
						week_wise_quantity.append((average/float(2.0))*(7/float(no_of_days_in_last_week_of_year)))
					else:
						week_wise_quantity.append(average/float(2.0))

				elif average > deviation:
					if (iterator%53 + 1) == 53:
						no_of_days_in_last_week_of_year = (datetime.date((2014+iterator//53),12,31).isocalendar()[2]+1)%7
						week_wise_quantity.append((average-deviation)*(7/float(no_of_days_in_last_week_of_year)))
					else:
						week_wise_quantity.append(average-deviation)

			forecasted_values = self.multiplicative(cr, uid, week_wise_quantity, this_year_week_no, last_year_week_no, 53, 4, taxon, product_type, theme, design, collection, material, price_band, last_month_sku_qty)