import os

if os.environ.get("ODOO_ENV") == 'production':
	home='/mnt/voylla-production/current'
	SPREE_URL = "http://app-temp-01.voylla.com/api/update_order.json"
	SPREE_TOKEN = "aded4bb18bd01df22dcd05997c6c71b6"
elif os.environ.get("ODOO_ENV") == 'staging':
	SPREE_URL = "http://beta.voylla.com/api/update_order.json"
	SPREE_TOKEN = "2318dfe156f4f90e8f6274fc876d6fb3"
	home='/mnt/odoo/'
else:
	home=os.getcwd()+'/'
	SPREE_URL = "http://localhost:3000/api/update_order.json"
	SPREE_TOKEN = "3efe11fc66f1372ad07e800daf043e44"	

applicable_bogos = ["BOGO"]
