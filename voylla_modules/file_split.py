import os
import csv

def split(filehandler, delimiter=',', row_limit=100, 
     output_path='.', keep_headers=True):

    """
    Splits a CSV file into multiple pieces.
    
    A quick bastardization of the Python CSV library.
    Arguments:
        `row_limit`: The number of rows you want in each output file. 10,000 by default.
        `output_name_template`: A %s-style template for the numbered output files.
        `output_path`: Where to stick the output files.
        `keep_headers`: Whether or not to print the headers in each output file.
    Example usage:
    
        >> from toolbox import csv_splitter;
        >> csv_splitter.split(open('/home/ben/input.csv', 'r'));
    
    """    

    filename_split=str(filehandler).split('/')
    filename=filename_split[-1]
    b=filename.split('.')
    c=b[0].split('-')
    current_piece=10
    lenght=len(b[0])
    name_change=b[0]
    d=''
    for x in name_change[0:lenght-2]:
        d=d+x
    output_name_template=str(d)+'%s'+'.csv'	
    current_part=1

    #output_name_template=str(b[0])+'_%s.csv'

    reader = csv.reader(filehandler, delimiter=delimiter)
    #current_piece = 1
    current_out_path = os.path.join(
         output_path,
         output_name_template  % current_piece
    )
    current_out_writer = csv.writer(open(current_out_path, 'w'), delimiter=delimiter)
    current_limit = row_limit
    if keep_headers:
        headers = reader.next()
        current_out_writer.writerow(headers)
    for i, row in enumerate(reader):
        if i + 1 > current_limit:
            current_piece =int(current_piece) +1
	    current_part +=1
            current_limit = row_limit * current_part
            current_out_path = os.path.join(
               output_path,
               output_name_template  % current_piece
            )
            current_out_writer = csv.writer(open(current_out_path, 'w'), delimiter=delimiter)
            if keep_headers:
                current_out_writer.writerow(headers)
        current_out_writer.writerow(row)
