import shlex
import os
import subprocess
import pdb
from psycopg2 import connect
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT
import csv

AWB_DELIVERY_TYPES = ["Prepaid", "COD", "Both"]
COURIER_PAYMENT_TYPES = ["Prepaid", "COD"]
COURIER_HEADER = ["PIN code", "City", "State", "Prepaid/COD", "Courier", "Preferred","routes"]
# AWB_HEADER = 

if os.environ.get("ODOO_ENV") == "production":
	DBNAME = 'voylla_erp_production'
	DBUSER = 'odoo'
	DBPASS = 'odoo'
	DBHOST = 'db01.voylla.com'
elif os.environ.get("ODOO_ENV") == "staging":
	DBNAME = 'odoo_production_7'
	DBUSER = 'odoo'
	DBPASS = 'odoo'
	DBHOST = 'odoo.voylla.com'
else:
	DBNAME = 'odoo'
	DBUSER = 'odoo'
	DBPASS = 'odoo'
	DBHOST = 'localhost'


def upload_response(filename, message, status=False):
	if not status:
		upload_file = open("error_" + filename, "w")
	else:
		upload_file = open("success_" + filename, "w")
	## Write message to file
	upload_file.write(message)
	upload_file.close()
	## upload file via s3cmd using subprocess
	cwd = os.getcwd()
	upload_filename = os.path.join(cwd, upload_file.name)
	cmd = 's3cmd put %s s3://voyllaerp/uploads/' %(upload_filename)
	args = shlex.split(cmd)
	p = subprocess.Popen(args)
	p.wait()


####GET THE LIST OF FILENMAES
def get_file_list():
	cmd = 's3cmd ls s3://voyllaerp/uploads/'
	args = shlex.split(cmd)
	ls_lines = subprocess.check_output(args).splitlines()
	ls_lines_copy = ls_lines[:]

	ls_lines.sort(reverse=True)

	files = [ls_line.split()[-1] for ls_line in ls_lines]
	try:
		files.remove("s3://voyllaerp/uploads/")
	except:
		pass
	if not files:
		return False
	else:
		return files



####GET THE LIST AWB AND COURIER FILES
def get_files(files):
	awb_files = []
	courier_files = []
	for f in files:
		file_name = f.split("/")[-1]
		if file_name.startswith("courier"):
			courier_files.append(f)
		elif file_name.startswith("awb"):
			awb_files.append(f)
		elif file_name.startswith("error_") or file_name.startswith("success_"):
			continue
	return awb_files, courier_files



def update_awb(awb_file, cursor):
	courier_ids = {}
	cursor.execute("select id,name from courier_list")
	courier_list = cursor.fetchall()
	for courier in courier_list:
		id = courier[0]
		name = courier[1]
		courier_ids[name] = id

	fout = open("upload_awb.csv", "w")
	cmd = 's3cmd get --force %s' %(awb_file)
	args = shlex.split(cmd)
	p = subprocess.Popen(args)
	p.wait()
	awb_file_name = awb_file.split("/")[-1]
	awb_file_obj = open(awb_file_name)
	reader = csv.DictReader(awb_file_obj)
	header = reader.fieldnames
	if "AWB Number" not in header or "Courier Name" not in header or "Type Of Delivery" not in header:
		upload_response(awb_file_name, "Invalid header. Header must contain 'AWB Number','Courier Name' and 'Type Of Delivery'")
		return False
	for row in reader:
		awb_no = row["AWB Number"]
		courier = row["Courier Name"]
		try:
			courier_id = courier_ids[courier]
		except KeyError:
			upload_response(awb_file_name, "%s not found in courier list. Please check for special characters also, if any." %(courier))
			return False
		type_of_delivery = row["Type Of Delivery"]
		if type_of_delivery not in AWB_DELIVERY_TYPES:
			upload_response(awb_file_name, "Type Of Delivery should be one of %s" %(AWB_DELIVERY_TYPES))
			return False
		fout.write(",".join([awb_no, str(courier_id), type_of_delivery]) + "\n")
	fout.close()
	cwd = os.getcwd()
	upload_filename = os.path.join(cwd, fout.name)
	try:
		cursor.execute("COPY awb_courier(awb_no,courier_name,tod) from '%s' DELIMITER ','" %(upload_filename))
		return True
	except Exception,e:
		upload_response(awb_file_name, e.message)
		return False



def update_courier(courier_file, cursor):
	states = []
	state_ids = {}
	fout = open("/mnt/voylla-production/current/voylla_modules/outbound/supporting_csv_outbound/upload_courier.csv", "w")
	cmd = 's3cmd get --force %s' %(courier_file)
	args = shlex.split(cmd)
	p = subprocess.Popen(args)
	p.wait()
	courier_file_name = courier_file.split("/")[-1]
	courier_file_obj = open(courier_file_name)
	reader = csv.DictReader(courier_file_obj)
	print courier_file_name
	for row in reader:
		state = row["State"].title().replace(" And ", " and ")
		if state not in states:
			states.append(state)
	print states
	states_sql_list = "('" + "','".join(states) + "')"
	cursor.execute("select id,name from res_country_state")
	state_results = cursor.fetchall()
	for state in state_results:
		name = state[1]
		id = state[0]
		state_ids[name] = id
	courier_file_obj.seek(0)
	print state_ids
	reader = csv.DictReader(courier_file_obj)
	header = reader.fieldnames
	validate = [x for x in COURIER_HEADER if x not in header]
	if validate:
		upload_response(courier_file_name, "Header must contain %s" %(valid_header))
		return False
	for row in reader:
		pin_code = row["PIN code"]
		city = row["City"]
		state = row["State"].title().replace(" And ", " and ").replace(" & ", " and ").replace("Gujrat", "Gujarat").replace("Uttrakhand", "Uttarakhand").replace('\xa0' , '')
		# state = row["State"].title().replace(" And ", " and ").replace(" & ", " and ")
		try:
			state_id = state_ids[state]
		except KeyError:
			if state == "0" or state == "NULL" or state == "Null":
				state_id = state_ids["Dummy"]
			elif pin_code == "999999":
				state_id = state_ids["Voylla"]
			else:
				upload_response(courier_file_name, "state %s not found" %(state))
				return False

		payment = row["Prepaid/COD"]
		if payment not in COURIER_PAYMENT_TYPES:
			upload_response(courier_file_name, "Prepaid/COD should be one of %s" %(COURIER_PAYMENT_TYPES))
			return False
		courier = row["Courier"]
		is_preferred = row["Preferred"]
		if is_preferred == "Yes":
			is_preferred = "TRUE"
		elif is_preferred == "No":
			is_preferred = "FALSE"
		else:
			upload_response(courier_file_name, "'preffered' should be one of %s" %(["Yes", "No"]))
			return False
		fout.write(",".join([pin_code, city, str(state_id), payment, courier, is_preferred,row["routes"]]) + "\n")

	cwd = os.getcwd()
	upload_filename = os.path.join(cwd, fout.name)
	upload_filename = '/tmp/upload_courier.csv'
	fout.close()
	cursor.execute("update sales_courier set to_delete=TRUE")
	try:
		cursor.execute("COPY sales_courier(pin_code, dest_city, state, type_of_delivery, courier_name, preference,routes) from '%s' DELIMITER ','" %(upload_filename))
		cursor.execute("delete from sales_courier where to_delete=TRUE")
	except Exception,e:
		upload_response(courier_file_name, e.message)
	return True


connection = connect(dbname=DBNAME, user=DBUSER, password=DBPASS, host=DBHOST)
connection.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
cursor = connection.cursor()


files = get_file_list()
awb_files, courier_files = get_files(files)
#awb_files.sort(reverse=True)
courier_files.sort(reverse=True)
#if awb_files:
#	status = update_awb(awb_files[0], cursor)
#	if status:
#		upload_response()

print courier_files
if courier_files:
	status = update_courier(courier_files[0], cursor)

connection.close()
