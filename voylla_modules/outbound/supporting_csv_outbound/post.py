import urllib2
import urllib
import json
import xml.etree.ElementTree as ET

url = "http://127.0.0.1:8069/spree_api/orders"

header = {"Content-Type":"xml", "API_KEY":"64f67f3b59101bf0a295ac5da71ededfa5bc0369d6b2cd094221976caaad43a2"}
#body = dict(order_number="R123456789", sku="ABCDE12345", location="Delhi")
#body = dict(Order_Completed_Date="2014-11-27 10:00:06 +0530", Order_Number="R388146655", SKU="PSJAI25008", Name="Gold Plated Single Chain Mangalsutra Set", Size="", Cost="350", Sale_Price="769", MRP="1060", Product_Created_at="2014-06-25 04:44:06 +0530", Product_Taxon="Jewellery", User_Email="shwethg@gmail.com", Name="shwetha gopal", Address="30/40 electronic city phase 1 hosur road 100 feet ring road ::", City="bangalore", Zip_Code="560100", State="Karnataka", Country="INDIA", Phone_="9972504022", Payment_Method="CashOnDelivery", Quantity="1", Total="769", Discounts="-769", Shipping_Cost="0", Shipping_Status="pending", Taxon="Mangalsutras", Promo_Used="New MS Bogo", Giftwrap="0", Giftwrap_Message="", Online_Payment_Discount="0", All_Adjustments="-769", CoD_charges="0", Promo_Applicable="New MS Bogo", Non_Standard_Adjustment="0", Scheduled_Shipment_Date="2014-11-27", PayU_ID="N/A", Currency="INR", Shipping_Type="standard", Three_P_Order_ID="N/A", Placed_By="shwethg@gmail.com")
data = "<csv_data>"+\
"<product>"+\
"<Order_Completed_Date>2014-11-27 10:00:06 +0530</Order_Completed_Date>"+\
"<Order_Number>R98765431</Order_Number>"+\
"<Status>Confirmed</Status>" +\
"<Notes>special instruction</Notes>"+\
"<Line_Items>"+\
	"<Line_Item>"+\
		"<EAN>8907275000006</EAN>"+\
		"<Size></Size>"+\
		"<Cost>350</Cost>"+\
		"<Sale_Price>769</Sale_Price>"+\
		"<MRP>1060</MRP>"+\
		"<Quantity>1</Quantity>"+\
	"</Line_Item>"+\
	"<Line_Item>"+\
		"<EAN>5901234123457</EAN>"+\
		"<Size></Size>"+\
		"<Cost>350</Cost>"+\
		"<Sale_Price>769</Sale_Price>"+\
		"<MRP>1060</MRP>"+\
		"<Quantity>1</Quantity>"+\
	"</Line_Item>"+\
"</Line_Items>"+\
"<Transaction>"+\
	"<Total>769</Total>"+\
	"<Discounts>-769</Discounts>"+\
	"<Shipping_Cost>0</Shipping_Cost>"+\
	"<Promo_Used>New MS Bogo</Promo_Used>"+\
	"<Online_Payment_Discount>0</Online_Payment_Discount>"+\
	"<Non_Standard_Adjustment>0</Non_Standard_Adjustment>"+\
	"<CoD_Charges>0</CoD_Charges>"+\
	"<Promo_Applicable>New MS Bogo</Promo_Applicable>"+\
"</Transaction>"+\
"<Product_Created_At>2014-06-25 04:44:06 +0530</Product_Created_At>"+\
"<Product_Taxon>Jewellery</Product_Taxon>"+\
"<Address>"+\
	"<User_Email>shwethg@gmail.com</User_Email>"+\
	"<Customer_Name>shwetha gopal</Customer_Name>"+\
	"<Street>30/40 electronic city phase 1 hosur road 100 feet ring road :: </Street>"+\
	"<City>bangalore</City>"+\
	"<Zip_Code>560100</Zip_Code>"+\
	"<State>rajastan</State>"+\
	"<Country>INDIA</Country>"+\
	"<Phone>9972504022</Phone>"+\
"</Address>"+\
"<Payment>"+\
	"<Payment_Method>CashOnDelivery</Payment_Method>"+\
	"<Payment_ID>1234554321</Payment_ID>"+\
"</Payment>"+\
"<Giftwrap>0</Giftwrap>"+\
"<Giftwrap_Message></Giftwrap_Message>"+\
"<Currency>INR</Currency>"+\
"<Shipping_Type>standard</Shipping_Type>"+\
"<ThreeP_Order_ID>N/A</ThreeP_Order_ID>"+\
"<Placed_By>shwethg@gmail.com</Placed_By>"+\
"</product>"+\
"</csv_data>"

root = ET.fromstring(data)
req = urllib2.Request(url, data, header)
try:
	response = urllib2.urlopen(req)
	print response.read()
except urllib2.HTTPError, e:
        print e.code
        print e.msg
        print e.headers
        print e.fp.read()
