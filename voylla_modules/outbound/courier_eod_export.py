from openerp.osv import osv, fields
import pdb
import base64
from datetime import datetime,timedelta
import logging
import time
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
import re
from voylla_modules.config import home
_logger = logging.getLogger(__name__)
import csv
from openerp.osv.orm import browse_record, browse_null
from openerp.tools.translate import _
import subprocess
import shlex
import os
import csv
import xlwt
import psycopg2
SELLER_NAME = 'Voylla Fashions Pvt Ltd'
SELLER_ADDRESS = 'J469-471,RIICO Industrial Area,Near Chatrala Circle,Sitapura'
SELLER_CITY = 'Jaipur'
SELLER_STATE = 'Rajasthan'
SELLER_ZIP_CODE = '302022'
WEIGHT = '0.5'
PRODUCT_NAME = 'Fashion Jewellery'
SUB_CUST_ID = '97486'
PICKUP_PHN = '9214505820'

class courier_eod_export(osv.osv):
	_name = "courier.eod.export"
	_columns = {
	'name' : fields.many2one('courier.list','Courier Name'),
	'start_date':fields.datetime('From Date Time'),
	'end_date' : fields.datetime('Till Date Time'),
	'url':fields.char('URL'),
	'file_name':fields.char('EOD File Name'),
	'eod_file_type':fields.selection([
			("courier_eod", "Couriers File"),
			("logistic", "Logistics File"),
			("3p_shipping", "3P Shipping File"),
			("shipment_upload","Shipment Upload"),
			("track_data","Track Data File"),
			],'EOD File Type'),
	}
	_defaults = {
	'eod_file_type':'courier_eod',
	}
	_order = "start_date desc"

	def export_eod(self,cr,uid,ids,context=None):
		courier_eod_obj = self.browse(cr,uid,ids)
		if not courier_eod_obj.name.name or not courier_eod_obj.start_date or not courier_eod_obj.end_date:
			raise osv.except_osv(('Warning!'), ("Please select Courier,Date-Time Interval to Generate EOD File"))
		sql="Select id from outbound_dispatch where date_time >='%s' and date_time <='%s' and courier_name = %s"%(courier_eod_obj.start_date,courier_eod_obj.end_date,courier_eod_obj.name.id)
		attachment_id = self.courier_eod_export_format(cr,uid,ids,sql,context)
		if not attachment_id:
			raise osv.except_osv(('Warning!'), ("EOD can not be Generated for given parameters"))
		
		return {
			'domain':"[('id','in',"+str(attachment_id)+")]",
			'action':'tree_view_eod_files',
			'view_type': 'form',
			'view_mode': 'tree,form',
			'res_model': 'courier.eod.export',
			'view_id': False,
			'type': 'ir.actions.act_window',
		}

	def courier_eod_export_format(self,cr,uid,ids,values,context=None):
		courier_eod_obj = self.browse(cr,uid,ids)
		out_disp_order_objs = self.pool.get('outbound.dispatch.order')
		inv_reg = self.pool.get('account.invoice')
		cr.execute(values)
		res_val = cr.fetchall()
		queried_dispatch_ids = [ r[0] for r in res_val]
		queried_ids = out_disp_order_objs.search(cr,uid,[('dispatches','in',queried_dispatch_ids)])
		queried_objs = out_disp_order_objs.browse(cr,uid,queried_ids)
		if len(queried_objs)==0:
			return 
		courier_name = courier_eod_obj.name.name
		courier_eod_name = ''
		d = []
		name = ''
		if courier_name.lower() == 'aramex':
			courier_eod_name = 'Voylla_Data_File_Aramex_'
			header = ["Order Num","Seller Name","Product Name","wt","Quantity","Courier Name","AWB Details","Shipping Name","Shipping Address","Shipping City","Shipping State","Shipping Zip Code","Shipping Mobile Number","Invoice Value","COD/Prepaid","COD Amount","Shipping Country","a/c#"]
			d.append(header)
			for val in queried_objs:
				a=['']*18
				a[0] = val.order_id.order_id
				a[1] = SELLER_NAME
				a[2] = PRODUCT_NAME
				a[3] = WEIGHT
				a[4] = str(val.quantity)
				a[5] = str(val.order_id.courier_name)
				a[6] = str(val.order_id.awb)
				a[7] = (val.order_id.partner_shipping_id.name).encode('utf8')
				a[8] = ((',').join((',').join(val.order_id.partner_shipping_id.contact_address.split('\r')).split('\n'))).encode('utf8')
				if val.order_id.partner_shipping_id.city:
					a[9] = val.order_id.partner_shipping_id.city.encode('utf8')
				a[10] = str(val.order_id.partner_shipping_id.state_id.name)
				a[11] = str(val.order_id.partner_shipping_id.zip)
				if val.order_id.partner_shipping_id.phone:
					a[12] = val.order_id.partner_shipping_id.phone.encode('utf8')
				inv_obj = inv_reg.browse(cr,uid,inv_reg.search(cr,uid,[('number','=',val.invoice_no)]))
				a[13] = str(inv_obj.amount_total)
				if val.tod == 'cashondelivery':
					a[14] = 'COD'
				elif val.tod == 'prepaid':
					a[14] = 'Prepaid'
				a[15] = str(val.amount_collectable)
				a[16] = str(val.order_id.partner_shipping_id.country_id.name)
				a[17] = (val.area_customer)
				d.append(a)
		elif courier_name.lower() == 'blue dart':
			name1 = ''
			name2 = ''
			courier_eod_name = '_Voylla_BlueDart_DataFile_Jaipur_'
			header = ["Airwaybill","Type","Reference Number","Sender/Store Name","attention","address1","address2","address3","pincode","tel number","mobile number","Prod/SKU code","contents","weight","Declared Value","Collectable Value","Vendor Code","Shipper Name","Return Address1","Return Address2","Return Address3","Return Pin","Length(Cms)","Bredth(Cms)","Height(Cms)","Pieces","Area_customer_code","Handover Date","Handover Time"]
			d_cod = []
			d_prep = []
			d_cod.append(header)
			d_prep.append(header)
			for val in queried_objs:
				
				a=['']*29
				a[0] = str(val.order_id.awb)
				a[1] = 'NONCOD'
				if val.tod == 'cashondelivery':
					a[1] = 'COD' 
				a[2] = str(val.order_id.order_id)
				a[3] = str(val.seller_name)
				a[4] = (val.order_id.partner_shipping_id.name).encode('utf8')
				a[5] = ((',').join((',').join(val.order_id.partner_shipping_id.contact_address.split('\r')).split('\n'))).encode('utf8')
				if val.order_id.partner_shipping_id.city:
					a[6] = val.order_id.partner_shipping_id.city.encode('utf8')
				a[7] = str(val.order_id.partner_shipping_id.state_id.name)
				a[8] = str(val.order_id.partner_shipping_id.zip)
				a[9] = val.order_id.partner_shipping_id.mobile
				if val.order_id.partner_shipping_id.phone:
					if val.order_id.partner_shipping_id.mobile == False:
						a[9] = val.order_id.partner_shipping_id.phone.encode('utf8')
					a[10] = val.order_id.partner_shipping_id.phone.encode('utf8')
				a[11] = str(val.product_id.name)
				a[12] = str(val.contents)
				a[13] = WEIGHT
				a[14] = str(val.order_id.amount_total)
				a[15] = str(val.amount_collectable)
				a[16] = (val.vendor_code)
				a[17] = SELLER_NAME
				a[18] = SELLER_ADDRESS
				a[19] = SELLER_CITY
				a[20] = SELLER_STATE
				a[21] = SELLER_ZIP_CODE
				a[22] = str(val.length)
				a[23] = str(val.breadth)
				a[24] = str(val.height)
				a[25] = str(val.quantity)
				a[26] = (val.area_customer)
				a[27] = (val.handover_date)
				a[28] = (val.handover_time)
				if a[1].lower() == 'cod':
					d_cod.append(a)
					name1 = datetime.now().strftime('%Y%d%m')+courier_eod_name+'COD_1'+'.xls'
				else:
					d_prep.append(a)
					name2 = datetime.now().strftime('%Y%d%m')+courier_eod_name+'NONCOD_1'+'.xls'
			k1=home+'/import/'+name1
			k2=home+'/import/'+name2
			attachment_ids = []
			with open(k1, "wb") as f:
				writer = csv.writer(f)
				writer.writerows(d_cod)
				f.close()
				c1="s3cmd put "+ k1 +" s3://voyllaerp/erp_tag/"
				args = shlex.split(c1)
				p=subprocess.Popen(args)
				p.wait()
				c2="s3cmd setacl s3://voyllaerp/erp_tag/"+name1+" --acl-public"
				args = shlex.split(c2)
				subprocess.Popen(args)
				p.wait()
				url_base='https://s3.amazonaws.com/voyllaerp/erp_tag/'+name1
				self.write(cr,uid,ids,{'url':url_base,'file_name':name1})
				attachment_ids.append(ids[0])
			with open(k2, "wb") as f:
				writer = csv.writer(f)
				writer.writerows(d_prep)
				f.close()
				c1="s3cmd put "+ k2 +" s3://voyllaerp/erp_tag/"
				args = shlex.split(c1)
				p=subprocess.Popen(args)
				p.wait()
				c2="s3cmd setacl s3://voyllaerp/erp_tag/"+name2+" --acl-public"
				args = shlex.split(c2)
				subprocess.Popen(args)
				p.wait()
				url_base='https://s3.amazonaws.com/voyllaerp/erp_tag/'+name2
				vals={'name':courier_eod_obj.name.id,'file_name':name2,'start_date':courier_eod_obj.start_date,'end_date':courier_eod_obj.end_date,'url':url_base}
				attachment_id=int(self.create(cr,uid,vals))
				attachment_ids.append(attachment_id)
			os.remove(k1)
			os.remove(k2)
			return attachment_ids
				
		elif courier_name.lower() == 'delhivery':
			courier_eod_name = 'Voylla_Data_File_Delhivery_'
			header = ["waybill","order no","Consignee Name","city","state","Address","Pincode","phone","mobile","Weight","payment mode","package amount","COD Amount","product to be shipped","shipping client","Length","Breadth","Height","Country","Shipment Date","Seller Name","Seller Address","Seller CST Number","Seller TIN","Invoice Number","Invoice Date"]
			d.append(header)
			for val in queried_objs:
				a=['']*26
				a[0] = str(val.order_id.awb)
				a[1] = str(val.order_id.order_id)
				a[2] = (val.order_id.partner_shipping_id.name).encode('utf8')
				if val.order_id.partner_shipping_id.city:
					a[3] = val.order_id.partner_shipping_id.city.encode('utf8')
				a[4] = str(val.order_id.partner_shipping_id.state_id.name)
				a[5] = ((',').join((',').join(val.order_id.partner_shipping_id.contact_address.split('\r')).split('\n'))).encode('utf8')
				a[6] = str(val.order_id.partner_shipping_id.zip)
				a[7] = val.order_id.partner_shipping_id.mobile
				if  val.order_id.partner_shipping_id.phone:
					if val.order_id.partner_shipping_id.mobile == False:
						a[7] = val.order_id.partner_shipping_id.phone.encode('utf8')
					a[8] = val.order_id.partner_shipping_id.phone.encode('utf8')
				a[9] = WEIGHT
				if val.tod == 'cashondelivery':
					a[10] = 'COD'
				elif val.tod == 'prepaid':
					a[10] = 'Prepaid'
				a[11] = str(val.order_id.amount_total)
				a[12] = str(val.amount_collectable)
				a[13] = PRODUCT_NAME
				a[14] = str(val.vendor_code)
				a[15] = str(val.length)
				a[16] = str(val.breadth)
				a[17] = str(val.height)
				a[18] = str(val.order_id.partner_shipping_id.country_id.name).upper()
				a[19] = str(val.order_id.dispatched_date).split(' ')[0]
				a[20] =  SELLER_NAME
				a[21] = SELLER_ADDRESS
				a[22] = str(val.seller_cst)
				a[23] = str(val.seller_cst)
				a[24] = (val.invoice_no).encode('utf8')
				inv_obj = inv_reg.browse(cr,uid,inv_reg.search(cr,uid,[('number','=',val.invoice_no)]))
				a[25] = str(inv_obj.date_invoice)
				d.append(a)

		elif courier_name.lower() == 'ecom express':
			courier_eod_name = 'Voylla_Data_File_EcomExpress_'
			header = ['Air Waybill number','Order Number','Product','Shipper','Consignee','Consignee Address1','Consignee Address2','Destination City','Pincode','State','Mobile','Telephone','Item Description','Pieces','Collectable Value','Declared value','Actual Weight(g)','Volumetric Weight(g)','Length(cms)','Breadth(cms)','Height(cms)','Sub Customer ID','Pickup name','Pickup Address','Pickup Phone','Pickup Pincode','Return name','Return Address','Return Phone','Return Pincode']
			d.append(header)
			for val in queried_objs:
				a=['']*30
				a[0] = str(val.order_id.awb)
				a[1] = str(val.order_id.order_id)
				if val.tod == 'cashondelivery':
					a[2] = 'COD'
				elif val.tod == 'prepaid':
					a[2] = 'PPD'
				a[3] = SELLER_NAME
				a[4] = (val.order_id.partner_shipping_id.name).encode('utf8')
				a[5] = ((',').join((',').join(val.order_id.partner_shipping_id.contact_address.split('\r')).split('\n'))).encode('utf8')
				a[6] = ''
				if val.order_id.partner_shipping_id.city:
					a[7] = val.order_id.partner_shipping_id.city.encode('utf8')
				a[8] = str(val.order_id.partner_shipping_id.zip) 
				a[9] = str(val.order_id.partner_shipping_id.state_id.name)	
				a[10] = (val.order_id.partner_shipping_id.mobile)
				if val.order_id.partner_shipping_id.phone:
					if val.order_id.partner_shipping_id.mobile == False:
						a[10] = (val.order_id.partner_shipping_id.phone).encode('utf8')
					a[11] = (val.order_id.partner_shipping_id.phone).encode('utf8')
				a[12] = val.contents
				a[13] = val.quantity
				a[14] = str(val.amount_collectable)
				a[15] = str(val.order_id.amount_total)
				a[16] = WEIGHT
				a[17] = ''
				a[18] = str(val.length)
				a[19] = str(val.breadth)
				a[20] = str(val.height)
				a[21] = SUB_CUST_ID
				a[22] = SELLER_NAME
				a[23] = SELLER_ADDRESS
				a[24] = PICKUP_PHN
				a[25] = SELLER_ZIP_CODE
				a[26] = SELLER_NAME
				a[27] = SELLER_ADDRESS
				a[28] = PICKUP_PHN
				a[29] = SELLER_ZIP_CODE
				d.append(a)
		name = courier_eod_name+datetime.now().strftime('%Y%d%m')+'.csv'
		k1=home+'/import/'+name
		with open(k1, "wb") as f:
		   writer = csv.writer(f)
		   writer.writerows(d)
		   f.close()
		c1="s3cmd put "+ k1 +" s3://voyllaerp/erp_tag/"
		args = shlex.split(c1)
		p=subprocess.Popen(args)
		p.wait()
		c2="s3cmd setacl s3://voyllaerp/erp_tag/"+name+" --acl-public"
		args = shlex.split(c2)
		subprocess.Popen(args)
		p.wait()
		url_base='https://s3.amazonaws.com/voyllaerp/erp_tag/'+name
		self.write(cr,uid,ids,{'url':url_base,'file_name':name})
  		os.remove(k1)
  		return ids

  	def view_generated_eod(self,cr,uid,ids,context=None):
		cr.execute("delete from courier_eod_export where id=%i"%ids[0])		
		return {
			'action':'tree_view_eod_files',
			'view_type': 'form',
			'view_mode': 'tree,form',
			'res_model': 'courier.eod.export',
			'view_id': False,
			'type': 'ir.actions.act_window',
		}

	def export_logistic_file(self,cr,uid,ids,context=None):
		out_disp_order_objs = self.pool.get('outbound.dispatch.order')
		inv_reg = self.pool.get('account.invoice')
		courier_eod_obj = self.browse(cr,uid,ids)
		if not courier_eod_obj.start_date or not courier_eod_obj.end_date:
			raise osv.except_osv(('Warning!'), ("Please select Date-Time Interval to Generate Logistic File"))
		sql="Select id from outbound_dispatch where date_time >='%s' and date_time <='%s'"%(courier_eod_obj.start_date,courier_eod_obj.end_date)
		courier_eod_obj = self.browse(cr,uid,ids)
		cr.execute(sql)
		res_val = cr.fetchall()
		queried_dispatch_ids = [ r[0] for r in res_val]
		selected_dispatched_ids = out_disp_order_objs.search(cr,uid,[('dispatches','in',queried_dispatch_ids)])
		selected_dispatched_objs = out_disp_order_objs.browse(cr,uid,selected_dispatched_ids)
		if len(selected_dispatched_objs)==0:
			raise osv.except_osv(('Warning!'), ("No record to Export"))
		queried_objs = []
		for val in selected_dispatched_objs:
			if val.order_id.shipping_type == 'standard':
				queried_objs.append(val)
		if len(queried_objs)==0:
			raise osv.except_osv(('Warning!'), ("No record found to Export"))
		d = []
		header = ["Shipment Date","AWB No","Order no","Type","Consignee Name","Address","city","state","Pincode","Tel Number","Declared Value","COD Amount","Order Date","Order Channel","3P Order Reference No","Courier"]
		d.append(header)
		for val in queried_objs:
			a=['']*16
			a[0] = datetime.strptime(val.handover_date, "%Y-%m-%d").strftime('%m/%d/%Y')
			a[1] = str(val.order_id.awb)
			a[2] = str(val.order_id.order_id)
			a[3] = str(val.tod)
			if val.tod == 'cashondelivery':
				a[3] = 'COD'
			a[4] = (val.order_id.partner_shipping_id.name)
			a[5] = ((',').join((',').join(val.order_id.partner_shipping_id.contact_address.split('\r')).split('\n'))).encode('utf8')
			if val.order_id.partner_shipping_id.city:
				a[6] = val.order_id.partner_shipping_id.city.encode('utf8')
			a[7] = str(val.order_id.partner_shipping_id.state_id.name)
			a[8] = str(val.order_id.partner_shipping_id.zip)
			if val.order_id.partner_shipping_id.phone:
				a[9] = val.order_id.partner_shipping_id.phone.encode('utf8')
			a[10] = str(val.order_id.amount_total)
			a[11] = str(val.amount_collectable)
			a[12] = datetime.strptime(val.order_id.date_order, "%Y-%m-%d %H:%M:%S").strftime('%m/%d/%Y %H:%M')
			a[13] = str(val.order_id.three_p_channel)
			a[14] = str(val.order_id.order_id)
			a[15] = str(val.order_id.courier_name)
			d.append(a)
		
		name = 'Shipment_Dump_Couriers_'+datetime.now().strftime('%Y-%m-%d')+'.xls'
		k1=home+'/import/'+name
		with open(k1, "wb") as f:
		   writer = csv.writer(f)
		   writer.writerows(d)
		   f.close()
		c1="s3cmd put "+ k1 +" s3://voyllaerp/erp_tag/"
		args = shlex.split(c1)
		p=subprocess.Popen(args)
		p.wait()
		c2="s3cmd setacl s3://voyllaerp/erp_tag/"+name+" --acl-public"
		args = shlex.split(c2)
		subprocess.Popen(args)
		p.wait()
		url_base='https://s3.amazonaws.com/voyllaerp/erp_tag/'+name
		self.write(cr,uid,ids,{'url':url_base,'file_name':name,'eod_file_type':'logistic'})
		os.remove(k1)
		return {
			'domain':"[('id','in',"+str(ids)+")]",
			'action':'tree_view_eod_files',
			'view_type': 'form',
			'view_mode': 'tree,form',
			'res_model': 'courier.eod.export',
			'view_id': False,
			'type': 'ir.actions.act_window',
		}

	def export_3p_details(self,cr,uid,ids,context=None):
		out_disp_order_objs = self.pool.get('outbound.dispatch.order')
		inv_reg = self.pool.get('account.invoice')
		courier_eod_obj = self.browse(cr,uid,ids)
		if not courier_eod_obj.start_date or not courier_eod_obj.end_date:
			raise osv.except_osv(('Warning!'), ("Please select Date-Time Interval to Generate Logistic File"))
		sql="Select id from outbound_dispatch where date_time >='%s' and date_time <='%s'"%(courier_eod_obj.start_date,courier_eod_obj.end_date)
		courier_eod_obj = self.browse(cr,uid,ids)
		cr.execute(sql)
		res_val = cr.fetchall()
		queried_dispatch_ids = [ r[0] for r in res_val]
		selected_dispatched_ids = out_disp_order_objs.search(cr,uid,[('dispatches','in',queried_dispatch_ids)])
		selected_dispatched_objs = out_disp_order_objs.browse(cr,uid,selected_dispatched_ids)
		if len(selected_dispatched_objs)==0:
			raise osv.except_osv(('Warning!'), ("No record to Export"))
		queried_objs = []
		for val in selected_dispatched_objs:
			if val.order_id.three_p_channel and val.order_id.three_p_channel.lower() != 'voylla website' and (val.order_id.shipping_type == 'standard' or val.order_id.courier_name in ['Fedex']) :
				queried_objs.append(val)
		if len(queried_objs)==0:
			raise osv.except_osv(('Warning!'), ("No record found to Export"))
		d = []
		header = ["Order Number","AWB","3P Channel","Assigned Courier","Invoice Number"]
		d.append(header)
		for val in queried_objs:
			a=['']*5
			a[0] = str(val.order_id.order_id)
			a[1] = str(val.order_id.awb)
			a[2] = str(val.order_id.three_p_channel)
			a[3] = str(val.order_id.courier_name)
			a[4] = (val.invoice_no).encode('utf8')
			d.append(a)
		
		name = '3P_Shipping_Order_Details_'+datetime.now().strftime('%Y-%m-%d')+'.xls'
		k1=home+'/import/'+name
		with open(k1, "wb") as f:
		   writer = csv.writer(f)
		   writer.writerows(d)
		   f.close()
		
		c1="s3cmd put "+ k1 +" s3://voyllaerp/erp_tag/"
		args = shlex.split(c1)
		p=subprocess.Popen(args)
		p.wait()
		c2="s3cmd setacl s3://voyllaerp/erp_tag/"+name+" --acl-public"
		args = shlex.split(c2)
		subprocess.Popen(args)
		p.wait()
		url_base='https://s3.amazonaws.com/voyllaerp/erp_tag/'+name
		self.write(cr,uid,ids,{'url':url_base,'file_name':name,'eod_file_type':'3p_shipping'})
		os.remove(k1)
		return {
			'domain':"[('id','in',"+str(ids)+")]",
			'action':'tree_view_eod_files',
			'view_type': 'form',
			'view_mode': 'tree,form',
			'res_model': 'courier.eod.export',
			'view_id': False,
			'type': 'ir.actions.act_window',
		}

	def export_shipment_upload_file(self,cr,uid,ids,context=None):
		out_disp_order_objs = self.pool.get('outbound.dispatch.order')
		inv_reg = self.pool.get('account.invoice')
		sale_order_reg = self.pool.get('sale.order')
		courier_eod_obj = self.browse(cr,uid,ids)
		if not courier_eod_obj.start_date or not courier_eod_obj.end_date:
			raise osv.except_osv(('Warning!'), ("Please select Date-Time Interval to Generate Logistic File"))
		sql="Select id from outbound_dispatch where date_time >='%s' and date_time <='%s'"%(courier_eod_obj.start_date,courier_eod_obj.end_date)
		courier_eod_obj = self.browse(cr,uid,ids)
		cr.execute(sql)
		res_val = cr.fetchall()
		queried_dispatch_ids = [ r[0] for r in res_val]
		selected_dispatched_ids = out_disp_order_objs.search(cr,uid,[('dispatches','in',queried_dispatch_ids)])
		selected_dispatched_objs = out_disp_order_objs.browse(cr,uid,selected_dispatched_ids)
		if len(selected_dispatched_objs)==0:
			raise osv.except_osv(('Warning!'), ("No record to Export"))
		queried_objs = []
		for val in selected_dispatched_objs:
			if val.order_id.shipping_type == 'standard' and val.order_id.three_p_channel.lower() == 'voylla website':
				queried_objs.append(val)
		if len(queried_objs)==0:
			raise osv.except_osv(('Warning!'), ("No record found to Export"))
		d = []
		header = ["Order Id","Courier Service","TrackINg Number","Date Shipped","Sku","Send Mail","Expected Delivery Date","Is Reshipment"]
		d.append(header)
		for val in queried_objs:
			a=['']*8
			a[0] = str(val.order_id.order_id)
			a[1] = str(val.order_id.courier_name) + 'Shipping'
			a[2] = str(val.order_id.awb)
			a[3] = datetime.strptime(val.handover_date, "%Y-%m-%d").strftime('%d/%m/%Y')
			if val.product_id:
				a[4] = val.product_id.name_template
			a[5] = 'No'
			expct_date = sale_order_reg.get_delivery_estimate_shipment(cr,uid,[val.order_id.id]).split(' ')
			a[6] = expct_date[len(expct_date)-1]
			a[7] = 'No'
			d.append(a)
		name = 'Shipment_Upload_File_'+datetime.now().strftime('%Y-%m-%d')+'.xls'
		k1=home+'/import/'+name
		with open(k1, "wb") as f:
		   writer = csv.writer(f)
		   writer.writerows(d)
		   f.close()
		
		c1="s3cmd put "+ k1 +" s3://voyllaerp/erp_tag/"
		args = shlex.split(c1)
		p=subprocess.Popen(args)
		p.wait()
		c2="s3cmd setacl s3://voyllaerp/erp_tag/"+name+" --acl-public"
		args = shlex.split(c2)
		subprocess.Popen(args)
		p.wait()
		url_base='https://s3.amazonaws.com/voyllaerp/erp_tag/'+name
		self.write(cr,uid,ids,{'url':url_base,'file_name':name,'eod_file_type':'shipment_upload'})
		os.remove(k1)
		return {
			'domain':"[('id','in',"+str(ids)+")]",
			'action':'tree_view_eod_files',
			'view_type': 'form',
			'view_mode': 'tree,form',
			'res_model': 'courier.eod.export',
			'view_id': False,
			'type': 'ir.actions.act_window',
		}

	def export_track_data(self,cr,uid,ids,context=None):
		courier_eod_obj = self.browse(cr,uid,ids)
		if not courier_eod_obj.start_date or not courier_eod_obj.end_date:
			raise osv.except_osv(('Warning!'), ("Please select Date-Time Interval to Generate Logistic File"))
		sql1 = "select so.courier_name,so.order_id,so.awb,ac.shipment_state,(so.date_order+interval'5 hour 30 minute'),(so.dispatched_date+interval'5 hour 30 minute'),so.mode_of_payment,rp.name,rp.city,rp.zip,rp.phone,rp.mobile,so.three_p_channel,data.status,data.status_time,data.status_loc,data.reason,ac.return_awb from sale_order so left join res_partner rp on so.partner_shipping_id=rp.id left join awb_courier ac on ac.awb_no=so.awb left join (select data1.invoice_id,max(data1.id) as data_id from tracking_details data1 group by data1.invoice_id) data2 on ac.invoice_no =data2.invoice_id left join tracking_details data on data.id=data2.data_id where so.courier_name in (select name from courier_list where to_be_tracked=True) and dispatched_date between '%s' and '%s' order by so.courier_name"%(courier_eod_obj.start_date,courier_eod_obj.end_date)
		name='Full'
		if courier_eod_obj.name:
			name=courier_eod_obj.name.name.split(' ')[0]
			sql1 = "select so.courier_name,so.order_id,so.awb,ac.shipment_state,(so.date_order+interval'5 hour 30 minute'),(so.dispatched_date+interval'5 hour 30 minute'),so.mode_of_payment,rp.name,rp.city,rp.zip,rp.phone,rp.mobile,so.three_p_channel,data.status,data.status_time,data.status_loc,data.reason,ac.return_awb from sale_order so left join res_partner rp on so.partner_shipping_id=rp.id left join awb_courier ac on ac.awb_no=so.awb left join (select data1.invoice_id,max(data1.id) as data_id from tracking_details data1 group by data1.invoice_id) data2 on ac.invoice_no =data2.invoice_id left join tracking_details data on data.id=data2.data_id where so.courier_name in (select name from courier_list where to_be_tracked=True and name='%s') and dispatched_date between '%s' and '%s' order by so.courier_name"%(courier_eod_obj.name.name,courier_eod_obj.start_date,courier_eod_obj.end_date)
		cr.execute(sql1)
		res_val = cr.fetchall()
		if not (len(res_val)):
			raise osv.except_osv(('Warning!!!!!!!'), ("No record to Export!!!!!!"))
		header = ['Courier Name','Order Number','AWB','Shipment Status','Create Date','Dispatched Date','TOD','Consignee Name','City','Pin','Phone','Mobile','Channel','Status','Status Time','Status Location','Undelivery Reason','Return AWB']
		d= []
		d.append(header)
		for val in res_val:
			a=['']*len(header)
			for i in range(0,len(val)):
				try:
					a[i] = val[i].encode('utf8')
				except:
					a[i] = val[i]
			d.append(a)
		name = name+'_Shipment_Details_File_'+datetime.now().strftime('%Y-%m-%d')+'.xls'
		k1=home+'/import/'+name
		with open(k1, "wb") as f:
		   writer = csv.writer(f)
		   writer.writerows(d)
		   f.close()
		c1="s3cmd put "+ k1 +" s3://voyllaerp/erp_tag/"
		args = shlex.split(c1)
		p=subprocess.Popen(args)
		p.wait()
		c2="s3cmd setacl s3://voyllaerp/erp_tag/"+name+" --acl-public"
		args = shlex.split(c2)
		subprocess.Popen(args)
		p.wait()
		url_base='https://s3.amazonaws.com/voyllaerp/erp_tag/'+name
		self.write(cr,uid,ids,{'url':url_base,'file_name':name,'eod_file_type':'track_data'})
		os.remove(k1)
		return {
			'domain':"[('id','in',"+str(ids)+")]",
			'action':'tree_view_eod_files',
			'view_type': 'form',
			'view_mode': 'tree,form',
			'res_model': 'courier.eod.export',
			'view_id': False,
			'type': 'ir.actions.act_window',
		}
