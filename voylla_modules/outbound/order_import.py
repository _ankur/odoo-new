from openerp.osv import osv, fields
import pdb
import base64
from datetime import datetime,timedelta
import logging
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
import re
from voylla_modules.config import home
import xlwt
import subprocess
import shlex
import os
import json
import csv

order_cancellation_reasons = {
    "reason1":"Customer Cancellation - Unconfirmed Order",
	"reason2":"Customer Cancellation - Doesn\'t want Product",
	"reason3":"Customer Cancellation - Duplicate/Multiple Orders",
	"reason4":"Customer Cancellation - Order Combination",
	"reason5":"Market Place - Out of Stock - Inventory Not updated on Market Place",
	"reason6":"Market Place - Out of Stock - Simultaneous Order",
	"reason7":"Market Place - Other technical issue",
	"reason8":"Market Place - Cancellation",
	"reason9":"Voylla Outbound - Last Item Damaged",
	"reason10":"Voylla Outbound - Product not found",
	"reason11":"Warehouse - Incorrect Qty uploaded on system",
	"test_order":"Test Order"
}

_logger = logging.getLogger(__name__)
	
class order_import(osv.osv):
	_name = 'order.import'

	_columns = {
		"name": fields.char("Name"),
		'module_file': fields.binary('Module file'),
	}

	def import_orders(self, cr, uid, ids, context=None):
		
		order_creation_registry = self.pool.get("threep.order.creation")
		remote_tasks_obj = self.pool.get('remote.tasks')
		order_obj = self.pool.get("sale.order")
		
		data = self.browse(cr, uid, ids[0] , context=context)
		csv_data = base64.decodestring(data.module_file)

		chars_to_remove = ['\r', ' ', '\n']
		chars_to_remove_1 = ['\r', '\n']
		rx = "[%s]" %(re.escape(''.join(chars_to_remove)))
		rx_1 = "[%s]" %(re.escape(''.join(chars_to_remove_1)))

		_logger.info(csv_data)
		rows = csv_data.split("\n")
		try:
			header = re.sub(rx, '', rows[0]).split(",")
			csv_order_details = rows[1:]
		except:
			pass
		order_details_dict = {}

		for order in csv_order_details:
			if order == '':
				continue

			order_details = dict(zip(header, re.sub(rx_1, '', order).split(",")))
			check = ''.join(order_details.values())
			if check == '':
				continue

			order_id = order_details["order_id"]
			if order_id in order_details_dict:
				temp = {}
				temp["sku"] = order_details["threep_sku"]
				temp["qty"] = int(order_details["quantity"])
				temp["shipping"] = int(order_details["shipping_cost"])
				try:
					temp["sale_price"] = int(order_details["sale_price"])
				except:
					pass
				a = order_details.get("order_item_id")
				if a != None:
					temp["order_item_id"] = order_details.get("order_item_id")
				order_details_dict[order_id]["line_items"].append(temp)
			else:
				temp = {}
				temp["sku"] = order_details["threep_sku"]
				temp["qty"] = int(order_details["quantity"])
				temp["shipping"] = int(order_details["shipping_cost"])
				temp["sale_price"] = (order_details["sale_price"])
				a = order_details.get("order_item_id")
				if a != None:
					temp["order_item_id"] = order_details.get("order_item_id")
				order_details["line_items"] = [temp]
				order_details_dict[order_id] = order_details

		for order_id, order_details in order_details_dict.items():
			_logger.info(order_details)
			order_line_items = order_details["line_items"]

			address_hash = {}
			address_hash["ship_name"] = order_details["name"]
			address_hash["ship_address"] = order_details["address_1"] + ", " + order_details["address_2"]
			address_hash["ship_zip_code"] = order_details["zip"]
			address_hash["country_code"] = order_details["country"]
			address_hash["state_name"] = order_details["state"]
			address_hash["ship_phone"] = order_details["phone"]
			address_hash["email"] = order_details["email"]
			address_hash["city_name"] = order_details.get("city")

			create_date = order_details["create_date"]

			try:
				create_date = datetime.strptime(create_date, "%Y-%m-%d %H:%M:%S")
				create_date = fields.datetime.context_timestamp(cr, uid, create_date, context)
			except:
				pass
			currency = order_details["currency_code"]

			shipping_type = order_details["ship_service"]

			source = order_details["source"]

			if shipping_type != "standard":
				shipping_type = "3rd_party"

			status = order_details["status"]
			if status == "Pending":
				status = "not_confirmed"
			elif status == "Unshipped":
				status = "confirmed"
			promotion = int(order_details['promotion'])
			shipping_cost = sum( i for i in [y["shipping"] for y in order_line_items])
			line_item_total = sum( i for i in [float(y["sale_price"])*y["qty"] for y in order_line_items])
			total_amount = shipping_cost + line_item_total - promotion

			payment_id = order_details["payment_id"]
			payment_method = order_details["payment_method"]
			
			check_order = order_obj.search(cr, uid, [("order_id", "=", order_id)])
			if not check_order:
				remote_tasks_obj.import_order_from_queue(cr, uid, order_id, order_line_items, address_hash, create_date, currency, shipping_type, status, source, total_amount, shipping_cost,payment_method,promotion)
				#order_creation_registry.create_orders(cr, uid, order_id, order_line_items, address_hash, create_date, currency, shipping_type, status, source, total_amount, shipping_cost, None, None, payment_method)


	def generate_order_dump(self, cr, uid, days, context=None):

		order_registry = self.pool.get("sale.order")
		line_items_registry = self.pool.get("sale.order.line")

		stamp = (datetime.now()+ timedelta(hours=5, minutes=30)).strftime('%Y-%m-%d_%H-%M')
		filenmae = "order_dump_" + stamp + ".xls"
		dump_filename = os.path.join(home, "import", filenmae)
		book = xlwt.Workbook(encoding="utf-8")
		sheet = book.add_sheet("Order Dump")

		# header = ['OrderDate', 'OrderNumber',  'SKU', 'EAN', 'Quantity', 'ProductName', 'Cost', 'SalePrice', 'MRP', 'Size',
		# 			'Taxonomy', 'PaymentMethod', 'PayUID', 'Currency', 'Total', 'ShippingCharges', 'OnlinePaymentDiscount', 'Giftwrap', 'CODCharge',
		# 			'Discounts', 'NonStandardAdjustments', 'Name', 'Email', 'Phone', 'Address', 'City', 'State', 'Country', 'Zip']
		header = ['OrderDate', 'OrderNumber', 'SKU', 'ProductName', 'Size', 'Cost', 'SalePrice', 'MRP', 'Email', 'Name', 'Address', 'City', 'Zip', 'State',
						'Country', 'Phone', 'PaymentMethod', 'Quantity', 'Total', 'Discounts', 'ShippingCharges', 'Taxonomy', 'Giftwrap', 'OnlinePaymentDiscount',
						"AllAdjustments", 'NonStandardAdjustments', 'PayUID', 'Currency', '3P Order ID', 'EAN', 'CODCharge', "Channel", "ShippingType", "OrderState", "PlacedBy", "Reason For Cancellation" , "Order Status"]

		for index,col_name in enumerate(header):
			sheet.write(0, index, col_name)

		lookback = datetime.now() - timedelta(days=days)
		check_time = lookback.strftime(DEFAULT_SERVER_DATETIME_FORMAT)
		order_ids = order_registry.search(cr, uid, [("date_order", ">=", check_time)])
		row_num = 0
		for order_id in order_ids:
			_logger.info(order_id)
			sale_order = order_registry.browse(cr, uid, order_id)
			reason = sale_order.reason
			reason = order_cancellation_reasons.get(reason,'')
			line_items = sale_order.order_line
			for item in line_items:
				row_num += 1
				row = []
				order_date = str(datetime.strptime(sale_order.create_date, "%Y-%m-%d %H:%M:%S") + timedelta(hours=5, minutes=30))
				order_number = sale_order.name
				order_id = sale_order.order_id

				##product details
				product = item.product_id
				sku = product.name_template
				try:
					sku = sku[:10]
				except:
					sku = ""
				ean = product.ean13
				quantity = item.product_uom_qty
				prod_name = item.name
				cost = item.cost_price_unit
				sale_price = item.price_unit
				discount = item.discount
				if isinstance(item.discount, float):
					sale_price = round(sale_price * (100 - discount)/100, 2)
				mrp = product.mrp
				size_attr = product.attribute_value_ids
				if size_attr:
					size = size_attr[0].name
				else:
					size = ""
				try:
					taxonomy = product.taxonomy.categ_id.display_name
				except:
					taxonomy = "NA"


				##payment details
				payments = sale_order.payments
				payment_method = []
				payuid = []
				for payment in payments:
					payment_method.append(payment.payment_method)
					payuid.append(payment.payment_reference)
				currency = sale_order.currency_id.name
				total = sale_order.amount_total


				##adjustment details
				shipping_cost = 0
				online_payment_discount = 0
				adjustment_total = 0
				giftwrap_total = 0
				cod_charges = 0
				discounts = 0
				adjustments = sale_order.adjustments
				for adjustment in adjustments:
					label = adjustment.adjusment_label
					amount = adjustment.amount
					adjustment_total += amount
					if label in ["standard_shipping", "express_shipping"]:
						shipping_cost += amount
					elif label == "online_payment_discount":
						online_payment_discount += amount
					elif label == "giftwrap":
						giftwrap_total += amount
					elif label == "cod_charges":
						cod_charges += amount
					elif label in ["coupon", "promotion", "employee_discount"]:
						discounts += amount
				non_standard_adjustment =  adjustment_total - cod_charges - shipping_cost -  online_payment_discount - discounts - giftwrap_total


				##customer details
				partner = sale_order.partner_invoice_id
				email = partner.email
				name = partner.name
				address = partner.street
				city = partner.city
				zip_code = partner.zip
				state_id = partner.state_id
				if state_id:
					state_id = state_id.name
				else:
					state_id = "NA"
				country_id = partner.country_id
				if country_id:
					country = country_id.name
				else:
					country = "NA"
				phone = partner.phone

				channel = sale_order.source_id
				if channel is not None:
					channel = channel.name
				else:
					channel = ""

				shipping_type = sale_order.shipping_type
				state = sale_order.state
				if state == "partial_return":
					state = item.state
				user = sale_order.user_id
				if not user:
					user = "NA"
				else:
					user = user.email
					if not email:
						user = "NA"
				order_status = sale_order.status

				row.append(order_date)
				row.append(order_number)
				row.append(sku)
				row.append(prod_name)
				row.append(size)
				row.append(cost)
				row.append(sale_price)
				row.append(mrp)
				row.append(email)
				row.append(name)
				row.append(address)
				row.append(city)
				row.append(zip_code)
				row.append(state_id)
				row.append(country)
				row.append(phone)
				row.append(",".join(payment_method))
				row.append(quantity)
				row.append(total)
				row.append(discounts)
				row.append(shipping_cost)
				row.append(taxonomy)
				row.append(giftwrap_total)
				row.append(online_payment_discount)
				row.append(adjustment_total)
				row.append(non_standard_adjustment)
				row.append(str(payuid))
				row.append(currency)
				row.append(order_id)
				row.append(ean)
				row.append(cod_charges)
				row.append(channel)
				row.append(shipping_type)
				row.append(state)
				row.append(user)
				row.append(reason)
				row.append(order_status)
				
				for col_num, value in enumerate(row):
					sheet.write(row_num, col_num, value)
		book.save(dump_filename)
		if os.environ.get("ODOO_ENV") == "production":
			upload_command = "s3cmd put " + dump_filename + " s3://voylladb-backups/dumps/order_dump/"
			upload_command_arguments = shlex.split(upload_command)
			upload_process = subprocess.Popen(upload_command_arguments)
			upload_process.wait()
			make_public_command = "s3cmd setacl s3://voylladb-backups/dumps/order_dump/" + filenmae + " --acl-public"
			make_public_command_arguments = shlex.split(make_public_command)
			make_public_process = subprocess.Popen(make_public_command_arguments)
			make_public_process.wait()


class tempx_bulk_order(osv.osv):
	_name='tempx.bulk.order'

	_columns={
			'sku':fields.text('sku'),
			'sale_price':fields.float('sale_price'),
			'cost_price':fields.float('cost'),
			'threep_sku':fields.text('threep_sku'),
			'product_id':fields.integer('product_id'),
			'vat':fields.integer('vat'),
			'order_number':fields.text('order_number'),
			'qty':fields.float('qty'),
			'product_tmpl_id':fields.integer('Product template id'),
			'excesise':fields.integer('Excise'),
			'tax_id':fields.integer('Tax'),
	}



class order_import_bulk(osv.osv):
	_name='order.import.bulk'
	_columns = {
		"name": fields.char("Name"),
		'module_file': fields.binary('Module file'),
	}

	def import_file_bulk_order(self,cr,uid,ids,context=None):
		data = self.browse(cr, uid, ids[0] , context=context)
		csv_data = base64.decodestring(data.module_file)
			
		chars_to_remove = ['\r', ' ', '\n']
		chars_to_remove_1 = ['\r', '\n']
		rx = "[%s]" %(re.escape(''.join(chars_to_remove)))
		rx_1 = "[%s]" %(re.escape(''.join(chars_to_remove_1)))
		# print csv_data
		rows = csv_data.split("\n")

		try:
			header = re.sub(rx, '', rows[0]).split(",")
			csv_prepaid_details = rows[1:]
		except:
			pass        
		prepaid_details = {}
		
		# with open('bulk_order.csv', 'wb') as csvfile:
		# 	spamwriter = csv.writer(csvfile, delimiter=',',
		# 					quoting=csv.QUOTE_MINIMAL)
		# 	for product in rows:
		# 		# pdb.set_trace()
		# 		if product == '':
		# 			continue
		# 		spamwriter.writerow(product.split(','))		
		# create a fix table tempx_bulk_order 
		
		sql="""delete from tempx_bulk_order"""
		cr.execute(sql)

		object_bulk=self.pool.get('tempx.bulk.order')
		_logger.info('==============bulk import start=================')
		_logger.info(csv_prepaid_details)
		_logger.info(rows)
		for prepaid in csv_prepaid_details:
			_logger.info('==============bulk import start=================')
			_logger.info(prepaid)
			if prepaid == '':
				continue
			prepaid_details = dict(zip(header, re.sub(rx_1, '', prepaid).split(",")))
			# pdb.set_trace()
			_logger.info('===================================================')
			_logger.info(prepaid_details)
			order_number=prepaid_details['order_number']
			ean=prepaid_details['sku']
			sale_price=prepaid_details['sale_price']
			# tax_id=prepaid_details['tax_id']
			cost=prepaid_details['cost_price']
			qty=prepaid_details['qty']
			tax_id=prepaid_details['tax_id']
			tax_flag=False
			if tax_id:	
				tax_flag=True
				vals={'sku':ean,'sale_price':sale_price,'qty':qty,'cost_price':cost,'order_number':order_number,'tax_id':tax_id}				
			else:	
				vals={'sku':ean,'sale_price':sale_price,'qty':qty,'cost_price':cost,'order_number':order_number}	
			#vals={'sku':ean,'sale_price':sale_price,'qty':qty,'cost_price':cost,'order_number':order_number}			
			create_id=object_bulk.create(cr,uid,vals,context)		
		self.create_bulk_order(cr,uid,order_number,tax_flag)
		cr.commit()
		  
	def create_bulk_order(self,cr,uid,order_number,tax_flag,context=None):
		# path=os.getcwd()
		# path=path+'/bulk_order.csv'
		# sql1="""CREATE TABLE tempx_bulk_order(sku text,sale_price numeric,cost_price numeric,threep_sku text,product_id integer,tax_id integer,order_number text,qty integer)"""
		# cr.execute(sql1)
		# sqla="""COPY tempx_bulk_order(sku,sale_price,cost_price,tax_id,qty,order_number) from '%s' DELIMITER ',' csv header"""%(path)
		# cr.execute(sqla)

		sql="""UPDATE tempx_bulk_order SET product_id = pp.id from (SELECT id,ean13 FROM product_product) as pp WHERE pp.ean13 = tempx_bulk_order.sku"""
		cr.execute(sql)

		sql2="""UPDATE tempx_bulk_order SET threep_sku = pp.threep_reference,product_tmpl_id=pp.product_tmpl_id from (SELECT id,threep_reference,product_tmpl_id FROM product_product) as pp WHERE pp.id = tempx_bulk_order.product_id"""
		cr.execute(sql2)
		if not tax_flag:
			cr.execute("""UPDATE tempx_bulk_order
						SET vat = pp.tax_id
						from  (
   						SELECT prod_id,tax_id FROM product_taxes_rel where tax_id not in (61,62,63)
						) as pp
						WHERE pp.prod_id = tempx_bulk_order.product_tmpl_id """)
		cr.execute("""UPDATE tempx_bulk_order
					SET excesise = pp.tax_id
					from  (
   					SELECT prod_id,tax_id FROM product_taxes_rel where tax_id in (61,62,63)
					) as pp
					WHERE pp.prod_id = tempx_bulk_order.product_tmpl_id """)

		sql3="""select id from sale_order where order_id='%s'"""%(order_number)
		cr.execute(sql3)
		order_id_line=cr.fetchall()
		cr.execute("""delete from tempx_bulk_order where threep_sku is Null""")

		sql4="""INSERT INTO sale_order_line (product_uos_qty,create_date,product_uom,order_id,price_unit,product_uom_qty,product_id,name,delay,state,cost_price_unit)
				SELECT 1,now(),1,%s,sale_price,qty,product_id,threep_sku,3,'queued',cost_price FROM tempx_bulk_order"""%(order_id_line[0][0])
		cr.execute(sql4)
		
		sql5="""insert into pending_sale_order (order_reference ) select id from sale_order_line where order_id=%s and state='queued'"""%(order_id_line[0][0])
		cr.execute(sql5)
		
		if tax_flag:	
			cr.execute("""INSERT INTO sale_order_tax (order_line_id,tax_id) SELECT id,49 from sale_order_line where product_id in (select product_id from tempx_bulk_order where tax_id = 2) and order_id = %s"""%(order_id_line[0][0]))
			cr.execute("""INSERT INTO sale_order_tax (order_line_id,tax_id) SELECT id,50 from sale_order_line where product_id in (select product_id from tempx_bulk_order where tax_id = 1) and order_id = %s"""%(order_id_line[0][0]))		
			sql8="""INSERT INTO sale_order_tax (order_line_id,tax_id) SELECT sol.id,tpi.excesise from sale_order_line as sol left join tempx_bulk_order as tpi on tpi.product_id=sol.product_id where sol.order_id =%s and tpi.excesise !=0"""%(order_id_line[0][0])
			cr.execute(sql8)

		if not tax_flag:
			sql6="""INSERT INTO sale_order_tax (order_line_id,tax_id) SELECT sol.id,tpi.vat from sale_order_line as sol left join tempx_bulk_order as tpi on tpi.product_id=sol.product_id where sol.order_id =%s and tpi.vat != 0"""%(order_id_line[0][0])
			sql7="""INSERT INTO sale_order_tax (order_line_id,tax_id) SELECT sol.id,tpi.excesise from sale_order_line as sol left join tempx_bulk_order as tpi on tpi.product_id=sol.product_id where sol.order_id =%s and tpi.excesise !=0"""%(order_id_line[0][0])
			cr.execute(sql6)
			cr.execute(sql7)

		# cr.execute('drop table tempx_bulk_order')
		# pending_object=self.pool.get('pending.sale.order')
		# assign_inventory=pending_object.get_inventory_pending_specific_id_orders(cr,uid,order_id_line[0][0])
		cr.commit()

class assign_inventory(osv.osv):
	_name='assign.inventory'

	def assignment(self,cr,uid,ids,context={'order_number'}):
		# pdb.set_trace()
		order_number=context['order_number']
		pending_object=self.pool.get('pending.sale.order')
		sql3="""select id from sale_order where order_id='%s'"""%(order_number)
		cr.execute(sql3)
		order_id_line=cr.fetchall()
		assign_inventory=pending_object.get_inventory_pending_specific_id_orders(cr,uid,order_id_line[0][0])
	
		return {
				'actions':'action_product_view',
				'view_type': 'form',
				'view_mode': 'form',
				'res_model': 'assign.inventory',
				'view_id': False,
				'type': 'ir.actions.act_window',
			}	

	_columns={
		'order_number':fields.text('Order Number'),
		} 
