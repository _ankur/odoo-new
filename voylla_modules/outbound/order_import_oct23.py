from openerp.osv import osv, fields
import pdb
import base64
from datetime import datetime,timedelta
import logging
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
import re
from voylla_modules.config import home
import xlwt
import subprocess
import shlex
import os

order_cancellation_reasons = {
    "reason1":"Customer Cancellation - Unconfirmed Order",
	"reason2":"Customer Cancellation - Doesn\'t want Product",
	"reason3":"Customer Cancellation - Duplicate/Multiple Orders",
	"reason4":"Customer Cancellation - Order Combination",
	"reason5":"Market Place - Out of Stock - Inventory Not updated on Market Place",
	"reason6":"Market Place - Out of Stock - Simultaneous Order",
	"reason7":"Market Place - Other technical issue",
	"reason8":"Market Place - Cancellation",
	"reason9":"Voylla Outbound - Last Item Damaged",
	"reason10":"Voylla Outbound - Product not found",
	"reason11":"Warehouse - Incorrect Qty uploaded on system",
	"test_order":"Test Order"
}

_logger = logging.getLogger(__name__)
	
class order_import(osv.osv):
	_name = 'order.import'

	_columns = {
		"name": fields.char("Name"),
		'module_file': fields.binary('Module file'),
	}

	def import_orders(self, cr, uid, ids, context=None):
		
		order_creation_registry = self.pool.get("threep.order.creation")
		remote_tasks_obj = self.pool.get('remote.tasks')
		order_obj = self.pool.get("sale.order")
		
		data = self.browse(cr, uid, ids[0] , context=context)
		csv_data = base64.decodestring(data.module_file)

		chars_to_remove = ['\r', ' ', '\n']
		chars_to_remove_1 = ['\r', '\n']
		rx = "[%s]" %(re.escape(''.join(chars_to_remove)))
		rx_1 = "[%s]" %(re.escape(''.join(chars_to_remove_1)))

		_logger.info(csv_data)
		rows = csv_data.split("\n")
		try:
			header = re.sub(rx, '', rows[0]).split(",")
			csv_order_details = rows[1:]
		except:
			pass
		order_details_dict = {}

		for order in csv_order_details:
			if order == '':
				continue

			order_details = dict(zip(header, re.sub(rx_1, '', order).split(",")))
			check = ''.join(order_details.values())
			if check == '':
				continue

			order_id = order_details["order_id"]
			if order_id in order_details_dict:
				temp = {}
				temp["sku"] = order_details["threep_sku"]
				temp["qty"] = int(order_details["quantity"])
				temp["shipping"] = int(order_details["shipping_cost"])
				try:
					temp["sale_price"] = int(order_details["sale_price"])
				except:
					pass
				a = order_details.get("order_item_id")
				if a != None:
					temp["order_item_id"] = order_details.get("order_item_id")
				order_details_dict[order_id]["line_items"].append(temp)
			else:
				temp = {}
				temp["sku"] = order_details["threep_sku"]
				temp["qty"] = int(order_details["quantity"])
				temp["shipping"] = int(order_details["shipping_cost"])
				temp["sale_price"] = int(order_details["sale_price"])
				a = order_details.get("order_item_id")
				if a != None:
					temp["order_item_id"] = order_details.get("order_item_id")
				order_details["line_items"] = [temp]
				order_details_dict[order_id] = order_details

		for order_id, order_details in order_details_dict.items():
			_logger.info(order_details)
			order_line_items = order_details["line_items"]

			address_hash = {}
			address_hash["ship_name"] = order_details["name"]
			address_hash["ship_address"] = order_details["address_1"] + ", " + order_details["address_2"]
			address_hash["ship_zip_code"] = order_details["zip"]
			address_hash["country_code"] = order_details["country"]
			address_hash["state_name"] = order_details["state"]
			address_hash["ship_phone"] = order_details["phone"]
			address_hash["email"] = order_details["email"]
			address_hash["city_name"] = order_details.get("city")

			create_date = order_details["create_date"]

			try:
				create_date = datetime.strptime(create_date, "%Y-%m-%d %H:%M:%S")
				create_date = fields.datetime.context_timestamp(cr, uid, create_date, context)
			except:
				pass
			currency = order_details["currency_code"]

			shipping_type = order_details["ship_service"]

			source = order_details["source"]

			if shipping_type != "standard":
				shipping_type = "3rd_party"

			status = order_details["status"]
			if status == "Pending":
				status = "not_confirmed"
			elif status == "Unshipped":
				status = "confirmed"
			promotion = int(order_details['promotion'])
			shipping_cost = sum( i for i in [y["shipping"] for y in order_line_items])
			line_item_total = sum( i for i in [y["sale_price"]*y["qty"] for y in order_line_items])
			total_amount = shipping_cost + line_item_total - promotion

			payment_id = order_details["payment_id"]
			payment_method = order_details["payment_method"]
			
			check_order = order_obj.search(cr, uid, [("order_id", "=", order_id)])
			if not check_order:
				remote_tasks_obj.import_order_from_queue(cr, uid, order_id, order_line_items, address_hash, create_date, currency, shipping_type, status, source, total_amount, shipping_cost,payment_method,promotion)
				#order_creation_registry.create_orders(cr, uid, order_id, order_line_items, address_hash, create_date, currency, shipping_type, status, source, total_amount, shipping_cost, None, None, payment_method)


	def generate_order_dump(self, cr, uid, days, context=None):

		order_registry = self.pool.get("sale.order")
		line_items_registry = self.pool.get("sale.order.line")

		stamp = (datetime.now()+ timedelta(hours=5, minutes=30)).strftime('%Y-%m-%d_%H-%M')
		filenmae = "order_dump_" + stamp + ".xls"
		dump_filename = os.path.join(home, "import", filenmae)
		book = xlwt.Workbook(encoding="utf-8")
		sheet = book.add_sheet("Order Dump")

		# header = ['OrderDate', 'OrderNumber',  'SKU', 'EAN', 'Quantity', 'ProductName', 'Cost', 'SalePrice', 'MRP', 'Size',
		# 			'Taxonomy', 'PaymentMethod', 'PayUID', 'Currency', 'Total', 'ShippingCharges', 'OnlinePaymentDiscount', 'Giftwrap', 'CODCharge',
		# 			'Discounts', 'NonStandardAdjustments', 'Name', 'Email', 'Phone', 'Address', 'City', 'State', 'Country', 'Zip']
		header = ['OrderDate', 'OrderNumber', 'SKU', 'ProductName', 'Size', 'Cost', 'SalePrice', 'MRP', 'Email', 'Name', 'Address', 'City', 'Zip', 'State',
						'Country', 'Phone', 'PaymentMethod', 'Quantity', 'Total', 'Discounts', 'ShippingCharges', 'Taxonomy', 'Giftwrap', 'OnlinePaymentDiscount',
						"AllAdjustments", 'NonStandardAdjustments', 'PayUID', 'Currency', '3P Order ID', 'EAN', 'CODCharge', "Channel", "ShippingType", "OrderState", "PlacedBy", "Reason For Cancellation" , "Order Status"]

		for index,col_name in enumerate(header):
			sheet.write(0, index, col_name)

		lookback = datetime.now() - timedelta(days=days)
		check_time = lookback.strftime(DEFAULT_SERVER_DATETIME_FORMAT)
		order_ids = order_registry.search(cr, uid, [("date_order", ">=", check_time)])
		row_num = 0
		for order_id in order_ids:
			_logger.info(order_id)
			sale_order = order_registry.browse(cr, uid, order_id)
			reason = sale_order.reason
			reason = order_cancellation_reasons.get(reason,'')
			line_items = sale_order.order_line
			for item in line_items:
				row_num += 1
				row = []
				order_date = str(datetime.strptime(sale_order.create_date, "%Y-%m-%d %H:%M:%S") + timedelta(hours=5, minutes=30))
				order_number = sale_order.name
				order_id = sale_order.order_id

				##product details
				product = item.product_id
				sku = product.name_template
				try:
					sku = sku[:10]
				except:
					sku = ""
				ean = product.ean13
				quantity = item.product_uom_qty
				prod_name = item.name
				cost = item.cost_price_unit
				sale_price = item.price_unit
				discount = item.discount
				if isinstance(item.discount, float):
					sale_price = round(sale_price * (100 - discount)/100, 2)
				mrp = product.mrp
				size_attr = product.attribute_value_ids
				if size_attr:
					size = size_attr[0].name
				else:
					size = ""
				try:
					taxonomy = product.taxonomy.categ_id.display_name
				except:
					taxonomy = "NA"


				##payment details
				payments = sale_order.payments
				payment_method = []
				payuid = []
				for payment in payments:
					payment_method.append(payment.payment_method)
					payuid.append(payment.payment_reference)
				currency = sale_order.currency_id.name
				total = sale_order.amount_total


				##adjustment details
				shipping_cost = 0
				online_payment_discount = 0
				adjustment_total = 0
				giftwrap_total = 0
				cod_charges = 0
				discounts = 0
				adjustments = sale_order.adjustments
				for adjustment in adjustments:
					label = adjustment.adjusment_label
					amount = adjustment.amount
					adjustment_total += amount
					if label in ["standard_shipping", "express_shipping"]:
						shipping_cost += amount
					elif label == "online_payment_discount":
						online_payment_discount += amount
					elif label == "giftwrap":
						giftwrap_total += amount
					elif label == "cod_charges":
						cod_charges += amount
					elif label in ["coupon", "promotion", "employee_discount"]:
						discounts += amount
				non_standard_adjustment =  adjustment_total - cod_charges - shipping_cost -  online_payment_discount - discounts - giftwrap_total


				##customer details
				partner = sale_order.partner_invoice_id
				email = partner.email
				name = partner.name
				address = partner.street
				city = partner.city
				zip_code = partner.zip
				state_id = partner.state_id
				if state_id:
					state_id = state_id.name
				else:
					state_id = "NA"
				country_id = partner.country_id
				if country_id:
					country = country_id.name
				else:
					country = "NA"
				phone = partner.phone

				channel = sale_order.source_id
				if channel is not None:
					channel = channel.name
				else:
					channel = ""

				shipping_type = sale_order.shipping_type
				state = sale_order.state
				if state == "partial_return":
					state = item.state
				user = sale_order.user_id
				if not user:
					user = "NA"
				else:
					user = user.email
					if not email:
						user = "NA"
				order_status = sale_order.status

				row.append(order_date)
				row.append(order_number)
				row.append(sku)
				row.append(prod_name)
				row.append(size)
				row.append(cost)
				row.append(sale_price)
				row.append(mrp)
				row.append(email)
				row.append(name)
				row.append(address)
				row.append(city)
				row.append(zip_code)
				row.append(state_id)
				row.append(country)
				row.append(phone)
				row.append(",".join(payment_method))
				row.append(quantity)
				row.append(total)
				row.append(discounts)
				row.append(shipping_cost)
				row.append(taxonomy)
				row.append(giftwrap_total)
				row.append(online_payment_discount)
				row.append(adjustment_total)
				row.append(non_standard_adjustment)
				row.append(str(payuid))
				row.append(currency)
				row.append(order_id)
				row.append(ean)
				row.append(cod_charges)
				row.append(channel)
				row.append(shipping_type)
				row.append(state)
				row.append(user)
				row.append(reason)
				row.append(order_status)
				
				for col_num, value in enumerate(row):
					sheet.write(row_num, col_num, value)
		book.save(dump_filename)
		if os.environ.get("ODOO_ENV") == "production":
			upload_command = "s3cmd put " + dump_filename + " s3://voylladb-backups/dumps/order_dump/"
			upload_command_arguments = shlex.split(upload_command)
			upload_process = subprocess.Popen(upload_command_arguments)
			upload_process.wait()
			make_public_command = "s3cmd setacl s3://voylladb-backups/dumps/order_dump/" + filenmae + " --acl-public"
			make_public_command_arguments = shlex.split(make_public_command)
			make_public_process = subprocess.Popen(make_public_command_arguments)
			make_public_process.wait()

