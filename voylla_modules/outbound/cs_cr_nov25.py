from openerp.osv import osv, fields
import pdb
from datetime import datetime, timedelta
import openerp.addons.decimal_precision as dp
import ast
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
import urllib2, json,urllib
from urllib2 import URLError, HTTPError
from functools import wraps
import time
import csv
import logging
from openerp.tools.translate import _
import base64
import webbrowser
from voylla_modules.config import home
import xlwt
import subprocess
import shlex
import os
from track_orders import update_bluedart_orders,update_delhivery_orders,update_all_courier_orders

_logger = logging.getLogger(__name__)
REVERSE_NOT_TRACK = ['cr_recvd']
TRACKED_COURIERS = ['Delhivery','Blue Dart','Blue Dart Priority']
MAX_AWB_COUNT_TO_TRACK = 100
LIVE_PUSH_REVERSE_PICKUP_DATA_URL = {"Delhivery":"https://track.delhivery.com/cmu/push/json/?token=a0eb70774c357ea745b19ce9fbe7c2e57956b36f"}
TEST_PUSH_REVERSE_PICKUP_DATA_URL = {"Delhivery":"https://test.delhivery.com/cmu/push/json/?token=a0eb70774c357ea745b19ce9fbe7c2e57956b36f"}
AVAILABILITY_PICKUP_AT_PINCODE_URL = {"Delhivery":"https://track.delhivery.com/c/api/pin-codes/json/?token=a0eb70774c357ea745b19ce9fbe7c2e57956b36f&filter_codes=PIN_CODES"}
MIN_AMOUNT_TO_REFUND = 0
SELLER_NAME = 'Voylla Retail Pvt Ltd'
SELLER_ADDRESS = 'E-521,RIICO Industrial Area,Near Chatrala Circle,Sitapura'
SELLER_CITY = 'Jaipur'
SELLER_STATE = 'Rajasthan'
SELLER_ZIP_CODE = '302022'
SELLER_PHONE = '7676111022'
SELLER_COUNTRY = 'India'

class returned_quant(osv.osv):
	_name = 'returned.quant'
	_columns = {
	'quant':fields.integer('Returned Quantity'),
	'solid':fields.many2one('sale.order.line','Sale Order Line')
	}

class outbound_cr(osv.osv):

	def _get_top(self, cr, uid, ids, name, arg, context=None):

		temp = {}
		for dispatch_order_obj in self.pool.get('outbound.cr').browse(cr, uid, ids):
			sale_reg = self.pool.get('sale.order')
			sale_id = sale_reg.search(cr,uid,[('order_id','=',dispatch_order_obj.order_number)])
			payment = sale_reg.get_payment_method(cr,uid,sale_id)
			temp[dispatch_order_obj.id] = payment
			return temp

	def create(self,cr,uid,values,context=None):
		x=self.search(cr,uid,[('order_number','=',values['order_number']),('return_awb','=',values['return_awb'])])
		if not x:
			x = super(outbound_cr, self).create(cr, uid, values, context=context)
		return x

	def onchange_order_no(self, cr, uid, ids, order_number,context):
		if context:
			total_product=0
			onumber=str(order_number)
			out_cr_req_reg = self.pool.get('outbound.cr.requests')
			dispatched_order_id = self.pool.get('outbound.dispatch.order').search(cr,uid,[('order_no','=',onumber)])
			time_of_dispatch = self.pool.get('outbound.dispatch.order').browse(cr,uid,dispatched_order_id).dispatches.date_time
			current_time = datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT)
			x = (datetime.strptime(current_time,'%Y-%m-%d %H:%M:%S')-datetime.strptime(time_of_dispatch,'%Y-%m-%d %H:%M:%S'))
			days = x.days
			return_awb=str(context)
			x=self.pool.get('sale.order')
			y=x.search(cr,uid,[('order_id','=',onumber),'|',('state','=','partial_return'),('state','=','done')])
			if not y:
				raise osv.except_osv(('Warning!'), ("Order Cannot be processed"))
			rto_search = self.pool.get('outbound.rto').search(cr,uid,[('return_awb','=',return_awb)])
			if rto_search:
				raise osv.except_osv(('Warning!'), ("This is RTO. Change action from CR->RTO and process"))
			order_line_obj=x.browse(cr,uid,y).order_line
			for line_order in order_line_obj:
				product_quantity=int(line_order.product_uom_qty)
				total_product=total_product+product_quantity
			courier_name=str(x.browse(cr,uid,y).courier_name)
			awb=str(x.browse(cr,uid,y).awb)
			order_channel=str(x.browse(cr,uid,y).three_p_channel)
			invoice_number=str(x.browse(cr,uid,y).invoice_ids[0].number)
			values={'return_courier':2 ,'action':'cr','return_awb':return_awb,'order_number':onumber,'total_line_items':total_product,'courier_name':courier_name,'order_channel':order_channel,'awb':awb}
			id_obj=self.create(cr,uid,values,context=None)
			cr_req_obj = out_cr_req_reg.browse(cr,uid,out_cr_req_reg.search(cr,uid,[('return_awb','=',return_awb)]))
			if cr_req_obj:
				out_cr_req_reg.write(cr,uid,cr_req_obj.id,{'reverse_shipment_status':'cr_recvd'})
			if days > 45:
				return {'warning':{'title':'CR Check', 'message':'Order is %s days old'%(days)},'value':{'total_line_items':total_product,'courier_name':courier_name,'order_channel':order_channel,'awb':awb}}
			return {'value':{'total_line_items':total_product,'courier_name':courier_name,'order_channel':order_channel,'awb':awb}}


	def on_scan_product(self,cr,uid,ids,order_number,total_line_items,return_awb,return_courier,courier_name,remarks,awb,order_channel,context):
		if context:			
			scan=str(context)
			total_line_items=str(total_line_items)
			return_courier=str(return_courier)
			courier_name=str(courier_name)
			remarks=str(remarks)
			awb=str(awb)
			order_channel=str(order_channel)
			return_awb=str(return_awb)
			onumber=str(order_number)
			cr_obj_id=self.search(cr,uid,[('order_number','=',onumber),('return_awb','=',return_awb)])
			x=self.pool.get('sale.order')
			y=x.search(cr,uid,[('order_id','=',onumber),'|',('state','=','partial_return'),('state','=','done')])
			out_inv_obj=self.pool.get('outbound.invoice')
			list1=scan.split(",")
			psku=list1[0]
			psize='0'
			length=len(list1)
			if length >= 5:
				# ean=list1[4]
				pbatch=list1[3]
				psize=list1[1]
				pmrp=list1[2]
				ean=out_inv_obj.calculate_ean(cr,uid,ids,psku,psize,context)
			elif length is 4:
				pmrp=list1[2]
				pbatch=list1[3]
				psize=list1[1]
				ean=out_inv_obj.calculate_ean(cr,uid,ids,psku,psize,context)
			elif length is 1:
				ean=out_inv_obj.calculate_ean(cr,uid,ids,psku,psize,context)
			elif length is 2:
				psize=list1[1]
				ean=out_inv_obj.calculate_ean(cr,uid,ids,psku,psize,context)

			order_line_obj=x.browse(cr,uid,y).order_line
			stock_move_obj=self.pool.get('stock.move')
			out_cr_req_reg = self.pool.get('outbound.cr.requests')
			source_location_id = self.pool.get('stock.location').search(cr,uid,[('complete_name','=','Partner Locations / Customers')])
			dest_location_id = self.pool.get('stock.location').search(cr,uid,[('complete_name','=','Voylla Locations / Inbound / Inbound / Inbound')])
			c1={'lang': 'en_US', 'tz': False, 'uid': uid, 'invoice_state': None, 'procure_method': 'make_to_stock', 'search_default_future': True}
			for obj in order_line_obj:
				if str(obj.product_id.ean13) == ean:
					if obj.state == 'returned':
						continue
					obj_ids=self.pool.get('returned.quant').search(cr,uid,[('solid','=',obj.id)])
					quant=float(obj.product_uom_qty)
					if not obj_ids:
						vals={'quant':0,'solid':obj.id}
						obj_ids=self.pool.get('returned.quant').create(cr,uid,vals,context=None)
					ret_quant=self.pool.get('returned.quant').browse(cr,uid,obj_ids).quant
					if quant==ret_quant:
						raise osv.except_osv(('Warning!'), ("All Product Quantities Already Returned"))
					else:

						ret_quant=float(self.pool.get('returned.quant').browse(cr,uid,obj_ids).quant)+1.00
						self.pool.get('returned.quant').write(cr,uid,obj_ids,{'quant':ret_quant})
						name = 'CR ' + str(datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT))
						product_uom = obj.product_uom.id
						product_id = obj.product_id.id
						vals2={'state':'assigned','product_uom':product_uom, 
							'company_id':1,'location_id':source_location_id[0],'location_dest_id':dest_location_id[0],'product_id':product_id,
							'name':name,'date_expected': datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT),
							'date':datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT),'product_uom_qty':1}
						stock_move_obj.create(cr, uid,vals2, context=c1)
						if quant==ret_quant:
							self.pool.get('sale.order.line').write(cr,uid,obj.id,{'state':'returned'})
							# obj.state='returned'
							check=True
							for obj in order_line_obj:
								if obj.state=='partial_return' or obj.state=='done':
									check=False
							if check:
								self.pool.get('sale.order').write(cr,uid,obj.order_id.id,{'state':'returned'})
							else:
								self.pool.get('sale.order').write(cr,uid,obj.order_id.id,{'state':'partial_return'})
									

						else:
							self.pool.get('sale.order.line').write(cr,uid,obj.id,{'state':'partial_returned'})																
							self.pool.get('sale.order').write(cr,uid,obj.order_id.id,{'state':'partial_return'})
							
						cr_id = out_cr_req_reg.search(cr,uid,[('return_awb','=',return_awb)])
						cr_line_reg = self.pool.get('outbound.cr.lines')
						if cr_id:
							cr_obj = out_cr_req_reg.browse(cr,uid,cr_id)
							for line in cr_obj.olid:
								if line.ean == ean:
									cr_line_reg.write(cr,uid,line.id,{'return_status':'returned'})
					vals={'pack_id':cr_obj_id[0],'ean':ean,'size':psize,'sku':psku}
					cr_id=self.pool.get('outbound.order.cr').create(cr,uid,vals,context=None)
					cr_obj=self.pool.get('outbound.cr').browse(cr,uid,cr_obj_id).olid
					return {'value':{'action':'cr','total_line_items':total_line_items,'order_number':onumber,'scan':None,'olid':cr_obj,'return_awb':return_awb,'return_courier':return_courier,'courier_name':courier_name,'remarks':remarks,'awb':awb,'order_channel':order_channel}}
			raise osv.except_osv(('Warning!'), ("Product Not found in given order Or already returned"))


	_name='outbound.cr'
	_columns={
	'awb': fields.char('AWB No.'),
	'olid':fields.one2many('outbound.order.cr','pack_id','Products'),
	'total_line_items':fields.integer('Total Items'),
	'courier_name':fields.char('Courier Name'),
	'order_number':fields.char('Order Number' , required=True),
	'top':fields.function(_get_top,string="Payment Type",type="char",store=True),
	'return_awb':fields.char('Return AWB', required=True),
	'order_channel':fields.char('Order Channel'),
	'remarks':fields.char('Remarks'),
	'scan':fields.char('Scan Product'),
	'return_courier' : fields.many2one('courier.list','Return Courier', required= True),
	'action':fields.selection([
			("cr", "CR"),
			],"Action" , required=True),
	}

class outbound_cr_requests(osv.osv):
	_name='outbound.cr.requests'
	_description='Customer Returns'
	_inherit = ['mail.thread']
	def update_reverse_shipment_state(self,cr,uid,context=None):
		remote_tasks_obj = self.pool.get('remote.tasks')
		cr_reg = self.pool.get('outbound.cr.requests')
		cour_list_reg = self.pool.get('courier.list')
		to_be_tracked_ids = cour_list_reg.search(cr,uid,[('to_be_tracked','=',True)])
		if len(to_be_tracked_ids)>0:
			to_be_tracked_objs = cour_list_reg.browse(cr,uid,to_be_tracked_ids)
			return_awb_ids = cr_reg.search(cr,uid,[('reverse_shipment_status','not in',REVERSE_NOT_TRACK),('return_courier','in',to_be_tracked_ids),('return_awb','!=','')])
			return_awb_objs = cr_reg.browse(cr,uid,return_awb_ids)
			batches = [return_awb_objs[x:x+MAX_AWB_COUNT_TO_TRACK] for x in xrange(0, len(return_awb_objs), MAX_AWB_COUNT_TO_TRACK)]
			for batch in batches:
				delhi_awbs = {}
				blue_awbs = {}
				for all_awb in batch:
					if all_awb.return_courier.name == TRACKED_COURIERS[0]:
						delhi_awbs[all_awb.return_awb] = all_awb.reverse_shipment_status
					elif all_awb.courier_name.name in [TRACKED_COURIERS[1],TRACKED_COURIERS[2]]:
						blue_awbs[all_awb.return_awb] = all_awb.reverse_shipment_status
				if blue_awbs:
					api_url_blue = ''
					for track_cour in to_be_tracked_objs:
						if track_cour.name in [TRACKED_COURIERS[1],TRACKED_COURIERS[2]]:
							api_url_blue = track_cour.api_url
							break
					_logger.info("***Bluedart AWB nos Shipment status task pushed***")
					remote_tasks_obj.update_shipment_state_bluedart(cr, uid, blue_awbs,api_url_blue,True)
				
				if delhi_awbs:
					api_url_delhi = ''
					for track_cour in to_be_tracked_objs:
						if track_cour.name == TRACKED_COURIERS[0]:
							api_url_delhi = track_cour.api_url
							break
					_logger.info("***Delhivery AWB nos Shipment status task pushed***")
					remote_tasks_obj.update_shipment_state_delhivery(cr, uid, delhi_awbs,api_url_delhi,True)
		else:
			_logger.info("***No Active Couriers to track***")
	
	def process_cr(self,cr,uid,ids,context=None):
		cr_obj = self.browse(cr,uid,ids)
		sale_reg = self.pool.get('sale.order')
		voucher_reg = self.pool.get('cr.voucher')
		if not cr_obj.new_order_id and cr_obj.return_type == 'replace':
			raise osv.except_osv(('Warning!'), ("New order is not created for Replacement,Please create new order for Replacement"))
		
		if cr_obj.return_type == 'refund':
			if cr_obj.top == 'cashondelivery':
				voucher_reg.create(cr,uid,{'previous_order_id':cr_obj.order_number,'v_amnt':float(cr_obj.amnt_return+cr_obj.amnt_compensation+cr_obj.amnt_shipping),'v_id':cr_obj.id,'v_pay_type':'bank','name':cr_obj.name,'v_status':'not_done'})
			else:
				voucher_reg.create(cr,uid,{'previous_order_id':cr_obj.order_number,'v_amnt':float(cr_obj.amnt_return+cr_obj.amnt_compensation+cr_obj.amnt_shipping),'v_id':cr_obj.id,'v_pay_type':'prepaid','name':cr_obj.name,'v_status':'not_done'})
			self.write(cr,uid,ids,{'cur_stage':'refund_init'})
		else:
			new_order_obj = sale_reg.browse(cr,uid,sale_reg.search(cr,uid,[('order_id','=',cr_obj.new_order_id)]))
			if not new_order_obj:
				raise osv.except_osv(('Warning!'), ("Please Enter valid order number for Replacement"))
			v_amnt = min(float(cr_obj.amnt_return+cr_obj.amnt_compensation+cr_obj.amnt_shipping),new_order_obj.amount_total)
			if v_amnt:
				voucher_reg.create(cr,uid,{'previous_order_id':cr_obj.order_number,'v_amnt':v_amnt,'v_id':cr_obj.id,'v_pay_type':'replace','name':cr_obj.name,'v_status':'not_done'})
			remaining_amount = abs(v_amnt - float(cr_obj.amnt_return+cr_obj.amnt_compensation+cr_obj.amnt_shipping))
			
			if remaining_amount > MIN_AMOUNT_TO_REFUND:
				if cr_obj.top == 'cashondelivery':
					voucher_reg.create(cr,uid,{'previous_order_id':cr_obj.order_number,'v_amnt':remaining_amount,'v_id':cr_obj.id,'v_pay_type':'bank','name':cr_obj.name,'v_status':'not_done'})
				else:
					voucher_reg.create(cr,uid,{'previous_order_id':cr_obj.order_number,'v_amnt':remaining_amount,'v_id':cr_obj.id,'v_pay_type':'prepaid','name':cr_obj.name,'v_status':'not_done'})
				self.write(cr,uid,ids,{'cur_stage':'refund_init'})

	def confirm_cr(self,cr,uid,ids,context=None):
		cr_obj = self.browse(cr,uid,ids)
		sale_reg = self.pool.get('sale.order')
		voucher_reg = self.pool.get('cr.voucher')
		sol_payments_reg = self.pool.get('sale.order.payment')
		sol_adjustments_reg = self.pool.get('sale.order.adjustment')
		if not cr_obj.new_order_id and cr_obj.return_type == 'replace':
			raise osv.except_osv(('Warning!'), ("New order is not created for Replacement,Please create new order for Replacement"))
		
		if cr_obj.return_type == 'refund':
			for obj in cr_obj.pay_ids:
				if obj.v_pay_type != 'replace' and not obj.transaction_id or obj.v_status != 'done':
					raise osv.except_osv(('Warning!'), ("Confirm CR after doing payment in customers accounts"))
				voucher_reg.write(cr,uid,obj.id,{'v_status':'done'})
		else:
			new_order_obj = sale_reg.browse(cr,uid,sale_reg.search(cr,uid,[('order_id','=',cr_obj.new_order_id)]))
			if not new_order_obj:
				raise osv.except_osv(('Warning!'), ("Please Enter valid order number for Replacement"))
			for obj in cr_obj.pay_ids:
				if obj.v_pay_type == 'replace':
					payment_id = sol_payments_reg.search(cr,uid,[('order_id','=',new_order_obj.id),('payment_method','=','CashOnDelivery')])
					if payment_id:
						sol_payments_reg.write(cr,uid,payment_id[0],{'payment_reference':cr_obj.name,'amount':sol_payments_reg.browse(cr,uid,payment_id).amount-obj.v_amnt})
					sol_adjustments_reg.create(cr,uid,{'order_id':new_order_obj.id,'adjustment_notes':cr_obj.name,'adjusment_label':'free_replacement','amount':-obj.v_amnt})
				elif obj.v_pay_type == 'refund' and not obj.transaction_id or obj.v_status != 'done':
					raise osv.except_osv(('Warning!'), ("Confirm CR after doing payment in customers accounts"))
				voucher_reg.write(cr,uid,obj.id,{'v_status':'done'})
			if new_order_obj.state == 'hold':
				sale_reg.on_release(cr,uid,new_order_obj.id,context=context)
		self.write(cr,uid,ids,{'cur_stage':'done'})
		
	def check_reverse_pickup_availability(self,cr,uid,ids,context=None):
		cr_obj = self.browse(cr,uid,ids)
		sale_reg = self.pool.get('sale.order')
		sale_id = sale_reg.search(cr,uid,[('order_id','=',cr_obj.order_number)])
		sale_obj = sale_reg.browse(cr,uid,sale_id)

		# if cr_obj.return_awb and cr_obj.return_courier:
		# 	raise osv.except_osv(('Warning!'), ("Courier for pickup is already assigned"))
		couriers = []
		try:
			for reverse_courier in AVAILABILITY_PICKUP_AT_PINCODE_URL.keys():
				check_url = AVAILABILITY_PICKUP_AT_PINCODE_URL[reverse_courier].replace('PIN_CODES',cr_obj.partner_return_id.zip)
				out_matrix = urllib2.urlopen(check_url)
				decoded_matrix = json.loads(out_matrix.read())
				if decoded_matrix['delivery_codes'] and decoded_matrix['delivery_codes'][0]['postal_code']['pickup'] == 'Y':
					couriers.append(reverse_courier)
	
			if not len(couriers):
				raise osv.except_osv(('Warning!'), ("Reverse Pickup is not available at this pin code"))
			
			assigned_return_awb = ''
			return_courier = ''
			for courier in couriers:
				push_rev_pickup_data_url = TEST_PUSH_REVERSE_PICKUP_DATA_URL[courier]
				if os.environ.get("WORKER_ENV") == 'production':
					push_rev_pickup_data_url = LIVE_PUSH_REVERSE_PICKUP_DATA_URL[courier]
				if courier == 'Delhivery':
					data={
							'pickup_location': { 
								"add": SELLER_ADDRESS,
								"city": SELLER_CITY,
								"country": SELLER_COUNTRY,
								"name": "Voylla",
								"phone": SELLER_PHONE,
								"pin": SELLER_ZIP_CODE,
								"state": SELLER_STATE
								} ,
							'shipments': [{
								"name": cr_obj.partner_return_id.name,
								"order" : cr_obj.order_number+'.'+datetime.today().strftime('%d%m%Y'),
								"client":"Voylla",
								"order_date": 'T'.join(sale_obj.date_order.split(' '))+"+00:00",
								"payment_mode": 'Pickup',
								"total_amount": sale_obj.amount_total,
								"cod_amount":sale_obj.get_cod_amount_to_be_collected(),
								"add": ((',').join((',').join(cr_obj.partner_return_id.contact_address.split('\r')).split('\n'))),
								"city": cr_obj.partner_return_id.city,
								"state": cr_obj.partner_return_id.state_id.name,
								"country": cr_obj.partner_return_id.country_id.name,
								"pin": cr_obj.partner_return_id.zip,
								"quantity": 1,
								"phone": cr_obj.partner_return_id.phone,
								"seller_name": SELLER_NAME,
								"seller_add": SELLER_ADDRESS,
								"seller_cst": "8792168747",
								"seller_tin": "8792168747",
								"seller_inv": "",
								"seller_inv_date": "",
									}, 
								  ] 
					}
					json_request_body = urllib.urlencode({'data':json.dumps(data),'format':'json'})
					request = urllib2.Request(push_rev_pickup_data_url, json_request_body, headers={})
					response = urllib2.urlopen(request)
					availability_matrix = json.loads(response.read())
					if 'packages' in availability_matrix.keys() and availability_matrix['packages']:
						assigned_return_awb = availability_matrix['packages'][0]['waybill']
						return_courier = courier
			if not return_courier or not assigned_return_awb : 
				raise osv.except_osv(('Warning!'), ("Reverse Courier cannot be assigned for this CR "))	
			cour_id = self.pool.get('courier.list').search(cr,uid,[('name','=',return_courier)])
			self.write(cr,uid,ids,{'cur_stage':'cour_asd','reverse_pickup_type':'courier','reverse_shipment_status':'requested','return_awb':assigned_return_awb,'return_courier':cour_id[0]}) 
		except URLError as e:
			if hasattr(e, 'reason'):
				raise osv.except_osv(('Warning!!!!!!!'), ("%s")%e.reason)	
		
	def _get_top(self, cr, uid, ids, name, arg, context=None):
		temp = {}
		for dispatch_order_obj in self.pool.get('outbound.cr.requests').browse(cr, uid, ids):
			sale_reg = self.pool.get('sale.order')
			sale_id = sale_reg.search(cr,uid,[('order_id','=',dispatch_order_obj.order_number)])
			payment = sale_reg.get_payment_method(cr,uid,sale_id)
			temp[dispatch_order_obj.id] = payment
			return temp

	def create(self,cr,uid,values,context=None):
		if values:
			sale_reg = self.pool.get('sale.order')
			total_product = 0
			sale_id = sale_reg.search(cr,uid,[('order_id','=',values['order_number'])])
			if sale_id:
				cr_id = self.search(cr,uid,[('order_number','=',values['order_number']),('name','=',values['name'])])
				if cr_id:
					return cr_id
				values['name'] = self.pool['ir.sequence'].get(cr, uid, 'outbound.cr.requests')
				sale_obj = sale_reg.browse(cr,uid,sale_id[0])
				values['courier_name'] = sale_obj.courier_name
				values['awb'] = sale_obj.awb
				x = super(outbound_cr_requests, self).create(cr, uid, values, context=context)
				return x

	def write(self, cr, uid,ids, values, context=None):
		body=''
		cr_obj = self.browse(cr,uid,ids)
		cr_line_obj = self.pool.get('outbound.cr.lines')
		if 'olid' in values.keys() and len(values['olid'][0])>1 and values['olid'][0][0] == 4:
			body = body+'Returned Line Item Added: '+cr_line_obj.browse(cr,uid,values['olid'][0][1]).sku+', '
		if 'olid' in values.keys() and len(values['olid'][0])>1 and values['olid'][0][0] == 2:
			body = body+'Returned Line Item Deleted: '+cr_line_obj.browse(cr,uid,values['olid'][0][1]).sku+', '
		if body:
			self.message_post(cr, uid, cr_obj.id, body=body, context=context)
		x = super(outbound_cr_requests, self).write(cr, uid,ids, values, context=context)
		return x

	def onchange_order_no1(self, cr, uid, ids,order_number,context=None):
		if order_number:
			total_product=0
			onumber=str(order_number)
			sale_id = self.pool.get('sale.order').search(cr,uid,[('order_id','=',onumber)])
			sale_obj = self.pool.get('sale.order').browse(cr,uid,sale_id)
			if sale_obj.state not in ['done','partial_return']:
				raise osv.except_osv(('Warning!'), ("Order Cannot be processed, As Order state is %s")%sale_obj.state)
			
			dispatched_order_id = self.pool.get('outbound.dispatch.order').search(cr,uid,[('order_no','=',onumber)])
			time_of_dispatch = self.pool.get('outbound.dispatch.order').browse(cr,uid,dispatched_order_id).dispatches.date_time
			current_time = datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT)
			x = (datetime.strptime(current_time,'%Y-%m-%d %H:%M:%S')-datetime.strptime(time_of_dispatch,'%Y-%m-%d %H:%M:%S'))
			if x.days > 45:
				return {'warning':{'title':'Check Customer Return validity!!', 'message':'Order is %s days old which is greater than 45 days old, So Return Request Cann\'t be Placed' % (x.days)}}
			
			return {'value':{'partner_return_id':sale_obj.partner_shipping_id.id,'order_disp_date':time_of_dispatch,'amnt_order':sale_obj.amount_total,'order_line':sale_obj.order_line,'adjustments':sale_obj.adjustments,'payments':sale_obj.payments,'courier_name':sale_obj.courier_name,'awb':sale_obj.awb,'create_date':datetime.now(),'user1_city':sale_obj.partner_shipping_id.city,'user1_country':sale_obj.partner_shipping_id.country_id.name,'user1_zip':sale_obj.partner_shipping_id.zip,'user1_street':sale_obj.partner_shipping_id.street,'user1_street2':sale_obj.partner_shipping_id.street2,'user1_name':sale_obj.partner_shipping_id.name,'user1_state':sale_obj.partner_shipping_id.state_id.name,'user1_phone':sale_obj.partner_shipping_id.phone,'user1_mobile':sale_obj.partner_shipping_id.mobile}}


	def onchange_sku(self,cr,uid,ids,sku=None,order_number=None,name=None,cur_stage=None,return_type=None,process_tym=None,create_date=None,context=None):
		if sku :
			if order_number and name:
				sale = self.pool.get('sale.order')
				sale_id = sale.search(cr,uid,[('order_id','=',order_number)])
				cr_obj_id=self.search(cr,uid,[('order_number','=',order_number),('name','=',name)])
				cr_obj = self.browse(cr,uid,cr_obj_id)
				out_inv_obj=self.pool.get('outbound.invoice')
				if sale_id:
					sale_obj = sale.browse(cr,uid,sale_id[0])
					all_ean = {}
					list1=sku.split(",")
					psku=list1[0]
					for sol in sale_obj.order_line:
						all_ean[sol.product_id.name] = sol.product_id.ean13
					if psku in all_ean.keys():
						psize='0'
						length=len(list1)
						if length is 5:
							pbatch=list1[3]
							psize=list1[1]
							pmrp=list1[2]
							ean=out_inv_obj.calculate_ean(cr,uid,ids,psku,psize,context)
						elif length is 4:
							pmrp=list1[2]
							pbatch=list1[3]
							psize=list1[1]
							ean=out_inv_obj.calculate_ean(cr,uid,ids,psku,psize,context)
						elif length is 1:
							ean=out_inv_obj.calculate_ean(cr,uid,ids,psku,psize,context)
						elif length is 2:
							psize=list1[1]
							ean=out_inv_obj.calculate_ean(cr,uid,ids,psku,psize,context)
						if ean == all_ean[psku]:
							out_order_cr = self.pool.get('outbound.cr.lines')
							out_cr_id = out_order_cr.search(cr,uid,[('ean','=',ean),('sku','=',psku),('pack_id','=',cr_obj_id[0]),('return_status','=','requested')])
							if out_cr_id:
								return {'value':{'olid':cr_obj.olid,'scan':None},'warning':{'title':'Customer Return', 'message':'Request for this product has already been placed'}}
							vals = {'pack_id':cr_obj_id[0],'ean':ean,'sku':psku,'size':psize,'return_qty':1,'return_reason':'other','return_status':'requested'}
							cr_id=self.pool.get('outbound.cr.lines').create(cr,uid,vals,context=None)
							
							return {'value':{'image_ids':cr_obj.olid,'name':name,'order_number':order_number,'scan':None,'olid':cr_obj.olid,'order_line':sale_obj.order_line,'amnt_order':sale_obj.amount_total,'cur_stage':cur_stage,'return_type':return_type,'process_tym':process_tym,'create_date':create_date,'user1_city':cr_obj.partner_return_id.city,'user1_country':cr_obj.partner_return_id.country_id.name,'user1_zip':cr_obj.partner_return_id.zip,'user1_street':cr_obj.partner_return_id.street,'user1_street2':cr_obj.partner_return_id.street2,'user1_name':cr_obj.partner_return_id.name,'user1_state':cr_obj.partner_return_id.state_id.name,'user1_phone':cr_obj.partner_return_id.phone,'user1_mobile':cr_obj.partner_return_id.mobile}}
					else:
						return {'value':{'scan':None},'warning':{'title':'Return is not possible', 'message':'This Product is not part of this order'}}
			else:
				raise osv.except_osv(('Warning!'), ("Please Fill Order Number and CR Number Field"))			

	def onchange_partner_return_id(self,cr,uid,ids,partner_return_id,context=None):
		if partner_return_id:
			partner_return_obj = self.pool.get('res.partner').browse(cr,uid,partner_return_id)
			return {'value':{'partner_return_id':partner_return_id,'user1_city':partner_return_obj.city,'user1_country':partner_return_obj.country_id.name,'user1_zip':partner_return_obj.zip,'user1_street':partner_return_obj.street,'user1_street2':partner_return_obj.street2,'user1_name':partner_return_obj.name,'user1_state':partner_return_obj.state_id.name,'user1_phone':partner_return_obj.phone,'user1_mobile':partner_return_obj.mobile}}
	
	def _get_functional_fields(self,cr,uid,ids,name,arg,context=None):

		order_lines = []
		adjustments = []
		payments = []
		res = {}
		for cr_obj in self.browse(cr,uid,ids):
			sale_id = self.pool.get('sale.order').search(cr,uid,[('order_id','=',cr_obj.order_number)])
			if sale_id:
				#Order lines in CR view
				sale_obj = self.pool.get('sale.order').browse(cr,uid,sale_id[0])
				for sol in sale_obj.order_line:
					values = {'product_id':sol.product_id,'name':sol.name,'product_ean':sol.product_id.ean13,'product_uom_qty':sol.product_uom_qty,'price_unit':sol.price_unit,'discount':sol.discount,'state':sol.state}	
					order_lines.append((1, sol.id, values))
				#Adjustments in CR view
				for sol in sale_obj.adjustments:
					values = {'adjustment_notes':sol.adjustment_notes,'adjusment_label':sol.adjusment_label,'amount':sol.amount}	
					adjustments.append((1, sol.id, values))
				#Payment methods in CR view
				for sol in sale_obj.payments:
					values = {'payment_method':sol.payment_method,'payment_reference':sol.payment_reference,'amount':sol.amount}	
					payments.append((1, sol.id, values))
				#Calculate Replacement Refund Amount
				replace_amnt = 0
				refund_amnt = 0
				for obj in cr_obj.pay_ids:
					if obj.v_pay_type == 'replace':
						replace_amnt = replace_amnt + obj.v_amnt
					else:
						refund_amnt = refund_amnt + obj.v_amnt
				res[cr_obj.id] = {'amnt_refund':refund_amnt,'amnt_voucher':replace_amnt,'order_line':order_lines,'adjustments':adjustments,'payments':payments,'order_disp_date':sale_obj.dispatched_date,'amnt_order':sale_obj.amount_total}
		return res

	def get_current_detail(self,cr,uid,ids,field_name=None,arg=None,context=None):
		res={}
		track_dict = {}
		obj = self.browse(cr,uid,ids)
		courier_list_reg = self.pool.get('courier.list')
		courier_name = obj.return_courier.name
		track_dict[obj.return_awb] = obj.reverse_shipment_status
		try:
			if courier_name == 'Delhivery':
				del_obj = courier_list_reg.browse(cr,uid,courier_list_reg.search(cr,uid,[('name','=',courier_name)]))
				update_delhivery_orders_obj = update_delhivery_orders()
				tracking_details = update_delhivery_orders_obj.delhivery_update(track_dict,del_obj.api_url)
			elif courier_name in ['Blue Dart','Blue Dart Priority']:
				blue_obj = courier_list_reg.browse(cr,uid,courier_list_reg.search(cr,uid,[('name','=',courier_name)]))
				update_bluedart_orders_obj = update_bluedart_orders()
				tracking_details = update_bluedart_orders_obj.bluedart_update(track_dict,blue_obj.api_url)
			else:
				courier_obj = courier_list_reg.browse(cr,uid,courier_list_reg.search(cr,uid,[('name','=',courier_name)]))
				if not courier_obj.to_be_tracked:
					raise osv.except_osv(('Warning!!!!'), ('Tracking not available for the Courier %s')%courier_name)
				update_all_courier_orders_obj = update_all_courier_orders()
				tracking_details = update_all_courier_orders_obj.all_couriers_update(track_dict,courier_name,courier_obj.api_url)	
				
		except URLError as e:
			if hasattr(e, 'reason'):
				raise osv.except_osv(('Internet Connection Issue!!!!'), ('%s')%e.reason)
		if tracking_details:
			html = "<table><tr><td> SCAN TYPE &nbsp;&nbsp;&nbsp;</td><td> LOCATION &nbsp;&nbsp;&nbsp;</td><td> TIME <br><br></td></tr>"
			for l in range(0,len(tracking_details['scan_details'])):
				html = html + "<tr><td>"+tracking_details['scan_details']['scan'+str(l)][0]+" &nbsp;&nbsp;&nbsp;</td><td>"+tracking_details['scan_details']['scan'+str(l)][1]+" &nbsp;&nbsp;&nbsp;</td><td>"+tracking_details['scan_details']['scan'+str(l)][2]+"</td> </tr>"
			html = html + "</table>"
			res[obj.id] = html
		else:
			res[obj.id] = 'No scan details available for the queried Airwaybill number'	
		return res

	def full_detail(self,cr,uid,ids,context=None):
		record = self.browse(cr, uid, ids[0], context=context)
		models_data = self.pool.get('ir.model.data')
		dummy, form_view = models_data.get_object_reference(cr, uid, 'outbound', 'view_scans_return')
		return {
			'name': 'Scan Details',
			'type': 'ir.actions.act_window',
			'view_type': 'form',
			'view_mode': 'form',
			'view_id':False,
			'views': [(form_view or False, 'form')],
			'res_model': 'outbound.cr.requests',
			'res_id':record.id,
			'target':'new',
		}

	_columns = {
		'awb': fields.char('AWB No.'),
		'olid':fields.one2many('outbound.cr.lines','pack_id','Products'),
		'courier_name':fields.char('Courier Name'),
		'order_number':fields.char('Order Number' , required=True),
		'top':fields.function(_get_top,string="Payment Type",type="char",store=True),
		'return_awb':fields.char('Return AWB',track_visibility='onchange'),
		'scan':fields.char('Scan Product'),
		'return_courier' : fields.many2one('courier.list','Return Courier'),
		'name' : fields.char('Request Number'),
		'new_order_id':fields.char('New Order Number',track_visibility='onchange'),
		'return_type':fields.selection([
				("refund", "Refund"),
				("replace", "Replacement"),
				],"Return Action" , required=True),
		'process_tym':fields.selection([
				("now", "Process Now"),
				("wen_pickup", "Process When Picked Up"),
				("wen_rcvd", "When Received at Voylla"),
				("wait_for_confirmation", "Wait for CS Confirmation"),
				("closed", "Closed"),
				],"Processing Time" ,required=True,track_visibility='onchange'),
		'cur_stage':fields.selection([
				("return_req", "Return Requested"),
				("cour_asd", "Courier Assigned"),
				("refund_init","Refund Initiated"),
				("refund_done","Refund Done"),
				("done", "Done"),
				("closed", "Closed")
				],"CR Current Stage",track_visibility='onchange'),
		'reverse_pickup_type':fields.selection([
			('self','Self'),
			('courier','Courier')
			],'Reverse PickUp Type',required=True),
		'holder_name':fields.char('Account Holder Name'),
		'bank' : fields.char('Bank Name'),
		'ifsc': fields.char('IFSC Code'),
		'account_num' : fields.char('Account Number'),
		'amnt_shipping' : fields.float('Return Shipping Charges',track_visibility='onchange'),
		'amnt_compensation' : fields.float('Compensation Amount',track_visibility='onchange'),
		'amnt_return' : fields.float('Order Returned Amount',track_visibility='onchange'),
		'reverse_shipment_status':fields.selection([
			('pending','Pending'),
			('requested','Requested'),
			('pick_up','Picked Up'),
			('lost','Lost'),
			('cancelled','Cancelled'),
			('cr_recvd','CR Received'),
			],'Reverse Shipment Status',track_visibility='onchange'),

		'amnt_order' : fields.function(_get_functional_fields, string='Order Total Amount', type='float',multi=True),
		"order_line": fields.function(_get_functional_fields,relation='sale.order.line',string= "Order Lines",type='one2many',multi=True),
		'order_disp_date' : fields.function(_get_functional_fields,string='Order Dispatch Date',type='datetime',multi=True),
		'adjustments' : fields.function(_get_functional_fields,relation='sale.order.adjustment',string='Adjustments',type='one2many',multi=True),
		'payments' : fields.function(_get_functional_fields,relation='sale.order.payment',string='Payments',type='one2many',multi=True),
		'amnt_voucher' : fields.function(_get_functional_fields, string='Voucher Value', type='float',multi=True),
		'amnt_refund' : fields.function(_get_functional_fields, string='Refund Amount', type='float',multi=True),

		'image_ids' : fields.one2many('outbound.cr.lines','pack_id','Image IDs'),
		'pay_ids' : fields.one2many('cr.voucher','v_id','Vouchers'),
		'partner_return_id': fields.many2one('res.partner', 'Customer'),
		'user1_city':fields.related('partner_return_id','city',string="Shipped City",type="char",relation="res.partner",readonly=True),
		'user1_zip':fields.related('partner_return_id','zip',string="Shipped City Zip",type="char",relation="res.partner",readonly=True),
		'user1_street':fields.related('partner_return_id','street',string="Shipped Address Street1",type="char",relation="res.partner",readonly=True),
		'user1_street2':fields.related('partner_return_id','street2',string="Shipped Address Street2",type="char",relation="res.partner",readonly=True),
		'user1_mobile':fields.related('partner_return_id','mobile',string="Shipped Customer Mobile",type="char",relation="res.partner",readonly=True),
		'user1_phone':fields.related('partner_return_id','phone',string="Shipped Customer Phone",type="char",relation="res.partner",readonly=True),
		'user1_country':fields.related('partner_return_id','country_id','name',string="Shipped Address Country",type="char",relation="res.partner",readonly=True),
		'user1_state':fields.related('partner_return_id','state_id','name',string="Shipped Address State",type="char",relation="res.partner",readonly=True),
		'user1_name':fields.related('partner_return_id','name',string="Shipped Customer Name",type="char",relation="res.partner",readonly=True),
		'scan_details' : fields.function(get_current_detail,string="All Scan Details",type='html',store=False),
		'note': fields.text("Notes")
	} 

	_defaults = {
		'cur_stage' : 'return_req',
		'return_type' : 'replace',
		'reverse_pickup_type':'self',
		'reverse_shipment_status':'pending',
		'process_tym':'wen_rcvd',
	}
	_sql_constraints = [
			('uniq_name', 'unique(name)', 'CR Number Duplication is not allowed!!!') 
			]	
	_order = "create_date desc"

class outbound_cr_lines(osv.osv):
	_name="outbound.cr.lines"
	_columns={
	'pack_id':fields.many2one('outbound.cr.requests','CR'),
	'sku':fields.char('SKU'),
	'mrp':fields.char('MRP'),
	'ean':fields.char('EAN'),
	'size':fields.char('Size'),
	'return_reason':fields.selection([
			("damaged", "Damaged Product"),
			("size_issue", "Size Issue"),
			("other", "others"),
			],"Return Reason" , required=True),
	'return_qty':fields.integer('Quantity to Return'),
	'return_status':fields.selection([
			("requested", "Requested"),
			("returned","Returned"),
			("cancelled", "Cancelled"),
			],"Return Status" , required=True),
	'image' : fields.char('Image Url')
	} 
	_defaults ={
	'return_reason':'other',
	'return_status':'requested',
	'return_qty': 1
	}	

	def write(self,cr,uid,ids,values,context=None):
		if 'image' in values.keys():
			self_obj = self.browse(cr,uid,ids)
			name = self_obj.pack_id.name+'_'+self_obj.sku+'.jpg'
			k1=home+'/import/'+name
			with open(k1, "wb") as f:
				f.write(values['image'].decode('base64'))
				f.close()

			c1="s3cmd put "+ k1 +" s3://voyllaerp/erp_tag/"
			args = shlex.split(c1)
			p=subprocess.Popen(args)
			p.wait()
			c2="s3cmd setacl s3://voyllaerp/erp_tag/"+name+" --acl-public"
			args = shlex.split(c2)
			subprocess.Popen(args)
			p.wait()
			url_base='https://s3.amazonaws.com/voyllaerp/erp_tag/'+name
			values['image']=url_base
			os.remove(k1)
		x = super(outbound_cr_lines, self).write(cr, uid,ids, values, context=context)	
		return x

	def view_image(self,cr,uid,ids,context=None):
		webbrowser.open(self.browse(cr,uid,ids).image,new=1,autoraise=True)

class outbound_order_cr(osv.osv):
	_name="outbound.order.cr"
	_columns={
	'pack_id':fields.many2one('outbound.cr','CR'),
	'sku':fields.char('SKU'),
	'mrp':fields.char('MRP'),
	'ean':fields.char('EAN'),
	'size':fields.char('Size'),
	}

class cr_voucher(osv.osv):
	_name = 'cr.voucher'

	def create(self,cr,uid,vals,context=None):
		if 'v_id' in vals.keys():
			vals['name'] = self.pool.get('outbound.cr.requests').browse(cr,uid,vals['v_id']).name
			vals['previous_order_id'] = self.pool.get('outbound.cr.requests').browse(cr,uid,vals['v_id']).order_number
		
		x = self.search(cr,uid,[('name','=',vals['name']),('v_pay_type','=',vals['v_pay_type'])])
		if not x:
			x = super(cr_voucher, self).create(cr, uid, vals, context=context) 
		return x

	_columns = {
	'v_id':fields.many2one('outbound.cr.requests','CR Vouchers'),
	'v_status':fields.selection([
		('done',"Done"),
		('not_done',"Not Done"),
		],"Voucher Status",required=True),
	'v_amnt':fields.float('Value'),
	'previous_order_id':fields.char('Previous Order Number'),
	'name':fields.char('CR Reference'),
	'v_pay_type':fields.selection([
			("prepaid", "Prepaid"),
			("bank", "Customer Bank"),
			("voylla", "Voylla"),
			("replace", "Replacement")
			],"Pay Type" , required=True),
	'transaction_id':fields.char('Transaction ID'),
	'holder_name':fields.related('v_id','holder_name',string='Account Holder Name',type="char",relation="outbound.cr.requests"),
	'bank' : fields.related('v_id','bank',string='Bank and Branch Name',type="char",relation="outbound.cr.requests"),
	'ifsc': fields.related('v_id','ifsc',string='IFSC Code',type="char",relation="outbound.cr.requests"),
	'account_num' : fields.related('v_id','account_num',string='Account Number',type="char",relation="outbound.cr.requests"),
	'new_order_id' : fields.related('v_id','new_order_id',string='New Order Number',type="char",relation="outbound.cr.requests"),

	}
	_defaults = {
	'v_status' :'not_done'
	}
	_sql_constraints = [
			('uniq_name', 'unique(name,v_pay_type)', 'One CR Number cannot have duplicate payment options!!!') 
			]

class outbound_cr_save(osv.osv_memory):
	_name = "outbound.cr.save"
	_columns = {
	'file_name':fields.char('File Name'),
	'url': fields.char('URL')
	}
	def export_refundable_crs(self,cr,uid,ids,context=None):
		# sql="Select id from outbound.cr where cur_stage=refund_init"%(courier_eod_obj.start_date,courier_eod_obj.end_date,courier_eod_obj.name.id)
		outbound_cr = self.pool.get('outbound.cr.requests')
		selected_cr_obj = outbound_cr.browse(cr,uid,context.get('active_ids'))
		if not len(selected_cr_obj):
			raise osv.except_osv(('Warning!!!!!!'), ("Select Records to Export them in the File"))
		
		for obj in selected_cr_obj:
			if obj.cur_stage != 'refund_init' or not obj.amnt_refund:
				raise osv.except_osv(('Warning!!!!!!'), ("CR %s is not in \'Refund Initiated\' stage or Amount to be Refunded is not calculated,Please check manually")%obj.name)
		
			if not obj.holder_name or not obj.account_num or not obj.bank or not obj.ifsc or not obj.name or not obj.order_number:
				raise osv.except_osv(('Warning!!!!!!'), ("Please fill Customers A\c Details and Order Number to export file to Refund"))
		
		record = []
		header = ['CR Number','Order Number','Refundabe Amount','A\c Holder Name','A\c No.','Bank-Branch','IFSC Code','Payment Mode','Transaction ID']
		record.append(header)
		for obj in selected_cr_obj:
			a = ['']*9
			for cr_line in obj.pay_ids:
				if cr_line.v_pay_type != 'replace':
					a[7] = cr_line.v_pay_type
					break
			if not a[7]:
				raise osv.except_osv(('Warning!!!!!!'), ("Some of the CRs payment method is not defined,Please check manually"))
		
			a[0] = obj.name
			a[1] = obj.order_number
			a[2] = obj.amnt_refund
			a[3] = obj.holder_name
			a[4] = obj.account_num
			a[5] = obj.bank
			a[6] = obj.ifsc
			a[8] = ''
			record.append(a)
		name = 'CR_Refund_'+datetime.now().strftime('%Y-%d-%m::%H:%M:%S')+'.csv'
		k1=home+'/import/'+name
		with open(k1, "wb") as f:
		   writer = csv.writer(f)
		   writer.writerows(record)
		   f.close()
		c1="s3cmd put "+ k1 +" s3://voyllaerp/erp_tag/"
		args = shlex.split(c1)
		p=subprocess.Popen(args)
		p.wait()
		c2="s3cmd setacl s3://voyllaerp/erp_tag/"+name+" --acl-public"
		args = shlex.split(c2)
		subprocess.Popen(args)
		p.wait()
		url_base='https://s3.amazonaws.com/voyllaerp/erp_tag/'+name
		self.write(cr,uid,ids,{'url':url_base,'file_name':name})
		os.remove(k1)
		models_data = self.pool.get('ir.model.data')
		dummy, form_view = models_data.get_object_reference(cr, uid, 'outbound', 'cr_refund_file_form_view')
		return {
			'name': 'Download CR File',
			'view_type': 'form',
			'view_mode': 'form',
			'view_id':False,
			'views': [(form_view or False, 'form')],
			'res_model': 'outbound.cr.save',
			'res_id':ids[0],
			'type': 'ir.actions.act_window',
		}

class outbound_cr_import(osv.osv_memory):
	_name='outbound.cr.import'
	_columns={
	'name' : fields.binary('CR File')
	}
	def update_refunded_crs(self,cr,uid,ids,context=None):
		cr_voucher_reg = self.pool.get('cr.voucher')
		outbound_cr_reg = self.pool.get('outbound.cr.requests')
		obj = self.browse(cr, uid, ids[0] , context=context)
		cr_data = base64.decodestring(obj.name)
		cr_records = cr_data.replace('\r','').split('\n')
		header = cr_records[0].split(",")
		for key,value in list(enumerate(header)):
			if value == 'CR Number':
				ind_name = int(key)
			if value == 'Order Number':
				ind_order_number = int(key)
			if value == 'A\c No.':
				ind_account_num = int(key)
			if value == 'Transaction ID':
				ind_transaction_id = int(key)	
			if value == 'Payment Mode':
				ind_payment_mode = int(key)		
		for record in cr_records[1:]:
			if record:
				record_list = record.split(',')
				if not record_list[ind_transaction_id]:
					raise osv.except_osv(('Warning!'), ("Row %s cannot be updated as transaction id column is empty"%(record)))
				try:
					sql1 = ("""select id from outbound_cr_requests where name = \'%s\' and order_number = \'%s\' and cur_stage = \'refund_init\'""")%(record_list[ind_name],record_list[ind_order_number])
					cr.execute(sql1)
					fetched_id = cr.fetchone()
					if not fetched_id:
						raise osv.except_osv(('Warning!'), ("Row %s cannot be updated as this record no more exists or already Refunded,Please check manually"%(record)))
					outbound_cr_obj = outbound_cr_reg.browse(cr,uid,[fetched_id[0]])
					for obj in outbound_cr_obj.pay_ids:
						if obj.v_pay_type == record_list[ind_payment_mode]:
							if obj.v_status == 'done':
								raise osv.except_osv(('Warning!'), ("Row %s is already Refunded"%(record)))
							cr_voucher_reg.write(cr,uid,obj.id,{'transaction_id':record_list[ind_transaction_id],'v_status':'done'})							
					for obj in outbound_cr_obj.pay_ids:
						if obj.v_pay_type != 'replace' and obj.v_status == 'not_done':
							raise osv.except_osv(('Warning!'), ("Refund is not done compeletly for the CR %s")%outbound_cr_obj.name)
					outbound_cr_reg.write(cr,uid,outbound_cr_obj.id,{'cur_stage':'refund_done'})
				except Exception as e:
					raise osv.except_osv(('%s')%e.message, ("Row %s cannot be updated"%(record)))
