from openerp.osv import osv, fields
import pdb
import base64
from datetime import datetime,timedelta
import logging
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
import re

_logger = logging.getLogger(__name__)

class courier_import(osv.osv):
	_name = "courier.import"
	_columns = {
	'name' : fields.binary('Courier File')
	}
	

	def import_file(self, cr, uid, ids, context=None):
		sales_courier_obj = self.pool.get('sales.courier')
		data = self.browse(cr, uid, ids[0] , context=context)
		csv_data = base64.decodestring(data.name)
		chars_to_remove = ['\r', ' ', '\n']
		chars_to_remove_1 = ['\r', '\n']
		rx = "[%s]" %(re.escape(''.join(chars_to_remove)))
		rx_1 = "[%s]" %(re.escape(''.join(chars_to_remove_1)))
		rows = csv_data.split("\n")
		headers = rows[0]
		headers = headers.split(",")
		for key,value in list(enumerate(headers)):
			if value == 'PIN code':
				index_pin_code = int(key)
			if value == 'City':
				index_city = int(key)
			if value == 'State':
				index_state = int(key)
			if value == 'Prepaid/COD':
				index_tod = int(key)
			if value == 'Courier':
				index_courier = int(key)
			if value == 'Is Preferred':
				index_preferred = int(key)
							

		error_list = []
		rows = rows[1:]
		for every_row in rows:
			
			if len(every_row) == 0:
				continue
			every_row = every_row.split(",")
			country_state_obj = self.pool.get('res.country.state')
			# state_id = country_state_obj.search(cr,uid,[('name','=',every_row[index_state])])
			vals = {'pin_code':every_row[index_pin_code],'dest_city':every_row[index_city],
				'type_of_delivery':every_row[index_tod],'courier_name':every_row[index_courier],'preference':every_row[index_preferred]}
			try:
				cr.execute("""insert into sales_courier (pin_code, dest_city, state, type_of_delivery, courier_name, preference) VALUES (%s, %s, %s, %s,%s,%s)""",(int(every_row[index_pin_code]),every_row[index_city],int(every_row[index_state]),every_row[index_tod],every_row[index_courier],bool(every_row[index_preferred])))
			except:

				raise osv.except_osv(('Warning!'), ("Row %s cannot be processed"%(every_row)))
		print error_list
			
			# sales_courier_obj.create(cr,uid,vals,context=None)
			# sql = "INSERT INTO sales_courier (pin_code, dest_city, state, type_of_delivery, courier_name, preference) VALUES (%i, %s, %s, %s,%s, %s)", \
			# %(int(every_row[index_pin_code]),every_row[index_city],every_row[index_state],every_row[index_tod],every_row[index_courier],every_row[index_preferred])
			# sql = "INSERT INTO sales_courier (pin_code, dest_city, state, type_of_delivery, courier_name, preference) VALUES (110001, 'Delhi', %s, 'COD','Aramex', 'Yes')", \
			# %(every_row[index_city],every_row[index_state],every_row[index_tod],every_row[index_courier],every_row[index_preferred])
			# pdb.set_trace()
			# cr.execute(sql)	
		

	def track_data_file(self,cr,uid,ids,context=None):
		_logger.info("Inside upload trck data file")
		data = self.browse(cr, uid, ids[0] , context=context)
		if not data.name:
			raise osv.except_osv(("File Import Error!!!!"),("Please select file to import"))
		csv_data = base64.decodestring(data.name)
		csv_data = csv_data.replace('\r','')
		rows = csv_data.split("\n")
		headers = rows[0]
		headers = headers.split(",")
		for key,value in list(enumerate(headers)):
			if value == 'Order Number':
				order_ind = int(key)
			if value == 'AWB':
				awb_ind = int(key)
			if value == 'Status':
				status_ind = int(key)
			if value == 'Status Time':
				time_ind = int(key)
			if value == 'Reason for Undelivery':
				reason_ind = int(key)
			if value == 'Status Location':
				loc_ind = int(key)
		error_list = ''
		vals = {}
		for every_row in rows[1:]:
			if not every_row:
				continue
			every_row = every_row.split(",")
			if not every_row or not every_row[order_ind] or not every_row[status_ind] or every_row[status_ind] not in ['Dispatched','Delivered','Undelivered','In Transit','RTO Initiated','LOST','RTO Delivered']:
				error_list = error_list+ 'Row %s cannot be updated,Please check row values!!!!\n'%(every_row)
				continue
			status = every_row[status_ind]
			status_time = None
			reason = None
			try:
				status_time = (datetime.strptime(every_row[time_ind],'%m/%d/%Y  %I:%M:%S %p')).strftime('%d %B %Y,%H:%M')
			except:
				error_list =error_list+'Row %s cannot be processed,Status time format should match with mm/dd/yyyy hh:mm:ss AM/PM\n'%(every_row)
				continue
			account_inv_reg = self.pool.get('account.invoice')
			tracking_reg = self.pool.get('tracking.details')
			invoice_id = account_inv_reg.search(cr,uid,[('order_id','=',(every_row[order_ind]).strip(' '))])
			if not invoice_id:
				error_list = error_list+'Row %s cannot be processed(invoice Not Found for the Order)!!!!\n'%(every_row)
				continue
			if every_row[reason_ind]:
				reason = every_row[reason_ind]
			vals[invoice_id[0]] = {'invoice_id':invoice_id[0],'status':every_row[status_ind],'status_time':status_time,'status_loc':every_row[loc_ind],'reason':reason}
		if error_list:
			raise osv.except_osv(('Errors While uploading File!!!'),(error_list))
		for val in vals:
			_logger.info(vals[val])
			cur_status = 'IT'
			previous_track_details1 = tracking_reg.search(cr,uid,[('invoice_id','=',val)])
			if not previous_track_details1:
				tracking_reg.create(cr,uid,vals[val])
				continue
			previous_track_details1_obj = [deta.status for deta in tracking_reg.browse(cr,uid,previous_track_details1)]
			if 'RTO Received' in previous_track_details1_obj or 'RTO Delivered' in previous_track_details1_obj or status == 'In Transit' or 'Delivered' in previous_track_details1_obj:
				continue
			previous_track_details = tracking_reg.search(cr,uid,[('invoice_id','=',val),('status','=',vals[val]['status']),('status_time','=',vals[val]['status_time'])])
			if not previous_track_details:
				tracking_reg.create(cr,uid,vals[val])



