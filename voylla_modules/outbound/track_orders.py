import os
import shlex
import subprocess
import string
import sys
import json
from datetime import datetime,timedelta
import requests
import urllib2
import xml.etree.ElementTree as ET
import sys
import httplib
httplib.HTTPConnection.debuglevel = 1 
from requests import session
from xml.dom.minidom import parse
import xml.dom.minidom
import pdb
BD_reason_list = ['NETWORK DELAY, WILL IMPACT DELIVERY','DELIVERY ATTEMPTED-PREMISES CLOSED','WRONG PINCODE, WILL IMPACT DELIVERY',
'CONSIGNEE\'S ADDRESS INCOMPLETE/LANDMARK NEEDED','CONSIGNEE REFUSED TO ACCEPT','CONSIGNEE NOT AVAILABLE','DELAY CAUSED BEYOND OUR CONTROL',
'OUT OF DELIVERY AREA','CONSIGNEE OUT OF STATION','DELIVERY ON NEXT BUSINESS DAY','CONSIGNEE\'S ADDRESS UNLOCATABLE/LANDMARK NEEDED',
'CONSIGNEE\'S ADDRESS INCORRECT','NO SUCH CONSIGNEE AT THE GIVEN ADDRESS','ON HOLD FOR REGULATORY PAPERWORK','PROHIBITED AREA-ENTRY RESTRICTED FOR DELIVERY',
'BULK ORDER, REFUSED BY CONSIGNEE','HOLIDAY, DELIVERY ON NEXT BUSINESS DAY','CONSIGNEE\'S ADDRESS INCORRECT','BULK ORDER, REFUSED BY CONSIGN',
'DELAY CAUSED BEYOND OUR CONTROL']

class update_delhivery_orders():
	_name = "update.delhivery.orders"
	def delhivery_update(self,update,api_url):
		awbs = ''
		for keys in update.keys():
			awbs = awbs+','+(str(keys))
		url=api_url.replace("AWB_LIST",awbs)
		
		track_details = {}
		status_details = {}
		scan_details = {}
		status = ''
		awb = ''
		status_loc = ''
		status_date_time = ''
		with requests.Session() as s:
			feed = urllib2.urlopen(url)
			EOMTree = ET.fromstring(feed.read())

			for shipdata in EOMTree.findall('Shipment'):
				status_details['reason'] = ''
				status_details['return_awb']  = ''
				code = ''
				if shipdata.find('AWB').text:
					awb = shipdata.find('AWB').text
				if len(shipdata.find('Status'))>0:
					if shipdata.find('Status').find('Status').text:
						status_details['status'] = shipdata.find('Status').find('Status').text
					if shipdata.find('Status').find('StatusDateTime').text:
						date_time_form = (shipdata.find('Status').find('StatusDateTime').text).split('T')
						status_details['status_date_time'] = date_time_form[0]+','+date_time_form[1].split('.')[0]
					if shipdata.find('Status').find('StatusLocation').text:
						status_details['status_loc'] = shipdata.find('Status').find('StatusLocation').text
					if shipdata.find('Status').find('StatusType').text:
						status_details['status_type'] = shipdata.find('Status').find('StatusType').text
					if shipdata.find('Status').find('StatusCode').text:
						code = shipdata.find('Status').find('StatusCode').text
				if len(shipdata.find('Scans'))>0:
					scan_dets = shipdata.find('Scans')	
	
					for l in range (len(scan_dets)-1,-1,-1):
						scan_det = scan_dets[len(scan_dets)-l-1]
						temp = []
						temp.append(str(scan_det.find('Scan').text)+'('+str(scan_det.find('Instructions').text)+')')
						temp.append(str(scan_det.find('ScannedLocation').text))
						date_time = str(scan_det.find('ScanDateTime').text).split('T')
						temp.append(date_time[0]+','+date_time[1].split('.')[0])
						scan_details['scan'+str(l)] = temp
				if status_details['status_type'] == 'RT':
					status_details['status'] = 'RTO Initiated' 
				elif code in ['EOD-78','EOD-74','EOD-72','EOD-16','EOD-69','EOD-45','EOD-43','EOD-3','EOD-2','EOD-40','EOD-31','EOD-15','EOD-14','EOD-13','EOD-11','EOD-12']:
					status_details['status'] = 'Undelivered'
					status_details['status_type'] = 'UD'
					if shipdata.find('Status').find('Instructions').text:
							status_details['reason'] = shipdata.find('Status').find('Instructions').text
				elif status_details['status_type'] == 'RTO':
					status_details['status'] = 'RTO Initiated'
					status_details['status_type'] = 'RT'
				elif status_details['status_type'] == 'DL':
					status_details['status'] = 'Delivered'
				elif status_details['status'] == 'In Transit':
					status_details['status_type'] = 'IT'
				elif status_details['status'] == 'Dispatched':
					status_details['status_type'] = 'IT'
					status_details['status'] = 'Dispatched'
				elif status_details['status'] == 'LOST':
					status_details['status_type'] = 'lost'
				elif status_details['status'] and status_details['status_date_time']:
					status_details['status'] = 'In Transit'
					status_details['status_type'] = 'IT'			

				track_details['awb'] = awb
				track_details['status_details'] = status_details
				track_details['scan_details'] = scan_details		
			return track_details

class update_bluedart_orders():
	_name = "update.bluedart.orders"
	def bluedart_update(self, update,api_url ):
		awbs = []		
		for keys in update.keys():
			awbs.append(str(keys))
		url=api_url.replace("AWB_LIST",str(awbs))
		track_details = {}
		status_details = {}
		scan_details = {}
		status = ''
		awb = ''
		status_date = ''
		status_time = ''
		with requests.Session() as s:
			feed = urllib2.urlopen(url)
			f = open('out1.xml', 'w')
			print >> f, feed.read()
			f.close()
			DOMTree = xml.dom.minidom.parse("out1.xml")
			collection = DOMTree.documentElement
			Shipments = collection.getElementsByTagName("Shipment")
			for ind in range(0,1):
				Shipment = Shipments[ind]
				awb = str(Shipment.getAttribute('WaybillNo'))
				if Shipment.getElementsByTagName("StatusType"):
					status_type = str(Shipment.getElementsByTagName('StatusType')[0].childNodes[0].data)
				if Shipment.getElementsByTagName("StatusDate"):
					status_date = str(Shipment.getElementsByTagName('StatusDate')[0].childNodes[0].data)
				if Shipment.getElementsByTagName("StatusTime"):
					status_time = str(Shipment.getElementsByTagName('StatusTime')[0].childNodes[0].data	)
				if Shipment.getElementsByTagName("Status"):
					status = str(Shipment.getElementsByTagName('Status')[0].childNodes[0].data	)
				if Shipment.getElementsByTagName("Scans"):
					scan_dets = Shipment.getElementsByTagName("ScanDetail")
					for l in range(0,len(scan_dets)):
						scan = scan_dets[l]
						temp = []
						if scan.getElementsByTagName('Scan'):
							temp.append(scan.getElementsByTagName('Scan')[0].childNodes[0].data)
						if  scan.getElementsByTagName('ScannedLocation'):
							temp.append(scan.getElementsByTagName('ScannedLocation')[0].childNodes[0].data)
						if  scan.getElementsByTagName('ScanDate') and Shipment.getElementsByTagName("ScanTime"):
							temp.append((scan.getElementsByTagName('ScanDate')[0].childNodes[0].data)+','+scan.getElementsByTagName('ScanTime')[0].childNodes[0].data )
						scan_details['scan'+str(l)] = temp
				status_details['reason'] = ''
				status_details['return_awb']  = ''
				if status in BD_reason_list:
					status_type = 'UD'
					status_details['reason'] = status
					status = 'Undelivered'
				elif status_type in ['RT','RTO'] or status in ['RETURN TO SHIPPER','DELIVERED BACK TO SHIPPER']:
					status_type == 'RT'
					status = 'RTO Initiated'
					if Shipment.getElementsByTagName('NewWaybillNo'):
						status_details['return_awb'] = Shipment.getElementsByTagName('NewWaybillNo')[0].childNodes[0].data
				elif status_type == 'DL':
					status = 'Delivered'
				elif status == 'In Transit. Await delivery information':
					status = 'In Transit'
					status_type = 'IT'
					if Shipment.getElementsByTagName("PickUpDate") and Shipment.getElementsByTagName("PickUpTime"):
						status_date = str(Shipment.getElementsByTagName('PickUpDate')[0].childNodes[0].data)
						status_time = str(Shipment.getElementsByTagName('PickUpTime')[0].childNodes[0].data)
				elif status == 'SHIPMENT OUT FOR DELIVERY' :
					status = 'Dispatched'
					status_type = 'IT'

				elif status and status_date and status_time:
					status = 'In Transit'
					status_type = 'IT'
				status_details['status'] = status
				status_details['status_date_time'] = status_date+','+status_time
				status_details['status_loc'] = ''
				status_details['status_type'] = status_type
				track_details['awb'] = awb
				track_details['status_details'] = status_details
				track_details['scan_details'] = scan_details
		
			os.remove("out1.xml")
			return track_details

class update_all_courier_orders():
	_name = "update.allcouriers.orders"
	def all_couriers_update(self, update,courier_name,api_url=False):
		track_details = {}
		status_details = {}
		scan_details = {}
		awb = ''
		
		if courier_name == 'Aramex':
			current_path = os.getcwd()
			os.chdir(os.path.dirname(os.path.realpath(__file__))+'/api_scripts/')
			proc = subprocess.Popen("php -f call_aramex_api.php %s"%update.keys()[0], shell=True, stdout=subprocess.PIPE)
			response = proc.stdout.read()
			status_details = {}
			scan_details = {}
			EOMTree = ET.fromstring(response)
			scansTree = EOMTree.find('Scans')
			status_details['reason'] = ''
			status_details['return_awb']  = ''
			status_details['status']  = ''
			status_details['status_type']  = ''
			if not len(EOMTree.findall('Scans')):
				return
			if len(scansTree.find('WaybillNumber').text)>0:
				awb = scansTree.find('WaybillNumber').text
			for l in range(0,len(scansTree.findall('ScanDetail'))):
				scan = scansTree.findall('ScanDetail')[l]
				temp = []
				temp.append(scan.find('UpdateDescription').text)
				temp.append((scan.find('UpdateLocation').text))
				date_time = (scan.find('UpdateDateTime').text).split('T')
				temp.append(date_time[0]+','+date_time[1].split('.')[0])
				temp.append(scan.find('UpdateCode').text)
				temp.append(scan.find('ProblemCode').text)
				scan_details['scan'+str(l)] = temp
			ind = 0
			for i in range(0,len(scan_details)):
				if scan_details['scan'+str(i)][3] not in ['SH369','SH247']:
					ind = i
					break
			if scan_details['scan'+str(ind)][3] in ['SH407']:
				status_details['status'] = 'RTO Initiated' 
				status_details['status_type'] = 'RT'
			elif scan_details['scan'+str(ind)][3] in ['SH294','SH156','SH043','SH033'] and scan_details[ind][4] in ['U02','U05','U07','U09','U10','U11','U12','U13','U14','U15','C01','A01','A02','A03','A04','A05','A06','A07','A08','A13','A14','A15','A16','A17','A18','A19','A13']:
				status_details['status'] = 'Undelivered'
				status_details['status_type'] = 'UD'
				status_details['reason'] = scan_details[ind][0].split('-')[1]
			elif scan_details['scan'+str(ind)][3] in ['SH005','SH006']:
				status_details['status_type'] = 'DL'
				status_details['status'] = 'Delivered'
			elif scan_details['scan'+str(ind)][3] in ['SH073','SH003','SH004']:
				status_details['status_type'] = 'IT'
				status_details['status'] = 'Dispatched'
			elif scan_details['scan'+str(ind)]:
				status_details['status'] = 'In Transit'
				status_details['status_type'] = 'IT'			
			status_details['status_date_time'] = scan_details['scan'+str(ind)][2]
			status_details['status_loc'] = scan_details['scan'+str(ind)][1]
	
		elif courier_name == 'Ecom Express':
			if not api_url:
				raise osv.except_osv(('Error!!!!'), ('API does not exist for this courier %s')%courier_name)
			url=api_url.replace("AWB_LIST",update.keys()[0])	
			reason = ''
			reason_code = ''
			status_details['status_loc'] = ''
			status_details['return_awb']  = ''
			status_details['reason'] = ''
			status_details['status_date_time'] = ''
			status_details['status'] = ''
			status_details['status_type'] = ''
			with requests.Session() as s:
				feed = urllib2.urlopen(url)
				EOMTree = ET.fromstring(feed.read())
				if not len(EOMTree.findall('object')):
					return
				scansTree = EOMTree.find('object').findall('field')
				for shipdata in scansTree:
					if shipdata.attrib['name'] == 'awb_number':
						awb = shipdata.text
					elif shipdata.attrib['name'] == 'current_location_name':
						status_details['status_loc'] = shipdata.text
					elif shipdata.attrib['name'] == 'last_update_date':
						day = shipdata.text.split('-')[0]
						month = shipdata.text.split('-')[1]
						year = shipdata.text.split('-')[2]
						date_object = datetime.strptime(year+' '+month+' '+day+','+'00:00:00', '%Y %b %d,%H:%M:%S')
						status_details['status_date_time'] = date_object.strftime('%d %B %Y,%H:%M:%S')
					elif shipdata.attrib['name'] == 'status':
						status_details['status'] = shipdata.text
					elif shipdata.attrib['name'] == 'reason_code_number':
						reason_code = shipdata.text
					elif shipdata.attrib['name'] == 'reason_code_description':
						reason = shipdata.text
					elif shipdata.attrib['name'] == 'scans':
						scan_dets = shipdata.findall('object')
						for l in range(0,len(scan_dets)):
							scan_det = scan_dets[l].findall('field')
							temp = []
							instruct = scan_det[1].text
							if scan_det[2].text:
								instruct = scan_det[1].text+'('+scan_det[2].text+')'
							temp.append(str(instruct))
							temp.append(scan_det[4].text)
							temp.append(scan_det[0].text)
							scan_details['scan'+str(l)] = temp
				if reason_code in ['201','207','209','210','212','214','215','217','221','222','223','224','227','228','230','300','309','331','218','219','220']:
					status_details['status'] = 'Undelivered'
					status_details['status_type'] = 'UD'
					status_details['reason'] = reason
				elif reason_code in ['777','206','211']:
					status_details['status'] = 'RTO Initiated'
					status_details['status_type'] = 'RT'
				elif reason_code == '999':
					status_details['status'] = 'Delivered'
					status_details['status_type'] = 'DL'
				elif reason_code in ['333','888']:
					status_details['status_type'] = 'lost'
					status_details['status'] = 'LOST'
				elif status_details['status'] and status_details['status_date_time']:
					status_details['status'] = 'In Transit'
					status_details['status_type'] = 'IT'			

		track_details['awb'] = awb
		track_details['status_details'] = status_details
		track_details['scan_details'] = scan_details
		return track_details
