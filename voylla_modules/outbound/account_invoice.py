from openerp.api import Environment
from openerp import models, api, _
from openerp import fields as fields_model
from openerp.exceptions import except_orm, Warning, RedirectWarning
from openerp.osv import osv, fields
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import openerp.addons.decimal_precision as dp
import pdb
import time
import logging
import requests
import urllib2
from xml.dom.minidom import parse
import xml.dom.minidom
import sys
_logger = logging.getLogger(__name__)
from track_orders import update_bluedart_orders,update_delhivery_orders,update_all_courier_orders
import datetime 
from datetime import datetime, timedelta
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
from urllib2 import URLError, HTTPError
MAX_AWB_COUNT_TO_TRACK = 100
from collections import defaultdict

class account_invoice(models.Model):

	_inherit = "account.invoice"

	@api.one
	@api.depends('invoice_line.price_subtotal', 'tax_line.amount')
	def _compute_amount(self):
		self.amount_untaxed = sum(line.price_subtotal for line in self.invoice_line)
		self.amount_tax = sum(line.amount for line in self.tax_line)
		self.amount_adjustment = sum(adjustment.amount for adjustment in self.adjustments)
		self.amount_total = self.amount_untaxed + self.amount_tax + self.amount_adjustment

	amount_adjustment = fields_model.Float(string='Adjustment', digits=dp.get_precision('Account'),
		store=True, readonly=True, compute='_compute_amount')
	adjustments = fields_model.One2many('account.invoice.adjustment', 'invoice_id', string='Adjustments',
		readonly=True, states={'draft': [('readonly', False)]}, copy=True)

	def get_current_detail(self,cr,uid,ids,field_name=None,arg=None,context=None):
		res={}
		track_dict = {}
		tracking_details = {}
		inv_obj = self.browse(cr,uid,ids[0])
		sale_objs = self.pool.get('sale.order')
		awb_cour = self.pool.get('awb.courier')
		tracking_details_reg = self.pool.get('tracking.details')
		courier_list_reg = self.pool.get('courier.list')
		sale_id = sale_objs.search(cr,uid,[('order_id','=',inv_obj.order_id)])
		sale_obj = sale_objs.browse(cr,uid,sale_id[0])
		awb_no = sale_obj.awb
		awb_obj = awb_cour.browse(cr,uid,awb_cour.search(cr,uid,[('awb_no','=',str(awb_no))]))
		state = awb_obj.shipment_state
		courier_name = sale_obj.courier_name
		track_dict[awb_no] = state
		if state not in ['pending','DL','RTO','lost','RTD'] or field_name == 'scan_details' and state !='pending':
			try:
				if courier_name == 'Delhivery':
					del_obj = courier_list_reg.browse(cr,uid,courier_list_reg.search(cr,uid,[('name','=',courier_name)]))
					update_delhivery_orders_obj = update_delhivery_orders()
					tracking_details = update_delhivery_orders_obj.delhivery_update(track_dict,del_obj.api_url)
				elif courier_name in ['Blue Dart','Blue Dart Priority']:
					blue_obj = courier_list_reg.browse(cr,uid,courier_list_reg.search(cr,uid,[('name','=',courier_name)]))
					update_bluedart_orders_obj = update_bluedart_orders()
					tracking_details = update_bluedart_orders_obj.bluedart_update(track_dict,blue_obj.api_url)
				else:
					courier_obj = courier_list_reg.browse(cr,uid,courier_list_reg.search(cr,uid,[('name','=',courier_name)]))
					if not courier_obj.to_be_tracked:
						raise osv.except_osv(('Warning!!!!'), ('Tracking not available for the Courier %s')%courier_name)
					update_all_courier_orders_obj = update_all_courier_orders()
					tracking_details = update_all_courier_orders_obj.all_couriers_update(track_dict,courier_name,courier_obj.api_url)
			except URLError as e:
				if hasattr(e, 'reason'):
					raise osv.except_osv(('Internet Connection Issue!!!!'), ('%s')%e.reason)
			if tracking_details:
				if tracking_details['status_details']['return_awb'] :
					awb_cour.write(cr,uid,awb.id,{'return_awb':tracking_details['status_details']['return_awb']})
				if tracking_details['status_details']['status_type'] in ['DL','RT','UD','IT','lost','RTD']:
					awb_state = awb_obj.shipment_state
					inv_id = ids
					status_time = tracking_details['status_details']['status_date_time']
					if len(status_time.split('-'))>1:
						year = status_time.split(',')[0].split('-')[0]
						month = status_time.split(',')[0].split('-')[1]
						day = status_time.split(',')[0].split('-')[2]
						date_object = datetime.strptime(year+' '+month+' '+day+','+status_time.split(',')[1], '%Y %m %d,%H:%M:%S')
						tracking_details['status_details']['status_date_time'] = date_object.strftime('%d %B %Y,%H:%M')
					if inv_id and len(tracking_details_reg.search(cr,uid,[('invoice_id','=',inv_id[0])])) == 0:
						vals = {}
						vals = {'invoice_id':inv_id[0],'reason':tracking_details['status_details']['reason'],'status':tracking_details['status_details']['status'],'status_time':tracking_details['status_details']['status_date_time'],'status_loc':tracking_details['status_details']['status_loc']}
						tracking_details_reg.create(cr,uid,vals)
					elif tracking_details['status_details']['status'] == 'Dispatched':
						vals = {}
						vals = {'invoice_id':inv_id[0],'status':tracking_details['status_details']['status'],'status_time':tracking_details['status_details']['status_date_time'],'status_loc':tracking_details['status_details']['status_loc']}
						tracking_details_reg.create(cr,uid,vals)
						awb_cour.write(cr,uid,awb_obj.id,{'is_delayed_sent':True})
					elif not tracking_details_reg.search(cr,1,[('invoice_id','=',inv_id[0]),('status','=',tracking_details['status_details']['status']),('status_time','=',tracking_details['status_details']['status_date_time'])]):
						vals = {}
						vals = {'invoice_id':inv_id[0],'reason':tracking_details['status_details']['reason'],'status':tracking_details['status_details']['status'],'status_time':tracking_details['status_details']['status_date_time'],'status_loc':tracking_details['status_details']['status_loc']}
						tracking_details_reg.create(cr,uid,vals)
					awb_cour.write(cr,uid,awb_obj.id,{'shipment_state':tracking_details['status_details']['status_type'],'return_awb':tracking_details['status_details']['return_awb']})
				if field_name == 'scan_details':
					html = "<table><tr><td> SCAN TYPE &nbsp;&nbsp;&nbsp;</td><td> LOCATION &nbsp;&nbsp;&nbsp;</td><td> TIME <br><br></td></tr>"
					for l in range(0,len(tracking_details['scan_details'])):
						html = html + "<tr><td>"+tracking_details['scan_details']['scan'+str(l)][0]+" &nbsp;&nbsp;&nbsp;</td><td>"+tracking_details['scan_details']['scan'+str(l)][1]+" &nbsp;&nbsp;&nbsp;</td><td>"+tracking_details['scan_details']['scan'+str(l)][2]+"</td> </tr>"
					html = html + "</table>"
					res[inv_obj.id] = html
					return res
			elif field_name == 'scan_details':
				res[inv_obj.id] = 'No scan details available for the queried Airwaybill number'	
				return res
		elif field_name == 'scan_details' and state == 'pending':
			res[inv_obj.id] = 'Pending orders cannot be Tracked'
			return res
		return {'value':{'return_awb' : awb_obj.return_awb}}

	def _get_awb(self,cr,uid,ids,field_name,arg,context=None):
		res = {}
		inv_obj = self.browse(cr,uid,ids[0])
		sale_obj = self.pool.get('sale.order')
		sale_id = sale_obj.search(cr,uid,['|',('order_id','=',inv_obj.order_id),('name','=',inv_obj.origin)])
		if not sale_id:
			res[inv_obj.id] = None
			return res
		awb_no = ''
		awb_no = sale_obj.browse(cr,uid,sale_id[0]).awb
		if field_name == 'awb_no':
			res[inv_obj.id] = awb_no
		elif field_name == 'return_awb':
			awb_no = self.pool.get('awb.courier').browse(cr,uid,self.pool.get('awb.courier').search(cr,uid,[('awb_no','=',awb_no)])).return_awb
			res[inv_obj.id] = awb_no
		return res

	def _get_state(self,cr,uid,ids,field_name,arg,context=None):
		res = {}
		inv_obj = self.browse(cr,uid,ids[0])
		sale_obj = self.pool.get('sale.order')
		awb_obj = self.pool.get('awb.courier')
		sale_id = sale_obj.search(cr,uid,['|',('order_id','=',inv_obj.order_id),('name','=',inv_obj.origin)])
		if not sale_id:
			res[inv_obj.id]=None
			return res
		awb_no = sale_obj.browse(cr,uid,sale_id[0]).awb
		state = awb_obj.browse(cr,uid,awb_obj.search(cr,uid,[('awb_no','=',str(awb_no))])).shipment_state
		if not state and sale_obj.browse(cr,uid,sale_id[0]).state in ('done','rto','returned'):
			track_details = self.pool.get('tracking.details').search(cr,uid,[('invoice_id','=',inv_obj.id)],order='create_date desc')
			if not track_details:
				state = 'shipped'
			else:
				cur_status = self.pool.get('tracking.details').browse(cr,uid,track_details[0]).status
				state = 'IT'
				if cur_status == 'Delivered':
					state = 'DL'
				elif cur_status == 'Undelivered':
					state = 'UD'
				elif cur_status == 'RTO Initiated':
					state = 'RT'
				elif cur_status == 'RTO Received':
					state = 'RTO'
				elif cur_status == 'LOST':
					state = 'lost'
				elif cur_status == 'RTO Delivered':
					state = 'RTD'
		res[inv_obj.id] = state
		return res

	def update_shipment_status_erp(self,cr,uid,context=None):
		remote_tasks_obj = self.pool.get('remote.tasks')
		awb_cour = self.pool.get('awb.courier')
		cour_list_reg = self.pool.get('courier.list')
		to_be_tracked_ids = cour_list_reg.search(cr,uid,[('to_be_tracked','=',True)])
		if len(to_be_tracked_ids)>0:
			to_be_tracked_objs = cour_list_reg.browse(cr,uid,to_be_tracked_ids)
			for cour_obj in to_be_tracked_objs:
				sql = "select awb_no,shipment_state from awb_courier where shipment_state not in ('pending','DL','RTO','lost','RTD') and courier_name = %s"%cour_obj.id
				cr.execute(sql)
				results = cr.fetchall()
				for x in xrange(0, len(results), 500):
					awb_objs = dict((awb,state) for awb,state in results[x:x+500])
					track_awbs = defaultdict(list)
					track_awbs[cour_obj.name] = awb_objs
					if awb_objs:
						_logger.info("*******UPDATE SHIPMENT STATE TASK PUSHED FOR %s *******"%cour_obj.name)
						remote_tasks_obj.update_shipment_state_bluedart(cr, uid, track_awbs,cour_obj.api_url)
		else:
			_logger.info("***No Active Couriers to track***")
			return

	def full_detail(self,cr,uid,ids,context=None):
		record = self.browse(cr, uid, ids[0], context=context)
		models_data = self.pool.get('ir.model.data')
		dummy, form_view = models_data.get_object_reference(cr, uid, 'outbound', 'view_scans2')
		return {
			'name': 'Scan Details',
			'type': 'ir.actions.act_window',
			'view_type': 'form',
			'view_mode': 'form',
			'view_id':False,
			'views': [(form_view or False, 'form')],
			'res_model': 'account.invoice',
			'res_id':record.id,
			'target':'new',
		}

	_columns = {
		'scans': fields.one2many('tracking.details','invoice_id','Status Details'),
		'awb_no' : fields.function(_get_awb,string="AWB No",type='char',store=False),
		'shipment_status' : fields.function(_get_state,string="Shipment Status",type='selection',selection=[
		   ('shipped','Shipped'),
		   ('DL','Delivered'),
		   ('pending','Pending'),
		   ('IT','In Transit'),
			('RT','RTO Initiated'),
			('RTD','RTO Delivered'),
			('RTO','RTO Received'),
			('UD','Undelivered'),
			('lost','LOST')
			],store=False),
		'scan_details' : fields.function(get_current_detail,string="All Scans",type='html',store=False),
		'return_awb' : fields.function(_get_awb,string="Return AWB No",type='char',store=False),
		}

class courier_list(osv.osv):
	_inherit='courier.list'
	_columns={
	'to_be_tracked':fields.boolean('To Be Tracked'),
	'api_url':fields.char('API URL'),
	}

class awb_courier(osv.osv):
	_inherit = "awb.courier"
	def write(self, cr, uid,ids, values, context=None):
		awb_obj = self.browse(cr,uid,ids)
		track_reg = self.pool.get('tracking.details')
		if 'shipment_state' in values.keys():
			if values['shipment_state'] == 'IT' and awb_obj.is_delayed_sent==False:
				undelivered_sent = track_reg.browse(cr,uid,track_reg.search(cr,uid,[('invoice_id','=',awb_obj.invoice_no.id),('status','=','Undelivered')]))
				if undelivered_sent:
					x = super(awb_courier, self).write(cr, uid,ids, {'is_delayed_sent':True}, context=context)
					return
				it_date = awb_obj.order_no.dispatched_date.split(' ')[0]
				cur_date = datetime.now().strftime("%Y-%m-%d")
				diff_format = "%Y-%m-%d"
				tdelta = datetime.strptime(cur_date, diff_format) - datetime.strptime(it_date, diff_format)
				if tdelta.days > 6:
					self.send_delay_delivery_mail_sms(cr,uid,ids,values)
					x = super(awb_courier, self).write(cr, uid,ids, {'is_delayed_sent':True}, context=context)  
			elif values['shipment_state'] == 'RT' and awb_obj.shipment_state=='RT':
				it_date = track_reg.browse(cr,uid,track_reg.search(cr,uid,[('invoice_id','=',awb_obj.invoice_no.id),('status','=','RTO Initiated')]))[0].status_time.split(',')[0]
				cur_date = datetime.now().strftime("%d %B %Y")
				diff_format = "%d %B %Y"
				tdelta = datetime.strptime(cur_date, diff_format) - datetime.strptime(it_date, diff_format)
				if tdelta.days > 30:
					values['shipment_state']='lost'
			if awb_obj.shipment_state != values['shipment_state']:
				x = super(awb_courier, self).write(cr, uid,ids, values, context=context)    
				return x
		else:
			x = super(awb_courier, self).write(cr, uid,ids, values, context=context)    
			return 

	def send_delay_delivery_mail_sms(self,cr,uid,ids,shipment_details,context=None):
		self_obj=self.browse(cr,uid,ids)
		try:
			if shipment_details:
				mail_sent = self.send_delay_mail(cr,uid,ids)
				context = dict(context or {}, mail_create_nolog=True)
				sms_sent =  self.send_delay_sms(cr,uid,ids)
				if mail_sent:
					self.pool.get('account.invoice').message_post(cr, uid, self_obj.invoice_no.id, body=(mail_sent+' Mail Sent , Msg '+sms_sent), context=context)
		except:
			pass

	def send_delay_mail(self,cr,uid,ids,context=None):
		name = 'Shipment Delayed'
		self_obj = self.browse(cr,uid,ids)
		if self_obj.order_no.source_id.name == 'Voylla Website':
			name = 'Shipment Delayed Voylla'
		email_template_obj = self.pool.get('email.template')
		template_id = email_template_obj.search(cr,uid,[('name','=',name)])
		values = email_template_obj.generate_email(cr, uid, template_id[0], self_obj.id, context=None)
		val3= values.get('body_html')
		values["subject"] = "Shipment Notification of order %s by Voylla.com" %(self_obj.order_no.order_id)
		values["email_from"] = "help@voylla.com"
		values["email_to"] = self_obj.order_no.partner_shipping_id.email
		values["body_html"] = val3
		email_template_id = email_template_obj.create(cr, uid, values)
		mail_mail_obj = self.pool.get('mail.mail')
		values['is_send'] = True
		msg_id = mail_mail_obj.create(cr, uid, values, context=context)
		mail_mail_obj.send(cr, uid, [msg_id], context=context)
		return name

	def send_delay_sms(self,cr,uid,ids,context=None):
		self_obj = self.browse(cr,uid,ids)
		username="jagrati@voylla.in"
		password="d3fault03"
		if not self_obj.order_no.partner_shipping_id.phone:
			return 'No. Not found'
		phone_number = str(self_obj.order_no.partner_shipping_id.phone.encode('ascii',errors='ignore'))
		sms="Your %s order no %s for VOYLLA product was shipped via %s tracking number %s but the courier company has not yet been able to deliver order. We are working on getting the shipment delivered to you at the earliest. Please call us on +917676111022 for alternative instructions"%(self_obj.order_no.source_id.name, self_obj.order_no.order_id, self_obj.order_no.courier_name, self_obj.order_no.awb)
		if self_obj.order_no.source_id.name == "Voylla Website":
			sms = "Your %s order no %s  was shipped via %s tracking number %s but the courier company has not yet been able to deliver order. We are working on getting the shipment delivered to you at the earliest. Please call us on +917676111022 for alternative instructions"%(self_obj.order_no.source_id.name, self_obj.order_no.order_id, self_obj.order_no.courier_name, self_obj.order_no.awb)
		user_agent = 'Mozilla/4.0 (compatible; MSIE 5.5; Windows NT)'
		headers = { 'User-Agent' : user_agent }

		url= "https://api.mVaayoo.com/mvaayooapi/MessageCompose?user=jagrati@voylla.in:d3fault03&senderID=VOYLLA&receipientno="+phone_number+"&dcs=0&msgtxt="+sms+"&state=4"
		url = url.replace('+', '%2B')
		s = requests.Session()
		x = s.get(url,data={},headers={ 'User-Agent' : user_agent },verify=False) 
		return 'Sent'

	def get_IT_time(self,cr,uid,ids,context=None): 
		dispatched_date = self.browse(cr,uid,ids).order_no.dispatched_date
		return dispatched_date.split(' ')[0]

	def get_track_url(self,cr,uid,ids,context=None):
		url = self.pool.get('sale.order').get_tracking_url(cr,uid,self.browse(cr,uid,ids).order_no.id)
		return url

class tracking_details(osv.osv):
	_name = 'tracking.details'
		

	_columns = {
	'invoice_id' : fields.many2one('account.invoice','Invoice ID',select=1),
	'status' : fields.char("Status",select=1),
	'status_time': fields.char("Status Date Time"),
	'status_loc' : fields.char("Status Location"),
	'reason':fields.char("Reason for Undelivery")
	}
	def create(self, cr, uid, values, context=None):
		if len(values['status_time'].split('-'))>1:
			year = values['status_time'].split(',')[0].split('-')[0]
			month = values['status_time'].split(',')[0].split('-')[1]
			day = values['status_time'].split(',')[0].split('-')[2]
			date_object = datetime.strptime(year+' '+month+' '+day+','+values['status_time'].split(',')[1], '%Y %m %d,%H:%M:%S')
			values['status_time'] = date_object.strftime('%d %B %Y,%H:%M')
		x = super(tracking_details, self).create(cr, uid, values, context=context)
		track_obj = self.browse(cr,uid,x)[0]
		if track_obj:
			self.send_couriers_mail(cr,uid,track_obj.id,values) 
		return x
		
	def send_couriers_mail(self,cr,uid,ids,shipment_details,context=None):
		self_obj=self.browse(cr,uid,ids)
		try:
			if shipment_details:
				mail_sent = self.send_delivery_mail(cr,uid,ids,shipment_details)
				context = dict(context or {}, mail_create_nolog=True)
				sms_sent = self.send_delivery_sms(cr,uid,ids,shipment_details)
				if mail_sent:
					self.pool.get('account.invoice').message_post(cr, uid, self_obj.invoice_id.id, body=(mail_sent+' Mail Sent, Msg '+sms_sent), context=context)
		except:
			pass

	def send_delivery_sms(self,cr,uid,ids,shipment_details):
		state = shipment_details['status']
		self_obj = self.browse(cr,uid,ids)
		sale_id = self.pool.get('sale.order').search(cr,uid,[('order_id','=',str(self.browse(cr,uid,ids).invoice_id.order_id))])
		sale_obj = self.pool.get('sale.order').browse(cr,uid,sale_id)[0]
		username="jagrati@voylla.in"
		password="d3fault03"
		if sale_obj.source_id.name == "Voylla Website":
			if state == 'Delivered':
				sms = "Your %s order no %s shipped via %s tracking number %s has been successfully delivered on %s. In case you have any query call us at +917676111022"%(sale_obj.source_id.name, sale_obj.order_id, sale_obj.courier_name, sale_obj.awb, self_obj.status_time.split(',')[0])
			elif state == 'Undelivered':
				sms = "Your %s order no %s shipped via %s tracking number %s could not be delivered on %s. We are continuously following up with courier company to deliver your order on priority. Please call us on +917676111022 for alternative instructions"%(sale_obj.source_id.name, sale_obj.order_id, sale_obj.courier_name, sale_obj.awb, self_obj.status_time.split(',')[0])
			elif state in ['RTO Initiated']:
				sms = "Your %s order no %s shipped via %s tracking number %s could not be delivered and is being returned. Please call us on +917676111022 for alternative instructions"%(sale_obj.source_id.name, sale_obj.order_id, sale_obj.courier_name, sale_obj.awb)
			elif state == 'Dispatched':
				diff_format='%d %B %Y,%H:%M'
				cur_time =  (datetime.now()+timedelta(hours=5.50)).strftime(diff_format)
				ship_time = shipment_details['status_time']
				tdelta = datetime.strptime(ship_time, diff_format) - datetime.strptime(cur_time, diff_format)
				if abs(tdelta).seconds/3600 >2.50 :
					return
				else:
					sms = "Your %s order no %s shipped via %s tracking number %s is out for delivery today. Please call us on +917676111022 for alternative instructions"%(sale_obj.source_id.name, sale_obj.order_id, sale_obj.courier_name, sale_obj.awb)
			else:
				return
		else:
			if state == 'Delivered': 
				sms="Your %s order no %s for VOYLLA product shipped via %s tracking number %s has been successfully delivered on %s. In case you have any query call us at +917676111022"%(sale_obj.source_id.name, sale_obj.order_id, sale_obj.courier_name, sale_obj.awb, self_obj.status_time.split(',')[0])
			elif state == 'Undelivered':
				sms="Your %s order no %s for VOYLLA product shipped via %s tracking number %s could not be delivered on %s. We are continuously following up with courier company to deliver your order on priority. Please call us on +917676111022 for alternative instructions"%(sale_obj.source_id.name, sale_obj.order_id, sale_obj.courier_name, sale_obj.awb, self_obj.status_time.split(',')[0])
			elif state in ['RTO Initiated']:
				sms="Your %s order no %s for VOYLLA product shipped via %s tracking number %s could not be delivered and is being returned. Please call us on +917676111022 for alternative instructions"%(sale_obj.source_id.name, sale_obj.order_id, sale_obj.courier_name, sale_obj.awb)
			elif state == 'Dispatched':
				diff_format='%d %B %Y,%H:%M'
				cur_time =  (datetime.now()+timedelta(hours=5.50)).strftime(diff_format)
				ship_time = shipment_details['status_time']
				tdelta = datetime.strptime(ship_time, diff_format) - datetime.strptime(cur_time, diff_format)
				if abs(tdelta).seconds/3600 >2.50 :
					return
				else:
					sms = "Your %s order no %s for VOYLLA product shipped via %s tracking number %s is out for delivery today. Please call us on +917676111022 for alternative instructions"%(sale_obj.source_id.name, sale_obj.order_id, sale_obj.courier_name, sale_obj.awb)
			else:
				return
		user_agent = 'Mozilla/4.0 (compatible; MSIE 5.5; Windows NT)'
		headers = { 'User-Agent' : user_agent }
		if not sale_obj.partner_shipping_id.phone:
			return 'No. Not Found'
		phone_number = str(sale_obj.partner_shipping_id.phone.encode('ascii',errors='ignore'))
		url= "https://api.mVaayoo.com/mvaayooapi/MessageCompose?user=jagrati@voylla.in:d3fault03&senderID=VOYLLA&receipientno="+phone_number+"&dcs=0&msgtxt="+sms+"&state=4"
		url = url.replace('+', '%2B')
		s = requests.Session()
		x = s.get(url,data={},headers={ 'User-Agent' : user_agent },verify=False) 
		return 'Sent'

	def send_delivery_mail(self,cr,uid,ids,shipment_details,context=None):
		self_obj=self.browse(cr,uid,ids)
		email_template_obj = self.pool.get('email.template')
		state = shipment_details['status']
		sale_id = self.pool.get('sale.order').search(cr,uid,[('order_id','=',str(self.browse(cr,uid,ids).invoice_id.order_id))])
		sale_obj = self.pool.get('sale.order').browse(cr,uid,sale_id)[0]
		name = ''
		try:
			if sale_obj.source_id.name == 'Voylla Website':
				if state == 'Delivered':
					name = 'Shipment Delivered Voylla'
				elif state == 'Undelivered':
					name = 'Shipment Undelivered Voylla'
				elif state in ['RTO Initiated']:
					name = 'Shipment Returned Voylla'
				elif state == 'Dispatched':
					diff_format='%d %B %Y,%H:%M'
					cur_time =  (datetime.now()+timedelta(hours=5.50)).strftime(diff_format)
					ship_time = shipment_details['status_time']
					tdelta = datetime.strptime(ship_time, diff_format) - datetime.strptime(cur_time, diff_format)
					if abs(tdelta).seconds/3600 >2.50 :
						return
					else:
						name = 'Shipment OutForDelivery Voylla'
				else:
					return

			else:
				if state == 'Delivered':
					name = 'Shipment Delivered'
				elif state == 'Undelivered':
					name = 'Shipment Undelivered'
				elif state in ['RTO Initiated']:
					name = 'Shipment Returned'
				elif state == 'Dispatched':
					diff_format='%d %B %Y,%H:%M'
					cur_time =  (datetime.now()+timedelta(hours=5.50)).strftime(diff_format)
					ship_time = shipment_details['status_time']
					tdelta = datetime.strptime(ship_time, diff_format) - datetime.strptime(cur_time, diff_format)
					if abs(tdelta).seconds/3600 >2.50 :
						return
					else:
						name = 'Shipment OutForDelivery'
				else:
					return
				
			if name:
				template_id = email_template_obj.search(cr,uid,[('name','=',name)])
				values = email_template_obj.generate_email(cr, uid, template_id[0], ids, context=None)
				val3= values.get('body_html')
				values["subject"] = "Shipment Notification of order %s by Voylla.com" %(self_obj.invoice_id.order_id)
				values["email_from"] = "help@voylla.com"
				values["email_to"] = sale_obj.partner_shipping_id.email
				values["body_html"] = val3
				email_template_id = email_template_obj.create(cr, uid, values)
				mail_mail_obj = self.pool.get('mail.mail')
				values['is_send'] = True
				msg_id = mail_mail_obj.create(cr, uid, values, context=context)
				mail_mail_obj.send(cr, uid, [msg_id], context=context)
				return name
		except:
			pass

	def mail_get_channel(self,cr,uid,ids,context=None):
		sale_id = self.pool.get('sale.order').search(cr,uid,[('order_id','=',str(self.browse(cr,uid,ids).invoice_id.order_id))])
		sale_obj = self.pool.get('sale.order').browse(cr,uid,sale_id)[0]
		return sale_obj.three_p_channel
	
	def mail_get_courier(self,cr,uid,ids,context=None):
		sale_id = self.pool.get('sale.order').search(cr,uid,[('order_id','=',str(self.browse(cr,uid,ids).invoice_id.order_id))])
		sale_obj = self.pool.get('sale.order').browse(cr,uid,sale_id)[0]
		return sale_obj.courier_name
	
	def mail_get_awb(self,cr,uid,ids,context=None):
		sale_id = self.pool.get('sale.order').search(cr,uid,[('order_id','=',str(self.browse(cr,uid,ids).invoice_id.order_id))])
		sale_obj = self.pool.get('sale.order').browse(cr,uid,sale_id)[0]
		return sale_obj.awb

	def mail_get_IT_time(self,cr,uid,ids,context=None):
		sale_id = self.pool.get('sale.order').search(cr,uid,[('order_id','=',str(self.browse(cr,uid,ids).invoice_id.order_id))])
		sale_obj = self.pool.get('sale.order').browse(cr,uid,sale_id)[0]
		return sale_obj.dispatched_date.split(' ')[0]

	def get_track_url(self,cr,uid,ids,context=None):
		sale_id = self.pool.get('sale.order').search(cr,uid,[('order_id','=',str(self.browse(cr,uid,ids).invoice_id.order_id))])
		sale_obj = self.pool.get('sale.order').browse(cr,uid,sale_id)[0]
		url = self.pool.get('sale.order').get_tracking_url(cr,uid,sale_obj.id)
		return url


class account_invoice_adjustments(osv.osv):
	_name = "account.invoice.adjustment"

	_columns = {
		"adjustment_label" : fields.selection([
			("standard_shipping", "Standard Shipping Charges"),
			("cashinaccount", "Cash In Account"),
			("express_shipping", "Express Shipping Charges"),
			("cod_charges", "CoD Charges"),
			("coupon", "Coupon"),
			("voucher", "Voucher"),
			("free_replacement", "Free Replacement"),
			("employee_discount", "Employee Discount"),
			("giftwrap", "Giftwrap"),
			("online_payment_discount", "Online Payment Discount"),
			("promotion", "Promotion"),
			("bogo", "Free Product Discount"),
			("other", "Others")
			], "Adjustment Type", required=True),
		"adjustment_notes": fields.char("Adjustment Notes"),
		"amount" : fields.float("Amount", required=True),
		"invoice_id": fields.many2one('sale.order', 'Order Reference', ondelete='cascade', select=True, readonly=True)
	}



class sale_order(osv.osv):

	_inherit = "sale.order"

	def action_invoice_create(self, cr, uid, ids, grouped=False, states=None, date_invoice = False, context=None):
			cr = self.pool.cursor()
			if states is None:
				states = ['confirmed', 'done', 'exception']
			res = False
			invoices = {}
			invoice_ids = []
			invoice = self.pool.get('account.invoice')
			obj_sale_order_line = self.pool.get('sale.order.line')
			obj_sale_order_adj = self.pool.get("sale.order.adjustment")
			partner_currency = {}
			# If date was specified, use it as date invoiced, usefull when invoices are generated this month and put the
			# last day of the last month as invoice date
			if date_invoice:
				context = dict(context or {}, date_invoice=date_invoice)
			
 
			for o in self.browse(cr, uid, ids, context=context):
				currency_id = o.pricelist_id.currency_id.id
				if (o.partner_id.id in partner_currency) and (partner_currency[o.partner_id.id] <> currency_id):
					raise osv.except_osv(
						_('Error!'),
						_('You cannot group sales having different currencies for the same partner.'))
					
				partner_currency[o.partner_id.id] = currency_id
				lines = []
				for line in o.order_line:
					if line.invoiced:
						continue
					elif (line.state in states):
						lines.append(line.id)
				adjustments = []
				for adjustment in o.adjustments:
					adjustments.append(adjustment.id)
				created_lines = obj_sale_order_line.invoice_line_create(cr, uid, lines)
				created_adjustments = obj_sale_order_adj.invoice_adjustment_create(cr, uid, adjustments)
				if created_lines:
					# invoices.setdefault(o.partner_invoice_id.id or o.partner_id.id, []).append((o, created_lines))
					invoices.setdefault(o.partner_invoice_id.id or o.partner_id.id, []).append((o, created_lines, created_adjustments))

			if not invoices:
				for o in self.browse(cr, uid, ids, context=context):
					for i in o.invoice_ids:
						if i.state == 'draft':
							return i.id
			for val in invoices.values():
				if grouped:
					res = self._make_invoice(cr, uid, val[0][0], reduce(lambda x, y: x + y, [l for o, l in val], []), context=context)
					invoice_ref = ''
					origin_ref = ''
					for o, l in val:
						invoice_ref += (o.client_order_ref or o.name) + '|'
						origin_ref += (o.origin or o.name) + '|'
						self.write(cr, uid, [o.id], {'state': 'progress'})
						cr.execute('insert into sale_order_invoice_rel (order_id,invoice_id) values (%s,%s)', (o.id, res))
						self.invalidate_cache(cr, uid, ['invoice_ids'], [o.id], context=context)
					#remove last '|' in invoice_ref
					if len(invoice_ref) >= 1:
						invoice_ref = invoice_ref[:-1]
					if len(origin_ref) >= 1:
						origin_ref = origin_ref[:-1]
					invoice.write(cr, uid, [res], {'origin': origin_ref, 'name': invoice_ref})
				else:
					for order, il, adj in val:
						res = self._make_invoice(cr, uid, order, il, adj, context=context)
						invoice_ids.append(res)
						self.write(cr, uid, [order.id], {'state': 'progress'})
						cr.execute('insert into sale_order_invoice_rel (order_id,invoice_id) values (%s,%s)', (order.id, res))
						self.invalidate_cache(cr, uid, ['invoice_ids'], [order.id], context=context)
			cr.commit()            
			cr.close()            
			return res


	def _make_invoice(self, cr, uid, order, lines, adjustments, context=None):

		inv_obj = self.pool.get('account.invoice')
		obj_invoice_line = self.pool.get('account.invoice.line')
		if context is None:
			context = {}
		invoiced_sale_line_ids = self.pool.get('sale.order.line').search(cr, uid, [('order_id', '=', order.id), ('invoiced', '=', True)], context=context)
		from_line_invoice_ids = []
		for invoiced_sale_line_id in self.pool.get('sale.order.line').browse(cr, uid, invoiced_sale_line_ids, context=context):
			for invoice_line_id in invoiced_sale_line_id.invoice_lines:
				if invoice_line_id.invoice_id.id not in from_line_invoice_ids:
					from_line_invoice_ids.append(invoice_line_id.invoice_id.id)
		for preinv in order.invoice_ids:
			if preinv.state not in ('cancel',) and preinv.id not in from_line_invoice_ids:
				for preline in preinv.invoice_line:
					inv_line_id = obj_invoice_line.copy(cr, uid, preline.id, {'invoice_id': False, 'price_unit': -preline.price_unit})
					lines.append(inv_line_id)
		inv = self._prepare_invoice(cr, uid, order, lines, adjustments, context=context)
		inv_id = inv_obj.create(cr, uid, inv, context=context)
		data = inv_obj.onchange_payment_term_date_invoice(cr, uid, [inv_id], inv['payment_term'], time.strftime(DEFAULT_SERVER_DATE_FORMAT))
		if data.get('value', False):
			inv_obj.write(cr, uid, [inv_id], data['value'], context=context)
		inv_obj.button_compute(cr, uid, [inv_id])
		return inv_id


	def _prepare_invoice(self, cr, uid, order, lines, adjustments, context=None):
		"""Prepare the dict of values to create the new invoice for a
		   sales order. This method may be overridden to implement custom
		   invoice generation (making sure to call super() to establish
		   a clean extension chain).

		   :param browse_record order: sale.order record to invoice
		   :param list(int) line: list of invoice line IDs that must be
								  attached to the invoice
		   :return: dict of value to create() the invoice
		"""
		if context is None:
			context = {}
		journal_ids = self.pool.get('account.journal').search(cr, uid,
			[('type', '=', 'sale'), ('company_id', '=', order.company_id.id)],
			limit=1)
		if not journal_ids:
			raise osv.except_osv(_('Error!'),
				_('Please define sales journal for this company: "%s" (id:%d).') % (order.company_id.name, order.company_id.id))
		invoice_vals = {
			'name': order.client_order_ref or '',
			'origin': order.name,
			'type': 'out_invoice',
			'reference': order.client_order_ref or order.name,
			'account_id': order.partner_id.property_account_receivable.id,
			'partner_id': order.partner_invoice_id.id,
			'journal_id': journal_ids[0],
			'invoice_line': [(6, 0, lines)],
			'adjustments': [(6, 0, adjustments)],
			'currency_id': order.pricelist_id.currency_id.id,
			'comment': order.note,
			'payment_term': order.payment_term and order.payment_term.id or False,
			'fiscal_position': order.fiscal_position.id or order.partner_id.property_account_position.id,
			'date_invoice': context.get('date_invoice', False),
			'company_id': order.company_id.id,
			'user_id': order.user_id and order.user_id.id or False,
			'section_id' : order.section_id.id
		}

		# Care for deprecated _inv_get() hook - FIXME: to be removed after 6.1
		invoice_vals.update(self._inv_get(cr, uid, order, context=context))
		return invoice_vals


# class sale_order_line
#         def _prepare_order_line_invoice_line(self, cr, uid, line, account_id=False, context=None):
#         """Prepare the dict of values to create the new invoice line for a
#            sales order line. This method may be overridden to implement custom
#            invoice generation (making sure to call super() to establish
#            a clean extension chain).

#            :param browse_record line: sale.order.line record to invoice
#            :param int account_id: optional ID of a G/L account to force
#                (this is used for returning products including service)
#            :return: dict of values to create() the invoice line
#         """
#         res = {}
#         if not line.invoiced:
#             if not account_id:
#                 if line.product_id:
#                     account_id = line.product_id.property_account_income.id
#                     if not account_id:
#                         account_id = line.product_id.categ_id.property_account_income_categ.id
#                     if not account_id:
#                         raise osv.except_osv(_('Error!'),
#                                 _('Please define income account for this product: "%s" (id:%d).') % \
#                                     (line.product_id.name, line.product_id.id,))
#                 else:
#                     prop = self.pool.get('ir.property').get(cr, uid,
#                             'property_account_income_categ', 'product.category',
#                             context=context)
#                     account_id = prop and prop.id or False
#             uosqty = self._get_line_qty(cr, uid, line, context=context)
#             uos_id = self._get_line_uom(cr, uid, line, context=context)
#             pu = 0.0
#             if uosqty:
#                 pu = round(line.price_unit * line.product_uom_qty / uosqty,
#                         self.pool.get('decimal.precision').precision_get(cr, uid, 'Product Price'))
#             fpos = line.order_id.fiscal_position or False
#             account_id = self.pool.get('account.fiscal.position').map_account(cr, uid, fpos, account_id)
#             if not account_id:
#                 raise osv.except_osv(_('Error!'),
#                             _('There is no Fiscal Position defined or Income category account defined for default properties of Product categories.'))
#             res = {
#                 'name': line.name,
#                 'sequence': line.sequence,
#                 'origin': line.order_id.name,
#                 'account_id': account_id,
#                 'price_unit': pu,
#                 'quantity': uosqty,
#                 'discount': line.discount,
#                 'uos_id': uos_id,
#                 'product_id': line.product_id.id or False,
#                 'invoice_line_tax_id': [(6, 0, [x.id for x in line.tax_id])],
#                 'account_analytic_id': line.order_id.project_id and line.order_id.project_id.id or False,
#             }

#         return res

#     def invoice_line_create(self, cr, uid, ids, context=None):
#         if context is None:
#             context = {}

#         create_ids = []
#         sales = set()
#         for line in self.browse(cr, uid, ids, context=context):
#             vals = self._prepare_order_line_invoice_line(cr, uid, line, False, context)
#             if vals:
#                 inv_id = self.pool.get('account.invoice.line').create(cr, uid, vals, context=context)
#                 self.write(cr, uid, [line.id], {'invoice_lines': [(4, inv_id)]}, context=context)
#                 sales.add(line.order_id.id)
#                 create_ids.append(inv_id)
#         # Trigger workflow events
#         for sale_id in sales:
#             workflow.trg_write(uid, 'sale.order', sale_id, cr)
#         return create_ids
