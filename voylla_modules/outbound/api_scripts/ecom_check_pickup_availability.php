<?php
$ch = curl_init();
$curlConfig = array(
    CURLOPT_URL            => $argv[1],
    CURLOPT_POST           => true,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_POSTFIELDS     => array(
        'username' => $argv[2],
        'password' => $argv[3],
        )
);
curl_setopt_array($ch, $curlConfig);
$result = curl_exec($ch);
if($result == false)
{
	die();
}
echo $result;
curl_close($ch);
?>