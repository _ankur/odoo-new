<?php
$ch = curl_init();
// $doc = new DOMDocument();
$data = json_decode($argv[1]);
$curlConfig = array(
    CURLOPT_URL            => $data->url,
    CURLOPT_POST           => true,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_POSTFIELDS     => array(
        'username' => $data->username,
        'password' => $data->password,
        'xml_input' => $data->SHIPMENT,
        )
);
curl_setopt_array($ch, $curlConfig);
$result = curl_exec($ch);
if($result == false)
{
	die();
}
$doc = str_replace("\n", '', $result);
echo $doc;
curl_close($ch);
?>