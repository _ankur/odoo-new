from openerp.osv import osv, fields
import pdb
import base64
from datetime import datetime,timedelta
import logging
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
import re
from voylla_modules.config import home
import xlwt
import subprocess
import shlex
import os

_logger = logging.getLogger(__name__)


PAYMENT_TYPE = [
	('cashondelivery', 'Cash On Delivery'),
	('prepaid', 'Prepaid')
]

CONFIRMATION_STATUS = [
	('confirmed', 'Confirmed'),
	('not_confirmed', 'Not Confirmed')
]

class order_confirmation_rules(osv.osv):
	_name = 'order.confirmation.rules'

	_columns = {
		'name': fields.char('Name', required=True),
		'gender': fields.many2one('product.gender','Gender',select=1),
		'start_time': fields.datetime("Start Time"),
		'end_time': fields.datetime("End Time"),
		'state_ids': fields.many2many('res.country.state', 'order_confirmation_rules_country_state_rel','order_confirmation_rules_id', 'res_country_state_id', 'States'),
		'order_value': fields.float('Order Value'),
		'payment_type': fields.selection(PAYMENT_TYPE, 'Payment Type', select=True),
		'channel_ids': fields.many2many('crm.tracking.source', 'order_confirmation_rules_crm_tracking_source_rel','order_confirmation_rules_id', 'crm_tracking_source_id', 'Channels'),
		'confirmation_status': fields.selection(CONFIRMATION_STATUS, 'Confirmation Status', select=True),
		'activate' : fields.boolean("Activate")
	}


class gift_and_offer_quantity(osv.osv):
	_name='gift_and_offer.quantity'
	_columns = {
				'product_id':fields.many2one('product.product', 'Product'),
				'qty':fields.integer('Qty'),
	}
	def gift_item_available(self,cr,uid,product_id,product_quantity,context=None):
		sql_query = "SELECT  SUM(qty.count) as count from ( select -1 * sum(product_uom_qty) as count from stock_move  where location_id = 2035 and state in ('done','assigned') and product_id= %d UNION ALL select sum(product_uom_qty) as count from stock_move  where location_dest_id = 2035 and state in ('done','assigned') and product_id= %d ) as qty"%(product_id,product_id)
		cr.execute(sql_query)
		avl_qty = cr.fetchall()
		if avl_qty > 0 and avl_qty > product_quantity:
			return True
		else:
			return False
		# gift_id = self.search(cr,uid,[('product_id','=',product_id)])
		# gift_item_qty = self.browse(cr,uid,gift_id).qty
		# if gift_item_qty > product_quantity:
		# 	bin_ids_qty = [2035,product_quantity,2035]
		# 	self.write(cr,uid,gift_id,{'qty':gift_item_qty-product_quantity})
		# 	return [bin_ids_qty]
		# else:
		# 	return False

	def gift_coin_adjustment(self,cr,uid,context=None):
		_logger.info("=========================")
		_logger.info("Game Begins")
		_logger.info("=========================")
		sale_line_obj = self.pool.get('sale.order.line')
		stock_move_obj=self.pool.get('stock.move')
		sale_obj = self.pool.get('sale.order')
		sql= "select id from sale_order where note like '%Free Polki bag%' and state='queued';"
		cr.execute(sql)
		o_order_list = cr.fetchall()
		for o_order_id in o_order_list:
			line_sql = "select id from sale_order_line where order_id = %d"%o_order_id[0]
			cr.execute(line_sql)
			order_list = cr.fetchall()
			for order in order_list:
				order_line = sale_line_obj.browse(cr,uid,order[0])
				_logger.info(order_line.order_id.order_id)
				_logger.info(order_line.id)
				_logger.info("==== ===== ==== ===")
				if order_line.product_id.id in (13014,9459,42406,42470,3003,39702,85205,85206,6605,85200,85207) and order_line.state=='queued':
					# _logger.info(order_line.order_id.order_id)
					# _logger.info("==== ===== ==== ===")
					check = self.gift_item_available(cr,uid,order_line.product_id.id,1,context)
					#sale_line_obj.write(cr,uid,order[0],{'price_unit':0,'cost_price_unit':0,'state':'assigned'})
					if not check:
						continue
					sale_line_obj.write(cr,uid,order[0],{'price_unit':0,'cost_price_unit':0,'state':'assigned'})
					c1={}
					c1={'lang': 'en_US', 'tz': False, 'uid': uid, 'invoice_state': None, 'procure_method': 'make_to_stock', 'search_default_future': True}
					vals2={}
					vals2={'state':'assigned','product_uom':1, 'origin':order_line.order_id.order_id,
						'company_id':1,'location_id':2035,'location_dest_id':20,'product_id':order_line.product_id.id,
						'name':order[0],'date_expected': datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT),
						'date':datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT),'product_uom_qty':1}
					for i in range(0,int(order_line.product_uom_qty)):
						stock_move_obj.create(cr, uid,vals2, context=c1)
			self.remove_adjustments_gift_coin(cr,uid,o_order_id[0])

		# sql2 = "select id from sale_order where id in ( select distinct order_id from sale_order_line where state='assigned' and product_id in (84014,48070)) and create_date > '2015-11-04 00:00:00'"
		# cr.execute(sql2)
		# sale_list = cr.fetchall()
		for line_item in o_order_list:
			sql3 = "select id from sale_order_line where order_id=%d"%(line_item[0])
			cr.execute(sql3)
			line_order_item = cr.fetchall()
			state_check = True
			for x in line_order_item:
				sol = sale_line_obj.browse(cr,uid,x)
				if sol.state == 'queued':
					state_check = False
			if state_check:
				sale_obj.write(cr,uid,line_item[0],{'state':'picklist','picklist_id':None})
		return

	def remove_adjustments_gift_coin(self, cr, uid, sale_order, context=None):
		order_obj = self.pool.get("sale.order").browse(cr,uid,sale_order)
		adj_obj = self.pool.get("sale.order.adjustment")
		try:	
			adjustments = order_obj.adjustments
			for adjustment in adjustments:
				if adjustment.adjustment_notes == 'Free Discount' and adjustment.adjusment_label == 'other':
					adj_obj.unlink(cr, uid, adjustment.id)
		except:
			pass

	def amazon_gift_wrap(self,cr,uid,context=None):
		sql = "update sale_order set product_giftwrap = True where three_p_channel = 'Amazon' and state not in ('cancel','done') and create_date > '2016-01-24 00:00:00' and create_date < '2016-02-15 00:00:00' Returning order_id"
		cr.execute(sql)
		order_list = cr.fetchall()
		_logger.info("======================")
		_logger.info(order_list)
		_logger.info("======================")
	# def temp_stock_move(self, cr, uid, context=None):
	# 	_logger.info("=========================")
	# 	_logger.info("Game Begins")
	# 	_logger.info("=========================")
	# 	stock_move_obj=self.pool.get('stock.move')
	# 	sql = "select  foo.id,foo.order_id from (select sol.id,sm.name,so.order_id from (select id,order_id from sale_order_line where product_id in (48070) and create_date >'2015-11-04 00:00:00') as sol left join (select distinct name from stock_move where location_id=2035 ) as sm on cast(sol.id as text) = sm.name left join sale_order as so on sol.order_id=so.id) as foo where foo.name is null"
	# 	cr.execute(sql)
	# 	item_list = cr.fetchall()
	# 	#for item in item_list:
	# 	#	c1={'lang': 'en_US', 'tz': False, 'uid': uid, 'invoice_state': None, 'procure_method': 'make_to_stock', 'search_default_future': True}
	# 	#	vals2={}
	# 	#	vals2={'state':'assigned','product_uom':1, 'origin':item[1],
	# 	#		'company_id':1,'location_id':2035,'location_dest_id':20,'product_id':48070,
	# 	#		'name':item[0],'date_expected': datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT),
	# 	#		'date':datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT),'product_uom_qty':1,'queue_stock_bin_id':2035}
	# 	#	stock_move_obj.create(cr, uid,vals2, context=c1)

	# 	sql1 = "select  foo.id,foo.order_id from (select sol.id,sm.name,so.order_id from (select id,order_id from sale_order_line where product_id in (84014) and create_date >'2015-11-04 00:00:00') as sol left join (select distinct name from stock_move where location_id=2035 ) as sm on cast(sol.id as text) = sm.name left join sale_order as so on sol.order_id=so.id) as foo where foo.name is null"
	# 	cr.execute(sql1)
	# 	item_list = cr.fetchall()
	# 	for item in item_list:
	# 		c1={'lang': 'en_US', 'tz': False, 'uid': uid, 'invoice_state': None, 'procure_method': 'make_to_stock', 'search_default_future': True}
	# 		vals4={}
	# 		vals4={'state':'assigned','product_uom':1, 'origin':item[1],
	# 			'company_id':1,'location_id':2035,'location_dest_id':20,'product_id':84014,
	# 			'name':item[0],'date_expected': datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT),
	# 			'date':datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT),'product_uom_qty':1,'queue_stock_bin_id':2035}
	# 		stock_move_obj.create(cr, uid,vals4, context=c1)

	# 	_logger.info("=========================")
	# 	_logger.info("Game Ends")
	# 	_logger.info("=========================")
	# 	return
