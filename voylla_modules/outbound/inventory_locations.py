from openerp.osv import osv, fields
import pdb
from datetime import datetime, timedelta
import openerp.addons.decimal_precision as dp
import ast
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
import urllib2, json
from functools import wraps
import time
import csv
import logging
from openerp.tools.translate import _
_logger = logging.getLogger(__name__)

class inventory_locations(osv.osv):
	_name = 'inventory.locations'
	_columns = {
		'inventory_location_id' : fields.many2one('stock.location','Inventory Location'),
		'channel_id' : fields.many2one('crm.tracking.source','Channel')
	}
