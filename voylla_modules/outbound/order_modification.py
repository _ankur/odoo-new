from openerp import models, fields, api, _
from openerp.osv import fields, osv
import pdb
from openerp import http
import csv
import base64
import re
import json
import time
import psycopg2
import sys
import urllib2
from functools import wraps
from voylla_modules.config import SPREE_TOKEN,SPREE_URL

class sale_order(osv.osv):
	_inherit = "sale.order"

	def retry(ExceptionToCheck, tries=3, delay=2, backoff=2, logger=None):
		def deco_retry(f):
			@wraps(f)
			def f_retry(*args, **kwargs):
				mtries, mdelay = tries, delay
				while mtries > 1:
					try:
						return f(*args, **kwargs)
					except ExceptionToCheck, e:
						msg = "%s, Retrying in %d seconds..." % (str(e), mdelay)
						if logger:
							logger.warning(msg)
						else:
							print msg
						time.sleep(mdelay)
						mtries -= 1
						mdelay *= backoff
				return f(*args, **kwargs)
			return f_retry  # true decorator
		return deco_retry

	@retry(urllib2.HTTPError)
	def update_order_spree(self,cr,SPREE_TOKEN, SPREE_URL, update,context=None):
		headers = {"Authorization": "Token token=%s" %(SPREE_TOKEN)}
		data = json.dumps(update)
		request = urllib2.Request(SPREE_URL, data, headers)
		resp = urllib2.urlopen(request)
		response = resp.read()
		print response

	def update_on_spree(self,cr,uid,ids,context=None):
		order_update_list = []
		for order in self.browse(cr,uid,ids):
			order_hash = {}
			order_amount_details = {}
			order_hash['order_number'] = order.order_id
			order_hash['order_state'] = order.state
			order_hash['order_status'] = order.status
			order_hash['customer_invoice_address_details'] = self.get_customer_details_spree(cr,uid,order.partner_invoice_id.id)
			order_hash['customer_shipping_address_details'] = self.get_customer_details_spree(cr,uid,order.partner_shipping_id.id)
			order_hash['order_line_details'] = self.get_order_line_details(cr,uid,order.id)
			order_hash['payment_details'] = self.get_payment_details(cr,uid,order.id)
			order_hash['adjustment_details'] = self.get_adjustment_details(cr,uid,order.id)
			order_hash['courier_details'] = self.get_courier_details(cr,uid,order.id)
			order_update_list.append(order_hash)
		if order_update_list:
			self.update_order_spree(cr,SPREE_TOKEN,SPREE_URL,order_update_list)

	def get_adjustment_details(self,cr,uid,ids,context=None):
		adjustment_list = []
		for order in self.browse(cr,uid,ids):
			adjustment_hash = {}
			adjustments = order.adjustments
			for adjustment in adjustments:
				adjustment_hash['adjustment_label'] = adjustment.adjusment_label
				adjustment_hash['adjustment_notes'] = adjustment.adjustment_notes
				adjustment_hash['adjustment_amount'] = adjustment.amount
				adjustment_list.append(adjustment_hash)
		return adjustment_list
		
	def get_courier_details(self,cr,uid,ids,context=None):
		courier_details = {}
		for order in self.browse(cr,uid,ids):
			courier_details['courier_name'] = order.courier_name
			courier_details['awb'] = order.awb
			courier_details['dispatched_date'] = order.dispatched_date
		return courier_details	

	def get_payment_details(self,cr,uid,ids,context=None):
		payment_list = []
		for order in self.browse(cr,uid,ids):
			payment_hash = {}
			payments = order.payments
			for payment in payments:
				payment_hash['payment_method'] = payment.payment_method
				payment_hash['payment_reference'] = payment.payment_reference
				payment_hash['payment_amount'] = payment.amount
				payment_list.append(payment_hash)
		return payment_list

	def get_customer_details_spree(self,cr,uid,ids,context=None):
		partner_registry = self.pool.get('res.partner')
		partner_hash = {}
		for partner in partner_registry.browse(cr,uid,ids):
			partner_hash['partner_name'] = partner.name
			partner_hash['partner_city'] = partner.city
			partner_hash['partner_zip'] = partner.zip
			partner_hash['partner_state'] = partner.state_id.name
			partner_hash['partner_country'] = partner.country_id.name
			partner_hash['partner_street'] = partner.street
			partner_hash['partner_street2'] = partner.street2
			partner_hash['partner_phone'] = partner.phone
			partner_hash['partner_email'] = partner.email
			partner_hash['partner_mobile'] = partner.mobile
		return partner_hash

	def get_order_line_details(self,cr,uid,ids,context=None):
		order_lines_list = []
		for order in self.browse(cr,uid,ids):
			order_lines = order.order_line
			for order_line in order_lines:
				order_line_hash = {}
				order_line_hash['ean'] = order_line.product_id.ean13
				order_line_hash['sku'] = order_line.product_id.name_template
				order_line_hash['price_unit'] = order_line.price_unit
				order_line_hash['quantity'] = order_line.product_uom_qty 
				order_lines_list.append(order_line_hash)
		return order_lines_list		

