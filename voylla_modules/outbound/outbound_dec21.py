from openerp.osv import osv, fields
import pdb
import datetime 
# from time import mktime
# from datetime import datetime
from datetime import datetime, timedelta
import openerp.addons.decimal_precision as dp
import ast
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
import urllib2, json
from functools import wraps
import time
import requests
import csv
import xlwt
import subprocess
import shlex
import os
import urllib
import requests
import urllib2
import logging
from openerp.tools.translate import _
import base64
# import pyautogui
_logger = logging.getLogger(__name__)

def retry(ExceptionToCheck, tries=3, delay=2, backoff=2, logger=None):
    def deco_retry(f):
        @wraps(f)
        def f_retry(*args, **kwargs):
            mtries, mdelay = tries, delay
            while mtries > 1:
                try:
                    return f(*args, **kwargs)
                except ExceptionToCheck, e:
                    msg = "%s, Retrying in %d seconds..." % (str(e), mdelay)
                    if logger:
                        logger.warning(msg)
                    else:
                        print msg
                    time.sleep(mdelay)
                    mtries -= 1
                    mdelay *= backoff
            return f(*args, **kwargs)

        return f_retry  # true decorator

    return deco_retry
    # overriding the mail_mail class
class mail_mail(osv.osv):
	_inherit="mail.mail"
	def create(self,cr,uid,values,context=None):
		if 'is_send' in values:
			return super(mail_mail,self).create(cr,uid,values,context)
		else:
			return 1


class warehouse_picklist(osv.osv):
	_name='warehouse.picklist'

	def correct_payment_method(self, cr, uid, filename, context=None):
		order_obj = self.pool.get("sale.order")
		pay_obj = self.pool.get("sale.order.adjustment")
		pay_hash = {}
		incorrect = []
		not_found = []
		error = []
		with open(filename) as fpay:
			reader = csv.DictReader(fpay)
			for row in reader:
				order_id = row["ThreeP_Order_ID"]
				payment_mode = row["Payment Mode"]
				if payment_mode == "1":
					payment_method = "Gateway::PayuIn"
				elif payment_mode == "2":
					payment_method = "PaypalPayment"
				elif payment_mode == "3":
					payment_method = "CashOnDelivery"
				elif payment_mode == "4":
					payment_method = "Cash"
				elif payment_mode == "5":
					payment_method = "BankTransfer"
				elif payment_mode == "6":
					payment_method = "ThirdPartyPayment"
				else:
					continue
				pay_hash[order_id] = payment_method
			sale_order_ids = order_obj.search(cr, uid, [])
			i = 1
			for sale_order_id in sale_order_ids:
				sale_order = order_obj.browse(cr, uid, sale_order_id)
				order_id = sale_order.order_id
				payments = sale_order.payments
				if len(payments) == 1:
					if order_id in pay_hash:
						if payments.payment_method != pay_hash[order_id]:
							incorrect.append(order_id)
							payments.payment_method = pay_hash[order_id]
							if pay_hash[order_id] == "ThirdPartyPayment":
								payments.payment_reference = order_id
					else:
						not_found.append(order_id)
				else:
					error.append(order_id)
					_logger.info(i)
					i += 1
					_logger.info(incorrect)
					_logger.info(not_found)
					_logger.info(error)


	def change_order_line_status(self, cr, uid, context=None):
		order_line_obj = self.pool.get("sale.order.line")
		order_line_ids = order_line_obj.search(cr, uid, [("state", "=", "queued")])
		for order_line_id in order_line_ids:
			order_line = order_line_obj.browse(cr, uid, order_line_id)
			order_line.state = "confirmed"

	def correct_adjustments(self, cr, uid, context=None):
		f = open("correct_adjustments", "w")
		order_obj = self.pool.get("sale.order")
		sale_order_ids = order_obj.search(cr, uid, [])
		i = 1
		for sale_order_id in sale_order_ids:
			sale_order = order_obj.browse(cr, uid, sale_order_id)
			amount_total = sale_order.amount_total
			amount_payment = 0
			payments = sale_order.payments
			for payment in payments:
				amount_payment += payment.amount
			if amount_total != amount_payment:
				f.write( ",".join([str(sale_order.id), sale_order.name, sale_order.order_id ]) + "\n")
			_logger.info(i)
			i += 1

	def remove_adjustments(self, cr, uid, filename, context=None):
		order_obj = self.pool.get("sale.order")
		adj_obj = self.pool.get("sale.order.adjustment")
		with open(filename) as fadj:
			reader = csv.DictReader(fadj)
			for row in reader:
				sale_id = int(row["sale_id"])
				# order_id = row["order_id"]
				sale_order = order_obj.browse(cr, uid, sale_id)
				amount_total = sale_order.amount_total
				adjustments = sale_order.adjustments
				mis_adj = None
				for adjustment in adjustments:
					if adjustment.adjusment_label == "other" and adjustment.adjustment_notes == "Payment Mismatch":
						mis_adj = adjustment.amount
						mis_adj_id = adjustment.id
						break
				amount_payment = 0
				payments = sale_order.payments
				for payment in payments:
					amount_payment += payment.amount
				if mis_adj is not None:
					if amount_total - mis_adj == amount_payment:
						print sale_id
						adj_obj.unlink(cr, uid, mis_adj_id)

	def remove_adjustments_amazon_orders_fix(self, cr, uid, filename, context=None):
		error = []
		order_obj = self.pool.get("sale.order")
		adj_obj = self.pool.get("sale.order.adjustment")
		reader = [{"sale_id":110522},{"sale_id":112012},{"sale_id":114953},{"sale_id":114961},{"sale_id":115029},{"sale_id":115333},{"sale_id":115507},{"sale_id":116035},{"sale_id":116054},{"sale_id":116737},{"sale_id":124246},{"sale_id":143420},{"sale_id":143580},{"sale_id":143613},{"sale_id":143637},{"sale_id":143712},{"sale_id":143761},{"sale_id":143803},{"sale_id":148466},{"sale_id":152885},{"sale_id":160725},{"sale_id":160901},{"sale_id":161496},{"sale_id":168776},{"sale_id":169156},{"sale_id":169531},{"sale_id":169539},{"sale_id":169566},{"sale_id":169568},{"sale_id":169636},{"sale_id":169653},{"sale_id":169662},{"sale_id":169678},{"sale_id":169707},{"sale_id":169821},{"sale_id":169872},{"sale_id":170355},{"sale_id":170745},{"sale_id":170747},{"sale_id":170758},{"sale_id":170770},{"sale_id":170776},{"sale_id":170791},{"sale_id":170823},{"sale_id":170834},{"sale_id":170878},{"sale_id":171030},{"sale_id":171039},{"sale_id":171057},{"sale_id":171063},{"sale_id":171078},{"sale_id":171083},{"sale_id":171117},{"sale_id":171130},{"sale_id":171831},{"sale_id":172160},{"sale_id":174722},{"sale_id":177500},{"sale_id":180887},{"sale_id":180904},{"sale_id":180937},{"sale_id":180938},{"sale_id":180939},{"sale_id":180947},{"sale_id":180948},{"sale_id":180949},{"sale_id":180950},{"sale_id":180951},{"sale_id":180952},{"sale_id":180955},{"sale_id":180964},{"sale_id":180985},{"sale_id":180986},{"sale_id":181005},{"sale_id":181006},{"sale_id":181016},{"sale_id":181017},{"sale_id":181039},{"sale_id":181049},{"sale_id":181078},{"sale_id":181079},{"sale_id":181088},{"sale_id":181089},{"sale_id":181095},{"sale_id":181115},{"sale_id":181116},{"sale_id":181179},{"sale_id":181186},{"sale_id":181189},{"sale_id":181191},{"sale_id":181194},{"sale_id":181201},{"sale_id":181204},{"sale_id":181205},{"sale_id":181208},{"sale_id":181212},{"sale_id":181213},{"sale_id":181236},{"sale_id":181250},{"sale_id":181258},{"sale_id":181311},{"sale_id":181317},{"sale_id":181319},{"sale_id":181431},{"sale_id":181433},{"sale_id":181483},{"sale_id":181546},{"sale_id":181547},{"sale_id":181549},{"sale_id":181573},{"sale_id":181574},{"sale_id":181599},{"sale_id":181603},{"sale_id":181604},{"sale_id":181606},{"sale_id":181638},{"sale_id":181681},{"sale_id":181705},{"sale_id":181706},{"sale_id":181717},{"sale_id":181718},{"sale_id":181732},{"sale_id":181763},{"sale_id":181781},{"sale_id":181782},{"sale_id":181788},{"sale_id":181789},{"sale_id":181807},{"sale_id":181808},{"sale_id":181821},{"sale_id":181865},{"sale_id":181866},{"sale_id":181867},{"sale_id":181868},{"sale_id":181890},{"sale_id":181891},{"sale_id":181910},{"sale_id":181911},{"sale_id":181943},{"sale_id":181944},{"sale_id":181945},{"sale_id":181970},{"sale_id":181979},{"sale_id":182005},{"sale_id":182020},{"sale_id":182021},{"sale_id":182022},{"sale_id":182035},{"sale_id":182037},{"sale_id":182045},{"sale_id":182072},{"sale_id":182080},{"sale_id":182088},{"sale_id":182226},{"sale_id":182227},{"sale_id":182229},{"sale_id":182233},{"sale_id":182237},{"sale_id":182248},{"sale_id":182277},{"sale_id":182278},{"sale_id":182302},{"sale_id":182327},{"sale_id":182328},{"sale_id":182339},{"sale_id":182358},{"sale_id":182359},{"sale_id":182391},{"sale_id":182392},{"sale_id":182423},{"sale_id":182424},{"sale_id":182425},{"sale_id":182449},{"sale_id":182465},{"sale_id":182469},{"sale_id":182482},{"sale_id":182506},{"sale_id":182507},{"sale_id":182523},{"sale_id":182524},{"sale_id":182525},{"sale_id":182526},{"sale_id":182527},{"sale_id":182553},{"sale_id":182556},{"sale_id":182595},{"sale_id":182601},{"sale_id":182614},{"sale_id":182616},{"sale_id":183077},{"sale_id":186754},{"sale_id":187301},{"sale_id":188661},{"sale_id":189905},{"sale_id":189921},{"sale_id":189944},{"sale_id":190006},{"sale_id":190074},{"sale_id":190083},{"sale_id":190085},{"sale_id":190090},{"sale_id":190102},{"sale_id":190103},{"sale_id":190104},{"sale_id":190155},{"sale_id":190176},{"sale_id":190178},{"sale_id":190187},{"sale_id":190200},{"sale_id":190201},{"sale_id":190202},{"sale_id":190220},{"sale_id":190239},{"sale_id":190253},{"sale_id":190347}]
		for row in reader:
			try:	
				sale_id = int(row["sale_id"])
				sale_order = order_obj.browse(cr, uid, sale_id)
				amount_total = sale_order.amount_total
				adjustments = sale_order.adjustments
				mis_adj = 0
				mis_adj_id = []
				standard_shipping_flag = False
				promotion_flag = False
				for adjustment in adjustments:
				 	mis_adj += float(adjustment.amount)
					if adjustment.adjusment_label == "standard_shipping":
						if standard_shipping_flag == True:
							mis_adj_id.append(adjustment.id)
						standard_shipping_flag = True
					if adjustment.adjusment_label == "promotion": 
						if promotion_flag == True:
							mis_adj_id.append(adjustment.id)
						promotion_flag = True
						# break
				amount_payment = 0
				payments = sale_order.payments
				for payment in payments:
					amount_payment += payment.amount
				if mis_adj is not None:
					if amount_total - mis_adj != amount_payment:
						print sale_id
						adj_obj.unlink(cr, uid, mis_adj_id)
			except:
				error.append(row)
		_logger.info("=====================================")
		_logger.info(error)
		_logger.info("=====================================")

	def change_state(self, cr, uid, context=None):
		order_registry = self.pool.get("sale.order")
		to_change = order_registry.search(cr, uid, [("state","=","queued")])
		for order_id in to_change:
			_logger.info(order_id)
			order_obj = order_registry.browse(cr, uid, order_id)
			order_obj.state = "manual"


	def import_product_taxes(self, cr, uid, csv_filename, context=None):
		product_registry = self.pool.get("product.template")
		tax_registry = self.pool.get("account.tax")
		done = []
		with open(csv_filename) as taxes_lines:
			reader = csv.DictReader(taxes_lines)
			for row in reader:
				product_name = row["Product Name"]
				tax_code = row["Tax"]
				tax_id = tax_registry.search(cr, uid, [("description", "=", tax_code)])
				product_ids = product_registry.search(cr, uid, [("name", "=", product_name)])
				if tax_id and product_ids:
					if product_name in done:
						continue
					product_id = product_ids[0]
					tax = tax_registry.browse(cr, uid, tax_id[0])
					product = product_registry.browse(cr, uid, product_id)
					product.taxes_id = tax
					_logger.info(product_name)
					done.append(product_name)
				else:
					_logger.info( "skipping %s" %(product_name))

	def change_source(self, cr, uid, filename, context=None):
		source_obj = self.pool.get("crm.tracking.source")
		order_obj = self.pool.get('sale.order')
		with open(filename) as fsource:
			reader = csv.DictReader(fsource)
			for row in reader:
				order_id = row["OrderNum"]
				source = row["Source"]
				new_order_id = row["OrderRef"]
				_logger.info(order_id)
				sale_order_id = order_obj.search(cr, uid, [("order_id","=",order_id)])
				if sale_order_id:
					sale_order = order_obj.browse(cr, uid, sale_order_id[0])
					source_id = source_obj.search(cr, uid, [("name","=",source)])
					if not source_id:
						source_id = source_obj.create(cr, uid, {"name":source})
						cr.commit()
					else:
						source_id = source_id[0]
					sale_order.source_id = source_id
					sale_order.order_id = new_order_id


	def create_invoices(self, cr, uid, order_ids, context=None):
		make_invoice_obj = self.pool.get("sale.make.invoice")
		sale_order_obj = self.pool.get("sale.order")
		# all_ids = sale_order_obj.search(cr, uid, [("state", "=", "manual")])
		if not isinstance(order_ids, list):
			order_ids = [order_ids]
		all_ids = sale_order_obj.search(cr, uid, [("id", "in", order_ids), ("state", "=", "manual")])
		ids_list = [all_ids[i:i+10] for i in xrange(0, len(all_ids), 10)]
		for ids in ids_list:
			values = {'grouped': False, 'invoice_date': datetime.now().strftime('%Y-%m-%d')}
			context = {'lang': 'en_US', 'tz': False, 'uid': uid, 'active_model': 'sale.order', 'search_disable_custom_filters': True, 'active_ids': ids, 'active_id': ids[0]}
			make_invoice_obj.make_invoices_1(cr, uid, values, ids, context)
			invoice_confirm_obj = self.pool.get("account.invoice.confirm")
			invoice_obj = self.pool.get("account.invoice")
			invoice_ids = invoice_obj.search(cr, uid, [('type','=','out_invoice'), ("state","=","draft")])
			invoice_confirm_obj.invoice_confirm_1(cr, uid, invoice_ids, context)
			_logger.info(ids)

	def create(self, cr, uid, values, context=None):
		 
		x=self.pool.get('warehouse.picklist').search(cr,uid,[('picklist_name','=',values['picklist_name'])])
		if x:
			y=int(self.browse(cr,uid,x).total_products_remaining)
			if y == 0: 
				self.pool.get('outbound.picklist').write(cr,uid,values['picklist_name'],{'state':'done'})
		else:
			x = super(warehouse_picklist, self).create(cr, uid, values, context=context)
		return x

	def discrepancy_check(self,cr,uid,ids,context=None):
		prod_obj = self.pool.get('product.product')
		temp = {}
		picklist_sale_orders = self.browse(cr,uid,ids).picklist_name.pid
		for sale_order in picklist_sale_orders:
			for sale_order_line in sale_order.order_line:
				product_id = sale_order_line.product_id.id
				product_quantity = sale_order_line.product_uom_qty
				if product_id not in temp.keys():
					temp[product_id] = product_quantity
				else:
					total_quantity = temp[product_id]
					temp[product_id] = total_quantity + product_quantity
		temp2 = {}
		picked_sale_orders = self.browse(cr,uid,ids).picklist_id
		for order in picked_sale_orders:
			ean = order.ean
			picked_product_id = prod_obj.search(cr,uid,[('ean13','=',ean)])
			if picked_product_id not in temp2.keys():
				temp2[picked_product_id[0]] = 1
			else:
				total_quantity = temp2[picked_product_id[0]]
				temp[picked_product_id[0]] = total_quantity + 1

		temp3 = {}
		prod_obj = self.pool.get('product.product')
		z = {key:temp[key]-temp2[key] if key in temp2 else temp[key] for key in temp.keys()}
		for key,value in z.items():
			if int(value) != 0:
				# product_id = prod_obj.search(cr,uid,[('ean13','=',key)])
				sku = prod_obj.browse(cr,uid,key).product_tmpl_id.name
				size1 = prod_obj.get_product_size(cr,uid,product_id)
				size = size1.get('size') 
				if not size:
					size = ''
				temp3[str(sku),size] = value
		if temp3:
			cr.commit()
			raise osv.except_osv(('Info'), ("Following Products are not picked %s"%(temp3)))

	def move_to_outbound1(self, cr, uid, ids,picklist_name,scan,total_products_remaining,total_line_items,context):

		if scan:
			out_inv_obj=self.pool.get('outbound.invoice') 
			vals={}
			stock_move_obj=self.pool.get('stock.move')
			sale_objs=self.pool.get('outbound.picklist').browse(cr,uid,picklist_name).pid
			z=self.search(cr,uid,[('picklist_name','=',picklist_name)])
			for sale in sale_objs:
				if sale.state in ['queued','cancel']:
					sale.picklist_id=None
					continue
				order_line_obj=sale.order_line
				order_line_obj_ids=order_line_obj.ids
				for line_order in order_line_obj:
					is_combo = line_order.product_id.product_tmpl_id.is_combos
					if is_combo:
						all_combos = line_order.product_id.product_tmpl_id.variant_product
						total_qty = 0
						for product in all_combos:
							total_qty = total_qty + int(product.qty * line_order.product_uom_qty)
						if line_order.picked_quantity == total_qty:
							continue	
						for product in all_combos:
							product_obj = product.product
							product_quantity = int(product.qty * line_order.product_uom_qty)
							product_ean=str(product_obj.ean13)
							list_of_ids=stock_move_obj.search(cr,uid,[('product_id','=',product_obj.id),('name','=',line_order.id),('state','=','assigned')])
							for count in range(len(list_of_ids)):
								if product_ean in vals:
									vals[product_ean].append(line_order)
								else:
									vals[product_ean] = []
									vals[product_ean].append(line_order)
					else:
						product_quantity=int(line_order.product_uom_qty)
						if line_order.picked_quantity == product_quantity:
							continue
						product_ean=str(line_order.product_id.ean13)
						list_of_ids=stock_move_obj.search(cr,uid,[('product_id','=',line_order.product_id.id),('name','=',line_order.id),('state','=','assigned')])
						for count in range(len(list_of_ids)):
							if product_ean in vals:
								vals[product_ean].append(line_order)
							else:
								vals[product_ean] = []
								vals[product_ean].append(line_order)				
			scan = str(scan)
			if scan!='Scan QR':
				pbatch='NA'
				pmrp='0'
				psize='0'
				list1=scan.split(",")
				if len(list1) < 5 or list1[4] == '':
					raise osv.except_osv(('Warning!'), ("Ean not found or not in proper format"))
				psku=list1[0]
				ean=list1[4]
				pbatch=list1[3]
				psize=list1[1]
				pmrp=list1[2]
				# length=len(list1)
				# if length > 4:
				# 	ean=out_inv_obj.calculate_ean(cr,uid,ids,psku,psize,context)
				# elif length is 4:
				# 	pbatch=list1[3]
				# 	pmrp=list1[2]
				# 	psize=list1[1]
				# 	ean=out_inv_obj.calculate_ean(cr,uid,ids,psku,psize,context)
				# elif length is 1:
				# 	ean=out_inv_obj.calculate_ean(cr,uid,ids,psku,psize,context)
				# elif length is 2:
				# 	psize=list1[1]
				# 	ean=out_inv_obj.calculate_ean(cr,uid,ids,psku,psize,context)
				if ean in vals:
					stock_move_obj=self.pool.get('stock.move')
					product_id=self.pool.get('product.product').search(cr,uid,[('ean13','=',ean)])
					if not product_id:
						raise osv.except_osv(('Warning!'), ("No EAN assigned to given SKU"))
					sale_order_line_obj=vals[ean][0]
					rorder_id=str(sale_order_line_obj.id)
					sorder_id=str(sale_order_line_obj.order_id.order_id)
					list_of_ids=stock_move_obj.search(cr,uid,[('product_id','in',product_id),('name','=',rorder_id),('state','=','assigned')])
					if len(list_of_ids)==0:
						raise osv.except_osv(('error'),('product id %s, sale_order_line id %s') % (product_id,rorder_id))
					id_done=list_of_ids[0]
					stock_move_obj.action_done(cr,uid,id_done)
					pq=int(sale_order_line_obj.picked_quantity)
					pq=pq+1
					is_combo = sale_order_line_obj.product_id.product_tmpl_id.is_combos
					if is_combo:
						all_combos = sale_order_line_obj.product_id.product_tmpl_id.variant_product
						total_qty = 0
						for product in all_combos:
							total_qty = total_qty + int(product.qty * sale_order_line_obj.product_uom_qty)
					else:
						product_quantity=int(sale_order_line_obj.product_uom_qty)
					self.pool.get("sale.order.line").write(cr, uid, [sale_order_line_obj.id], { 'picked_quantity' :pq })
					awb_sale = self.pool.get('sale.order').browse(cr,uid,[sale_order_line_obj.order_id.id]).order_line
					# product_quantity=int(sale_order_line_obj.product_uom_qty)
					if(sale_order_line_obj.picked_quantity==product_quantity):
						self.pool.get("sale.order.line").write(cr, uid, [sale_order_line_obj.id], { 'state' :'done' })
						check = True
						for line_order1 in awb_sale:
							if (line_order1.state!='done'):
								check = False
						if check:
							self.pool.get("sale.order").write(cr, uid, [sale_order_line_obj.order_id.id], { 'state' :'manual' })
							sale=self.pool.get('sale.order')
							sale.get_awb(cr,uid,[sale_order_line_obj.order_id.id],context=context)
					vals2={'oid':z[0],'mrp':pmrp,'order_id':sorder_id,'ean':ean,'sku':psku,'size':psize,'batch_id':pbatch}		
					self.pool.get('outbound.picked').create(cr,uid,vals2)
					all_obj=self.browse(cr,uid,z[0]).picklist_id
					total_products_remaining=int(total_products_remaining)-1
					self.write(cr,uid,z[0],{'total_products_remaining':total_products_remaining})
					return{'value':
						{'picklist_id':all_obj,'picklist_name':picklist_name,'scan':None,'total_line_items':total_line_items,'total_products_remaining':str(total_products_remaining)}
					}
				else:
					raise osv.except_osv(('Warning!'), ("Product Already Picked or not part of this Picklist "))

	def onchange_picklist_name(self, cr, uid, ids, context):
		if context:
			total_product=0
			sale_objs=self.pool.get('outbound.picklist').browse(cr,uid,context).pid
			for sale in sale_objs:
				order_line_obj=sale.order_line
				for line_order in order_line_obj:
					is_combo = line_order.product_id.product_tmpl_id.is_combos
					if is_combo:
						all_combos = line_order.product_id.product_tmpl_id.variant_product
						for product in all_combos:
							total_product = total_product + int(product.qty * line_order.product_uom_qty)
					else:		
						product_quantity=int(line_order.product_uom_qty)
						total_product=total_product+product_quantity
			vals={'picklist_name':context,'total_line_items':total_product,'total_products_remaining':total_product}		
			x=self.create(cr,uid,vals,context=None)
			y=str(self.browse(cr,uid,x).total_products_remaining)
			all_obj=self.browse(cr,uid,x).picklist_id
			return {'value':{'picklist_id':all_obj,'total_line_items': total_product,'total_products_remaining':y}}

	
	_columns={
		'picklist_name':fields.many2one('outbound.picklist','Picklist Name'),
		'scan':fields.char('Scan Bar Code'),
		'total_line_items':fields.char('Total Products'),
		'total_products_remaining':fields.char('Product Remaining'),
		'picklist_id':fields.one2many('outbound.picked','oid','Picked Products')
	}

class sale_make_invoice(osv.osv):
	_inherit = 'sale.make.invoice'

	def create_invoices_with_queue(self, cr, uid, order_ids, context=None):
		order_ids = context["active_ids"]
		remote_tasks_obj = self.pool.get('remote.tasks')
		for order_id in order_ids:
			remote_tasks_obj.generate_invoice(cr, uid, [order_id])
		return


class outbound_picklist(osv.osv):
	_name = 'outbound.picklist'
	

	def calculate_next_1(self,cr,uid,ids,context=None):
		# ids=ids[0]
		obj = self.browse(cr, uid, ids).sort_pl_id
		list1 = []
		cr.execute("""select id from sort_picklist where (sort_id = %s) ORDER BY assigned_warehouse_name,sku ASC""",(ids))
		list1= cr.fetchall()
		temp = []
		for return_ids in range(0,len(list1)):
			temp.append(list1[return_ids][0])

		return self.pool.get('sort.picklist').browse(cr, uid, temp)	



	def save_picklist(self, cr, uid, ids, context=None):
		sale_obj=self.pool.get('sale.order')
		order=self.pool.get('sale.order').browse(cr,uid,context['active_ids'])
		for sale in order:
			if not sale.state=="picklist":
				check=self.pool.get("outbound.picklist").unlink(cr, uid, context['res_id'], context=None)
				raise osv.except_osv(('Warning!'), ("Order already on Picklist Or Picked"))				
		
		self.pool.get("outbound.picklist").browse(cr, uid, context["res_id"]).name =str((datetime.now() + timedelta(hours=5, minutes=30)).strftime('%Y-%m-%d-%H-%M-%S'))
		for sale in order:
			if sale.state=='picklist':
				 
				sale_obj.write(cr, uid, sale.id, {'picklist_id':context['res_id'],'state' : 'on_picklist'})
			else:
				return False
		x=self.pool.get('sort.picklist')
		y=x.write_sort_id(cr,uid,ids,context)

	_columns={
	'pid' : fields.one2many('sale.order','picklist_id'),
	'name' : fields.char('Picklist Name'),
	'product_id' : fields.one2many('outbound.picked','oid','EAN code'),
	'total_line_items':fields.integer('Total Line Items'),
	'remaining_line_items':fields.integer('Line Items Remaining'),
	'sort_pl_id':fields.one2many('sort.picklist','sort_id'),
	'state':fields.selection([
		('pending', 'Pending'),
		('done', 'Done'),
		],'Status')

	}
	_defaults={
	'state' : 'pending',

	}	

class outbound_picklist_stn(osv.osv):
	_name = 'outbound.picklist.stn'

	def get_excise_file(self,cr,uid,ids,context=None):
		filename = "excise_file.xls"
		values_pass = {}
		book = xlwt.Workbook(encoding="utf-8")
		sheet = book.add_sheet("Excise Info File")
		stock_transfer = self.pool.get('stock.transfer.record')
		return_ids=stock_transfer.create(cr,uid,values_pass)
		name_of_stn = stock_transfer.browse(cr,uid,return_ids).name
		style_0 = xlwt.easyxf('font: name Times New Roman, bold on; align: wrap on,vert centre;')
		sheet.write_merge(0, 2, 0, 3,'Stock Transfer Note:%s \n PAN NO: \n DATE : %s' %(name_of_stn,datetime.now()), style_0)
		sheet.write_merge(3, 8, 0, 3,'FROM: \nVoylla Fashions Pvt Ltd,(Manufacturing) \nE-521, RIICO Industrial Area,\nNear Chatrala Circle, Sitapura\nJaipur, Raj-302022\nPh.No-7676111022', style_0)
		sheet.write_merge(3, 8, 4, 7,'TO:\nVoylla Fashions Pvt Ltd,(Trading)\n E-521, RIICO Industrial Area,\nNear Chatrala Circle, Sitapura\nJaipur, Raj-302022\nPh.No-7676111022', style_0)
		header = ['Sku Code','Base Price','Quantity','Excise Duty',]
		for index,col_name in enumerate(header):
			sheet.write(10,index,col_name)
		picklists = self.pool.get('outbound.picklist').browse(cr,uid,context['active_ids'])
		row_next = 11
		for pick in picklists:
			pickedlist = pick.id
			# row_num +=1  
			sale_order = self.pool.get('sale.order')
			product_product=self.pool.get('product.product')
			product_template= self.pool.get('product.template')
			so_obj1=sale_order.browse(cr,uid,ids)
			stock_move=self.pool.get('stock.move')
			stock_picking=self.pool.get('stock.picking')
			# Manufacturing_products = stock_move.search(cr,uid,[('origin','=',so_obj1.name),('location_dest_id','=',84)])
			
			y = self.pool.get('sale.order').search(cr,uid,[('picklist_id','=',pickedlist)])
			# row_num += 1
			for sale in y:
				sale_order_line_id = self.pool.get('sale.order.line').search(cr,uid,[('order_id','=',sale)])
				# row_next = row_num + 1
				for line in sale_order_line_id:
					# row_next += 1
					# row_next = row_num + 1
					flag = False
					row = []
					sale_line_obj =  self.pool.get('sale.order.line').browse(cr,uid,line)
					assigned_bin = sale_line_obj.assigned_bin.id
					product_name = sale_line_obj.product_id.name
					taxes = sum( x.amount for x in sale_line_obj.tax_id)
					quantity = sale_line_obj.product_uom_qty
					excise_duty = sale_line_obj.calculate_excise_charges_file(context=context['active_id'])
					price_without_tax = sale_line_obj.price_subtotal
					retail_price = price_without_tax
					row.append(product_name)
					row.append(retail_price)
					row.append(quantity)
					row.append(excise_duty)
					for col_num, value in enumerate(row):
						if assigned_bin == 84:
							flag = True
							sheet.write(row_next, col_num, value)
					if flag == True:
						row_next += 1
		sheet.write_merge(row_next, row_next+5, 0, 9,'Declaration:\nWe declare that:\nThe above mentioned goods are being transferred from our primary place of business to additional place of business.That said, the movement of goods between our branches is a stock transfer and not a transaction, hence not liable to any VAT/CST.\nSignature & Seal', style_0)
		book.save(filename)
		upload_command = "s3cmd put " + filename + " s3://voylladb-backups/dumps/purchase_order/"
		upload_command_arguments = shlex.split(upload_command)
		upload_process = subprocess.Popen(upload_command_arguments)
		upload_process.wait()
		make_public_command = "s3cmd setacl s3://voylladb-backups/dumps/purchase_order/" + filename + " --acl-public"
		make_public_command_arguments = shlex.split(make_public_command)
		make_public_process = subprocess.Popen(make_public_command_arguments)
		make_public_process.wait()
		outputfilename = "excise_file.xls"
		url_to_file = "https://s3-ap-southeast-1.amazonaws.com/voylladb-backups/dumps/purchase_order/excise_file.xls"
		# resp = requests.get(url_to_file)
		vals={'name':outputfilename,'type':'url','url':url_to_file}
		attachment_id=int(self.pool.get('ir.attachment').create(cr,uid,vals))
		urllib.urlretrieve(url_to_file,outputfilename)
		return {
		'domain':"[('id','=',%i)]"%attachment_id,
		'actions':'check_action_attachment',
		'view_type': 'form',
		'view_mode': 'tree,form',
		'res_model': 'ir.attachment',
		'view_id': False,
		'type': 'ir.actions.act_window',
		'search_view_id':attachment_id 
		}


class stock_transfer_record(osv.osv):
	_name='stock.transfer.record'
	_columns={
		'reference' : fields.many2one('sale.order', 'SO'),
		'supplier_id' : fields.many2one('res.partner', 'Supplier Name'),
		'name':fields.char('STN No'),
		
}

	def create(self,cr,uid,vals,context=None):
		if vals.get('name','/')=='/':
			vals['name'] = self.pool.get('ir.sequence').get(cr, uid, 'stock.transfer.record') or '/'
		return super(stock_transfer_record, self).create(cr,uid,vals,context)

class sort_picklist(osv.osv):
	_name='sort.picklist'

	def write_sort_id(self,cr,uid,ids,context):
		sort={}
		x=self.pool.get('sale.order').browse(cr,uid,context['active_ids'])
		sort_id=context['res_id']
		for order in x:
			for line in order.order_line:
				if line.state == 'assigned':
					is_combo = line.product_id.product_tmpl_id.is_combos
					if is_combo:
						all_combos = line.product_id.product_tmpl_id.variant_product
						for product in all_combos:
							product_obj = product.product
							attribute_hash = product_obj.get_product_size()
							size=attribute_hash.get('size',False)
							if not size:
								size='-'	
							gold = attribute_hash.get('gold',False)	
							if not gold:
								gold = '-'
							diamond = attribute_hash.get('diamond',False)
							if not diamond:
								diamond = '-'
							sku=str(product_obj.name)
							qty=float(product.qty * line.product_uom_qty)
							try:
								taxon=str(product_obj.product_tmpl_id.taxonomy[0].categ_id.complete_name.split('/')[1])
							except:
								taxon='-'	
							courier_name=str(line.order_id.courier_name)

							if len(line.bins)==0:
								warehouse_name=str(line.assigned_bin.name)
								combined=courier_name+'/'+sku+'/'+taxon+'/'+size+'/'+gold+'/'+diamond+'/'+warehouse_name
								if combined in sort:
									qty=qty+sort[combined]
									sort[combined]=qty
								else:
									sort[combined]=qty
							
							else:
								for bin_name in line.bins:
									if bin_name.product_id.id==product_obj.id:
										warehouse_name=str(bin_name.location_id.name)
										qty=int(bin_name.qty)
										combined=courier_name+'/'+sku+'/'+taxon+'/'+size+'/'+gold+'/'+diamond+'/'+warehouse_name
										if combined in sort:
											qty=qty+sort[combined]
											sort[combined]=qty
										else:
											sort[combined]=qty
					else:			
						attribute_hash = line.product_id.get_product_size()
						size = attribute_hash.get('size',False)
						if not size:
							size = '-'
						gold = attribute_hash.get('gold',False)
						if not gold:
							gold = '-'
						diamond = attribute_hash.get('diamond',False)
						if not diamond:
							diamond = '-'
						sku = str(line.product_id.name)
						# qty = float(line.product_uom_qty)
						try:
							taxon = str(line.product_id.product_tmpl_id.taxonomy[0].categ_id.complete_name.split('/')[1])
						except:
							taxon = '-'	
						courier_name=str(line.order_id.courier_name)
						if len(line.bins)==0:
							warehouse_name=str(line.assigned_bin.name)
							qty=int(line.product_uom_qty)
							combined=courier_name+'/'+sku+'/'+taxon+'/'+size+'/'+gold+'/'+diamond+'/'+warehouse_name
							if combined in sort:
								qty=qty+sort[combined]
								sort[combined]=qty
							else:
								sort[combined]=qty
						
						else:
							for bin_name in line.bins:
								warehouse_name=str(bin_name.location_id.name)
								qty=int(bin_name.qty)
								combined=courier_name+'/'+sku+'/'+taxon+'/'+size+'/'+gold+'/'+diamond+'/'+warehouse_name
								if combined in sort:
									qty=qty+sort[combined]
									sort[combined]=qty
								else:
									sort[combined]=qty
												
		for key in sort.keys():
			list_vals=key.split('/')
			vals={'courier_name':list_vals[0],'sku':list_vals[1],'taxon':list_vals[2],'size':list_vals[3],
					'gold':list_vals[4],'diamond':list_vals[5],'assigned_warehouse_name':list_vals[6],'sort_id':sort_id,'qty':sort[key]}
			obj=self.pool.get('sort.picklist')
			z = obj.create(cr, uid, vals)		

	_columns={
	'sort_id':fields.many2one('outbound.picklist'),
	'qty':fields.float('QTY'),
	'size':fields.char('SIZE'),
	'gold':fields.char('GOLD'),
	'diamond':fields.char('DIAMOND'),
	'taxon':fields.char('Taxon'),
	'sku':fields.char('SKU'),
	'courier_name':fields.char('Courier Name'),
	'assigned_warehouse_name':fields.char('Warehouse Name')
	}	

class outbound_picked(osv.osv):
	_name='outbound.picked'
	
	_columns={
	'oid':fields.many2one('warehouse.picklist','Picked ID'),
	'scan':fields.char('Scan QR'),
	'sku':fields.char('SKU'),
	'mrp':fields.char('MRP'),
	'batch_id':fields.char('Batch ID'),
	'order_id':fields.char('Order ID'),
	'ean':fields.char('EAN'),
	'size':fields.char('Size'),

	}
	_defaults={
	'scan' : 'Scan QR',
	
	
	}

class crm_tracking_source(osv.osv):
	_inherit = "crm.tracking.source"
	_columns = {
	'shipping':fields.selection([
			("standard", "Standard"),
			("3rd_party", "Third party"),
			("both", "Both"),
			],'Shipping Type',required=True),
	}

class sale_order(osv.osv):

	_inherit = "sale.order"


	## Apply OCR to Orders.
	def apply_order_confirmation_rules(self,cr,uid,context=None):
		# Applicable only on 'Voylla Website', 'Voonik', 'Yepme' channel orders
		res_group = self.pool.get('res.groups').search(cr,uid,[('name','=','Automation Order Creation')])[0]
		allowed_channel_ids = self.pool.get("crm.tracking.source").search(cr,uid,['|','|',('name','=','Voylla Website'),('name','=','Voonik'),('name','=','Yepme')])
		allowed_channel_ids = ','.join(str(x) for x in allowed_channel_ids)
		sql="select id from sale_order where create_date > now()-interval'15 minutes' and source_id in ("+allowed_channel_ids+") and note NOT LIKE '%OCR %';"
		cr.execute(sql)
		sale_order_ids = cr.fetchall()
		sale_order_ids = map(lambda x: x[0],sale_order_ids)
		for saleorder in self.browse(cr,uid,sale_order_ids):
			write_user = saleorder.write_uid.groups_id
			if res_group not in write_user.ids:
				continue
			try:
				confirmation_status = self.get_confirmation_status(cr,uid,saleorder)
				if confirmation_status[0] == 'not_confirmed':
					# If courier is not assigned, always mark as not_confirmed.
					if saleorder.courier_name=='False':
						sql="update sale_order set status='not_confirmed', write_uid=1, note=concat(note,' [OCR Message: "+confirmation_status[1] + " . Also courier is not assigned.] ') where id = "+ str(saleorder.id) + ";"
						cr.execute(sql)
					else:
						sql="update sale_order set status='"+confirmation_status[0]+"', write_uid=1, note=concat(note,' [OCR Message: "+confirmation_status[1] + " ] ') where id = "+ str(saleorder.id) + ";"
						cr.execute(sql)
			except Exception, e: # Handle exceptions to prevent blocking of other tasks.
				_logger.info("Error in applying OCR to " + saleorder.name + " ::::" + str(e))
			
	# Returns order confirmation status with OCR Message. Ex: ["not_confirmed", "Applied women > 5000"]
	def get_confirmation_status(self,cr,uid,sale_order):
		order_confirmation_rules_obj = self.pool.get("order.confirmation.rules")
		active_order_confirmation_rules_ids = order_confirmation_rules_obj.search(cr,uid,[('activate','=',True)])
		active_order_confirmation_rules_objs = order_confirmation_rules_obj.browse(cr,uid,active_order_confirmation_rules_ids)
		# Returns on first rule match
		for active_ocr_obj in active_order_confirmation_rules_objs:
			flag_gender = True 
			if active_ocr_obj.channel_ids and sale_order.source_id not in active_ocr_obj.channel_ids:
				continue
			if active_ocr_obj.gender:
				for order_line in sale_order.order_line:
					product_gender = self.get_product_gender(cr,uid,order_line.product_id.id)
					if product_gender:
						if active_ocr_obj.gender != product_gender:
							flag_gender = False
							break
						else:
							continue
					else:
						continue
				if flag_gender == False:
					continue
			if active_ocr_obj.start_time and active_ocr_obj.end_time:
				order_date = datetime.strptime(sale_order.create_date,"%Y-%m-%d %H:%M:%S")
				start_time = datetime.strptime(datetime.strptime(active_ocr_obj.start_time,"%Y-%m-%d %H:%M:%S").strftime(order_date.strftime("%Y")+"-"+order_date.strftime("%m")+"-"+order_date.strftime("%d")+" %H:%M:%S"),"%Y-%m-%d %H:%M:%S")
				end_time = datetime.strptime(datetime.strptime(active_ocr_obj.end_time,"%Y-%m-%d %H:%M:%S").strftime(order_date.strftime("%Y")+"-"+order_date.strftime("%m")+"-"+order_date.strftime("%d")+" %H:%M:%S"),"%Y-%m-%d %H:%M:%S")
				if not start_time <= order_date <= end_time:
					continue
			if active_ocr_obj.order_value and active_ocr_obj.order_value != 0.0:
				if sale_order.amount_total < active_ocr_obj.order_value:
					continue
			#if active_ocr_obj.confirmation_status:
			#	if sale_order.status != active_ocr_obj.confirmation_status.lower().replace(" ","_"):
			#		continue
			if active_ocr_obj.state_ids and sale_order.partner_shipping_id.state_id not in active_ocr_obj.state_ids:
				continue
			all_payment_method = map(lambda x: 'cashondelivery' if x.payment_method.lower() == 'cashondelivery' else 'prepaid',sale_order.payments)
			if active_ocr_obj.payment_type and active_ocr_obj.payment_type not in all_payment_method:
				continue
			return ["not_confirmed", "Applied " + active_ocr_obj.name]
		# If none rule fulfilled, order is confirmed. 
		return ["confirmed", "No Order Confirmation Rule Matched"]

	def check_and_payment_method(self,cr,uid,context=None):
		self.apply_order_confirmation_rules(cr,uid,context) # Apply Order Confirmation Rules
		sql_one="update sale_order set payment_flag=False where payment_flag"
		sql_two="update sale_order as so set status='not_confirmed',payment_flag = True from ( select s1.id,s1.order_id,s1.sum as order_total ,s2.sum as payment_total,s1.three_p_channel from (select order_id ,sum(amount) from sale_order_payment group by order_id) as s2 ,(select id,order_id ,three_p_channel,sum(amount_total) from sale_order where create_date > now() - interval'7 days' and dispatched_date is null group by id,order_id,three_p_channel) as s1 where s1.id = s2.order_id and s1.sum != s2.sum ) as test where  so.id = test.id and so.three_p_channel not like '%Amazon%'"
		sql_three ="update sale_order set status='not_confirmed',payment_flag=True where id in (select so.id from sale_order as so left join sale_order_payment as sop on so.id = sop.order_id  where so.create_date > now() - interval'7 days'and dispatched_date is null and sop.id is null and so.three_p_channel not like '%Amazon%')"
		cr.execute(sql_one)
		cr.execute(sql_two)
		cr.execute(sql_three)

	def cancel_order_action(self, cr, uid, ids, context=None):
		record = self.browse(cr, uid, ids[0], context=context)
		models_data = self.pool.get('ir.model.data')
		dummy, form_view = models_data.get_object_reference(cr, uid, 'outbound', 'view_form_reason_cancel')
		return {
			'name': 'Reason Form',
        	'view_type': 'form',
        	'view_mode': 'form',
        	'view_id':False,
        	'views': [(form_view or False, 'form')],
        	'res_model': 'sale.order',
        	'res_id':record.id,
        	'target':'new',
        	'type': 'ir.actions.act_window',
        }


	def get_cod_amount_to_be_collected(self,cr,uid,ids,context=None):
		for payment in self.browse(cr,uid,ids).payments:
			if payment.payment_method.lower() == "cashondelivery":
				amount = float(payment.amount)
				return  "%.2f"%float(amount)
		return "%.2f"%float(0)		

	def get_country(self,cr,uid,ids,context=None):
		return (self.browse(cr,uid,ids).partner_shipping_id.country_id.name).lower()

	def is_website_order(self,cr,uid,sale_id,context=None):
		three_p_channel = self.pool.get('sale.order').browse(cr,uid,sale_id).source_id
		if not three_p_channel:
			return False
		if three_p_channel.name != 'Voylla Website':
			return False
		return True

	def get_courier_number(self,cr,uid,ids,context=None):
		courier_list_registry = self.pool.get('courier.list')
		courier_name = self.browse(cr,uid,ids).courier_name
		courier_no_id = courier_list_registry.search(cr,uid,[('name','=',courier_name)])
		courier_number = courier_list_registry.browse(cr,uid,courier_no_id).courier_number
		if courier_number:
			return courier_number
		return None	
		
	

	def bogo_adjustment_calculator(self, cr, uid, order_ids, context=None):
		product_obj = self.pool.get('product.product')
		adjustment_obj = self.pool.get('sale.order.adjustment')
		sale_orders = self.browse(cr, uid, order_ids, context)


		for sale_order in sale_orders:
			order_bogo_tags = {}
			adjustments = sale_order.adjustments
			bogo_adjustments = [adjustment.id for adjustment in adjustments if adjustment.adjusment_label == "bogo"]
			source_id = sale_order.source_id
			if source_id is not None:
				source_name = source_id.name
				if source_name != "Voylla Website":
					return

			order_lines = sale_order.order_line
			for line in order_lines:
				product_id = line.product_id.id
				bogo_tags = product_obj.get_product_bogo_tag(cr, uid, product_id)
				quantity = line.product_uom_qty
				for bogo_tag in bogo_tags:
					if bogo_tag in order_bogo_tags:
						while quantity > 0:
							order_bogo_tags[bogo_tag].append(line.price_unit)
							quantity -= 1
					else:
						order_bogo_tags[bogo_tag] = []
						while quantity > 0:
							order_bogo_tags[bogo_tag].append(line.price_unit)
							quantity -= 1


			if bogo_adjustments:
				adjustment_obj.unlink(cr, uid, bogo_adjustments)

			for bogo_tag,prices in order_bogo_tags.items():
				prices = sorted(prices, reverse=True)
				if len(prices) == 0:
					continue
				else:
					for index, price in enumerate(prices):
						if index == len(prices) - 1 and len(prices) % 2 != 0:
							continue
						elif index %2 == 0:
							continue
						else:
							adjustment_obj.create(cr, uid, {"adjusment_label":"bogo", "adjustment_notes":bogo_tag, "amount":(0-price), "order_id":sale_order.id}, context)

	def get_subtotal(self,cr,uid,ids,context=None):
		order_obj = self.pool.get('sale.order')
		order_lines = order_obj.browse(cr,uid,ids).order_line
		price = 0

		for line in order_lines:
			price = price + (line.price_unit * line.product_uom_qty)
		return price


	def get_product_gender(self,cr,uid,pid):
		product_obj = self.pool.get("product.product")
		product_template_obj = self.pool.get("product.template")
		product = product_obj.browse(cr,uid,[pid])
		try:
			product_template = product_template_obj.browse(cr,uid,[product.product_tmpl_id.id])
			if product_template:
				return product_template.gender
			else:
				return False
		except Exception, e:
			_logger.info("Error in outbound get_product_gender method ::::" + str(e))
			return False

	def create(self, cr, uid, values, context=None):
		sales_courier_registry = self.pool.get('sales.courier')
		partner_shipping_id = values.get('partner_shipping_id',False)
		res_partner_registry = self.pool.get('res.partner')
		india_id = self.pool.get('res.country').search(cr,uid,[('name','=','India')])[0]
		notes = values['note']
		note = ''
		if partner_shipping_id:
			partner_shipping_obj = res_partner_registry.browse(cr,uid,partner_shipping_id)
			if partner_shipping_obj.country_id.id == india_id:
				partner_zip_code = partner_shipping_obj.zip
				partner_zip_code = partner_zip_code.replace(" ", "")
				partner_state = partner_shipping_obj.state_id.id
				sales_courier = sales_courier_registry.search(cr,uid,[('pin_code','=',partner_zip_code),('state','=',partner_state)])
				if not sales_courier:
					note =  note + ' State and pin code does not match ' 
					values['status']='not_confirmed'
		duplicate_order_ids = self.search(cr,uid,[('partner_id','=',values['partner_id']),('state','not in',('done','cancel','partial_return','returned','rto'))])
		source_registry = self.pool.get('crm.tracking.source')
		source_id = source_registry.search(cr,uid,[('id','=',values['source_id'])])
		source_name = source_registry.browse(cr,uid,source_id).name
		if duplicate_order_ids and source_name == 'Voylla Website':
			note = note + ' Duplicate Order '+str([self.browse(cr,uid,ids).name for ids in duplicate_order_ids])
			values['is_duplicate']=True
		if notes:
			values['note'] = notes + note
		else:
			values['note']= note
		x = super(sale_order, self).create(cr, uid, values, context=context)
		if 'order_line' in values:
			self.update_shipping(cr,uid,x,context)
			self.update_payments(cr,uid,x,context)
		amount_total = self.browse(cr,uid,x).amount_total
		if amount_total > 9999 and source_name == 'Voylla Website':
			payment_method = self.get_payment_method(cr,uid,x)
			if payment_method == 'cashondelivery':
				notes = self.browse(cr,uid,x).note
				if notes:
					notes = notes + ' Amount Total greater than Rs. 9999 '
				else:
					notes = ' Amount Total greater than Rs. 9999 '	
				self.write(cr,uid,x,{'status':'not_confirmed','note':notes})
		return x


	def write(self, cr, uid,ids, values, context=None):
		partner_id = values.get('partner_id',False)
		amount_total = values.get('amount_total',False)
		if partner_id:
			sale_obj = self.browse(cr,uid,ids)
			duplicate_order_ids = self.search(cr,uid,[('partner_id','=',values['partner_id']),('state','not in',('done','cancel','partial_return','returned','rto'))])
			source_registry = self.pool.get('crm.tracking.source')
			source_id = source_registry.search(cr,uid,[('id','=',sale_obj.source_id.id)])
			source_name = source_registry.browse(cr,uid,source_id).name
			if duplicate_order_ids and source_name == 'Voylla Website':
				note = map(str,[self.browse(cr,uid,duplicate_order_id).name for duplicate_order_id in duplicate_order_ids])
				values['is_duplicate']=True
				notes = values.get('note',False)
				if notes:
					values['note'] = notes + ' Duplicate Orders' + str(note) + '|'
				else:
					values['note'] = ' Duplicate Orders ' + str(note) + '|'
		if amount_total > 9999 and source_name == 'Voylla Website':
			payment_method = self.get_payment_method(cr,uid,ids)
			if payment_method == 'cashondelivery':
				values['status'] = 'not_confirmed'
		x = super(sale_order, self).write(cr, uid,ids, values, context=context)		
		if 'order_line' in values or 'shipping_type' in  values:
			self.update_shipping(cr,uid,ids,context)
		return x

	def update_payments(self,cr,uid,ids,context=None):
		sale_order_payment_reg = self.pool.get('sale.order.payment')
		payments =  self.browse(cr,uid,ids).payments
		if payments:
			return
		for sale in self.browse(cr,uid,ids):
			vals= {'order_id':sale.id,'amount':sale.amount_total,'payment_method':'CashOnDelivery'}
			sale_order_payment_reg.create(cr,uid,vals,context=context)

	def voylla_shipping_mapping(self,cr,uid,ids,source_id,context=None):
		if source_id is not False:	
			channel_map = self.pool.get('crm.tracking.source')
			sale_order = self.pool.get('sale.order')
			voylla_shipping	= channel_map.browse(cr, uid, source_id).shipping
			if voylla_shipping == 'standard':
				shipping = 'standard'
			elif voylla_shipping == '3rd_party':
				shipping = '3rd_party'
				# shipping = sale_order.browse(cr, uid, ids).shipping_type
			else:
				shipping = 'standard'
			shipping = str(shipping)
			return{'value':{'shipping_type':shipping}}
		else:
			return{'value':{'shipping_type':'standard'}}	

	def get_date(self, cr, uid, ids, fields,context=None):
		for sale in self.pool.get('sale.order').browse(cr,uid,ids):
			date_order = (datetime.strptime(sale.date_order,'%Y-%m-%d %H:%M:%S')) + timedelta(hours=5,minutes=30)
			# date_order = (datetime.strptime(sale.date_order,'%Y-%m-%d')) change the date format 
			date_order = date_order.strftime('%d/%m/%y')
			return date_order


	def get_amount(self, cr, uid, ids, context=None):
		for payment in self.browse(cr,uid,ids).payments:
			if payment.payment_method.lower() == "cashondelivery":
				return int(round(payment.amount/1.05))

	def get_amount_bnpl(self,cr,uid,ids,context=None):
		for payment in self.browse(cr,uid,ids).payments:
			if payment.payment_method.lower() == "cashondelivery":
				return int(round(payment.amount))

	def get_in_words(self, cr, uid, ids, context=None):
		from num2words import num2words
		amount = self.get_amount(cr, uid, ids)
		amount_in_words = num2words(amount, lang = "en_IN")
		return amount_in_words.title()

	def update_shipping(self,cr,uid,ids,context=None):
		adjustment_obj = self.pool.get("sale.order.adjustment")
		sale_order = self.browse(cr,uid,ids)
		for sale_order_obj in sale_order:
			if sale_order_obj.source_id.name=='Voylla Website':
				if sale_order_obj.user1_country=='India':
					if sale_order_obj.shipping_type=='express':
						for adjustment in sale_order_obj.adjustments:
							if adjustment.adjusment_label in ['standard_shipping','express_shipping']:
								adjustment_obj.unlink(cr, uid, adjustment.id)
						sale_order.adjustments= [[0, False, {'amount': 150.00, 'adjustment_notes': 'Express Shipping', 'adjusment_label': 'express_shipping'}]]
						continue
					else:
						for adjustment in sale_order_obj.adjustments:
							if adjustment.adjusment_label in ['standard_shipping','express_shipping']:
								adjustment_obj.unlink(cr, uid, adjustment.id)

					if sale_order_obj.amount_total > 500:
						for adjustment in sale_order_obj.adjustments:
							if adjustment.adjusment_label in ['standard_shipping']:
								if sale_order_obj.amount_total - adjustment.amount > 500:
									adjustment_obj.unlink(cr, uid, adjustment.id)
									break
					else:
						cod_flag=True
						for adjustment in sale_order_obj.adjustments:
							if adjustment.adjusment_label in ['standard_shipping']:
								cod_flag=False
								break
						if cod_flag:
							# vals={'payments': [[0, False, {'payment_method': 'CashOnDelivery', 'payment_reference': 'NA', 'amount': 50}]]}
							sale_order_obj.adjustments=[[0, False, {'adjustment_notes': 'CashOnDelivery', 'adjusment_label': 'standard_shipping', 'amount': 50}]]
				else:
					shipping_type=sale_order_obj.shipping_type+'_shipping'
					standard_shipping_flag=False
					express_shipping_flag=False
					standard_shipping_adjustment_id=[]
					express_shipping_adjustment_id=[]
					for adjustment in sale_order_obj.adjustments:
						if adjustment.adjusment_label=='standard_shipping':
							standard_shipping_flag=True
							standard_shipping_adjustment_id.append(adjustment.id)
						elif adjustment.adjusment_label=='express_shipping':
							express_shipping_flag=True
							express_shipping_adjustment_id.append(adjustment.id)
					amount=0
					if sale_order_obj.shipping_type=='express':
						
						if express_shipping_flag==False:
							amount=1200
							sale_order.adjustments= [[0, False, {'amount': amount, 'adjustment_notes': shipping_type, 'adjusment_label': shipping_type}]]
							if standard_shipping_flag:
								adjustment_obj.unlink(cr, uid,standard_shipping_adjustment_id[0])
						
					else:
						if standard_shipping_flag==False:
							amount=500
							sale_order.adjustments= [[0, False, {'amount': amount, 'adjustment_notes': shipping_type, 'adjusment_label': shipping_type}]]
							if express_shipping_flag:
								adjustment_obj.unlink(cr, uid,express_shipping_adjustment_id[0])

    
	def get_payment_method(self, cr, uid, ids, context=None):
		sop_obj = self.browse(cr,uid,ids).payments
		for obj in sop_obj:
			if obj.payment_method.lower() == "cashondelivery":
				return "cashondelivery"
		return "prepaid"
	def calculate_adjust(self, cr, uid , ids, context=None):
		temp={}		
		
		adjustment_objs=self.pool.get('sale.order').browse(cr,uid,ids).adjustments
		for adjustment in adjustment_objs:		
			if adjustment.adjusment_label == "standard_shipping":
				return  "%.2f"%float(adjustment.amount)
			elif adjustment.adjusment_label == "express_shipping":
				return  "%.2f"%float(adjustment.amount)	
		return None

	def calculate_cashin(self,cr,uid,ids, context=None):
		temp = {}

		cashin_obj = self.pool.get('sale.order').browse(cr,uid,ids).adjustments
		for adjustment in cashin_obj:
			if adjustment.adjusment_label == "cashinaccount":
				return "%.2f"%float(adjustment.amount)
			
		return None

	def get_pay_method(self, cr, uid,ids,context=None):
		pay_obj = self.browse(cr,uid,ids).payments
		for obj in pay_obj:
			if obj.payment_method.lower() == "cashondelivery":
				return "Cash On Delivery"
			return "Prepaid"

	def get_cod_method(self,cr,uid,ids,context=None):
		pay_cod = self.browse(cr,uid,ids).payments
		for obj in pay_cod:
			if obj.payment_method.lower() == "cashondelivery":
				return "Cash On Delivery"
			

	def get_3p_channel(self, cr, uid,ids, context=None):
		threep = self.pool.get('sale.order').browse(cr,uid,ids)
		source_id = str(threep.source_id.name)
		if source_id=='False':
			return "N.A."
		else:
			return source_id

	def get_delivery_estimate_shipment(self,cr,uid,ids,context=None):
		sale_order=self.browse(cr,uid,ids).order_line

		country = self.browse(cr,uid,ids[0]).partner_shipping_id.country_id.name
		shipping_method = self.browse(cr,uid,ids[0]).shipping_type
		courier_obj = self.browse(cr,uid,ids[0]).courier_name
		product_product_registry = self.pool.get('product.product')
		order_date_obj = self.browse(cr,uid,ids).dispatched_date
		order_date_id = datetime.strptime(order_date_obj, '%Y-%m-%d %H:%M:%S')
		order_date_in = order_date_id.strftime('%d/%m/%y')
		order_date = datetime.strptime(order_date_in,'%d/%m/%y')
		if country.lower() =='india' and shipping_method == 'express':
			if today_time <= time_to_compare and today_day != 'Sunday':
				delta = (timedelta(days=0))
				expected = order_date + delta
				expected_time = expected.strftime('%d/%m/%y')
				delta1 = (timedelta(days=1))
				expected2 = order_date + delta1
				expected_time_2 = expected2.strftime('%d/%m/%y')
				return "%s" %(expected_time_2)

			elif today_time >= time_to_compare and today_day == 'Saturday':
				delta = (timedelta(days=0))
				expected = order_date + delta
				expected_time = expected.strftime('%d/%m/%y')
				delta1 = (timedelta(days=2))
				expected2 = order_date + delta1
				expected_time_2 = expected2.strftime('%d/%m/%y')
				return "%s" %(expected_time_2)

			elif today_day == 'Sunday':
				delta = (timedelta(days=0))
				expected = order_date + delta
				expected_time = expected.strftime('%d/%m/%y')
				delta1 = (timedelta(days=2))
				expected2 = order_date + delta1
				expected_time_2 = expected2.strftime('%d/%m/%y')
				return "%s" %(expected_time_2)

			elif today_time >= time_to_compare and today_day != 'Sunday':
				delta = (timedelta(days=0))
				expected = order_date + delta
				expected_time = expected.strftime('%d/%m/%y')
				delta1 = (timedelta(days=2))
				expected2 = order_date + delta1
				expected_time_2 = expected2.strftime('%d/%m/%y')
				return "%s" %(expected_time_2)
			elif today_time >= time_to_compare:
				delta = (timedelta(days=0))
				expected = order_date + delta
				expected_time = expected.strftime('%d/%m/%y')
				delta1 = (timedelta(days=2))
				expected2 = order_date + delta1
				expected_time_2 = expected2.strftime('%d/%m/%y')
				return "%s" %(expected_time_2)

		if country.lower() != 'india' and courier_obj.lower() =='speed post' and shipping_method=='standard':
			delta = (timedelta(days=10))
			expected = order_date + delta
			expected_time = expected.strftime('%d/%m/%y')
			delta1 = (timedelta(days=15))
			expected2 = order_date + delta1
			expected_time_2 = expected2.strftime('%d/%m/%y')
			return "%s to %s" %(expected_time,expected_time_2)
		elif shipping_method =='express':
			delta = (timedelta(days=4))
			expected = order_date + delta
			expected_time = expected.strftime('%d/%m/%y')
			delta1 = (timedelta(days=5))
			expected2 = order_date + delta1
			expected_time_2 = expected2.strftime('%d/%m/%y')
			return "%s to %s" %(expected_time,expected_time_2)
		if country.lower() == 'india':
			delta = (timedelta(days=5))
			expected = order_date + delta
			expected_time = expected.strftime('%d/%m/%y')
			delta1 = (timedelta(days=7))
			expected2 = order_date + delta1
			expected_time_2 = expected2.strftime('%d/%m/%y')
		elif shipping_method =='express':
			delta = (timedelta(days=4))
			expected = order_date + delta
			expected_time = expected.strftime('%d/%m/%y')
			delta1 = (timedelta(days=5))
			expected2 = order_date + delta1
			expected_time_2 = expected2.strftime('%d/%m/%y')
		elif shipping_method =='standard':
			delta = (timedelta(days=5))
			expected = order_date + delta
			expected_time = expected.strftime('%d/%m/%y')
			delta1 = (timedelta(days=7))
			expected2 = order_date + delta1
			expected_time_2 = expected2.strftime('%d/%m/%y')
		elif shipping_method=='3rd_party':
			expected_time = ""
			expected_time_2 = ""
		return "%s to %s" %(expected_time,expected_time_2)

	def get_delivery_estimate(self, cr, uid , ids, context=None):
		sale_order=self.browse(cr,uid,ids).order_line

		country = self.browse(cr,uid,ids[0]).partner_shipping_id.country_id.name
		shipping_method = self.browse(cr,uid,ids[0]).shipping_type
		courier_obj = self.browse(cr,uid,ids[0]).courier_name
		product_product_registry = self.pool.get('product.product')
		order_date_obj = self.browse(cr,uid,ids).date_order
		order_date_id = datetime.strptime(order_date_obj, '%Y-%m-%d %H:%M:%S')
		order_date_in = order_date_id.strftime('%d/%m/%y')
		order_date = datetime.strptime(order_date_in,'%d/%m/%y')
		for product in sale_order:
			product_line = product.product_id.product_tmpl_id
			product_template_obj = self.pool.get('product.template')
		# mod_products_id = product_template_obj.search(cr,uid,[('route_ids','=', 'Make To Order')])
		# mod_products_obj = product_template_obj.browse(cr,uid,mod_products_id)
			lead_time = product_line.seller_ids.delay
			td = datetime.now()
			today_date = str(datetime.now())
			tt = td.time()
			today_time = str(tt)
	 		start="07:30:00"
			time_compare_to = datetime.strptime(start,"%H:%M:%S").time()
			time_to_compare = str(time_compare_to)
			today_day = td.strftime("%A")

			# for sale in self.browse(cr,uid,ids,context=None):
			# order_line_obj=sale.order_line
			mod=False
			# for order_line in order_line_obj:
			product_routes = product_product_registry.browse(cr,uid,product.product_id.id).route_ids
			if product_routes:
				for route in product_routes:
					if route.name == 'Make To Order':
						mod=True
		if mod:
			delta = (timedelta(days=6) - timedelta(days=1) + timedelta(days=lead_time))
			ex = order_date + delta
			expected_time = ex.strftime('%d/%m/%y')
			delta1 = (timedelta(days=6) + timedelta(days=1) + timedelta(days=lead_time))
			ex2 = order_date + delta1
			expected_time_2 = ex2.strftime('%d/%m/%y')
			return "%s to %s" %(expected_time,expected_time_2)



		if country.lower() =='india' and shipping_method == 'express':
			if today_time <= time_to_compare and today_day != 'Sunday':
				delta = (timedelta(days=0))
				expected = order_date + delta
				expected_time = expected.strftime('%d/%m/%y')
				delta1 = (timedelta(days=1))
				expected2 = order_date + delta1
				expected_time_2 = expected2.strftime('%d/%m/%y')
				return "%s" %(expected_time_2)

			elif today_time >= time_to_compare and today_day == 'Saturday':
				delta = (timedelta(days=0))
				expected = order_date + delta
				expected_time = expected.strftime('%d/%m/%y')
				delta1 = (timedelta(days=2))
				expected2 = order_date + delta1
				expected_time_2 = expected2.strftime('%d/%m/%y')
				return "%s" %(expected_time_2)

			elif today_day == 'Sunday':
				delta = (timedelta(days=0))
				expected = order_date + delta
				expected_time = expected.strftime('%d/%m/%y')
				delta1 = (timedelta(days=2))
				expected2 = order_date + delta1
				expected_time_2 = expected2.strftime('%d/%m/%y')
				return "%s" %(expected_time_2)

			elif today_time >= time_to_compare and today_day != 'Sunday':
				delta = (timedelta(days=0))
				expected = order_date + delta
				expected_time = expected.strftime('%d/%m/%y')
				delta1 = (timedelta(days=2))
				expected2 = order_date + delta1
				expected_time_2 = expected2.strftime('%d/%m/%y')
				return "%s" %(expected_time_2)
			elif today_time >= time_to_compare:
				delta = (timedelta(days=0))
				expected = order_date + delta
				expected_time = expected.strftime('%d/%m/%y')
				delta1 = (timedelta(days=2))
				expected2 = order_date + delta1
				expected_time_2 = expected2.strftime('%d/%m/%y')
				return "%s" %(expected_time_2)

		if country.lower() != 'india' and courier_obj.lower() =='speed post' and shipping_method=='standard':
			delta = (timedelta(days=10))
			expected = order_date + delta
			expected_time = expected.strftime('%d/%m/%y')
			delta1 = (timedelta(days=15))
			expected2 = order_date + delta1
			expected_time_2 = expected2.strftime('%d/%m/%y')
			return "%s to %s" %(expected_time,expected_time_2)
		elif shipping_method =='express':
			delta = (timedelta(days=4))
			expected = order_date + delta
			expected_time = expected.strftime('%d/%m/%y')
			delta1 = (timedelta(days=5))
			expected2 = order_date + delta1
			expected_time_2 = expected2.strftime('%d/%m/%y')
			return "%s to %s" %(expected_time,expected_time_2)
		if country.lower() == 'india':
			delta = (timedelta(days=5))
			expected = order_date + delta
			expected_time = expected.strftime('%d/%m/%y')
			delta1 = (timedelta(days=7))
			expected2 = order_date + delta1
			expected_time_2 = expected2.strftime('%d/%m/%y')
		elif shipping_method =='express':
			delta = (timedelta(days=4))
			expected = order_date + delta
			expected_time = expected.strftime('%d/%m/%y')
			delta1 = (timedelta(days=5))
			expected2 = order_date + delta1
			expected_time_2 = expected2.strftime('%d/%m/%y')
		elif shipping_method =='standard':
			delta = (timedelta(days=5))
			expected = order_date + delta
			expected_time = expected.strftime('%d/%m/%y')
			delta1 = (timedelta(days=7))
			expected2 = order_date + delta1
			expected_time_2 = expected2.strftime('%d/%m/%y')
		elif shipping_method=='3rd_party':
			expected_time = ""
			expected_time_2 = ""
		return "%s to %s" %(expected_time,expected_time_2)



	def get_url(self, cr, uid, ids, context=None):
		url = "http://www.voylla.com/sku/"
		return url

	def get_account_url(self, cr, uid, ids, context=None):
		url_account = "http://voylla.com/account/orders"
		return url_account

	def get_order_url(self, cr, uid, ids, context=None):
		url_order = "http://www.voylla.com/orders/"
		return url_order

	def get_tracking_url(self,cr,uid,ids,context=None):
		sale_order=self.browse(cr,uid,ids)
		courier_list_registry = self.pool.get('courier.list')
		courier_name = self.browse(cr,uid,ids).courier_name
		courier_no_id = courier_list_registry.search(cr,uid,[('name','=',courier_name)])
		tracking_url = courier_list_registry.browse(cr,uid,courier_no_id).tracking_url
		if tracking_url:
			original_url = tracking_url.replace("courier_awb_no",str(sale_order.awb))
			return original_url
		return None


	def get_index(self, cr, uid, ids, context=None):

		x = []
		for i,val in enumerate(ids):
			if i %2 == 0:
				if i == len(ids) -1:
					x.append([val])
				else:
					y = []
					y.append(val)
			else:
				y.append(val)
				x.append(y)
		res = []
		for orders in x:
			order_obj = self.browse(cr, uid, orders)
			res.append(order_obj)
		return res


	def check_validity(self,cr,uid,ids,context=None):
		for sale in self.browse(cr,uid,ids):
			if sale.courier_name.lower() == "speed post":
				continue
			else:
				raise osv.except_osv(('Warning!'), ("Assigned Courier is not speed post"))


	def get_awb(self,cr,uid,ids,context):		 
		sale=self.pool.get('sale.order').browse(cr,uid,ids)
		payments = sale.payments
		payment_methods = [payment.payment_method for payment in payments]
		if "CashOnDelivery" in payment_methods:
			pmethod = "CashOnDelivery"
		else:
			pmethod = "Prepaid"
		# pmethod=str(sale.payments.payment_method)
		courier=str(sale.courier_name)
		try:
			if(pmethod=='CashOnDelivery'):
				awb_id=self.pool.get('awb.courier').search(cr,uid,['|',('tod','=','Both'),('tod','=','COD'),('courier_name','=',courier),('order_no','=',None)])
			else:
				awb_id=self.pool.get('awb.courier').search(cr,uid,['|',('tod','=','Both'),('tod','=','Prepaid'),('courier_name','=',courier),('order_no','=',None)])
			awbobj=self.pool.get('awb.courier').browse(cr,uid,awb_id)[0]
			awbno=str(awbobj.awb_no)
			self.pool.get('awb.courier').write(cr,uid,awbobj.id,{'order_no':sale.id})
			self.write(cr, uid, ids, { 'awb' : awbno})
		except:
			pass
			# awbno=str(sale.order_id)
			# self.write(cr, uid, ids, { 'awb' : awbno})
			
	def get_area_code(self,cr,uid,zip_code,courier,tod,context=None):
		sales_cour_reg = self.pool.get('sales.courier')
		sales_cour_id = sales_cour_reg.search(cr,uid,[('courier_name','=',courier),('pin_code','=',zip_code),('type_of_delivery','=',tod)])
		if sales_cour_id:
			sales_cour_obj = sales_cour_reg.browse(cr,uid,sales_cour_id[0])
			if sales_cour_obj.routes:
				return sales_cour_obj.routes

	def action_cancel(self, cr, uid, ids, context=None):
		if context is None:
			context = {}
		sale_order_line_obj = self.pool.get('sale.order.line')
		stock_move_obj=self.pool.get('stock.move')
		account_invoice_obj = self.pool.get('account.invoice')
		for sale in self.browse(cr, uid, ids, context=context):
			for inv in sale.invoice_ids:
				if inv.state not in ('draft', 'cancel'):
					sale_order_line_obj.write(cr, uid, [l.id for l in  sale.order_line],
					{'reason': ''})
					raise osv.except_osv(_('Cannot cancel this sales order!'),_('First cancel all invoices attached to this sales order.'))
				inv.signal_workflow('invoice_cancel')
			sale_order_line_obj.write(cr, uid, [l.id for l in  sale.order_line],
				{'state': 'cancel'})
		self.write(cr, uid, ids, {'state': 'cancel'})
		if sale.shipping_type != "3rd_party":
			if sale.reason !="reason3":
				self.pool.get('sale.order').send_mail_cancel(cr,uid,ids[0],context)
		order_line_objs=sale.order_line
		for order_line in order_line_objs:
			x=order_line.id
			stock_move_ids=stock_move_obj.search(cr,uid,[('name','=',x),('state','=','assigned')])
			if stock_move_ids:
				stock_move_obj.action_cancel(cr,uid,stock_move_ids,context=context)
		return True

	def add_order_line(self, cr, uid, vals, context=None):
		line_item_handle = self.pool.get("sale.order.line")
		lid = line_item_handle.create(cr, uid, vals)
		return lid
		

	def order_confirm(self, cr, uid, ids, context=None):
		 
		order= self.pool.get('sale.order').browse(cr,uid,ids)[0]
		state=str(order.state)
		status=str(order.status)
		if state=='queued':
			self.write(cr, uid, ids, { 'state' : 'queued'})
		elif state !='manual':
			self.write(cr, uid, ids, { 'state' : 'confirmed'})
		else:
			return False
	
		x=self.pool.get('sale.order').read(cr,uid,ids,['order_id','name'])
		 
		order_id=x[0]['order_id']
		name=str(x[0]['name'])
		if not order_id:
			self.pool.get('sale.order').write(cr,uid,ids,{'order_id':name})
		return True

	@retry(Exception, tries=3, delay=1)
	def send_mail(self,cr,uid,ids,context=None,**kwargs):
		sale_order=self.browse(cr,uid,ids)
		# order = self.pool.get('sale.order').browse(cr,uid,ids)[0]
		email_template_obj = self.pool.get('email.template')
		# template_id = email_template_obj.search(cr,uid,[('model_id.model', '=','sale.order')],context=context)
		template_id =  self.pool.get('email.template').search(cr,uid,[('name','=','Email Template Voylla')])
		values = email_template_obj.generate_email(cr, uid, template_id[0], ids, context=None)
		val2 = values.get('body_html')
		values['email_from'] = 'help@voylla.com'
		values['email_to'] = "%s" %(sale_order.partner_id.email)
		values['email_cc'] = "riya@voylla.com"
		values['subject'] = "voylla.com Order Confirmation %s" %(sale_order.order_id)
		values["body_html"] = val2
		"""values['subject'] = subject 
		values['email_to'] = email_to
		values['body_html'] = body_html
		values['body'] = body_html"""
		values['is_send'] = True
		mail_mail_obj = self.pool.get('mail.mail')
		msg_id = mail_mail_obj.create(cr, uid, values, context=context)
		try:
			# if msg_id:
			mail_mail_obj.send(cr, uid, [msg_id], context=context) 
			flag_mail = True
			# return True	
		except:
			if 'mtries' not in kwargs:
				context = dict(context or {}, mail_create_nolog=True)
				self.message_post(cr, uid, ids, body=("Order Confirmation Mail Failed"), context=context)
			else:
				print "Retrying"

	def send_mail_2(self, cr, uid, ids, context=None):
		sale_order=self.browse(cr,uid,ids)
		email_template_obj = self.pool.get('email.template')
		template_id = self.pool.get('email.template').search(cr,uid,[('name','=','Email Template Shipment')])
		values = email_template_obj.generate_email(cr, uid, template_id[0], ids, context=None)
		val3= values.get('body_html')
		values["subject"] = "Voylla.com Shipment Notification %s" %(sale_order.order_id)
		values["email_from"] = "help@voylla.com"
		values["email_to"] = "%s" %(sale_order.partner_id.email)
		values["email_cc"] = "riya@voylla.com"
		values["body_html"] = val3
		email_template_id = email_template_obj.create(cr, uid, values)
		mail_mail_obj = self.pool.get('mail.mail')
		values['is_send'] = True
		msg_id = mail_mail_obj.create(cr, uid, values, context=context)
		mail_mail_obj.send(cr, uid, [msg_id], context=context) 


	def send_mail_cancel(self,cr,uid,ids,context=None):
		sale_order=self.browse(cr,uid,ids)
		# order = self.pool.get('sale.order').browse(cr,uid,ids)[0]
		email_template_obj = self.pool.get('email.template')
		# template_id = email_template_obj.search(cr,uid,[('model_id.model', '=','sale.order')],context=context)
		template_id =  self.pool.get('email.template').search(cr,uid,[('name','=','Order Cancel Email')])
		values = email_template_obj.generate_email(cr, uid, template_id[0], ids, context=None)
		val2 = values.get('body_html')
		values['email_from'] = 'help@voylla.com'
		values['email_to'] = "%s" %(sale_order.partner_id.email)
		values['email_cc'] = "riya@voylla.com"
		values['subject'] = " Voylla.com Cancellation of Order %s" %(sale_order.order_id)
		values["body_html"] = val2
		"""values['subject'] = subject 
		values['email_to'] = email_to
		values['body_html'] = body_html
		values['body'] = body_html"""
		values['is_send'] = True
		mail_mail_obj = self.pool.get('mail.mail')
		msg_id = mail_mail_obj.create(cr, uid, values, context=context)
		# if msg_id:
		mail_mail_obj.send(cr, uid, [msg_id], context=context) 
		# return True	


	def confirm_shipment(self, cr, uid, ids, context=None):
		
		order= self.pool.get('sale.order').browse(cr,uid,ids)[0]
		status=str(order.state)
		shipping_type=str(order.shipping_type)
		if shipping_type != "3rd_party":
			if status == "done":
				self.pool.get('sale.order').send_mail_2(cr,uid,context['template_id'],context)
				return True
			else:
				raise osv.except_osv(('Warning!'), ("Order Status is not Done"))
		
					

	def confirm_order(self, cr, uid, ids, context=None):
		order= self.pool.get('sale.order').browse(cr,uid,ids)[0]
		state=str(order.status)
		if state=='not_confirmed':
			if not str(order.partner_shipping_id.zip) == 'NA':
				self.write(cr, uid, ids, { 'status' : 'confirmed'})
				self.pool.get('sale.order').send_mail(cr,uid,context['template_id'],context)
				return True
			else:
				raise osv.except_osv(('Warning!'), ("Zip Code Not Correctly Set"))

	def check_inventory(self, cr, uid, ids, context=None):
		 
		order= self.pool.get('sale.order').browse(cr,uid,ids)[0]
		state=str(order.state)
		if state=='confirmed':
			self.write(cr, uid, ids, { 'state' : 'picklist','picklist_id':None})		
			return True
		else:
			return False	
		


	def on_hold(self, cr, uid, ids, context=None):
		for order in self.browse(cr,uid,ids):
			self.write(cr,uid,ids,{'prev_state' : order.state})
			self.write(cr, uid, ids, { 'state' : 'hold'})
		return True
	

	def send_sms2(self, cr, uid, ids, context=None):

		sale_order = self.browse(cr,uid,ids)

		username="jagrati@voylla.in"
		password="d3fault03"
		phone_number = "%s" %(sale_order.partner_id.phone)

		smstext = "Your Voylla order no %s has been shipped via %s with tracking no %s. Thanks for shopping with us." %(sale_order.order_id, sale_order.courier_name, sale_order.awb)
		user_agent = 'Mozilla/4.0 (compatible; MSIE 5.5; Windows NT)'
		headers = { 'User-Agent' : user_agent }

		url= "https://api.mVaayoo.com/mvaayooapi/MessageCompose?user=jagrati@voylla.in:d3fault03&senderID=VOYLLA&receipientno="+phone_number+"&dcs=0&msgtxt="+smstext+"&state=4"
		order= self.pool.get('sale.order').browse(cr,uid,ids)[0]
		state =str(order.state)
		s = requests.Session()
		x = s.get(url,data={},headers={ 'User-Agent' : user_agent },verify=False)    


	def send_sms(self,cr,uid,ids,context=None):

		sale_order = self.browse(cr, uid, ids)

		username="jagrati@voylla.in"
		password="d3fault03"
		phone_number = "%s" %(sale_order.partner_id.phone)
		order1 = "%s" %(sale_order.order_id)
		smstext = "We are trying to reach you to confirm your cash on delivery order, with order ID {0} . Please call us at 07676111022 or %2B917676111022 to confirm your order".format(order1)
		user_agent = 'Mozilla/4.0 (compatible; MSIE 5.5; Windows NT)'

		headers = { 'User-Agent' : user_agent }
		# url11=("%s and %s ")
		
		# url = "http://api.mVaayoo.com/mvaayooapi/MessageCompose?user=jagrati@voylla.in:d3fault03&senderID=VOYLLA&receipientno=9826610896&msgtxt=Thanks%20for%20a%20ordering%20withus%20mVaayoo API&state=0"
		url= "https://api.mVaayoo.com/mvaayooapi/MessageCompose?user=jagrati@voylla.in:d3fault03&senderID=VOYLLA&receipientno="+phone_number+"&dcs=0&msgtxt="+smstext+"&state=4"

		order= self.pool.get('sale.order').browse(cr,uid,ids)[0]
		state =str(order.state)
		s = requests.Session()
		x = s.get(url,data={},headers={ 'User-Agent' : user_agent },verify=False)
		# else:
		# 	raise osv.except_osv(('Warning!'), ("order state is not Sales Order"))


		

	def on_release(self, cr, uid, ids, context=None):
		for order in self.browse(cr,uid,ids):
			self.write(cr, uid, ids, { 'state' : order.prev_state})
		return True	

	def	on_new(self, cr, uid, ids,line_ids, context=None):
		x=self.get_inventory(cr,uid,ids,line_ids,context)
		if x:
			self.write(cr, uid, ids, { 'state' : 'picklist','picklist_id':None})
		else:	
			self.write(cr, uid, ids, { 'state' : 'queued'})

	def check_combo_availability(self, cr, uid, line_order, context=None):
		stock_move_obj=self.pool.get('stock.move')
		order_line = self.pool.get('sale.order.line')
		product_product = self.pool.get('product.product')
		location_dest_obj=self.pool.get('stock.location').search(cr,uid,[('complete_name','=','Voylla Locations / Outbound / Outbound / Outbound')])
		location_dest_obj_id=self.pool.get('stock.location').browse(cr,uid,location_dest_obj)[0].id
		bin_registry = self.pool.get('bin.location_qty')
		product_id = []
		for i in line_order.product_id.variant_product:
			product_id.append({'line_product_id':int(i.product.id),'unit':int(i.qty)})
		product_quantity=int(line_order.product_uom_qty)
		queued_flag=False
		count=0
		for item in product_id:
			if len(stock_move_obj.search(cr,uid,[("name",'=',str(line_order.id)),('origin','=',line_order.order_id.order_id),('product_id','=',item["line_product_id"]),('state','!=','cancel')]))==0:
				prod_virtual_quant = order_line.is_item_available_in_qty(cr,uid,int(item["line_product_id"]),product_quantity * int(item["unit"]),context)						#is_item_available_in_qty(self,cr,uid,int(item["line_product_id"]),product_quantity * int(item["unit"]),context=None)
				if prod_virtual_quant:
					count=count+1
					rorder_id=str(line_order.id)
					product_uom = product_product.browse(cr,uid,item["line_product_id"]).product_tmpl_id.uom_id.id
					c1={}
					c1={'lang': 'en_US', 'tz': False, 'uid': uid, 'invoice_state': None, 'procure_method': 'make_to_stock', 'search_default_future': True}
					vals2={}
					for location_qty in prod_virtual_quant:
						vals2={'state':'assigned','product_uom':product_uom, 'origin':line_order.order_id.order_id,
							'company_id':1,'location_id':location_qty[0],'location_dest_id':location_dest_obj_id,'product_id':item["line_product_id"],
							'name':rorder_id,'date_expected': datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT),
							'date':datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT),'product_uom_qty':1,'queue_stock_bin_id':location_qty[2]}
						bin_registry.create(cr,uid,{'product_id':item["line_product_id"],'location_id':location_qty[0],'qty':location_qty[1],'line_id':line_order.id})
						for i in range(0,location_qty[1]):
							stock_move_obj.create(cr, uid,vals2, context=c1)
			else:
				count=count+1
		if len(product_id)==count:
			True
		else:
			False

	def get_inventory(self, cr, uid, ids,line_ids, context=None):
		sale = self.pool.get('sale.order').browse(cr,uid,ids)
		stock_move_obj=self.pool.get('stock.move')
		product_obj = self.pool.get('product.product')
		inventory_location_obj = self.pool.get("inventory.locations")
		sol_registry = self.pool.get('sale.order.line')
		order_source = sale.source_id
		
		location_dest_obj = self.pool.get('stock.location').search(cr,uid,[('complete_name','=','Voylla Locations / Outbound / Outbound / Outbound')])
		location_dest_obj_id = self.pool.get('stock.location').browse(cr,uid,location_dest_obj)[0].id
		if sale.state == "manual":
			return
		sale_line = sale.order_line
		state_check = False
		for line_order in sale_line:
			if line_order.id in line_ids:
				continue
			if line_order.state != 'queued':
				continue
			sale_id=line_order.id
			product_id=line_order.product_id.id
			product_quantity=int(line_order.product_uom_qty)
			product_uom =product_obj.browse(cr,uid,product_id).product_tmpl_id.uom_id.id
			is_combo = product_obj.browse(cr,uid,product_id).product_tmpl_id.is_combos
			if is_combo:
				assign_check = self.check_combo_availability(cr,uid,line_order)
				if not assign_check:
					state_check = True
				continue
			else:
				check = sol_registry.is_item_available_in_qty(cr,uid,product_id,product_quantity,context)
			if check:
				pro_order_id=str(sale.name)
				product_uom =self.pool.get('product.product').browse(cr,uid,product_id).product_tmpl_id.uom_id.id
				for location_qty in check:
					c1={}
					c1={'lang': 'en_US', 'tz': False, 'uid': uid, 'invoice_state': None, 'procure_method': 'make_to_stock', 'search_default_future': True}
					vals2={}
					vals2={'state':'assigned','product_uom':product_uom, 'origin':line_order.order_id.order_id,
						'company_id':1,'location_id':location_qty[0],'location_dest_id':location_dest_obj_id,'product_id':product_id,
						'name':line_order.id,'date_expected': datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT),
						'date':datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT),'product_uom_qty':1,'queue_stock_bin_id':location_qty[2]}
					self.pool.get('bin.location_qty').create(cr,uid,{'product_id':product_id,'location_id':location_qty[0],'qty':location_qty[1],'line_id':line_order.id})
					for i in range(0,location_qty[1]):
						stock_move_obj.create(cr, uid,vals2, context=c1)
				self.pool.get('sale.order.line').write(cr,uid,line_order.id,{'state':'assigned'})
			else:
				state_check = True
		if state_check == True:
			return False
		else:
			return True
			


	def _calculate_3p_channel(self, cr, uid, ids, name, arg, context=None):
		temp = {}
		for sale in self.browse(cr, uid, ids, context=context):
			source_id = sale.source_id
			source_name = str(source_id.name)
			temp[sale.id]=source_name
			return temp

	def onchange_shipping_type(self,cr,uid,ids,shipping_type,context=None):
		if not shipping_type:
			return {'domain':{'preferred_courier':[('id','in',[])]}}
		courier_list_registry = self.pool.get('courier.list')
		crm_tracking_source_registry = self.pool.get('crm.tracking.source')
		zip_code = self.browse(cr,uid,ids)
		sales_courier_registry = self.pool.get('sales.courier')
		res_country_registry = self.pool.get('res.country')
		india_id = res_country_registry.search(cr,uid,[('name','=','India')])
		for order in self.browse(cr,uid,ids):
			TOD = 'Prepaid'
			payment_method = self.get_payment_method(cr,uid,order.id)
			if payment_method=='cashondelivery':
				TOD='COD'
			if shipping_type == 'standard':
				courier_ids = ['Speed Post']
			if shipping_type == 'express':
				courier_ids = ['Fedex']
			if shipping_type == '3rd_party':
				source_name = order.source_id.name
				courier_ids = [source_name]
			
			if shipping_type and not order.partner_shipping_id.country_id:
				return {'domain':{'preferred_courier':[('id','in',[])]}}
			if shipping_type == 'express' and order.partner_shipping_id.country_id.id == india_id[0]:
				priority_courier_ids = courier_list_registry.search(cr,uid,[('priority','=','express')])
				courier_name = [obj.name for obj in courier_list_registry.browse(cr,uid,priority_courier_ids)]
				sale_courier_ids = sales_courier_registry.search(cr,uid,[('pin_code','=',order.partner_shipping_id.zip),('type_of_delivery','=',TOD),('courier_name','in',courier_name)])
				courier_ids = []
				if sale_courier_ids:
					courier_ids = [obj.courier_name for obj in sales_courier_registry.browse(cr,uid,sale_courier_ids) if obj.courier_name not in courier_ids]
			if shipping_type == 'standard' and order.partner_shipping_id.country_id.id == india_id[0]:
				standard_courier_ids = courier_list_registry.search(cr,uid,[('priority','=','standard')])
				courier_name = [obj.name for obj in courier_list_registry.browse(cr,uid,standard_courier_ids)]
				sale_courier_ids = sales_courier_registry.search(cr,uid,[('pin_code','=',order.partner_shipping_id.zip),('type_of_delivery','=',TOD),('courier_name','in',courier_name)])
				courier_ids = []
				if sale_courier_ids:
					courier_ids = [obj.courier_name for obj in sales_courier_registry.browse(cr,uid,sale_courier_ids) if obj.courier_name not in courier_ids]
			
			return {'domain':{'preferred_courier':[('id','in',courier_ids)]}}
	def _assign_routes(self, cr, uid, ids, name, arg, context=None):
		temp = {}
		sales_courier_registry = self.pool.get('sales.courier')
		res_country_registry = self.pool.get('res.country')
		courier_list_registry = self.pool.get('courier.list')
		sales_courier_registry = self.pool.get('sales.courier')
		india_id = res_country_registry.search(cr,uid,[('name','=','India')])
		for order in self.browse(cr,uid,ids):
			TOD = 'Prepaid'
			payment_method = self.get_payment_method(cr,uid,order.id)
			if payment_method=='cashondelivery':
				TOD='COD'
			shipping_type = order.shipping_type
			if shipping_type == '3rd_party':
				try:
					temp[order.id] = order.source_id.name
				except:
					order.status = 'not_confirmed'
					temp[order.id] = 'False'
				return temp	

			domain = self.onchange_shipping_type(cr,uid,ids,shipping_type)
			if order.partner_shipping_id.country_id.id:
				preferred_courier = order.preferred_courier
				# if order.partner_shipping_id.country_id.id != india_id[0]:
				# 	if len(preferred_courier)==0:
				# 		temp[order.id] = "NA"
				# 	else:
				# 		temp[order.id]=preferred_courier.routes
				# 	return temp
			# print 'Hi'
			all_eligible_courier_ids = domain['domain']['preferred_courier'][0][2]
			preferred_courier = order.preferred_courier.name

			if all_eligible_courier_ids:
				courier_ids = sales_courier_registry.search(cr,uid,[('type_of_delivery','=',TOD),('pin_code','=',order.partner_shipping_id.zip),('preference','=','yes'),('courier_name','in',all_eligible_courier_ids)])	
				if courier_ids:
					courier_ids = sales_courier_registry.browse(cr,uid,courier_ids[0]).routes
				else:
					order.status = 'not_confirmed'
					courier_ids='False'
			else:
				order.status = 'not_confirmed'
				courier_ids = 'False'
			temp[order.id] = courier_ids
			return temp

	def _assign_courier(self, cr, uid, ids, name, arg, context=None):
		temp = {}
		sales_courier_registry = self.pool.get('sales.courier')
		res_country_registry = self.pool.get('res.country')
		courier_list_registry = self.pool.get('courier.list')
		sales_courier_registry = self.pool.get('sales.courier')
		india_id = res_country_registry.search(cr,uid,[('name','=','India')])
		for order in self.browse(cr,uid,ids):
			TOD = 'Prepaid'
			payment_method = self.get_payment_method(cr,uid,order.id)
			if payment_method=='cashondelivery':
				TOD='COD'
			shipping_type = order.shipping_type
			if shipping_type == '3rd_party':
				try:
					temp[order.id] = order.source_id.name
				except:
					order.status = 'not_confirmed'
					temp[order.id] = 'False'
				return temp	

			domain = self.onchange_shipping_type(cr,uid,ids,shipping_type)
			if order.partner_shipping_id.country_id.id:
				preferred_courier = order.preferred_courier
				if order.partner_shipping_id.country_id.id != india_id[0]:
					if len(preferred_courier)==0:
						temp[order.id] = domain['domain']['preferred_courier'][0][2][0]
					else:
						temp[order.id]=preferred_courier.name
					return temp
			# print 'Hi'
			all_eligible_courier_ids = domain['domain']['preferred_courier'][0][2]
			preferred_courier = order.preferred_courier.name
			if all_eligible_courier_ids:
				if preferred_courier in all_eligible_courier_ids: 
					courier_name = order.preferred_courier.name
					temp[order.id] = courier_name
					return temp
				else:	
					courier_ids = sales_courier_registry.search(cr,uid,[('type_of_delivery','=',TOD),('pin_code','=',order.partner_shipping_id.zip),('preference','=','yes'),('courier_name','in',all_eligible_courier_ids)])	
				if courier_ids:
					courier_ids = sales_courier_registry.browse(cr,uid,courier_ids[0]).courier_name
				else:
					order.status = 'not_confirmed'
					courier_ids='False'
			else:
				order.status = 'not_confirmed'
				courier_ids = 'False'
			temp[order.id] = courier_ids
			return temp
		
	def onchange_user_email_phone(self, cr, uid, ids,email,phone,context=None):
		res_partner_registry = self.pool.get('res.partner')
		if not email and not phone:
			customer_ids = res_partner_registry.search(cr,uid,[])
			return {'value':{'partner_id':False,'partner_invoice_id': False, 'partner_shipping_id': False,  'payment_term': False, 'fiscal_position': False},'domain':{'partner_id':[('id','in',customer_ids)]}}
		customer_ids = []
		if phone and not email:	
			customer_ids=res_partner_registry.search(cr,uid,[('parent_id','=',None),'|',('phone','ilike',phone),('mobile','ilike',phone)])
		if email and not phone:
			customer_ids=res_partner_registry.search(cr,uid,[('parent_id','=',None),('email','=',email)])
		if email and phone:
			customer_ids=res_partner_registry.search(cr,uid,[('parent_id','=',None),('email','=',email),'|',('phone','ilike',phone),('mobile','ilike',phone)])
		# email_ids=self.pool.get('res.partner').search(cr,uid,[('parent_id','=',None),'|',('phone','like',phone)])
		return{'domain':{'partner_id':[('id', 'in',customer_ids)]}}


	

	def _amount_all_wrapper(self, cr, uid, ids, field_name, arg, context=None):
		""" Wrapper because of direct method passing as parameter for function fields """
		return self._amount_all(cr, uid, ids, field_name, arg, context=context)

	def _get_quantity(self, cr, uid, ids, field_name, arg, context=None):
		for sale in self.browse(cr,uid,ids,context=None):
			temp = {}
			quantity = sale._get_total_quantity()
			temp[sale.id] = quantity
			return temp
		return self._amount_all(cr, uid, ids, field_name, arg, context=context)	


	def _amount_all(self, cr, uid, ids, field_name, arg, context=None):
		cur_obj = self.pool.get('res.currency')
		res = {}
		for order in self.browse(cr, uid, ids, context=context):
			res[order.id] = {
				'amount_untaxed': 0.0,
				'amount_tax': 0.0,
				'amount_total': 0.0,
			}
			val = val1 = val2 = 0.0
			cur = order.pricelist_id.currency_id
			for line in order.order_line:
				val1 += line.price_subtotal
				val += self._amount_line_tax(cr, uid, line, context=context)
			for adjustment in order.adjustments:
				val2 += adjustment.amount
			res[order.id]['amount_tax'] = cur_obj.round(cr, uid, cur, val)
			res[order.id]['amount_untaxed'] = cur_obj.round(cr, uid, cur, val1)
			res[order.id]["amount_adjustment"] = cur_obj.round(cr, uid, cur, val2)
			res[order.id]['amount_total'] = res[order.id]['amount_untaxed'] + res[order.id]['amount_tax'] + res[order.id]["amount_adjustment"]
		return res
				


	def _get_adj_order(self, cr, uid, ids, context=None):
		res = {}
		for adj in self.pool.get("sale.order.adjustment").browse(cr, uid, ids, context=context):
			res[adj.order_id.id] = True
		return res.keys()

	def _get_line_order(self, cr, uid, ids, context=None):
		res = {}
		for line in self.pool.get('sale.order.line').browse(cr, uid, ids, context=context):
			res[line.order_id.id] = True
		return res.keys()

	def _get_sale_order(self, cr, uid, ids, context=None):
		res={}
		for sale in self.pool.get('sale.order').browse(cr,uid,ids,context=context):
			res[sale.id]=True
		return res.keys()
	
	def _get_payment(self, cr, uid, ids, context=None):
		res={}
		for payment in self.pool.get('sale.order.payment').browse(cr,uid,ids,context=context):
			res[payment.order_id.id]=True
			return res.keys()

	def _get_res_id(self, cr, uid, ids, context=None):
		res={}
		for partner in self.pool.get('res.partner').browse(cr,uid,ids,context=context):
			sale_orders = partner.sale_order_ids
			for sale in sale_orders:
				res[sale.id] = True
			return res.keys()


	def _get_total_quantity(self, cr, uid, ids, context=None):
		for sale in self.pool.get('sale.order').browse(cr,uid,ids):
			total_quantity = 0
			sale_order_lines = sale.order_line 
			for order_line in sale_order_lines:
				total_quantity = total_quantity + int(order_line.product_uom_qty)
		return total_quantity


	def get_is_bogo_availed(self, cr, uid, ids, context=None):
		for sale in self.browse(cr,uid,ids,context=None):
			order_line_objs = sale.order_line
			check = True
			total_quantity = 0
			product_obj = self.pool.get('product.product')
			for sale_order_line in order_line_objs:
				product_id = sale_order_line.product_id
				flag = product_id.is_product_bogo()
				if flag:
					total_quantity = total_quantity + int(sale_order_line.product_uom_qty)
			if total_quantity % 2 == 0:
				return True
			return False

	def _get_mod_orders(self, cr, uid, ids, name, arg, context=None):
		product_product_registry = self.pool.get('product.product')
		temp = {}
		for sale in self.browse(cr,uid,ids,context=None):
			order_line_obj=sale.order_line
			mod=False
			for order_line in order_line_obj:
				product_routes = product_product_registry.browse(cr,uid,order_line.product_id.id).route_ids
				if product_routes:
					for route in product_routes:
						if route.name == 'Make To Order':
							mod=True
			temp[sale.id] = mod
			return temp

	def _get_mode_of_payments(self, cr, uid, ids, name, arg, context=None):
		temp = {}
		for sale in self.browse(cr,uid,ids):
			payment_methods = [payment.payment_method for payment in sale.payments]
			if "CashOnDelivery" in payment_methods:
				temp[sale.id] = 'COD'
			else:
				temp[sale.id] = 'Prepaid'
			return temp	


	_columns = {
		'payment_flag':fields.boolean('Payment not there'),
		'prev_state' : fields.char('Previous State'),	
		'order_id' : fields.char('Order Number', select=True),
		'currency':fields.char('Currency'),
		'shipping_type':fields.selection([
			("standard", "Standard"),
			("express", "Express"),
			("3rd_party", "Third party"),
			],'Shipping Type',required=True),
		'user_email':fields.related('partner_id','email',string="Customer Email",type="char",select=True, relation="res.partner" ,readonly=True, states={'draft': [('readonly', False)], 'sent': [('readonly', False)]}, change_default=True,track_visibility='always'),
		'user_partner_phone':fields.related('partner_id','phone',string="Customer Phone",type="char",select=True, relation="res.partner" ,readonly=True, states={'draft': [('readonly', False)], 'sent': [('readonly', False)]}, change_default=True,track_visibility='always'),
		"status" : fields.selection([
			('confirmed', 'Confirmed'),
            ('not_confirmed', 'Not Confirmed')
            ], "Order Status"),
		'product_giftwrap' : fields.boolean('Giftwrap?'),
		'giftwrap_message' : fields.char ('Giftwrap Message'),
		'courier_name':fields.function(_assign_courier, method=True,  string='Assigned Courier',type='char',
			store={
				'sale.order' : (_get_sale_order,['three_p_channel','order_id','preferred_courier','shipping_type'],3),
				'res.partner':(_get_res_id,['zip','state_id'],3),
				'sale.order.payment':(_get_payment,['payment_method'],3),
			}),
		'routes_name':fields.function(_assign_routes, method=True,  string='Assigned Routes',type='char',
			store={
				'sale.order' : (_get_sale_order,['three_p_channel','order_id','preferred_courier','shipping_type'],3),
				'res.partner':(_get_res_id,['zip','state_id'],3),
				'sale.order.payment':(_get_payment,['payment_method'],3),
			}),
		
		'preferred_courier' : fields.many2one('courier.list',' Preferred Courier'),
		'three_p_channel' : fields.function(_calculate_3p_channel, method=True, string='3P Channel',type='char',
			store={
				'sale.order': (_get_sale_order, ['source_id'],3),
				
			}),
		
		'reason': fields.selection([
        	("reason1","Customer Cancellation - Unconfirmed Order"),
        	("reason2","Customer Cancellation - Doesn\'t want Product"),
        	("reason3","Customer Cancellation - Duplicate/Multiple Orders"),
        	("reason4","Customer Cancellation - Order Combination"),
        	("reason5","Market Place - Out of Stock - Inventory Not updated on Market Place"),
        	("reason6","Market Place - Out of Stock - Simultaneous Order"),
        	("reason7","Market Place - Other technical issue"),
        	("reason8","Market Place - Cancellation"),
        	("reason9","Voylla Outbound - Last Item Damaged"),
        	("reason10","Voylla Outbound - Product not found"),
        	("reason11","Warehouse - Incorrect Qty uploaded on system"),
        	("test_order","Test Order"),
        	],'Reason for Order Cancellation'),
		'picklist_id': fields.many2one("outbound.picklist","Picklist Name"),
		'user_city':fields.related('partner_invoice_id','city',string="Invoiced City",type="char",relation="res.partner"),
		'user_zip':fields.related('partner_invoice_id','zip',string="Invoiced City Zip",type="char",relation="res.partner"),
		'user_street':fields.related('partner_invoice_id','street',string="Invoiced Customer Street1",type="char",relation="res.partner"),
		'user_street2':fields.related('partner_invoice_id','street2',string="Invoiced Customer Street2",type="char",relation="res.partner"),
		'user_mobile':fields.related('partner_invoice_id','mobile',string="Invoiced Customer Mobile",type="char",relation="res.partner"),
		'user_phone':fields.related('partner_invoice_id','phone',string="Invoiced Customer Phone",type="char",relation="res.partner"),
		'user_country':fields.related('partner_invoice_id','country_id','name',string="Invoiced Country",type="char",relation="res.partner"),
		'user_state':fields.related('partner_invoice_id','state_id','name',string="Invoiced State",type="char",relation="res.partner"),
		'user_name':fields.related('partner_invoice_id','name',string="Invoiced Customer Name",type="char",relation="res.partner"),
		'user1_city':fields.related('partner_shipping_id','city',string="Shipped City",type="char",relation="res.partner"),
		'user1_zip':fields.related('partner_shipping_id','zip',string="Shipped City Zip",type="char",relation="res.partner"),
		'user1_street':fields.related('partner_shipping_id','street',string="Shipped Address Street1",type="char",relation="res.partner"),
		'user1_street2':fields.related('partner_shipping_id','street2',string="Shipped Address Street2",type="char",relation="res.partner"),
		'user1_mobile':fields.related('partner_shipping_id','mobile',string="Shipped Customer Mobile",type="char",relation="res.partner"),
		'user1_phone':fields.related('partner_shipping_id','phone',string="Shipped Customer Phone",type="char",relation="res.partner"),
		'user1_country':fields.related('partner_shipping_id','country_id','name',string="Shipped Address Country",type="char",relation="res.partner"),
		'user1_state':fields.related('partner_shipping_id','state_id','name',string="Shipped Address State",type="char",relation="res.partner"),
		'user1_name':fields.related('partner_shipping_id','name',string="Shipped Customer Name",type="char",relation="res.partner"),
		'state': fields.selection([
			('new', 'New'),
			('confirmed', 'Confirmed'),
			('not_confirmed', 'Not Confirmed'),
			('picklist', 'Picklist'),
			('on_picklist', 'On Picklist'),
			('queued', 'Queued'),
			('hold', 'Hold'),
			('packed', 'Packed'),
            ('draft', 'Draft Quotation'),
            ('sent', 'Quotation Sent'),
            ('cancel', 'Cancelled'),
            ('waiting_date', 'Waiting Schedule'),
            ('progress', 'Sales Order'),
            ('rto', 'RTO'),
            ('partial_return', 'Partial Returned'),
            ('returned', 'Returned'),
            ('manual', 'Sale to Invoice'),
            ('invoice_except', 'Invoice Exception'),
            ('done', 'Done'),
            ], 'Status', readonly=True, track_visibility='onchange',
            help="Gives the status of the quotation or sales order. \nThe exception status is automatically set when a cancel operation occurs in the processing of a document linked to the sales order. \nThe 'Waiting Schedule' status is set when the invoice is confirmed but waiting for the scheduler to run on the order date.", select=True),
		"adjustments": fields.one2many("sale.order.adjustment", "order_id", "Adjustments", states={'picklist': [('readonly', False)], 'on_picklist': [('readonly', False)], 'queued': [('readonly', False)], 'manual': [('readonly', False)]}),
		"payments": fields.one2many("sale.order.payment", "order_id", "Payments"),
		"amount_adjustment": fields.function(_amount_all_wrapper, digits_compute=dp.get_precision('Account'), string='Total Adjustment Amount',
			store={
				'sale.order.adjustment': (_get_adj_order, ['amount'], 10),
			},
			multi='sums', help="The amount without tax."),

		'amount_untaxed': fields.function(_amount_all_wrapper, digits_compute=dp.get_precision('Account'), string='Untaxed Amount',
			store={
				'sale.order': (lambda self, cr, uid, ids, c={}: ids, ['order_line'], 10),
				'sale.order.line': (_get_line_order, ['price_unit', 'tax_id', 'discount', 'product_uom_qty'], 10),
				'sale.order.adjustment': (_get_adj_order, ['amount'], 10),
			},
			multi='sums', help="The amount without tax.", track_visibility='always'),
		'amount_tax': fields.function(_amount_all_wrapper, digits_compute=dp.get_precision('Account'), string='Taxes',
			store={
				'sale.order': (lambda self, cr, uid, ids, c={}: ids, ['order_line'], 10),
				'sale.order.line': (_get_line_order, ['price_unit', 'tax_id', 'discount', 'product_uom_qty'], 10),
				'sale.order.adjustment': (_get_adj_order, ['amount'], 10),
			},
			multi='sums', help="The tax amount."),
		'amount_total': fields.function(_amount_all_wrapper, digits_compute=dp.get_precision('Account'), string='Total',
			store={
				'sale.order': (lambda self, cr, uid, ids, c={}: ids, ['order_line'], 10),
				'sale.order.line': (_get_line_order, ['price_unit', 'tax_id', 'discount', 'product_uom_qty'], 10),
				'sale.order.adjustment': (_get_adj_order, ['amount'], 10),
			},
			multi='sums', help="The total amount."),
		'awb':fields.char('AWB No.'),
		"assigned_courier":fields.char("Assigned Courier by file upload"),
		'bogo_availed' : fields.boolean("BOGO Availed"),
		'bogo_applicable' : fields.boolean("BOGO Applicable"),
		'is_duplicate' : fields.boolean("Duplicate"),
		'total_quantity' : fields.function(_get_quantity, string='Total Quantity',type='integer',store={
			'sale.order': (lambda self, cr, uid, ids, c={}: ids, ['order_line','order_id'],10),
			'sale.order.line': (_get_line_order, ['price_unit', 'tax_id', 'discount', 'product_uom_qty'], 10)}),
		'is_automated': fields.boolean("Automated"),
		'dispatched_date': fields.datetime("Dispatched Date And Time"),
		'is_mod':fields.function(_get_mod_orders, string='MOD', type='boolean',
			store={
				'sale.order': (lambda self, cr, uid, ids, c={}: ids, ['order_line'], 10),
				'sale.order.line': (_get_line_order, ['price_unit', 'tax_id', 'discount', 'product_uom_qty'], 10),
			},
			),
		'mode_of_payment':fields.function(_get_mode_of_payments,string='Mode Of Payment',type='char',
			store={
				'sale.order':  (lambda self, cr, uid, ids, c={}: ids, ['payments'], 10),
				'sale.order.payment': (_get_payment, ['amount','payment_method'], 10),

			})
			
	}
	_defaults={
		'state': 'draft',
		'status':'confirmed',
		'bogo_applicable' :False,
		'bogo_availed':True,
		'is_duplicate':False,
	}
	_sql_constraints = [
        ('unique_3p_order', 'unique(order_id, source_id)', 'Order already exists with given 3P number')
        ]

		
class threep_shipping_mapping(osv.osv):
	_name = "threep.shipping.mapping"
	_columns = {
	'source_id' :fields.many2one("crm.tracking.source",'Three P Channel')
	}
	_sql_constraints = [
        ('unique source id', 'unique(source_id)', 'Three P Channel already in the list')
        ]

class product_gender(osv.osv):
	_name='product.gender'
	_columns = {
		'name':fields.char('Name',size=64,required=True)
	}
product_gender()

class pending_sale_order(osv.osv):
	def check_sale_order(self, cr, uid, ids,pending_order_ids,location_obj_id,context=None):
		sale_order_line = self.pool.get('sale.order.line')
		sale_order_line.write(cr,uid,ids,{'state':'assigned'})
		self.unlink(cr, uid, [pending_order_ids], context=None)
		sale=sale_order_line.browse(cr,uid,ids).order_id
		line_obj=sale.order_line
		_logger.info("in pending order assigned ::::" + str(ids))
		check=True
		for line_order in line_obj:
			if line_order.state == 'queued' and line_order.id != ids:
				check=False
		if check:
				self.pool.get('sale.order').write(cr,uid,[sale.id],{'state':'picklist','picklist_id':None})
		cr.commit()
	def get_inventory_pending_specific_id_orders(self, cr, uid,order_id,context=None):
		stock_move_obj = self.pool.get('stock.move')
		#sale_ids = self.pool.get('sale.order').search(cr,uid,[('order_id','in',order_id)])
		sale_ids=[273562,273568,273569]
		
		line_orders = self.pool.get('sale.order.line').search(cr,uid,[('order_id','in',sale_ids),('state','=','queued')])
		_logger.info("in looger ::"+ ','.join(str(x) for x in line_orders))
		pending_orders_id=self.pool.get('pending.sale.order').search(cr,uid,[('order_reference','in',line_orders)])
		_logger.info("in pending order ::"+ ','.join(str(x) for x in pending_orders_id))
		pending_orders_obj=self.pool.get('pending.sale.order').browse(cr,uid,pending_orders_id)
		product_obj = self.pool.get('product.product')

		inventory_location_obj = self.pool.get("inventory.locations")

		location_dest_obj=self.pool.get('stock.location').search(cr,uid,[('complete_name','=','Voylla Locations / Outbound / Outbound / Outbound')])
		location_dest_obj_id=self.pool.get('stock.location').browse(cr,uid,location_dest_obj)[0].id
		for pending_order in pending_orders_obj:
			line_order=pending_order.order_reference
			if line_order.state != 'queued':
				continue
			sale = line_order.order_id
			order_source = sale.source_id
			check=True
			product_id=[]
			if line_order.product_id.is_combos==True:
				for i in line_order.product_id.variant_product:
					product_id.append({'line_product_id':int(i.product.id),'unit':int(i.qty)})
			else:
				product_id.append({'line_product_id':int(line_order.product_id.id),'unit':1})
			_logger.info("pending order line item sku" + str(line_order.product_id.name_template))
			_logger.info("pending order order number" + str(sale.order_id))
			product_quantity=int(line_order.product_uom_qty)
			queued_flag=False
			count=0
			for item in product_id:
					if len(stock_move_obj.search(cr,uid,[('name','=',line_order.id),('origin','=',line_order.order_id.order_id),('product_id','=',item["line_product_id"]),('state','!=','cancel')]))==0:
						prod_virtual_quant = self.pool.get('sale.order.line').is_item_available_in_qty(cr,uid,int(item["line_product_id"]),product_quantity * int(item["unit"]),context)						#is_item_available_in_qty(self,cr,uid,int(item["line_product_id"]),product_quantity * int(item["unit"]),context=None)
						if prod_virtual_quant:
							count=count+1
							stock_move_obj=self.pool.get('stock.move')
							rorder_id=str(line_order.id)
							product_uom =self.pool.get('product.product').browse(cr,uid,item["line_product_id"]).product_tmpl_id.uom_id.id
							c1={}
							c1={'lang': 'en_US', 'tz': False, 'uid': uid, 'invoice_state': None, 'procure_method': 'make_to_stock', 'search_default_future': True}
							vals2={}
							for location_qty in prod_virtual_quant:
								vals2={'state':'assigned','product_uom':product_uom, 'origin':line_order.order_id.order_id,
									'company_id':1,'location_id':location_qty[0],'location_dest_id':location_dest_obj_id,'product_id':item["line_product_id"],
									'name':rorder_id,'date_expected': datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT),
									'date':datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT),'product_uom_qty':1,'queue_stock_bin_id':location_qty[2]}
								self.pool.get('bin.location_qty').create(cr,uid,{'product_id':item["line_product_id"],'location_id':location_qty[0],'qty':location_qty[1],'line_id':line_order.id})
								for i in range(0,location_qty[1]):	
									stock_move_obj.create(cr, uid,vals2, context=c1)
						else:
							_logger.info("pending order line item sku not available" + str(line_order.product_id.name_template))
							_logger.info("pending order order number order in queue" + str(sale.order_id))
					else:
						count=count+1
			if len(product_id)==count:
				self.check_sale_order(cr,uid,line_order.id,pending_order.id,line_order.assigned_bin.id,context=context)


	def get_inventory_pending_orders(self, cr, uid, context=None):
		stock_move_obj = self.pool.get('stock.move')
		pending_orders_id=self.pool.get('pending.sale.order').search(cr,uid,[])
		_logger.info("in pending order ::"+ ','.join(str(x) for x in pending_orders_id))
		pending_orders_obj=self.pool.get('pending.sale.order').browse(cr,uid,pending_orders_id)
		product_obj = self.pool.get('product.product')

		inventory_location_obj = self.pool.get("inventory.locations")

		location_dest_obj=self.pool.get('stock.location').search(cr,uid,[('complete_name','=','Voylla Locations / Outbound / Outbound / Outbound')])
		location_dest_obj_id=self.pool.get('stock.location').browse(cr,uid,location_dest_obj)[0].id
		for pending_order in pending_orders_obj:
			line_order=pending_order.order_reference
			if line_order.state != 'queued':
				continue
			sale = line_order.order_id
			order_source = sale.source_id
			check=True
			product_id=[]
			if line_order.product_id.is_combos==True:
				for i in line_order.product_id.variant_product:
					product_id.append({'line_product_id':int(i.product.id),'unit':int(i.qty)})
			else:
				product_id.append({'line_product_id':int(line_order.product_id.id),'unit':1})
			_logger.info("pending order line item sku" + str(line_order.product_id.name_template))
			_logger.info("pending order order number" + str(sale.order_id))
			product_quantity=int(line_order.product_uom_qty)
			queued_flag=False
			count=0
			for item in product_id:
					if len(stock_move_obj.search(cr,uid,[('name','=',line_order.id),('origin','=',line_order.order_id.order_id),('product_id','=',item["line_product_id"]),('state','!=','cancel')]))==0:
						prod_virtual_quant = self.pool.get('sale.order.line').is_item_available_in_qty(cr,uid,int(item["line_product_id"]),product_quantity * int(item["unit"]),context)						#is_item_available_in_qty(self,cr,uid,int(item["line_product_id"]),product_quantity * int(item["unit"]),context=None)
						if prod_virtual_quant:
							count=count+1
							stock_move_obj=self.pool.get('stock.move')
							rorder_id=str(line_order.id)
							product_uom =self.pool.get('product.product').browse(cr,uid,item["line_product_id"]).product_tmpl_id.uom_id.id
							c1={}
							c1={'lang': 'en_US', 'tz': False, 'uid': uid, 'invoice_state': None, 'procure_method': 'make_to_stock', 'search_default_future': True}
							vals2={}
							for location_qty in prod_virtual_quant:
								vals2={'state':'assigned','product_uom':product_uom, 'origin':line_order.order_id.order_id,
									'company_id':1,'location_id':location_qty[0],'location_dest_id':location_dest_obj_id,'product_id':item["line_product_id"],
									'name':rorder_id,'date_expected': datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT),
									'date':datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT),'product_uom_qty':1,'queue_stock_bin_id':location_qty[2]}
								self.pool.get('bin.location_qty').create(cr,uid,{'product_id':item["line_product_id"],'location_id':location_qty[0],'qty':location_qty[1],'line_id':line_order.id})
								for i in range(0,location_qty[1]):	
									stock_move_obj.create(cr, uid,vals2, context=c1)
						else:
							_logger.info("pending order line item sku not available" + str(line_order.product_id.name_template))
							_logger.info("pending order order number order in queue" + str(sale.order_id))
					else:
						count=count+1
			if len(product_id)==count:
				self.check_sale_order(cr,uid,line_order.id,pending_order.id,line_order.assigned_bin.id,context=context)
			
	_name='pending.sale.order'
	_columns={
		'order_reference':fields.many2one('sale.order.line','Pending Orders',ondelete='cascade')
	}

class sale_order_line(osv.osv):

	_inherit = "sale.order.line"
	_columns ={
		'picked_quantity':fields.integer("Picked_Quantity"),
		'packed_quantity':fields.integer("Packed_Quantity"),
		'rto_id':fields.many2one('outbound.rto','RTO'),
		"product_ean": fields.text('EAN', readonly=True, states={'draft': [('readonly', False)]}),
		'order_item_id':fields.char('Order Item ID'),
		'assigned_bin':fields.many2one('stock.location','Assigned Bin'),
	}
	_defaults={
		'picked_quantity':0,
		'packed_quantity':0
	}

	def get_quantity(self, cr, uid, ids, context=None):
		x =int(self.browse(cr,uid,ids).quantity)
		return x
	
	def calculate_excise_charges_file(self,cr,uid,ids,context=None):
		for line in self.browse(cr, uid, ids, context=context):
			excise_id = line.product_id.taxes_id
			flag = False
			for excise_app in excise_id:
				excise_name = excise_app.name
				excise_amount = excise_app.amount
				if excise_name =='Central Excise 6%' or excise_name=='Central Excise 0%':
					flag = True
			if flag == True:
				total_excise = 0
				for excise in excise_id:
					excise_name = excise.name
					excise_amount = excise.amount		
					if excise_name =='Central Excise 6%' or excise_name=='Central Excise 0%':
						unit_price = line.price_subtotal
						base_price = unit_price
						quantity = line.product_uom_qty
						amount = quantity * ((excise_amount) * base_price)
						total_excise += amount
						return total_excise
			else:
				return 0


	def unlink(self, cr, uid, ids, context=None):
		if context is None:
			context = {}
		sale_order_registry = self.pool.get('sale.order')
		sale_order_obj = self.browse(cr,uid,ids[0]).order_id
		"""Allows to delete sales order lines in draft,cancel states"""
		for rec in self.browse(cr, uid, ids, context=context):
			if rec.state not in ['draft', 'confirmed','done','exception','assigned','queued','cancel']:
				raise osv.except_osv(_('Invalid Action!'), _('Cannot delete a sales order line which is in state \'%s\'.') %(rec.state,))
			if rec.order_id.state not in ['picklist','on_picklist','manual','queued']:
				raise osv.except_osv(_('Invalid Action!'), _('Cannot modify a sales order which is in state \'%s\'. Delete the Sales Order.') %(rec.order_id.state,))
			stock_move_obj = self.pool.get('stock.move')
			stock_move_ids=stock_move_obj.search(cr,uid,[('name','=',ids[0]),('state','=','assigned')])
			if stock_move_ids:
				stock_move_obj.action_cancel(cr,uid,stock_move_ids,context=context)
			sale_order_registry.bogo_adjustment_calculator(cr,uid,rec.order_id.id,context=context)
			x = self.pool.get('sale.order').on_new(cr,uid,rec.order_id.id ,ids)
		z = super(sale_order_line, self).unlink(cr, uid, ids, context=context)
		check = True
		for order_line in sale_order_obj.order_line:
			if order_line.state == 'queued':
				check=False
		if check:
			sale_order_registry.write(cr,uid,sale_order_obj.id,{'state':'picklist'})
		return z
	def is_item_available_in_qty(self,cr,uid,product_id,product_quantity,context=None):
		queue_stock_bin = self.pool.get('queue.stock_bin')
		avaiable_bin_ids = queue_stock_bin.search(cr,uid,[('product_id','=',product_id),('is_active','=',True)])
		remaining_qty = product_quantity 
		if len(avaiable_bin_ids)==0:
			return False
		else:
			bin_ids_qty = []
			for product_in_bin in queue_stock_bin.browse(cr,uid,sorted(avaiable_bin_ids)):
				if product_in_bin.available_qty <=0 :
					continue
				if product_in_bin.available_qty >= remaining_qty :
					bin_ids_qty.append([product_in_bin.stock_location.id,remaining_qty,product_in_bin.id])
					remaining_qty=0
					break
				else:
					remaining_qty = remaining_qty - product_in_bin.available_qty
					bin_ids_qty.append([product_in_bin.stock_location.id,product_in_bin.available_qty,product_in_bin.id])
			if remaining_qty <=0 and len(bin_ids_qty) > 0:
				return bin_ids_qty
			else:
				return False

	def inventory_assignment_for_combo(self,cr,uid,product_id,sale_line,sale,context=None):
		sale_order_obj = self.pool.get('sale.order')
		pending_order_obj=self.pool.get('pending.sale.order')
		product_obj = self.pool.get('product.product').browse(cr,uid,product_id)
		all_combo_products = product_obj.product_tmpl_id.variant_product
		products = [product.product.name_template for product in all_combo_products]
		line_item_qty = int(sale_line.product_uom_qty)
		location_dest_obj=self.pool.get('stock.location').search(cr,uid,[('complete_name','=','Voylla Locations / Outbound / Outbound / Outbound')])
		location_dest_obj_id=self.pool.get('stock.location').browse(cr,uid,location_dest_obj)[0].id
		price_check = True
		if sale_line.price_unit < sale_line.cost_price_unit:
			price_check=False
		vals = {'product':product_id, 'cost_price': sale_line.cost_price_unit, 'template_id':product_obj.product_tmpl_id.id,'for_connect_id_to_sol':sale_line.id}
		#self.pool.get('order.line.combo').create(cr,uid,vals,context=None)
		combo_count=0
		notes=''
		for product in all_combo_products:
			product_quantity = int(product.qty * sale_line.product_uom_qty)
			product_id = product.product.id
			mod=False
			product_routes = product.product.route_ids
			if product_routes:
				for route in product_routes:
					if route.name == 'Make To Order':
						mod=True
			is_qty_available = self.is_item_available_in_qty(cr,uid,product_id,product_quantity,context=None)
			check=True
			if not is_qty_available:
				check=False
			if check:
				combo_count=combo_count+1
				pro_order_id=str(sale.name)
				product_uom =self.pool.get('product.product').browse(cr,uid,product_id).product_tmpl_id.uom_id.id
				stock_move_obj=self.pool.get('stock.move')
				origin_name = sale_line.order_id.order_id
				if not origin_name:
					origin_name = sale_line.order_id.name
				for product_bin_qty in is_qty_available:
					c1={}
					c1={'lang': 'en_US', 'tz': False, 'uid': uid, 'invoice_state': None, 'procure_method': 'make_to_stock', 'search_default_future': True}
					vals2={}
					vals2={'state':'assigned','product_uom':product_uom, 'origin':origin_name,
						'company_id':1,'location_id':product_bin_qty[0],'location_dest_id':location_dest_obj_id,'product_id':product_id,
						'name':sale_line.id,'date_expected': datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT),
						'date':datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT),'product_uom_qty':1,'queue_stock_bin_id':product_bin_qty[2]}
					self.pool.get('bin.location_qty').create(cr,uid,{'location_id':product_bin_qty[0],'qty':product_bin_qty[1],'line_id':sale_line.id,'product_id':product_id})
					for i in range(0,product_bin_qty[1]):
						stock_move_obj.create(cr, uid,vals2, context=c1)
			else:
				if mod:
					context.update({'mod':True,'sale_order':sale.id})
					self.pool.get('purchase.order').genrate_purchase_order_for_make_to_order(cr,uid,product_id,product_quantity,context=context)	
				notes = sale_order_obj.browse(cr, uid, sale.id).note
				if not notes:
					notes = ''
				notes = notes + 'COMBO PRODUCT'	+ str(products)
				if sale.bogo_availed:
					notes = notes.replace("BOGO Not Availed","")
				elif sale.browse(cr, uid, sale.id).bogo_applicable:
					notes = notes + "BOGO Not Availed"
				if sale_line.price_unit < sale_line.cost_price_unit:
					notes = notes + "cost is greater than selling price"
				if not sale.is_automated:
					self.pool.get('sale.order').bogo_adjustment_calculator(cr,uid,sale.id,context=context)

		if len(all_combo_products)==0 or len(all_combo_products)!=combo_count :
			sale_line.write({'state':'queued'})
			if not price_check:
				sale.write({ 'status' : 'not_confirmed','state' : 'queued','note' : notes})
			else:
				sale.write({ 'state' : 'queued','note' : notes})
			vals={'order_reference':sale_line.id}
			pending_order_obj.create(cr,uid,vals,context=None)
			return False
		else:
			state_check = True
			sale_order_update_hash={}
			for sol in sale.order_line:
				if sol.state == 'queued':
					state_check = False
			if state_check:
				sale_order_update_hash.update({'state':'picklist','picklist_id':None})
			else:
				sale_order_update_hash.update({'state':'queued'})
			if not price_check:
				sale_order_update_hash.update({'status':'not_confirmed'})
			if notes!='':
				sale_order_update_hash.update({'note':notes})
			sale_line.write({'state':'assigned'})
			sale.write(sale_order_update_hash)
			return True

	def check_inventory_mod(self,cr,uid,location_obj_id,product_id,context=None):
		product_obj = self.pool.get('product.product')
		return product_obj._product_available(cr, uid,[product_id],context={'location':location_obj_id,'compute_child':False})[product_id]['qty_backorder']			
	def check_inventory(self,cr,uid,location_obj_id,product_id,context=None):
		product_obj = self.pool.get('product.product')
		return product_obj._product_available(cr, uid,[product_id],context={'location':location_obj_id,'compute_child':False})[product_id]['virtual_available']			
	def check_combo_inventory(self,cr,uid,line_item_qty,product_id,context=None):
		all_combo_products = self.pool.get('product.product').browse(cr,uid,product_id).product_tmpl_id.variant_product
		products = [product.product.name_template for product in all_combo_products]
		#warehouse_name = ['Good Bin','Manufacturing Bin']
		assigned_bin_qty = []
		number_qty_combo = len(all_combo_products)
		#for name in warehouse_name:
		#	remaining_product_qty=number_qty_combo
		#	location_obj_id = self.pool.get('stock.location').search(cr,uid,[('name','=',name)])[0]
		for product in all_combo_products:
			product_quantity = int(product.qty * line_item_qty)
			product_id = product.product.id
			#prod_virtual_quant = self.check_inventory(cr,uid,location_obj_id,product_id)
			available_bin_qty = self.is_item_available_in_qty(cr,uid,product_id,product_quantity,context)
			#if prod_virtual_quant < product_quantity:
			#	break
			#remaining_product_qty=remaining_product_qty-1
			if available_bin_qty:
				assigned_bin_qty.append(available_bin_qty)
			else:
				return False

		if len(assigned_bin_qty)==0:
			return False
		else:
			return assigned_bin_qty
		return False
	def create(self, cr, uid, values, context=None):
		if not context:
			context = {}
		if values.get('order_id') and values.get('product_id') and  any(f not in values for f in ['name', 'price_unit', 'type', 'product_uom_qty', 'product_uom']):
			order = self.pool['sale.order'].read(cr, uid, values['order_id'], ['pricelist_id', 'partner_id', 'date_order', 'fiscal_position'], context=context)
			defaults = self.product_id_change(cr, uid, [], order['pricelist_id'][0], values['product_id'],
				qty=float(values.get('product_uom_qty', False)),
				uom=values.get('product_uom', False),
				qty_uos=float(values.get('product_uos_qty', False)),
				uos=values.get('product_uos', False),
				name=values.get('name', False),
				partner_id=order['partner_id'][0],
				date_order=order['date_order'],
				fiscal_position=order['fiscal_position'][0] if order['fiscal_position'] else False,
				flag=False,  # Force name update
				context=context
			)['value']
			if defaults.get('tax_id'):
				defaults['tax_id'] = [[6, 0, defaults['tax_id']]]
			values = dict(defaults, **values)
		sale_obj = self.pool.get("sale.order").browse(cr, uid, values['order_id'])
		if sale_obj.state == 'done':
			raise osv.except_osv(_('Invalid Action!'), _('Cannot modify a sales order which is in state \'%s\'.')%sale_obj.state)
		product = self.pool.get('product.product').browse(cr,uid,values['product_id'])
		product_template = product.product_tmpl_id
		if not values.get("cost_price_unit"):
			cost_price = product_template.standard_price
			if isinstance(cost_price, float):
				values["cost_price_unit"] = cost_price
		if not values.get("product_partner_id"):
			designer = product_template.seller_id
			if designer:
				values["product_partner_id"] = designer.id
		x = super(sale_order_line, self).create(cr, uid, values, context=context)
		if self.pool.get("sale.order").browse(cr, uid, values['order_id']).state == "manual":
			return x
		mod=False
		product_routes = self.pool.get('product.product').browse(cr,uid,values['product_id']).route_ids
		if product_routes:
			for route in product_routes:
				if route.name == 'Make To Order':
					mod=True
		sale_line=self.pool.get('sale.order.line').browse(cr,uid,[x])
		product_id=sale_line.product_id.id
		combo = self.pool.get('product.product').browse(cr,uid,product_id).product_tmpl_id.is_combos
		product_quantity=int(sale_line.product_uom_qty)
		sale = self.pool.get('sale.order.line').browse(cr,uid,[x]).order_id
		product_obj = self.pool.get('product.product')
		inventory_location_obj = self.pool.get("inventory.locations")
		flag = product_obj.is_product_bogo(cr,uid,product_id)
		flag2 = self.pool.get('sale.order').is_website_order(cr,uid,sale.id)
		availed = True
		if flag and flag2:
			self.pool.get('sale.order').write(cr, uid, sale.id, {'bogo_applicable' : True})
			availed = self.pool.get('sale.order').get_is_bogo_availed(cr,uid,sale.id)
		if not availed:
			self.pool.get('sale.order').write(cr, uid, sale.id, {'bogo_availed' : False})
		elif availed:
			self.pool.get('sale.order').write(cr, uid, sale.id, {'bogo_availed' : True})

		order_source = sale.source_id
		inventory_location_id = inventory_location_obj.search(cr, uid, [("channel_id", "=", order_source.id)])
		if not inventory_location_id:
			location_obj = self.pool.get('stock.location').search(cr,uid,[('complete_name','=','Voylla Locations / Warehouse / Warehouse / Good Bin')])
			location_obj_id = self.pool.get('stock.location').browse(cr,uid,location_obj)[0].id
		else:
			inventory_location = inventory_location_obj.browse(cr, uid, inventory_location_id)
			location_obj_id = inventory_location.inventory_location_id.id

		location_dest_obj=self.pool.get('stock.location').search(cr,uid,[('complete_name','=','Voylla Locations / Outbound / Outbound / Outbound')])
		location_dest_obj_id=self.pool.get('stock.location').browse(cr,uid,location_dest_obj)[0].id
		check=True
		price_check=True
		combo = self.pool.get('product.product').browse(cr,uid,product_id).product_tmpl_id.is_combos
		if combo:
			self.inventory_assignment_for_combo(cr,uid,product_id,sale_line,sale,context)

		else:
			#prod_virtual_quant = self.check_inventory(cr,uid,location_obj_id,product_id)
			check = self.is_item_available_in_qty(cr,uid,product_id,product_quantity,context)
			
			if sale_line.price_unit < sale_line.cost_price_unit:
				price_check=False
			if check:
				pro_order_id=str(sale.name)
				product_uom =self.pool.get('product.product').browse(cr,uid,product_id).product_tmpl_id.uom_id.id
				origin_name=sale_line.order_id.order_id
				if not origin_name:
					origin_name = sale_line.order_id.name
				stock_move_obj=self.pool.get('stock.move')
				for location_qty in check:
					c1={}
					c1={'lang': 'en_US', 'tz': False, 'uid': uid, 'invoice_state': None, 'procure_method': 'make_to_stock', 'search_default_future': True}
					vals2={}
					vals2={'state':'assigned','product_uom':product_uom, 'origin':sale_line.order_id.order_id,
						'company_id':1,'location_id':location_qty[0],'location_dest_id':location_dest_obj_id,'product_id':product_id,
						'name':sale_line.id,'date_expected': datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT),
						'date':datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT),'product_uom_qty':1,'queue_stock_bin_id':location_qty[2]}
					self.pool.get('bin.location_qty').create(cr,uid,{'product_id':product_id,'location_id':location_qty[0],'qty':location_qty[1],'line_id':sale_line.id})
					for i in range(0,location_qty[1]):
						stock_move_obj.create(cr, uid,vals2, context=c1)
				self.pool.get('sale.order.line').write(cr,uid,x,{'state':'assigned'})
				state_check = True
				for sol in sale.order_line:
					if sol.state == 'queued':
						state_check = False
				if state_check:		
					self.pool.get('sale.order').write(cr,uid,values['order_id'],{'state':'picklist','picklist_id':None})
				if price_check==False:
						self.pool.get('sale.order').write(cr, uid, sale.id, { 'status' : 'not_confirmed'})
			else:
				if mod:
					#prod_virtual_quant = self.check_inventory_mod(cr,uid,location_obj_id,product_id)
					context.update({'mod':True,'sale_order':values['order_id']})
					self.pool.get('purchase.order').genrate_purchase_order_for_make_to_order(cr,uid,values['product_id'],values['product_uom_qty'],context=context)
				self.pool.get('sale.order.line').write(cr,uid,x,{'state':'queued'})
				self.pool.get('sale.order').write(cr, uid, sale.id, { 'state' : 'queued'})
				if price_check==False:
					self.pool.get('sale.order').write(cr, uid, sale.id, { 'status' : 'not_confirmed'})
				pending_order_obj=self.pool.get('pending.sale.order')
				vals={'order_reference':x}
				pending_order_obj.create(cr,uid,vals,context=None)
			notes = self.pool.get('sale.order').browse(cr, uid, sale.id).note
			if not notes:
				notes = ''
			if self.pool.get("sale.order").browse(cr, uid, sale.id).bogo_availed:
				notes = notes.replace("BOGO Not Availed","")
				self.pool.get('sale.order').write(cr, uid, sale.id, {'note' : notes})
			elif self.pool.get("sale.order").browse(cr, uid, sale.id).bogo_applicable:
				notes = notes + "BOGO Not Availed"
				self.pool.get('sale.order').write(cr, uid, sale.id, {'note' : notes})
			if sale_line.price_unit < sale_line.cost_price_unit:
				notes = notes + "cost is greater than selling price"
				self.pool.get('sale.order').write(cr, uid, sale.id, {'note' : notes})
			if not sale.is_automated:
				self.pool.get('sale.order').bogo_adjustment_calculator(cr,uid,sale.id,context=context)
		return x

	def product_id_change(self, cr, uid, ids, pricelist, product, qty=0,
			uom=False, qty_uos=0, uos=False, name='', partner_id=False,
			lang=False, update_tax=True, date_order=False, packaging=False, fiscal_position=False, flag=False, context=None):
		context = context or {}
		lang = lang or context.get('lang', False)
		if not partner_id:
			raise osv.except_osv(_('No Customer Defined!'), _('Before choosing a product,\n select a customer in the sales form.'))
		warning = False
		product_uom_obj = self.pool.get('product.uom')
		partner_obj = self.pool.get('res.partner')
		product_obj = self.pool.get('product.product')
		context = {'lang': lang, 'partner_id': partner_id}
		partner = partner_obj.browse(cr, uid, partner_id)
		lang = partner.lang
		context_partner = {'lang': lang, 'partner_id': partner_id}

		if not product:
			return {'value': {'th_weight': 0,
					'product_uos_qty': qty}, 'domain': {'product_uom': [],
					'product_uos': []}}
		if not date_order:
			date_order = time.strftime(DEFAULT_SERVER_DATE_FORMAT)

		result = {}
		warning_msgs = ''
		product_obj = product_obj.browse(cr, uid, product, context=context_partner)
		##changed
		result['product_ean'] = product_obj.ean13
		##changed

		uom2 = False
		if uom:
			uom2 = product_uom_obj.browse(cr, uid, uom)
			if product_obj.uom_id.category_id.id != uom2.category_id.id:
				uom = False
		if uos:
			if product_obj.uos_id:
				uos2 = product_uom_obj.browse(cr, uid, uos)
				if product_obj.uos_id.category_id.id != uos2.category_id.id:
					uos = False
			else:
				uos = False
		fpos = False
		if not fiscal_position:
			fpos = partner.property_account_position or False
		else:
			fpos = self.pool.get('account.fiscal.position').browse(cr, uid, fiscal_position)
		if update_tax: #The quantity only have changed
			result['tax_id'] = self.pool.get('account.fiscal.position').map_tax(cr, uid, fpos, product_obj.taxes_id)

		if not flag:
			##changed
			result['name'] = self.pool.get('product.product').name_get_order_line(cr, uid, [product_obj.id])
			##changed
			if product_obj.description_sale:
				result['name'] += '\n'+product_obj.description_sale
		domain = {}
		if (not uom) and (not uos):
			result['product_uom'] = product_obj.uom_id.id
			if product_obj.uos_id:
				result['product_uos'] = product_obj.uos_id.id
				result['product_uos_qty'] = qty * product_obj.uos_coeff
				uos_category_id = product_obj.uos_id.category_id.id
			else:
				result['product_uos'] = False
				result['product_uos_qty'] = qty
				uos_category_id = False
			result['th_weight'] = qty * product_obj.weight
			domain = {'product_uom':
						[('category_id', '=', product_obj.uom_id.category_id.id)],
						'product_uos':
						[('category_id', '=', uos_category_id)]}
		elif uos and not uom: # only happens if uom is False
			result['product_uom'] = product_obj.uom_id and product_obj.uom_id.id
			result['product_uom_qty'] = qty_uos / product_obj.uos_coeff
			result['th_weight'] = result['product_uom_qty'] * product_obj.weight
		elif uom: # whether uos is set or not
			default_uom = product_obj.uom_id and product_obj.uom_id.id
			q = product_uom_obj._compute_qty(cr, uid, uom, qty, default_uom)
			if product_obj.uos_id:
				result['product_uos'] = product_obj.uos_id.id
				result['product_uos_qty'] = qty * product_obj.uos_coeff
			else:
				result['product_uos'] = False
				result['product_uos_qty'] = qty
			result['th_weight'] = q * product_obj.weight        # Round the quantity up

		if not uom2:
			uom2 = product_obj.uom_id
		# get unit price
		if not pricelist:
			warn_msg = _('You have to select a pricelist or a customer in the sales form !\n'
						'Please set one before choosing a product.')
			warning_msgs += _("No Pricelist ! : ") + warn_msg +"\n\n"
		else:
			price = self.pool.get('product.pricelist').price_get(cr, uid, [pricelist],
					product, qty or 1.0, partner_id, {
						'uom': uom or result.get('product_uom'),
						'date': date_order,
						})[pricelist]
			if price is False:
				warn_msg = _("Cannot find a pricelist line matching this product and quantity.\n"
						"You have to change either the product, the quantity or the pricelist.")

				warning_msgs += _("No valid pricelist line found ! :") + warn_msg +"\n\n"
				result.update({'price_unit': product_obj.lst_price})
			else:
				result.update({'price_unit': price})
		if warning_msgs:
			warning = {
						'title': _('Configuration Error!'),
						'message' : warning_msgs
					}

		return {'value': result, 'domain': domain, 'warning': warning}


class outbound_invoice(osv.osv):

	def calculate_ean(self,cr,uid,ids,psku,psize,context):
		prod_obj_ids=self.pool.get('product.template').search(cr,uid,[('name','=',psku)])
		if psize=='0' or psize=='' or psize==' ':
			prod_obj=self.pool.get('product.template').browse(cr,uid,prod_obj_ids).attribute_line_ids
			if len(prod_obj)>0:
				raise osv.except_osv(('Warning!'), ("Enter Size"))
			else:	
				ean=str(self.pool.get('product.template').browse(cr,uid,prod_obj_ids).product_variant_ids[0].ean13)
				return ean
		else:	
			t_product_ids=[]
			a_product_ids=[]
			
			prod_obj=self.pool.get('product.template').browse(cr,uid,prod_obj_ids).product_variant_ids
			for product in prod_obj:
				size = product.get_product_size()
				size_value = size.get('size',False)
				if size_value:
					try:
						if float(psize) == float(size_value):
							return product.ean13
					except:
						if psize.lower() == size_value.lower():
							return product.ean13
			raise osv.except_osv(('Warning!'), ("Wrong Size entered"))

	def pack_order(self, cr, uid, ids,invoice_name,order_id,scan,total_line_items,total_products_remaining,giftwrap,giftwrap_msg,context):
		if order_id:
			vals={}
			onumber=str(scan)
			inumber=str(invoice_name)
			sale=self.pool.get('sale.order')
			sale_obj_ids=sale.search(cr,uid,[('order_id','=',onumber),('state','=','progress')])
			sale_order_line_objs=self.pool.get('sale.order').browse(cr,uid,sale_obj_ids).order_line
			for order_line in sale_order_line_objs:
				product_quantity=int(order_line.product_uom_qty)
				if order_line.packed_quantity == product_quantity:
					continue
				product_ean=str(order_line.product_id.ean13)
				if product_ean in vals:
					vals[product_ean].append(order_line)
				else:
					vals[product_ean] = []
					vals[product_ean].append(order_line)
			order_id = str(order_id)
			list1=order_id.split(",")
			psku=list1[0]
			psize='0'
			length=len(list1)
			if length > 4:
				ean=self.calculate_ean(cr,uid,ids,psku,psize,context)
				pbatch=list1[3]
				psize=list1[1]
				pmrp=list1[2]
			elif length is 4:
				pmrp=list1[2]
				pbatch=list1[3]
				psize=list1[1]
				ean=self.calculate_ean(cr,uid,ids,psku,psize,context)
			elif length is 1:
				ean=self.calculate_ean(cr,uid,ids,psku,psize,context)
			elif length is 2:
				psize=list1[1]
				ean=self.calculate_ean(cr,uid,ids,psku,psize,context)
			if ean in vals:
				sale_order_line_obj=vals[ean][0]
				sale=sale_order_line_obj.order_id
				pq=int(sale_order_line_obj.packed_quantity)
				pq=pq+1
				self.pool.get("sale.order.line").write(cr, uid, [sale_order_line_obj.id], { 'packed_quantity' :pq })
				total_products_remaining=int(total_products_remaining)-1
				product_quantity=int(sale_order_line_obj.product_uom_qty)
				if(sale_order_line_obj.packed_quantity==product_quantity):
					self.pool.get("sale.order.line").write(cr, uid, [sale_order_line_obj.id], { 'state' :'packed' })
					check = True
					for line_order1 in sale_order_line_objs:
						if (line_order1.state!='packed'):
							check = False
					if check:
						self.pool.get("sale.order").write(cr, uid, [sale.id], { 'state' :'packed' })
						sale_id = self.pool.get('sale.order').search(cr,uid,[('order_id','=',onumber)])
						awb_obj_id=self.pool.get('awb.courier').search(cr,uid,[('order_no','=',sale_id[0]),('invoice_no','=',None)])
						inv_id=self.pool.get('account.invoice').search(cr,uid,[('number','=',inumber)])
						inv_obj=self.pool.get('account.invoice').browse(cr,uid,inv_id)
						if inv_obj:
							self.pool.get('awb.courier').write(cr,uid,awb_obj_id,{'invoice_no':inv_obj.id})
						else:
							raise osv.except_osv(('Warning!'), ("AWB Number Not Availbale"))
				res={'comp':False,'invoice_name':inumber,'scan':onumber,'total_products_remaining':total_products_remaining }					
				pack_id=self.pool.get('outbound.invoice').create(cr,uid,res,context=None)
				self.pool.get('outbound.invoice').write(cr,uid,pack_id,{'total_products_remaining':total_products_remaining})
				vals2={'ean':ean,
					'scan':scan,
					'sku':psku,
					'size':psize,
					'pack_id':pack_id[0]}
				self.pool.get('outbound.pack').create(cr,uid,vals2,context=None)
				all_obj=self.pool.get('outbound.invoice').browse(cr,uid,pack_id[0]).product_id					
				return{'value':{'product_id':all_obj,'order_id':None,'total_products_remaining':str(total_products_remaining),'total_line_items':total_line_items,'scan':onumber,'invoice_name':inumber,'giftwrap':giftwrap,'giftwrap_msg':giftwrap_msg}
				}
			else:
				raise osv.except_osv(('Warning!'), ("Product already Packed Or Not part of this order"))

	def create(self, cr, uid, values, context=None):
		 
		x=self.pool.get('outbound.invoice').search(cr,uid,[('scan','=',values['scan']),('invoice_name','=',values['invoice_name'])])
		if not x:
			x = super(outbound_invoice, self).create(cr, uid, values, context=context)
			return x
		else:
			if 'comp' in values:
				return x
			else:
				sale_obj_ids=self.pool.get('sale.order').search(cr,uid,[('order_id','=',values['scan'])])
				giftwrap=self.pool.get('sale.order').browse(cr,uid,sale_obj_ids).product_giftwrap
				if giftwrap != values['giftwrap']:
					raise  osv.except_osv(('Warning!'), ("Giftwrap status not match with Order"))

				total_products_remaining=values['total_products_remaining']

				if total_products_remaining!=0:
					raise  osv.except_osv(('Warning!'), ("All products in sale order are not packed"))				
				else:
					return x


	def onchange_order_no(self, cr, uid, ids,context):
		if context:
			total_product=0
			onumber=str(context)
			sale_obj_ids=self.pool.get('sale.order').search(cr,uid,[('order_id','=',onumber)])
			sale_objs=self.pool.get('sale.order').browse(cr,uid,sale_obj_ids)
			if sale_objs.state == 'hold':
				raise osv.except_osv(('Warning!'), ("This order cannot be Packed as order state is %s")%sale_objs.state)
	
			order=str(sale_objs.name)
			iname_obj_ids=self.pool.get('account.invoice').search(cr,uid,[('origin','=',order),('state','=','open')])
			iname=str(self.pool.get('account.invoice').browse(cr,uid,iname_obj_ids).number)
			giftwrap_message = sale_objs.giftwrap_message
			giftwrap = sale_objs.product_giftwrap
			
			
			for sale in sale_objs:
				order_line_obj=sale.order_line
				for line_order in order_line_obj:
					product_quantity=int(line_order.product_uom_qty)
					total_product=total_product+product_quantity
			
			vals={'comp':False,'invoice_name':iname,'scan':onumber,'total_line_items':total_product,'total_products_remaining':total_product,'giftwrap_msg':giftwrap_message,'giftwrap':giftwrap}
			 
			u=self.pool.get('outbound.invoice').create(cr,uid,vals)
			y=str(self.browse(cr,uid,u).total_products_remaining)
			# all_obj=self.browse(cr,uid,u).picklist_id
			return {'value':{'total_line_items': total_product,'invoice_name':iname,'total_products_remaining':y,'giftwrap_msg':giftwrap_message}}

		


	_name="outbound.invoice"
	_columns={
	'invoice_name':fields.char("Invoice Number"),
	'order_id':fields.char("Scan Product"),
	'total_line_items':fields.integer("Total Products"),
	'total_products_remaining':fields.integer("Product Remaining"),
	'product_id':fields.one2many('outbound.pack','pack_id','Product'),
	'scan':fields.char('Scan Order Number'),
	'comp':fields.boolean('Product Completely Picked'),
	'giftwrap':fields.boolean('Giftwrap'),
	'giftwrap_msg':fields.char('Giftwrap message', readonly=True)
	}
	_defaults={
	'comp':True
	}


class outbound_pack(osv.osv):
	_name="outbound.pack"
	_columns={
	'pack_id':fields.many2one('outbound.invoice','Pack ID'),
	'sku':fields.char('SKU'),
	'mrp':fields.char('MRP'),
	'batch_id':fields.char('Batch ID'),
	'ean':fields.char('EAN'),
	'size':fields.char('Size'),
	}
	


class courier_list(osv.osv):
	_name='courier.list'

	def _get_awb_courier(self, cr, uid, ids, context=None):
		res={}
		for awb in self.browse(cr,uid,ids,context=context):
			# res[payment.order_id.id]=True
			res[awb.courier_name.id]=True
			return res.keys()
	def _get_unassigned_awb(self, cr, uid, ids, name, arg, context=None):
		res={}
		for courier in self.browse(cr,uid,ids):
			if courier.name.lower() == 'blue dart':
				unassigned_prepaid = len(self.pool.get('awb.courier').search(cr,uid,[('courier_name','=',courier.name),('order_no','=',None),('tod','=','Prepaid')]))
				unassigned_cod = len(self.pool.get('awb.courier').search(cr,uid,[('courier_name','=',courier.name),('order_no','=',None),('tod','=','COD')]))
				res[courier.id] = 'Prepaid '+ str(unassigned_prepaid) +', COD ' + str(unassigned_cod)
			else:	
				unassigned_no = len(self.pool.get('awb.courier').search(cr,uid,[('courier_name','=',courier.name),('order_no','=',None)]))
				res[courier.id] = unassigned_no
			return res


	_columns={
	'name':fields.char('Courier Name',select=True,),
	'courier_number':fields.char('Courier Number'),
	'tracking_url':fields.char('Tracking Url'),
	'charge_amount':fields.float('Shipping Cost'),
	'priority':fields.selection([
			("standard", "Standard Shipping"),
			("express", "Express Shipping"),
		],"Type Of Delivery" , required=True),
	'journal_id': fields.many2one('account.journal',"Journal",required=True),
	'account_id': fields.char('Account NUmber'),
	'unassigned_awb':fields.function(_get_unassigned_awb, string='Unassigned AWB', type='char',
		store={
			'awb.courier': (_get_awb_courier,['order_no', 'invoice_no'], 10),
		},
		)
	}

class sales_courier(osv.osv):
	_name = 'sales.courier'
	_columns = {
	'pin_code' : fields.text('PIN code', required=True),
	'dest_city' : fields.char('City',required=True),
	'state' : fields.many2one('res.country.state','State',required=True),
	'type_of_delivery': fields.selection([
			("Prepaid", "Prepaid"),
			("COD", "COD"),
			],"Type Of Delivery" , required=True),
	'courier_name':fields.char('Courier Name',required=True),
	'preference':fields.boolean('Is Preferred'),
	'to_delete': fields.boolean('To Be Deleted'),
	'routes':fields.text('Delivery info'),
	}



class courier_import_delete(osv.osv):
	_name='courier.import.delete'


	def delete_all_button(self, cr, uid, ids, context=None):
		cr.execute("""delete from sales_courier """)
		# where (preference='yes' OR preference='no')
		return None

class awb_courier(osv.osv):
	_name = 'awb.courier'
	_columns = {
	'courier_name' : fields.many2one('courier.list','Courier Name', required= True),
	'awb_no' : fields.char('AWB',required=True,select=1),
	'order_no': fields.many2one('sale.order','Order No'),
	'invoice_no': fields.many2one('account.invoice','Invoice No'),
	'tod' : fields.selection([
			("Prepaid", "Prepaid"),
			("COD", "COD"),
			("Both", "Both"),
			],"Type Of Delivery" , required=True),
	'shipment_state' : fields.selection([
		('shipped','Shipped'),
		('DL','Delivered'),
		('pending','Pending'),
		('IT','In Transit'),
		('RT','RTO Initiated'),
		('RTO','RTO Received'),
		('UD','Undelivered'),
		('lost','LOST')
		],"Shipping Status", required=True,select=1),
	'return_awb' : fields.char('Return AWB No'),
	'is_delayed_sent':fields.boolean('Is Delay in delivery mail-sms sent?')
	}
	_sql_constraints = [
		('unique_awb_no', 'unique(awb_no)', 'AWB number already in the list')
	]
	_defaults = {
	'shipment_state' : 'pending'
	}


class outbound_rto(osv.osv):
	def _get_top(self, cr, uid, ids, name, arg, context=None):
		temp = {}
		for dispatch_order_obj in self.pool.get('outbound.rto').browse(cr, uid, ids):
			sale_reg = self.pool.get('sale.order')
			sale_id = sale_reg.search(cr,uid,[('order_id','=',dispatch_order_obj.order_number)])
			payment = sale_reg.get_payment_method(cr,uid,sale_id)
			temp[dispatch_order_obj.id] = payment
			return temp

	def create (self,cr,uid,values,context=None):
		x=self.search(cr,uid,[('order_number','=',values['order_number'])])
		if x:
			sale_obj = self.pool.get('sale.order')
			stock_move_obj = self.pool.get('stock.move')
			y = sale_obj.search(cr,uid,[('order_id','=',values['order_number'])])

			if values['action']=='rto':
				source_location_id = self.pool.get('stock.location').search(cr,uid,[('complete_name','=','Partner Locations / Customers')])
				dest_location_id = self.pool.get('stock.location').search(cr,uid,[('complete_name','=','Voylla Locations / Inbound / Inbound / Inbound')])
				sale_obj.write(cr,uid,y,{'state':'rto'})
				try:
					awb_obj = self.pool.get('awb.courier')
					sale_order_obj = sale_obj.browse(cr,uid,y)
					awb_id = awb_obj.search(cr,uid,[('awb_no','=',sale_order_obj.awb)])
					awb_obj.write(cr,uid,awb_id,{'shipment_state':'RTO',})
					inv_id = self.pool.get('account.invoice').search(cr,uid,[('order_id','=',sale_order_obj.order_id)])
					val_track = {'invoice_id':inv_id[0],'status':'RTO Received','status_time':datetime.now().strftime('%d %B %Y,%H:%M')}
					self.pool.get('tracking.details').create(cr,uid,val_track,context=context)
				except:
					raise osv.except_osv(('Warning!!!'), ("Shipment status is not updated"))
					
				c1={'lang': 'en_US', 'tz': False, 'uid': uid, 'invoice_state': None, 'procure_method': 'make_to_stock', 'search_default_future': True}
				sale_order_lines = sale_obj.browse(cr,uid,y).order_line
				for order_line in sale_order_lines:
					product_uom = order_line.product_uom.id
					product_id = order_line.product_id.id
					name = 'RTO_' + str(datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT))
					quantity = order_line.product_uom_qty
					vals2={'state':'assigned','product_uom':product_uom,
					'company_id':1,'location_id':source_location_id[0],'location_dest_id':dest_location_id[0],'product_id':product_id,
					'name':name,'date_expected': datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT),
					'date':datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT),'product_uom_qty':quantity}
					stock_move_obj.create(cr, uid,vals2, context=c1)
			self.write(cr,uid,x,{'return_awb':values['return_awb']})
		elif not x:
			x = super(outbound_rto, self).create(cr, uid, values, context=context)
		if isinstance(x, list):
			x = x[0]
		return x

	def onchange_order_no(self, cr, uid, ids, context):
		if context:
			total_product=0
			onumber=str(context)
			x=self.pool.get('sale.order')
			sol_obj = self.pool.get('sale.order.line')
			y=x.search(cr,uid,[('order_id','=',onumber),('state','=','done')])
			if not y:
				raise osv.except_osv(('Warning!'), ("Order Cannot be processed"))
			order_line_obj=x.browse(cr,uid,y).order_line
			for line_order in order_line_obj:
				product_quantity=int(line_order.product_uom_qty)
				total_product=total_product+product_quantity
			courier_name=str(x.browse(cr,uid,y).courier_name)
			awb=str(x.browse(cr,uid,y).awb)
			order_channel=str(x.browse(cr,uid,y).three_p_channel)
			invoice_number=str(x.browse(cr,uid,y).invoice_ids[0].number)

			values={'action':'rto','return_awb':'0','order_number':onumber,'olid':order_line_obj,'total_line_items':total_product,'courier_name':courier_name,'order_channel':order_channel,'awb':awb}
			create_id = self.create(cr,uid,values,context=None)
			for line_order in order_line_obj:
				sol_obj.write(cr,uid,[line_order.id],{'rto_id':create_id})
			return {'value':{'olid':order_line_obj,'total_line_items':total_product,'courier_name':courier_name,'order_channel':order_channel,'awb':awb}}

	_name='outbound.rto'
	_columns={
	'awb': fields.char('AWB No.'),
	'olid':fields.one2many('sale.order.line','rto_id','Sale Order Line'),
	'total_line_items':fields.integer('Total Items'),
	'courier_name':fields.char('Courier Name'),
	'order_number':fields.char('Order Number' , required=True),
	'top':fields.function(_get_top,string="Payment Type",type="char",store=True),
	'return_awb':fields.char('Return AWB', required=True),
	'order_channel':fields.char('Order Channel'),
	'remarks':fields.char('Remarks'),
	'action':fields.selection([
			("rto", "RTO"),
			],"Action" , required=True),
	}
	
class account_invoice_line(osv.osv):
	_inherit = 'account.invoice.line'

	def get_net_amount(self,cr,uid,ids,context=None):
		for invoice_line in self.browse(cr,uid,ids):
			discount = (invoice_line.discount/100)
			mrp = invoice_line.price_unit
			qty = invoice_line.quantity
			net_amount = ((1- discount)*mrp)*qty
			return net_amount

	def get_excise_duty(self,cr,uid,ids,context=None):
		invoice_line_object = self.browse(cr,uid,ids)
		for invoice_line in invoice_line_object:
			excise_id = invoice_line.invoice_line_tax_id
			flag = False
			for excise_app in excise_id:
				excise_name = excise_app.name
				if excise_name =='Central Excise 6%' or excise_name=='Central Excise 0%':
					flag = True
			if flag == True:
				for excise in excise_id:
					excise_name = excise.name
					if excise_name =='Central Excise 6%' or excise_name=='Central Excise 0%':
						return excise_name

	def not_excise_applicable(self,cr,uid,ids,context=None):
		invoice_line_object = self.browse(cr,uid,ids)
		for invoice_line in invoice_line_object:
			excise_id = invoice_line.invoice_line_tax_id
			flag = False
			for excise_app in excise_id:
				excise_name = excise_app.name
				if excise_name =='Central Excise 6%' or excise_name=='Central Excise 0%':
					flag = True
			if flag == False:
				for excise in excise_id:
					excise_name = excise.name
					if excise_name !='Central Excise 6%' or excise_name!='Central Excise 0%':
						return excise_name

	def get_sp(self,cr,uid,ids,context=None):
		for invoice_line in self.browse(cr,uid,ids):
			subtotal = invoice_line.price_subtotal
			quantity = invoice_line.quantity
			selling_price = subtotal/quantity
			return selling_price

	def get_tax(self,cr,uid,ids,context=None):
		for invoice_line in self.browse(cr,uid,ids):
			invoice_line_tax_id = invoice_line.invoice_line_tax_id
			tax_list = ''
			for invoice_line_tax in invoice_line_tax_id:
				invoice_name = invoice_line_tax.name
				tax_list = tax_list + invoice_name
			return tax_list

	def get_quantity(self, cr,uid,ids,context=None):
		x = int(self.browse(cr,uid,ids).quantity)
		return x


class account_invoice(osv.osv):


	def get_amount_to_be_collected(self,cr,uid,ids,fields,context=None):
		for payment in fields.payments:
			if payment.payment_method.lower() == "cashondelivery":
				amount = float(payment.amount)
				return  "%.2f"%float(amount)
		return "%.2f"%float(0)	


	def get_sale_id(self, cr, uid, ids, context=None):
		sale_order_registry = self.pool.get('sale.order')
		order_name = self.browse(cr,uid,ids).origin
		order_id = sale_order_registry.search(cr,uid,[('name','=',order_name)])
		return sale_order_registry.browse(cr,uid,order_id)

	def _get_picklist_id(self, cr, uid, ids, name, arg, context=None):
		sale= self.pool.get('sale.order')
		temp = {}
		for invoice in self.browse(cr,uid,ids):
			sale_id =  sale.search(cr,uid,[('name','=',invoice.origin)])
			picklist_name = sale.browse(cr,uid,sale_id).picklist_id.name
			temp[invoice.id] = picklist_name
			return temp

	def _get_order_id(self, cr, uid, ids, name, arg, context=None):
		sale= self.pool.get('sale.order')
		temp = {}
		for invoice in self.browse(cr,uid,ids):
			sale_id =  sale.search(cr,uid,[('name','=',invoice.origin)])
			threep_order = sale.browse(cr,uid,sale_id).order_id
			temp[invoice.id] = threep_order
			return temp


	_inherit = "account.invoice"
	_columns = {
	# 'picklist_id' : fields.related('partner_id','sale_order_ids','picklist_id','name',string="Picklist Name",type="char",relation="res.partner"),
	# 'order_id' : fields.related('partner_id','sale_order_ids','order_id',string="Order Number",type="char",relation="res.partner")
	'picklist_id' : fields.function(_get_picklist_id,string="Picklist Name",type="char",store=True),
	'order_id' : fields.function(_get_order_id,string="Order Number",type="char",store=True),
	}

	def get_combo(self,cr,uid,ids,fields,context=None):
		combo = fields.product_tmpl_id.is_combos
		products=[]
		if combo:
			all_combo_products = fields.product_tmpl_id.variant_product
			products = [str(product.product.name_template) for product in all_combo_products]
			return_string = "Combo of %s "%(products)
			return return_string	
		if not products:
			return None

	def get_size(self,cr,uid,ids,fields,context=None):
		attribute_hash = fields.get_product_size()
		return_attributes = {}
		for key,value in attribute_hash.items():
			return_attributes.update({str(key).title():str(value)})
		return_attributes = str(return_attributes)	
		return_attributes = return_attributes.replace('{','')
		return_attributes = return_attributes.replace('}','')
		return_attributes = return_attributes.replace(':','=')
		return_attributes = return_attributes.replace("'",'')
		return str(return_attributes)

	def calculate_adjust(self,cr,uid,ids,fields,context=None):
		temp={}		
		for invoice in self.browse(cr, uid, ids, context=context):
			sale_id=self.pool.get('sale.order').search(cr,uid,[('name','=',invoice.origin)])
			adjustment_objs=self.pool.get('sale.order').browse(cr,uid,sale_id).adjustments
			for adjustment in adjustment_objs:
				if fields == "shipping_charges":
					if adjustment.adjusment_label == "standard_shipping":
						return  "%.2f"%float(adjustment.amount)
					elif adjustment.adjusment_label == "express_shipping":
						return  "%.2f"%float(adjustment.amount)	

				if adjustment.adjusment_label == fields:
					return "%.2f"%float(adjustment.amount)
			return "%.2f"%float(0)

	def calculate_vat(self,cr,uid,ids,context=None):
		tax  = self.browse(cr,uid,ids).amount_tax
		excise = self.calculate_excise_charges(cr,uid,ids)
		total_excise = float(excise)
		vat = tax - total_excise
		return "%.2f"%float(vat)


	def calculate_excise_charges(self,cr,uid,ids,context=None):
		for invoice in self.browse(cr, uid, ids, context=context):
			sale_id=self.pool.get('sale.order').search(cr,uid,[('name','=',invoice.origin)])
			sale_object = self.pool.get('sale.order').browse(cr,uid,sale_id)
			sale_order_line_id = self.pool.get('sale.order.line').search(cr,uid,[('order_id','=',sale_object.id)])
			invoice_line = invoice.invoice_line
			for lines in invoice_line:
				excise_id = lines.invoice_line_tax_id
				flag = False
				for excise_app in excise_id:
					excise_name = excise_app.name
					excise_amount = excise_app.amount
					if excise_name =='Central Excise 6%' or excise_name=='Central Excise 0%':
						flag = True
				if flag == True:
					total_excise = 0
					for excise in excise_id:
						excise_name = excise.name
						excise_amount = excise.amount		
						if excise_name =='Central Excise 6%' or excise_name=='Central Excise 0%':
							unit_price = lines.price_subtotal
							quantity = lines.quantity
							amount = quantity * ((excise_amount) * unit_price)
							total_excise += amount
							return "%.2f"%float(total_excise)
				else:
					return 0.00

	def calculate_discount_charges(self,cr,uid,ids,fields,context=None):

		for invoice in self.browse(cr,uid,ids,context=context):
			balance=0
			sale_id=self.pool.get('sale.order').search(cr,uid,[('name','=',invoice.origin)])
			adjustment_objs=self.pool.get('sale.order').browse(cr,uid,sale_id).adjustments
			for adjustment in adjustment_objs:
				if fields == 'discount':
					if (adjustment.amount) <= 0.0:
						balance=balance+adjustment.amount
				if fields == 'charges':
					if (adjustment.amount) >= 0.0:
						balance=balance+adjustment.amount	
			return "%.2f"%float(balance)

	def calculate_total_line_items(self,cr,uid,ids,context=None):
		total_product=0
		for invoice in self.browse(cr, uid, ids, context=context):
			sale_id=self.pool.get('sale.order').search(cr,uid,[('name','=',invoice.origin)])
			order_line_obj=self.pool.get('sale.order').browse(cr,uid,sale_id).order_line
			for line_order in order_line_obj:
					product_quantity=int(line_order.product_uom_qty)
					total_product=total_product+product_quantity
		return total_product

class sale_order_adjustment(osv.osv):
	_name = "sale.order.adjustment"

	_columns = {
		"adjusment_label" : fields.selection([
			("standard_shipping", "Standard Shipping Charges"),
			("cashinaccount", "Cash In Account"),
			("express_shipping", "Express Shipping Charges"),
			("cod_charges", "CoD Charges"),
			("coupon", "Coupon"),
			("voucher", "Voucher"),
			("free_replacement", "Free Replacement"),
			("employee_discount", "Employee Discount"),
			("giftwrap", "Giftwrap"),
			("online_payment_discount", "Online Payment Discount"),
			("promotion", "Promotion"),
			("bogo", "Free Product Discount"),
			("other", "Others")
            ], "Adjustment Type", required=True),
		"adjustment_notes": fields.char("Adjustment Notes"),
		"amount" : fields.float("Amount", required=True),
		"order_id": fields.many2one('sale.order', 'Order Reference', ondelete='cascade', select=True, readonly=True)
		# 'invoice_lines': fields.many2many('account.invoice.line', 'sale_order_line_invoice_rel', 'order_line_id', 'invoice_id', 'Invoice Lines', readonly=True, copy=False),
		# 'invoiced': fields.function(_fnct_line_invoiced, string='Invoiced', type='boolean',
		# 	store={
		# 		'account.invoice': (_order_lines_from_invoice, ['state'], 10),
		# 		'sale.order.line': (lambda self,cr,uid,ids,ctx=None: ids, ['invoice_lines'], 10)
		# 	}),
	}

	def invoice_adjustment_create(self, cr, uid, ids, context=None):

		invoice_adj_obj = self.pool.get("account.invoice.adjustment")

		create_ids = []

		for adj_id in ids:
			vals = {}
			adjustment = self.browse(cr, uid, adj_id)
			vals["adjustment_label"] = adjustment.adjusment_label
			vals["adjustment_notes"] = adjustment.adjustment_notes
			vals["amount"] = adjustment.amount
			#vals["invoice_id"]=adjustment.order_id.id

			inv_adj_id = invoice_adj_obj.create(cr, uid, vals)
			create_ids.append(inv_adj_id)

		return create_ids

class sale_order_payment(osv.osv):
	_name = "sale.order.payment"

	_columns = {
		"payment_method": fields.selection([
			("Gateway", "Gateway"),
			("InternationalPayment", "International Payment"),
			("Gateway::PayuIn", "Gateway PayuIn"),
			("CashOnDelivery", "Cash On Delivery"),
			("ThirdPartyPayment", "Third Party Payment"),
			("PaymentMethod::Check", "Payment Method Check"),
			("PaypalPayment", "Paypal Payment"),
			("Gateway::Bogus", "Gateway Bogus"),
			("GiftVoucherPayment", "Gift Voucher Payment"),
			("Cash", "Cash"),
			("BankTransfer","Bank Transfer"),
			("PaytmWallet", "PayTM Wallet"),
			("free_rep", "Free Replacement"),
			], "Payment Method", required=True),
		"payment_reference": fields.char("Payment Reference"),
		"amount": fields.float("Amount", required=True),
		"order_id": fields.many2one("sale.order", "Order Reference", ondelete="cascade", select=True, readonly=True),
	}

class outbound_dispatch(osv.osv):

	def onchange_press_tab(self, cr, uid, ids, scan, context=None):
		# if scan:
		#	 pyautogui.keyDown('tab')
		#	 pyautogui.keyUp('tab')
		return

	def onchange_scan_order(self, cr, uid, ids, scan_awb, scan_three_p_label,onumber,courier_name, date_time,context):
		remote_tasks_obj = self.pool.get('remote.tasks')
		if not scan_three_p_label or not onumber or not scan_awb:
			return
		if str(onumber) == str(scan_three_p_label):
			total_product=0
			if not date_time:
				date_time=datetime.now()
				date_time = datetime.strftime(date_time, "%Y-%m-%d %H:%M:%S")
			onumber=str(onumber)
			courier_id=courier_name
			courier_name=str(self.pool.get('courier.list').browse(cr,uid,courier_id).name)
			x=self.pool.get('sale.order')
			z = x.search(cr,uid,[('order_id','=',onumber)])
			phone = x.browse(cr,uid,z).partner_id.phone
			y=x.search(cr,uid,[('order_id','=',onumber),('state','=','packed'),('courier_name','=',courier_name)])
			
			stock_move_obj=self.pool.get('stock.move')
			dest_location_id = self.pool.get('stock.location').search(cr,uid,[('complete_name','=','Partner Locations / Customers')])
			source_location_id = self.pool.get('stock.location').search(cr,uid,[('complete_name','=','Voylla Locations / Outbound / Outbound / Outbound')])
			c1={'lang': 'en_US', 'tz': False, 'uid': uid, 'invoice_state': None, 'procure_method': 'make_to_stock', 'search_default_future': True}
			if y:
				# pyautogui.hotkey('shift','tab')
				# pyautogui.hotkey('shift','tab')
				sale = x.browse(cr,uid,y)
				product_id_obj = sale.order_line[0].product_id.id
				courier_name = str(sale.courier_name)
				courier_obj = self.pool.get('courier.list').browse(cr,uid,courier_id)
				if courier_obj.to_be_tracked and scan_awb !=sale.awb:
					raise osv.except_osv(('Error!!!!'), ("Order cannot be Dispatched. scanned AWB number:\' %s \' doesn't match with assigned AWB number: \' %s\ '" %(scan_awb,sale.awb)))
				x.write(cr,uid,y,{'awb':scan_awb})
				awb = str(scan_awb)
				
				obj=self.pool.get('outbound.dispatch.order').browse(cr,uid,ids)
				vals2={'courier_name':courier_id,'scan':onumber,'order':obj.id,'date_time':date_time}
				u=self.create(cr,uid,vals2,context=None)
				invoice_number=str(x.browse(cr,uid,y).invoice_ids[0].number)
				vals={'product_id':product_id_obj,'order_no':onumber,'courier_name':courier_name,'invoice_no':invoice_number,'awb':awb,'dispatches':u}
				z=self.pool.get('outbound.dispatch.order')
				 
				order_ids=z.create(cr,uid,vals,context=None)
				display=self.pool.get('outbound.dispatch').search(cr,uid,[('courier_name','=',courier_id),('date_time','=',date_time)])
				display_obj=self.pool.get('outbound.dispatch').browse(cr,uid,display)
				x.write(cr, uid, y, {'state':'done' , 'shipped': True , 'dispatched_date':date_time})

				template_id = self.pool.get('email.template').search(cr,uid,[('name','=','Email Template Shipment')])
				if sale.state=='done':
					awb_id = self.pool.get('awb.courier').search(cr,uid,[('awb_no','=',sale.awb)])
					self.pool.get('awb.courier').write(cr,uid,awb_id,{'shipment_state':'shipped'})
					if sale.shipping_type != "3rd_party":
						try:
							self.pool.get('sale.order').send_mail_2(cr,uid,y[0],context)
						except:
							pass
						try:
							_logger.info("==========================")
							_logger.info("Dispatched SMS taks starts")
							_logger.info("==========================")
							values = [onumber,courier_name,scan_awb,phone]
							remote_tasks_obj.dispatch_sms_of_scanned_product(cr, uid, values)
						except:
							pass
						# self.pool.get('sale.order').send_sms2(cr,uid,y[0],context)
				line_order=x.browse(cr,uid,y).order_line
				for line_order_obj in line_order:
					product_uom = line_order_obj.product_uom.id
					product_id = line_order_obj.product_id.id
					quantity = line_order_obj.product_uom_qty
					name = 'Dispatched ' + str(courier_name) + str(line_order_obj.order_id.order_id)
					vals2={'state':'assigned','product_uom':product_uom, 'origin':line_order_obj.order_id.order_id,
					'company_id':1,'location_id':source_location_id[0],'location_dest_id':dest_location_id[0],'product_id':product_id,
					'name':name,'date_expected': datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT),
					'date':datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT),'product_uom_qty':quantity}
					self.pool.get('sale.order.line').write(cr,uid,[line_order_obj.id],{'state':'done'})
					ids_done = stock_move_obj.create(cr, uid,vals2, context=c1)
					stock_move_obj.action_done(cr,uid,ids_done)

				id_sale=x.search(cr,uid,[('courier_name','=',courier_name),('state','=','packed')])
				length=len(id_sale)
				all_ids=[]
				for obj in display_obj:
					dispatch_obj=obj.order
					for dispatch in dispatch_obj:
						all_ids.append(dispatch.id)
				 		
				all_obj=z.browse(cr,uid,all_ids)	
				return {'value':{'courier_name':courier_id,'order':all_obj,'date_time':date_time,'scan':None,'length':length}}
			else:
				y=x.search(cr,uid,[('order_id','=',onumber)])
				sale= x.browse(cr,uid,y)
				raise osv.except_osv(('Warning!'), ("Order cannot be Dispatched. Order state: %s Assigned Courier: %s" %(sale.state,sale.courier_name)))
		else:
			raise osv.except_osv(('Error'), ('Three P label and Order does not match'))	

	def create(self, cr, uid, values, context=None):
		
		x=self.pool.get('outbound.dispatch').search(cr,uid,[('courier_name','=',values['courier_name']),('date_time','=',values['date_time'])])
		if x:
			x=x[0]
		if not x:
			x = super(outbound_dispatch, self).create(cr, uid, values, context=context)
		return x			

	def  onchange_courier_name(self, cr, uid, ids,courier_name,context=None):
		if courier_name:
			 
			courier_name=str(self.pool.get('courier.list').browse(cr,uid,courier_name).name)
			x=self.pool.get('sale.order')
			id_sale=x.search(cr,uid,[('courier_name','=',courier_name),('state','=','packed')])
			length=len(id_sale)
			length=str(length)
			# pyautogui.keyDown('tab')
			# pyautogui.keyUp('tab')
			return {'value':{'length':length}}




	_name="outbound.dispatch"
	_columns={
		'length':fields.char('Total Orders'),
		"courier_name":fields.many2one('courier.list','Courier Name', required=True),
		"scan":fields.char("Scan Order Number"),
		"scan_three_p_label":fields.char("Scan 3P Label"),
		"scan_awb" :fields.char("Scan AWB"),
		'order':fields.one2many('outbound.dispatch.order','dispatches',"Order Details"),
		'date_time':fields.datetime('Dispatched Date And Time'),
		}

class outbound_dispatch_order(osv.osv):

	def _get_quantity(self, cr, uid, ids, name, arg, context=None):
		temp = {}
		dispatch_order_obj = self.pool.get('outbound.dispatch.order').browse(cr, uid, ids)
		sale_reg = self.pool.get('sale.order')
		sale_id = sale_reg.search(cr,uid,[('order_id','=',dispatch_order_obj.order_no)])
		quantity = sale_reg._get_total_quantity(cr,uid,sale_id)

		temp[dispatch_order_obj.id] = quantity
		return temp
		
		

	def _get_tod(self, cr, uid, ids, name, arg, context=None):
		temp = {}
		dispatch_order_obj = self.pool.get('outbound.dispatch.order').browse(cr, uid, ids)
		sale_reg = self.pool.get('sale.order')
		sale_id = sale_reg.search(cr,uid,[('order_id','=',dispatch_order_obj.order_no)])
		payment = sale_reg.get_payment_method(cr,uid,sale_id)
		temp[dispatch_order_obj.id] = payment
		return temp

	def _get_area_customer(self, cr, uid, ids, name, arg, context=None):
		sale_reg = self.pool.get('sale.order')
		temp = {}
		for dispatched_order in self.browse(cr,uid,ids):
			sale_id = sale_reg.search(cr,uid,[('order_id','=',dispatched_order.order_no)])
			payment = sale_reg.get_payment_method(cr,uid,sale_id)
			courier_name = dispatched_order.dispatches.courier_name.name
			account_id = dispatched_order.dispatches.courier_name.account_id
			if courier_name == 'Blue Dart':
				account_id = account_id.split(",")
				if len(account_id) == 2:
					if payment == 'prepaid':
						temp[dispatched_order.id] = account_id[0]
					elif payment == 'cashondelivery':
						temp[dispatched_order.id] = account_id[1]
					return temp	
				else:
					raise osv.except_osv(('Warning!'), ("Specify prepaid and cod accounts for Blue Dart"))
			temp[dispatched_order.id] = account_id		
			return temp
						

	def _get_handover_time(self, cr, uid, ids, name, arg, context=None):
		for dispatched_order in self.browse(cr,uid,ids):
			temp = {}
			x= dispatched_order.dispatches.date_time
			x= x.split(" ")
			temp[dispatched_order.id] = x[1]
			return temp

	def _get_handover_date(self, cr, uid, ids, name, arg, context=None):
		
		for dispatched_order in self.browse(cr,uid,ids):
			temp = {}
			x= dispatched_order.dispatches.date_time
			x= x.split(" ")
			temp[dispatched_order.id] = x[0]
			return temp

	def _get_dispatched_order_id(self, cr, uid, ids, name, arg, context=None):
		sale_reg = self.pool.get('sale.order')
		for dispatched_order in self.browse(cr,uid,ids):
			temp = {}
			sale_id = sale_reg.search(cr,uid,[('order_id','=',dispatched_order.order_no)])
			temp[dispatched_order.id] = sale_id
			return temp
			

	def _get_default_shipper(self, cr, uid, ids,context=None):
		shipper_id = self.pool.get('res.partner').search(cr,uid,[('name','=','Voylla Retail Pvt. Ltd.')])
		return shipper_id

	def _get_amount(self, cr, uid, ids, name, arg, context=None):
		sale_reg = self.pool.get('sale.order')
		sale_order_payment = self.pool.get('sale.order.payment')
		for dispatched_order in self.browse(cr,uid,ids):
			temp = {}
			sale_id = sale_reg.search(cr,uid,[('order_id','=',dispatched_order.order_no)])
			payment_type = sale_reg.get_payment_method(cr,uid,sale_id)
			if payment_type == 'prepaid':
				temp[dispatched_order.id] = 0.0
			elif payment_type == 'cashondelivery':
				sale_order_payment_reg =sale_reg.browse(cr,uid,sale_id).payments
				for payment in sale_order_payment_reg:
					if payment.payment_method.lower() == "cashondelivery":
						temp[dispatched_order.id] = float(payment.amount)
			return temp





	_name="outbound.dispatch.order"
	_columns={
		'dispatches':fields.many2one("outbound.dispatch"),
		'order_no':fields.char("Order Number"),
		"invoice_no":fields.char("Invoice Number"),
		'awb':fields.char("AWB"),
		'quantity':fields.function(_get_quantity,method=True,string='Total Quantity',type='integer',store=True),
		'tod':fields.function(_get_tod,method=True,string='Type Of Delivery',type='char',store=True),
		'contents':fields.char('Contents'),
		'product_id':fields.many2one('product.product','Product SKU'),
		'weight':fields.integer('Weight'),
		'length':fields.integer('Length'),
		'breadth':fields.integer('Breadth'),
		'height':fields.integer('Height'),
		'seller_name':fields.char('Seller Name'),
		'amount_collectable': fields.function(_get_amount,method=True, string='Amount Collectable',type='float',store=True),
		'area_customer':fields.function(_get_area_customer,method=True, string='Area Customer',type='char',store=True),
		'handover_time' : fields.function(_get_handover_time,method=True, string='Handover Time',type='char',store=True),
		'handover_date' : fields.function(_get_handover_date,method=True, string='Handover Date',type='char',store=True),
		'vendor_code':fields.char('Vendor Code'),
		'seller_cst':fields.function(_get_area_customer,method=True, string='Customer CST',type='char',store=True),
		'shipper_id': fields.many2one('res.partner','Shipper Details'),
		'order_id':fields.function(_get_dispatched_order_id, method=True, string='Order Details',type='many2one',relation='sale.order',store=True),
		}
	_defaults={
		'contents':'Fashion Accessories',
		'weight':0.5,
		'length':13.5,
		'breadth':13.5,
		'height':5.5,
		'seller_name':'Voylla.com',
		'vendor_code':'voylla',
		'shipper_id':_get_default_shipper,
		}

	def create(self, cr, uid, values, context=None):
		 
		x = super(outbound_dispatch_order, self).create(cr, uid, values, context=context)
		return x

class batch_movement(osv.osv):
	_name="batch.movement"

	def action_confirm(self, cr, uid, ids, context=None):
		x=self.pool.get('batch.movement').browse(cr,uid,context['res_id'])
		source_location=x.source_location.id
		name=str(x.name)
		dest_location=x.destination_location.id
		outbound_movement_objs=x.move_id
		stock_move_obj=self.pool.get('stock.move')
		c1={'lang': 'en_US', 'tz': False, 'uid': uid, 'invoice_state': None, 'procure_method': 'make_to_stock', 'search_default_future': True}
		for move in outbound_movement_objs:
			product_id=move.product_id.id
			product_uom =self.pool.get('product.product').browse(cr,uid,product_id).product_tmpl_id.uom_id.id
			quantity=move.product_uom_qty
			vals2={'state':'assigned','product_uom':product_uom,'origin':name, 
			'company_id':1,'location_id':source_location,'location_dest_id':dest_location,'product_id':product_id,
			'name':name,'date_expected': datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT),
			'date':datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT),'product_uom_qty':quantity}
			stock_move_obj.create(cr, uid,vals2, context=c1)
		x.state='done'


	def create(self, cr, uid, values, context=None):
		x = self.search(cr,uid,[('creation_date','=',values['creation_date']),('name','=',values['name'])])
		if x:
			x = x[0]
		if not x:
			values['state']='process'
			x = super(batch_movement, self).create(cr, uid, values, context=context)
		return x 	

	def	onchange_scan_product(self, cr, uid, ids, name, scan, creation_date, source_location, destination_location, context=None):
		if scan and (not name or not source_location or not destination_location):
			raise osv.except_osv(('Warning!'), ("Required values is missing"))
		if not creation_date:
			creation_date = datetime.now()
			creation_date = datetime.strftime(creation_date, "%Y-%m-%d %H:%M:%S")
		
		if scan:			
			out_inv_obj=self.pool.get('outbound.invoice')
			scan=str(scan)
			pbatch='NA'
			pmrp='0'
			psize='0'

			
			list1=scan.split(",")
			psku=list1[0]
			length=len(list1)
			if length > 5:
				# ean=list1[4]
				pbatch=list1[3]
				psize=list1[1]
				pmrp=list1[2]
				ean=out_inv_obj.calculate_ean(cr,uid,ids,psku,psize,context)
			elif length is 4:
				pbatch=list1[3]
				pmrp=list1[2]
				psize=list1[1]
				ean=out_inv_obj.calculate_ean(cr,uid,ids,psku,psize,context)
			elif length is 1:
				ean=out_inv_obj.calculate_ean(cr,uid,ids,psku,psize,context)
			elif length is 2:
				psize=list1[1]
				ean=out_inv_obj.calculate_ean(cr,uid,ids,psku,psize,context)

			product_id = self.pool.get('product.product').search(cr,uid,[('ean13','=',ean)])	
			product_obj_id = self.pool.get('product.product').browse(cr,uid,product_id).id
			values = {'name':name,'creation_date':creation_date, 'source_location':source_location, 'destination_location':destination_location}
			x = self.create(cr,uid,values,context=None)
			values2 = {'product_id':product_obj_id,'batch_id':x,'product_size':psize, 'product_uom_qty':1}
			y = self.pool.get('outbound.movement').create(cr,uid,values2,context=None)
			batch_movement_obj = self.browse(cr,uid,x).move_id
			
			return {'value':{'move_id':batch_movement_obj, 'scan':None,'creation_date':creation_date,'source_location':source_location,
				'destination_location':destination_location}}



	_columns={
		'creation_date':fields.datetime("Creation Date"),
		'scan':fields.char("Scan Product"),
		'move_id':fields.one2many('outbound.movement','batch_id','Products'),
		'name':fields.char('Description',required=True),		
		'source_location':fields.many2one('stock.location','Source Location'),
		'destination_location':fields.many2one('stock.location','Destination Location'),
		'state':fields.selection([
			('draft', 'Draft'),
			('done', 'Done'),
			('process', 'Process'),
			],'Status')
		}

	_defaults={
		'state' : 'draft',
		}

class outbound_movement(osv.osv):
	_name="outbound.movement"

	_columns={
	'batch_id':fields.many2one('batch.movement'),
	'product_id':fields.many2one('product.product','Product',required=True),
	'product_size':fields.char('Product Size'),
	'product_uom_qty':fields.float('Quantity',required=True)
	}

class bins_to_sync(osv.osv):
	_name = "bins.to.sync"	
	_columns = {
		"name":fields.many2one('stock.location','Location'),
		"to_sync" : fields.boolean('To Sync?')
	}
	_sql_constraints = [
        ('unique_name', 'unique(name)', 'Location already in the list')
        ]


class failure_notification(osv.osv):

	_name = "failure.notification"
	_description = 'Sending notifications in case of failures'
	_inherit = ['mail.thread', 'ir.needaction_mixin']

	_columns = {
		"name": fields.char("Name"),
		"type": fields.selection([
					("order_creation", "Order Creation Failure"),
					("pricelist_error", "Pricelist Error"),
					("quantity_update", "Quantity Update Failure"),
					("order_line_creation", "Order Line Creation Failure"),
					], "Type", required=True, select=True),
		"order_id": fields.char("Order Number", select=True),
		"description": fields.char("Description")
	}

class import_stock_moves(osv.osv):
	_name = 'import.stock.move'
	_columns = {
		'module_file': fields.binary('File to upload', required=True),
		'source_location': fields.many2one('stock.location','Source Location',required=True),
		'destination_location': fields.many2one('stock.location','Destination Location',required=True),
		'name': fields.char('Move Name',required=True)
	}
	_sql_constraints = [
        ('unique_name', 'unique(name)', 'Name already used before')
       ]


	def import_stock_move(self, cr, uid, ids, context=None):
		ean_error = {}
		product_id_error = {}
		quantity_error = []
		available_quantity = {}
		stock_move_registry = self.pool.get('stock.move')
		product_product_registry = self.pool.get('product.product')
		c1 = {'lang': 'en_US', 'tz': False, 'uid': uid, 'invoice_state': None, 'procure_method': 'make_to_stock', 'search_default_future': True}
		for import_stock_move_obj in self.browse(cr,uid,ids):
			name = import_stock_move_obj.name
			source_location_id = import_stock_move_obj.source_location.id
			destination_location_id = import_stock_move_obj.destination_location.id
			if not source_location_id or not destination_location_id or not name:
				raise osv.except_osv(('Info'), ("Mandatory Fields are missing"))
			try:
				csv_data = base64.decodestring(import_stock_move_obj.module_file)
			except:
				raise osv.except_osv(('Info'), ("Import File"))
			rows = csv_data.split("\n")
			headers = rows[0].split(",")
			csv_move_details = rows[1:]
			check = True
			for move in csv_move_details:
				if move == '':
					check = False
					continue
				move_list = move.split(",")
				# ean = outbound_invoice_registry.calculate_ean(cr,uid,ids,move_list[0],move_list[1],context)
				# if not ean or ean =='False':
				# 	ean_error[move_list[0]] = move_list[1]
				# 	continue
				product_id = product_product_registry.search(cr,uid,[('ean13','=',move_list[0])])
				if not product_id:
					check = False
					ean_error[move_list[0]] = move_list[1]
					continue
				elif len(product_id) > 1:
					check = False
					product_id_error[move_list[0]] = move_list[1]
					continue
				if move_list[1] =='':
					check = False
					quantity_error.append(move_list[0])
					continue
				vals={}
				try:
					quantity = int(move_list[1])
					prod_virtual_quant=product_product_registry._product_available(cr, uid,
						[product_id[0]],context={'location':source_location_id,'compute_child':False})[product_id[0]]['virtual_available']
					if int(prod_virtual_quant) < quantity:
						available_quantity[move_list[0]] = prod_virtual_quant
						check = False
						continue
				except:
					check = False
					quantity_error.append(move_list[0])
					continue
			if check:
				# product_uom = product_product_registry.browse(cr,uid,product_id).product_tmpl_id.uom_id.id
				# vals2={'state':'assigned','product_uom':product_uom,'origin':name,
				# 	'company_id':1,'location_id':source_location_id,'location_dest_id':destination_location_id,'product_id':product_id[0],
				# 	'name':name,'date_expected': datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT),
				# 	'date':datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT),'product_uom_qty':quantity}
				# id_done = stock_move_registry.create(cr, uid,vals2, context=c1)
				# stock_move_registry.action_done(cr,uid,id_done)
				# cr.commit()
				self.create_stock_move_from_file_import(cr,uid,ids,name,source_location_id,destination_location_id,csv_data,context=context)
			else:	
				raise osv.except_osv(('Info'), ("1. No EAN %s \n 2. EAN to multiple products %s \n \
							3. Invalid Quantity %s \n 4. Not moved products: EAN and available quantity %s \n"%(ean_error,product_id_error,quantity_error,available_quantity)))	

	def create_stock_move_from_file_import(self,cr,uid,ids,name,source_location_id,destination_location_id,csv_data,context=None):
		# stock_move_registry = self.pool.get('stock.move')
		c1 = {'lang': 'en_US', 'tz': False, 'uid': uid, 'invoice_state': None, 'procure_method': 'make_to_stock', 'search_default_future': True}
		product_product_registry = self.pool.get('product.product')
		remote_tasks_obj = self.pool.get('remote.tasks')
		rows = csv_data.split("\n")
		headers = rows[0].split(",")
		csv_move_details = rows[1:]
		check = True
		for move in csv_move_details:
			move_list = move.split(",")
			quantity = int(move_list[1])
			product_id = product_product_registry.search(cr,uid,[('ean13','=',move_list[0])])[0]
			product_uom = product_product_registry.browse(cr,uid,product_id).product_tmpl_id.uom_id.id
			state='assigned'
			origin=name
			location_id = int(source_location_id)
			location_dest_id = int(destination_location_id)
			remote_tasks_obj.create_complete_stock_moves(cr,uid,quantity,product_id,product_uom,state,origin,location_id,location_dest_id)

class update_orders(osv.osv):
	_name="update.orders"

	def update_promotion(self,cr,uid,soa_ids,context=None):
		soa_obj = self.pool.get('sale.order.adjustment')
		so_obj = self.pool.get('sale.order')
		error_list = []
		for soa_dict in soa_ids:			
			order_id = soa_dict['order_id']
			# notes = 'Amazon Promotion'
			amount = soa_dict['amount']
			# label = 'promotion'
			try:
				print soa_dict
				soa_obj.create(cr,uid,{'adjusment_label':'promotion','adjustment_notes':'Amazon Promotion',\
						'order_id':order_id,'amount':-amount})
				cr.commit()
			except:
				error_list.append(soa_dict)
				print 'error in %s'%(soa_dict)
				continue
			amount_total = so_obj.browse(cr,uid,order_id).amount_total
			if amount_total>500:
				continue
			else:
				soa_obj.create(cr,uid,{'adjusment_label':'standard_shipping','adjustment_notes':"Standard Shipping",\
					'order_id':order_id,'amount':50.00})
				cr.commit()
		print error_list
					

	def update_order(self,cr,uid,sol_ids,context=None):
		sol_obj = self.pool.get('sale.order.line')
		error_list = []
		for sol_dict in sol_ids:
			solid = sol_dict['sol_id']
			price = sol_dict['price']
			print sol_dict
			try:
				sol_obj.write(cr,uid,solid,{'price_unit':price,'discount':0.0})
			except:
				error_list.append(sol_dict)
				print 'error in %s'%(sol_dict)
				continue
		print error_list		
