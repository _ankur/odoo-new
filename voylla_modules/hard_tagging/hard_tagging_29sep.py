from openerp.osv import fields, osv
import logging
import threading
from openerp.api import Environment
import pdb
from datetime import datetime
class add_purchase_order(osv.osv):
	_inherit='purchase.order'
	def product_update_confirmed(self,cr,uid,ids,context=None):
		product_product=self.pool.get('product.product')
		product_template=self.pool.get('product.template')
		purchase_obj= self.browse(cr,uid,ids)
		for purchase_line_item in purchase_obj.order_line:
			variant=purchase_line_item.product_id
			if len(variant)==0:
				continue
			t=variant.product_tmpl_id
			try:
				temp_vals={}
				if purchase_line_item.manu_cost > 0 :
					variant.write({'manu_cost':purchase_line_item.manu_cost})
				if t.standard_price==0:
					temp_vals.update({'standard_price':purchase_line_item.price_unit})
				if len(t.supplier_taxes_id)==0 and len(purchase_line_item.taxes_id)!=0:
					temp_vals.update({'supplier_taxes_id':[purchase_line_item.taxes_id[0].id]})
				if len(t.taxonomy)==0 and 'taxonomy' in purchase_line_item.taxonomy:
					temp_vals.update({'taxonomy': [[0, False, {'categ_id': purchase_line_item.taxonomy.id}]]})
				if len(t.seller_ids)==0:
					temp_vals.update({'seller_ids':  [[0, False, {'pricelist_ids': [], 'name': purchase_obj.partner_id.id, 
						'sequence': 1,
					 'company_id': 1, 'delay': 30, 'min_qty': 0, 
					 'product_code': purchase_line_item.supplier_product_code, 'product_name': purchase_line_item.supplier_product_code}]]})
				if len(temp_vals) > 0:
					product_template.write(cr,uid,t.id,temp_vals,context)
					cr.commit()
			except:
				continue
	def write(self , cr, uid,ids,vals,context=None):
		order=super(add_purchase_order, self).write(cr, uid, ids,vals, context=context)
		#if 'state' in vals and vals['state']=='confirmed':
		#	self.product_update_confirmed(cr,uid,ids,context)
		return order
	def genrate_purchase_order_for_make_to_order(self,cr,uid,ids,qty,context=None):
		product=self.pool.get('product.product')
		product=product.browse(cr,uid,ids)
		tax=[]
		for tax_id in product.supplier_taxes_id:
			tax.append(tax_id.id)
		if not context:
			context = {}
		is_mod = context.get('mod',False)
		if is_mod:
			purchase_ids=self.search(cr,uid,[('partner_id','=',product.seller_id.id),('is_mod','=',True),('state','=','draft')])
			if len(purchase_ids) == 0:
				vals={'origin': False,'message_follower_ids': False,'is_mod':True,
				 'order_line': [[0, False, {'sale_order':context['sale_order'],'product_id': product.id, 'product_uom': product.uom_id.id, 'date_planned': datetime.now().strftime("%Y-%m-%d"),
				  'price_unit': product.standard_price, 'taxon_id': product.taxon.id, 'taxes_id': [[6, False, tax]], 'product_qty': qty, 
				  'account_analytic_id': False, 'manu_cost': product.manu_cost, 'supplier_product_code': product.seller_ids[0].product_code, 
				  'name': product.name_template}]], 'company_id': 1, 'currency_id': 21, 'date_order': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
				   'location_id': 7, 'message_ids': False, 'dest_address_id': False, 'fiscal_position': False, 
				   'picking_type_id': 1, 'partner_id': product.seller_id.id, 'journal_id': 3, 
				   'bid_validity': False, 'pricelist_id': 2, 'incoterm_id': False,
				    'payment_term_id': False, 'partner_ref': False, 'notes': False, 
				    'invoice_method': 'order', 'minimum_planned_date': False,
				     'related_location_id': 7,}
				order=self.create(cr, uid, vals, context=context)
				return
			else:
				vals={'order_line': [[0, False, {'sale_order':context['sale_order'],'product_id': product.id, 'product_uom': product.uom_id.id, 
				'date_planned': datetime.now().strftime("%Y-%m-%d"), 'price_unit': product.standard_price, 'taxon_id': product.taxon.id,
				 'taxes_id': [[6, False, tax]], 'product_qty': qty,
				  'account_analytic_id': False,'manu_cost':product.manu_cost,
				  'supplier_product_code': product.seller_ids[0].product_name, 'name': product.name_template}]]}
				purchase_obj=self.browse(cr,uid,purchase_ids[0])
				purchase_obj.write(vals)	 
				return
		# purchase_ids=self.search(cr,uid,[('partner_id','=',product.seller_id.id),('state','=','draft')])
		# if len(purchase_ids)==0:
		# 	vals= {'origin': False, 'message_follower_ids': False,
		# 	 'order_line': [[0, False, {'product_id': product.id, 'product_uom': product.uom_id.id, 'date_planned': datetime.now().strftime("%Y-%m-%d"),
		# 	  'price_unit': product.standard_price, 'taxon_id': product.taxon.id, 'taxes_id': [[6, False, tax]], 'product_qty': qty, 
		# 	  'account_analytic_id': False, 'manu_cost': product.manu_cost, 'supplier_product_code': product.seller_ids[0].product_code, 
		# 	  'name': product.name_template}]], 'company_id': 1, 'currency_id': 21, 'date_order': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
		# 	   'location_id': 7, 'message_ids': False, 'dest_address_id': False, 'fiscal_position': False, 
		# 	   'picking_type_id': 1, 'partner_id': product.seller_id.id, 'journal_id': 3, 
		# 	   'bid_validity': False, 'pricelist_id': 2, 'incoterm_id': False,
		# 	    'payment_term_id': False, 'partner_ref': False, 'notes': False, 
		# 	    'invoice_method': 'order', 'minimum_planned_date': False,
		# 	     'related_location_id': 7}
		# 	order=self.create(cr, uid, vals, context=context)
		# else:
		# 	vals={'order_line': [[0, False, {'product_id': product.id, 'product_uom': product.uom_id.id, 
		# 	'date_planned': datetime.now().strftime("%Y-%m-%d"), 'price_unit': product.standard_price, 'taxon_id': product.taxon.id,
		# 	 'taxes_id': [[6, False, tax]], 'product_qty': qty,
		# 	  'account_analytic_id': False,'manu_cost':product.manu_cost,
		# 	  'supplier_product_code': product.seller_ids[0].product_name, 'name': product.name_template}]]}
		# 	purchase_obj=self.browse(cr,uid,purchase_ids)
		# 	purchase_obj.write(vals)
	def product_update_test(self,cr,uid,vals,context=None):
		with Environment.manage():
			product_product=self.pool.get('product.product')
			product_template=self.pool.get('product.template')
			new_cr = self.pool.cursor()
			for product in vals['order_line']:
				t=product_product.browse(new_cr,uid,product[2]['product_id']).product_tmpl_id
				if t.purchase_count==0:
					temp_vals={}
					if t.standard_price==0:
						temp_vals.update({'standard_price':product[2]['price_unit']})
					temp_vals.update({'supplier_taxes_id':product[2]['taxes_id']})
					if len(t.seller_ids)==0:
						temp_vals.update({'seller_ids':  [[0, False, {'pricelist_ids': [], 'name': vals['partner_id'], 'sequence': 1, 'company_id': 1, 'delay': 1, 'min_qty': 0, 'product_code': product[2]['name'], 'product_name': product[2]['name']}]]})
					product_template.write(new_cr,uid,t.id,temp_vals,context)
					new_cr.commit()
			new_cr.close()
			return {}
	_columns={
	'is_mod':fields.boolean('MOD')
	}
class purchase_order_line(osv.osv):
	_inherit='purchase.order.line'
	_columns={
	'sale_order':fields.many2one('sale.order', 'Sale Order'),
	}


class add_product_quant(osv.osv):
	_inherit='stock.quant'
	def _calculate_ean(self, cr, uid, ids, name, arg, context=None):
		ean_obj=self.browse(cr,uid,ids) 
		res={}
		for k in ean_obj:
			res.update({k.id:k.product_id.ean13})
		return res
	_columns={
		'purchase_order':fields.many2one('stock.picking','Purchase order'),
		'ean':fields.function(_calculate_ean, type='char',string='EAN of product',method=True,store=True),
	}
	
	def go_to_current_inventory(self, cr, uid, ids, context=None):
		if 1:
			return {
			'name': 'Current Inventory',
			'view_type': 'form',
			'view_mode': 'tree,form',
			'target':'current',
			'res_model': 'product.template',
			'type': 'ir.actions.act_window',
			'context': {'location': self.browse(cr,uid,ids).location_id.id},
			'domain': [('id','=',self.browse(cr,uid,ids).product_id.product_tmpl_id.id)],}

	def create(self,cr,uid,vals,context=None):
		if 'active_id' in context and 'active_model' in context and context['active_model']=='stock.picking':
			owner=self.pool.get('stock.picking').browse(cr,uid,context['active_id']).partner_id.id
			vals.update({'owner_id':owner})
			vals.update({'purchase_order':context['active_id']})
		if 1:
			order=super(add_product_quant, self).create(cr, uid, vals, context=context)
			return order
	def write(self,cr,uid,ids,vals,context=None):
		if 1:
			order=super(add_product_quant, self).write(cr, uid, ids,vals, context=context)
			return order
