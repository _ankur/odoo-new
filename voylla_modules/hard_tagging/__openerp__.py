{
    'name': "Hard Tagging",
    'version': "1.0",
    'author': "Devendra",
    'category': "Tools",
    'depends': ['stock', 'procurement','purchase','crm','sale'],
    'data': [
        'security/hard_tagging_security.xml',
        'wizard/purchase_order_group_view.xml',
        'product_view.xml',
        'purchase_order.xml'
    ],
    'demo': [],
    'installable': True,
    'auto_install': True,
    'application': True,
}