from openerp.osv import fields, osv
import logging
import threading
from openerp.api import Environment
import pdb
from datetime import datetime
_logger = logging.getLogger(__name__)
from collections import defaultdict

class add_purchase_order(osv.osv):
	_inherit='purchase.order'
	def product_update_confirmed(self,cr,uid,ids,context=None):
		product_product=self.pool.get('product.product')
		product_template=self.pool.get('product.template')
		purchase_obj= self.browse(cr,uid,ids)
		for purchase_line_item in purchase_obj.order_line:
			variant=purchase_line_item.product_id
			if len(variant)==0:
				continue
			t=variant.product_tmpl_id
			try:
				temp_vals={}
				if purchase_line_item.manu_cost > 0 :
					variant.write({'manu_cost':purchase_line_item.manu_cost})
				if t.standard_price==0:
					temp_vals.update({'standard_price':purchase_line_item.price_unit})
				if len(t.supplier_taxes_id)==0 and len(purchase_line_item.taxes_id)!=0:
					temp_vals.update({'supplier_taxes_id':[purchase_line_item.taxes_id[0].id]})
				if len(t.taxonomy)==0 and 'taxonomy' in purchase_line_item.taxonomy:
					temp_vals.update({'taxonomy': [[0, False, {'categ_id': purchase_line_item.taxonomy.id}]]})
				if len(t.seller_ids)==0:
					temp_vals.update({'seller_ids':  [[0, False, {'pricelist_ids': [], 'name': purchase_obj.partner_id.id, 
						'sequence': 1,
					 'company_id': 1, 'delay': 30, 'min_qty': 0, 
					 'product_code': purchase_line_item.supplier_product_code, 'product_name': purchase_line_item.supplier_product_code}]]})
				if len(temp_vals) > 0:
					product_template.write(cr,uid,t.id,temp_vals,context)
					cr.commit()
			except:
				continue
	def write(self , cr, uid,ids,vals,context=None):
		res_partner = self.pool.get('res.partner')
		if 'partner_id' in vals:
			partner_id=vals['partner_id']
			vals.pop("partner_id", None)
			supplier_type = res_partner.browse(cr,uid,partner_id).non_consig
			self_obj = self.browse(cr,uid,ids)
			if vals and vals.get('order_line'):
				product_ids = {}
				wrong_products = []
				product_reg = self.pool.get('product.product')
				for line_item in vals.get('order_line'):
					product_id = line_item[2]['product_id']
					product_ids[product_id] = line_item[2]['supplier_product_code']

				product_list = product_reg.browse(cr,uid,product_ids.keys())
				# for product in product_list:
				# 	if product.consignment != supplier_type:
				# 		wrong_products.append(product.name_template)

				if len(wrong_products) != 0:
					raise osv.except_osv(('Warning!'), ("Product/Products does not match Supplier Type"))
			order=super(add_purchase_order, self).write(cr, uid, ids,vals, context=context)
		else:
			order=super(add_purchase_order, self).write(cr, uid, ids,vals, context=context)
		#if 'state' in vals and vals['state']=='confirmed':
		#	self.product_update_confirmed(cr,uid,ids,context)
		return order
	def genrate_purchase_order_for_make_to_order(self,cr,uid,ids,qty,context=None):
		# product=self.pool.get('product.product')
		# product=product.browse(cr,uid,ids)
		# tax=[]
		# for tax_id in product.supplier_taxes_id:
		# 	tax.append(tax_id.id)
		# if not context:
		# 	context = {}
		# is_mod = context.get('mod',False)
		# if is_mod:
		# 	purchase_ids=self.search(cr,uid,[('partner_id','=',product.seller_id.id),('is_mod','=',True),('state','=','draft')])
		# 	if len(purchase_ids) == 0:
		# 		vals={'origin': False,'message_follower_ids': False,'is_mod':True,
		# 		 'order_line': [[0, False, {'sale_order':context['sale_order'],'product_id': product.id, 'product_uom': product.uom_id.id, 'date_planned': datetime.now().strftime("%Y-%m-%d"),
		# 		  'price_unit': product.standard_price, 'taxon_id': product.taxon.id, 'taxes_id': [[6, False, tax]], 'product_qty': qty, 
		# 		  'account_analytic_id': False, 'manu_cost': product.manu_cost, 'supplier_product_code': product.seller_ids[0].product_code, 
		# 		  'name': product.name_template}]], 'company_id': 1, 'currency_id': 21, 'date_order': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
		# 		   'location_id': 7, 'message_ids': False, 'dest_address_id': False, 'fiscal_position': False, 
		# 		   'picking_type_id': 1, 'partner_id': product.seller_id.id, 'journal_id': 3, 
		# 		   'bid_validity': False, 'pricelist_id': 2, 'incoterm_id': False,
		# 		    'payment_term_id': False, 'partner_ref': False, 'notes': False, 
		# 		    'invoice_method': 'order', 'minimum_planned_date': False,
		# 		     'related_location_id': 7,}
		# 		order=self.create(cr, uid, vals, context=context)
		# 		return
		# 	else:
		# 		vals={'order_line': [[0, False, {'sale_order':context['sale_order'],'product_id': product.id, 'product_uom': product.uom_id.id, 
		# 		'date_planned': datetime.now().strftime("%Y-%m-%d"), 'price_unit': product.standard_price, 'taxon_id': product.taxon.id,
		# 		 'taxes_id': [[6, False, tax]], 'product_qty': qty,
		# 		  'account_analytic_id': False,'manu_cost':product.manu_cost,
		# 		  'supplier_product_code': product.seller_ids[0].product_name, 'name': product.name_template}]]}
		# 		purchase_obj=self.browse(cr,uid,purchase_ids[0])
		# 		purchase_obj.write(vals)	 
		# 		return
		# purchase_ids=self.search(cr,uid,[('partner_id','=',product.seller_id.id),('state','=','draft')])
		# if len(purchase_ids)==0:
		# 	vals= {'origin': False, 'message_follower_ids': False,
		# 	 'order_line': [[0, False, {'product_id': product.id, 'product_uom': product.uom_id.id, 'date_planned': datetime.now().strftime("%Y-%m-%d"),
		# 	  'price_unit': product.standard_price, 'taxon_id': product.taxon.id, 'taxes_id': [[6, False, tax]], 'product_qty': qty, 
		# 	  'account_analytic_id': False, 'manu_cost': product.manu_cost, 'supplier_product_code': product.seller_ids[0].product_code, 
		# 	  'name': product.name_template}]], 'company_id': 1, 'currency_id': 21, 'date_order': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
		# 	   'location_id': 7, 'message_ids': False, 'dest_address_id': False, 'fiscal_position': False, 
		# 	   'picking_type_id': 1, 'partner_id': product.seller_id.id, 'journal_id': 3, 
		# 	   'bid_validity': False, 'pricelist_id': 2, 'incoterm_id': False,
		# 	    'payment_term_id': False, 'partner_ref': False, 'notes': False, 
		# 	    'invoice_method': 'order', 'minimum_planned_date': False,
		# 	     'related_location_id': 7}
		# 	order=self.create(cr, uid, vals, context=context)
		# else:
		# 	vals={'order_line': [[0, False, {'product_id': product.id, 'product_uom': product.uom_id.id, 
		# 	'date_planned': datetime.now().strftime("%Y-%m-%d"), 'price_unit': product.standard_price, 'taxon_id': product.taxon.id,
		# 	 'taxes_id': [[6, False, tax]], 'product_qty': qty,
		# 	  'account_analytic_id': False,'manu_cost':product.manu_cost,
		# 	  'supplier_product_code': product.seller_ids[0].product_name, 'name': product.name_template}]]}
		# 	purchase_obj=self.browse(cr,uid,purchase_ids)
		# 	purchase_obj.write(vals)
		failure_notification_registry = self.pool.get('failure.notification')
		product=self.pool.get('product.product')
		sale_order_object=self.pool.get('sale.order')
		order_ids=sale_order_object.browse(cr,uid,sale_order_object.search(cr,uid,[('id','=',context['sale_order'])])).name
		bom_reg = self.pool.get('mrp.bom')
		product=product.browse(cr,uid,ids)
		tax=[]
		for tax_id in product.supplier_taxes_id:
			tax.append(tax_id.id)
		if not context:
			context = {}
		is_mod = context.get('mod',False)
		sale_obj = self.pool.get('sale.order').browse(cr,uid,[context.get('sale_order')])
		try:
			if is_mod:
				mrp_bom_obj = bom_reg.browse(cr,uid,bom_reg.search(cr,uid,[('product_id','=',product.id)]))

				if mrp_bom_obj and len(mrp_bom_obj[0].bom_line_ids) > 1:
					purchase_group = defaultdict(list)
					for component in mrp_bom_obj[0].bom_line_ids:
						purchase_group[component.product_id.seller_id.id].append([0, False, {'sale_order':context['sale_order'],'product_id': component.product_id.id, 'product_uom': component.product_id.uom_id.id, 'date_planned': datetime.now().strftime("%Y-%m-%d"),'price_unit': component.product_id.standard_price, 'taxon_id': component.product_id.taxon.id, 'taxes_id': [[6, False, tax]], 'product_qty': qty, 	'account_analytic_id': False, 'manu_cost': component.product_id.manu_cost, 'supplier_product_code': component.product_id.seller_ids[0].product_code, 'name': component.product_id.name_template}])
					
					for purchase_partner in purchase_group.keys():
						purchase_id = self.search(cr,uid,[('partner_id','=',purchase_partner),('is_mod','=',True),('state','=','draft')])
						if len(purchase_id) == 0:				
							vals={'origin': False,'message_follower_ids': False,'is_mod':True,
							 'order_line': purchase_group[purchase_partner], 'company_id': 1, 'currency_id': 21, 'date_order': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
							   'location_id': 7, 'message_ids': False, 'dest_address_id': False, 'fiscal_position': False, 
							   'picking_type_id': 1, 'partner_id': purchase_partner, 'journal_id': 3, 
							   'bid_validity': False, 'pricelist_id': 2, 'incoterm_id': False,
								'payment_term_id': False, 'partner_ref': 'MOD', 'notes': False, 
								'invoice_method': 'order', 'minimum_planned_date': False,
								 'related_location_id': 7}
							order=self.create(cr, uid, vals, context=context)
						else:
							vals={'order_line': purchase_group[purchase_partner], 'partner_id': purchase_partner ,'partner_ref':'MOD'}
							purchase_obj=self.browse(cr,uid,purchase_id[0])
							purchase_obj.write(vals)	 

				else:
					purchase_ids=self.search(cr,uid,[('partner_id','=',product.seller_id.id),('is_mod','=',True),('state','=','draft')])
					order_line = [[0, False, {'sale_order':context['sale_order'],'product_id': product.id, 'product_uom': product.uom_id.id, 'date_planned': datetime.now().strftime("%Y-%m-%d"),
					  'price_unit': product.standard_price, 'taxon_id': product.taxon.id, 'taxes_id': [[6, False, tax]], 'product_qty': qty, 
					  'account_analytic_id': False, 'manu_cost': product.manu_cost, 'supplier_product_code': product.seller_ids[0].product_code, 
					  'name': product.name_template}]]
					if len(purchase_ids) == 0:				
						vals={'origin': False,'message_follower_ids': False,'is_mod':True,
						 'order_line': order_line, 'company_id': 1, 'currency_id': 21, 'date_order': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
						   'location_id': 7, 'message_ids': False, 'dest_address_id': False, 'fiscal_position': False, 
						   'picking_type_id': 1, 'partner_id': product.seller_id.id, 'journal_id': 3, 
						   'bid_validity': False, 'pricelist_id': 2, 'incoterm_id': False,
							'payment_term_id': False, 'partner_ref': 'MOD', 'notes': False, 
							'invoice_method': 'order', 'minimum_planned_date': False,
							 'related_location_id': 7}
						order=self.create(cr, uid, vals, context=context)
					else:
						vals={'order_line': order_line, 'partner_id':  product.seller_id.id , 'partner_ref':'MOD'}
						
						purchase_obj=self.browse(cr,uid,purchase_ids[0])
						purchase_obj.write(vals)	
		except Exception,e:
			vals = {"name": "Failed to add product to order %s" %(order_ids), "type":"order_line_creation", "description":"Wrong Product/Supplier on MOD for product %s" %(product.name_template), "order_id":order_ids }
			failure_notification_registry.create(cr, 1, vals)
			pass
			
	def product_update_test(self,cr,uid,vals,context=None):
		with Environment.manage():
			product_product=self.pool.get('product.product')
			product_template=self.pool.get('product.template')
			new_cr = self.pool.cursor()
			for product in vals['order_line']:
				t=product_product.browse(new_cr,uid,product[2]['product_id']).product_tmpl_id
				if t.purchase_count==0:
					temp_vals={}
					if t.standard_price==0:
						temp_vals.update({'standard_price':product[2]['price_unit']})
					temp_vals.update({'supplier_taxes_id':product[2]['taxes_id']})
					if len(t.seller_ids)==0:
						temp_vals.update({'seller_ids':  [[0, False, {'pricelist_ids': [], 'name': vals['partner_id'], 'sequence': 1, 'company_id': 1, 'delay': 1, 'min_qty': 0, 'product_code': product[2]['name'], 'product_name': product[2]['name']}]]})
					product_template.write(new_cr,uid,t.id,temp_vals,context)
					new_cr.commit()
			new_cr.close()
			return {}

	def create(self, cr, uid, vals, context=None):
		product = self.pool.get('product.product')
		res_partner = self.pool.get('res.partner')
		product_supplier_reg = self.pool.get('product.supplierinfo')
		# supplier_type = res_partner.browse(cr,uid,vals['partner_id']).non_consig
		order_line = vals['order_line']
		product_ids = {}
		wrong_products = []
		for line_item in order_line:
			product_id = line_item[2]['product_id']
			product_ids[product_id] = line_item[2]['supplier_product_code']
			
		product_list = product.browse(cr,uid,product_ids.keys())
		# for product in product_list:
		# 	if product.consignment != supplier_type:
		# 		wrong_products.append(product.name_template)

		if len(wrong_products) != 0:
			raise osv.except_osv(('Warning!'), ("Product/Products does not match Supplier Type"))
		for line in product_list:
			if line.seller_ids:
				product_supplier_reg.write(cr,uid,line.seller_ids[0].id,{'name':vals['partner_id'],'company_id':vals['company_id'],'delay':1,'min_qty':0.0,'product_tmpl_id':line.product_tmpl_id.id,'product_code':product_ids[line.id]})
			else:
				product_supplier_reg.create(cr,uid,{'name':vals['partner_id'],'company_id':vals['company_id'],'delay':1,'min_qty':0.0,'product_tmpl_id':line.product_tmpl_id.id,'product_code':product_ids[line.id]})
		po = super(add_purchase_order, self).create(cr, uid, vals, context=context)
		return po

	_columns={
	'is_mod':fields.boolean('MOD')
	}
class purchase_order_line(osv.osv):
	_inherit='purchase.order.line'
	_columns={
	'sale_order':fields.many2one('sale.order', 'Sale Order'),
	}


class add_product_quant(osv.osv):
	_inherit='stock.quant'
	def _calculate_ean(self, cr, uid, ids, name, arg, context=None):
		ean_obj=self.browse(cr,uid,ids) 
		res={}
		for k in ean_obj:
			res.update({k.id:k.product_id.ean13})
		return res
	_columns={
		'purchase_order':fields.many2one('stock.picking','Purchase order'),
		'ean':fields.function(_calculate_ean, type='char',string='EAN of product',method=True,store=True),
	}
	
	def go_to_current_inventory(self, cr, uid, ids, context=None):
		if 1:
			return {
			'name': 'Current Inventory',
			'view_type': 'form',
			'view_mode': 'tree,form',
			'target':'current',
			'res_model': 'product.template',
			'type': 'ir.actions.act_window',
			'context': {'location': self.browse(cr,uid,ids).location_id.id},
			'domain': [('id','=',self.browse(cr,uid,ids).product_id.product_tmpl_id.id)],}

	def create(self,cr,uid,vals,context=None):
		if 'active_id' in context and 'active_model' in context and context['active_model']=='stock.picking':
			owner=self.pool.get('stock.picking').browse(cr,uid,context['active_id']).partner_id.id
			vals.update({'owner_id':owner})
			vals.update({'purchase_order':context['active_id']})
		if 1:
			order=super(add_product_quant, self).create(cr, uid, vals, context=context)
			return order
	def write(self,cr,uid,ids,vals,context=None):
		if 1:
			order=super(add_product_quant, self).write(cr, uid, ids,vals, context=context)
			return order
