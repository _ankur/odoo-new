# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import time
import subprocess
import shlex
import os
import pdb
import csv
import xlwt
import psycopg2
from datetime import datetime
from openerp.osv import fields, osv
from openerp import netsvc
from openerp import pooler
from openerp.osv.orm import browse_record, browse_null
from openerp.tools.translate import _
from voylla_modules.config import home
class temp_product(osv.osv):
    _name="temp.product"
    _columns={
    'product':fields.many2one('product.product','Product',readonly=True),
    'qty':fields.integer('Quantity',readonly=True),
    'qty_accept':fields.integer('Accepted Quantity'),
    'name':fields.char('po name'),
    'po_id':fields.integer('purchase order'),
    'flag':fields.boolean('processed'),
    'remark':fields.char('Remark')
    }
    _defults={
    'po_id':1,
    'flag':False
    }
    def write(self,cr,uid,ids,vals,context=None):
        if 'qty_accept' in vals and 'qty' not in vals:
            temp_obj=self.browse(cr,uid,ids)
            if vals['qty_accept'] > temp_obj.qty:
                text=temp_obj.product.name_template
                raise osv.except_osv('Warning!',('this product %s has less quantity') % (text,))
        temp_id=super(temp_product,self).write(cr,uid,ids,vals,context=context)
        return temp_id

    def move_orders(self, cr, uid, ids,qty_accept,context=None):
        print True
        
temp_product()

class purchase_order_group(osv.osv_memory):
    _name = "purchase.order.group"
    _description = "Purchase Order Merge"
    def _sel_selection(self,cr,uid,context=None):
        if context.get('for_return_challan',False):
            return [('39','Suppliers')] 
        user_group=self.pool.get('res.groups')
        ll=self.pool.get('res.users').read(cr,uid,[uid],['groups_id'])
        temp=ll[0]['groups_id']
        inbound_id=user_group.search(cr,uid,[('name','=','Inbound User')])
        other_id=user_group.search(cr,uid,[('name','in',('Measurement User','Photoshoot User','Merchandising User',
            'Hard Tagging User','Quality Control User','Content User'))])
        if inbound_id[0] in temp:
            return [('8','Inbound Rejected'),('17','merchandising'),
                      ('11','content'),('26','Dimension'),('14','photoshoot'),('4','QC'),
                      ('30','warehouse'),('23','HardTagging'),('7','Inbound'), ('39','Suppliers')]
        elif len( list(set(temp).intersection(other_id)))!=0:
            return [('8','Inbound Rejected'),('7','Inbound')]
        else:
            return []
    _columns={
        'dest':fields.selection(_sel_selection,'Location to move',required=True),
        'temp':fields.one2many('temp.product','po_id')
    
    }

    def fields_view_get(self, cr, uid, view_id=None, view_type='form',
                        context=None, toolbar=False, submenu=False):
        """
         Changes the view dynamically
         @param self: The object pointer.
         @param cr: A database cursor
         @param uid: ID of the user currently logged in
         @param context: A standard dictionary
         @return: New arch of view.
        """
        if context is None:
            context={}
        res = super(purchase_order_group, self).fields_view_get(cr, uid, view_id=view_id, view_type=view_type, context=context, toolbar=toolbar,submenu=False)
        if context.get('active_model','') == 'purchase.order' and len(context['active_ids']) > 1:
            raise osv.except_osv(_('Warning!'),
            _('You can move one PO at a time'))
        return res

    def on_change(self, cr, uid, ids,dest,context=None):
        temp_obj=self.pool.get('temp.product')
        po=self.pool.get('purchase.order').browse(cr,uid,context['active_ids'])
        stock_picking=self.pool.get('stock.picking')
        stock_picking_id=stock_picking.search(cr,uid,[('origin','=',po.name),('state','in',('done','confirmed'))])
        stock_picking_obj=stock_picking.browse(cr,uid,stock_picking_id)
        def move_ids(x):
            return x.move_lines
        temp=map(move_ids,stock_picking_obj)
        print temp
        if len(temp)==0:
            temp_vals = {'temp': temp_obj.browse(cr,uid,[])}  
            return {'value':temp_vals}
        temp=reduce(lambda x,y:x+y,temp)
        available_product={}
        for stock_moves in temp:
            if stock_moves.state in ['confirmed','done','assigned'] and stock_moves.location_id.id==context['location_id']:
                if stock_moves.product_id.id in available_product:
                    available_product[stock_moves.product_id.id]-=stock_moves.product_qty
                else:
                    available_product.update({stock_moves.product_id.id: -stock_moves.product_qty})
            elif stock_moves.state in ['done'] and stock_moves.location_dest_id.id==context['location_id']:
                if stock_moves.product_id.id in available_product:
                    available_product[stock_moves.product_id.id]+=stock_moves.product_qty
                else:
                    available_product.update({stock_moves.product_id.id:stock_moves.product_qty})

        temp_po_array=[]
        temp_obj1=[]
        for item in available_product:
            vals={'product':item,'name':po.name,'qty':available_product[item],'create_uid':uid}
            temp_po_array1=temp_obj.search(cr,uid,[('product','=',item),('name','=',po.name),('create_uid','=',uid),('flag','=',False)])
            if len(temp_po_array1)==0:
                vals.update({'qty_accept':available_product[item]})
                temp_po_array.append(temp_obj.create(cr,uid,vals))
            else:
                temp_obj.write(cr,uid,temp_po_array1[0],{'qty_accept':available_product[item],'qty':available_product[item]})
                temp_po_array.append(temp_po_array1[0])
        vals = {'temp': temp_obj.browse(cr,uid,temp_po_array)}  
        return {'value': vals}

    def move_orders(self, cr, uid, ids, context=None):
        purchase_order = self.pool.get('purchase.order')
        po_obj1=purchase_order.browse(cr,uid,context['active_ids'])
        purchase_order_group = self.pool.get('purchase.order.group')
        temp_dest=purchase_order_group.read(cr, uid, context['res_id'],['temp','dest'] ,context=context)
        stock_move=self.pool.get('stock.move')
        temp_product=self.pool.get('temp.product')
        purchase_line_item_move=temp_product.browse(cr,uid,temp_dest['temp'])
        stock_picking=self.pool.get('stock.picking')
        stock_picking_id=stock_picking.search(cr,uid,[('origin','=',po_obj1.name),('state','in',('done','confirmed'))])
        check=temp_product.search(cr,uid,[('name','=',po_obj1.name),('create_uid','=',uid),('flag','=',False)])
        purchase_line_item_move=temp_product.browse(cr,uid,check)
        for product_order in purchase_line_item_move:
            if product_order.qty_accept ==0:
                continue
            c1={}
            c1={'lang': 'en_US', 'tz': False, 'uid': uid, 'invoice_state': None, 'procure_method': 'make_to_stock', 'search_default_future': True}
            vals={}
            vals={'state':'confirmed','product_uom':1,'company_id':1,
            'location_dest_id':int(temp_dest['dest']),'location_id':context['location_id'],
            'product_id':product_order.product.id,'name':product_order.product.name,'origin':po_obj1.name,'picking_id':stock_picking_id[0],
            'date_expected':datetime.now().strftime('%Y-%m-%d %H:%M:%S'),'date':datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
            'product_uom_qty':product_order.qty_accept}
            stock_move.create(cr, uid,vals, context=c1)
        temp_product.write(cr,uid,check,{'flag':True})

    def genrate_return_file(self, cr, uid,ids, context=None):
        purchase_order = self.pool.get('purchase.order')
        product_product=self.pool.get('product.product')
        product_template= self.pool.get('product.template')
        po_obj1=purchase_order.browse(cr,uid,context['active_ids'])
        stock_move=self.pool.get('stock.move')
        stock_picking=self.pool.get('stock.picking')
        inbound_rejected_products = stock_move.search(cr,uid,[('origin','=',po_obj1.name),('location_id','=',8)])
        stock_picking_id=stock_picking.search(cr,uid,[('origin','=',po_obj1.name),('state','in',('done','confirmed'))])
        temp_product=self.pool.get('temp.product')
        check3=temp_product.search(cr,uid,[('name','=',po_obj1.name),('flag','=',False),('create_uid','=',uid)])
        purchase_line_item_move=temp_product.browse(cr,uid,check3)    # for product in check3:
        temp_product.write(cr,uid,check3,{'flag':True})
        cr.commit()
        for stock_id in inbound_rejected_products:
            stock_move_po_obj = self.pool.get('stock.move').browse(cr,uid,stock_id)
            stock_moved_product = stock_move_po_obj.product_id.id
            foo = False
            for item in purchase_line_item_move:
                if item.product.id == stock_moved_product:
                    foo = True
                    xtra = item.id
                    break
            if foo==True:
                check3.remove(xtra)
        purchase_line_item_move=temp_product.browse(cr,uid,check3)  # updated list

        style_0 = xlwt.easyxf('font: name Times New Roman, bold on; align: wrap on,vert centre;')
        wb = xlwt.Workbook()
        ws = wb.add_sheet('Return Challan')
        ws.write_merge(0, 2, 4, 7,'*TIN NO: \n PAN NO: \n DATE : %s' %datetime.now(), style_0)
        ws.write_merge(3, 8, 0, 3,'FROM: \nVoylla Retail Pvt Ltd, \nE-521, RIICO Industrial Area,\nNear Chatrala Circle, Sitapura\nJaipur, Raj-302022\nPh.No-7676111022', style_0)
        ws.write_merge(3, 8, 4, 7,'TO:%s' % po_obj1.partner_id.contact_address, style_0)
        col=9           # for write in praticular column
        ws.write(col, 0, 'S.NO', style_0)
        ws.write(col, 1, 'Voylla Code', style_0)
        ws.write(col, 2, 'Description', style_0)
        ws.write(col, 3, 'SIZE', style_0)
        ws.write(col, 4, 'QTY', style_0)
        ws.write(col, 5, 'PRICE', style_0)
        ws.write(col, 6, 'AMOUNT', style_0)
        ws.write(col, 7, 'REMARK', style_0)
        temp_hash={}
        for line_item in po_obj1.order_line:
            temp_hash.update({line_item.product_id.id:[line_item.price_unit,line_item.supplier_product_code]}) 
        supplier_product_qty={}
        for val,product_qty in enumerate(purchase_line_item_move):
            if product_qty in supplier_product_qty:
                supplier_product_qty[product_qty.product.id]=product_qty.qty
            else:
                supplier_product_qty.update({product_qty.product.id:product_qty.qty})

        qty1=0
        amt=0
        col_nxt=col+1           # to write string "Total"
        values_pass={}
        vals_list=[]
        index=0                 
        index1=-1
        for val,product_order in enumerate(purchase_line_item_move):
            index1+=1
            variant=product_order.product
            variant_size=variant.get_product_size()
            if len(variant_size)!=0:
                if 'size' in variant_size:
                    product_size=variant_size['size']
            else:
                product_size='N/A'
            if supplier_product_qty[product_order.product.id]==0:
                index1=-1
            else:     
                if product_order.product.id in temp_hash:
                    ws.write(index1+col_nxt, 2, temp_hash[product_order.product.id][1])
                    ws.write(index1+col_nxt, 5, temp_hash[product_order.product.id][0])
                    ws.write(index1+col_nxt, 6, supplier_product_qty[product_order.product.id]*temp_hash[product_order.product.id][0])
                    amt+=(supplier_product_qty[product_order.product.id]*temp_hash[product_order.product.id][0])
               
                else:
                    ws.write(index1+col_nxt, 2, product_order.product.supplier_product_code) # prevent rest of code to get effected
                ws.write(index1+col_nxt, 0, val+1)
                ws.write(index1+col_nxt, 1, product_order.product.name)
                ws.write(index1+col_nxt, 3, product_size)
                ws.write(index1+col_nxt, 4, supplier_product_qty[product_order.product.id])
                qty1+=supplier_product_qty[product_order.product.id]
                remark = product_order.remark
                if not remark:
                    remark = None
                ws.write(index1+col_nxt, 7,remark)
                vals_temp=[0, False,{'remark':product_order.remark, 'product_id':product_order.product.id, 'qty':supplier_product_qty[product_order.product.id]}]
                vals_list.append(vals_temp)

            index=index1+1
        values_pass.update({'return_challan_line':vals_list,'supplier_id':po_obj1.partner_id.id,'reference':po_obj1.id})
        
        ws.write(index+col_nxt+2, 3, 'TOTAL', style_0)
        ws.write(index+col_nxt+2, 4, qty1, style_0)
        ws.write(index+col_nxt+2, 6, amt, style_0)
        ws.write_merge(index+col_nxt+3, index+col_nxt+5, 0, 7,'Signature & Seal', style_0)

        for product in supplier_product_qty:
            cntxt={}
            cntxt={'lang': 'en_US', 'tz': 'Asia/Kolkata', 'uid': 1, 'active_model': 'purchase.order', 'res_id': 2581, 'search_disable_custom_filters': True, 'active_ids': [2991], 'location_id': 8, 'active_id': 2991}
            vals1={}
            vals1={'state':'confirmed','product_uom':1,'company_id':1,
            'location_dest_id':39,'location_id':8,
            'product_id':product,'name':product_order.product.name,'origin':po_obj1.name,'picking_id':stock_picking_id[0],
            'date_expected':datetime.now().strftime('%Y-%m-%d %H:%M:%S'),'date':datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
            'product_uom_qty': supplier_product_qty[product]}
            stock_move_id=stock_move.create(cr, uid,vals1, context=cntxt)
            #stock_move.action_done(cr,uid,stock_move_id,context=cntxt)
        return_challan = self.pool.get('return.challan.record')
        return_ids=return_challan.create(cr,uid,values_pass)
        name_of_challan = return_challan.browse(cr,uid,return_ids).name
        ws.write_merge(0, 2, 0, 3,'%s \nSupplier:-%s \nPurchase Order NO :-%s' % (name_of_challan,po_obj1.partner_id.name,po_obj1.name), style_0)
        wb.save('Return_Challan.xls')
        
        feed='Return_Challan.xls'
        c1="s3cmd put "+ feed +" s3://voyllaerp/erp_tag/"
        args = shlex.split(c1)
        p = subprocess.Popen(args)
        p.wait()
        c2="s3cmd setacl s3://voyllaerp/erp_tag/"+feed+" --acl-public"
        args = shlex.split(c2)
        subprocess.Popen(args)
        p.wait()
        url_base = 'https://s3.amazonaws.com/voyllaerp/erp_tag/'+feed
        vals={'name':feed,'type':'url','url':url_base}
        attachment_id=int(self.pool.get('ir.attachment').create(cr,uid,vals))
        return {
            'domain':"[('id','=',%i)]"%attachment_id,
            'actions':'check_action_attachment',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'ir.attachment',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'search_view_id':attachment_id
        }

    def genrate_file(self, cr, uid, ids, context=None):
        purchase_order = self.pool.get('purchase.order')
        product_product=self.pool.get('product.product')
        product_template= self.pool.get('product.template')
        po_obj=purchase_order.browse(cr,uid,context['active_ids'])
        temp_product=self.pool.get('temp.product')
        check=temp_product.search(cr,uid,[('name','=',po_obj.name),('create_uid','=',uid),('flag','=',False)])
        purchase_line_item_move=temp_product.browse(cr,uid,check)
        header=['Product code','Category','mrp','size','Batch number','EAN code','Master EAN','diamond','gold']
        d=[]
        d.append(header)
        for product_order in purchase_line_item_move:
            a=['']*9
            i=product_order.qty_accept
            if i < 1:
                continue
            variant=product_order.product
            a[0]=variant.name
            if len(variant.taxonomy)==0:
                raise osv.except_osv('Warning!',(' %s taxon not there') % (variant.name_template,))
            else:
                complete_name=str(variant.taxonomy[0].categ_id[0].complete_name)
                if len(complete_name.split('/')) < 2:
                    raise osv.except_osv('Warning!',(' %s taxon not available %s name ') % (variant.name_template,variant.taxonomy[0].categ_id[0].complete_name))
                else:
                    a[1]=complete_name.split('/')[1].strip()
            a[2]=variant.mrp
            a[4]=po_obj.name
            variant_size=variant.get_product_size()
            product_master_ean=variant.product_tmpl_id.masterean
            product_threepnumber=variant.product_tmpl_id.threep_reference
            variant_ean=variant.ean13
            variant_threepnumber=variant.threep_reference
            if len(variant_size)!=0:
                if 'size' in variant_size: 
                    a[3]=variant_size['size']
                if 'diamond' in variant_size: 
                    a[7]=variant_size['diamond']
                if 'gold' in variant_size: 
                    a[8]=variant_size['gold']
                if variant_ean not in [False,'']:
                    a[5]=variant_ean
                    if product_master_ean==variant_ean:
                        a[5]=product_template.generate_next_ean13_code(cr,uid,context)
                        variant.ean13=a[5]
                else:
                    a[5]=product_template.generate_next_ean13_code(cr,uid,context)
                    variant.ean13=a[5]
                if variant_threepnumber in [False,'']:
                    variant.threep_reference=a[5]
                elif variant_threepnumber=='N':
                    if 'size' in variant_size:
                        if variant_size['size'].lower()=="m" or variant_size['size'].lower()=="adjustable" or variant_size['size'].lower()=="free size":
                            variant.threep_reference=a[0]
                        else:
                            variant.threep_reference=a[0]+'_'+a[3]
                if product_master_ean not in [ False,'']:
                    a[6]=product_master_ean
                else:
                    a[6]=product_template.generate_next_ean13_code(cr,uid,context)
                    variant.product_tmpl_id.masterean=a[6]
                if product_threepnumber in [False,'']:
                    variant.product_tmpl_id.threep_reference=a[6]
                elif product_threepnumber=='N':
                    variant.product_tmpl_id.threep_reference=a[0]
                cr.commit()
            else:
                if variant_ean not in [ False ,'']:
                    a[6]=variant_ean
                else:
                    if product_master_ean in [False,'']:
                        a[6]=product_template.generate_next_ean13_code(cr,uid,context)
                        variant.ean13=a[6]
                    else:
                        a[6]=product_master_ean
                        variant.ean13=product_master_ean
                if product_master_ean in [False,''] or product_master_ean!=a[6]:
                    variant.product_tmpl_id.masterean=a[6]
                if product_threepnumber=='N':
                    variant.threep_reference=a[0]
                    variant.product_tmpl_id.threep_reference=a[0]
                elif product_threepnumber in [False,'']:
                    variant.threep_reference=a[6]
                    variant.product_tmpl_id.threep_reference=a[6]   
                else:
                    variant.threep_reference=product_threepnumber
                a[5]=a[6]
                cr.commit()
            while i > 0:
                d.append(a)
                i-=1
        k1=datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
        k=po_obj.name+k1+'.csv'
        k2=str(home+'/import/'+k)
        with open(k2, "wb") as f:
            writer = csv.writer(f)
            writer.writerows(d)
            f.close()
        url_base='https://s3.amazonaws.com/voyllaerp/erp_tag/'+k
        vals={'name':po_obj.name,'type':'url','url':url_base}
        attachment_id=int(self.pool.get('ir.attachment').create(cr,uid,vals))
        c1="s3cmd put "+ k2 +" s3://voyllaerp/erp_tag/"
        args = shlex.split(c1)
        p = subprocess.Popen(args)
        p.wait()
        c2="s3cmd setacl s3://voyllaerp/erp_tag/"+k+" --acl-public"
        args = shlex.split(c2)
        subprocess.Popen(args)
        return {
            'domain':"[('id','=',%i)]"%attachment_id,
            'actions':'check_action_attachment',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'ir.attachment',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'search_view_id':attachment_id 
        }

            
    def merge_orders(self, cr, uid, ids, context=None):
        """
             To merge similar type of purchase orders.

             @param self: The object pointer.
             @param cr: A database cursor
             @param uid: ID of the user currently logged in
             @param ids: the ID or list of IDs
             @param context: A standard dictionary

             @return: purchase order view

        """
        order_obj = self.pool.get('purchase.order')
        proc_obj = self.pool.get('procurement.order')
        mod_obj =self.pool.get('ir.model.data')
        if context is None:
            context = {}
        result = mod_obj._get_id(cr, uid, 'purchase', 'view_purchase_order_filter')
        id = mod_obj.read(cr, uid, result, ['res_id'])

        allorders = order_obj.do_merge(cr, uid, context.get('active_ids',[]), context)
        for new_order in allorders:
            proc_ids = proc_obj.search(cr, uid, [('purchase_id', 'in', allorders[new_order])], context=context)
            for proc in proc_obj.browse(cr, uid, proc_ids, context=context):
                if proc.purchase_id:
                    proc_obj.write(cr, uid, [proc.id], {'purchase_id': new_order}, context)

        return {
            'domain': "[('id','in', [" + ','.join(map(str, allorders.keys())) + "])]",
            'name': _('Purchase Orders'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'purchase.order',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'search_view_id': id['res_id']
        }

purchase_order_group()

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
