from openerp.osv import fields, osv
import time
import pdb
import csv
import logging
_logger = logging.getLogger(__name__)

class account_fiscal_position_rule(osv.osv):

    _name = "account.fiscal.position.rule"
    _description = 'Rules for account fiscal position'

    _columns = {
        "name" : fields.char("Rule", required=True),
        'description': fields.char('Description', size=128),
        'company_id': fields.many2one('res.company', 'Company', required=True, select=True),
        "warehouse_id": fields.many2one('stock.warehouse','Warehouse', domain="[('company_id','=',company_id)]"),
        'fiscal_position_id': fields.many2one( 'account.fiscal.position', 'Fiscal Position', domain="[('company_id','=',company_id)]", select=True),
        'date_start': fields.date('Start Date', help="Starting date for this rule to be valid."),
        'date_end': fields.date('End Date', help="Ending date for this rule to be valid."),
        'to_invoice_country': fields.many2one('res.country', 'Invoice Country'),
        'to_invoice_state': fields.many2one('res.country.state', 'Invoice State', domain="[('country_id','=',to_invoice_country)]"),
        'to_shipping_country': fields.many2one('res.country', 'Destination Country'),
        'to_shipping_state': fields.many2one('res.country.state', 'Destination State', domain="[('country_id','=',to_shipping_country)]"),
        'use_sale': fields.boolean('Use in sales order'),
        # 'use_invoice': fields.boolean('Use in Invoices'),
        # 'use_purchase': fields.boolean('Use in Purchases'),
        # 'sequence': fields.integer('Priority', required=True, help='The lowest number will be applied.'),
    }
    # _defaults = {
    #     'sequence': 10,
    # }


    def fiscal_position_map(self, cr, uid, company_id=None, partner_id=None, partner_shipping_id=None, partner_invoice_id=None, context=None, **kwargs):
        # fiscal_position_map(cr, uid, company_id, partner_id, delivery_id, context=context)

        result = {'fiscal_position': False}
        if not partner_id or not company_id:
            return result

        obj_fsc_rule = self.pool.get('account.fiscal.position.rule')
        obj_partner = self.pool.get("res.partner")
        obj_company = self.pool.get("res.company")
        partner = obj_partner.browse(cr, uid, partner_id, context=context)
        company = obj_company.browse(cr, uid, company_id, context=context)

        #Case 1: Partner Specific Fiscal Position
        if partner.property_account_position:
            result['fiscal_position'] = partner.property_account_position.id
            return result

        #Case 2: Rule based determination
        addrs = {}
        if partner_invoice_id:
            addrs['invoice'] = obj_partner.browse(cr, uid, partner_invoice_id, context=context)

        else:
            partner_addr = partner.address_get(['invoice'])
            addr_id = partner_addr['invoice'] and partner_addr['invoice'] or None
            if addr_id:
                addrs['invoice'] = obj_partner.browse(cr, uid, addr_id, context=context)
        if partner_shipping_id:
            addrs['shipping'] = obj_partner.browse(cr, uid, partner_shipping_id, context=context)

        domain = self._map_domain(cr, uid, partner, addrs, company, context, **kwargs)


        fsc_pos_id = self.search(cr, uid, domain, context=context)

        if fsc_pos_id:
            fsc_rule = obj_fsc_rule.browse(cr, uid, fsc_pos_id, context=context)[0]
            result['fiscal_position'] = fsc_rule.fiscal_position_id.id

        return result


    def _map_domain(self, cr, uid, partner, addrs, company,
                    context=None, **kwargs):
        if context is None:
            context = {}

        document_date = context.get('date', time.strftime('%Y-%m-%d'))
        use_domain = context.get('use_domain', ('use_sale', '=', True))

        domain = ['&', ('company_id', '=', company.id), use_domain,
                '|', ('date_start', '=', False), ('date_start', '<=', document_date),
                '|', ('date_end', '=', False), ('date_end', '>=', document_date),]

        for address_type, address in addrs.items():
            key_country = 'to_%s_country' % address_type
            key_state = 'to_%s_state' % address_type
            to_country = address.country_id.id or False
            domain += ['|', (key_country, '=', to_country),
                (key_country, '=', False)]
            to_state = address.state_id.id or False
            domain += ['|', (key_state, '=', to_state),
                (key_state, '=', False)]

        return domain


class account_fiscal_position(osv.osv):

    _inherit = "account.fiscal.position"

    _columns = {
    "warehouse_id": fields.many2one('stock.warehouse','Warehouse'),
    "state": fields.many2one('res.country.state','Destination State', domain="[('country_id','=',country_id)]"),
    "state_not": fields.many2one('res.country.state','Exclude Destination State', domain="[('country_id','=',country_id)]"),
    "country_not": fields.many2one('res.country','Exclude Destination Country'),
    }

    def get_fiscal_position(self, cr, uid, company_id, partner_id, delivery_id=None, context=None):
        if not partner_id:
            return False
        # This can be easily overriden to apply more complex fiscal rules
        part_obj = self.pool['res.partner']
        partner = part_obj.browse(cr, uid, partner_id, context=context)

        # partner manually set fiscal position always win
        if partner.property_account_position:
            return partner.property_account_position.id

        # if no delivery use invocing
        if delivery_id:
            delivery = part_obj.browse(cr, uid, delivery_id, context=context)
        else:
            delivery = partner

        domain = [
            ('auto_apply', '=', True),
            '|', ('vat_required', '=', False), ('vat_required', '=', partner.vat_subjected),
            "|", "|", ("state", "=", delivery.state_id.id), ("state_not", "!=", delivery.state_id.id),
            "&", ("state", "=", False), ("state_not", "=", False),
            "|", ('country_id', '=', delivery.country_id.id), ("country_not", "!=", delivery.country_id.id)
        ]

        fiscal_position_ids = self.search(cr, uid, domain, context=context, limit=1)

        if fiscal_position_ids:
            return fiscal_position_ids[0]

        fiscal_position_ids = self.search(cr, uid, domain + [('country_group_id.country_ids', '=', delivery.country_id.id)], context=context, limit=1)
        if fiscal_position_ids:
            return fiscal_position_ids[0]

        fiscal_position_ids = self.search(cr, uid, domain + [('country_id', '=', None), ('country_group_id', '=', None)], context=context, limit=1)
        if fiscal_position_ids:
            return fiscal_position_ids[0]
        return False


    def import_product_taxes(self, cr, uid, csv_filename, context=None):
        product_registry = self.pool.get("product.template")
        tax_registry = self.pool.get("account.tax")
        with open(csv_filename) as taxes_lines:
            reader = csv.DictReader(taxes_lines)
            for row in reader:
                product_name = row["Product Name"]
                tax_code = row["Tax"]
                tax_id = tax_registry.search(cr, uid, [("description", "=", tax_code)])
                product_ids = product_registry.search(cr, uid, [("name", "=", product_name)])
                if tax_id and product_ids:
                    product_id = product_ids[0]
                    tax = tax_registry.browse(cr, uid, tax_id[0])
                    product = product_registry.browse(cr, uid, product_id)
                    product.taxes_id = tax
                    _logger.info(product_name)
                else:
                    _logger.info( "skipping %s" %(product_name))
                cr.commit()