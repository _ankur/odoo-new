{
    'name': "Accounting Tax Addons",
    'version': "1.0",
    'author': "Nishant Sharma",
    'category': "Tools",
    'depends': ['account_accountant'],
    'data': [
        "account_fiscal_position_rule_view.xml",
        "fiscal_position_view.xml",
        ],
    'update_xml': [],
    'demo': [],
    'installable': True,
    'auto_install': False,
    'application': True,
}