create or replace function correct_three_p_channel(three_p_channel text)
returns text as
$$
declare
_result text;
BEGIN
IF three_p_channel = 'False' THEN
    _result = 'Voylla Website';
    RETURN _result;
ELSIF three_p_channel = '' THEN
    _result = 'Voylla Website';
    RETURN _result;
ELSE
    _result = three_p_channel;
    RETURN _result;
END IF;
END;
$$
language plpgsql;
