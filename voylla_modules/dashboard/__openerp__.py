{
    'name': "Dashboard",
    'version': "1.0",
    'author': "Abhishek Saini",
    'category': "Tools",
    'depends': ['stock', 'procurement','purchase','crm','sale'],
    'data': ['dashboard_view.xml'],
    'demo': [],
    'installable': True,
    'auto_install': True,
    'application': True,
}