from openerp import tools
from openerp.osv import fields, osv
import pdb
import base64
import re
import datetime
from datetime import date

class dashboard_weekly_plot(osv.osv):

    _name = "dashboard.weekly_plot"
    _description = "Weekly Plot"
    _auto = False
    _columns = {
            'total_order_count':fields.float('Order Count', readonly=True),
            'order_date':fields.datetime('Order Date', readonly=True),
            'total_price':fields.float('Total Price', readonly=True),
            'order_year': fields.integer('Order Year', readonly=True),
            'channel':fields.char('3P-Channel', readonly=True)
    }



    # In the query below changed the time of order to 00:00:00 in select statement
    # for every order to group by date only
    def init(self, cr):
        tools.drop_view_if_exists(cr, self._table)
        cr.execute( """ CREATE OR REPLACE VIEW %s AS (
            SELECT min(id) as id, COUNT(*) as total_order_count,
            round(sum(amount_total)) as total_price,
            date_trunc('day', create_date + interval'5 hours 30 minutes') as order_date,
            EXTRACT('YEAR' FROM  create_date) as order_year,
            coalesce(correct_three_p_channel(three_p_channel), 'Voylla Website') as channel
            FROM (select * from sale_order where three_p_channel not like %s) as p
            WHERE create_date > (select date_trunc('day',now()-interval'6 days')) - interval'5 hours 30 minutes' and state not in ('cancel')
            GROUP BY order_date, order_year, channel
            ORDER BY order_date) """ % (self._table,"'%Central%'"))


class dashboard_weekly_3p_plot(osv.osv):

    _name = "dashboard.weekly_3p_plot"
    _description = "Weekly 3P Plot"
    _auto = False
    _columns = {
            'total_order_count':fields.float('Order Count', readonly=True),
            'order_date':fields.datetime('Order Date', readonly=True),
            'total_price':fields.float('Total Price', readonly=True),
            'channel':fields.char('3P-Channel', readonly=True)
    }


    # In the query below changed the time of order to 00:00:00 in select statement
    # for every order to group by date only
    def init(self, cr):
        tools.drop_view_if_exists(cr, self._table)
        cr.execute( """ CREATE OR REPLACE VIEW %s AS (
            SELECT min(id) as id, COUNT(*) as total_order_count, round(sum(amount_total)) as total_price,
            date_trunc('day', create_date + interval'5 hours 30 minutes') as order_date,
            coalesce(correct_three_p_channel(three_p_channel), 'Voylla Website') as channel
            FROM sale_order
            WHERE create_date > (select date_trunc('day',now()-interval'6 days')) - interval'5 hours 30 minutes' and state not in ('cancel')
            and (three_p_channel in (select three_p_channel from (select count(*) as order_count,
            three_p_channel from sale_order group by three_p_channel order by order_count desc limit 10) as
            top_channels UNION ALL select distinct three_p_channel from sale_order where three_p_channel='Voylla Bulk') or three_p_channel in (		   'False',''))
            GROUP BY order_date,channel
            ORDER BY order_date asc,total_order_count desc) """ % (self._table))

class dashboard_monthly_plot(osv.osv):

    _name = "dashboard.monthly_plot"
    _description = "Monthly Plot"
    _auto = False
    _columns = {
            'total_order_count':fields.float('Order Count', readonly=True),
            'order_date':fields.datetime('Order Date', readonly=True),
            'total_price':fields.float('Total Price', readonly=True),
            'order_year': fields.integer('Order Year', readonly=True),
            'channel':fields.char('3P-Channel', readonly=True)
    }


    # In the query below, changed the time of order to 00:00:00 in select statement
    # for every order to group by date only
    def init(self, cr):
        tools.drop_view_if_exists(cr, self._table)
        cr.execute( """ CREATE OR REPLACE VIEW %s AS (
            SELECT min(id) as id, COUNT(*) as total_order_count, round(sum(amount_total)) as total_price, date_trunc('day', date_order) as order_date, EXTRACT('YEAR' FROM  date_order) as order_year, three_p_channel as channel
            FROM sale_order
            WHERE (date_order::timestamp::date) > now() - interval'1 month'
            GROUP BY order_date,order_year,channel
            ORDER BY order_date
            ) """ % (self._table))


class dashboard_annual_plot(osv.osv):

    _name = "dashboard.annual_plot"
    _description = "Annual Plot"
    _auto = False
    _columns = {
            'total_order_count':fields.float('Order Count', readonly=True),
            'order_week':fields.float('Order Week No.', readonly=True),
            'order_year':fields.float('Order Year', readonly=True),
            'total_price':fields.float('Total Price', readonly=True),
            'channel':fields.char('3P-Channel', readonly=True)
    }

    def init(self, cr):
        tools.drop_view_if_exists(cr, self._table)
        cr.execute( """ CREATE OR REPLACE VIEW %s AS (
            SELECT min(id) as id, COUNT(*) as total_order_count, round(sum(amount_total)) as total_price,  week_num_year(date_order::date) as order_week, EXTRACT('YEAR' FROM  date_order) as order_year, three_p_channel as channel
            FROM sale_order
            WHERE (date_order::timestamp::date) > now() - interval'1 year'
            GROUP BY order_week,order_year,three_p_channel
            ORDER BY order_year,order_week
            ) """ % (self._table))


class dashboard_alldata_plot(osv.osv):

    _name = "dashboard.alldata_plot"
    _description = "All Data Plot"
    _auto = False
    _columns = {
            'total_order_count':fields.float('Order Count', readonly=True),
            'order_month':fields.float('Order Week No.', readonly=True),
            'order_year':fields.float('Order Year', readonly=True),
            'total_price':fields.float('Total Price', readonly=True),
            'channel':fields.char('3P-Channel', readonly=True)
    }

    def init(self, cr):
        tools.drop_view_if_exists(cr, self._table)
        cr.execute( """ CREATE OR REPLACE VIEW %s AS (
            SELECT min(id) as id, COUNT(*) as total_order_count, round(sum(amount_total)) as total_price,  EXTRACT( 'MONTH' FROM date_order) as order_month, EXTRACT('YEAR' FROM  date_order) as order_year, three_p_channel as channel
            FROM sale_order
            GROUP BY order_month,order_year,channel
            ORDER BY order_year,order_month
            ) """ % (self._table))

class dashboard_hourly_taxon_plot(osv.osv):

    _name = "dashboard.hourly_taxon_plot"
    _description = "Hourly taxon Plot"
    _auto = False
    _columns = {
            'sum':fields.float('Product Count', readonly=True),
            
            'order_date':fields.datetime('Order Date', readonly=True),
            'order_hour':fields.char('Day Hour', readonly=True),
            'cnct':fields.char('Taxon Gender',readonly=True)
    }


    # In the query below changed the time of order to 00:00:00 in select statement
    # for every order to group by date only
    def init(self, cr):
        tools.drop_view_if_exists(cr, self._table)
        
        cr.execute( """ CREATE OR REPLACE VIEW %s AS (
            select * from get_hourly_top_taxon() as graph where cnct in (select * from get_top_ten_taxon())) """ % (self._table))

class required_table_cac_voylla(osv.osv):
    _name = "require.table.cac.voylla"
    _columns={
        "month_count":fields.text("week_count"),
        "total_new_user":fields.float("total_new_user"),
        "repeated_user":fields.integer('repeated_user'),
        "cost_voylla":fields.float('cost_voylla'),
    }

class import_file_cac(osv.osv):
    _name = "import.file.cac"
    _columns={
        "month_count":fields.text("month_count"),
        "total_expenses":fields.float("total_expenses"),
    }

class import_file_cac(osv.osv):
    _name = 'file.import.cac'
    
    _columns = {
    'ref_no':fields.char("Reference Number"),
    'module_file':fields.binary("Module File"),
    
    }

    def import_file_cac(self,cr,uid,ids,context=None):
        data = self.browse(cr, uid, ids[0] , context=context)
        csv_data = base64.decodestring(data.module_file)
            
        chars_to_remove = ['\r', ' ', '\n']
        chars_to_remove_1 = ['\r', '\n']
        rx = "[%s]" %(re.escape(''.join(chars_to_remove)))
        rx_1 = "[%s]" %(re.escape(''.join(chars_to_remove_1)))
        print csv_data
        rows = csv_data.split("\n")

        try:
            header = re.sub(rx, '', rows[0]).split(",")
            csv_prepaid_details = rows[1:]
        except:
            pass        
        prepaid_details = {}
        object_entry=self.pool.get('import.file.cac')
        total_week_spent=0
        for prepaid in csv_prepaid_details:
            if prepaid == '':
                continue
            prepaid_details = dict(zip(header, re.sub(rx_1, '', prepaid).split(",")))
            total_charges=int(prepaid_details['AmountSpendFB'])+int(prepaid_details['AdwordsSpend'])
            total_week_spent +=total_charges
            date=prepaid_details['Date']
            date2=b=datetime.datetime.strptime(date,"%m/%d/%Y")
            m=date2.isocalendar()
            # week=m[1]
            month=date2.strftime('%m')
            year=m[0]
            week_final=('%s'+'::'+'%s')%(month,year)
        vals2={'month_count':week_final,'total_expenses':total_week_spent}
        import_file_final=object_entry.create(cr,uid,vals2,context)    
        cr.commit()
    
class dashboard_cac_plot(osv.osv):
    _name='dashboard.cac.plot'
    _description = "cac Plot"
    _auto = False

    _columns= {
        "month_count":fields.text("month_count"),
        "total_new_user":fields.float("total_new_user"),
        "repeated_user":fields.integer('repeated_user'),
        "cost_voylla":fields.float('cost_voylla'),
        "cac_value":fields.float('cac_value'),
        "repeat_percentage":fields.float('repeat_percentage')
     }   
        
    def init(self, cr,context=None):
        tools.drop_view_if_exists(cr, self._table) 
        table_required=self.pool.get('require.table.cac.voylla')
        table_required2=self.pool.get('import.file.cac')
        today_date=datetime.datetime.now()
        calendar=today_date.isocalendar()
        current_month_str=datetime.datetime.strftime(today_date,'%m')
        current_month=int(current_month_str)
        current_year=calendar[0]
        starting_year=2015
        sql="""delete from require_table_cac_voylla"""
        cr.execute(sql)
        for year in range(starting_year,current_year):
            year=str(year)
            if starting_year==2015:
                starting_month=5
            else:
                starting_month=1    
            for x in range(starting_month,13):
                sql1="""select count(p.first_order) from (select q.order_create_date,q.first_order from (SELECT so.order_id as order_number,so.three_p_channel as channel,so.partner_id as order_partner_id,rp.id as res_partner_id,coalesce(rp.parent_id,rp.id) as res_partner_parent_id_or_id,so.create_date as order_create_date,rp.create_date as partner_create_date,a.min as first_order_date,a.min = so.create_date as first_order
                        FROM (select * from sale_order where state != 'cancel') as so
                        LEFT JOIN res_partner as rp on rp.id = so.partner_id
                        LEFT JOIN (select partner_id,min(create_date) from sale_order where state != 'cancel' group by partner_id) as a ON a.partner_id = so.partner_id where so.three_p_channel='Voylla Website') as q where date_part('month',q.order_create_date)=%s and date_part('year',q.order_create_date)=%s and q.first_order=True) as p"""%(x,year)
                cr.execute(sql1)
                total_new_user=cr.fetchall()
                number=max(int(total_new_user[0][0]),1)
                sql2="""select count(final.count) from (select count(p.first_order),p.res_partner_parent_id_or_id from (select q.res_partner_parent_id_or_id,q.order_create_date,q.first_order from (SELECT so.order_id as order_number,so.three_p_channel as channel,so.partner_id as order_partner_id,rp.id as res_partner_id,coalesce(rp.parent_id,rp.id) as res_partner_parent_id_or_id,so.create_date as order_create_date,rp.create_date as partner_create_date,a.min as first_order_date,a.min = so.create_date as first_order
                        FROM (select * from sale_order where state != 'cancel') as so
                        LEFT JOIN res_partner as rp on rp.id = so.partner_id
                        LEFT JOIN (select partner_id,min(create_date) from sale_order where state != 'cancel' group by partner_id) as a ON a.partner_id = so.partner_id where so.three_p_channel='Voylla Website') as q where date_part('month',q.order_create_date)=%s and date_part('year',q.order_create_date)=%s and q.first_order=False) as p group by p.res_partner_parent_id_or_id) as final"""%(x,year)
                cr.execute(sql2)
                data_cac=cr.fetchall()
                month_final=('%s'+'::'+'%s')%(x,year)
                browse_month=table_required2.search(cr,1,[('month_count','=',month_final)])
                total_expenses=table_required2.browse(cr,1,browse_month).total_expenses
                if len(data_cac)==0:
                    vals={'month_count':('%s'+'::'+'%s')%(x,year),'total_new_user':max(1,number),'repeated_user':1,'cost_voylla':total_expenses}
                else:
                    vals={'month_count':month_final,'total_new_user':number,'repeated_user':data_cac[0][0],'cost_voylla':total_expenses}    
                create_entries=table_required.create(cr,1,vals,context)
            year=int(year)+1
        if current_year==2015:
            starting_month=5
        else:
            starting_month=1
        
        for x in range(starting_month,current_month+1):
            sql1="""select count(p.first_order) from (select q.order_create_date,q.first_order from (SELECT so.order_id as order_number,so.three_p_channel as channel,so.partner_id as order_partner_id,rp.id as res_partner_id,coalesce(rp.parent_id,rp.id) as res_partner_parent_id_or_id,so.create_date as order_create_date,rp.create_date as partner_create_date,a.min as first_order_date,a.min = so.create_date as first_order
                        FROM (select * from sale_order where state != 'cancel') as so
                        LEFT JOIN res_partner as rp on rp.id = so.partner_id
                        LEFT JOIN (select partner_id,min(create_date) from sale_order where state != 'cancel' group by partner_id) as a ON a.partner_id = so.partner_id where so.three_p_channel='Voylla Website') as q where date_part('month',q.order_create_date)=%s and date_part('year',q.order_create_date)=%s and q.first_order=True) as p"""%(x,current_year)
            cr.execute(sql1)
            total_new_user=cr.fetchall()
            number=max(int(total_new_user[0][0]),1)
            sql2="""select count(final.count) from (select count(p.first_order),p.res_partner_parent_id_or_id from (select q.res_partner_parent_id_or_id,q.order_create_date,q.first_order from (SELECT so.order_id as order_number,so.three_p_channel as channel,so.partner_id as order_partner_id,rp.id as res_partner_id,coalesce(rp.parent_id,rp.id) as res_partner_parent_id_or_id,so.create_date as order_create_date,rp.create_date as partner_create_date,a.min as first_order_date,a.min = so.create_date as first_order
                        FROM (select * from sale_order where state != 'cancel') as so
                        LEFT JOIN res_partner as rp on rp.id = so.partner_id
                        LEFT JOIN (select partner_id,min(create_date) from sale_order where state != 'cancel' group by partner_id) as a ON a.partner_id = so.partner_id where so.three_p_channel='Voylla Website') as q where date_part('month',q.order_create_date)=%s and date_part('year',q.order_create_date)=%s and q.first_order=False) as p group by p.res_partner_parent_id_or_id) as final"""%(x,current_year)
            cr.execute(sql2)
            data_cac=cr.fetchall()
            month_final=('%s'+'::'+'%s')%(x,current_year)
            browse_month=table_required2.search(cr,1,[('month_count','=',month_final)])
            total_expenses=table_required2.browse(cr,1,browse_month).total_expenses
            if len(data_cac)==0:
                vals={'month_count':('%s'+'::'+'%s')%(x,current_year),'total_new_user':number,'repeated_user':1,'cost_voylla':total_expenses}
            else:
                vals={'month_count':month_final,'total_new_user':number,'repeated_user':data_cac[0][0],'cost_voylla':total_expenses}   
            
            create_entries=table_required.create(cr,1,vals,context)
        cr.commit()
        cr.execute( """ CREATE OR REPLACE VIEW %s AS (
        select min(id) as id,month_count,total_new_user,repeated_user,cost_voylla,(cost_voylla/total_new_user) as cac_value,(repeated_user*100)/(total_new_user+repeated_user) as repeat_percentage from require_table_cac_voylla group by month_count,total_new_user,repeated_user,cost_voylla,cac_value,repeat_percentage)""" % (self._table))       


class required_table_cac_voylla(osv.osv):
    _name = "require.table.cac.all"
    _columns={
        "month_count":fields.text("week_count"),
        "total_new_user":fields.float("total_new_user"),
        "repeated_user":fields.integer('repeated_user'),
        "cost_voylla":fields.float('cost_voylla'),
    }

class import_file_cac(osv.osv):
    _name = "expenses.report"
    _columns={
        "month_count":fields.text("month_count"),
        "total_expenses":fields.float("total_expenses"),
    }    


class dashboard_cac_plot(osv.osv):
    _name='dashboard.cac.plot.all'
    _description = "cac Plot all"
    _auto = False

    _columns= {
        "month_count":fields.text("Month Count"),
        "total_new_user":fields.float("Total New User"),
        "repeated_user":fields.integer('Repeated User'),
        "cost_voylla":fields.float('Cost'),
        "cac_value":fields.float('Cac Value'),
        "repeat_percentage":fields.float('Repeat Percentage')
     }   
        
    def init(self, cr,context=None):
        tools.drop_view_if_exists(cr, self._table) 
        
        table_required=self.pool.get('require.table.cac.all')
        table_required2=self.pool.get('expenses.report')
        today_date=datetime.datetime.now()
        calendar=today_date.isocalendar()
        current_month_str=datetime.datetime.strftime(today_date,'%m')
        current_month=int(current_month_str)
        # current_year_week=calendar[1]
        # pdb.set_trace()
        current_year=calendar[0]
        starting_year=2015
        sql="""delete from require_table_cac_all"""
        cr.execute(sql)

        for year in range(starting_year,current_year):
            year=str(year)
            if starting_year==2015:
                starting_month=5
            else:
                starting_month=1    
            for x in range(starting_month,13):
                sql1="""select count(p.first_order) from (select q.order_create_date,q.first_order 
                         from (SELECT so.order_id as order_number,so.three_p_channel as channel,so.partner_id 
                        as order_partner_id,rp.id as res_partner_id,coalesce(rp.parent_id,rp.id) 
                        as res_partner_parent_id_or_id,so.create_date as order_create_date,rp.create_date 
                        as partner_create_date,a.min as first_order_date,a.min = so.create_date as first_order
                        FROM (select * from sale_order where state != 'cancel') as so
                        LEFT JOIN res_partner as rp on rp.id = so.partner_id
                        LEFT JOIN (select partner_id,min(create_date) from sale_order where state != 'cancel' 
                        group by partner_id) as a ON a.partner_id = so.partner_id ) 
                         as q where date_part('month',q.order_create_date)='%s'
                        and date_part('year',q.order_create_date)='%s' and q.first_order=True) as p"""%(x,year)
                cr.execute(sql1)
                total_new_user=cr.fetchall()
                number=max(int(total_new_user[0][0]),1)
                sql2="""select count(final.count) from (select count(p.first_order),p.res_partner_parent_id_or_id 
                        from (select q.res_partner_parent_id_or_id,q.order_create_date,q.first_order 
                        from (SELECT so.order_id as order_number,so.three_p_channel as channel,so.partner_id 
                        as order_partner_id,rp.id as res_partner_id,coalesce(rp.parent_id,rp.id) 
                        as res_partner_parent_id_or_id,so.create_date as order_create_date,rp.create_date 
                        as partner_create_date,a.min as first_order_date,a.min = so.create_date as first_order
                        FROM (select * from sale_order where state != 'cancel') as so
                        LEFT JOIN res_partner as rp on rp.id = so.partner_id
                        LEFT JOIN (select partner_id,min(create_date) from sale_order where state != 'cancel' 
                        group by partner_id) as a ON a.partner_id = so.partner_id) as q 
                        where date_part('month',q.order_create_date)='%s'
                        and date_part('year',q.order_create_date)='%s' and q.first_order=False) as p 
                        group by p.res_partner_parent_id_or_id) as final"""%(x,year)
                cr.execute(sql2)
                data_cac=cr.fetchall()
                # data_cac=cr.fetchall()
                # pdb.set_trace()
                month_final=('%s'+'::'+'%s')%(x,year)
                browse_month=table_required2.search(cr,1,[('month_count','=',month_final)])
                total_expenses=table_required2.browse(cr,1,browse_month).total_expenses
                if len(data_cac)==0:
                    vals={'month_count':('%s'+'::'+'%s')%(x,year),'total_new_user':max(1,number),'repeated_user':1,'cost_voylla':total_expenses}
                else:
                    vals={'month_count':month_final,'total_new_user':number,'repeated_user':data_cac[0][0],'cost_voylla':total_expenses}    
                create_entries=table_required.create(cr,1,vals,context)
            year=int(year)+1
        if current_year==2015:
            starting_month=5
        else:
            starting_month=1
        
        for x in range(starting_month,current_month+1):
            sql1="""select count(p.first_order) from (select q.order_create_date,q.first_order 
                         from (SELECT so.order_id as order_number,so.three_p_channel as channel,so.partner_id 
                        as order_partner_id,rp.id as res_partner_id,coalesce(rp.parent_id,rp.id) 
                        as res_partner_parent_id_or_id,so.create_date as order_create_date,rp.create_date 
                        as partner_create_date,a.min as first_order_date,a.min = so.create_date as first_order
                        FROM (select * from sale_order where state != 'cancel') as so
                        LEFT JOIN res_partner as rp on rp.id = so.partner_id
                        LEFT JOIN (select partner_id,min(create_date) from sale_order where state != 'cancel' 
                        group by partner_id) as a ON a.partner_id = so.partner_id ) 
                         as q where date_part('month',q.order_create_date)='%s'
                        and date_part('year',q.order_create_date)='%s' and q.first_order=True) as p"""%(x,current_year)
            cr.execute(sql1)
            total_new_user=cr.fetchall()
            number=max(int(total_new_user[0][0]),1)
            sql2="""select count(final.count) from (select count(p.first_order),p.res_partner_parent_id_or_id 
                        from (select q.res_partner_parent_id_or_id,q.order_create_date,q.first_order 
                        from (SELECT so.order_id as order_number,so.three_p_channel as channel,so.partner_id 
                        as order_partner_id,rp.id as res_partner_id,coalesce(rp.parent_id,rp.id) 
                        as res_partner_parent_id_or_id,so.create_date as order_create_date,rp.create_date 
                        as partner_create_date,a.min as first_order_date,a.min = so.create_date as first_order
                        FROM (select * from sale_order where state != 'cancel') as so
                        LEFT JOIN res_partner as rp on rp.id = so.partner_id
                        LEFT JOIN (select partner_id,min(create_date) from sale_order where state != 'cancel' 
                        group by partner_id) as a ON a.partner_id = so.partner_id) as q 
                        where date_part('month',q.order_create_date)='%s'
                        and date_part('year',q.order_create_date)='%s' and q.first_order=False) as p 
                        group by p.res_partner_parent_id_or_id) as final"""%(x,current_year)
            cr.execute(sql2)
            data_cac=cr.fetchall()
            # data_cac=cr.fetchall()
            # pdb.set_trace()
            month_final=('%s'+'::'+'%s')%(x,current_year)
            browse_month=table_required2.search(cr,1,[('month_count','=',month_final)])
            total_expenses=table_required2.browse(cr,1,browse_month).total_expenses
            # pdb.set_trace()
            if len(data_cac)==0:
                vals={'month_count':('%s'+'::'+'%s')%(x,current_year),'total_new_user':number,'repeated_user':1,'cost_voylla':total_expenses}
            else:
                vals={'month_count':month_final,'total_new_user':number,'repeated_user':data_cac[0][0],'cost_voylla':total_expenses}   
            
            create_entries=table_required.create(cr,1,vals,context)
        cr.commit()
        # pdb.set_trace()
        cr.execute( """ CREATE OR REPLACE VIEW %s AS (
                        select min(id) as id,month_count,total_new_user,repeated_user,cost_voylla,(cost_voylla/total_new_user) as cac_value,(repeated_user*100)/(total_new_user+repeated_user) as repeat_percentage from require_table_cac_all group by month_count,total_new_user,repeated_user,cost_voylla,cac_value,repeat_percentage)""" % (self._table))
