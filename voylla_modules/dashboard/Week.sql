create or replace function week_num_year(_date date)
returns integer as
$body$
declare
_year date;
_week_number integer;
begin
select date_trunc('year', _date)::date into _year
;
with first_saturday as (
    select extract(doy from a::date) ff
    from generate_series(_year, _year + 6, '1 day') s(a)
    where extract(dow from a) = 6
)
select floor(
        (extract(doy from _date) - (select ff from first_saturday) - 1) / 7
    ) + 2 into _week_number
;
return _week_number
;
end;
$body$
language plpgsql immutable


# Main Query
SELECT min(id) as id, COUNT(*) as total_order_count, round(sum(total_price)) as total_price, order_date, channel
FROM (SELECT min(id) as id, COUNT(*) as total_order_count, partner_id, round(sum(amount_total)) as total_price, date_trunc('day', date_order) as order_date, three_p_channel as channel
            FROM sale_order
            WHERE (date_order::timestamp::date) > now()::date - 7 and partner_id in (select partner_id from sale_order
            where partner_id in (select partner_id from sale_order where create_date::date = date_order::date) group by partner_id  having count(partner_id) > 1)
            GROUP BY order_date, partner_id, channel
            ORDER BY order_date) AS old_user
GROUP BY order_date,channel
ORDER BY order_date

SELECT min(id) as id, COUNT(*) as total_order_count, round(sum(total_price)) as total_price, order_date, channel
FROM (SELECT min(so.id) as id, COUNT(*) as total_order_count, round(sum(so.amount_total)) as total_price, date_trunc('day', so.date_order) as order_date, so.three_p_channel as channel, rp.email as email, rp.phone as phone, rp.mobile as mobile
            FROM sale_order as so left join res_partner as rp ON so.partner_id = rp.id
            WHERE (so.date_order::timestamp::date) > now()::date - 7 and rp.email in (select email from res_partner
            where email in (select email from res_partner where create_date::date = so.date_order::date) group by email having count(email) > 1)
            GROUP BY order_date, email, phone, mobile, channel
            ORDER BY order_date) AS old_user
GROUP BY order_date,channel
ORDER BY order_date


# Temporary Query

select partner_id, count(partner_id)
from sale_order
where partner_id in (select partner_id from sale_order where create_date::date = '2015-06-25') group by partner_id  having count(partner_id) > 1;

select partner_invoice_id , count(partner_invoice_id )
from sale_order
where partner_invoice_id  in (select partner_invoice_id  from sale_order where create_date::date='2015-06-25') group by partner_invoice_id   having count(partner_invoice_id ) > 1;
