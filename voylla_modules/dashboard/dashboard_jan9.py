from openerp import tools
from openerp.osv import fields, osv
import pdb

class dashboard_weekly_plot(osv.osv):

    _name = "dashboard.weekly_plot"
    _description = "Weekly Plot"
    _auto = False
    _columns = {
            'total_order_count':fields.float('Order Count', readonly=True),
            'order_date':fields.datetime('Order Date', readonly=True),
            'total_price':fields.float('Total Price', readonly=True),
            'order_year': fields.integer('Order Year', readonly=True),
            'channel':fields.char('3P-Channel', readonly=True)
    }



    # In the query below changed the time of order to 00:00:00 in select statement
    # for every order to group by date only
    def init(self, cr):
        tools.drop_view_if_exists(cr, self._table)
        cr.execute( """ CREATE OR REPLACE VIEW %s AS (
            SELECT min(id) as id, COUNT(*) as total_order_count,
            round(sum(amount_total)) as total_price,
            date_trunc('day', create_date + interval'5 hours 30 minutes') as order_date,
            EXTRACT('YEAR' FROM  create_date) as order_year,
            coalesce(correct_three_p_channel(three_p_channel), 'Voylla Website') as channel
            FROM sale_order
            WHERE create_date > (select date_trunc('day',now()-interval'6 days')) - interval'5 hours 30 minutes' and state not in ('cancel')
            GROUP BY order_date, order_year, channel
            ORDER BY order_date) """ % (self._table))


class dashboard_weekly_3p_plot(osv.osv):

    _name = "dashboard.weekly_3p_plot"
    _description = "Weekly 3P Plot"
    _auto = False
    _columns = {
            'total_order_count':fields.float('Order Count', readonly=True),
            'order_date':fields.datetime('Order Date', readonly=True),
            'total_price':fields.float('Total Price', readonly=True),
            'channel':fields.char('3P-Channel', readonly=True)
    }


    # In the query below changed the time of order to 00:00:00 in select statement
    # for every order to group by date only
    def init(self, cr):
        tools.drop_view_if_exists(cr, self._table)
        cr.execute( """ CREATE OR REPLACE VIEW %s AS (
            SELECT min(id) as id, COUNT(*) as total_order_count, round(sum(amount_total)) as total_price,
            date_trunc('day', create_date + interval'5 hours 30 minutes') as order_date,
            coalesce(correct_three_p_channel(three_p_channel), 'Voylla Website') as channel
            FROM sale_order
            WHERE create_date > (select date_trunc('day',now()-interval'6 days')) - interval'5 hours 30 minutes' and state not in ('cancel')
            and (three_p_channel in (select three_p_channel from (select count(*) as order_count,
            three_p_channel from sale_order group by three_p_channel order by order_count desc limit 10) as
            top_channels) or three_p_channel in ('False',''))
            GROUP BY order_date,channel
            ORDER BY order_date asc,total_order_count desc) """ % (self._table))

class dashboard_monthly_plot(osv.osv):

    _name = "dashboard.monthly_plot"
    _description = "Monthly Plot"
    _auto = False
    _columns = {
            'total_order_count':fields.float('Order Count', readonly=True),
            'order_date':fields.datetime('Order Date', readonly=True),
            'total_price':fields.float('Total Price', readonly=True),
            'order_year': fields.integer('Order Year', readonly=True),
            'channel':fields.char('3P-Channel', readonly=True)
    }


    # In the query below, changed the time of order to 00:00:00 in select statement
    # for every order to group by date only
    def init(self, cr):
        tools.drop_view_if_exists(cr, self._table)
        cr.execute( """ CREATE OR REPLACE VIEW %s AS (
            SELECT min(id) as id, COUNT(*) as total_order_count, round(sum(amount_total)) as total_price, date_trunc('day', date_order) as order_date, EXTRACT('YEAR' FROM  date_order) as order_year, three_p_channel as channel
            FROM sale_order
            WHERE (date_order::timestamp::date) > now() - interval'1 month'
            GROUP BY order_date,order_year,channel
            ORDER BY order_date
            ) """ % (self._table))


class dashboard_annual_plot(osv.osv):

    _name = "dashboard.annual_plot"
    _description = "Annual Plot"
    _auto = False
    _columns = {
            'total_order_count':fields.float('Order Count', readonly=True),
            'order_week':fields.float('Order Week No.', readonly=True),
            'order_year':fields.float('Order Year', readonly=True),
            'total_price':fields.float('Total Price', readonly=True),
            'channel':fields.char('3P-Channel', readonly=True)
    }

    def init(self, cr):
        tools.drop_view_if_exists(cr, self._table)
        cr.execute( """ CREATE OR REPLACE VIEW %s AS (
            SELECT min(id) as id, COUNT(*) as total_order_count, round(sum(amount_total)) as total_price,  week_num_year(date_order::date) as order_week, EXTRACT('YEAR' FROM  date_order) as order_year, three_p_channel as channel
            FROM sale_order
            WHERE (date_order::timestamp::date) > now() - interval'1 year'
            GROUP BY order_week,order_year,three_p_channel
            ORDER BY order_year,order_week
            ) """ % (self._table))


class dashboard_alldata_plot(osv.osv):

    _name = "dashboard.alldata_plot"
    _description = "All Data Plot"
    _auto = False
    _columns = {
            'total_order_count':fields.float('Order Count', readonly=True),
            'order_month':fields.float('Order Week No.', readonly=True),
            'order_year':fields.float('Order Year', readonly=True),
            'total_price':fields.float('Total Price', readonly=True),
            'channel':fields.char('3P-Channel', readonly=True)
    }

    def init(self, cr):
        tools.drop_view_if_exists(cr, self._table)
        cr.execute( """ CREATE OR REPLACE VIEW %s AS (
            SELECT min(id) as id, COUNT(*) as total_order_count, round(sum(amount_total)) as total_price,  EXTRACT( 'MONTH' FROM date_order) as order_month, EXTRACT('YEAR' FROM  date_order) as order_year, three_p_channel as channel
            FROM sale_order
            GROUP BY order_month,order_year,channel
            ORDER BY order_year,order_month
            ) """ % (self._table))

class dashboard_hourly_taxon_plot(osv.osv):

    _name = "dashboard.hourly_taxon_plot"
    _description = "Hourly taxon Plot"
    _auto = False
    _columns = {
            'sum':fields.float('Product Count', readonly=True),
            
            'order_date':fields.datetime('Order Date', readonly=True),
            'order_hour':fields.char('Day Hour', readonly=True),
            'cnct':fields.char('Taxon Gender',readonly=True)
    }


    # In the query below changed the time of order to 00:00:00 in select statement
    # for every order to group by date only
    def init(self, cr):
        tools.drop_view_if_exists(cr, self._table)
        
        cr.execute( """ CREATE OR REPLACE VIEW %s AS (
            select * from get_hourly_top_taxon() as graph where cnct in (select * from get_top_ten_taxon())) """ % (self._table))
