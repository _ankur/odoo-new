{
    'name': "Quality Control",
    'version': "1.0",
    'author': "Devendra",
    'category': "Tools",
    'depends': ['stock', 'procurement','purchase','crm','sale'],
    'data': [
        'security/qc_security.xml',
        'product_view.xml'
    ],
    'demo': [],
    'installable': True,
    'auto_install': True,
    'application': True,
}