function openerp_pos_voucher(instance,module){
    // var module   = instance.point_of_sale;
    var round_pr = instance.web.round_precision;
    _t = instance.web._t;
    var QWeb = instance.web.qweb;

    module.discount_voucher = instance.web.Class.extend({
        name: 'openerp_pos_voucher',
        init: function(){
            console.log('I Called');
            // options = options || {};
            self.active_voucher = {};
            self.active_credit_note = {};
            // var active_voucher = {};
        },

        get_dis_voucher: function(){

            fields = ['name','expire_date','active','values','percent','limits'];
            domain = [['active','=',true]];
            var records = new instance.web.Model('pos.discount.voucher').query(fields).filter(domain).all()
            
                $.when(records).then(function(voucher){
                self.voucher = voucher;
                for(var i=0, len=voucher.length; i<len; i++){

                    if(voucher[i].name){
                       // this.active_voucher = {}
                       active_voucher[voucher[i].name] = voucher[i];
                       // active_voucher[voucher.name].push() = [voucher.percent,voucher.expire_date];
                       console.log(active_voucher);
                        }
                    }
            });
           
        },

        get_credit_note: function(){
            fields = ['name','expire_date','active','values','amount','remaining_amt','limits','mobile'];
            domain = [['active','=',true]];
            var records = new instance.web.Model('pos.credit.note').query(fields).filter(domain).all()
                $.when(records).then(function(giftNote){
                self.giftNote = giftNote;
                for(var i=0, len=giftNote.length; i<len; i++){

                    if(giftNote[i].name){
                       // this.active_voucher = {}
                       active_credit_note[giftNote[i].name] = giftNote[i];
                       // active_voucher[voucher.name].push() = [voucher.percent,voucher.expire_date];
                       console.log(active_credit_note);
                        }
                    }
            });
           
        },

    });
    
    // module.Order.include({
    //     addPaymentline: function(cashregister){
    //          var paymentLines = this.get('paymentLines');
    //         var newPaymentline = new module.Paymentline({},{cashregister:cashregister, pos:this.pos});
    //         if(cashregister.journal.type !== 'cash'){
    //             newPaymentline.set_amount( Math.max(this.getDueLeft(),0) );
    //         }
    //         paymentLines.add(newPaymentline);
    //         this.selectPaymentline(newPaymentline);
    // })

    module.PaymentScreenWidget.include({
        initialize: function(){
            this.set({
                paymentLines:   new module.PaymentlineCollection(),
            });
        },
        show: function(){
            this._super();
            var self = this;
            console.log("I'm here");

            this.add_action_button({
                    label: _t('Credit Note'),
                    icon: '/voylla_stores/static/src/img/png/credit-receipts.png',
                    click: function(){
                        self.redeem_credit_note(self.pos.cashregisters,self.pos.get('selectedOrder'));
                    },
                });
        },

        redeem_credit_note: function(cashregisters,order){
            var name=prompt("Please enter Credit Note Number","e.g Voucher");
            var x;
            var cashregister;
            if (name!=null){
                try{
                    amt = active_credit_note[name].remaining_amt;   
                    limit = active_credit_note[name].limits
                    var used_amt=prompt("Customer is available with " + amt + " amount with " + limit + " number of reddem chances\n How much to use:","");
                    if(used_amt > amt){
                            alert("You are trying to redeem more than available");
                            return false;
                        }
                    }
                catch(err){
                        x = "Invaild Voucher or Already been redeemed"
                        alert(x);
                        return false;
                    }
                for (var index in cashregisters){
                    journal_id = cashregisters[index].journal.code;
                    if (journal_id == "TCSH"){
                        cashregister = cashregisters[index];
                    }
                } 
                var paymentLines = order.get('paymentLines');
                var newPaymentline = new module.Paymentline({},{cashregister:cashregister, pos:this.pos});
                if(cashregister.journal.code === "TCSH"){
                    newPaymentline.set_amount( Math.max(parseInt(used_amt),0) );
                }
                paymentLines.add(newPaymentline);
                order.selectPaymentline(newPaymentline);
                new instance.web.Model('pos.credit.note').call('action_over_credit_note',[name,--limit,amt-used_amt])
            }
            console.log(index)
        },

        // creditNotePaymentLine: function(cashregister){
        //     debugger
        //     var paymentLines = this.get('paymentLines');
        //     var newPaymentline = new module.Paymentline({},{cashregister:cashregister, pos:this.pos});
        //     if(cashregister.journal.type !== 'cash'){
        //         newPaymentline.set_amount( Math.max(this.getDueLeft(),0) );
        //     }
        //     paymentLines.add(newPaymentline);
        //     this.selectPaymentline(newPaymentline);
        // }

    });


}




function openerp_pos_voucher_buuton(instance,module){
    // var module   = instance.point_of_sale;
    var round_pr = instance.web.round_precision;
    var QWeb = instance.web.qweb;

    QWeb.add_template('/voylla_stores/static/src/xml/voucher.xml');

    module.PosWidget.include({
        build_widgets: function(){
            var self = this;
            this._super();
            
            var discount = $(QWeb.render('VoucherButton'));
            // var active_voucher = {};

            discount.click(function(){
                var x;
                // var today = new Date();
                // var today_date = today.get_Date();
                // var today_month = today.get_month()+1;
                // var year = today.getFullYear();
                var loaded = new $.Deferred();
                var name=prompt("Please enter Voucher Number","e.g Voucher");
                current_order = self.pos.get('selectedOrder');
                // context = {name};
                total_paid = current_order.getTotalTaxIncluded()
                if (name!=null){

                    // var rounding = this.pos.currency.rounding;
                    try{
                        dis_percent = active_voucher[name].percent == 0 ? (Math.min(active_voucher[name].values,total_paid)*100/total_paid) : active_voucher[name].percent;
                        limit = active_voucher[name].limits;
                    }
                    catch(err){
                        x = "Invaild Voucher or Already been redeemed"
                        alert(x);
                        return false;
                    }
                    debugger;
                    order_lines = current_order.get('orderLines').models;
                    if(order_lines[0].discount != 0){
                        alert("Only one voucher is applicable in Order");
                        return false;
                    }
                    for (i=0; i<order_lines.length; i++){
                        sel_order_line = order_lines[i];
                        console.log(sel_order_line.price);
                        sel_order_line.discount = dis_percent;
                    }
                    // limit = --active_voucher[name].limits
                    new instance.web.Model('pos.discount.voucher').call('action_over_used_voucher',[name,limit])
                }
                else{
                    x = "Please check Voucher number or contact to admin";
                    alert(x);
                }

            });
            
                discount.appendTo(this.$('.control-buttons'));
                this.$('.control-buttons').removeClass('oe_hidden');
        },

    });

};

