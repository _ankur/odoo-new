function openerp_pos_override(instance, module){
    var QWeb = instance.web.qweb;
    var _t = instance.web._t;
    var round_pr = instance.web.round_precision;

    module.Order = module.Order.extend({
        initialize: function(attributes){
            Backbone.Model.prototype.initialize.apply(this, arguments);
            this.pos = attributes.pos; 
            this.sequence_number = this.pos.pos_session.sequence_number++;
            this.uid =     this.generateUniqueId();
            this.set({
                creationDate:   new Date(),
                orderLines:     new module.OrderlineCollection(),
                paymentLines:   new module.PaymentlineCollection(),
                name:           _t("Order ") + this.uid,
                client:         null,
            });
            this.selected_orderline   = undefined;
            this.selected_paymentline = undefined;
            this.screen_data = {};  // see ScreenSelector
            this.receipt_type = 'receipt';  // 'receipt' || 'invoice'
            this.temporary = attributes.temporary || false;
            this.fiscal_position_by_store = this.get_tax_as_fiscal_position();
            // this.comp_details=this.get_company_details();
            this.pos_details=this.get_shop_details();
            return this;
        },
        getTaxDetails: function(){
            var details = {};
            var fulldetails = [];
            var taxes_by_id = {};
            for(var i = 0; i < this.pos.taxes.length; i++){
                taxes_by_id[this.pos.taxes[i].id] = this.pos.taxes[i];
            }

            this.get('orderLines').each(function(line){
                var ldetails = line.get_tax_details();
                for(var id in ldetails){
                    if(ldetails.hasOwnProperty(id)){
                        details[id] = (details[id] || 0) + ldetails[id];
                    }
                }
            });
            
            for(var id in details){
                if(details.hasOwnProperty(id)){
                    fulldetails.push({amount: details[id], tax: taxes_by_id[id], name: taxes_by_id[id].name});
                }
            }

            return fulldetails;
        },

        get_All_Details: function(){
            var pos_details = self.pos_compliances[0];
            var fulldetails=[];
            fulldetails.push({tin_number: pos_details.tin_number, cst_number: pos_details.cst_number,cin_no: pos_details.cin_no, shop_address: pos_details.address ,name: pos_details.name ,shop_phone: pos_details.phone_no});
            return fulldetails;
        },

        get_shop_details: function(){
            var pos_details ={}
            fields = ['tin_number','cst_number','cin_no','address','name','phone_no'];
            domain = [['name','=',this.pos.config.name]]
            var record = new instance.web.Model('pos.config').query(fields).filter(domain).all()
                $.when(record).then(function(pos_compliances){
                self.pos_compliances=pos_compliances;
                pos_details=pos_compliances[0];
            });
             return pos_details;
        },


        
        get_tax_as_fiscal_position: function(){
            var fiscal_position = {}
            fields = ['tax_src_id','tax_dest_id'];
            domain = [['position_id','=',this.pos.config.fiscal_position[0]]]
            var record = new instance.web.Model('account.fiscal.position.tax').query(fields).filter(domain).all()
                $.when(record).then(function(fiscal){
                self.fiscal = fiscal;
                for(var i=0, len=fiscal.length; i<len; i++){
                    fiscal_position[i]=fiscal[i];
                    }
            });
            return fiscal_position;
        },
    });

    module.ClientListScreenWidget = module.ClientListScreenWidget.extend({

         save_client_details: function(partner) {
            var self = this;
            
            var fields = {}
            this.$('.client-details-contents .detail').each(function(idx,el){
                fields[el.name] = el.value;
            });

            if (!fields.name) {
                this.pos_widget.screen_selector.show_popup('error',{
                    message: _t('A Customer Name Is Required'),
                });
                return;
            }
            if(!fields.phone) {
                this.pos_widget.screen_selector.show_popup('error',{
                    message: _t('A Customer Phone Number Is Required'),
                });
                return;
            }
            
            if (this.uploaded_picture) {
                fields.image = this.uploaded_picture;
            }

            fields.id           = partner.id || false;
            fields.country_id   = fields.country_id || false;
            fields.ean13        = fields.ean13 ? this.pos.barcode_reader.sanitize_ean(fields.ean13) : false; 
            fields.email        = fields.email || 'retail_customer@voylla.com'

            new instance.web.Model('res.partner').call('create_from_ui',[fields]).then(function(partner_id){
                self.saved_client_details(partner_id);
            });
        },
        
        // what happens when we've just pushed modifications for a partner of id partner_id
        saved_client_details: function(partner_id){
            var self = this;
            this.reload_partners().then(function(){
                var partner = self.pos.db.get_partner_by_id(partner_id);
                if (partner) {
                    self.new_client = partner;
                    self.toggle_save_button();
                    self.display_client_details('show',partner);
                } else {
                    // should never happen, because create_from_ui must return the id of the partner it
                    // has created, and reload_partner() must have loaded the newly created partner. 
                    self.display_client_details('hide');
                }
            });
        },
    });

    module.Orderline = module.Orderline.extend({

        get_tax_details: function(){
            return this.get_all_prices().taxDetails;
        },
        get_all_prices: function(){
            var self = this;
            var currency_rounding = this.pos.currency.rounding;
            var base = this.get_base_price();
            var totalTax = base;
            var totalNoTax = base;
            
            var product =  this.get_product(); 
            var taxes_ids = product.taxes_id;
            var taxes =  self.pos.taxes;
            var taxtotal = 0;
            var taxdetail = {};
            for (i=0; i<taxes_ids.length; i++){
                for(k=0; k<fiscal.length; k++){
                    if(taxes_ids[i] == fiscal[k]['tax_src_id'][0]){
                            taxes_ids[i] = fiscal[k]['tax_dest_id'][0];
                            break;
                        }
                }

                if(taxes_ids[i] == undefined){
                    delete taxes_ids[i]
                }
            }
            _.each(taxes_ids, function(el) {
                var tax = _.detect(taxes, function(t) {return t.id === el;});
                if (tax.price_include) {
                    var tmp;
                    if (tax.type === "percent") {
                        tmp =  base - round_pr(base / (1 + tax.amount),currency_rounding); 
                    } else if (tax.type === "fixed") {
                        tmp = round_pr(tax.amount * self.get_quantity(),currency_rounding);
                    } else {
                        throw "This type of tax is not supported by the point of sale: " + tax.type;
                    }
                    tmp = round_pr(tmp,currency_rounding);
                    taxtotal += tmp;
                    totalNoTax -= tmp;
                    taxdetail[tax.id] = tmp;
                } else {
                    var tmp;
                    if (tax.type === "percent") {
                        tmp = tax.amount * base;
                    } else if (tax.type === "fixed") {
                        tmp = tax.amount * self.get_quantity();
                    } else {
                        throw "This type of tax is not supported by the point of sale: " + tax.type;
                    }
                    tmp = round_pr(tmp,currency_rounding);
                    taxtotal += tmp;
                    totalTax += tmp;
                    taxdetail[tax.id] = tmp;
                }
            });
            return {
                "priceWithTax": totalTax,
                "priceWithoutTax": totalNoTax,
                "tax": taxtotal,
                "taxDetails": taxdetail,
            };
        },

    });


    module.PosModel = module.PosModel.extend({  

        check_customer: function(order){
            return order.get_client();
        },

    });


    module.PaymentScreenWidget = module.PaymentScreenWidget.extend({
        
        validate_order: function(options) {
            var self = this;
            options = options || {};

            var currentOrder = this.pos.get('selectedOrder');
            var client=this.pos.check_customer(currentOrder);
            if(!client & options.validate){
                self.pos_widget.screen_selector.show_popup('error',{
                message: _t('An anonymous order cannot be invoiced'),
                comment: _t('Please select a customer for this order. This can be done by clicking the customer tab.'),
            });
                return;
            }

            if(currentOrder.get('orderLines').models.length === 0){
                this.pos_widget.screen_selector.show_popup('error',{
                    'message': _t('Empty Order'),
                    'comment': _t('There must be at least one product in your order before it can be validated'),
                });
                return;
            }

            if(!this.is_paid()){
                return;
            }

            // The exact amount must be paid if there is no cash payment method defined.
            if (Math.abs(currentOrder.getTotalTaxIncluded() - currentOrder.getPaidTotal()) > 0.00001) {
                var cash = false;
                for (var i = 0; i < this.pos.cashregisters.length; i++) {
                    cash = cash || (this.pos.cashregisters[i].journal.type === 'cash');
                }
                if (!cash) {
                    this.pos_widget.screen_selector.show_popup('error',{
                        message: _t('Cannot return change without a cash payment method'),
                        comment: _t('There is no cash payment method available in this point of sale to handle the change.\n\n Please pay the exact amount or add a cash payment method in the point of sale configuration'),
                    });
                    return;
                }
            }

            if (this.pos.config.iface_cashdrawer) {
                    this.pos.proxy.open_cashbox();
            }

            if(options.invoice){
                // deactivate the validation button while we try to send the order
                this.pos_widget.action_bar.set_button_disabled('validation',true);
                this.pos_widget.action_bar.set_button_disabled('invoice',true);

                var invoiced = this.pos.push_and_invoice_order(currentOrder);

                invoiced.fail(function(error){
                    if(error === 'error-no-client'){
                        self.pos_widget.screen_selector.show_popup('error',{
                            message: _t('An anonymous order cannot be invoiced'),
                            comment: _t('Please select a client for this order. This can be done by clicking the order tab'),
                        });
                    }else{
                        self.pos_widget.screen_selector.show_popup('error',{
                            message: _t('The order could not be sent'),
                            comment: _t('Check your internet connection and try again.'),
                        });
                    }
                    self.pos_widget.action_bar.set_button_disabled('validation',false);
                    self.pos_widget.action_bar.set_button_disabled('invoice',false);
                });

                invoiced.done(function(){
                    self.pos_widget.action_bar.set_button_disabled('validation',false);
                    self.pos_widget.action_bar.set_button_disabled('invoice',false);
                    self.pos.get('selectedOrder').destroy();
                });

            }else{
                this.pos.push_order(currentOrder) 
                if(this.pos.config.iface_print_via_proxy){
                    var receipt = currentOrder.export_for_printing();
                    this.pos.proxy.print_receipt(QWeb.render('XmlReceipt',{
                        receipt: receipt, widget: self,
                    }));
                    this.pos.get('selectedOrder').destroy();    //finish order and go back to scan screen
                }else{
                    this.pos_widget.screen_selector.set_current_screen(this.next_screen);
                }
            }

            // hide onscreen (iOS) keyboard 
            setTimeout(function(){
                document.activeElement.blur();
                $("input").blur();
            },250);
        },

    });

    

};