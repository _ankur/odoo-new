import logging
import time

from openerp import tools
from openerp.osv import fields, osv
from openerp.tools.translate import _
# from openerp import models, fields, api, _
import openerp.addons.decimal_precision as dp
import openerp.addons.product.product
import pdb
import string
import random
import requests

_logger = logging.getLogger(__name__)

class pos_discount_voucher(osv.osv):
	_name = 'pos.discount.voucher'
	_columns={
	'name':fields.text('Voucher Code', select=1),
	'expire_date':fields.date('Expier On'),
	'active':fields.boolean('Active'),
	'values':fields.float('Flat Off'),
	'percent':fields.float('Percent Off'),
	'limits':fields.integer('Limit')
	}
	_sql_constraints = [
        ('unique_discount_note', 'unique(name)', 'Discount Voucher already exist')
        ]
	

	def action_over_used_voucher(self,cr,uid,name,limit,context=None):
		voucher_id = self.search(cr,uid,[('name','=',name)]);
		voucher_obj = self.browse(cr,uid,voucher_id[0]);
		if limit <= 0:
		   self.write(cr,uid,voucher_obj.id, {'active':False});
		return;

# class pos_order_credit(osv.osv):
# 	_name = 'pos.order'
# 	_inherit = 'pos.order'



class pos_credit_note(osv.osv):
	_name = 'pos.credit.note'
	# _inherit = 'pos.make.payment'

	_columns={
		'name':fields.char('Credit Note',select=1,readonly=True),
		'expire_date':fields.date('Expier On'),
		'active':fields.boolean('Active'),
		'amount':fields.float('Flat Off'),
		'remaining_amt':fields.float('Amt Left'),
		'limits':fields.integer('Limit'),
		'journal_id':fields.many2one('account.journal','Payment Mode'),
		'return_order_id':fields.many2one('pos.order','Issue Order'),
		'mobile':fields.char('Mobile Number', required=True)
		}
	_defaults={
		'limits':1,
		'active':True,
		'name' : lambda self,cr,uid,x: ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(16)),
		'journal_id':15,
	}
	_sql_constraints = [
        ('unique_credit_note', 'unique(name)', 'Credit Note already exist')
        ]

	def send_sms(self, cr, uid, phone, name,chance,rem_amt,expire_on,context=None):
		username="jagrati@voylla.in"
		password="d3fault03"
		phone_number = phone
		smstext = "Hi. Credit note %s worth Rs. %d with has been generated for redemption on your next purchase. Valid till %s."%(name,rem_amt,expire_on)
		user_agent = 'Mozilla/4.0 (compatible; MSIE 5.5; Windows NT)'
		headers = { 'User-Agent' : user_agent }

		url= "https://api.mVaayoo.com/mvaayooapi/MessageCompose?user=jagrati@voylla.in:d3fault03&senderID=VOYLLA&receipientno="+phone_number+"&dcs=0&msgtxt="+smstext+"&state=4"
		s = requests.Session()
		x = s.get(url,data={},headers={ 'User-Agent' : user_agent },verify=False)
		return

	def generate_return_credit_note(self,cr,uid,ids,context=None):
		context = context or {}
		order_obj = self.pool.get('pos.order')
		active_id = context and context.get('active_id', False)
		order = order_obj.browse(cr, uid, active_id, context=context)
		amount = order.amount_total - order.amount_paid
		data = self.read(cr, uid, ids, context=context)[0]
		# this is probably a problem of osv_memory as it's not compatible with normal OSV's
		data['journal'] = data['journal_id'][0]
		data['remaining_amt'] = data['amount']
		data['amount'] = -data['amount']
		self.write(cr,uid,ids,{'remaining_amt':data['remaining_amt'],'return_order_id':active_id})
		if amount != 0.0:
			order_obj.add_payment(cr, uid, active_id, data, context=context)
			order.write({'state':'paid'})
		self.send_sms(cr,uid,data['mobile'],data['name'],data['limits'],data['remaining_amt'],data['expire_date'])

		return

		# return self.launch_payment(cr, uid, ids, context=context)

	def launch_payment(self, cr, uid, ids, context=None):
		return {
			'name': _('Payment'),
			'view_type': 'form',
			'view_mode': 'form',
			'res_model': 'pos.make.payment',
			'view_id': False,
			'target': 'new',
			'views': False,
			'type': 'ir.actions.act_window',
			'context': context,
		}

	def action_over_credit_note(self,cr,uid,name,chance,rem_amt,context=None):
		creditID = self.search(cr,uid,[('name','=',name)])[0]
		creditObj = self.write(cr,uid,creditID,{'remaining_amt':rem_amt,'limits':chance})
		return