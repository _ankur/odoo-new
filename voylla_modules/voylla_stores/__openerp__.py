{
    'name': "Voylla Store",
    'version': "1.0",
    'author': "Ankur Gupta",
    'category': "Tools",
    'depends': ['point_of_sale'],
    'data': [
        'voucher.xml',
        'gift_voucher.xml',
        'pos_view.xml'
        ],
    'js':[
        'static/src/js/store_main.js',
        'static/src/js/voucher.js',
        'static/src/js/override.js'
    ],
    'update_xml': [],
    'demo': [],
    'installable': True,
    'auto_install': False,
    'application': True,
}