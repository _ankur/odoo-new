from openerp.osv import fields, osv
import openerp.addons.decimal_precision as dp
import pdb
import datetime 
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
from datetime import datetime, timedelta

class shop_config(osv.osv):
	_inherit='pos.config'

	_columns = {
	'fiscal_position' : fields.many2one('account.fiscal.position','Fiscal Position'),
	'tin_number' : fields.char('TIN No.',help="TIN Number of the Store"),
    'cst_number' : fields.char('CST No.',help="CST Number of the Store"),
    'address' : fields.char('Address',help="Address of the Store"),
    'cin_no'  : fields.char('CIN No.'),
    'phone_no'  : fields.char('Phone No.'),
	}

	# def fiscal_position_by_store(self,cr,uid,fiscal_id,context=None):
	# 	fiscal_obj = self.pool.get('account.fiscal.position').browse(cr,uid,fiscal_id)
	# 	position_tax = {}
	# 	for iterate in fiscal_obj.tax_ids:
	# 		position_tax.update({iterate.tax_src_id.id:iterate.tax_dest_id.id})
	# 		pdb.set_trace()
	# 	return position_tax

class pos_order_line(osv.osv):
	_inherit='pos.order.line'
	_name='pos.order.line'


	def _amount_line_all(self, cr, uid, ids, field_names, arg, context=None):
		res = dict([(i, {}) for i in ids])
		taxes_ids = []
		account_tax_obj = self.pool.get('account.tax')
		cur_obj = self.pool.get('res.currency')
		pos_config_name = self.browse(cr, uid, ids, context=context).order_id['display_name'].split("/")[0]
		config_obj = self.pool.get('pos.config')
		config_id = config_obj.search(cr,uid,[('name','=',pos_config_name)])
		fiscal_position = config_obj.browse(cr,uid,config_id).fiscal_position.tax_ids
		for line in self.browse(cr, uid, ids, context=context):
			tax_ids = [ tax for tax in line.product_id.taxes_id if tax.company_id.id == line.order_id.company_id.id ]
			for tax_id in tax_ids:
				for item in fiscal_position:
					if (tax_id == item.tax_src_id):
						tax_id =  item.tax_dest_id
				taxes_ids.append(tax_id)
			price = line.price_unit * (1 - (line.discount or 0.0) / 100.0)
			taxes = account_tax_obj.compute_all(cr, uid, taxes_ids, price, line.qty, product=line.product_id, partner=line.order_id.partner_id or False)
			cur = line.order_id.pricelist_id.currency_id
			res[line.id]['price_subtotal'] = cur_obj.round(cr, uid, cur, taxes['total'])
			res[line.id]['price_subtotal_incl'] = cur_obj.round(cr, uid, cur, taxes['total_included'])
		return res

	_columns = {
		'price_subtotal': fields.function(_amount_line_all, multi='pos_order_line_amount', digits_compute=dp.get_precision('Account'), string='Subtotal w/o Tax', store=True),
		'price_subtotal_incl': fields.function(_amount_line_all, multi='pos_order_line_amount', digits_compute=dp.get_precision('Account'), string='Subtotal', store=True),
	}