from __future__ import with_statement
from fabric.api import *
from fabric.network import ssh
from contextlib import contextmanager as _contextmanager


env.directory='/mnt/odoo'
env.vdirectory='/mnt/odoo/env_odoo'
env.activate='. /mnt/odoo/env_odoo/bin/activate'

deploy_user="ubuntu"

dependencies="libpq-dev python-dev libxml2-dev libxslt-dev libldap2-dev libsasl2-dev libssl-dev"

gunicron_pid_file="/mnt/odoo/pid"
gunicorn_access_logfile="/var/log/odoo/gunicorn-access_gevent1.log"
gunicorn_error_logfile="/var/log/odoo/gunicorn-error_gevent1.log"
command_gunicorn="gunicorn -b 0.0.0.0:8069 --pid %s  --workers=2 -k gevent --worker-connections=2000 \
--backlog=1000 --daemon --access-logfile %s  \
--error-logfile %s openerp-wsgi"%(gunicron_pid_file,gunicorn_access_logfile,gunicorn_error_logfile)

def beta():
    env.user = 'root'
    env.hosts=['odoo.voylla.com']

def production():
	print 'Environment for production'
	#environment for production

def install_dependency():
	sudo('apt-get install %s'%(dependencies))


def check_deploy_dir():
	cmd='[ -d %s ] && echo "exists" || echo "not exists"'%(env.directory)
	out=sudo("%s"%(cmd))
	if out=='not exists':
		sudo('mkdir %s'%(env.directory))
	
def enter_env():
	with settings(sudo_user=deploy_user):
		cmd = '[ -d %s ] && echo "exists" || echo "not exists"'%(env.vdirectory)
		out = run("%s"%(cmd))
		if out == 'exists':
			with virtualenv():
				run('pip install -r requirements.txt')

def pull_code():
	with cd(env.directory):
		sudo('git checkout . && git pull')

def restart_gunicorn():
	with settings(sudo_user=deploy_user,warn_only=True):
		with virtualenv():
			cmd='[ -f %s ] && echo "exists" || echo "not exists"'%(gunicron_pid_file)
			out=run("%s"%(cmd))
			if out == 'not exists':
				run(command_gunicorn)
			if out == 'exists':
				pid = run('cat %s'%(gunicron_pid_file))	
				result=run('kill -9 %s'%(pid))
				if result.return_code == 1:
					print 'No process to kill,Starting Now'
				else:
					print 'Restarting'
				run(command_gunicorn,pty=False)




@_contextmanager
def virtualenv():
    with cd(env.directory):
        with prefix(env.activate):
            yield


# scp -r foo your_username@remotehost.edu:/some/remote/directory/bar 



